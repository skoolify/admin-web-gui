<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['students'] = array();
        $data['records'] = array();

        return view('attendance.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $this->schools = $this->schoolHelper->list();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        return view('roles.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'roleName' => 'required|min:3',
            'roleDescription' => 'required|min:3',
            'school' => 'required',
            'isActive' => 'required'
        ]);
        
        $role = $this->roleHelper->save($request);
        if($role->response === 'success'):
            $message[] = 'success';
            $message[] = 'Role has been successfully added';
            return redirect('roles')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding role';
            return redirect('roles')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($roleId, Request $request)
    {
        
        $token = session()->get('apiToken');
        $this->schools = $this->schoolHelper->list();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $role = $this->roleHelper->detail($roleId);
        print_r($role);
        exit;
        $data['record'] = $role;

        return view('roles.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $validateDate = $request->validate([
            'roleName' => 'required|min:3',
            'roleDescription' => 'required|min:3',
            'school' => 'required',
            'isActive' => 'required'
        ]);

        $role = $this->roleHelper->update($id, $request);
        if($feeType->response === 'success'):
            $message[] = 'success';
            $message[] = 'Role has been successfully updated';
            return redirect()->route('roles')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
