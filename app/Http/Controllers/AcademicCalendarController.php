<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{CalendarDayHelper, SchoolApiHelper, ApiHelper};

class AcademicCalendarController extends Controller
{
    private $schoolHelper, $api, $schools, $calendarDays, $calendarDayHelper;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->schoolHelper = new SchoolApiHelper();
        $this->calendarDayHelper = new CalendarDayHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classes'] = array();
        $data['records'] = array();

        return view('academic-calendar.list', $data);

    }
}
