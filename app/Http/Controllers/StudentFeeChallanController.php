<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use NumberFormatte;
use App\Http\Helpers\ApiHelper;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};
use Illuminate\Support\Str;

class StudentFeeChallanController extends Controller
{
    private $apiUrl, $api;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
        $this->api = new ApiHelper();
    }

    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classes'] = array();
        $data['students'] = array();
        $data['feetypes'] = array();

        return view('student-fee-challan.list', $data);

    }

    public function printFeeVoucher(Request $request, $id)
    {
        $params = array();
        $data = array();
        $data['result'] = array();
        $mainUrl = $this->apiUrl.'/getFeesVoucherPrint';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        $params['schoolID'] = $request->query('schoolID');
        $params['branchID'] = $request->query('branchID');
        $params['classID'] = $request->query('classID');
        $params['billingPeriodID'] = $id;
        if(!empty($request->query('studentID'))){
            $params['studentID'] = $request->query('studentID');
        }
        try{
            $response = $client->request('POST', $mainUrl, [
                'form_params' => $params
            ]);
            $data['success'] =  true;
            $data['error'] =  false;
            $data['data'] = json_decode($response->getBody());
            $data['code'] =  $response->getStatusCode();
            if(isset($data['data']->response) && isset($data['data']->response->userMessage)){
                $data['code'] = 404;
                $data['message'] = $data['data']->response->userMessage;
                
            }
        }catch(BadResponseException $e){
                $error = true;
            if ($e->hasResponse()){
                $response = $e->getResponse();
                $data['success'] =  false;
                $data['error'] =  true;
                $data['data'] = json_decode($response->getBody()->getContents());
                $data['code'] =  $response->getStatusCode();
            }
        }
        $data['title'] = "Fee Voucher";
        // print_r($data);
        // die();
        // if(isset($data['data']->response[0]) && count($data['data']->response[0]) > 0 && isset($data['data']->response[0]->feeChallan[0]) && count($data['data']->response[0]->feeChallan[0]) > 0){
        //     $data['result'] = $data['data']->response[0]->feeChallan[0];
        // }
        // $pdf = PDF::loadView('student-fee-challan.student-voucher-challan', $data);  
        // return $pdf->stream('student Voucher.pdf');
        return view('student-fee-challan.student-voucher-challan', $data);
    }

    public static function numberInWords($number = 0)
    {
        $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $number = $f->format($number);
        return $number;
    }
}
