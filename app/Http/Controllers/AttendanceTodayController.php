<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttendanceTodayController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['students'] = array();
        $data['records'] = array();

        return view('attendance-today.list', $data);

    }
}
