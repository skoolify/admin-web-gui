<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ApiHelper, SchoolLogoApiHelper};

class SchoolLogoController extends Controller
{
    
    private $schoolLogoHelper, $api, $token, $schoolLogo;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->schoolLogoHelper = new SchoolLogoApiHelper();
        $this->schoolLogo = env('DEFAULT_SCHOOL_LOGO');
    }
    
    public function index(Request $request)
    {
        $data = array();
        $data['error'] = true;
        $token = session()->get('apiToken')->response;
        if($token):
            $logo = $this->schoolLogoHelper->logo($request);
            $code  = $logo['code'];
            $logo = $logo['body'];
            if( !($logo->error) && (int)$code !== 503 && ($response = $logo->response)):
                $data['error'] = false;
                $data['response'] = last($response);
                $logo = str_contains($data['response']->schoolLogo, 'base64,') ? $data['response']->schoolLogo : 'data:image/*;base64,'.$data['response']->schoolLogo;
                session(['schoolLogo' => $logo]);
            else:
                $data['error'] = true;
                $data['code'] = $code;
                session(['schoolLogo' => $this->schoolLogo]);
            endif;
        else:
            $data['error'] = true;
            session(['schoolLogo' => $this->schoolLogo]);
        endif;

        return response()->json($data);
    }
}
