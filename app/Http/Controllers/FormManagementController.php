<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormManagementController extends Controller
{
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['classLevels'] = array();
        $data['students'] = array();
        $data['timeZones'] = array();

        return view('form-management.list')->with($data);

    }
}
