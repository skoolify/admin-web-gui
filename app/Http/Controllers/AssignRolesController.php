<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AssignRolesController extends Controller
{
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['records'] = array();
        $data['roles'] = array();

        return view('assign-roles.list', $data);

    }
}
