<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\{PaymentHelper,SchoolPaymentHelper};
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    private $paymentHelper, $api, $token;

    public function __construct()
    {
        $this->paymentHelper = new PaymentHelper();
        $this->api = new ApiHelper();
    }

    public function viewForm(Request $request)
    {
        extract($request->all());
        $this->token = $this->api->token();
        $viewParams = array();
        if($request->all() && count($request->all()) > 0){
            if($this->token && $this->token->response && $data != ''){
                $this->token = $this->token->response;
                $apiDecrptedData = $this->paymentHelper->decryptData($this->token, $data);
                if(isset($apiDecrptedData) && $apiDecrptedData->response && $apiDecrptedData->response !== ''){
                    $d = base64_decode($apiDecrptedData->response);
                    $hash = $this->paymentHelper->generateHash($d);
                }else{
                    return abort(301);
                }
                $params = array();
                foreach (explode(',', $d) as $k => $value) {
                    $key = head(explode('=', $value));
                    $val = last(explode('=', $value));
                    if($key === 'secret_key'){
                        unset($params[$key]);
                    }else if($key === 'bill_to_address_line1'){
                        $customParam = base64_decode(str_replace('bill_to_address_line1=', '', $value));
                        $params[$key] = $customParam;
                    }else{
                        if ($key === 'signed_field_names'):
                            $params[$key] = preg_replace('/\|+/', ',', $val);
                        else:
                            $params[$key] = $val;
                        endif;
                    }
                    if($key === 'providerURL'){
                        $viewParams['providerURL'] = $val;
                    }
                }
                $viewParams['params'] = $params;
                $viewParams['hash'] = $hash;
                return view('payment.hbl-payment', $viewParams);
            }else{
                return abort(301);
            }
        }else{
            return abort(301);
        }
    }

    public function paymentStatus(Request $request){

        $postBackData = array();
        $data = $request->all();

        foreach ($data as $key => $value) {
            $postBackData[$key] = $value;
        }
        try {
            //code...
            $token = $this->api->token();
           $responseData = $this->paymentHelper->paymentStatus($token, $postBackData);
        } catch (\Exception $e) {
            //throw $th;
            \Log::error($e);
        }

        return view('payment.payment-status',compact('postBackData'));
    }
}
