<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentFeeController extends Controller
{
    public function index(Request $request){
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['classLevels'] = array();
        $data['students'] = array();
        $data['feetypes'] = array();

        return view('student-fee.list', $data);
    }
}
