<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ApiHelper, SchoolBranchApiHelper, SchoolApiHelper, CountryApiHelper};

class SchoolBranchesController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['areas'] = array();
        $data['cities'] = array();
        $data['countries'] = array();

        return view('school-branches.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $countries = array();

        $data['schoolId'] = $request->query('schoolId');
        $data['countries'] = gettype($this->countries->response) === 'array' ? $this->countries->response : array();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();

        return view('school-branches.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validateDate = $request->validate([
            'branchName' => 'required|min:3',
            'branchCode' => 'required|min:2',
            'branchPrincipleName' => 'required|min:3',
            'branchCountry' => 'required',
            'branchCity' => 'required',
            'branchArea' => 'required',
            'branchSchool' => 'required',
            'branchEmail' => 'required|email',
            'branchContact1' => 'required',
            'branchGpsCoords' => 'required',
            'branchAddress1' => 'required',
            'branchAddress2' => 'required',
            'branchAddress3' => 'required',
            'isActive' => 'required',
        ]);
        
        $token = $this->api->token();
        if($token->response):
            $branch = $this->branchHelper->save($token->response, $request);
            if($branch->response === 'success'):
                $message[] = 'success';
                $message[] = 'Branch has been successfully added';
                return redirect()->route('school-branches', ['schoolId' => $request->input('branchSchool')])->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $branchId)
    {
        
        $schoolId = $request->query('schoolId');

        if($schoolId && $branchId):
            $data['schoolId'] = $schoolId;
            $data['branchId'] = $branchId;
            $data['countries'] = gettype($this->countries->response) === 'array' ? $this->countries->response : array();
            $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();

            $token = $this->api->token();
            $branch = $this->branchHelper->detail($token->response, $schoolId, $branchId);

            if($branch->response):
                $data['record'] = head($branch->response);
            endif;

        else:
            $message[] = 'danger';
            $message[] = 'No School found';
            return redirect()->route('school-branches')->with('flash-message', $message);
        endif;        

        return view('school-branches.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $branchId)
    {
        
        $validateDate = $request->validate([
            'branchName' => 'required|min:3',
            'branchCode' => 'required|min:2',
            'branchPrincipleName' => 'required|min:3',
            'branchCountry' => 'required',
            'branchCity' => 'required',
            'branchArea' => 'required',
            'branchSchool' => 'required',
            'branchEmail' => 'required|email',
            'branchContact1' => 'required',
            'branchGpsCoords' => 'required',
            'branchAddress1' => 'required',
            'isActive' => 'required',
        ]);

        if($branchId):
            $token = $this->api->token();
            if($token->response):
                $branch = $this->branchHelper->update($token->response, $branchId, $request);
                if($branch->response === 'success'):
                    $message[] = 'success';
                    $message[] = 'Branch has been successfully updated';
                    return redirect()->route('school-branches', ['schoolId' => $request->input('branchSchool')])->with('flash-message', $message);
                endif;
            else:
                echo 'Error in Response';
            endif;
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
