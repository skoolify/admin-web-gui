<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OtpController extends Controller
{
    public function index(Request $request)
    {
        $data['countries'] = array();
        $data['cities'] = array();
        $data['schools'] = array();
        $data['branches'] = array();
       
        return view('otp-management.list', $data);

    }
}
