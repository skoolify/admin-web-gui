<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PHPExcel_IOFactory;
use PHPExcel_Style_Protection;
use File;
use PHPExcel;
use PHPExcel_Style;
use PHPExcel_Style_NumberFormat;

class ImportParentStudentsController extends Controller
{
    public function index()
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['classLevels'] = array();

        return view('import-excel.list', $data);
    }

    public function importExcel(Request $request)
    {
        $validateDate = $request->validate([
            'studentsExcelFile' => 'required|mimes:xls,xlsx,csv'
        ]);
        $name = $request->file('studentsExcelFile')->getClientOriginalName();
        $fileName = \Storage::disk('public')->putFileAs('sheets', $request->file('studentsExcelFile'), $name);

        $filePath = \Storage::disk('public')->path($fileName);
        $excelReader = PHPExcel_IOFactory::load($filePath);
        $i = 2;
        $classes = $request->classArray;
        $Sections = $request->sectionArray;
        $classSection = $request->classSectionArray;

        $classesArray = array();
        $sectionArray = array();
        $classSectionArray = array();
        $rollNumbersArray = array();
        $classSectionRollNoArray = array();

        $classesArray[] = explode(',', $classes);
        $sectionArray[] = explode(',', $Sections);
        $classSectionArray[] = explode(',', $classSection);

        $dataErrorCheck = false;
        $records = array();
        $accessFileCell = $excelReader->getActiveSheet();
        $comparisonCollection = collect();

        while($accessFileCell->getCell('A'.$i)->getValue() != ''){
            $data = array();
            $data['schoolID'] = $request->schoolID ?? session()->get('user')->schoolID;
            $data['branchID'] = $request->branchID ?? session()->get('user')->branchID;
            $studentName = trim($accessFileCell->getCell('A'.$i)->getValue());
                if($studentName !== ''){
                    $data['studentName'] = $studentName;
                }else{
                    $data['studentName'] = $studentName;
                    $data['studentNameError'] = 1;
                    $dataErrorCheck = true;
                }

            $gender = trim($accessFileCell->getCell('B'.$i)->getValue());
                if($gender === 'M' || $gender === 'F'){
                    $data['gender'] = $gender;
                }else{
                    $data['gender'] = $gender;
                    $data['genderError'] = 1;
                    $dataErrorCheck = true;
                }

            $className = trim($accessFileCell->getCell('C'.$i)->getValue());
                if($className !== '' && in_array($className,$classesArray[0])){
                    $data['classLevelName'] = $className;
                }else{
                    $data['classLevelName'] = $className;
                    $data['classLevelNameError'] = 1;
                    $dataErrorCheck = true;
                }

            $classSection = trim($accessFileCell->getCell('D'.$i)->getValue());

                if($classSection !== '' && in_array($className.$classSection, $classSectionArray[0])){
                    $data['classSection'] = $classSection;
                }else{
                    $data['classSection'] = $classSection;
                    $data['classSectionError'] = 1;
                    $dataErrorCheck = true;
                }

            $rollNo = trim($accessFileCell->getCell('E'.$i)->getValue());
                if($rollNo !== ''){
                    $checkRollNo = $className.$classSection.$rollNo;
                    if(!(in_array($checkRollNo, $classSectionRollNoArray))){
                        // if(!(in_array($rollNo, $rollNumbersArray))){
                        $data['rollNo'] = $rollNo;
                        $rollNumbersArray[] = $rollNo;
                        $classSectionRollNoArray[] = $className.$classSection.$rollNo;
                    }else{
                        $data['rollNo'] = $rollNo;
                        $data['rollNoError'] = 1;
                        $dataErrorCheck = true;
                    }
                }else{
                    $data['rollNo'] = "<p style='color:red;' data-toggle='tooltip' title='Roll Number is missing'> ".$rollNo."</p>";
                    $dataErrorCheck = true;
                }

            $dob = trim($accessFileCell->getCell('F'.$i)->getValue());
                if($dob !== ''){
                    $dob = PHPExcel_Style_NumberFormat::toFormattedString($dob, PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
                    $data['dob'] = $dob;
                }else{
                    $data['dob'] = $dob;
                    $data['dobError'] = 1;
                    $dataErrorCheck = true;
                }
            $data['fatherName'] = trim($accessFileCell->getCell('G'.$i)->getValue());
            $data['fatherCNIC'] = trim($accessFileCell->getCell('H'.$i)->getValue());

            $fatherMobileNumber = trim($accessFileCell->getCell('I'.$i)->getValue());
                if($fatherMobileNumber !== ''){
                    if(!(strlen($fatherMobileNumber) < 8)){
                        $data['fatherMobileNo'] = $fatherMobileNumber;
                    }else{
                        $data['fatherMobileNo'] = $fatherMobileNumber;
                        $data['fatherMobileNoError'] = 1;
                        $dataErrorCheck = true;
                    }
                }else{
                    $data['fatherMobileNo'] = $fatherMobileNumber;
                }
            $data['fatherEmail'] = trim($accessFileCell->getCell('J'.$i)->getValue());
            $data['motherName'] = trim($accessFileCell->getCell('K'.$i)->getValue());
            $data['motherCNIC'] = trim($accessFileCell->getCell('L'.$i)->getValue());

            $motherMobileNumber = trim($accessFileCell->getCell('M'.$i)->getValue());
                if($motherMobileNumber !== ''){
                    if(!(strlen($motherMobileNumber) < 8)){
                        $data['motherMobileNo'] = $motherMobileNumber;
                    }else{
                        $data['motherMobileNo'] = $fatherMobileNumber;
                        $data['motherMobileNoError'] = 1;
                        $dataErrorCheck = true;
                    }
                }else{
                    $data['motherMobileNo'] = $motherMobileNumber;
                }

            $data['motherEmail'] = trim($accessFileCell->getCell('N'.$i)->getValue());

            $collection = collect();
            $arr = array(
                'studentName',
                'classLevelName',
                'rollNo',
                'classSection',
                'dob',
                'fatherMobileNo',
                'motherMobileNo'
            );
            extract($data);
            foreach ($arr as $key => $value) {
                $collection->put($value, ${$value});
            }
            foreach ($arr as $key => $value) {
                $q = $comparisonCollection->where($value, ${$value});
            }
            $qu = $q->where('studentName', $studentName)
                    ->where('classLevelName', $classLevelName)
                    ->where('rollNo', $rollNo)
                    ->where('classSection', $classSection)
                    ->where('dob', $dob)
                    ->where('fatherMobileNo', $fatherMobileNo)
                    ->where('motherMobileNo', $motherMobileNo)
                    ->first();

            if($qu){
                $collection->put('error', true);
            }
            $comparisonCollection->push($collection);

            $i++;
            $records['data'][] =  $data;
        }
        if($dataErrorCheck === true){
            $records['errors'] = $dataErrorCheck;
        }
        if(File::exists($filePath)){
            unlink($filePath);
        }

        $records['comparison'] = $comparisonCollection->toArray();
        return response()->json($records);
    }
}
