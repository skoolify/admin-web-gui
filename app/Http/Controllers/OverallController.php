<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OverallController extends Controller
{
    public function index(Request $request){
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['classLevels'] = array();
        $data['records'] = array();
        $data['students'] = array();

        return view('overall.list', $data);
    }
}
