<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BillingPeriodController extends Controller
{
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();

        return view('billing-period.index')->with($data);

    }
}
