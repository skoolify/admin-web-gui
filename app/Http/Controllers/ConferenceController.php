<?php namespace App\Http\Controllers;

class ConferenceController extends Controller
{
    public function index()
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['classLevels'] = array();
        $data['records'] = array();
        $data['students'] = array();
        $data['subjects'] = array();
        $data['timeZones'] = array();
        return view('conference.list', $data);
    }
}
