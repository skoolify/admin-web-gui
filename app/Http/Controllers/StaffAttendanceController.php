<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffAttendanceController extends Controller
{
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();

        return view('staff-attendance.list')->with($data);

    }
}
