<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommRequestsController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['records'] = array();
        $data['students'] = array();

        return view('comm-requests.list', $data);

    }
}
