<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorsController extends Controller
{
    
    public function error404(Request $request){
        return view('errors.404');
    }

    public function error403(Request $request){
        return view('errors.403');
    }

    public function error500(Request $request){
        return view('errors.500');
    }
    
}
