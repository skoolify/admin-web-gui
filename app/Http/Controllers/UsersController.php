<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ApiHelper, SchoolApiHelper, UserApiHelper, SchoolBranchApiHelper};

class UsersController extends Controller
{
    private $schoolHelper, $api, $userHelper, $branchHelper;
    private $branches, $schools;
    
    public function __construct()
    {
        $this->schoolHelper = new SchoolApiHelper();
        $this->userHelper = new UserApiHelper();
        $this->branchHelper = new SchoolBranchApiHelper();
        $this->api = new ApiHelper();
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['records'] = array();

        return view('users.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $this->schools = $this->schoolHelper->list();
        $this->branches = $this->branchHelper->list();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['branches'] = gettype($this->branches->response) === 'array' ? $this->branches->response : array();        
        return view('users.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'fullName' => 'required|min:3',
            'mobileNo' => 'required|min:3',
            'password' => 'required',
            'emailAddress' => 'required',
            'school' => 'required',
            'isActive' => 'required'
        ]);
        $user = $this->userHelper->save($request);
        if($user->response === 'success'):
            $message[] = 'success';
            $message[] = 'User has been successfully added';
            return redirect()->route('users')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding user';
            return redirect()->route('users')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = $this->userHelper->detail($id);
        $record = $this->api->extractDetail($record->response, $id, 'userID');
        $schools = $this->schoolHelper->list();
        $data['schools'] = gettype($schools->response) === 'array' ? $schools->response : array();
        $data['record'] = $record;
        return view('users.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validateDate = $request->validate([
            'fullName' => 'required|min:3',
            'mobileNo' => 'required|min:3',
            'password' => 'required',
            'emailAddress' => 'required',
            'school' => 'required',
            'isActive' => 'required'
        ]);
        
        $user = $this->userHelper->update($id, $request);
        if($user->response === 'success'):
            $message[] = 'success';
            $message[] = 'User has been successfully updated';
            return redirect()->route('users')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating user';
            return redirect()->route('users')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
