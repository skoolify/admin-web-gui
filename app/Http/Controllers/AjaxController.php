<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{CityApiHelper, ApiHelper};
use App\Http\Middleware\SetLanguage;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class AjaxController extends Controller
{
    private $apiUrl, $cityHelper, $api;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
        $this->cityHelper = new CityApiHelper();
        $this->api = new ApiHelper();
    }

    public function countries(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveCountries';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function cities(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveCities';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'countryID' => $request->query('countryID')
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }


    public function areas(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveAreas';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'cityID' => $request->query('cityID')
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function branches(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveBranches';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        $params = array(
            'schoolID' => $request->input('schoolID')
        );

        if($request->has('areaID'))
            $params['areaID'] = $request->input('areaID');

        try{

            $response = $client->request('POST', $url, [
                'form_params' => $params
            ]);


            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    // public function comms(Request $request)
    // {
    //     $error = false;
    //     $url = $this->apiUrl.'/getCommsCount';
    //     $code = 200;

    //     $client = new Client([
    //         'headers' => [
    //             'Authorization' => 'Bearer '.session()->get('apiToken')->response
    //         ]
    //     ]);

    //     $params = array(
    //         'schoolID' => $request->input('schoolID'),
    //         'branchID' => $request->input('branchID')
    //     );

    //     try{
    //         $response = $client->request('POST', $url, [
    //             'form_params' => $params
    //         ]);
    //         $responseBody = json_decode($response->getBody());
    //     }catch(BadResponseException $e){
    //         $error = true;
    //         if ($e->hasResponse()) {
    //             $response = $e->getResponse();
    //             $code = $response->getStatusCode();
    //             $responseJson = json_decode($response->getBody()->getContents());
    //             $responseBody = $responseJson;
    //         }
    //     }
    //     return $this->response($error, $responseBody, $code);
    // }

    public function subjectsInTimeTable(Request $request)
    {
        $error = false;
        $url = $this->apiUrl.'/getActiveSubjectsInTimetable';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        if($request->has('userID')){
            $param = [
                'subjectID' => $request->query('subjectID'),
                'branchID' => $request->query('branchID'),
                'userID' => $request->query('userID'),
                'attendanceDate' => $request->query('attendanceDate')
            ];
        }else{
            $param = [
                'subjectID' => $request->query('subjectID'),
                'branchID' => $request->query('branchID'),
                'attendanceDate' => $request->query('attendanceDate')
            ];
        }
        if($request->query('weekdayNo'))
            $param['weekdayNo'] = $request->query('weekdayNo');
        try{
            $response = $client->request('POST', $url, [
                'form_params' => $param
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function users(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getUserList';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => $request->query('schoolID'),
                    'branchID' => $request->query('branchID')
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }


    public function classes(Request $request)
    {

        $params = [];
        $error = false;
        $route = 'getActiveClasses';
        $code = 200;

        if($request->has('schoolID')){
            $params['schoolID'] = $request->input('schoolID');
        }

        if($request->has('branchID')){
            $params['branchID'] = $request->input('branchID');
        }

        if($request->has('classLevelID')){
            $params['classLevelID'] = $request->input('classLevelID');
        }

        if(!(session()->get('user')->isAdmin)){
            $params['userID'] = session()->get('user')->userID;
            $route = 'getUserClasses';
        }
        $url = $this->apiUrl.'/'.$route;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);


        try{
            $response = $client->request('POST', $url, [
                'form_params' => $params
            ]);
            $responseBody = json_decode($response->getBody());

        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function subjects(Request $request)
    {

        $error = false;
        $route = 'getActiveSubjects';
        $code = 200;
        $params = array();
        if($request->has('classLevelID')){
            $params['classLevelID'] = $request->input('classLevelID');
        }

        if($request->has('classID')){
            $params['classID'] = $request->input('classID');
        }


        if(!(session()->get('user')->isAdmin)){
            $params['userID'] = session()->get('user')->userID;
            $route = 'getUserSubjects';
        }

        $url = $this->apiUrl.'/'.$route;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => $params
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function examSubjects(Request $request)
    {
        $error = false;
        $route = 'getExamSubjects';
        $code = 200;
        $params = array();
        if($request->has('classLevelID')){
            $params['classLevelID'] = $request->input('classLevelID');
        }

        if($request->has('classID')){
            $params['classID'] = $request->input('classID');
        }


        if(!(session()->get('user')->isAdmin)){
            $params['userID'] = session()->get('user')->userID;
            $route = 'getUserExamSubjects';
        }

        $url = $this->apiUrl.'/'.$route;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => $params
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function nonSubjects(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveNonSubjects';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'classLevelID' => $request->query('classLevelID')
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function feeTypes(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveFeeTypes';
        $code = 200;
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => $request->query('schoolID'),
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }
    public function modules(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveModules';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => []
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }
        return $this->response($error, $responseBody, $code);
    }

    public function students(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getClassActiveStudents';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'classID' => $request->query('classID'),
                    'schoolID' => $request->query('schoolID') ?? null,
                    'branchID' => $request->query('branchID') ?? null,
                    'classLevelID' => $request->query('classLevelID') ?? null,
                    'classSection' => $request->query('classSection') ?? null
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function allStudents(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getAllClassStudents';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'classID' => $request->query('classID'),
                    'schoolID' => $request->query('schoolID') ?? null,
                    'branchID' => $request->query('branchID') ?? null,
                    'classLevelID' => $request->query('classLevelID') ?? null,
                    'classSection' => $request->query('classSection') ?? null
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function updateApiToken(Request $request){
        $error = false;
        $code = 200;
        $data['response'] = $this->api->token();
        session(['apiToken' => $data['response']]);
        return response()->json($data, $code);
    }

    public function schools(Request $request)
    {
        $code  = 200;
        $error = false;
        $url = $this->apiUrl.'/getActiveSchools';

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                ]
            ]);

            $responseBody = json_decode($response->getBody());
            $code = $response->getStatusCode();
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code );
    }

    public function quizResults(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getQuizResults';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'classID' => $request->query('classID')
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function studentFeeChallans(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getStudentFeeChallans';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'classID' => $request->query('classID')
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function examTerms(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getActiveExamTerm';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => $request->query('schoolID'),
                    'branchID' => $request->query('branchID'),
                    'classLevelID' => $request->query('classLevelID'),
                    'classID' => $request->query('classID')
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function classLevels(Request $request)
    {

        $route = 'getActiveClassLevels';
        $params = [
            'schoolID' => $request->query('schoolID'),
            'branchID' => $request->query('branchID')
        ];

        if(!(session()->get('user')->isAdmin)){
            $params = [];
            $params = [
                'userID' => session()->get('user')->userID,
                'branchID' => session()->get('user')->branchID
            ];
            $route = 'getUserClassLevels';
        }

        $error = false;
        $url = $this->apiUrl.'/'.$route;
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => $params
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function roles(Request $request)
    {

        $error = false;
        $url = $this->apiUrl.'/getRoles';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => $request->query('schoolID'),
                    // 'branchID' => $request->query('branchID') ?? NULL,
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }

    public function response($error, $data, $code = 200)
    {
        $response = array();
        if(gettype($data->response) === 'array' && !$error):
            $response['error'] = $error;
            $response['data'] = $data->response;
        else:
            $response['error'] = $error;
            $response['data'] = array();
        endif;
        return response()->json($response, $code);
    }

    public function branchGroup(Request $request)
    {

        $error = false;
        // $url = $this->apiUrl.'/getBranchGroupName';
        $url = $this->apiUrl.'/getBranchGroups';
        $code = 200;

        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        $params = array(
            'schoolID' => $request->input('schoolID')
        );

        // if($request->has('areaID'))
        //     $params['areaID'] = $request->input('areaID');

        try{

            $response = $client->request('POST', $url, [
                'form_params' => $params
            ]);

            $responseBody = json_decode($response->getBody());
            // print_r($responseBody);die;
        }catch(BadResponseException $e){
            $error = true;
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $this->response($error, $responseBody, $code);
    }
}
