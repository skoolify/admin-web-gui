<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewTimeTableController extends Controller
{
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['records'] = array();
        $data['students'] = array();
        $data['classLevels'] = array();
        return view('new-time-table.list', $data);
    }
}
