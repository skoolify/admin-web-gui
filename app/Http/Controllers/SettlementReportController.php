<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettlementReportController extends Controller
{
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();

        return view('settlement-report.list')->with($data);

    }
}
