<?php namespace App\Http\Controllers;

use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\SchoolPaymentHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SchoolPaymentController extends Controller
{
    private $paymentHelper, $api, $token;

    public function __construct()
    {
        $this->paymentHelper = new SchoolPaymentHelper();
        $this->api = new ApiHelper();
    }

    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['countries'] = array();
        $data['cities'] = array();
        $data['groups'] = array();

        return view('school-payment.list', $data);
    }

    public function submitPayment(Request $request){
        // $generated_transaction_uuid = $this->getGUIDnoHash();
        $currentDateTime = date("Y-m-d\TH:i:s\Z", strtotime(date('Y-m-d H:i:s')));
        $data = array();
        extract($request->all());
        $data['access_key'] = $access_key;
        $data['profile_id'] = $profile_id;
        $data['transaction_uuid'] = $transaction_uuid;
        $data['signed_field_names'] = $signed_field_names;
        $data['unsigned_field_names'] = $unsigned_field_names;
        $data['signed_date_time'] = $currentDateTime;
        $data['locale'] = $locale;
        $data['transaction_type'] = $transaction_type;
        $data['reference_number'] = $reference_number;
        $data['amount'] = $amount;
        $data['currency'] = $currency;
        $data['bill_to_address_city'] = $bill_to_address_city;
        $data['bill_to_address_country'] = $bill_to_address_country;
        $data['bill_to_address_line1'] = $bill_to_address_line1;
        $data['bill_to_address_postal_code'] = $bill_to_address_postal_code;
        $data['bill_to_address_state'] = $bill_to_address_state;
        $data['bill_to_forename'] = $bill_to_forename;
        $data['bill_to_phone'] = $bill_to_phone;
        $data['bill_to_surname'] = $bill_to_surname;
        $data['bill_to_email'] = $bill_to_email;
        $data['consumer_id'] = $consumer_id;
        $data['customer_ip_address'] = $customer_ip_address;
        $data['merchant_defined_data1'] = $merchant_defined_data1;
        $data['merchant_defined_data2'] = $merchant_defined_data2;
        $data['merchant_defined_data4'] = $merchant_defined_data4;
        $data['merchant_defined_data5'] = $merchant_defined_data5;
        $data['merchant_defined_data7'] = $merchant_defined_data7;
        $data['merchant_defined_data20'] = $merchant_defined_data20;
        $data['secret_key'] = $secret_key;
        $data['hbl_url'] = $hbl_url;

        if($data && count($data) > 0){
            $data['hash'] = $this->paymentHelper->generateHash($data);
            return view('school-payment.schools-hbl-payment', compact('data'));
        }else{
            return abort(301);
        }
    }

    public function getGUIDnoHash(){
        mt_srand((double)microtime()*10000);
        $charid = md5(uniqid(rand(), true));
        $c = unpack("C*",$charid);
        $c = implode("",$c);

        return substr($c,0,20);
    }
}
