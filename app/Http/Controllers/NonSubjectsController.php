<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ClassApiHelper, ApiHelper, SchoolApiHelper, SchoolBranchApiHelper, ClassLevelApiHelper, NonSubjectApiHelper};

class NonSubjectsController extends Controller
{
    private $api, $schoolHelper, $schools, $classHelper, $branches, $branchHelper, $classLevelHelper, $levels, $nonSubjectHelper, $classes, $subjects;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->schoolHelper = new SchoolApiHelper();
        $this->classLevelHelper = new ClassLevelApiHelper();
        $this->classHelper = new ClassApiHelper();
        $this->nonSubjectHelper = new NonSubjectApiHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classes'] = array();
        $data['classTeachers'] = array();
        $data['records'] = array();

        return view('non-subjects.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $this->schools = $this->schoolHelper->list();
        $this->levels = $this->classLevelHelper->list();

        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['classLevels'] = gettype($this->levels->response) === 'array' ? $this->levels->response : array();

        return view('non-subjects.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'classLevel' => 'required',
            'nonSubjectName' => 'required|min:2',
            'isActive' => 'required'
        ]);
        
        $nonSubject = $this->nonSubjectHelper->save($request);
        if($nonSubject->response === 'success'):
            $message[] = 'success';
            $message[] = 'Non-Subject has been successfully added';
            return redirect('non-subjects')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding Non-Subject';
            return redirect('non-subjects')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nonSubjectId, Request $request)
    {
        
        $this->schools = $this->schoolHelper->list();
        $this->levels = $this->classLevelHelper->list();

        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['classLevels'] = gettype($this->levels->response) === 'array' ? $this->levels->response : array();

        $record = $this->nonSubjectHelper->detail($nonSubjectId);
        $record = $this->api->extractDetail($record->response, $nonSubjectId, 'nonsubjectID');
        $data['record'] = $record;

        return view('non-subjects.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($nonSubjectId, Request $request)
    {

        $validateDate = $request->validate([
            'classLevel' => 'required',
            'nonSubjectName' => 'required|min:2',
            'isActive' => 'required'
        ]);

        $nonSubject = $this->nonSubjectHelper->update($nonSubjectId, $request);
        if($nonSubject->response === 'success'):
            $message[] = 'success';
            $message[] = 'Non-Subject has been successfully updated';
            return redirect('non-subjects')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating Non-Subject';
            return redirect('non-subjects')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
