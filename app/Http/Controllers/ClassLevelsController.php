<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ClassApiHelper, ApiHelper, SchoolApiHelper, SchoolBranchApiHelper, ClassLevelApiHelper};

class ClassLevelsController extends Controller
{
    private $api, $schoolHelper, $schools, $classHelper, $branches, $branchHelper, $classLevelHelper, $levels;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->schoolHelper = new SchoolApiHelper();
        $this->classHelper = new ClassApiHelper();
        $this->branchHelper = new SchoolBranchApiHelper();
        $this->classLevelHelper = new ClassLevelApihelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['records'] = array();

        return view('class-levels.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $this->schools = $this->schoolHelper->list();
        $this->branches = $this->branchHelper->list();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        return view('class-levels.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'classLevelName' => 'required',
            'assessmentOnly' => 'required',
            'school' => 'required'
        ]);
        
        $classLevel = $this->classLevelHelper->save($request);        
        if($classLevel->response === 'success'):
            $message[] = 'success';
            $message[] = 'Class has been successfully added';
            return redirect('class-levels')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding Class';
            return redirect('class-levels')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($levelId)
    {
        
        $this->schools = $this->schoolHelper->list();
        $record = $this->classLevelHelper->detail($levelId);
        if($record->response):
            $data['record'] = head($record->response);
        endif;
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();

        return view('class-levels.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $validateDate = $request->validate([
            'classLevelName' => 'required',
            'assessmentOnly' => 'required',
            'school' => 'required'
        ]);
        
        $classLevel = $this->classLevelHelper->update($id, $request);
        if($classLevel->response === 'success'):
            $message[] = 'success';
            $message[] = 'Class has been successfully updated';
            return redirect('class-levels')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating Class';
            return redirect('class-levels')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
