<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttendanceReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classes'] = array();
        return view('attendance-report.list', $data);
    }

    
}
