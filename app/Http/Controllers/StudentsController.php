<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ClassApiHelper, ApiHelper, SchoolApiHelper, SchoolBranchApiHelper, ClassLevelApiHelper, StudentApiHelper};

class StudentsController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['subjects'] = array();
        $data['classes'] = array();
        $data['records'] = array();

        return view('students.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $this->schools = $this->schoolHelper->list();
        $this->branches = $this->branchHelper->list();
        $this->levels = $this->classLevelHelper->list();
        $this->classes = $this->classHelper->list();

        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['branches'] = gettype($this->branches->response) === 'array' ? $this->branches->response : array();
        $data['classLevels'] = gettype($this->levels->response) === 'array' ? $this->levels->response : array();
        $data['classes'] = gettype($this->classes->response) === 'array' ? $this->classes->response : array();

        return view('students.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'classLevel' => 'required',
            'subjectName' => 'required|min:2',
            'isActive' => 'required'
        ]);
        
        $subject = $this->studentHelper->save($request);
        if($subject->response === 'success'):
            $message[] = 'success';
            $message[] = 'Subject has been successfully added';
            return redirect('students')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding Subject';
            return redirect('students')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($subjectId, Request $request)
    {
        
        $this->schools = $this->schoolHelper->list();
        $this->branches = $this->branchHelper->list();
        $this->levels = $this->classLevelHelper->list();
        $this->classes = $this->classHelper->list();

        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['branches'] = gettype($this->branches->response) === 'array' ? $this->branches->response : array();
        $data['classLevels'] = gettype($this->levels->response) === 'array' ? $this->levels->response : array();
        $data['classes'] = gettype($this->classes->response) === 'array' ? $this->classes->response : array();

        $record = $this->studentHelper->detail($subjectId);
        $record = $this->api->extractDetail($record->response, $subjectId, 'subjectID');
        $data['record'] = $record;

        return view('students.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($subjectId, Request $request)
    {

        $validateDate = $request->validate([
            'classLevel' => 'required',
            'subjectName' => 'required|min:2',
            'isActive' => 'required'
        ]);

        $subject = $this->studentHelper->update($subjectId, $request);
        if($subject->response === 'success'):
            $message[] = 'success';
            $message[] = 'Subject has been successfully updated';
            return redirect('students')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating Subject';
            return redirect('students')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pictureUpload(Request $request)
    {
        $validateDate = $request->validate([
            'logoImg' => 'required|image'
        ]);
       
        $image = \Image::make($request->file('logoImg'));
        $width = $image->width() * env('STUDENT_WIDTH_RESIZE');
        $height = $image->height() * env('STUDENT_HEIGHT_RESIZE');
        $code = $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('data-url', env('STUDENT_COMPRESSION_RATE'));
        
        return response()->json([
            'picture' => (string) $code,
        ]);
    }
}
