<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromotionController extends Controller
{
    public function index(Request $request){
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['classLevels'] = array();
        $data['records'] = array();

        return view('promotion.list', $data);
    }
}
