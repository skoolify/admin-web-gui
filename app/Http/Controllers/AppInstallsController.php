<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppInstallsController extends Controller
{
    public function __construct()
    {
    }
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        return view('app-installs.list', $data);
    }
}
