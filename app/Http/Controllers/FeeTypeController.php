<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{FeeTypeApiHelper, ApiHelper};

class FeeTypeController extends Controller
{
    
    private $feeTypeHelper, $api;
    
    public function __construct()
    {
        $this->feeTypeHelper = new FeeTypeApiHelper();
        $this->api = new ApiHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['records'] = array();

        return view('fee-type.list')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $schoolId = $request->input('schoolId');

        if($schoolId):
            $data['schoolId'] = $schoolId;
        else:
            $message[] = 'danger';
            $message[] = 'No School Specified';
            return redirect()->back()->with('flash-message', $message);
        endif;

        return view('fee-type.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'feeType' => 'required|min:3',
            'isActive' => 'required'
        ]);
        
        $schoolId = $request->input('schoolId');
        if($schoolId):
            $token = $this->api->token();
            if($token->response):
                $feeType = $this->feeTypeHelper->save($token->response, $schoolId, $request);
                if($feeType->response === 'success'):
                    $message[] = 'success';
                    $message[] = 'Fee Type has been successfully added';
                    return redirect('fee-type?schoolId='.$schoolId)->with('flash-message', $message);
                endif;
            else:
                echo 'Error in Response';
            endif;
        else:
            $message[] = 'danger';
            $message[] = 'No School Specified';
            return redirect()->back()->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $feeTypeId)
    {

        $data['feeType'] = $request->input('feeType');
        $data['schoolId'] = $request->input('schoolId');
        $data['feeTypeID'] = $feeTypeId;
        $data['isActive'] = $request->input('isActive');

        return view('fee-type.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $feeTypeId)
    {

        $validateDate = $request->validate([
            'feeType' => 'required|min:3',
            'isActive' => 'required'
        ]);

        $token = $this->api->token();
        if($token->response):
            $feeType = $this->feeTypeHelper->update($token->response, $feeTypeId, $request);
            if($feeType->response === 'success'):
                $message[] = 'success';
                $message[] = 'Fee Type has been successfully updated';
                return redirect()->route('fee-type', ['schoolId' => $request->input('schoolId')])->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
