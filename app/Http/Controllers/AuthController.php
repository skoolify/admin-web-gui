<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};
use App\Http\Helpers\{ApiHelper, AuthApiHelper};

class AuthController extends Controller
{

    private $authHelper, $schoolLogoHelper, $api, $token;

    public function __construct()
    {
        $this->authHelper = new AuthApiHelper();
        $this->api = new ApiHelper();
    }

    public function index(Request $request)
    {
        $token = $this->api->token();
        session()->forget(array('user', 'apiToken', 'tempSchoolID', 'tempBranchID', 'tempClassLevelID', 'tempClassID', 'permissions'));

        $authData = array(
            'authenticated' => false,
            'userId' => NULL,
            'apiToken' => $token
        );
        $url = explode('.', \Request::getHost());
        $host = 'admin';
        if(count($url) >= 3){
            $host = head($url);
        }
        session($authData);
        $data['domain'] = $host;
        $this->token = session()->get('apiToken');
        session(['locale' => $request->query('lang') ?? 'en']);
        \App::setLocale(session()->get('locale'));
        return view('auth.login', $data);
    }

    public function password()
    {
        return view('auth.change-password');
    }

    public function login(Request $request)
    {
        $data = array();
        $data['error'] = true;
        $token = session()->get('apiToken')->response;
        if($token):
            $user = $this->authHelper->login($token, $request);
            if(!($user->response->error) && ($response = $user->response)):
                $response->id = $response->userID;
                $authData = array(
                    'user' => $response
                );
                session($authData);
                $data['error'] = false;
                $data['response'] = $response;
            else:
                $user = $user->response;
                $data['error'] = $user->error;
                $data['code'] = $user->code;
                $data['message'] = $user->userMessage;
            endif;
        else:
            $data['error'] = true;
            $data['code'] = $user->code;
            $data['message'] = 'Sorry there is something wrong';
        endif;

        return response()->json($data);
    }

    public function validateUser(Request $request)
    {
        $data = array();
        $token = session()->get('apiToken')->response;
        if($token):
            $user = $this->authHelper->validateUser($token, $request);
            if(!($user->response->error) && ($response = $user->response)):
                $authData = array(
                    'authenticated' => false,
                    'user' => $response
                );
                $lastLoginDate = $response->lastLoginDate;
                \Session::put('lastLoginDate', $lastLoginDate);
                session($authData);
                $data['error'] = $response->error;
                $data['response'] = $response;
            else:
                $user = $user->response;
                $data['error'] = $user->error;
                $data['code'] = $user->code;
                $data['message'] = $user->userMessage;
            endif;
        else:
            $data['error'] = true;
            $data['code'] = $user->code;
            $data['message'] = 'Sorry there is something wrong';
        endif;

        return response()->json($data);
    }

    public function userDetail()
    {
        $data = array();
        $data['error'] = true;
        $token = session()->get('apiToken')->response;
        if($token):
            $user = $this->authHelper->userDetail($token);
            $response = $user->response;
            if(isset($user->meetingDetails)){
                $meetingDetails = $user->meetingDetails;
            }
            if(!(isset($user->userMessage))){
                if(gettype($user->response) === 'array'){
                    if(!($user->error ) && ($response)):
                        $data['error'] = false;
                        $data['response'] = $response;
                        if(isset($meetingDetails) && $meetingDetails !== null){
                            $data['meetingDetails'] = $meetingDetails;
                        }
                        $authData = array(
                            'authenticated' => false,
                            'user' => $response,
                        );
                        session($authData);
                    else:
                        $data['error'] = true;
                        $data['message'] = $user->response->userMessage;

                    endif;
                }else{
                    $data['error'] = true;
                    $data['message'] = $user->response->userMessage;
                }
            }else{
                $data['error'] = true;
                $data['message'] = $user->response->userMessage;
            }

        else:
            $data['error'] = true;
        endif;
        return response()->json($data);
    }

    public function authorization()
    {
        $allModules = $this->authHelper->modules();
        if(($allModules = $allModules->response)){
            session(['modules' => $allModules]);
            return view('auth.authorization');
        }else{
            return redirect()->route('403');
        }
    }

    public function contractAcceptance(Request $request)
    {
        $data = array();
        $data['error'] = true;
        $token = session()->get('apiToken')->response;
        if($token){
            $contract = $this->authHelper->getSchoolContract($token, $request);
            if(isset($contract->response->error)){
                $contract = $contract->response;
                $data['error'] = $contract->error;
                $data['code'] = $contract->code;
                $data['message'] = $contract->userMessage;
            }else{
                if($contract && !($contract->error) && ($response = $contract->response[0])){
                    $contractData = array();
                    $contractTextVariableReplacement = array();
                    $responseArray = (array) $response;
                    foreach($responseArray as $key => $value){
                        if($key == 'contractText'){
                            $contractData[$key] = base64_decode($value);
                        }else if($key == 'chargesText'){
                            $contractData[$key] = base64_decode($value);
                        }else if($key == 'collectionText'){
                            $contractData[$key] = base64_decode($value);
                        }else{
                            $contractData[$key] = $value;
                            $contractTextVariableReplacement[$key] = $value;
                        }
                    }
                    foreach($contractTextVariableReplacement as $varKey => $varValue){
                        if(isset($varValue) && $varValue !== '' && $varValue != 'null'){
                            $contractData['contractText'] = str_replace($varKey,$varValue, $contractData['contractText']);
                            $contractData['chargesText'] = str_replace($varKey,$varValue, $contractData['chargesText']);
                            $contractData['collectionText'] = str_replace($varKey,$varValue, $contractData['collectionText']);
                        }
                    }
                    $data['error'] = false;
                    $data['response'] = $response;
                    $authData = array(
                        'authenticated' => false,
                        'contractData' => $contractData,
                    );
                    session($authData);
                }else{
                    $contract = $contract->response;
                    $data['error'] = $contract->error;
                    $data['code'] = $contract->code;
                    $data['message'] = $contract->userMessage;
                }
            }
        }else{
            $data['error'] = true;
            $data['code'] = $contract->code;
            $data['message'] = 'Sorry there is something wrong';
        }
        return response()->json($data);
    }

    public function contractAcceptanceView(){
        return view('auth.contract-acceptance');
    }

    public function switchRole($index){
        $token = session()->get('apiToken')->response;
        $roles = session()->get('roles');
        $user = session()->get('user');
        $user = $roles[$index];
        session(['user' => $user]);

        if($token):
            $permissions = array();
            $modules = $this->authHelper->userModules($token);
            $allModules = session()->get('modules');
            $subChildrens = \Config::get("constants.subChildren");
            $requiredModules = array();
            if(!($modules->response->error) && ($response = $modules->response)):
                if(isset($modules->response->moduleDetails)){
                    foreach ($allModules as $key => $module) {
                        if($module->isParent == 1){
                            $permissions[$module->moduleURL] = (object) array(
                                'moduleName' => $module->moduleName,
                                'moduleName_ar' => $module->moduleName_ar,
                                'moduleURL' => $module->moduleURL,
                                'read' => true,
                                'add' => true,
                                'edit' => true,
                                'delete' => true,
                                'parent' => $module->moduleURL,
                                'isParent' => true,
                                'childrens' => array(),
                                'required' => true
                            );
                        }
                    }

                    foreach ($allModules as $key => $module) {
                        if($module->isParent != 1 ){
                            $permissions[$module->parentURL]->childrens[$module->moduleURL] = (object) array(
                                'moduleName' => $module->moduleName,
                                'moduleName_ar' => $module->moduleName_ar,
                                'moduleURL' => $module->moduleURL,
                                'read' => true,
                                'add' => true,
                                'edit' => true,
                                'delete' => true,
                                'parent' => $module->parentURL,
                                'isParent' => false,
                                'required' => false
                            );
                        }
                    }
                    foreach ($modules->response->moduleDetails as $key => $module) {
                        if($module->isParent !== 1){
                            if(isset($permissions[$module->parentModuleURL]) && isset($permissions[$module->parentModuleURL]->childrens[$module->moduleURL])){
                                $userModule = $permissions[$module->parentModuleURL]->childrens[$module->moduleURL];

                                $userModule->read = $module->readAccess == 1 ? true : false;
                                $userModule->add = $module->addAccess == 1 ? true : false;
                                $userModule->edit = $module->editAccess == 1 ? true : false;
                                $userModule->delete = $module->deleteAccess == 1 ? true : false;

                                $userModule->required = true;
                            }
                        }
                    }
                    foreach ($permissions as $key => $permission) {
                        if(isset($permission->childrens)){
                            $childrens = $permission->childrens;
                            foreach ($childrens as $k => $child) {
                                if(!($child->required) && !($child->moduleURL == 'exam-term-results')){
                                    unset($permissions[$key]->childrens[$k]);
                                }
                            }
                        }
                    }
                    foreach ($permissions as $key => $permission){
                        if(isset($permission->childrens)){
                            $childrens = $permission->childrens;
                            if(count($childrens) <= 0){
                                unset($permissions[$key]);
                            }
                        }
                    }
                    $redirectedUrl = session()->has('last-visited-url') ?  session()->get('last-visited-url') : '';
                    $redirectedUrlDetail = '';
                    if(isset($modules->response->moduleDetails)){
                        if(isset($allModules) && $allModules){
                            foreach ($allModules as $key => $module) {
                                if($module->moduleURL === $redirectedUrl ){
                                    $redirectedUrlDetail = $module;
                                }
                            }
                        }
                    }
                    session(['permissions' => $permissions]);
                    $module = key($permissions); //modules
                    $module = head($permissions[$module]->childrens);

                    foreach ($permissions as $key => $permission) {
                        foreach ($permission->childrens as $key => $perm) {
                            $requiredModules[] = $perm->moduleURL;
                        }
                    }
                    if(isset($redirectedUrlDetail) && $redirectedUrlDetail ){
                        $data['module'] = \Authorization::check($redirectedUrlDetail->moduleURL, 'read', $redirectedUrlDetail->parentURL, true) ? $redirectedUrl : $module->moduleURL ;
                        $data['requiredModules'] = (array) $requiredModules ?? array();
                    }else{
                        $data['requiredModules'] = (array) $requiredModules ?? array();
                        $data['module'] = $module->moduleURL;
                    }
                    $data['error'] = true;
                    $data['error'] = $response->error;
                }else{
                    $data['error'] = true;
                    $data['code'] = 404;
                    $data['message'] = "You do not have access to any module";
                }
            else:
                $data['error'] = true;
                $data['code'] = 404;
                $data['message'] = "You do not have access to any module";
            endif;
        else:
            $data['error'] = true;
            $data['code'] = 401;
            $data['message'] = 'Sorry there is something wrong';
        endif;
        return redirect()->back()->with('switch-role', session()->get('user'));
    }

    public function userModules(Request $request)
    {
        $data = array();
        $token = session()->get('apiToken')->response;
        $index = (string) $request->input('index');
        if($index == ''){
            $index = 0;
        }
        $parents = array();
        $roles = session()->get('user');
        session(['roles' => $roles]);
        if($token):
            $permissions = array();
            $user = $roles;
            session()->put('user', $user[$index]);
            $modules = $this->authHelper->userModules($token);
            $allModules = session()->get('modules');
            $subChildrens = \Config::get("constants.subChildren");
            $requiredModules = array();
            if(!($modules->response->error) && ($response = $modules->response)):
                if(isset($modules->response->moduleDetails)){
                    foreach ($allModules as $key => $module) {
                        if($module->isParent == 1){
                            $permissions[$module->moduleURL] = (object) array(
                                'moduleName' => $module->moduleName,
                                'moduleName_ar' => $module->moduleName_ar,
                                'moduleURL' => $module->moduleURL,
                                'read' => true,
                                'add' => true,
                                'edit' => true,
                                'delete' => true,
                                'parent' => $module->moduleURL,
                                'isParent' => true,
                                'childrens' => array(),
                                'required' => true
                            );
                        }
                    }

                    foreach ($allModules as $key => $module) {
                        if($module->isParent != 1 ){
                            $permissions[$module->parentURL]->childrens[$module->moduleURL] = (object) array(
                                'moduleName' => $module->moduleName,
                                'moduleName_ar' => $module->moduleName_ar,
                                'moduleURL' => $module->moduleURL,
                                'read' => true,
                                'add' => true,
                                'edit' => true,
                                'delete' => true,
                                'parent' => $module->parentURL,
                                'isParent' => false,
                                'required' => false
                            );
                        }
                    }
                    foreach ($modules->response->moduleDetails as $key => $module) {
                        if($module->isParent !== 1){
                            if(isset($permissions[$module->parentModuleURL]) && isset($permissions[$module->parentModuleURL]->childrens[$module->moduleURL])){
                                $userModule = $permissions[$module->parentModuleURL]->childrens[$module->moduleURL];

                                $userModule->read = $module->readAccess == 1 ? true : false;
                                $userModule->add = $module->addAccess == 1 ? true : false;
                                $userModule->edit = $module->editAccess == 1 ? true : false;
                                $userModule->delete = $module->deleteAccess == 1 ? true : false;

                                $userModule->required = true;
                            }
                        }
                    }
                    foreach ($permissions as $key => $permission) {
                        if(isset($permission->childrens)){
                            $childrens = $permission->childrens;
                            foreach ($childrens as $k => $child) {
                                if(!($child->required) && !($child->moduleURL == 'exam-term-results')){
                                    unset($permissions[$key]->childrens[$k]);
                                }
                            }
                        }
                    }
                    foreach ($permissions as $key => $permission){
                        if(isset($permission->childrens)){
                            $childrens = $permission->childrens;
                            if(count($childrens) <= 0){
                                unset($permissions[$key]);
                            }
                        }
                    }
                    $redirectedUrl = session()->has('last-visited-url') ?  session()->get('last-visited-url') : '';
                    $redirectedUrlDetail = '';
                    if(isset($modules->response->moduleDetails)){
                        if(isset($allModules) && $allModules){
                            foreach ($allModules as $key => $module) {
                                if($module->moduleURL === $redirectedUrl ){
                                    $redirectedUrlDetail = $module;
                                }
                            }
                        }
                    }
                    session(['permissions' => $permissions]);
                    $module = key($permissions); //modules
                    $module = head($permissions[$module]->childrens);

                    foreach ($permissions as $key => $permission) {
                        foreach ($permission->childrens as $key => $perm) {
                            $requiredModules[] = $perm->moduleURL;
                        }
                    }
                    if(isset($redirectedUrlDetail) && $redirectedUrlDetail ){
                        $data['module'] = \Authorization::check($redirectedUrlDetail->moduleURL, 'read', $redirectedUrlDetail->parentURL, true) ? $redirectedUrl : $module->moduleURL ;
                        $data['requiredModules'] = (array) $requiredModules ?? array();
                    }else{
                        $data['requiredModules'] = (array) $requiredModules ?? array();
                        $data['module'] = $module->moduleURL;
                    }
                    $data['error'] = true;
                    $data['error'] = $response->error;
                }else{
                    $data['error'] = true;
                    $data['code'] = 404;
                    $data['message'] = "You do not have access to any module";
                }
            else:
                $data['error'] = true;
                $data['code'] = 404;
                $data['message'] = "You do not have access to any module";
            endif;
        else:
            $data['error'] = true;
            $data['code'] = 401;
            $data['message'] = 'Sorry there is something wrong';
        endif;
        return response()->json($data);
    }


    public function default(Request $request){

        if(session()->has('home-page')){
            return redirect()->route(session()->get('home-page'));
        }else{
            return redirect()->route('auth-login');
        }
    }

    public function switchLanguage(Request $request){
        extract($request->all());
        session(['locale' => $lang ?? 'en']);
        // if(isset($_COOKIE['lang']) && $_COOKIE['lang']){
        //     unset($_COOKIE['lang']);
        // }
        setcookie('lang', $lang);
        $_COOKIE['lang'] = $lang;

        if($lang && in_array($lang, array('en', 'ar'))){
            \App::setLocale(session()->get('locale'));
            \App::setLocale($_COOKIE['lang']);
        }
        // $previousUrl = app('url')->previous();
        // $previousUrl = strtok($previousUrl, '?');
        // return redirect()->to($previousUrl.'?'. http_build_query(['lang'=>session()->get('locale')]));
        return redirect()->back();
    }
}


