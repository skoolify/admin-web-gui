<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NonAcademicController extends Controller
{
    public function index(Request $request){
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['classLevels'] = array();
        $data['records'] = array();
        $data['students'] = array();
        $data['nonSubjects'] = array();

        return view('nonacademic.list', $data);
    }
}
