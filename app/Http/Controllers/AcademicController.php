<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AcademicController extends Controller
{
    public function index(Request $request){
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['classLevels'] = array();
        $data['records'] = array();
        $data['students'] = array();

        return view('academic.list', $data);
    }

    public function test(){
        return view('errors.5032');
    }
}
