<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{CalendarDayHelper, SchoolApiHelper, ApiHelper};

class CalendarDaysController extends Controller
{
    private $schoolHelper, $api, $schools, $calendarDays, $calendarDayHelper;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->schoolHelper = new SchoolApiHelper();
        $this->calendarDayHelper = new CalendarDayHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $data['schools'] = array();
        $data['branches'] = array();
        $data['records'] = array();

        return view('calendar-days.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $this->schools = $this->schoolHelper->list();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        return view('calendar-days.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'calendarDate' => 'required',
            'holidayName' => 'required',
            'school' => 'required',
            'isActive' => 'required'
        ]);
        
        $calendarDay = $this->calendarDayHelper->save($request);
        if($calendarDay->response === 'success'):
            $message[] = 'success';
            $message[] = 'Calendar Day has been successfully added';
            return redirect('calendar-days')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding role';
            return redirect('calendar-days')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($calendarDayId, Request $request)
    {
        
        $token = session()->get('apiToken');
        $this->schools = $this->schoolHelper->list();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $record = $this->calendarDayHelper->detail($calendarDayId);
        $record = $this->api->extractDetail($record->response, $calendarDayId, 'calendarID');
        $data['record'] = $record;

        return view('calendar-days.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $validateDate = $request->validate([
            'calendarDate' => 'required',
            'holidayName' => 'required',
            'school' => 'required',
            'isActive' => 'required'
        ]);

        $calendarDay = $this->calendarDayHelper->update($id, $request);
        if($calendarDay->response === 'success'):
            $message[] = 'success';
            $message[] = 'Calendar Day has been successfully updated';
            return redirect()->route('calendar-days')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating Calendar Day';
            return redirect()->route('calendar-days')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
