<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ApiHelper, ModuleApiHelper};

class ModulesController extends Controller
{
    private $api, $moduleHelper, $modules;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->moduleHelper = new ModuleApiHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$this->api->setTempParams($request);
        $this->modules = $this->moduleHelper->list();
        $data['records'] = gettype($this->modules->response) === 'array' ? $this->modules->response : array();
        return view('modules.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $this->modules = $this->moduleHelper->list();
        $data['modules'] = gettype($this->modules->response) === 'array' ? $this->modules->response : array();

        return view('modules.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'moduleName' => 'required',
            'moduleDescription' => 'required|min:5',
            'moduleUrl' => 'required',
            'parent' => 'required',
            'isActive' => 'required'
        ]);
        
        $module = $this->moduleHelper->save($request);
        if($module->response === 'success'):
            $message[] = 'success';
            $message[] = 'Module has been successfully added';
            return redirect('modules')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding Module';
            return redirect('modules')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($moduleId, Request $request)
    {

        $this->modules = $this->moduleHelper->list();
        $data['modules'] = gettype($this->modules->response) === 'array' ? $this->modules->response : array();
        $record = $this->moduleHelper->detail($moduleId);
        $record = $this->api->extractDetail($record->response, $moduleId, 'moduleID');
        $data['record'] = $record;

        return view('modules.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($moduleId, Request $request)
    {

        $validateDate = $request->validate([
            'moduleName' => 'required',
            'moduleDescription' => 'required|min:5',
            'moduleUrl' => 'required',
            'parent' => 'required',
            'isActive' => 'required'
        ]);

        $module = $this->moduleHelper->update($moduleId, $request);
        if($module->response === 'success'):
            $message[] = 'success';
            $message[] = 'Module has been successfully updated';
            return redirect('modules')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating Module';
            return redirect('modules')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
