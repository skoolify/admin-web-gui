<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContractController extends Controller
{
    public function index(Request $request){
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['students'] = array();

        return view('contract.list', $data);
    }
}

