<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ApiHelper, SchoolApiHelper};

class SchoolsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['branches'] = array();
        return view('schools.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schools.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validateDate = $request->validate([
            'schoolName' => 'required|min:3',
            'schoolDomain' => 'required|min:3',
            'schoolOwnerName' => 'required|min:3',
            'schoolOnBoardingDate' => 'required|date',
            'schoolWebsiteURL' => 'required|url',
            'logo' => 'image|mimes:jpeg,bmp,png,jpg',
            'isActive' => 'required'
        ]);

        $token = session()->get('apiToken');

        if($token->response):
            $school = $this->schoolHelper->save($token->response, $request);
            if($school->response === 'success'):
                $message[] = 'success';
                $message[] = 'School has been successfully added';
                return redirect()->route('schools')->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
    }

    public function show($id)
    {
        $token = session()->get('apiToken');
        $school = $this->schoolHelper->detail($token->response, $id);

        if($school->response):
            $data['school'] = head($school->response);
        endif;

        $data['schoolId'] = $id;
    }

    public function edit($id)
    {
        $token = session()->get('apiToken');
        $school = $this->schoolHelper->detail($token->response, $id);

        if($school->response):
            $data['school'] = head($school->response);
        endif;

        $data['schoolId'] = $id;

        return view('schools.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        $validateDate = $request->validate([
            'schoolName' => 'required|min:3',
            'schoolDomain' => 'required|min:3',
            'schoolOwnerName' => 'required|min:3',
            'schoolOnBoardingDate' => 'required|date',
            'schoolWebsiteURL' => 'required|url',
            'isActive' => 'required'
        ]);

        $token = session()->get('apiToken');

        if($token->response):
            $school = $this->schoolHelper->update($token->response, $id, $request);
            if($school->response === 'success'):
                $message[] = 'success';
                $message[] = 'School has been successfully updated';
                return redirect()->route('schools')->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
    }

    public function destroy($id)
    {
        //
    }

    public function compressImageAndConvertToBase64(Request $request){
        $validateDate = $request->validate([
            'logoImage' => 'required|mimes:jpg,jpeg,png,gif,size:2049'
        ]);
        $image = $request->file('logoImage');

        $attachmentName = rand() . '_logoImage.' . $image->getClientOriginalExtension();

        $image = \Image::make($image);
        $width = $image->width() * env('SCHOOL_LOGO_WIDTH_RESIZE');
        $height = $image->height() * env('SCHOOL_LOGO_HEIGHT_RESIZE');
        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })->save(env('ATTACHMENT_UPLOAD_PATH').'/'.$attachmentName, env('DAILY_DIARY_COMPRESSION_RATE'));
        $logoImage = file_get_contents(env('ATTACHMENT_UPLOAD_PATH').'/'.$attachmentName);
        $logoImageBase64 = base64_encode($logoImage);
        if (file_exists(env('ATTACHMENT_UPLOAD_PATH').'/'.$attachmentName)) {
            unlink (env('ATTACHMENT_UPLOAD_PATH').'/'.$attachmentName);
        }
        return response()->json([
            'attachment' => $logoImageBase64,
        ]);
    }

    public function compressBase64Image(Request $request){
        $validateDate = $request->validate([
            'logoImage' => 'required'
        ]);
        $image = $request['logoImage'];
        $width = 200 * env('SCHOOL_LOGO_WIDTH_RESIZE');
        $height = 200 * env('SCHOOL_LOGO_HEIGHT_RESIZE');
        if ($image) {
            $name = time().'.'.explode('/',explode(':',substr($image,0,strpos($image,';')))[1])[1];
            $image = \Image::make($image);
            $image->resize($width, $height, function($constraint){
                $constraint->aspectRatio();
            })->save(env('ATTACHMENT_UPLOAD_PATH').'/'.$name, env('DAILY_DIARY_COMPRESSION_RATE'));
            $logoImage = file_get_contents(env('ATTACHMENT_UPLOAD_PATH').'/'.$name);
            $logoImageBase64 = base64_encode($logoImage);
            if (file_exists(env('ATTACHMENT_UPLOAD_PATH').'/'.$name)) {
                unlink (env('ATTACHMENT_UPLOAD_PATH').'/'.$name);
            }
            return response()->json([
                'attachment' => $logoImageBase64,
            ]);
        }
    }
}
