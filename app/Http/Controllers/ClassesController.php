<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ClassApiHelper, ApiHelper, SchoolApiHelper, SchoolBranchApiHelper, ClassLevelApiHelper, UserApiHelper};

class ClassesController extends Controller
{
    private $api, $schoolHelper, $schools, $classHelper, $branches, $branchHelper, $classLevelHelper, $levels, $teachers;
    private $userHelper;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->schoolHelper = new SchoolApiHelper();
        $this->classHelper = new ClassApiHelper();
        $this->branchHelper = new SchoolBranchApiHelper();
        $this->classLevelHelper = new ClassLevelApiHelper();
        $this->userHelper = new UserApiHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->teachers = $this->userHelper->list();
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classTeachers'] = gettype($this->teachers->response) === 'array' ? $this->teachers->response : array();
        $data['records'] = array();

        return view('classes.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $this->teachers = $this->userHelper->list();
        $data['teachers'] = gettype($this->teachers->response) === 'array' ? $this->teachers->response : array();        
        return view('classes.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'classSection' => 'required',
            'classShift' => 'required',
            'branch' => 'required',
            'classLevel' => 'required',
            'classTeacher' => 'required',
            'isActive' => 'required'
        ]);
        
        $class = $this->classHelper->save($request);
        if($class->response === 'success'):
            $message[] = 'success';
            $message[] = 'Section has been successfully added';
            return redirect('classes')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding Section';
            return redirect('classes')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($classId, Request $request)
    {
        
        $this->schools = $this->schoolHelper->list();
        $this->branches = $this->branchHelper->list();
        $this->levels = $this->classLevelHelper->list();
        $this->teachers = $this->userHelper->list();
        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['branches'] = gettype($this->branches->response) === 'array' ? $this->branches->response : array();
        $data['classLevels'] = gettype($this->levels->response) === 'array' ? $this->levels->response : array();
        $data['teachers'] = gettype($this->teachers->response) === 'array' ? $this->teachers->response : array();

        $record = $this->classHelper->detail($classId);
        $record = $this->api->extractDetail($record->response, $classId);
        $data['record'] = $record;

        return view('classes.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($classId, Request $request)
    {

        $validateDate = $request->validate([
            'classSection' => 'required',
            'classShift' => 'required',
            'branch' => 'required',
            'classLevel' => 'required',
            'classTeacher' => 'required',
            'isActive' => 'required'
        ]);

        $class = $this->classHelper->update($classId, $request);
        if($class->response === 'success'):
            $message[] = 'success';
            $message[] = 'Section has been successfully updated';
            return redirect('classes')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating Section';
            return redirect('classes')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
