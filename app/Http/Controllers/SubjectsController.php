<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ClassApiHelper, ApiHelper, SchoolApiHelper, SchoolBranchApiHelper, ClassLevelApiHelper, SubjectApiHelper};

class SubjectsController extends Controller
{
    private $api, $schoolHelper, $schools, $classHelper, $branches, $branchHelper, $classLevelHelper, $levels, $subjectHelper, $classes, $subjects;
    
    public function __construct()
    {
        $this->api = new ApiHelper();
        $this->schoolHelper = new SchoolApiHelper();
        $this->classHelper = new ClassApiHelper();
        $this->branchHelper = new SchoolBranchApiHelper();
        $this->classLevelHelper = new ClassLevelApiHelper();
        $this->subjectHelper = new SubjectApiHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['schools'] = array();
        $data['branches'] = array();
        $data['classLevels'] = array();
        $data['classes'] = array();
        $data['classTeachers'] = array();
        $data['records'] = array();

        return view('subjects.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   

        $this->schools = $this->schoolHelper->list();
        $this->branches = $this->branchHelper->list();
        $this->levels = $this->classLevelHelper->list();
        $this->classes = $this->classHelper->list();

        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['branches'] = gettype($this->branches->response) === 'array' ? $this->branches->response : array();
        $data['classLevels'] = gettype($this->levels->response) === 'array' ? $this->levels->response : array();
        $data['classes'] = gettype($this->classes->response) === 'array' ? $this->classes->response : array();

        return view('subjects.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'classLevel' => 'required',
            'subjectName' => 'required|min:2',
            'isActive' => 'required'
        ]);
        
        $subject = $this->subjectHelper->save($request);
        if($subject->response === 'success'):
            $message[] = 'success';
            $message[] = 'Subject has been successfully added';
            return redirect('subjects')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in adding Subject';
            return redirect('subjects')->with('flash-message', $message);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($subjectId, Request $request)
    {
        
        $this->schools = $this->schoolHelper->list();
        $this->branches = $this->branchHelper->list();
        $this->levels = $this->classLevelHelper->list();
        $this->classes = $this->classHelper->list();

        $data['schools'] = gettype($this->schools->response) === 'array' ? $this->schools->response : array();
        $data['branches'] = gettype($this->branches->response) === 'array' ? $this->branches->response : array();
        $data['classLevels'] = gettype($this->levels->response) === 'array' ? $this->levels->response : array();
        $data['classes'] = gettype($this->classes->response) === 'array' ? $this->classes->response : array();

        $record = $this->subjectHelper->detail($subjectId);
        $record = $this->api->extractDetail($record->response, $subjectId, 'subjectID');
        $data['record'] = $record;

        return view('subjects.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($subjectId, Request $request)
    {

        $validateDate = $request->validate([
            'classLevel' => 'required',
            'subjectName' => 'required|min:2',
            'isActive' => 'required'
        ]);

        $subject = $this->subjectHelper->update($subjectId, $request);
        if($subject->response === 'success'):
            $message[] = 'success';
            $message[] = 'Subject has been successfully updated';
            return redirect('subjects')->with('flash-message', $message);
        else:
            $message[] = 'danger';
            $message[] = 'Error in updating Subject';
            return redirect('subjects')->with('flash-message', $message);
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
