<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{CityApiHelper, CountryApiHelper, ApiHelper};

class CitiesController extends Controller
{

    public function index(Request $request)
    {
        $api = new ApiHelper();
        $token = $api->token();

        $cityHelper = new CityApiHelper();
        $countryHelper = new CountryApiHelper();

        $data['countryId'] = $request->input('countryId') ? $request->input('countryId') : NULL;

        if($token->response):
            // $cities = $cityHelper->list($token->response, $request->input('countryId'));
            $countries = $countryHelper->list($token->response);
            // $data['cities'] = gettype($cities->response) === 'array' ? $cities->response : array();
            $data['cities'] = array();
            $data['countries'] = $countries->response;
        else:
            echo 'Error in Response';
        endif;
       
        return view('cities.list')->with($data);
    }

    public function create()
    {

        $token = session()->get('apiToken');
        $countryHelper = new CountryApiHelper();
        $countries = $countryHelper->list($token->response);
        $data['countries'] = $countries->response;

        return view('cities.add')->with($data);
    }

    public function store(Request $request)
    {

        $validateDate = $request->validate([
            'country' => 'required',
            'cityName' => 'required|min:3',
            'cityGMTOffset' => 'required|min:1',
            'isActive' => 'required'
        ]);

        $api = new ApiHelper();
        $token = session()->get('apiToken');

        $cityHelper = new CityApiHelper();

        if($token->response):
            $queryParams = array();
            $city = $cityHelper->save($token->response, $request);
            if($city->response === 'success'):
                $message[] = 'success';
                $message[] = 'City has been successfully added';
                if($request->has('countryId')):
                    $queryParams['countryId'] = $request->query('countryId');
                endif;
                return redirect()->route('cities', $queryParams)->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
    }

    public function edit(Request $request, $id)
    {

        $api = new ApiHelper();
        $token = $api->token();

        $countryHelper = new CountryApiHelper();
        $countries = $countryHelper->list($token->response);
        $data['countries'] = $countries->response;

        $data['cityId'] = $id;
        $data['record'] = (object) $request->all();

        return view('cities.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        
        $validateDate = $request->validate([
            'country' => 'required',
            'cityName' => 'required|min:3',
            'cityGMTOffset' => 'required|min:1',
            'isActive' => 'required'
        ]);
        
        $api = new ApiHelper();
        $token = session()->get('apiToken');
        $queryParams = array();
        $cityHelper = new CityApiHelper();

        if($token->response):
            $city = $cityHelper->update($token->response, $id, $request);
            if($city->response === 'success'):
                $message[] = 'success';
                $message[] = 'City has been successfully updated';
                if($request->has('countryId')):
                    $queryParams['countryId'] = $request->query('countryId');
                endif;
                return redirect()->route('cities', $queryParams)->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
    }
}
