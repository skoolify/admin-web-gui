<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegistrationReportsController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();

        return view('registration-reports.list', $data);

    }
}
