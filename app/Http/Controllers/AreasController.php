<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{ApiHelper, AreaApiHelper, CityApiHelper, CountryApiHelper};

class AreasController extends Controller
{
    
    private $areaHelper, $api;
    
    public function __construct()
    {
        $this->areaHelper = new AreaApiHelper();
        $this->cityHelper = new CityApiHelper();
        $this->countryHelper = new CountryApiHelper();
        $this->api = new ApiHelper();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // $cityId = $request->input('cityId') ? $request->input('cityId') : NULL;
        // $token = session()->get('apiToken');
        // if($token->response):
        //     $data['cityId'] = $cityId;
        //     $areas = $this->areaHelper->list($token->response, $cityId);
        //     $cities = $this->cityHelper->list($token->response);
        //     $data['cities'] = gettype($cities->response) === 'array' ? $cities->response : array();
        //     if(gettype($areas->response) === 'array'):
        //         $data['areas'] = $areas->response;
        //     else:
        //         $message[] = 'danger';
        //         $message[] = $areas->response->userMessage;
        //         $data['areas'] = array();
        //         //session()->flash('flash-message', $message);
        //     endif;
        // endif;

        $data['countries'] = array();
        $data['cities'] = array();

        return view('areas.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $token = session()->get('apiToken');
        $data['cities'] = array();
        if($token->response):
            $citiesResponse = $this->cityHelper->list($token->response);
            if($citiesResponse->response):
                $data['cities'] = $citiesResponse->response;
            endif;
        endif;

        return view('areas.add')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'city' => 'required',
            'areaName' => 'required|min:3',
            'isActive' => 'required'
        ]);
        $queryParams = array();
        $token = session()->get('apiToken');
        if($token->response):
            $area = $this->areaHelper->save($token->response, $request);
            if($area->response === 'success'):
                $message[] = 'success';
                $message[] = 'Area has been successfully added';
                if($request->has('cityId')):
                    $queryParams['cityId'] = $request->query('cityId');
                endif;
                return redirect()->route('areas', $queryParams)->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $token = session()->get('apiToken');
        $data['cities'] = array();
        $data['areaId'] = $id;
        if($token->response):
            $citiesResponse = $this->cityHelper->list($token->response);
            if($citiesResponse->response):
                $data['cities'] = $citiesResponse->response;
            endif;
            $data['record'] = (object) $request->all();
        endif;
        
        return view('areas.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $areaId)
    {
        
        $validateDate = $request->validate([
            'city' => 'required',
            'areaName' => 'required|min:3',
            'isActive' => 'required'
        ]);

        $token = session()->get('apiToken');

        if($token->response):
            $queryParams = array();
            $area = $this->areaHelper->update($token->response, $areaId, $request);
            if($area->response === 'success'):
                $message[] = 'success';
                $message[] = 'Area has been successfully updated';
                if($request->has('cityId')):
                    $queryParams['cityId'] = $request->query('cityId');
                endif;
                return redirect()->route('areas', $queryParams)->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
