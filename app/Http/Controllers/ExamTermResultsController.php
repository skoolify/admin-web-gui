<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExamTermResultsController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['classLevels'] = array();
        $data['records'] = array();
        $data['students'] = array();
        $data['nonSubjects'] = array();

        return view('exam-term-results.list', $data);

    }
}
