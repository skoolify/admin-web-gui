<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['countries'] = array();
        $data['cities'] = array();
        $data['areas'] = array();
        $data['classLevels'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['records'] = array();
        $data['students'] = array();

        return view('messages.list', $data);

    }

    public function pictureUpload(Request $request)
    {
       
        $validateDate = $request->validate([
            'attachmentUpload' => 'required|mimes:pdf,jpg,jpeg,png,gif,xlsx,xls,doc,docx|max:10240',
        ]);
       
        $image = $request->file('attachmentUpload');
        $attachmentName = rand() . '_attachment.' . $image->getClientOriginalExtension();
        $attachmentExtension = $image->getClientOriginalExtension();
        if(in_array($attachmentExtension, array('jpg','jpeg','png','gif')) ){
            $image = \Image::make($image);
            $width = $image->width() * env('DAILY_DIARY_WIDTH_RESIZE');
            $height = $image->height() * env('DAILY_DIARY_HEIGHT_RESIZE');
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->save(env('ATTACHMENT_UPLOAD_PATH').'/'.$attachmentName, env('DAILY_DIARY_COMPRESSION_RATE'));
        }else{
            try{
               Storage::disk('attachments')->put($attachmentName, file_get_contents($image));
            }catch(\Exception $e){

            }
        }
        return response()->json([
            'attachment' => $attachmentName,
        ]);
    }
}
