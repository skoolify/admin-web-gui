<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DailyDiaryController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['classes'] = array();
        $data['subjects'] = array();
        $data['records'] = array();
        $data['students'] = array();
        $data['classLevels'] = array();

        return view('daily-diary.list', $data);

    }

    public function pictureUpload(Request $request)
    {
       
        $validateDate = $request->validate([
            'attachmentUpload' => 'required|image'
        ]);
       
        $image = $request->file('attachmentUpload');
        $attachmentName = rand() . '_attachment.' . $image->getClientOriginalExtension();

        $image = \Image::make($image);
        $width = $image->width() * env('DAILY_DIARY_WIDTH_RESIZE');
        $height = $image->height() * env('DAILY_DIARY_HEIGHT_RESIZE');
        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })->save(env('ATTACHMENT_UPLOAD_PATH').'/'.$attachmentName, env('DAILY_DIARY_COMPRESSION_RATE'));
        
        return response()->json([
            'attachment' => $attachmentName,
        ]);
    }

    public function documentUpload(Request $request)
    {
       
        $validateDate = $request->validate([
            'documentUpload' => 'required|mimes:pdf,doc,docx,xlsx,xls,ppt,pptx,txt|max:10240'
        ]);

        $image = $request->file('documentUpload');
        $documentName = rand() . '_document.' . $image->getClientOriginalExtension();
        $image->move(env('DOCUMENT_UPLOAD_PATH'), $documentName);
        
        return response()->json([
            'document'   => $documentName
        ]);
    }

}
