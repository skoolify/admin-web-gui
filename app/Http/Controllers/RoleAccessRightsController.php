<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RoleAccessRightsController extends Controller
{
    public function __construct()
    {
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['schools'] = array();
        $data['branches'] = array();
        $data['roles'] = array();
        $data['records'] = array();


        return view('role-access-rights.list', $data);

    }
}
