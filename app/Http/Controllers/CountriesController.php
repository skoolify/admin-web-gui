<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\{CountryApiHelper, ApiHelper};

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $api = new ApiHelper();
        $token = $api->token();

        $countryHelper = new CountryApiHelper();

        if($token->response):
            $countries = $countryHelper->list($token->response);
            $data['countries'] = $countries->response;
        else:
            echo 'Error in Response';
        endif;

        return view('countries.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('countries.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateDate = $request->validate([
            'countryName' => 'required|min:3',
            'dialingCode' => 'required'
        ]);
        
        
        $api = new ApiHelper();
        $token = $api->token();

        $countryHelper = new CountryApiHelper();

        if($token->response):
            $country = $countryHelper->save($token->response, $request);
            if($country->response === 'success'):
                $message[] = 'success';
                $message[] = 'Country has been successfully added';
                return redirect()->route('countries')->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        
        $api = new ApiHelper();
        $token = $api->token();

        $countryHelper = new CountryApiHelper();

        $data['countryId'] = $id;

        if($token->response):
            $country = $countryHelper->detail($token->response, $id);
            if($country->response):
                $data['record'] = head($country->response);
            else:
                $message[] = 'danger';
                $message[] = 'Country not found';
                return redirect()->route('countries')->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;

        return view('countries.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateDate = $request->validate([
            'countryName' => 'required|min:3',
            'dialingCode' => 'required'
        ]);
        
        $api = new ApiHelper();
        $token = $api->token();

        $countryHelper = new CountryApiHelper();

        if($token->response):
            $country = $countryHelper->update($token->response, $id, $request);
            if($country->response === 'success'):
                $message[] = 'success';
                $message[] = 'Country has been successfully updated';
                return redirect()->route('countries')->with('flash-message', $message);
            endif;
        else:
            echo 'Error in Response';
        endif;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
