<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class SchoolApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list($token = NULL)
    {
        $url = $this->apiUrl.'/getSchools';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save($token = NULL, Request $request)
    {
        $url = $this->apiUrl.'/setSchools';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolName' => $request->input('schoolName'),
                        'onBoardingDate' => $request->input('schoolOnBoardingDate'),
                        'domain' => $request->input('schoolDomain'),
                        'website' => $request->input('schoolWebsiteURL'),
                        'ownerName' => $request->input('schoolOwnerName'),
                        'schoolLogo' => base64_encode(file_get_contents($request->file('logo'))),
                        'isAcitve' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($token = NULL, $id){

        $url = $this->apiUrl.'/getSchools';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $id
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;

    }

    public function update($token = NULL, $id, Request $request){

        $url = $this->apiUrl.'/setSchools';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $id,
                        'schoolName' => $request->input('schoolName'),
                        'onBoardingDate' => $request->input('schoolOnBoardingDate'),
                        'domain' => $request->input('schoolDomain'),
                        'website' => $request->input('schoolWebsiteURL'),
                        'ownerName' => $request->input('schoolOwnerName'),
                        'schoolLogo' => 'logo',
                        'isAcitve' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
