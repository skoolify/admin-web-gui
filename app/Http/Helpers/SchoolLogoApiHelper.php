<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};
use App\Http\Helpers\{ApiHelper};

class SchoolLogoApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
        $this->api = new ApiHelper();
    }

    public function logo(Request $request)
    {
        $responseData = array();

        $url = $this->apiUrl.'/getSchoolLogo';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'domain' => $request->input('domain')
                ]
            ]
        );
            $code = $response->getStatusCode();
            $responseJson = json_decode($response->getBody());
            $responseBody =  $this->api->setStatusCode($response, $responseJson);
            $responseData['code'] = $code;
            $responseData['body'] = $responseBody;
        }catch(BadResponseException $e){
            if ($e->hasResponse()){
                $response = $e->getResponse();
                $code = $e->getStatusCode();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody =  $this->api->setStatusCode($response, $responseJson);
                $responseData['code'] = $code;
                $responseData['body'] = $responseBody;
            }
        }



        return $responseData;
    }

}
