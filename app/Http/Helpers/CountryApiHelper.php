<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class CountryApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list($token)
    {
        $url = $this->apiUrl.'/getCountries';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save($token, Request $request)
    {
        $url = $this->apiUrl.'/setCountries';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'countryName' => $request->input('countryName'),
                        'dialingCode' => $request->input('dialingCode'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($token, $id){

        $url = $this->apiUrl.'/getCountries';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'countryID' => $id
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;

    }

    public function update($token, $id, Request $request){

        $url = $this->apiUrl.'/setCountries';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'countryID' => $id,
                        'countryName' => $request->input('countryName'),
                        'dialingCode' => $request->input('dialingCode'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
