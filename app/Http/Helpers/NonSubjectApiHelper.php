<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class NonSubjectApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list()
    {
        
        $url = $this->apiUrl.'/getNonSubjects';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => session()->get('tempSchoolID'),
                    'classLevelID' => session()->get('tempClassLevelID') ?? 0
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save(Request $request)
    {
        
        $url = $this->apiUrl.'/setNonSubjects';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'classLevelID' => $request->input('classLevel'),
                        'nonSubjectName' => $request->input('nonSubjectName'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($nonSubjectId){
        $url = $this->apiUrl.'/getNonSubjects';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => session()->get('tempSchoolID'),
                        'classLevelID' => session()->get('tempClassLevelID') ?? NULL,
                        'nonSubjectID' => $nonSubjectId
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;

    }

    public function update($nonSubjectId, Request $request){

        $url = $this->apiUrl.'/setNonSubjects';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'nonSubjectID' => $nonSubjectId,
                        'classLevelID' => $request->input('classLevel'),
                        'nonSubjectName' => $request->input('nonSubjectName'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
