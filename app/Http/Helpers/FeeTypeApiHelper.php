<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class FeeTypeApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list($token, $schoolId)
    {

        $url = $this->apiUrl.'/getFeeTypes';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => $schoolId
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save($token, $schoolId, Request $request)
    {
        $url = $this->apiUrl.'/setFeeTypes';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $schoolId,
                        'feeType' => $request->input('feeType'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($token, $feeTypeId, $schoolId){
    }

    public function update($token, $feeTypeId, Request $request){

        $url = $this->apiUrl.'/setFeeTypes';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('schoolId'),
                        'feeTypeID' => $feeTypeId,
                        'feeType' => $request->input('feeType'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
