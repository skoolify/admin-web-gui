<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class RoleApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list()
    {

        $url = $this->apiUrl.'/getRoles';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => session()->get('user')->isAdmin && !session()->has('temp') ? session()->get('user')->schoolID : session()->get('temp')->schoolID
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }        

        return $responseBody;
    }

    public function save(Request $request)
    {
        $url = $this->apiUrl.'/setRole';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('school'),
                        'roleName' => $request->input('roleName'),
                        'roleDesc' => $request->input('roleDescription'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($roleId){

        $url = $this->apiUrl.'/getRoles';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => session()->get('user')->isAdmin && !session()->has('temp') ? session()->get('user')->schoolID : session()->get('temp')->schoolID,
                    'roleID' => $roleId,
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function update($roleId, Request $request){

        $url = $this->apiUrl.'/setRole';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('school'),
                        'roleID' => $roleId,
                        'roleName' => $request->input('roleName'),
                        'roleDesc' => $request->input('roleDescription'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
