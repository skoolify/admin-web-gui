<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;

class Authorization
{

    public static function check($module = 'admin', $flow = 'read', $parent = 'admin', $checkParent = false)
    {
        $authorize = false;
        $permissions = session()->get('permissions');
        if(isset($permissions[$parent])){
            $p = $permissions[$parent];
            if($checkParent){
                if(isset($p->childrens[$module])){
                    $module = $p->childrens[$module];
                    if(!$module->isParent){
                        $authorize = $module->$flow ? true : false;
                    }
                }
            }else{
                $authorize = true;
            }   
        } 
        return $authorize;
    }

    public static function getParent($module)
    {
        $parent = NULL;
        if(session()->has('permissions')){
            foreach (session()->get('permissions') as $key => $permission) {
                if(isset($permission->childrens[$module])){
                    $parent = $permission->childrens[$module]->parent;
                } 
            }
            return $parent;
        }else{
            return redirect()->route('auth-login');
        }
    }
}

