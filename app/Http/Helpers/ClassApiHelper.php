<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class ClassApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list()
    {
        
        $url = $this->apiUrl.'/getClasses';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => session()->has('tempSchoolID') ? session()->get('tempSchoolID') : session()->get('user')->schoolID,
                    'branchID' => session()->has('tempBranchID') ? session()->get('tempBranchID') : session()->get('user')->branchID
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save(Request $request)
    {
        $url = $this->apiUrl.'/setClasses';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'branchID' => $request->input('branch'),
                        'classLevelID' => $request->input('classLevel'),
                        'section' => $request->input('classSection'),
                        'shift' => $request->input('classShift'),
                        'classTeacherID' => $request->input('classTeacher'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($classId){
        $url = $this->apiUrl.'/getClasses';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => session()->has('tempSchoolID') ? session()->get('tempSchoolID') : session()->get('user')->schoolID,
                        'branchID' => session()->has('tempBranchID') ? session()->get('tempBranchID') : session()->get('user')->branchID,
                        'classID' => $classId
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;

    }

    public function update($classId, Request $request){

        $url = $this->apiUrl.'/setClasses';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'classID' => $classId,
                        'branchID' => $request->input('branch'),
                        'classLevelID' => $request->input('classLevel'),
                        'section' => $request->input('classSection'),
                        'shift' => $request->input('classShift'),
                        'classTeacherID' => $request->input('classTeacher'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
