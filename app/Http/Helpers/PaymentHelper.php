<?php

namespace App\Http\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', '7c5245f5470c43fea71dd6a0db9dff806bb140a92c88446091709d5328fb7df71184ac51f24c46f99cf4b92f9e6889502c519f88c04e4c87817f9b4417fab84152e2ec0e79984dfd8035f70abd764c056fc2b674a0e34b5e8a2573fcfd32ad3ba583a17e75fe47f5b932a8400cb8b5323c61d21f406d46bd87f06dbfc164faf1');

class PaymentHelper
{
    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function generateHash($data)
    {
        $secretKey = '';
        $customParam = array();
        $data = explode(',', $data);
        foreach ($data as $key => $value) {
            if(head(explode('=', $value)) === 'signed_field_names'){
                $data[$key] = preg_replace('/\|+/', ',', $value);
            }
            if(head(explode('=', $value)) === 'bill_to_address_line1'){
                $customParam['head'] = head(explode('=', $value));
                $customParam['last'] = base64_decode(str_replace('bill_to_address_line1=', '', $value));
                $data[$key] = implode('=', $customParam);
            }
            if(head(explode('=',$value)) === 'providerURL'){
                unset($data[$key]);
            }
            if(head(explode('=',$value)) === 'secret_key'){
                $secretKey = $data[$key];
                unset($data[$key]);
            }
        }
        if($secretKey !== ''){
            $secretKey = explode('=',$secretKey);
            $secretKey = $secretKey[1];
        }else{
            return abort(401);
        }
        $data = implode(',', $data);
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    public function decryptData($token, $data)
    {
        $url = $this->apiUrl.'/setDecryptData';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'inputData' => $data
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }
        return $responseBody;
    }

    public function paymentStatus($token, $data){
        $dataForEncodeBase64 = http_build_query($data,'','|');
        $dataForEncodeBase64 = base64_encode($dataForEncodeBase64);
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token->response
            ]
        ]);

        if(isset($data['auth_time'])){
            $tranResponseDateTime = $data['auth_time'];
            $tranResponseDateTime = strtotime($tranResponseDateTime);
            $tranResponseDateTime = date("Y-m-d H:i:s", $tranResponseDateTime);
        }else{
            $tranResponseDateTime = \Carbon\Carbon::now()->toDateTimeString();
        }

        if(isset($data['signed_date_time'])){
            $tranRequestDateTime = $data['signed_date_time'];
            $tranRequestDateTime = strtotime($tranRequestDateTime);
            $tranRequestDateTime = date("Y-m-d H:i:s", $tranRequestDateTime);
        }else{
            $tranRequestDateTime = '';
        }

        $referenceNumber = explode('-',$data['req_transaction_uuid']);
        if($referenceNumber[0] === 'SSC'){
            $url = $this->apiUrl.'/setPaymentTransactionLogWeb';
            $params = [
                'form_params' => [
                    'IPAddress'             =>  isset($data['req_customer_ip_address']) ? $data['req_customer_ip_address'] : '',
                    'providerTxnRefNo'      =>  isset($data['transaction_id']) ? $data['transaction_id'] : '',
                    'internalTxnRefNo'      =>  isset($data['req_transaction_uuid']) ? $data['req_transaction_uuid'] : '',
                    'maskedInstrument'      =>  isset($data['req_card_number']) ? $data['req_card_number'] : '',
                    'paymentCurrency'       =>  isset($data['req_currency']) ? $data['req_currency'] : '',
                    'paymentAmount'         =>  isset($data['req_amount']) ? $data['req_amount'] : '',
                    'tranRequestDateTime'   =>  $tranRequestDateTime,
                    'tranRequestCode'       =>  isset($data['req_transaction_type']) ? $data['req_transaction_type'] : '',
                    'additionalData'        =>  isset($data['decision_rmsg']) ? $data['decision_rmsg'] : '',
                    'tranResponseDateTime'  =>  $tranResponseDateTime,
                    'tranResponseCode'      =>  isset($data['decision']) ? $data['decision'] : '',
                    'tranResponseData'      =>  $dataForEncodeBase64,
                    'userUUID'              =>  session()->get('user') !== null ? session()->get('user')->userUUID : NULL,
                ]
            ];
        }else{
            $url = $this->apiUrl.'/setPaymentLogHBLCC';
            $params = [
                'form_params' => [
                    'IPAddress'             =>  isset($data['req_customer_ip_address']) ? $data['req_customer_ip_address'] : '',
                    'providerTxnRefNo'      =>  isset($data['transaction_id']) ? $data['transaction_id'] : '',
                    'internalTxnRefNo'      =>  isset($data['req_transaction_uuid']) ? $data['req_transaction_uuid'] : '',
                    'maskedInstrument'      =>  isset($data['req_card_number']) ? $data['req_card_number'] : '',
                    'paymentCurrency'       =>  isset($data['req_currency']) ? $data['req_currency'] : '',
                    'paymentAmount'         =>  isset($data['req_amount']) ? $data['req_amount'] : '',
                    'tranRequestDateTime'   =>  $tranRequestDateTime,
                    'tranRequestCode'       =>  isset($data['req_transaction_type']) ? $data['req_transaction_type'] : '',
                    'additionalData'        =>  isset($data['decision_rmsg']) ? $data['decision_rmsg'] : '',
                    'tranResponseDateTime'  =>  $tranResponseDateTime,
                    'tranResponseCode'      =>  isset($data['decision']) ? $data['decision'] : '',
                    'tranResponseData'      =>  $dataForEncodeBase64,
                ]
            ];
        }

        try{
            $response = $client->request('POST', $url, $params);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }
    }
}
