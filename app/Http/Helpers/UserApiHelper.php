<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class UserApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list()
    {

        $url = $this->apiUrl.'/getUserList';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => session()->get('tempSchoolID'),
                    'branchID' => session()->get('tempBranchID') ? session()->get('tempBranchID') : session()->get('tempBranchID')
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save(Request $request)
    {
        
        $url = $this->apiUrl.'/setUser';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('school'),
                        'fullName' => $request->input('fullName'),
                        'mobileNo' => $request->input('mobileNo'),
                        "password" => $request->input('password'),
                        'emailAddress' => $request->input('emailAddress'),
                        'createdByUserID' => session()->get('user')->userID,
                        'isAdmin' => $request->input('admin'),
                        'userTimeZone' => '+5',
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($userID){

        $url = $this->apiUrl.'/getUserList';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' => session()->get('temp')->schoolID,
                    'userID' => $userID,
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function update($userId, Request $request){

        $url = $this->apiUrl.'/setUser';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'userID' => $userId,
                        'schoolID' => $request->input('school'),
                        'fullName' => $request->input('fullName'),
                        'mobileNo' => $request->input('mobileNo'),
                        "password" => $request->input('password'),
                        'emailAddress' => $request->input('emailAddress'),
                        'createdByUserID' => session()->get('user')->userID,
                        'isAdmin' => $request->input('admin'),
                        'userTimeZone' => '+5',
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
