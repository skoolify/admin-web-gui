<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class CalendarDayHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list()
    {

        $url = $this->apiUrl.'/getCalendarDays';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolId' => session()->get('tempSchoolID'),
                    'calendarDateFrom' => \Carbon\Carbon::now()->startOfMonth()->toDateString(),
                    'calendarDateTo' => \Carbon\Carbon::now()->endOfMonth()->toDateString()
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save(Request $request)
    {
        $url = $this->apiUrl.'/setCalendarDays';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('school'),
                        'calendarDate' => $request->input('calendarDate'),
                        'holidayName' => $request->input('holidayName'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($calendarDayId){

        $url = $this->apiUrl.'/getCalendarDays';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolId' => session()->get('tempSchoolID'),
                    'calendarDateFrom' => \Carbon\Carbon::now()->startOfMonth()->toDateString(),
                    'calendarDateTo' => \Carbon\Carbon::now()->endOfMonth()->toDateString()
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function update($calendarDayId, Request $request){

        $url = $this->apiUrl.'/setCalendarDays';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('school'),
                        'calendarID' => $calendarDayId,
                        'calendarDate' => $request->input('calendarDate'),
                        'holidayName' => $request->input('holidayName'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
