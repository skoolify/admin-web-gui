<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class CurlHelper
{

    public function token()
    {
        $url = env('API_URL').'/'.env('API_VERSION').'/getToken';
        $client = new Client();
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'apiKey' => '42E358A838BA26CD1E5FEB67BA0D6879',
                        'sourceUuid' => '12345678'
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }
}
