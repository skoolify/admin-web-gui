<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};
use App\Http\Helpers\{ApiHelper};

class AuthApiHelper
{

    private $apiUrl, $api;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
        $this->api = new ApiHelper();
    }

    public function validateUser($token, $request)
    {

        $url = $this->apiUrl.'/userValidation';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'mobileNo' => $request->input('mobileNo'),
                    'domain' => $request->input('domain')
                ]
            ]
        );
            $responseJson = json_decode($response->getBody());
            $responseBody =  $this->api->setStatusCode($response, $responseJson);
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody =  $this->api->setStatusCode($response, $responseJson);
            }
        }

        return $responseBody;
    }

    public function login($token, $request)
    {

        $url = $this->apiUrl.'/passwordValidation';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'mobileNo' => $request->input('mobileNo'),
                    'domain' => $request->input('domain'),
                    'password' => md5($request->input('password'))
                ]
            ]
        );
            $responseJson = json_decode($response->getBody());
            $responseBody =  $this->api->setStatusCode($response, $responseJson);
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody =  $this->api->setStatusCode($response, $responseJson);
            }
        }

        return $responseBody;
    }

    public function getSchoolContract($token, $request)
    {

        $url = $this->apiUrl.'/getSchoolContract';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);

        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'userID' => $request->input('userID')
                ]
            ]
        );
            $responseJson = json_decode($response->getBody());
            $responseBody =  $this->api->setStatusCode($response, $responseJson);
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody =  $this->api->setStatusCode($response, $responseJson);
            }
        }

        return $responseBody;
    }

    public function userDetail($token)
    {
        if(isset(session()->get('user')->userID) && session()->get('user')->userID !== ''){
            $userID = session()->get('user')->userID;
        }else{
            $userID = session()->get('user')[0]->userID;
        }
        $url = $this->apiUrl.'/userDetails';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'userID' => $userID
                ]
            ]
        );
            
            $responseJson = json_decode($response->getBody());
            $responseBody =  $this->api->setStatusCode($response, $responseJson);
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody =  $this->api->setStatusCode($response, $responseJson);
            }
        }
        return $responseBody;
    }

    public function userModules($token)
    {

        $url = $this->apiUrl.'/getUserModules';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'userID' => session()->get('user')->userID,
                    'roleID' => session()->get('user')->roleID
                ]
            ]
        );
            $responseJson = json_decode($response->getBody());
            $responseBody =  $this->api->setStatusCode($response, $responseJson);
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody =  $this->api->setStatusCode($response, $responseJson);
            }
        }

        return $responseBody;
    }


    public function modules()
    {

        $url = $this->apiUrl.'/getModules';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                ]
            ]
        );
            $responseJson = json_decode($response->getBody());
            $responseBody =  $this->api->setStatusCode($response, $responseJson);
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody =  $this->api->setStatusCode($response, $responseJson);
            }
        }

        return $responseBody;
    }

}
