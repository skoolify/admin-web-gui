<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class ApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function token()
    {
        $url = $this->apiUrl.'/getToken';
        $client = new Client();
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'apiKey' => 'a412da5eae606cfd313bb5ff01bbb830',
                        'sourceUuid' => 'WebApplication'
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $code = $response->getStatusCode();
                if($code === 503){
                    return abort(503);
                }
                $responseJson = json_decode($response->getBody()->getContents());
            }
        }
        return $responseBody;
    }


    // public function commsCount()
    // {
    //     $error = false;
    //     $url = $this->apiUrl.'/getCommsCount';
    //     $code = 200;
        
    //     $client = new Client([
    //         'headers' => [
    //             'Authorization' => 'Bearer '.session()->get('apiToken')->response
    //         ]
    //     ]);

    //     $params = array(
    //         'schoolID' => session()->get('user')->schoolID,
    //         'branchID' => session()->get('user')->branchID
    //     );

    //     try{
    //         $response = $client->request('POST', $url, [
    //             'form_params' => $params
    //         ]);
    //         $responseBody = json_decode($response->getBody());
    //         session([ 'commsCount' => head($responseBody->response) ]);
    //     }catch(BadResponseException $e){
    //         if ($e->hasResponse()) {
    //             $response = $e->getResponse();
    //             $responseJson = json_decode($response->getBody()->getContents());
    //             return abort(503);
    //         }
    //     }
    //     return $responseBody;
    // }

    public function setStatusCode($guzzleResponse, $responseJson){
        $responseJson = (array) $responseJson;
        if(gettype($responseJson['response']) !== 'array'){
            $responseJson['response']->code = $guzzleResponse->getStatusCode();
            $responseJson['response']->error = $guzzleResponse->getStatusCode() == 200 ? false : true;
        }else{
            $responseJson['code'] = $guzzleResponse->getStatusCode();
            $responseJson['error'] = $guzzleResponse->getStatusCode() == 200 ? false : true;
        }
        return (object) $responseJson;
    }

    public function extractDetail($response = [], $needle = NULL, $key = 'classID')
    {
        $record = NULL;
        if($needle && $key):
            foreach ($response as $index => $res):
                if($res->$key == $needle):
                    $record = $res;
                endif;
            endforeach;
        endif;

        return $record;
    }

    public function setTempParams(Request $request)
    {
        foreach ($request->all() as $key => $val):
            $key = strrev(ucfirst(strrev(ucfirst($key))));
            if($key && $val):
                session(['temp'.$key => ($val ?? NULL)]);
            else:
                session()->forget(['temp'.$key]);
            endif;
        endforeach;
    }

}
