<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class CityApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list($token, $countryId = NULL)
    {

        $url = $this->apiUrl.'/getCities';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'countryID' => $countryId
                ]
            ]
        );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save($token, Request $request)
    {
        $url = $this->apiUrl.'/setCities';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'countryID' => $request->input('country'),
                        'cityName' => $request->input('cityName'),
                        'gmtOffset' => $request->input('cityGMTOffset'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($token, $id){

        $url = $this->apiUrl.'/getCities';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('GET', $url, [
                    'query' => [
                        'countryID' => $id
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;

    }

    public function update($token, $id, Request $request){

        $url = $this->apiUrl.'/setCities';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'cityID' => $id,
                        'countryID' => $request->input('country'),
                        'cityName' => $request->input('cityName'),
                        'gmtOffset' => $request->input('cityGMTOffset'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
