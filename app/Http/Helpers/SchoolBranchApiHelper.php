<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\BadResponseException, Psr7};

class SchoolBranchApiHelper
{

    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = env('API_URL').'/'.env('API_VERSION');
    }

    public function list($token = NULL, $schoolId = NULL)
    {
        $url = $this->apiUrl.'/getSchoolBranches';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('apiToken')->response
            ]
        ]);        
        try{
            $response = $client->request('POST', $url, [
                'form_params' => [
                    'schoolID' =>  session()->has('tempSchoolID') ? session()->get('tempSchoolID') : session()->get('user')->schoolID
                ]
            ]);
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function save($token, Request $request)
    {

        $url = $this->apiUrl.'/setSchoolBranches';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('branchSchool'),
                        'branchName' => $request->input('branchName'),
                        'branchCode' => $request->input('branchCode'),
                        'address1' => $request->input('branchAddress1'),
                        'address2' => $request->input('branchAddress2'),
                        'address3' => $request->input('branchAddress3'),
                        'contact1' => $request->input('branchContact1'),
                        'contact2' => $request->input('branchContact2'),
                        'contact3' => $request->input('branchContact3'),
                        'principleName' => $request->input('branchPrincipleName'),
                        'email' => $request->input('branchEmail'),
                        'countryID' => $request->input('branchCountry'),
                        'cityID' => $request->input('branchCity'),
                        'areaID' => $request->input('branchArea'),
                        'gpsCordinates' => $request->input('branchGpsCoords'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

    public function detail($token, $schoolId, $branchId){

        $url = $this->apiUrl.'/getSchoolBranches';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $schoolId,
                        'branchID' => $branchId
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;

    }

    public function update($token, $branchId, Request $request){

        $url = $this->apiUrl.'/setSchoolBranches';
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer '.$token
            ]
        ]);
        
        try{
            $response = $client->request('POST', $url, [
                    'form_params' => [
                        'schoolID' => $request->input('branchSchool'),
                        'branchID' => $branchId,
                        'branchName' => $request->input('branchName'),
                        'branchCode' => $request->input('branchCode'),
                        'address1' => $request->input('branchAddress1'),
                        'address2' => $request->input('branchAddress2'),
                        'address3' => $request->input('branchAddress3'),
                        'contact1' => $request->input('branchContact1'),
                        'contact2' => $request->input('branchContact2'),
                        'contact3' => $request->input('branchContact3'),
                        'principleName' => $request->input('branchPrincipleName'),
                        'email' => $request->input('branchEmail'),
                        'countryID' => $request->input('branchCountry'),
                        'cityID' => $request->input('branchCity'),
                        'areaID' => $request->input('branchArea'),
                        'gpsCordinates' => $request->input('branchGpsCoords'),
                        'isActive' => $request->input('isActive')
                    ]
                ]
            );
            $responseBody = json_decode($response->getBody());
        }catch(BadResponseException $e){
            dd($e); //BROKEN
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseJson = json_decode($response->getBody()->getContents());
                $responseBody = $responseJson;
            }
        }

        return $responseBody;
    }

}
