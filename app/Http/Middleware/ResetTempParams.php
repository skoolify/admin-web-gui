<?php

namespace App\Http\Middleware;

use Closure;

class ResetTempParams
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($url = url()->previous()){
            if(!str_contains($url, ['add', 'edit']) && !($request->has('schoolId'))){

                session()->forget(
                    [
                        'tempSchoolID',
                        'tempBranchID',
                        'tempClassLevelID',
                        'tempClassID',
                        'tempCountryID',
                        'tempCityID',
                        'tempAreaID',
                        'temp'
                    ]
                );

                if(session()->get('user')){
                    session(
                        [
                            'tempSchoolID' => session()->get('user')->schoolID,
                            'tempBranchID' => session()->get('user')->branchID,
                        ]
                    );
                }else{
                    return redirect()->route('auth-login');
                }
            }
        }

        return $next($request);
    }
}
