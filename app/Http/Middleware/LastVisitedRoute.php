<?php namespace App\Http\Middleware;

use Closure;

class LastVisitedRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        session(['last-visited-url' => request()->segment(count(request()->segments()))]);
        return $next($request);
    }
}
