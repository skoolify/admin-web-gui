<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Stevebauman\Location\Facades\Location;

class SetLanguage
{
    public function handle($request, Closure $next)
    {
        $ip = request()->ip();
        $position = Location::get($ip);
        $_SESSION['country'] = isset($position) && isset($position->countryName) && $position->countryName ? $position->countryName : 'Pakistan';
        if(isset($_COOKIE['lang']) && $_COOKIE['lang']){
            App::setLocale($_COOKIE['lang']);
        }else{
            $locale = session()->get('locale');
            App::setLocale($locale ?? 'en');
        }

        return $next($request);
    }
}
