<?php

namespace App\Http\Middleware;

use Closure;

class Permissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = explode('/', $request->getRequestUri());
        $route = end($route);
        if (strpos($route, '?')) {
            $route = explode('?',$route);
            $route = $route[0];
        }
        $permissions = session()->get('permissions');
        $module = NULL;
        if($permissions && count($permissions) >= 1){
            foreach ($permissions as $key => $permission) {
                if(isset($permission->childrens[$route])){
                    $module = $permission->childrens[$route];
                }
            }
            if(!empty($module)){
                if($module->read == 1){
                    session(['home-page' => $route]);
                    return $next($request);
                }else{
                    abort(403);
                }
            }else{
                $route = head(head($permissions)->childrens)->moduleURL;
                $module = head($permissions)->childrens[$route];
                if($module->read == 1){
                    session(['home-page' => $route]);
                    return redirect()->route($route);
                }else{
                    abort(403);
                }
            }
        }else{
            return redirect()->route('auth-login');
        }
    }
}
