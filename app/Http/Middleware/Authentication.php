<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Helpers\{ApiHelper};
use Carbon\Carbon;

class Authentication
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentTimestamp = Carbon::now()->timestamp;
        $api = new ApiHelper();
        // $api->commsCount();
        $api->token();
        if(session()->has('apiToken')):
            $token = session()->get('apiToken');
            if($currentTimestamp < $token->expiry){
                $user = session()->get('user');
                if(!$user){
                    return redirect()->route('auth-login');
                }
                return $next($request);
            }else{
                $token = $api->token();
                session(['apiToken' => $token]);
                $user = session()->get('user');
                if(!$user){
                    return redirect()->route('auth-login');
                }
                return $next($request);
            }
        else:
            return redirect()->route('auth-login');
        endif;
        
    }
}
