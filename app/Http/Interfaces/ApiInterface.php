<?php

namespace App\Http\Interfaces;
use Illuminate\Http\Request;

interface ApiInterface{
    public function list($token);
    public function detail($token, $id);
    public function save($token, Request $request);
    public function update($token, $id, Request $request);
}