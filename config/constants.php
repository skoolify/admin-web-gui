<?php

return [
    'subParentModule' => [
        'subParent' => '1'
    ],
    'subChildren' => [
        "promotions",
        "nonacademic",
        "academic",
        "overall-exam"
    ],
    'hours' => [
        '00','01','02','03','04','05','06','07','08','09','10','11',
        '12','13','14','15','16','17','18','19','20','21','22','23'
    ],
    'minutes' => [
        '00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20',
        '21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40',
        '41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'
    ],
    'classLevels' => [
        '-6' => 'DayCare',
        '-5' => 'PlayGroup',
        '-4' => 'PrePrimary',
        '-3' => 'Nursery',
        '-2' => 'KG1',
        '-1' => 'KG2',
        '1' => 'Grade 1',
        '2' => 'Grade 2',
        '3' => 'Grade 3',
        '4' => 'Grade 4',
        '5' => 'Grade 5',
        '6' => 'Grade 6',
        '7' => 'Grade 7',
        '8' => 'Grade 8',
        '9' => 'Grade 9',
        '10' => 'Grade 10',
        '11' => 'Intermediate',
        '12' => 'O-Level',
        '13' => 'A-Level'
    ],
    'timezones' => [
        '+00:00',
        '+01:00',
        '+02:00',
        '+03:00',
        '+03:30',
        '+04:00',
        '+04:30',
        '+05:00',
        '+05:30',
        '+05:45',
        '+06:00',
        '+06:30',
        '+07:00',
        '+08:00',
        '+08:30',
        '+08:45',
        '+09:00',
        '+09:30',
        '+10:00',
        '+10:30',
        '+11:00',
        '+12:00',
        '+13:00',
        '+13:45',
        '+14:00',
        '-01:00',
        '-02:00',
        '-03:00',
        '-03:30',
        '-04:00',
        '-05:00',
        '-06:00',
        '-07:00',
        '-08:00',
        '-09:00',
        '-09:30',
        '-10:00',
        '-11:00',
    ],
    'modules' => [
        array(
            'title' => 'Administration',
            'icon' => 'chart-line',
            'route' => 'admin',
            'childrens' => [
                'schools' => [
                    'title' => 'Schools',
                    'icon' => 'city',
                    'route' => 'schools',
                    'parent' => 'Administration'
                ],
                'school-branches' => [
                    'title' => 'School Branches',
                    'icon' => 'share-alt',
                    'route' => 'school-branches',
                    'parent' => 'Administration'
                ],
                'roles' => [
                    'title' => 'Roles',
                    'icon' => 'flash',
                    'route' => 'roles',
                    'parent' => 'Administration'
                ],
                'modules' => [
                    'title' => 'Modules',
                    'icon' => 'cog',
                    'route' => 'modules',
                    'parent' => 'Administration'
                ],
                'countries' => [
                    'title' => 'Countries',
                    'icon' => 'globe',
                    'route' => 'countries',
                    'parent' => 'Administration'
                ],
                'cities' => [
                    'title' => 'Cities',
                    'icon' => 'map-marker',
                    'route' => 'cities',
                    'parent' => 'Administration'
                ],
                'areas' => [
                    'title' => 'Areas',
                    'icon' => 'road',
                    'route' => 'areas',
                    'parent' => 'Administration'
                ],
                'classes' => [
                    'title' => 'Classes',
                    'icon' => 'level-up',
                    'route' => 'class-levels',
                    'parent' => 'Administration'
                ],
                'role-access-rights' => [
                    'title' => 'Role Access Rights',
                    'icon' => 'lock',
                    'route' => 'role-access-rights',
                    'parent' => 'Administration'
                ]
            ]
        ),
        array(
            'title' => 'Management',
            'icon' => 'chart-area',
            'route' => 'management',
            'childrens' => [
                'users' => [
                    'title' => 'Users',
                    'icon' => 'user',
                    'route' => 'users',
                    'parent' => 'Management'
                ],
                'fee-type' => [
                    'title' => 'Fee Types',
                    'icon' => 'tags',
                    'route' => 'fee-type',
                    'parent' => 'Management'
                ],
                'subjects' => [
                    'title' => 'Subjects',
                    'icon' => 'book',
                    'route' => 'subjects',
                    'parent' => 'Management'
                ],
                'non-subjects' => [
                    'title' => 'Non-Subjects',
                    'icon' => 'star',
                    'route' => 'non-subjects',
                    'parent' => 'Management'
                ],
                'students' => [
                    'title' => 'Students',
                    'icon' => 'users',
                    'route' => 'students',
                    'parent' => 'Management'
                ],
                'time-table' => [
                    'title' => 'Time Table',
                    'icon' => 'clock-o',
                    'route' => 'time-table',
                    'parent' => 'Management'
                ],
                'attendance' => [
                    'title' => 'Attendance',
                    'icon' => 'calendar',
                    'route' => 'attendance',
                    'parent' => 'Management'
                ],
                'messages' => [
                    'title' => 'Messages',
                    'icon' => 'comments',
                    'route' => 'messages',
                    'parent' => 'Management'
                ],
                'communication' => [
                    'title' => 'Communication',
                    'icon' => 'envelope',
                    'route' => 'comm-requests',
                    'parent' => 'Management'
                ],
                'exam-term' => [
                    'title' => 'Exam Terms',
                    'icon' => 'file',
                    'route' => 'exam-term',
                    'parent' => 'Management'
                ],
                'quiz-results' => [
                    'title' => 'Quiz Results',
                    'icon' => 'star-of-life',
                    'route' => 'quiz-results',
                    'parent' => 'Management'
                ],
                'exam-term-results' => [
                    'title' => 'Exam Term Results',
                    'icon' => 'file-signature',
                    'route' => 'exam-term-results',
                    'parent' => 'Management'
                ],
                'classes' => [
                    'title' => 'Class/Sections',
                    'icon' => 'bar-chart',
                    'route' => 'classes',
                    'parent' => 'Management'
                ],
                'calendar-days' => [
                    'title' => 'Calendar Days',
                    'icon' => 'calendar',
                    'route' => 'calendar-days',
                    'parent' => 'Management'
                ],
                'daily-diary' => [
                    'title' => 'Daily Diary',
                    'icon' => 'pencil',
                    'route' => 'daily-diary',
                    'parent' => 'Management'
                ],
                'parents' => [
                    'title' => 'Parents',
                    'icon' => 'user',
                    'route' => 'parents',
                    'parent' => 'Management'
                ],
                'assessments' => [
                    'title' => 'Assessments',
                    'icon' => 'star',
                    'route' => 'assessments',
                    'parent' => 'Management'
                ]
            ]
        ),
        array(
            'title' => 'Finance',
            'icon' => 'money-bill-alt',
            'route' => 'finance',
            'childrens' => [
                [
                    'title' => 'Fee Challans',
                    'icon' => 'money-check',
                    'route' => 'student-fee-challan',
                    'parent' => 'Finance'
                ]
            ]
        ),
    ]
];


