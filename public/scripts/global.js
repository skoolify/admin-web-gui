var isFormValidation = false;
var idEditButtonClicked = false
var isWebLinkExist = false
var studentClassSectionID = null;
// var messageUserClassLevels = []
$(function () {
    $(document).ready(function(){
        // $('.language').on('click', function(){
        //     var lang = $(this).attr('data-value')
        //     $('#language-input').val(lang)
        //     $('#language-form').submit()
        // })

        if((user && user.isBlocked !== 1) || (user && user.isBlocked === 1 && user.feePaymentAllowed === 1)){
            paymentMessageAlert = JSON.parse(localStorage.getItem('paymentMessageAlert'));
            if(paymentMessageAlert && paymentMessageAlert !== null && paymentMessageAlert !== ''){
                $('#loginToast').html('Reminder : '+paymentMessageAlert)
                $('#toastMessageDiv').slideDown(1000)
            }
        }
        localStorage.removeItem('paymentMessageAlert');
        if(localStorage.getItem('isAccepted') === '0'){
            window.location.replace(`${__BASE__}/auth/login?logout=1`)
        }
        idEditButtonClicked = false
        isWebLinkExist = false
        toastMessage = JSON.parse(localStorage.getItem('toast'));
        if(toastMessage && toastMessage !== null){
            $('#loginToast').text('Note: '+toastMessage)
            $('#toastMessageDiv').slideDown(1000)
        }
        localStorage.removeItem('toast');
        meetingReminderToast = JSON.parse(localStorage.getItem('meetingDetails'));
        if(meetingReminderToast && meetingReminderToast !== null && meetingReminderToast !== ''){
            reminderMessage = `${meetingReminderToast.subjectName} ${messages.sessionIsScheduleForClass} ${meetingReminderToast.classAlias+'-'+meetingReminderToast.classSection} ${messages.at} ${moment(meetingReminderToast.startDateTime).format('hh:mm A')}. <a style="font-weight:700;" class="text-danger" href="${meetingReminderToast.webLink}" target="_blank"> ${messages.joinNow} </a>`
            $('#loginToast').html('Reminder : '+reminderMessage)
            $('#toastMessageDiv').slideDown(1000)
        }
        localStorage.removeItem('meetingDetails');
        getToken();
        var filterArray = [
            "filter-modules","classLevelName","assessmentOnly","classShift", "filter-schools", "filter-branches", "filter-classLevels", "filter-classes", "filter-users", "filter-class-teachers", "filter-subjects", "filter-non-subjects", "filter-students", "filter-countries", "filter-cities", "filter-exam-term", "filter-fee-type", "filter-areas", "filter-roles", "filter-message-type", "billing-period-filter","filter-activities","filter-week-day","filter-subjects-in-time-table","filter-batch-no","filter-class-room","filter-branches-branches","payment-status-filter","filter-branch-group","filter-verified","filter-settled","filter-active-inactive"
        ];
        filterArray.forEach(function(value, index){
            var maxHeight = 300;
            if(__ROUTE__ === 'parents' &&  __ROUTE__ === 'users' ){
                maxHeight = 250
            }
            $("#"+value).multiselect({
                enableFiltering:true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: maxHeight,
                maxWidth:100,
                templates: {
                            filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                            filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-times"></i></button></span>',
                        },
            });

        });

        if(__ROUTE__ !== 'contract' && __ROUTE__ !== 'terms-conditions'){
            $('.multiselect').addClass('w-100');
            $('.btn-group').addClass('w-100');
            $('#multiselect-container').addClass('w-100')
        }


        if(typeof __SWITCH_ROLE__ !== 'undefined'){

            localStorage.setItem('user', __SWITCH_ROLE__);
        }

    });

})

var token = JSON.parse(localStorage.getItem('apiToken'));
var user = JSON.parse(localStorage.getItem('user'));
if(typeof user === 'string'){
    user = JSON.parse(user);
}
if(user){
    var schoolID = user.schoolID;
    var branchID = user.branchID;
    var countryID = user.countryID;
    var roleID = user.roleID;
    var userID = user.userID;
}
var apiUrl = __BASE_AJAX__;
var routeBaseUrl = __BASE__;
var herokuUrl1 = 'https://apps.skoolify.app/v1';
var herokuUrl = __BASEURL__;
var __REPROCESS__ = false;
var __RETRIES__ = 0;
var subjectAttendanceDataTable = null;

var submitLoader = 'submit-loader';
var moduleID = "";
var activityID = ''
var classID = "";
var cityID = "";
var feeTypeID = "";
var areaID = "";
var subjectID = "";
var billingPeriodID = "";
var subjectName ="";
var nonSubjectID = "";
var studentID = "";
var examTermID = "";
var classLevelID = "";
var classTeacherID = '';
var classSection = "";
var quizID = "";
var messageType = '';
var feeTpesResponse = '';
var timetableSubjectID = '';
var tableData = null;
var subjectTimeTableData = null;
var nonSubjectResultList = $('#non-subject-results-list');
var subjectResultList = $('#non-subject-results-list');
var overallSujectResult = $('#overall-result-list');
var subjectAttandanceTable = null;
var academicEventCheck = null;
var batchNo = '';
var inviteListTable = null
var weekNo =  []

var __MODULES__ = [];
var __ATTENDANCE__ = [];
var __OVERALLRESULT__ = [];
var __SUBJECTATTENDANCE__ = []

var __TOKPROCESS__ = true;

var dateFromVal = moment().startOf('month').format('YYYY-MM-DD');
var dateToVal = moment().endOf('month').format('YYYY-MM-DD');
var jsonArray = [];
var submittedRights = [];
var deleteRolesArray = [];
var allClasses = [];
var messagesClassLevelArray = [];
var messagesClassArray = [];
var messagesSelectedClassesArray = [];
var messagesStudentArray = [];
var messagesSelectedStudentsArray = [];
var feeTypeArray = [];
var studentRecordsArray = [];
var assignedActivitiesList = [];
var timeTableArray = [];
var subjectArray = []
var weekdayNo = "";
var selectedClassLevelArray = [];
var assignedSubjectArray = []
var selectedForDelete = []
var headers = {
    'Authorization': 'Bearer ' + token.response,
    //'Content-Type' : 'application/json',
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
};
var data = {};

// function setSessionValue(language){
//     $.ajax({
//         url: apiUrl+'/set-session-lang',
//         method: 'POST',
//         data: {
//             "_token": __CSRF__,
//             language
//         },
//         success: function(res){
//             console.log(res)
//             // return
//             if(res.success === true){
//                 const { previousUrl } = res
//                 window.location.replace(previousUrl)
//             }
//         },
//         error: function(err){
//             window.invokedFunctionArray.push('setSessionValue')
//             window.requestForToken(err)
//         }
//     })
// }

$(document).ready(function(){
    if(schoolID != '' && (user && user.isAdmin !== 1) ){
        if(__ROUTE__ == 'billing-period'){
            getBillingPeriod();
        }
    }

    $(document).on('click', 'a.nav-link', function(){
        let link = $(this).attr('href');
        link = link.replace(__BASE__+'/', '');
        if(link !== '#'){
            localStorage.setItem('last-link', link);
        }
    })
})

var modules = $("#filter-modules");
var schools = $("#filter-schools");
var branches = $("#filter-branches");
var branchGroup = $("#filter-branch-group");
var classLevels = $("#filter-classLevels");
var classes = $("#filter-classes");
var users = $("#filter-users");
var classTeachers = $("#filter-class-teachers");
var branchGroups = $("#filter-groups");
var subjects = $("#filter-subjects");
var subjectsInTimeTable = $("#filter-subjects-in-time-table");
var nonSubjects = $("#filter-non-subjects");
var students = $("#filter-students");
var countries = $("#filter-countries");
var cities = $("#filter-cities");
var examTerms = $("#filter-exam-term");
var feeTypes = $("#filter-fee-type");
var areas = $("#filter-areas");
var roles = $("#filter-roles");
var addFormErrors = $('#add-form-errors');
var addFormErrorsMessage = $('#add-form-error-message');
var resultsList = $('#results-list');
var submitButton = $('#submit-add-form-container');
var cancelButton = $('#hide-add-form-container');
var editHiddenField = $('#editHiddenField');
var filterFormTitle = $('#filter-form-title');
var dateFrom = $('#filter-date-from');
var dateTo = $('#filter-date-to');
var messageTypeObj = $('#filter-message-type');
var billingPeriod = $('#billing-period-filter');
var classRoom = $('#filter-class-room');
var activities = $("#filter-activities")
var weekDays =  $("#filter-week-day");
var instructor = $("#filter-instructor");
var timeZone = $('#filter-timezone');
var examTermTableData = null;
var nonExamTermTableData = null;
var overAllExamTermTableData = null;
var overAllExamTermAssessmentTableData = null;
var studentSubjectTable = null;
var studentNonSubjectTable = null;
var messageListTable = null;
var parentDetailTable = null;
var notesDetailTable = null;
var parentLinkedStudentTable = null;
var viewOtpDetailTable = null;
var studenDetailtTable = null;
var attendanceDataTable = null;
var subjectAttendanceDataTable = null;
var quizResultAddForm = null;
var academicResult = null;
var nonAcademicResult = null;
var overallResult = null;
var roleLinkedUserTable = null;
var termStatus = 0;

/* FOR EXAM RESULTS */
var subjectsList = $('#subjects-list');
var nonSubjectsList = $('#non-subjects-list');
/* END FOR EXAM RESULTS */

var optionalFields = {
    'quiz-results': ['studentID', 'subjectID'],
    'attendance': ['studentID'],
    'exam-term': ['branchID', 'classID', 'classLevelID'],
    'fee-type': ['feeTypeID'],
    'messages': ['studentID', 'classLevelID', 'classID', 'branchID', 'schoolID', 'areaID', 'cityID'],
    'daily-diary': ['subjectID']
};

$(document).ready(function(){
    if(__COMM_ACCESS__){
        getCommunicationNotification();
    }
});

function generateUUID() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

function getCommunicationNotification(){
    if(schoolID != '' && branchID != ''){
        $.ajax({
            url: herokuUrl + '/getCommsCount',
            type: 'POST',
            data: {schoolID:schoolID,branchID:branchID},
            dataType:'JSON',
            headers,
            success: function(data){
                if(typeof data.response.length != 'undefined' && data.response.length > 0 ){
                   $('.comm-notification').html(data.response[0].total);
                }
            },
            error: function(xhr){
                invokedFunctionArray.push('getCommunicationNotification');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        });
    }
}

/* CHECK IF TOKEN IS EXPIRED */
// setInterval(function () {
//     getToken();
// }, 30000);
/* CHECK IF TOKEN IS EXPIRED */

dateFrom.on('change', function (e) {
    dateFromVal = $(this).val();
    if(__ROUTE__ !== 'calendar-days' && __ROUTE__ !== 'attendance-reports' && __ROUTE__ !== 'exam-term' && __ROUTE__ !== 'academic-calendar' && __ROUTE__ !== 'settlement-report' && __ROUTE__ !== 'staff-attendance'){
        tableData.destroy();
    }

    if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
        window[routeListFunctions[__ROUTE__]]();
    }
});

dateTo.on('change', function () {
    dateToVal = $(this).val();
    if(__ROUTE__ !== 'calendar-days' && __ROUTE__ !== 'attendance-reports' && __ROUTE__ !== 'exam-term' && __ROUTE__ !== 'academic-calendar' && __ROUTE__ !== 'settlement-report' && __ROUTE__ !== 'staff-attendance'){
        tableData.destroy();
    }
    if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
        window[routeListFunctions[__ROUTE__]]();
    }
});

modules.on('change', function () {
    clearDependentFilters($(this).attr('id'));
    moduleID = $(this).val();
    if(moduleID !== ''){
        if (__ROUTE__ === 'role-access-rights'){
            if (moduleID !== '' && roleID !== ''){
                if (tableData) {
                    tableData.destroy();
                }
                if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                    window[routeListFunctions[__ROUTE__]]();
                }
            }
        }
    }
});

areas.on('change', function (){
    areaID = $(this).val();
    clearDependentFilters($(this).attr('id'));
    if(areaID !== ''){
        if(__ROUTE__ == 'messages' && schoolID != '' ){
            getAjaxSchoolBranches();
        }
        if (areaID !== '') {
            if (tableData && __ROUTE__ !== 'school-branches'){
                tableData.destroy();
            }
            if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});
function getFeeType(){
    $.ajax({
        url: herokuUrl+'/getFeeTypes',
        type: 'POST',
        headers,
        beforeSend: function(){
            feeTypeArray = [];
            $('.fee-type-filters').empty();
            $('#fee-type-filters-list').empty();
        },
        data: {
           schoolID: schoolID,
           branchID: branchID
        },
        success: function(res){
            response = parseResponse(res);
            feeTypeArray = response;
            if (response.length >= 1) {
                feeTpesResponse = response;
            }
            $.each(response, function (index, value) {
                if(__ROUTE__ == "student-fee-challan"){
                    if(value.isActive == '1' && value.isRecurring == '1'){
                        $('#fee-type-filters-list').append(`<li>${value.feeType}</li>`);
                    }

                }else{
                    if(__ROUTE__ == 'student-fee'){
                        if(value.isActive == '1' && value.isRecurring == '1'){
                            $('.fee-type-filters').append(`<option value="${value.feeTypeID}">${value.feeType}</option>`);
                        }else{
                            $('.fee-type-filters').append(`<option value="${value.feeTypeID}">${value.feeType}</option>`);
                        }
                    }
                }
            });

        },
        error: function(xhr){
            invokedFunctionArray.push('getFeeType');
            window.requestForToken(xhr)
            console.log(xhr);
        }
    });
}
schools.on('change', function () {
    classLevelID = '';
    classTeacherID = '';
    schoolID = $(this).val();
    clearDependentFilters($(this).attr('id'));
    if(schoolID !== ''){
        if( __ROUTE__ == 'student-fee-challan'){
            $('#download-csv').addClass('hidden')
            $('#unpublish-all-vouchers').addClass('hidden')
            $('#publish-all-vouchers').addClass('hidden')
            getFeeType();
        }

        if(__ROUTE__ == 'school-branches'){
            getSchoolBranches()
        }

        if(__ROUTE__ === 'users'){
            getAjaxSchoolBranches()
        }

        // if(__ROUTE__ !== 'subject-time-table'){
        //     getAjaxUsers();
        // }

        if(branches.length && (!idEditButtonClicked)){
            getAjaxSchoolBranches(schoolID);
        }

        if (users.length > 0 && __ROUTE__ !== 'subject-time-table'){
            getAjaxUsers();
        }
        if(__ROUTE__ !== 'classes' && __ROUTE__ !== 'subject-time-table'){
            if (classTeachers.length) {
                getAjaxTeachers(schoolID, branchID);
            }
        }

        if (roles.length && __ROUTE__ !== 'users') {
            getAjaxRoles();
        }
        if (__ROUTE__ == 'users') {
            if($('#filter-branches').length > 0){
                branchID = $('#filter-branches').val();
            }
            getAjaxRoles();
            getUserList();
        }
        if(schoolID !== '' && __ROUTE__ == 'roles'){
            getRoles();
        }

        if (__ROUTE__ === 'exam-term') {
            getExamTerm();
        }

        if (feeTypes.length){
            getAjaxFeeTypes();
        }

        if(__ROUTE__ !== 'assessments' && __ROUTE__ !== 'subjects' && __ROUTE__ !== 'non-subjects' && __ROUTE__ !== 'daily-diary' && __ROUTE__ !== 'quiz-results' && __ROUTE__ !== 'attendance-today' && __ROUTE__ !== 'role-access-rights' && __ROUTE__ !== 'time-table' && __ROUTE__ !== 'new-time-table' && __ROUTE__ !== 'attendance' && __ROUTE__ !== 'students' && __ROUTE__ !== 'users' && __ROUTE__ !== 'student-fee-challan' && __ROUTE__ !== 'parents' && __ROUTE__ !== 'billing-period' && __ROUTE__ !== 'registration-reports' && __ROUTE__ !== 'school-branches' && __ROUTE__ !== 'activities' && __ROUTE__ !== 'assign-activities' && __ROUTE__!=='roles' && __ROUTE__ !== 'class-levels' && __ROUTE__ !== 'exam-term' && __ROUTE__ !== 'attendance-reports' && __ROUTE__ !== 'class-rooms' && __ROUTE__ !== 'assign-subjects' && __ROUTE__ !== 'subject-attendance' && __ROUTE__ !== 'classes' && __ROUTE__ !== 'school-payment' && __ROUTE__ !== 'settlement-report' && __ROUTE__ !== 'staff-attendance' && __ROUTE__ !== 'form-management'){

            if (tableData && __ROUTE__!=='calendar-days') {
                tableData.destroy();
            }

            if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});

function clearDependentFilters(currentFilterID = 'filter-schools'){
    if(__ROUTE__ !== 'conference'){
        if(typeof dependentDropdowList[currentFilterID] !== 'undefined' && dependentDropdowList[currentFilterID].length > 0 ){
            dependentDropdowList[currentFilterID].forEach(function(value,index){
                if(__ROUTE__ === 'academic' && value === 'filter-subjects'){
                    $("#"+ value).multiselect('rebuild');
                }else{
                    $("#"+ value).empty();
                    $("#"+ value).find('option').remove();
                    $("#"+ value).multiselect('refresh');
                    $("#"+ value).multiselect('rebuild');
                }

            });
        }

        if(typeof dependentRecordListOnDropdown[__ROUTE__] !== 'undefined' && dependentRecordListOnDropdown[__ROUTE__].length > 0){
            if(dependentRecordListOnDropdown[__ROUTE__].indexOf(String(currentFilterID)) !== -1 ){
                if(tableData){
                  tableData
                    .clear()
                    .draw();
                }
            }
        }
    }
}

classTeachers.on('change', function () {
    classTeacherID = $(this).val();
    if(tableData && __ROUTE__ !== 'classes' && __ROUTE__ !== 'conference'){
        tableData.destroy();
    }

    if(classTeacherID !== ''){
        if(__ROUTE__ === 'subject-time-table'){
            getSubjectTimeTable();
        }else if(__ROUTE__ !== 'conference'){
            getClasses();
        }
        if(__ROUTE__ === 'subject-time-table')
            getSubjectTimeTable();
    }
})

activities.on('change',function(){
    activityID = $(this).val();
    clearDependentFilters($(this).attr('id'));
    if(activityID !== ''){
        getAssignedActivities();
    }
})

function getTimeZones(){
    $.ajax({
        url: herokuUrl+'/getTimezoneList',
        type: 'POST',
        headers,
        beforeSend(){
            timeZone.empty();
        },
        success: function (res) {
            response = parseResponse(res);
            var timeZoneAbbr = moment.tz.guess();
            response.forEach(function(value, index){
                timeZone.append(`
                    <option value="${value.Offset}" ${ value.tzName === timeZoneAbbr ? 'selected' : ''}>${value.tzName} (${value.Offset})</option>
                `);
            })
        },
        error: function (err) {

        }
    })
}

function getActiveClassRooms(){
    let data = {};

    if(tableData){
        tableData.destroy();
    }
    if(schoolID !== ''){
        data.schoolID = schoolID
    }

    if(branchID !== ''){
        data.branchID = branchID
    }

     $.ajax({
            url: herokuUrl+'/getActiveClassRoom',
            type: 'POST',
            headers,
            beforeSend: function(){
                showProgressBar('p2-class-room');
                classRoom.attr('disabled', true);
                classRoom.html('');
                classRoom.find('option').remove();
                classRoom.empty();
                classRoom.html(`<option value="">${messages.selectClassRoom}<option>`);
            },
            data,
            success: function(res){
                  response = parseResponse(res);
                  $.each(response, function (index, value) {
                      classRoom.append(`<option value="${value.roomID}">${value.roomName}</option>`);
                  });
                hideProgressBar('p2-class-room');

                  $('#filter-class-room option')
                  .filter(function() {
                      return $.trim(this.text).length == 0;
                  })
                  .remove();
                  classRoom.multiselect('rebuild');
                  },
            error: function(xhr){
                invokedFunctionArray.push('getActiveClassRooms');
                window.requestForToken(xhr)
                console.log(xhr);
            },
        })
}

branches.on('change', function () {
    if(__ROUTE__ === 'schools'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-branches').multiselect('selectAll');
            $('#filter-branches').multiselect('rebuild');
        }else{
            $('#filter-branches').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-branches').multiselect('rebuild');
        }
    }

    schoolBranchesArray = [];
    classTeacherID = '';
    classLevelID = '';
    branchID = $(this).val();
    clearDependentFilters($(this).attr('id'));
    if(branchID !== ''){
        if (__ROUTE__ === 'exam-term') {
            if(branches.length){
                branchID = $(this).val();
            }
            getExamTerm();
        }

        if(__ROUTE__ === 'form-management'){
            getFormList()
        }

        if(__ROUTE__ === 'subject-time-table'){
            getActiveClassRooms();
        }

        if(__ROUTE__ === 'school-payment'){
            if (tableData) {
                tableData.destroy();
            }
            $.LoadingOverlay("show");
            getSchoolOutstandingList()
        }

        if(__ROUTE__ === 'student-tag'){
            $.LoadingOverlay("show");
            getStudentTags();
        }

        if(__ROUTE__ === 'student-fee-challan'){
            getActiveBillingPeriod();
            // populateRecurringFee();
        }

        if(__ROUTE__ === 'class-rooms'){
            getClassRooms();
        }

        if(__ROUTE__ === 'class-levels'){
            if(branches.length){
                branchID = $(this).val();
            }
            getClassLevels();
        }

        if(activities.length){
            getAjaxActivities();
        }

        if(__ROUTE__ == 'activities'){
            getActivities();
        }

        if (users.length) {
            getAjaxUsers();
        }

        // if(__ROUTE__ === 'schools'){
        //     getContractCharges()
        // }

        if (examTerms.length) {
            getAjaxExamTerms();
        }

        if (classLevels.length) {
            getAjaxClassLevels();
        }


        if(__ROUTE__ === 'users' && $('#multiple').length){
            getAjaxRoles();
            getUserList();
        }

        if(__ROUTE__ === 'users'){
            getUserList();
        }

        if (roles.length) {
            getAjaxRoles();
        }

        if (__ROUTE__ === 'roles'){
            getRoles();
        }

        if(__ROUTE__ === 'classes'){
            getClasses()
            if (classTeachers.length) {
                getAjaxActiveTeachers(schoolID, branchID, classLevelID);
            }
        }

        // if(__ROUTE__ === 'conference') getAjaxTeachers(schoolID, branchID);


        if(__ROUTE__ !== 'classes' && __ROUTE__ !== 'subject-time-table'){
            if (classTeachers.length) {
                getAjaxTeachers(schoolID, branchID);
            }
        }

        if (__ROUTE__ !== 'quiz-results' && __ROUTE__ !== 'attendance' && __ROUTE__ !== 'attendance-today' && __ROUTE__ !== 'students' && __ROUTE__ !== 'daily-diary' && __ROUTE__ !== 'parents' && __ROUTE__ !== 'subjects' && __ROUTE__ !== 'non-subjects' && __ROUTE__ !== 'assessments' && __ROUTE__ !== 'role-access-rights' && __ROUTE__ !== 'time-table'  && __ROUTE__ !== 'new-time-table' && __ROUTE__ !== 'users' && __ROUTE__ !== 'student-fee-challan' && __ROUTE__ !== 'billing-period' && __ROUTE__ !== 'registration-reports' && __ROUTE__ !== 'exam-term' && __ROUTE__ !== 'activities'  && __ROUTE__ !== 'assign-activities' && __ROUTE__ !== 'non-subjects' && __ROUTE__ !== 'roles' && __ROUTE__ !== 'class-levels' && __ROUTE__ !== 'attendance-reports' && __ROUTE__ !== 'class-rooms' && __ROUTE__ !== 'assign-subjects' && __ROUTE__ !== 'classes' && __ROUTE__ !== 'subject-attendance' && __ROUTE__ !== 'calendar-days' && __ROUTE__ !== 'schools' && __ROUTE__ !== 'student-tag' && __ROUTE__ !== 'school-payment' && __ROUTE__ !== 'settlement-report' && __ROUTE__ !== 'staff-attendance' && __ROUTE__ !== 'conference' && __ROUTE__ !== 'otp-management' && __ROUTE__ !== 'form-management') {
            if (tableData) {
                tableData.destroy();
            }
            if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});

roles.on('change', function () {
    roleID = $(this).val();
    clearDependentFilters($(this).attr('id'));
    if(roleID !== ''){
        if (__ROUTE__ === 'role-access-rights'){
            if (tableData) {
                tableData.destroy();
            }
            if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});

classLevels.on('change', function () {
    classTeacherID = '';
    if(__ROUTE__ === 'messages' || __ROUTE__ === 'academic-calendar'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-classLevels').multiselect('deselectAll');
            $('#filter-classLevels').multiselect('select', 0);
            $('#filter-classLevels').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-classLevels').multiselect('rebuild');
        }else{
            $('#filter-classLevels').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-classLevels').multiselect('rebuild');
        }
    }else if(__ROUTE__ === 'form-management'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-classLevels').multiselect('deselectAll');
            $('#filter-classLevels').multiselect('select', 0);
            $('#filter-classLevels').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-classLevels').multiselect('rebuild');
        }else{
            $('#filter-classLevels').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-classLevels').multiselect('rebuild');
        }
    }

    classLevelID = $(this).val();
    messagesClassLevelArray = new Array();
    clearDependentFilters($(this).attr('id'));

    if(classLevelID !== ''){
        if(Array.isArray(classLevelID)){
            classLevelID = classLevelID.filter(function (el) {
                return el != null && el !== '' && el !== 0 && el !== '0';
              });
            classLevelID.forEach(function(value, index){
                if(messagesClassLevelArray.indexOf(value) == -1){
                    messagesClassLevelArray.push(value);
                }
            })
            if(__ROUTE__ == 'messages' && classLevelID.length == ''){
                $('#filter-classes').append(`<option value="0">${messages.allSection}</option>`)
                $('#filter-classes').multiselect('rebuild')
            }
            if(__ROUTE__ == 'academic-calendar' && classLevelID.length == ''){
                $('#filter-classes').append(`<option value="0">${messages.allSection}</option>`)
                $('#filter-classes').multiselect('rebuild')

            }
            if(__ROUTE__ == 'form-management' && classLevelID.length == ''){
                $('#filter-classes').append(`<option value="0">${messages.allSection}</option>`)
                $('#filter-classes').multiselect('rebuild')

            }
            classLevelID =  classLevelID.join(',');
        }

        // if(__ROUTE__ === 'form-management'){
        //     console.log(getFormList())
        // }

        if(__ROUTE__ === 'form-management'){
            getFormList()
        }

        if(classes.length && __ROUTE__ !== 'classes'){
            if(__ROUTE__ === 'academic-calendar'){
                getAcademicEvents();
            }
            if(Array.isArray(classLevelID)){
                if(classLevelID.length >= 1 && classLevelID != '0'){
                    classLevelID = classLevelID.join(',');
                }
            }else{
                if(Number(classLevelID) !== 0)
                    classLevelID =  classLevelID;
            }
            if((__ROUTE__ === 'messages' || __ROUTE__ === 'form-management') && ($('#filter-classLevels').find('option:first').prop('selected'))){
                messagesSelectedClassesArray = [];
                $('#filter-classes').multiselect('disable');
                $('#filter-students').multiselect('disable');
            }else{
                getAjaxClasses();
            }

        }

        if(__ROUTE__ === 'conference'){
            getConferrences();
        }

        if (examTerms.length) {
            getAjaxExamTerms();
        }

        if(__ROUTE__ === 'classes'){
            // getClasses()
            if (classTeachers.length) {
                getAjaxActiveTeachers(schoolID, branchID, classLevelID);
            }
        }

        if(classLevelID !== '' && __ROUTE__ !== 'nonacademic'){
            if (nonSubjects.length) {
                getAjaxNonSubjects();
            }
        }
        if(__ROUTE__ === 'attendance'){
            classID = $('#filter-classes').val();
            if(classID !== ''){
                getAttendance();
            }else{
                if(tableData){
                    tableData
                    .clear()
                    .draw();
                }
            }
        }

        if(__ROUTE__ === 'student-fee-challan'){
            getStudentFeeChallans()
        }

        // if(__ROUTE__ === 'promotions'){
        //     getOverallResult()
        // }
        if (classLevelID !== '') {
            if (__ROUTE__ !== 'assessments' && __ROUTE__ !== 'quiz-results' && __ROUTE__ !== 'time-table'  && __ROUTE__ !== 'new-time-table' && __ROUTE__ !== 'students' && __ROUTE__ !== 'parents' && __ROUTE__ !== 'student-fee-challan'  && __ROUTE__ !== 'conference'  ) {
                if (tableData && __ROUTE__ !== 'daily-diary' && __ROUTE__ !== 'billing-period' && __ROUTE__ !== 'registration-reports'  && __ROUTE__ !== 'assign-activities' && __ROUTE__ !== 'attendance' && __ROUTE__ !== 'non-subjects' && __ROUTE__ !== 'new-time-table' && __ROUTE__ !== 'attendance-reports' && __ROUTE__ !== 'assign-subjects' && __ROUTE__ !== 'subject-time-table' && __ROUTE__ !== 'classes' && __ROUTE__ !== 'subject-attendance' && __ROUTE__ !== 'academic-calendar' && __ROUTE__ !== 'conference' && __ROUTE__ !== 'form-management') {
                    tableData.destroy();
                }
                if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                    window[routeListFunctions[__ROUTE__]]();
                }
            }
        }


    }

});

classes.on('change', function () {
    if(__ROUTE__ === 'messages'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-classes').multiselect('deselectAll');
            $('#filter-classes').multiselect('select', 0);
            $('#filter-classes').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-classes').multiselect('rebuild');
        }else{
            $('#filter-classes').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-classes').multiselect('rebuild');
        }
    }else if(__ROUTE__ === 'daily-diary'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-classes').multiselect('deselectAll');
            $('#filter-classes').multiselect('select', '');
            $('#filter-classes').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-classes').multiselect('rebuild');
        }else{
            $('#filter-classes').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-classes').multiselect('rebuild');
        }
    }else if(__ROUTE__ === 'form-management'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-classes').multiselect('deselectAll');
            $('#filter-classes').multiselect('select', 0);
            $('#filter-classes').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-classes').multiselect('rebuild');
        }else{
            $('#filter-classes').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-classes').multiselect('rebuild');
        }
    }

    classID = $(this).val();
    messagesSelectedClassesArray = [];
    clearDependentFilters($(this).attr('id'));
    if(classID !== ''){
        if(Array.isArray(classID)){
            if(messagesClassLevelArray.length > 0){
                classID = classID.filter(function (el) {
                    return el != null && el !== '';
                });
            }else{
                classID = classID.filter(function (el) {
                    return el != null && el !== '' && el !== '0' && el !== 0 ;
                });
            }
            classID.forEach(function(value, index){
                if(messagesSelectedClassesArray.indexOf(String(value)) == -1){
                    messagesSelectedClassesArray.push(value)
                }
            })
                classID =  classID.join(',');
        }
        if(__ROUTE__ !== 'messages' && __ROUTE__ !== 'form-management'){
            getAjaxStudents();
        }

        if(__ROUTE__ === 'form-management'){
            getFormList()
        }

        if(__ROUTE__ === 'messages' && classID == 0){
            $('#filter-students').append(`<option value="0">${messages.allStudents}</option>`)
            $('#filter-students').multiselect('rebuild')
            if(__ROUTE__ === 'messages' && ($('#filter-classes').find('option:first').prop('selected'))){
                $('#filter-students').multiselect('disable');
            }else if(__ROUTE__ !== 'form-management'){
                getAjaxStudents();
            }
        }else{
            if(classID.length > 0 && __ROUTE__ === 'messages' && __ROUTE__ !== 'academic-calendar' && __ROUTE__ !== 'students' && __ROUTE__ !== 'assign-activities' && __ROUTE__ !== 'form-management'){
                getAjaxStudents();
            }else if(__ROUTE__ === 'academic-calendar'){
                getAcademicEvents()
            }
        }
        if(__ROUTE__ === 'form-management'){
            if(classID == 0){
                $('#filter-students').append(`<option value="0">${messages.allStudents}</option>`)
                $('#filter-students').multiselect('rebuild')
                $('#filter-students').multiselect('disable');
            }else if(classID != 0 && classID.length > 0){
                getAjaxStudents()
            }
        }

        if(__ROUTE__ === 'conference'){
            getConferrences();
        }

        if(__ROUTE__ === 'subject-time-table'){
            getAjaxActiveTeachers(schoolID, branchID, classLevelID);
        }

        if(__ROUTE__ !== 'messages' && __ROUTE__ !== 'parents' && __ROUTE__ !== 'daily-diary' && __ROUTE__ !== 'overall-exam' && __ROUTE__ !== 'student-fee-challan' && __ROUTE__ !== 'student-fee' && __ROUTE__ !== 'quiz-results' && __ROUTE__ !== 'conference' && __ROUTE__ !== 'assign-subjects' && __ROUTE__ !== 'assign-activities' && __ROUTE__ !== 'form-management'){
            if (students.length) {
                getAjaxStudents();
            }
        }

        // if (__ROUTE__ === 'assign-subjects'){
        //     getAjaxStudents();
        // }

        if(__ROUTE__ == 'new-time-table' && weekdayNo !== ''){
            weekdayNo = $("#filter-week-day option:selected").val();
            getTimeTable()
        }

        if(__ROUTE__ === 'student-fee'){
            getAjaxFeeTypes();
        }

        if (__ROUTE__ === 'academic' || __ROUTE__ === 'quiz-results' || __ROUTE__ === 'attendance' || __ROUTE__ === 'assign-subjects' || __ROUTE__ === 'subject-time-table') {
            if(user.isAdmin === 1){
                getAjaxSubjectsNonSubjectsList();
            }else{
                if(subjects.length)
                    getAjaxSubjects();
            }
        }

        if (__ROUTE__ === 'conference') {
            if(user.isAdmin !== 1){
                getAjaxSubjectsNonSubjectsList();
            }else{
                if(subjects.length)
                    getAjaxSubjects();
            }
        }

        if(__ROUTE__ === 'daily-diary'){
            getAjaxSubjectsNonSubjectsList();
        }

        if(__ROUTE__ === 'nonacademic'){
            getAjaxNonSubjects();
        }

        if(__ROUTE__ === 'student-fee-challan'){
            // getActiveBillingPeriod();
            getStudentFeeChallans()
            populateRecurringFee();
        }

        if (examTerms.length && __ROUTE__ !== 'promotions') {
            getAjaxExamTerms();
        }

        if(__ROUTE__ === 'promotions'){
            getOverallResult()
        }

        if (__ROUTE__ !== 'parents' && __ROUTE__ !== 'subjects' && __ROUTE__ !== 'student-fee-challan' && __ROUTE__ !== 'non-subjects' && __ROUTE__ !== 'new-time-table'  && __ROUTE__ !== 'conference') {
            if (tableData  && __ROUTE__ !== 'attendance' && __ROUTE__ !== 'daily-diary' && __ROUTE__ !== 'billing-period' && __ROUTE__ !== 'registration-reports' && __ROUTE__ !== 'assign-activities' && __ROUTE__ !== 'attendance-reports'  && __ROUTE__ !== 'subject-time-table' && __ROUTE__ !== 'assign-subjects' && __ROUTE__ !== 'classes' && __ROUTE__ !== 'subject-attendance' && __ROUTE__ !== 'academic-calendar' && __ROUTE__ !== 'conference' && __ROUTE__ !== 'form-management') {
                tableData.destroy();
            }
            if (typeof window[routeListFunctions[__ROUTE__]] === 'function'){
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});

messageTypeObj.on('change', function () {
    messageType = $(this).val();
    if (tableData) {
        tableData.destroy();
    }
    if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
        window[routeListFunctions[__ROUTE__]]();
    }
});

$('#filter-subjects-in-time-table').on('change',function(){
    timetableSubjectID = $(this).val();
    weekdayNo = $('option:selected', this).attr('data-weekday-no');
    if(__ROUTE__ === 'subject-attendance'){
        getStudentSubjectAttendance();
        let date = $('#attendanceDate').val();
        $.LoadingOverlay("show");
        getStudentsInTimetable();
        dateFromVal = $('#attendanceDate').val();
        dateToVal = $('#attendanceDate').val();
        getCalendarDays();
        setTimeout(() => {
            $.LoadingOverlay("hide");
        },1000)
    }

    if(__ROUTE__ === 'attendance'){
        getStudentSubjectAttendance();
        let date = $('#attendanceDate').val();
        $.LoadingOverlay("show");
        getStudentsInTimetable();
        dateFromVal = $('#attendanceDate').val();
        dateToVal = $('#attendanceDate').val();
        getCalendarDays();
        setTimeout(() => {
            $.LoadingOverlay("hide");
        },1000)
    }

    if(__ROUTE__ === 'assign-subjects'){
        getAssignedSubjects();
        hideAndShowAssignSubjectTable();
        setTimeout(() => {
            iterateAssignSubjectsStudentTable();
        }, 1500);
    }

    // getStudentsInTimetable();
});

subjects.on('change', function () {
    if(__ROUTE__ !== 'daily-diary' && __ROUTE__ !== 'conference'){
        clearDependentFilters($(this).attr('id'));
    }
    subjectID = $(this).val();
    if(subjectID !== ''){
        if(__ROUTE__ === 'attendance' || __ROUTE__ === 'assign-subjects'){
            getAjaxSubjectsInTimeTable();
        }

        if(__ROUTE__ === 'subject-time-table'){
            getSubjectTimeTable();
        }
        if(__ROUTE__ === 'assign-subjects'){
            getAssignedSubjects();
            hideAndShowAssignSubjectTable();
            setTimeout(() => {
                iterateAssignSubjectsStudentTable();
            }, 1500);

        }
        if(__ROUTE__ === 'quiz-results'){
            $('#quizDate').val('');
        }

        // if(__ROUTE__ === 'daily-diary'){
        //     getDailyDiarySummary();
        // }

        if ((tableData) && (__ROUTE__ !== 'daily-diary'  && __ROUTE__ !== 'subject-time-table' && __ROUTE__ !== 'assign-subjects' && __ROUTE__ !== 'attendance' && __ROUTE__ !== 'conference')) {
            tableData.destroy();
        }

        if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
            window[routeListFunctions[__ROUTE__]]();
        }
    }
});

nonSubjects.on('change', function () {
    nonSubjectID = $(this).val();
});

students.on('change', function () {
    if(__ROUTE__ === 'messages'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-students').multiselect('deselectAll');
            $('#filter-students').multiselect('select', 0);

            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-students').multiselect('rebuild');
        }else{
            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-students').multiselect('rebuild');
        }
    }else if(__ROUTE__ === 'conference'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-students').multiselect('deselectAll');
            $('#filter-students').multiselect('select', 0);

            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-students').multiselect('rebuild');
        }else{
            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-students').multiselect('rebuild');
        }
    }else if(__ROUTE__ === 'daily-diary'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-students').multiselect('deselectAll');
            $('#filter-students').multiselect('select', '');

            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-students').multiselect('rebuild');
        }else{
            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-students').multiselect('rebuild');
        }
    }else if(__ROUTE__ === 'form-management'){
        if($(this).find('option:first').prop('selected')){
            $('#filter-students').multiselect('deselectAll');
            $('#filter-students').multiselect('select', 0);

            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : true,
            });
            $('#filter-students').multiselect('rebuild');
        }else{
            $('#filter-students').find('option').not('option:first').attr({
                'disabled' : false,
            });
            $('#filter-students').multiselect('rebuild');
        }
    }
    clearDependentFilters($(this).attr('id'));
    messagesSelectedStudentsArray = [];
    studentID = $(this).val();
    if(studentID !== ''){
        if(activityID !== ''){
            getAssignedActivities();
        }

        // if(__ROUTE__ === 'daily-diary'){
        //     getDailyDiarySummary();
        // }

        if(__ROUTE__ === 'student-fee-challan'){
            getStudentFeeChallans()
        }

        if(Array.isArray(studentID)){
            studentID = studentID.filter(function (el) {
                return el != null && el !== '';
              });

            studentID.forEach(function(value, index){
                if(messagesSelectedStudentsArray.indexOf(value) == -1  && value != ''){
                    messagesSelectedStudentsArray.push(value);
                }
            })
            studentID =  studentID.join(',');
        }

        if(__ROUTE__ === 'assign-subjects'){
            getAssignedSubjects();
            hideAndShowAssignSubjectTable();
            setTimeout(() => {
                iterateAssignSubjectsStudentTable();
            }, 1500);

        }

        if (__ROUTE__ !== 'parents' && __ROUTE__ !== 'student-fee-challan' && __ROUTE__ !== 'assign-activities' && __ROUTE__ !== 'daily-diary' &&  __ROUTE__ !== 'conference' && __ROUTE__ !== 'assign-subjects' &&  __ROUTE__ !== 'form-management') {
            if (tableData) {
                tableData.destroy();
            }
            if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});

weekDays.on('change',function(){
    clearDependentFilters($(this).attr('id'));
    weekdayNo = $(this).val();
    if(weekdayNo !== ''){
        if(Array.isArray(weekdayNo)){
            weekdayNo = weekdayNo.filter(function (el) {
                return el != null && el !== '';
              });
              weekdayNo =  weekdayNo.join(',');
        }
        if(__ROUTE__ === 'assign-activities' && activityID !== '' ){
            getAssignedActivities();
        }

        if(__ROUTE__ === 'new-time-table'){
            newTimeTableIterateTable();
        }

    }
})

examTerms.on('change', function () {
    examTermID = $(this).val();
    if(examTermID !== ''){
        if (tableData) {
            tableData.destroy();
        }
        // if(__ROUTE__ === 'nonacademic'){
        //     getAjaxNonSubjects();
        // }
        if(__ROUTE__ === 'overall-exam'){
            getAjaxStudents();
        }
        if(nonSubjectResultList.length){
            getNonSubjectResult();
        }
        if(subjectResultList.length){
            getSubjectResult();
        }
        // if(__ROUTE__ === 'academic'){
        //     getAjaxSubjects();
        // }
        if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
            window[routeListFunctions[__ROUTE__]]();
        }
    }
});

countries.on('change', function () {
    clearDependentFilters($(this).attr('id'));
    countryID = $(this).val();
    if(countryID !== ''){
        if (cities.length) {
            getAjaxCities(countryID);
        }

        if (__ROUTE__ !== 'areas' && __ROUTE__ !== 'school-branches' && __ROUTE__ !== 'parents') {
            if (tableData) {
                tableData.destroy();
            }
            if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});

cities.on('change', function () {
    cityID = $(this).val();
    clearDependentFilters($(this).attr('id'));
    if(cityID !== ''){
        if (areas.length) {
            getAjaxAreas();
        }

        if (__ROUTE__ !== 'school-branches' && __ROUTE__ !== 'parents') {
            if (tableData) {
                tableData.destroy();
            }
            if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                window[routeListFunctions[__ROUTE__]]();
            }
        }
    }
});

//Get API Token
function requestForToken(xhr){
    if (xhr.status === 503 || xhr.status === 511) {
        window.__REPROCESS__ = true;
        window.__RETRIES__ = window.__RETRIES__ + 1;
        // localStorage.removeItem('apiToken');
        window.getToken();
    }
}

function getToken(){
    var utcFormat = moment.utc();
    let seconds = utcFormat.format('X')
    let token = localStorage.getItem('apiToken');
    if (token !== null) {
        token = JSON.parse(token);
        let diff = token.expiry - seconds;
        if (diff < 30) {
            updateTockenOnExpiry();
        }
    } else {
        window.location.replace(__BASE__ + '/auth/login');
    }
}
function updateTockenOnExpiry(){
    if(window.__TOKPROCESS__ === true){
        $.ajax({
            url: apiUrl + '/updateApiToken',
            beforeSend: function(){
                window.__TOKPROCESS__ = false;
            },
            method: 'GET',
            data: {
            },
            success: function (res) {
                if (res.response) {
                    window.token = res.response;
                    window.headers = {
                        'Authorization': 'Bearer ' + res.response.response,
                        //'Content-Type' : 'application/json',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    };
                    localStorage.removeItem('apiToken');
                    localStorage.setItem('apiToken', JSON.stringify(res.response));
                    window.__TOKPROCESS__ = true;
                    if (window.__REPROCESS__ === true ) {
                        if(window.__RETRIES__ !== 0){
                            let retriesArr = invokedFunctionArray.slice((0 - window.__RETRIES__));
                            for (let t = 0; t < retriesArr.length; t++) {
                                window[retriesArr[t]]();
                            }
                            window.__RETRIES__ = 0;
                        }
                    }
                } else {
                    window.location.replace(__BASE__ + '/auth/login');
                }
            },
            error: function (err) {

                window.requestForToken(err)
                $("#error-message").show(messages.sorryThereIsSomethingWrongPleaseTryAgain);
            }
        })
    }
}

//Parse laravel validation errors
function parseValidationErrors(xhr) {
    var errors = xhr.responseJSON.errors;
    $.each(errors, function (index, value) {
        var elem = $('[name=' + index + ']');
        var parent = elem.parent().first();
        parent.addClass('has-error');
        $('<span class="help-block">' + value + '</span>').insertAfter(elem);
    });
    return false;
}

//show add/edit form container
function showAddFormContainer() {
    if(__ROUTE__ === 'conference' && classID === ''){
        swal(
            messages.pleaseSelectSection,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    if(__ROUTE__ === 'calendar-days'){
        if(academicEventCheck == '0'){
            $('.multipleDaysDiv').removeClass('hidden');
        }
    }
        $("#add-form-container").slideDown();
        filterFormTitle.text(messages.create).css({
            'color': '#2fa7ff'
        });
    hideResultsList();
}

//hide add/edit form container
function hideAddFormContainer() {
    if(__ROUTE__ === 'calendar-days'){
        $('.endDateDiv').addClass('hidden');
    }
        $("#add-form-container").slideUp();
        filterFormTitle.text(messages.filters).css({
            'color': 'black'
        });
    showResultsList();
}

//hide filters progress bar
function hideProgressBar(id) {
    $("#" + id).addClass('hidden');
}

//Reset Error/Success Message form
function resetMessages() {
    addFormErrors.empty();
    addFormErrorsMessage.html('');
}

//show filters progress bar
function showProgressBar(id) {
    $("#" + id).removeClass('hidden');
}

function showResultsList() {
    resultsList.slideDown();
}

function hideResultsList() {
    resultsList.slideUp();
}

/* FITLERS AJAX START */
function getAjaxModules() {
    $.ajax({
        url: apiUrl + '/modules',
        method: 'GET',
        headers,
        data,
        dataType: 'json',
        beforeSend: function () {
            modules.empty();
            modules.append(`<option value="">${messages.selectModule}</option>`);
            modules.attr('disabled', true);
            showProgressBar('p2-modules');
        },
        success: function (res) {
            if (!res.error) {
                modules.removeAttr('disabled');
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                        modules.append(`<option value="${value.moduleID}" data-parent="${value.isParent}">${value.moduleName}</option>`);
                    });

                    if(__ROUTE__ === 'role-access-rights'){
                        res.data.forEach((modul, index) => {
                            if(modul.isParent == 1  ){
                                modul.childrens = new Array();
                                __MODULES__[modul.moduleURL] = modul;
                            }
                        });
                        res.data.forEach((modul, index) => {
                            if(modul.isParent == 0 ){
                                __MODULES__[modul.parentURL].childrens.push(modul);
                            }
                        });
                    }
                    hideProgressBar('p2-modules');
                } else {
                    modules.append(`<option value="">${messages.noModuleFound}</option>`);
                    modules.attr('disabled', true);
                    hideProgressBar('p2-modules');
                }
            } else {
                modules.attr('disabled');
                hideProgressBar('p2-modules');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxModules');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-modules');
        }
    }).done(function(){
        if(__ROUTE__ === 'role-access-rights'){
            // iterateModulesArray();
        }
    });
}

function getAjaxSchools() {
    if(__SCHOOLS__ == '1'){
        $.ajax({
            url: apiUrl + '/schools',
            method: 'GET',
            headers,
            data,
            dataType: 'json',
            beforeSend: function (){
                schools.empty();
                if(__ROUTE__ == 'messages'){
                    schools.append(`<option value="">${messages.allSchool}</option>`);
                }else{
                    schools.append(`<option value="">${messages.selectSchool}</option>`);
                }
                schools.attr('disabled', true);
                showProgressBar('p2-schools');
            },
            success: function (res) {
                if (!res.error) {
                    schools.removeAttr('disabled');
                    if (res.data.length >= 1) {
                        $.each(res.data, function (index, value) {
                            if(value.isActive === 1){
                                schools.append(`<option value="${value.schoolID}">${value.schoolName}</option>`);
                            }
                        });
                        $('#filter-schools').multiselect('rebuild')

                        hideProgressBar('p2-schools');
                    } else {
                        schools.append(`<option value="">${messages.noSchoolFound}</option>`);
                        schools.attr('disabled', true);
                        hideProgressBar('p2-schools');
                    }
                } else {
                    schools.attr('disabled');
                    hideProgressBar('p2-schools');
                }
            },
            error: function (xhr) {
                invokedFunctionArray.push('getAjaxSchools');
                window.requestForToken(xhr);
                hideProgressBar('p2-schools');
            }
        }).done(function () {
            if (schoolID !== '') {
                if (__ROUTE__ === 'fee-type' && __SCHOOLS__ == '') {
                    if (tableData) {
                        tableData.destroy();
                    }
                    if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                        window[routeListFunctions[__ROUTE__]]();
                    }
                }
            }
            //console.log('SCHOOLS COMPLETED');
        });
    }else if(__BRANCHES__ == '1'){
        getAjaxSchoolBranches();
    }else{
        if(classLevels.length){
            getAjaxClassLevels();
        }
        if(classes.length && classID !== ''){
           if(__ROUTE__ != 'student-fee-challan'){
                getAjaxClasses();
           }
        }
        if(classTeachers.length){
            getAjaxTeachers(schoolID, branchID);
        }
        if(roles.length){
            getAjaxRoles();
        }
        if(examTerms.length && __ROUTE__ !== 'academic'){
            getAjaxExamTerms();
        }

        if(__SCHOOLS__ === '' && __BRANCHES__ === ''){

            if(__ROUTE__ === 'class-levels' || __ROUTE__ === 'exam-term' || __ROUTE__ === 'roles' || __ROUTE__ === 'fee-type' || __ROUTE__ === 'comm-requests'){
                if (tableData) {
                    tableData.destroy();
                }
                if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                    window[routeListFunctions[__ROUTE__]]();
                }
            }
        }
    }
}

function getAjaxRoles() {
    $.ajax({
        url: apiUrl + '/roles',
        method: 'GET',
        headers,
        data: {
            schoolID
            // branchID
        },
        dataType: 'json',
        beforeSend: function () {
            $('.select2-search__field').attr('placeholder', messages.selectRole);
            roles.html('');
            roles.find('option').remove();
            roles.empty();
            $('.users-role-filters').empty();
            roles.append(`<option value="">${messages.selectRole}</option>`);
            $('.users-role-filters').append(`<option value="">${messages.selectRole}</option>`);
            roles.attr('disabled', true);
            $(".users-role-filters").attr('disabled', true);
            showProgressBar('p2-roles');
        },
        success: function (res) {
            if (!res.error) {
                roles.removeAttr('disabled');
                //roleID = res.data[0].roleID;
                $(".users-role-filters").removeAttr('disabled');
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                        roles.append(`<option value="${value.roleID}">${value.roleName}</option>`);
                    });
                    $.each(res.data, function (index, value){
                        $(".users-role-filters").append($("<option ${value.roleID == roleID ? 'selected' : ''}></option>").val(value.roleID).html(value.roleName));

                    });
                    if( __ROUTE__ == 'users' ){
                        setSeletedrolesList();
                    }

                    $('.select2-search__field').attr('placeholder', messages.selectRole);
                    roles.multiselect('rebuild');
                    hideProgressBar('p2-roles');
                } else {
                    // roles.append(`<option value="">No Role Found</option>`);
                    roles.attr('disabled', true);
                    hideProgressBar('p2-roles');
                }
            } else {
                roles.attr('disabled');
                hideProgressBar('p2-roles');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxRoles');
            window.requestForToken(err)
            hideProgressBar('p2-roles');
        }
    })
}

function getAjaxSchoolBranches() {
    var data = {};
    var modalBranches = $('#filter-branches-branches')
    data.schoolID = schoolID;
    if(__ROUTE__ == 'messages'){
        data.areaID = areaID ;
    }
    $.ajax({
        url: apiUrl + '/branches',
        method: 'GET',
        beforeSend: function () {

            modalBranches.empty();
            modalBranches.find('option').remove();
            branches.empty();
            branches.find('option').remove();
            if(__ROUTE__ == 'messages'){
                branches.html(`<option value="0">${messages.allBranch}</option>`);
            }
            // else if(__ROUTE__ == 'schools'){
            //     branches.html(`<option value="">${messages.allBranch}</option>`);
            // }
            else{
                branches.html(`<option value="">${messages.selectBranch}</option>`);
                modalBranches.html(`<option value="">${messages.selectBranch}</option>`);;
            }
            if(__ROUTE__ == 'registration-reports'){
                branches.empty();
            }
            modalBranches.multiselect('rebuild')
            branches.multiselect('rebuild')
            branches.attr('disabled', true);
            showProgressBar('p2-branches');
        },
        data,
        success: function (res) {
            if (!res.error) {
                branches.removeAttr('disabled');
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                        branches.append(`<option value="${value.branchID}">${value.branchName}</option>`);
                        modalBranches.append(`<option value="${value.branchID}">${value.branchName}</option>`);
                    });
                    modalBranches.multiselect('rebuild')
                    branches.multiselect('rebuild')
                    if(__BRANCHES__ === '1' && __SCHOOLS__ !== '1'){

                        if(__ROUTE__ == 'student-fee' || __ROUTE__ == 'billing-period'){
                            getAjaxFeeTypes();
                        }

                        // if(schoolID != ''){
                        //     if(__ROUTE__ == 'student-fee-challan'){
                        //         getActiveBillingPeriod();
                        //     }
                        // }

                        if(classLevels.length){
                            getAjaxClassLevels();
                        }
                        if(classes.length){
                            getAjaxClasses();
                        }
                        if(classTeachers.length){
                            getAjaxTeachers(schoolID, branchID);
                        }
                        if(roles.length){
                            getAjaxRoles();
                        }
                    }

                    hideProgressBar('p2-branches');
                } else {
                    hideProgressBar('p2-branches');
                }
            } else {
                branches.attr('disabled');
                hideProgressBar('p2-branches');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxSchoolBranches');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-branches');
        }
    })
}

function getAjaxBranchGroup() {
    var data = {};
    data.schoolID = schoolID;
    $.ajax({
        url: apiUrl + '/branch-group',
        method: 'GET',
        beforeSend: function () {
            branchGroup.empty();
            branchGroup.find('option').remove();
            if(__ROUTE__ == 'messages'){
                branchGroup.html(`<option value="0">${messages.allGroup}</option>`);
            }else{
                branchGroup.html(`<option value="">${messages.selectGroup}</option>`);
            }
            if(__ROUTE__ == 'registration-reports'){
                branchGroup.empty();
            }
            branchGroup.multiselect('rebuild')
            branchGroup.attr('disabled', true);
            showProgressBar('p2-branchGroup');
        },
        data,
        success: function (res) {
            if (!res.error) {
                branchGroup.removeAttr('disabled');
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                        branchGroup.append(`<option value="${value.branchGroupID}">${value.groupName}</option>`);
                    });
                    branchGroup.multiselect('rebuild')
                    if(__BRANCHES__ === '1' && __SCHOOLS__ !== '1'){

                        if(__ROUTE__ == 'student-fee' || __ROUTE__ == 'billing-period'){
                            getAjaxFeeTypes();
                        }

                        if(classLevels.length){
                            getAjaxClassLevels();
                        }
                        if(classes.length){
                            getAjaxClasses();
                        }
                        if(classTeachers.length){
                            getAjaxTeachers(schoolID, branchID);
                        }
                        if(roles.length){
                            getAjaxRoles();
                        }
                    }

                    hideProgressBar('p2-branchGroup');
                } else {
                    hideProgressBar('p2-branchGroup');
                }
            } else {
                branchGroup.attr('disabled');
                hideProgressBar('p2-branchGroup');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxBranchGroup');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-branchGroup');
        }
    })
}

function getActiveBillingPeriod(){
  $.ajax({
      url: herokuUrl+'/getActiveBillingPeriod',
      type: 'POST',
      headers,
      beforeSend: function(){
        showProgressBar('p2-billing-period');
        billingPeriod.attr('disabled', true);
        billingPeriod.html('');
        billingPeriod.find('option').remove();
        billingPeriod.empty();
        billingPeriod.html(`<option value="">${messages.selectBillingPeriod}<option>`);
      },
      data: {
          schoolID: schoolID,
          branchID : branchID
      },
      success: function(res){
            billingPeriod.removeAttr('disabled');
            hideProgressBar('p2-billing-period');
            response = parseResponse(res);
            $.each(response, function (index, value) {
                billingPeriod.append(`<option value="${value.billingPeriodID}">${value.periodName}</option>`);
            });
            $('#billing-period-filter option')
            .filter(function() {
                return $.trim(this.text).length == 0;
            })
            .remove();
            billingPeriod.multiselect('rebuild');
            },

      error: function(xhr){
        invokedFunctionArray.push('getActiveBillingPeriod');
        window.requestForToken(xhr)
        billingPeriod.removeAttr('disabled');
        hideProgressBar('p2-billing-period');
        console.log(xhr);
      }
  });
}

function getAjaxClasses() {
    if(schoolID === undefined){
        schoolID = user.schoolID;
    }
    let data = {
        schoolID: schoolID,
        branchID: branchID
    }

    if(classLevelID !== ''){
        data.classLevelID = classLevelID;
    }

    $.ajax({
        url: apiUrl + '/classes',
        method: 'GET',
        beforeSend: function () {
            // $('#filter-classes').multiselect('enable');
            classes.find('option').remove();
            classes.empty();
            if(__ROUTE__ == 'messages' || __ROUTE__ === 'academic-calendar' || __ROUTE__ === 'form-management'){
                classes.append(`<option value="0">${messages.allSection}</option>`);
            }else{
                classes.append(`<option value="">${messages.selectSection}</option>`);
            }
            classes.multiselect('refresh');
            classes.multiselect('rebuild');
            classes.attr('disabled', true);
            showProgressBar('p2-classes');
        },
        data: data,
        success: function (res) {
            if (!res.error) {
                classes.removeAttr('disabled');
                if (res.data.length >= 1) {
                    var classAliasArray  = [];
                    var result = Array.isArray(res.data) ? res.data : []

                    if(__ROUTE__ == 'daily-diary' || __ROUTE__ == 'messages' || __ROUTE__ === 'academic-calendar' || __ROUTE__ === 'form-management'){
                        messagesClassArray = [];
                        var finalArray = [];
                        result.forEach((res, index) => {
                            if(classAliasArray.indexOf(res.classAlias) == -1){
                                classAliasArray.push(res.classAlias);
                            }
                            let position = messagesClassLevelArray.indexOf(String(res.classLevelID));
                            if(position >= 0 ){
                                messagesClassArray[messagesClassLevelArray[position]] = new Array();
                            }
                        });
                        result.forEach((res, index) => {
                            let position = messagesClassLevelArray.indexOf(String(res.classLevelID));
                            if(position >= 0 ){
                                messagesClassArray[messagesClassLevelArray[position]].push(res);
                            }
                        });
                        for(var i = 0; i < classAliasArray.length; i++ ){
                            var data = {};
                            data['alias'] = classAliasArray[i];
                            data['sections'] = [];
                            finalArray.push(data);
                        }
                        for(var i = 0; i < finalArray.length; i++ ){
                            result.forEach((res, index) => {
                                if(res.classAlias == finalArray[i].alias){
                                    finalArray[i].sections.push(res);
                                }
                            });
                        }
                        $.each(finalArray, function(i, optgroups) {
                            if(optgroups.sections.length > 0){
                                var $optgroup = $("<optgroup>", {
                                    label: messages.class+' - ' +optgroups.alias
                                });
                            }
                            $optgroup.appendTo('#filter-classes');
                            $.each(optgroups.sections, function(groupName, options) {
                                var $option = $("<option>", {
                                    text: `${options.classAlias}-${options.classSection}`,
                                    value: options.classID,
                                    classLevelid: options.classLevelID
                                });
                                $option.appendTo($optgroup);
                            });
                        });
                    }else{
                        $.each(res.data, function (index, value) {
                            classes.append(`<option value="${value.classID}">${value.classAlias +' - '+ value.classSection }</option>`);
                        });
                    }

                    if(__ROUTE__ === 'students'){
                        $("option[value="+studentClassSectionID+"]", $('#filter-classes')).prop("selected", true);
                        $('#filter-classes').multiselect('refresh');
                    }

                    classes.multiselect('rebuild');
                    if(schoolID != '' ){
                        if(__ROUTE__ == 'billing-period'){
                            getAjaxFeeTypes();
                        }
                    }
                    $('.multiselect-container').addClass('w-100')
                    $('.multiselect-container').css({
                        'max-height':'300px',
                        'overflow-y':'scroll'
                    })
                    hideProgressBar('p2-classes');
                } else {
                    hideProgressBar('p2-classes');
                }

            } else {
                classes.attr('disabled');
                hideProgressBar('p2-classes');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxClasses');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-classes');
        }
    }).done(function () {
        if (classID !== '') {
            if (__ROUTE__ === 'assessments') {
                if (tableData) {
                    tableData.destroy();
                }
                if (typeof window[routeListFunctions[__ROUTE__]] === 'function') {
                    window[routeListFunctions[__ROUTE__]]();
                }
            }
        }
    })
}

function getAjaxClassLevels() {
    data = {}
    data.branchID = branchID
    if(schoolID !== 0)
        data.schoolID = schoolID
    $.ajax({
        url: apiUrl + '/classLevels',
        method: 'GET',
        beforeSend: function () {

            if(__ROUTE__ == 'daily-diary' || __ROUTE__ == 'messages' || __ROUTE__ === 'form-management'){
                classLevels.find('option').remove();
            }else{
                classLevels.empty();
            }
            classLevels.find('option').remove();
            if(__ROUTE__ == 'messages' || __ROUTE__ == 'academic-calendar' || __ROUTE__ === 'form-management'){
                classLevels.append(`<option value="0">${messages.allClass}</option>`);
                classLevels.multiselect('refresh');
            }else{
                classLevels.append(`<option value="">${messages.selectClass}</option>`);
            }

            classLevels.attr('disabled', true);
            showProgressBar('p2-classLevels');
        },
        data: data,
        success: function (res) {
            if (!res.error) {
                classLevels.removeAttr('disabled');
                if (res.data.length >= 1) {
                    //classLevelID = res.data[0].classLevelID;
                    // messageUserClassLevels = []
                    allClasses = [];
                    $.each(res.data, function (index, value) {
                        allClasses.push(value)
                        classLevels.append(`<option value="${value.classLevelID}" data-assesstment="${value.assessmentOnly}" data-isSubjectLevelAttendance="${value.isSubjectLevelAttendance}">${value.classAlias }</option>`);
                        // messageUserClassLevels.push(value.classLevelID)
                    });
                    classLevels.multiselect('rebuild');
                    if(schoolID != '' ){
                        if(__ROUTE__ == 'billing-period'){
                            getAjaxFeeTypes();
                        }
                        // if(__ROUTE__ == 'student-fee-challan'){
                        //     getActiveBillingPeriod();
                        // }
                    }
                    hideProgressBar('p2-classLevels');
                } else {
                    hideProgressBar('p2-classLevels');
                }
            } else {
                classLevels.attr('disabled');
                hideProgressBar('p2-classLevels');
            }
            $('.multiselect-container').addClass('w-100')
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxClassLevels');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-classLevels');
        }
    })
}

function getAjaxActiveTeachers(schoolID = null, branchID = null) {
    let data = {};
    data.schoolID = schoolID;
    if (branchID) {
        data.branchID = branchID
    }

    $.ajax({
        url: herokuUrl+'/getActiveUserList',
        type: 'POST',
        headers,
        beforeSend: function () {
            classTeachers.empty();
            classTeachers.append(`<option value="">${messages.selectTeacher}</option>`);
            classTeachers.attr('disabled', true);
            showProgressBar('p2-class-teachers');
        },
        data: {
            schoolID,
            branchID
        },
        success: function (res) {
            if (!res.error) {
                classTeachers.removeAttr('disabled');
                if (res.response.length >= 1) {
                    $.each(res.response, function (index, value) {
                        classTeachers.append(`<option value="${value.userID}">${value.fullName}</option>`);
                    })
                    hideProgressBar('p2-class-teachers');
                    classTeachers.multiselect('rebuild')
                } else {
                    hideProgressBar('p2-class-teachers');
                }
            } else {
                classTeachers.attr('disabled');
                hideProgressBar('p2-class-teachers');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxTeachers');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-class-teachers');
        }
    })
}


function getAjaxTeachers(schoolID = null, branchID = null) {
    let data = {};
    data.schoolID = schoolID;
    if (branchID) {
        data.branchID = branchID
    }

    $.ajax({
        url: apiUrl + '/users',
        method: 'GET',
        beforeSend: function () {
            classTeachers.empty();
            instructor.empty();
            instructor.append(`<option value="">${messages.selectInstructor}</option>`);
            classTeachers.append(`<option value="">${messages.selectTeacher}</option>`);
            classTeachers.attr('disabled', true);
            showProgressBar('p2-class-teachers');
        },
        data: {
            schoolID,
            branchID
        },
        success: function (res) {
            if (!res.error) {
                classTeachers.removeAttr('disabled');
                if (res.data.length >= 1) {
                    $.each(res.data[0].userData, function (index, value) {
                        if(value.isActive === 1){
                            classTeachers.append(`<option value="${value.userID}" data-name="${value.fullName}">${value.fullName}</option>`);
                            instructor.append(`<option value="${value.userID}" data-name="${value.fullName}">${value.fullName}</option>`)
                        }
                    })
                    hideProgressBar('p2-class-teachers');
                    classTeachers.multiselect('rebuild')
                } else {
                    hideProgressBar('p2-class-teachers');
                }
            } else {
                classTeachers.attr('disabled');
                hideProgressBar('p2-class-teachers');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxTeachers');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-class-teachers');
        }
    })
}

function getAjaxSubjectsInTimeTable() {
    data = {};
    if(branchID !== ''){
        data.branchID = branchID;
    }

    if(subjectID !== ''){
        data.subjectID = subjectID;
    }

    if(user.isAdmin == '0'){
        data.userID = user.userID;
    }

    if($('#attendanceDate').val() !== ''){
        data.attendanceDate = $('#attendanceDate').val();
    }

    // if(weekdayNo !== ''){
    //     data.weekdayNo = weekdayNo;
    // }

    $.ajax({
        url: apiUrl + '/subjectsInTimeTable',
        method: 'GET',
        beforeSend: function () {
            subjectsInTimeTable.empty();
            subjectsInTimeTable.append(`<option value="">${messages.selectSubjectTimeTable}</option>`);
            subjectsInTimeTable.multiselect('rebuild')
            subjectsInTimeTable.attr('disabled', true);
            showProgressBar('p2-subjectsInTimeTable');
        },
        data,
        success: function (res) {
            if (!res.error) {
                subjectsInTimeTable.removeAttr('disabled');
                if (res.data.length >= 1) {
                    // userID = res.data[0].userID;
                    // var result = typeof res.data[0].userData !== 'undefined' ? res.data[0].userData : [] ;
                    // if(__ROUTE__ === 'activities'){
                        $.each(res.data, function (index, value) {
                            subjectsInTimeTable.append(`<option value="${value.timetableSubjectID}" data-weekday-no="${value.weekdayNo}" >${value.fullName+' ('+value.weekDayName+' '+moment(value.startTime,'h:mm:ss').format('h:mm')+'-'+moment(value.endTime,'h:mm:ss').format('h:mm')+')'}</option>`);
                        })
                    // }else{
                    //     $.each(res.data, function (index, value) {
                    //         $("#classTeacher").append(`<option value="${value.userID}">${value.userName}</option>`);
                    //     })
                    // }
                    hideProgressBar('p2-subjectsInTimeTable');
                    subjectsInTimeTable.multiselect('rebuild');
                } else {
                    hideProgressBar('p2-subjectsInTimeTable');
                }
            } else {
                users.attr('disabled');
                hideProgressBar('p2-subjectsInTimeTable');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxSubjectsInTimeTable');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-subjectsInTimeTable');
        }
    })
}

function getAjaxUsers() {

    $.ajax({
        url: apiUrl + '/users',
        method: 'GET',
        beforeSend: function () {
            users.empty();
            users.append(`<option value="">${messages.selectUser}</option>`);
            users.attr('disabled', true);
            showProgressBar('p2-users');
        },
        data: {
            schoolID,
            branchID
        },
        success: function (res) {
            if (!res.error) {
                users.removeAttr('disabled');
                if (res.data.length >= 1) {
                    userID = res.data[0].userID;
                    var result = typeof res.data[0].userData !== 'undefined' ? res.data[0].userData : [] ;
                    if(__ROUTE__ === 'activities' || __ROUTE__ === 'conference'  ){
                        $.each(result, function (index, value) {
                            $("#filter-users").append(`<option value="${value.userID}">${value.fullName}</option>`);
                        })
                    }else{
                        $.each(res.data, function (index, value) {
                            $("#classTeacher").append(`<option value="${value.userID}">${value.userName}</option>`);
                        })
                    }
                    hideProgressBar('p2-users');
                    $("#filter-users").multiselect('rebuild');
                } else {
                    hideProgressBar('p2-users');
                }
            } else {
                users.attr('disabled');
                hideProgressBar('p2-users');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxUsers');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-users');
        }
    })
}

function getAjaxSubjects() {

    let data = {};

    if(classLevelID !== ''){
        data.classLevelID = classLevelID
    }
    if(classID !== ''){
        data.classID = classID
    }
    if(user.userID !== ''){
        data.userID = user.userID

    }
    $.ajax({
        url: herokuUrl+'/getUserSubjects',
        method: 'POST',
        headers,
        beforeSend: function () {
            subjects.html('');
            subjects.empty();
            subjects.find('option').remove();
            if(__ROUTE__ === 'conference'){
                subjects.append(`<option value="0">${messages.generalMeeting}</option>`);
            }else{
                subjects.append(`<option value="">${messages.selectSubject}</option>`);
            }
            subjects.multiselect('rebuild');
            subjects.attr('disabled', true);
            showProgressBar('p2-subjects');
        },
        data,
        success: function (res) {
            if(!(res.response.internalMessage)){
                subjects.removeAttr('disabled');
                if (res.response.subjects.length >= 1) {
                    if (optionalFields[__ROUTE__] !== undefined && !(optionalFields[__ROUTE__].indexOf())) {
                        subjectID = res.response.subjects[0].subjectID;
                    };
                    $.each(res.response.subjects, function (index, value) {
                        if(__ROUTE__ === 'daily-diary' || __ROUTE__ === 'quiz-results' || __ROUTE__ === 'time-table'  || __ROUTE__ === 'new-time-table' || __ROUTE__ === 'academic'){
                            if(  typeof value.isActive !== 'undefined' && value.isActive){
                                subjectsList.append(`<option value="${value.subjectID}">${value.subjectName}</option>`);
                                subjects.append(`<option value="${value.subjectID}">${value.subjectName}</option>`);
                            }
                        }else{
                            subjects.append(`<option value="${value.subjectID}">${value.subjectName}</option>`);
                            subjectsList.append(`<option value="${value.subjectID}">${value.subjectName}</option>`);
                        }
                    });
                    if(__ROUTE__ == 'messages' || __ROUTE__ == 'daily-diary'){
                        subjects.multiselect('rebuild');
                    }
                    hideProgressBar('p2-subjects');
                } else {
                    hideProgressBar('p2-subjects');
                }
                subjects.multiselect('rebuild');
            } else {
                subjects.attr('disabled');
                hideProgressBar('p2-subjects');
                subjects.multiselect('rebuild');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxSubjects');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-subjects');
        }
    })
}

function getAjaxNonSubjects() {
    data = {}
    var route = '';
    if(user.isAdmin == '1'){
        route = '/getNonSubjects';
    }else{
        route = '/getUserNonSubjects';
    }
    if(classLevelID != ''){
        data.classLevelID = classLevelID;
    }
    if(classID != ''){
        data.classID = classID;
    }
    if(user.userID !== ''){
        data.userID = user.userID

    }

    $.ajax({
        url: herokuUrl+route,
        method: 'POST',
        headers,
        beforeSend: function () {
            nonSubjects.html('');
            nonSubjects.find('option').remove()
            nonSubjects.empty();
            nonSubjects.append(`<option value="">${messages.selectNonSubject}</option>`);
            nonSubjects.attr('disabled', true);
            showProgressBar('p2-non-subjects');
        },
        data,
        success: function (res) {
            if(user.isAdmin == '1'){
                if (!res.error) {
                    nonSubjects.removeAttr('disabled');
                    if (res.response.length >= 1) {
                        if (optionalFields[__ROUTE__] !== undefined && !(optionalFields[__ROUTE__].indexOf())) {
                            nonSubjectID = res.response[0].nonSubjectID;
                        };
                        $.each(res.response, function (index, value) {
                            nonSubjects.append(`<option value="${value.nonSubjectID}">${value.nonSubjectName}</option>`);
                        });
                        nonSubjects.multiselect('rebuild');
                        hideProgressBar('p2-non-subjects');
                    } else {
                        hideProgressBar('p2-non-subjects');
                    }
                } else {
                    nonSubjects.attr('disabled');
                    hideProgressBar('p2-non-subjects');
                }
            }else{
                if (!res.error) {
                    nonSubjects.removeAttr('disabled');
                    if (res.response && res.response.nonsubjects && res.response.nonsubjects.length >= 1) {
                        if (optionalFields[__ROUTE__] !== undefined && !(optionalFields[__ROUTE__].indexOf())) {
                            nonSubjectID = res.nonsubjects[0].nonSubjectID;
                        };
                        $.each(res.response.nonsubjects, function (index, value) {
                            nonSubjects.append(`<option value="${value.nonSubjectID}">${value.nonsubjectName}</option>`);
                        });
                        nonSubjects.multiselect('rebuild');
                        hideProgressBar('p2-non-subjects');
                    } else {
                        hideProgressBar('p2-non-subjects');
                    }
                } else {
                    nonSubjects.attr('disabled');
                    hideProgressBar('p2-non-subjects');
                }
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxNonSubjects');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-non-subjects');
        }
    })
}

function getAjaxSubjectsNonSubjectsList() {

    let data = {};
    if(userID !== ''){
        data.userID = userID

    }
    if(classID !== ''){
        data.classID = classID
    }
    if(classLevelID !== ''){
        data.classLevelID = classLevelID
    }

    $.ajax({
        url: herokuUrl+'/getUserAllSubjects',
        method: 'POST',
        headers,
        beforeSend: function () {
            subjects.html('');
            subjects.empty();
            subjects.find('option').remove();
            subjects.append(`<option value="">${messages.selectSubject}</option>`);
            subjects.attr('disabled', true);
            showProgressBar('p2-subjects');
        },
        data,
        success: function (res) {
            if(!(res.response.internalMessage)){
                if (typeof res.response !== 'undefined') {
                    subjects.removeAttr('disabled');
                    finalArray = [];
                    if (res.response) {
                        if(res.response.subjects.length >= 1){
                            finalArray['Subject(s)'] = res.response.subjects;
                        }
                        if(res.nonsubjects.length >= 1){
                            finalArray['Non-Subject(s)'] = res.nonsubjects;
                        }
                    }
                    setTimeout( () => {
                        for (var heading in finalArray) {
                            if(finalArray[heading].length > 0){
                                var $optgroup = $("<optgroup>", {
                                    label: heading
                                });
                            }
                            $optgroup.appendTo('#filter-subjects');

                            finalArray[heading].forEach(function(value, index) {
                                var $option = $("<option>", {
                                    text: `${value.subjectName}`,
                                    value: value.subjectID,
                                    attribute: heading,
                                });
                                $option.appendTo($optgroup);
                            });
                        }
                        $('.multiselect-container').addClass('w-100')
                        $('.multiselect-container').css({
                            'max-height':'300px',
                            'overflow-y':'scroll'
                        })
                        subjects.removeAttr('disabled');
                        subjects.multiselect('rebuild');
                    },
                    500
                    )
                    hideProgressBar('p2-subjects');
                    subjects.removeAttr('disabled');
                }
            }else{
                hideProgressBar('p2-subjects');
                subjects.multiselect('rebuild');
            }

        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxSubjectsNonSubjectsList');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-subjects');
        }
    })
}

function getAjaxExamSubjects() {
    let data = {};
    if(classLevelID !== ''){
        data.classLevelID = classLevelID

    }
    if(classID !== ''){
        data.classID = classID
    }

    $.ajax({
        url: apiUrl + '/userSubjects',
        method: 'GET',
        beforeSend: function () {
            subjects.html('');
            subjects.empty();
            subjects.find('option').remove();
            subjects.append(`<option value="">${messages.selectSubject}</option>`);
            subjects.attr('disabled', true);
            showProgressBar('p2-subjects');
        },
        data,
        success: function (res) {
            if (!res.error) {
                subjects.removeAttr('disabled');
                if (res.data.length >= 1) {
                    if (optionalFields[__ROUTE__] !== undefined && !(optionalFields[__ROUTE__].indexOf())) {
                        subjectID = res.data[0].subjectID;
                    };
                    $.each(res.data, function (index, value) {
                        subjects.append(`<option value="${value.subjectID}">${value.subjectName}</option>`);
                    });

                    subjects.multiselect('rebuild');
                    hideProgressBar('p2-subjects');
                } else {
                    hideProgressBar('p2-subjects');
                }
                subjects.multiselect('rebuild');
            } else {
                subjects.attr('disabled');
                hideProgressBar('p2-subjects');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxExamSubjects');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-subjects');
        }
    })
}

function getAjaxExamTerms() {

    $.ajax({
        url: apiUrl + '/examTerms',
        method: 'GET',
        beforeSend: function () {
            examTerms.html('');
            examTerms.empty();
            examTerms.find('option').remove();
            examTerms.append(`<option value="">${messages.selectExamTerm}</option>`);
            examTerms.multiselect('refresh')
            examTerms.multiselect('rebuild')
            examTerms.attr('disabled', true);
            showProgressBar('p2-exam-term');
        },
        data: {
            classID: classID,
            branchID: branchID,
            classLevelID: classLevelID,
            schoolID: schoolID
        },
        success: function (res) {
            if (!res.error) {
                examTerms.removeAttr('disabled');
                if (res.data.length >= 1) {
                    if (optionalFields[__ROUTE__] !== undefined && !(optionalFields[__ROUTE__].indexOf())) {
                        subjectID = res.data[0].subjectID;
                    };
                    $.each(res.data, function (index, value){
                        examTerms.append(`<option value="${value.examsTermID}" data-isPublished="${value.isPublished}">${value.termName}</option>`);
                    });
                    examTerms.multiselect('rebuild');
                    hideProgressBar('p2-exam-term');
                } else {
                    hideProgressBar('p2-exam-term');
                }
            } else {
                examTerms.attr('disabled');
                hideProgressBar('p2-exam-term');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxExamTerms');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-exam-term');
        }
    })
}


function getAjaxActivities() {

    $.ajax({
        url: herokuUrl + '/getActiveActivity',
        method: 'POST',
        headers,
        beforeSend: function () {
            activities.html('');
            activities.empty();
            activities.find('option').remove();
            activities.append(`<option value="">${messages.selectActivity}</option>`);
            // activities.multiselect('refresh')
            // activities.multiselect('rebuild')
            activities.attr('disabled', true);
            showProgressBar('p2-activities');
        },
        data: {
            branchID: branchID,
        },
        success: function (res) {
            var response = parseResponse(res)
            if (!res.error) {
                activities.removeAttr('disabled');
                if (response.length >= 1) {
                    $.each(response, function (index, value) {
                        activities.append(`<option value="${value.activityID}" >${value.activityName}</option>`);
                    });
                    activities.multiselect('rebuild');
                    hideProgressBar('p2-activities');
                } else {
                    hideProgressBar('p2-activities');
                }
            } else {
                activities.attr('disabled');
                hideProgressBar('p2-activities');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxActivities');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-activities');
        }
    })
}

function getAjaxStudents() {
    var quizResultBody = $('#quiz-results-add-tbody');
    var assessmentBody = $('#assessments-add-tbody');
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {
            if(__ROUTE__ == 'messages'){
                students.find('option').remove();
                students.empty();
                students.append(`<option value="0">${messages.allStudents}</option>`);
                students.multiselect('refresh');
            }else if(__ROUTE__ == 'conference'){
                students.find('option').remove();
                students.empty();
                students.append(`<option value="0">${messages.allStudents}</option>`);
                students.multiselect('refresh');
            }else if(__ROUTE__ == 'form-management'){
                students.find('option').remove();
                students.empty();
                students.append(`<option value="0">${messages.allStudents}</option>`);
                students.multiselect('refresh');
            }else{
                students.find('option').remove();
                students.empty();
                students.append(`<option value="">${messages.allStudents}</option>`);
            }
            students.multiselect('refresh');
            students.multiselect('rebuild');
            students.attr('disabled', true);
            showProgressBar('p2-students');
            if (quizResultBody.length) {
                // iterateQuizResultAddList();
            }

            if (assessmentBody.length) {
                assessmentBody.empty();
                assessmentBody.append(`<tr>
                    <td colspan="3" class="text-center"><strong>${messages.loading}</strong></td>
                </tr>`)
            }
            studentRecordsArray = [];
        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                studentRecordsArray = res.data;
                students.removeAttr('disabled');
                if (res.data.length >= 1) {
                    if (optionalFields[__ROUTE__] !== undefined && !(optionalFields[__ROUTE__].indexOf())) {
                        if (res.data[0].studentID !== undefined) {
                            studentID = res.data[0].studentID;
                        }
                    };

                    if ($('#assessments-add-tbody').length) {

                        assessmentBody.empty();
                        $.each(res.data, function (index, value) {
                            assessmentBody.append(`<tr>
                                <input type="hidden" name="studentID" value="${value.studentID}" />
                                <td class="text-center">${value.rollNo}</td>
                                <td>${value.studentName}</td>
                                <td><textarea class="form-control" placeholder="${messages.assessmentNotes}" name="assessmentNotes"></textarea></td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-icon-blue p-0">
                                        <input id="isPublish${value.studentID}" type="checkbox" data-index="${index}" value="1">
                                        <label for="isPublish${value.studentID}"></label>
                                    </div>
                                </td>
                            </tr>`)
                        });
                    }
                    if(__ROUTE__ == 'messages' || __ROUTE__ == 'form-management'){
                        messagesStudentArray = []
                        var finalArray = [];
                        var classAliasArray  = [];
                        var result = Array.isArray(res.data) ? res.data : []
                        result.forEach((res, index) => {
                            if(classAliasArray.indexOf(res.classAlias+'-'+res.classSection) == -1){
                                classAliasArray.push(res.classAlias+'-'+res.classSection);
                            }
                            let position = messagesSelectedClassesArray.indexOf(String(res.classID));
                            if(position >= 0 ){
                                messagesStudentArray[messagesSelectedClassesArray[position]] = new Array();
                            }
                        });
                        result.forEach((res, index) => {
                            let position = messagesSelectedClassesArray.indexOf(String(res.classID));
                            if(position >= 0 ){
                                messagesStudentArray[messagesSelectedClassesArray[position]].push(res);
                            }
                        });
                        for(var i = 0; i < classAliasArray.length; i++ ){
                            var data = {};
                            let classSectionArray = classAliasArray[i].split('-');
                            if(classSectionArray.length === 3){
                                classSectionArray = classAliasArray[i].split('-',3);
                                data['alias'] = classSectionArray[0]+'-'+classSectionArray[1];
                                data['sections'] = classSectionArray[2];
                            }else if(classSectionArray.length === 2){
                                data['alias'] = classSectionArray[0];
                                data['sections'] = classSectionArray[1];
                            }
                            data['students'] = [];
                            finalArray.push(data);
                        }
                        for(var i = 0; i < finalArray.length; i++ ){
                            result.forEach((res, index) => {
                                if(res.classSection == finalArray[i].sections){
                                    finalArray[i].students.push(res);
                                }
                            });
                        }

                        $.each(finalArray, function(i, optgroups) {
                            var $optgroup = $("<optgroup>", {
                                label: 'Section-'+optgroups.alias+"-"+optgroups.sections
                            });
                            $optgroup.appendTo('#filter-students');
                            $.each(optgroups.students, function(groupName, options) {
                                var $option = $("<option>", {
                                    text: options.studentName,
                                    value: options.studentID,
                                    classid: options.classID
                                });
                                $option.appendTo($optgroup);
                            });
                        });
                    }else{
                        $.each(res.data, function (index, value) {
                            students.append(`<option value="${value.studentID}">${value.studentName}</option>`);
                        });
                    }
                    students.multiselect('rebuild');
                    hideProgressBar('p2-students');
                } else {
                    studentID = '';
                    if (quizResultBody.length) {
                        quizResultBody.empty();
                        quizResultBody.append(`<tr>
                            <td colspan="4" class="text-center"><strong>${messages.noStudentFound}</strong></td>
                        </tr>`)
                    }

                    if (assessmentBody.length) {
                        assessmentBody.empty();
                        assessmentBody.append(`<tr>
                            <td colspan="4" class="text-center"><strong>${messages.noStudentFound}</strong></td>
                        </tr>`)
                    }
                    hideProgressBar('p2-students');
                }
            } else {
                students.attr('disabled');
                hideProgressBar('p2-students');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxStudents');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-users');
        }
    })
}

function getAjaxCountries() {
    $.ajax({
        url: apiUrl + '/countries',
        method: 'GET',
        headers,
        data,
        dataType: 'json',
        beforeSend: function () {
            countries.empty();
            if(__ROUTE__ == 'messages'){
                countries.append(`<option value="">${messages.allCountry}</option>`);
            }else{
                countries.append(`<option value="">${messages.selectCountry}</option>`);
            }

            countries.attr('disabled', true);
            showProgressBar('p2-countries');
        },
        success: function (res) {
            if (!res.error) {
                countries.removeAttr('disabled');
                //countryID = res.data[0].countryID;
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                            countries.append(`<option value="${value.countryID}" data-code="${value.countryTwoAlphaCode}">${value.countryName}</option>`);
                    });
                    countries.multiselect('rebuild');
                    hideProgressBar('p2-countries');
                    if(cities.length && cityID !== ''){
                        getAjaxCities(countryID);
                    }
                } else {
                    countries.append(`<option value="">${messages.noCountryFound}</option>`);
                    countries.attr('disabled', true);
                    hideProgressBar('p2-countries');
                }
            } else {
                countries.attr('disabled');
                hideProgressBar('p2-countries');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxCountries');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-countries');
        }
    });
}

function getAjaxFeeTypes() {
    $.ajax({
        url: apiUrl + '/feeTypes',
        method: 'GET',
        beforeSend: function () {
            feeTypes.html('');
            feeTypes.find('option').remove();
            feeTypes.empty();
            feeTypes.append(`<option value="">${messages.selectFeeType}</option>`);
            feeTypes.attr('disabled', true);
            showProgressBar('p2-feeTypes');
        },
        data: {
            schoolID: schoolID
        },
        success: function (res) {
            if (!res.error) {
                feeTypes.removeAttr('disabled');
                removeAttrFromMultipleElement('fee-type-filter', 'disabled' );
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                        if(__ROUTE__ == 'student-fee'){
                            if(value.isActive === 1 && value.isRecurring === 1){
                                feeTypes.append(`<option value="${value.feeTypeID}" >${value.feeType}</option>`);
                            }
                        }else{
                            feeTypes.append(`<option value="${value.feeTypeID}" >${value.feeType}</option>`);
                        }

                    });
                    feeTypes.multiselect('rebuild');
                    hideProgressBar('p2-feeTypes');

                } else {
                    hideProgressBar('p2-feeTypes');
                }
            } else {
                feeTypes.attr('disabled');
                addAttrOnMultipleElement('fee-type-filter', 'disabled', true);
                hideProgressBar('p2-feeTypes');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxFeeTypes');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-feeTypes');
        }
    })
}
// remember  all below functions work for multi or single element => 13-dec-2018
function showMultipleProgressBar(className){

    $("."+className).each( function () {
        $(this).siblings().removeClass('hidden');
    });

}
function hideMultipleProgressBar(className){

    $("."+className).each( function () {
        $(this).siblings().addClass('hidden');
    });

}

function addAttrOnMultipleElement(className , attrName , attrValue){

    $("."+className).each( function () {
        $(this).attr(attrName , attrValue );
    });

}

function removeAttrFromMultipleElement(className , attrName ){

    $("."+className).each( function () {
        $(this).removeAttr(attrName);
    });

}
// 13-dec-2018 Waqar Ali.
//function getAjaxCities(cities, countryID){

function getAjaxCities(countryID = null) {
    if(countryID === ''){
        countryID = user.countryID
    }else{
        countryID = countryID
    }

    $.ajax({
        url: apiUrl + '/cities',
        method: 'GET',
        beforeSend: function () {
            cities.html('');
            cities.find('option').remove();
            cities.empty();
            if(__ROUTE__ == 'messages'){
                cities.append(`<option value="">${messages.allCity}</option>`);
            }else{
                cities.append(`<option value="">${messages.selectCity}</option>`);
            }

            cities.attr('disabled', true);
            showProgressBar('p2-cities');
        },
        data: {
            countryID: countryID
        },
        success: function (res) {
            if (!res.error) {
                cities.removeAttr('disabled');
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                        cities.append(`<option value="${value.cityID}" data-city-name="${value.cityName}">${value.cityName}</option>`);
                    });
                    if(__ROUTE__ == 'messages' || __ROUTE__ == 'daily-diary'){
                        cities.multiselect('rebuild');
                    }
                    cities.multiselect('rebuild');
                    hideProgressBar('p2-cities');
                    if(areas.length && areaID !== ''){
                        getAjaxAreas();
                    }
                } else {
                    cities.append(`<option value="">${messages.noCityFound}</option>`);
                    cities.attr('disabled', true);
                    hideProgressBar('p2-cities');
                }
            } else {
                cities.attr('disabled');
                hideProgressBar('p2-cities');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxCities');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-cities');
        }
    })
}

function getAjaxAreas(){
    if(cityID === ''){
        cityID = user.cityID
    }
    $.ajax({
        url: apiUrl + '/areas',
        method: 'GET',
        beforeSend: function () {
            areas.html('');
            areas.find('option').remove();
            areas.empty();
            if(__ROUTE__ == 'messages'){
                areas.append(`<option value="">${messages.allArea}</option>`);
            }else{
                areas.append(`<option value="">${messages.selectArea}</option>`);
            }
            areas.multiselect('refresh')
            areas.multiselect('rebuild')
            areas.attr('disabled', true);
            showProgressBar('p2-areas');
        },
        data: {
            cityID: cityID
        },
        success: function (res) {
            if (!res.error) {
                areas.removeAttr('disabled');
                if (res.data.length >= 1) {
                    $.each(res.data, function (index, value) {
                            areas.append(`<option value="${value.areaID}">${value.areaName}</option>`);
                    })
                    if(__ROUTE__ == 'messages' || __ROUTE__ == 'daily-diary'){
                        areas.multiselect('rebuild');
                    }
                    areas.multiselect('rebuild');
                    hideProgressBar('p2-areas');
                } else {
                    areas.append(`<option value="">${messages.noAreaFound}</option>`);
                    areas.attr('disabled', true);
                    hideProgressBar('p2-areas');
                }
            } else {
                areas.attr('disabled');
                hideProgressBar('p2-areas');
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getAjaxAreas');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-areas');
        }
    })
}
/* FITLERS AJAX END */

// Parse Heroku Response
function parseResponse(res) {
    if (Array.isArray(res.response) && (response = res.response)) {
        return response;
    } else {
        return [];
    }
}

// Show Message Box
function showMessageBox(text, type = 'danger') {
    $('.msg-box').addClass('lightSpeedIn');
    $('.msg-box').addClass(`${type === 'success' ? 'msg-box-success' : 'msg-box-danger'}`)
    $('.msg-box').removeClass('hidden');
    $('.msg-box').html(`<p class="mb-0" id="msg-box-text">${text}</p>`);

    // setTimeout(function(){
    //     $('.msg-box').addClass('fadeOutDown')
    // }, 5000);

    // setTimeout(function(){
    //     $('.msg-box').addClass('hidden');
    // }, 6000);
}


function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

//Edit Record Parsing
$(document).on('click', '.list-record-edit', function () {
    $('.submitLoader').addClass('hidden')
    if(__ROUTE__ === 'academic-calendar' && $('#filter-classLevels').val() == '0'){
        swal(
            messages.pleaseSelectSingleClassToEdit,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    idEditButtonClicked = true
    if(__ROUTE__ === 'class-levels'){
        let json = JSON.parse($(this).attr('data-json'));
        if(json.isActive === 1){
            $('#isActive1').prop('checked', true);
        }else{
            $('#isActive1').prop('checked', false);
        }
        if(json.isSubjectLevelAttendance === 1){
            $('#isSubjectLevelAttendance1').prop('checked', true);
        }else{
            $('#isSubjectLevelAttendance1').prop('checked', false);
        }
    }
    if (__ROUTE__ === 'parents' ) {
        let json = JSON.parse($(this).attr('data-json'));
        if (json.isActive === 1) {
            $('#checkIsActive').prop('checked', true);
            $('#isActive').val(1);
        } else {
            $('#checkIsActive').attr('checked', false);
            $('#isActive').val(0);
        }
    }

    if(__ROUTE__ === 'subject-time-table'){
        let json = JSON.parse($(this).attr('data-json'));
        $('#timetableSubjectID').val(json.timetableSubjectID)
        $('#timetableSubjectID').attr('name', 'timetableSubjectID')
        $("#filter-class-room option[value='" + json.roomID + "']").prop("selected", true);
        $('#filter-class-room').multiselect('rebuild');
        $("#filter-week-day option[value='" + json.weekdayNo + "']").prop("selected", true);
        $('#filter-week-day').multiselect('rebuild');
        $("#filter-class-teachers option[value='" + json.userID + "']").prop("selected", true);
        $("#filter-class-teachers").multiselect('rebuild');
    }
    $('#assigned-recurring-error').empty();
    if(__ROUTE__ === 'calendar-days'){
        academicEventCheck = '1'
        $('.calender-edit').removeClass('hidden');
        $('.calender-add').addClass('hidden');
        $('.endDateDiv').addClass('hidden');
        $('.multipleDaysDiv').addClass('hidden');
    }

    if(__ROUTE__ === 'student-fee-challan'){
        var json = JSON.parse($(this).attr('data-json'));
        let voucherNote = json.voucherNote
        if(voucherNote !== null){
            voucherNote = atob(voucherNote)
            // json = voucherNote.slice(1,-1);
            $(".contract-summernote").summernote('code', voucherNote.trim());
        }
        $.LoadingOverlay("show");
        getFeeType();
    }

    if(__ROUTE__ === 'terms-conditions'){
        var json = $(this).attr('data-tnc');
        var json_ar = $(this).attr('data-tnc-ar');
        json = json.slice(1,-1);
        json_ar = json_ar.slice(1,-1);
        $("#summernote").summernote('code', json.trim());
        $("#arabicSummernote").summernote('code', json_ar.trim());
    }

    if(__ROUTE__ === 'contract'){
        var json = $(this).attr('data-contract');
        json = json.slice(1,-1);
        $(".contract-summernote").summernote('code', atob(json.trim()));
        var json = $(this).attr('data-charges');
        json = json.slice(1,-1);
        $(".charges-summernote").summernote('code', atob(json.trim()));
        var json = $(this).attr('data-collection');
        json = json.slice(1,-1);
        $(".collection-summernote").summernote('code', atob(json.trim()));
    }
    var json = JSON.parse($(this).attr('data-json').replace(/&/g, "\'"));
    var keys = Object.keys(json);

    if(__ROUTE__ === 'users'){
        jsonArray  = JSON.parse($(this).attr('data-json'));
        $('#mobileNo').attr('readonly', true)
    }

    if(keys.length >= 1) {
        keys.forEach(function(keyValue, keyIndex){
                editHiddenField.attr('name', keys[0]);
        })
        keys.forEach((value, index) => {
            if(__ROUTE__ == 'parents'){
                $('[name='+value+']').removeAttr('disabled');
                submitButton.removeAttr('disabled');
            }
            if(value == 'showAccNoOnVoucher' && __ROUTE__ === 'schools'){
                if(json[value] == 1){
                    $('#showAccNoOnVoucher').prop('checked', true);
                }else{
                    $('#showAccNoOnVoucher').prop('checked', false);
                }
            }
            if (value == 'isActive' ||  value == 'isPublished' || value == 'isParent' || value == 'isAdmin' || value == 'isSignatory' || value == 'isTaxRegistered' || value == 'selfRegistered') {
                if(value == 'isActive' && __ROUTE__ === 'school-branches'){
                    if(json[value] == 1){
                        $('#isActive').prop('checked', true);
                        $('#isActiveInput').val(1)
                    }else{
                        $('#isActive').prop('checked', false);
                        $('#isActiveInput').val(0)
                    }
                }
                if(value == 'isActive' && __ROUTE__ === 'schools'){
                    if(json[value] == 1){
                        $('#isActive').prop('checked', true);
                        $('#isActiveInput').val(1)
                    }else{
                        $('#isActive').prop('checked', false);
                        $('#isActiveInput').val(0)
                    }
                }
                if(value == 'selfRegistered' && __ROUTE__ === 'schools'){
                    if(json[value] == 1){
                        $('#selfRegistered').prop('checked', true);
                        $('#selfRegisteredInput').val(1)
                    }else{
                        $('#selfRegistered').prop('checked', false);
                        $('#selfRegisteredInput').val(0)
                    }
                }
                if (value == 'isActive' && __ROUTE__ == 'fee-type' ) {
                    if (json[value] == 1) {
                        $('#isActive').prop('checked', true);
                        $('#isActiveInput').val(1);
                    } else {
                        $('#isActive').attr('checked', false);
                        $('#isActiveInput').val(0);
                    }
                }
                if (value == 'isActive' && __ROUTE__ == 'users' ) {
                    if (json[value] == 1) {
                        $('#isActiveCheck').prop('checked', true);
                        $('#isActive').val(1);
                    } else {
                        $('#isActiveCheck').attr('checked', false);
                        $('#isActive').val(0);
                    }
                }
                if (value == 'isActive' && __ROUTE__ == 'students' ) {
                    if (json[value] == 1) {
                        $('#isActiveCheck').prop('checked', true);
                        $('#isActive').val(1);
                    } else {
                        $('#isActiveCheck').attr('checked', false);
                        $('#isActive').val(0);
                    }
                }
                if (value == 'isAdmin' && __ROUTE__ == 'users' ) {
                    if (json[value] == 1) {
                        $('#isAdminCheck').prop('checked', true);
                        $('#isAdmin').val(1);
                    } else {
                        $('#isAdminCheck').attr('checked', false);
                        $('#isAdmin').val(0);
                    }
                }

                if (json[value] == 1) {
                    if(value == 'isActive' && __ROUTE__ === 'class-rooms'){
                        if(json[value] == 1){
                            $('#isActive').prop('checked', true);
                        }else{
                            $('#isActive').prop('checked', false);
                        }
                    }
                    if(value == 'isTaxRegistered' && __ROUTE__ === 'parents'){
                        if(json[value] == 1){
                            $('#tax-beneficiary').prop('checked', true);
                            $('#taxId').removeClass('hidden')
                        }else{
                            $('#tax-beneficiary').prop('checked', false);
                            $('#taxId').addClass('hidden')
                        }
                    }
                    // if(value == 'TaxID' && __ROUTE__ === 'parents'){
                    //     var taxID = json[value]
                    //     console.log(taxID,'taxID')
                    //         $('#TaxID').val(taxID);

                    // }
                    // if(value == 'isActive' && __ROUTE__ === 'parents'){
                    //     if (json[value] == '1') {
                    //         $('#isActive').prop('checked', true);
                    //         $('#isActiveInput').val(1);
                    //     } else {
                    //         $('#isActive').attr('checked', false);
                    //         $('#isActiveInput').val(0);
                    //     }
                    // }
                    if(value == 'isActive' && __ROUTE__ === 'parents'){
                        if(json[value] == 1){
                            $('#checkIsActive').prop('checked', true);
                            $('#isActive').val(1)
                        }else{
                            $('#checkIsActive').prop('checked', false);
                            $('#isActive').val(0)
                        }
                    }else if(value == 'isActive' && __ROUTE__ === 'student-tag'){
                        if(json[value] == 1){
                            $('#isActive').prop('checked', true);
                            $('#isActiveInput').val(1)
                        }else{
                            $('#isActive').prop('checked', false);
                            $('#isActiveInput').val(0)
                        }
                    }
                    if (__ROUTE__ == 'parents' && value == 'parentLastName' ){
                        var lastName = json[value];
                        $('#lastName').val(lastName);
                    }
                    if(value == 'isSignatory' && __ROUTE__ === 'users'){
                        $('#checkIsSignatory').prop('checked', true)
                    }
                    if(value == 'tagID' && __ROUTE__ === 'users'){
                        $('#tagID').val(json[value])
                    }
                    if(value == 'isAdmin'){
                        $('#isAdmin0').removeAttr('checked');
                        $('#isAdmin1').attr('checked', 'checked');
                    }
                    if(value == 'isParent'){
                        $('input#optionsRadios2').removeAttr('checked');
                        $('input#optionsRadios1').attr('checked', 'checked');
                    }
                    if(value == 'isPublished'){
                        $('#isPublished0').removeAttr('checked');
                        $('#isPublished1').attr('checked', 'checked');
                    }
                    $('#activeField0').removeAttr('checked');
                    $('#activeField1').attr('checked', true);
                } else {

                    if(value == 'isAdmin'){
                        $('#isAdmin1').removeAttr('checked');
                        $('#isAdmin0').attr('checked', 'checked');
                    }

                    if(value == 'isParent'){
                        $('input#optionsRadios1').removeAttr('checked');
                        $('input#optionsRadios2').attr('checked', 'checked');
                    }
                    if(value == 'isPublished'){
                        $('#isPublished1').removeAttr('checked');
                        $('#isPublished0').attr('checked', 'checked');
                    }
                    $('#activeField1').removeAttr('checked');
                    $('#activeField0').attr('checked', true);
                }
            }else if (value == 'isHoliday' && __ROUTE__ == 'calendar-days' ) {
                if (json[value] == 1) {
                    $('#isHoliday').prop('checked', true);
                }
            }else if (value == 'isRecurring' && __ROUTE__ == 'fee-type' ) {
                if (json[value] == 1) {
                    $('#isRecurring').prop('checked', true);
                } else {
                    $('#isRecurring').attr('checked', false);
                }
            }else if(value == 'isClassable' && __ROUTE__ === 'class-rooms'){
                if(json[value] == 1){
                    $('#isClassable').prop('checked', true);
                }else{
                    $('#isClassable').prop('checked', false);
                }
            }else if (value == 'penaltyType' && __ROUTE__ === 'student-fee-challan' ) {
                if (json[value] == 'F') {
                    $('#activeFieldP').removeAttr('checked');
                    $('#activeFieldF').attr('checked', true);
                } else {
                    $('#activeFieldF').removeAttr('checked');
                    $('#activeFieldP').attr('checked', true);
                }
            }else if (value === 'branchID' && __ROUTE__ === 'users' ) {
                if(json[value] !== '' && json[value] !== null && json[value] !== 'null' ){
                    $("#filter-branches option[value='" + json[value] + "']").prop("selected", true);
                    $("#filter-branches").multiselect('rebuild');
                }
            }else if(value == 'feePaymentAllowed' && __ROUTE__ === 'users'){
                if(json[value] === 1){
                    $('#feePaymentAllowedCheckbox').prop('checked', true)
                    $('#feePaymentAllowed').val(1)
                }else{
                    $('#feePaymentAllowedCheckbox').prop('checked', false)
                    $('#feePaymentAllowed').val(0)
                }
            }
            else if (value == 'userRoleData' && __ROUTE__ === 'users') {
                var userRoleArray = json[value];
                userRoleArray.forEach(function(val, ind){
                    if($('#filter-branches').val() == val.branchID){
                        $("#multiple option[value='" + val.roleID + "']").prop("selected", true);
                    }
                });
                // $('#multiple').trigger('change');
            }else if (value == 'invoiceNumber' && __ROUTE__ == 'student-fee-challan') {
               $('.auto-generate-checkbox').hide();
               $('.invoice-number-label').show();
               $('[name=' + value + ']').val(json[value]).removeAttr('disabled');
            }else if (value == 'classID' && __ROUTE__ == 'daily-diary') {
                $('option', $('#filter-classes')).each(function(element) {
                    $(this).removeAttr('selected').prop('selected', false);
                });
                $("option[value="+json[value]+"]", $('#filter-classes')).prop("selected", true);
                $('#filter-classes').multiselect('refresh');
            }else if (value == 'description' && __ROUTE__ == 'daily-diary') {
                $("#description").text(json[value]);
            }
            else if (value == 'classLevelName' && __ROUTE__ == 'class-levels') {
                $('[name=' + value + ']').val(json[value]);
                $('#classLevelName').multiselect('rebuild');
            }else if (value == 'assessmentOnly' && __ROUTE__ == 'class-levels') {
                $('[name=' + value + ']').val(json[value]);
                $('#assessmentOnly').multiselect('rebuild');
            }else if(__ROUTE__ == 'roles' && value === 'roleDesc'){
                if(json[value] === '' || json[value] === 'undefined'){
                    $('#roleDescription').val('');
                }else{
                    $('#roleDescription').val(json[value]);
                }
            }else if (value === 'tncText' && __ROUTE__ === 'terms-conditions') {
                $("#summernote").summernote('code', json[value]);
            }else if (value === 'insertionDate' && __ROUTE__ === 'terms-conditions') {
                $('[name=' + value + ']').val(moment(json[value]).format('YYYY-MM-DD'));
            }else if (value === 'userID' && __ROUTE__ === 'activities') {
                users.val(json[value])
                users.multiselect('rebuild')
            }else if (__ROUTE__ == 'parents' && value == 'mobileNumber' ){
                var phoneNumber = json[value];
                setTimeout(() => {
                    $('#passwordInput').removeAttr('required');
                    $('#phoneNumber').val(phoneNumber);
                    $('#phone-error').text('')
                }, 500);
            }else if(value == 'TaxID' && __ROUTE__ === 'parents'){
                var taxID = json[value]
                $('#TaxID').val(taxID);

            }else if(value == 'addressLine1' && __ROUTE__ === 'parents'){
                if(json[value] !== null){
                    $('#addressLine1').val(atob(json[value]))
                }
            }else if(value == 'addressLine2' && __ROUTE__ === 'parents'){
                if(json[value] !== null){
                    $('#addressLine2').val(atob(json[value]))
                }
            }else if(__ROUTE__ == 'users' && value == 'mobileNo'){
                $('#passwordDiv').hide();
                $('#passwordInput').removeAttr('required');
                $('[name=' + value + ']').val(json[value]);
            }else if(__ROUTE__ == 'students' && value == 'dob'){
                var dob = moment(json[value]).format('YYYY-MM-DD');
                $('[name=' + value + ']').val(dob);
            }else if(__ROUTE__ == 'students' && value == 'classID'){
                studentClassSectionID = json[value]
            }
            else if(__ROUTE__ == 'quiz-results'){
                if(value == 'quizDate'){
                    var quizDate = moment(json[value]).format('YYYY-MM-DD');
                    $('[name=' + value + ']').val(quizDate);
                }
                if(value == 'marksObtained'){
                    var checkDecimal = json[value].split('.')
                    if(checkDecimal[1] > 0){
                        obtainedMarks = parseFloat(json[value]).toFixed(2)
                    }else{
                        obtainedMarks = parseFloat(json[value]).toFixed(0)
                    }
                    $('[name=' + value + ']').val(obtainedMarks);
                }
                if(value == 'marksTotal'){
                    $('.hidden-div').removeClass('hidden');
                    $('[name=' + value + ']').val(json[value]);
                }
                if(value == 'comments'){
                    $('[name=' + value + ']').val(json[value]);
                }
                if(value == 'studentID'){
                    $("option[value="+json[value]+"]", $('#filter-students')).prop("selected", true);
                }
                if(value == 'subjectID'){
                    $("option[value="+json[value]+"]", $('#filter-subjects')).prop("selected", true);
                }
                if(value == 'showGraph'){
                    if(json[value] === 1){
                        $('#showGraph').val(1);
                        $('#showGraphInput').attr('checked', true);
                    } else{
                        $('#showGraph').val(0);
                        $('#showGraphInput').attr('checked', false);
                    }
                }
            }
            else if (value == 'billingPeriodID' && __ROUTE__ === 'student-fee-challan') {
                $('#fee-type-table-body').empty();
                billingPeriodID = json[value];
                getFeeVoucherDetail();
            }  else {
                if(__ROUTE__ == 'daily-diary' && value == 'attachment'){
                    if(json[value] !== '' && json[value] !== 'undefined' &&  json[value] != 'null' &&  json[value] != null){
                        $('#diary-attachment').attr('src',json[value]);
                        $('#attachement-div').slideDown(1000);
                    }
                }else if (__ROUTE__ == 'daily-diary' && value == 'documentLink' && json[value] !== 'undefined' &&  json[value] != 'null'  &&  json[value] != null){
                    if(json[value] !== ''){
                        var doc = '';
                        doc = json[value].split('/');
                        doc = doc[doc.length - 1];
                        $('#diary-document').attr('href', json[value] )
                        $('#diary-document').text(doc);
                        $('#document-div').slideDown(1000);
                    }
                }else if(value == 'studentID' && __ROUTE__ === 'daily-diary'){
                    $('#filter-students').find('option').attr({
                        'disabled' : false,
                    });
                    if(json[value] === null){
                        $('#filter-students').find('option:first').prop('selected', true)
                        $('#filter-students').multiselect('rebuild')
                    }else if(json[value].length && json[value].length !== undefined){
                       setTimeout(() => {
                            var students = json[value].split(',');
                            $("#filter-students").multiselect('deselectAll', true)
                            students.forEach(function(stID, stInd){
                                $("#filter-students").find('option[value="'+stID+'"]').prop('selected', true);
                            });
                            $('#filter-students').multiselect('rebuild')
                       }, 500);
                    }else{
                        setTimeout(() => {
                            $('#filter-students').find('option[value="'+json[value]+'"]').prop('selected', true)
                            $('#filter-students').multiselect('rebuild')
                        }, 500);
                    }
                }else if(value == 'subjectFlag' && __ROUTE__ == 'daily-diary'){
                    if(json[value] == '1'){
                        $('#hidden-flag').val(1)
                    }else{
                        $('#hidden-flag').val(0)
                    }
                }else if(__ROUTE__ === 'student-tag'){
                    if(value == 'tagText'){
                        $('#tagText').val(json[value])
                    }
                    if(value == 'tagUUID'){
                        $('#editHiddenField').val(json[value])
                    }
                }
                $('[name=' + value + ']').val(json[value]);
            }
            if (value.includes('ID')) {
                if (typeof value !== 'undefined') {
                    window[value] = json[value];
                }
            }
        });
        if(__ROUTE__ == 'student-fee-challan'){
            $.LoadingOverlay("hide");
        }
    }
    var filterArray = [
        "filter-modules","classLevelName","assessmentOnly","classShift", "filter-schools", "filter-branches", "filter-classLevels", "filter-classes", "filter-users", "filter-class-teachers", "filter-subjects", "filter-non-subjects", "filter-students", "filter-countries", "filter-cities", "filter-exam-term", "filter-fee-type", "filter-areas", "filter-roles", "filter-message-type", "billing-period-filter","filter-activities","filter-week-day","filter-subjects-in-time-table","filter-batch-no","filter-class-room","filter-branches-branches"
    ];
    filterArray.forEach(function(value, index){
        $('#' + value).multiselect('rebuild');
    });

        resetMessages();
        hideResultsList();
        showAddFormContainer();
});

function getFeeVoucherDetail(){
    var table = $('#fee-type-table-body');
    $.ajax({
        url: herokuUrl+'/getFeeVoucherDetail',
        type: 'POST',
        headers,
        beforeSend: function(){
            showProgressBar('p2-feeTypesTable');
        },
        data: {
            billingPeriodID: billingPeriodID,
            studentID : $('#filter-students').val(),
            classID: classID
        },
        success: function(res){
            response = parseResponse(res);
            if (response.length > 0) {
                hideProgressBar('p2-feeTypesTable');
                response.forEach(function(val, ind){
                    $(`#filter-fee-type option[value="${val.feeTypeID}"]`).attr("disabled","disabled");
                    var table_content = `
                    <tr>
                    <td style="display:none;">
                    <input class="form-control" type="text" value="${val.feeVoucherDetailID}" name="feeVoucherDetailID">
                    </td>
                    <td style="display:none;">
                        <input class="form-control" type="text" value="${val.feeTypeID}" name="feeTypeID">
                    </td>
                        <td>
                        <select name=""  class="form-control filter-select fee-type-filter">
                        <option>${messages.selectFeeType}</option>`;
                    $.each(feeTpesResponse, function (index, value) {

                            table_content += `<option value="${value.feeTypeID}" ${val.feeTypeID == value.feeTypeID ? 'selected' : '' } data-feeTypeId="${value.feeTypeID}" data-taxRate="${value.taxRate}" data-feeTypeName="${value.feeType}">${value.feeType}</option>`;
                    });
                    table_content += ` </select>
                                    <div id="p2-feeTypes" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                    </td>
                                    <td>
                                        <input class="form-control taxRate" type="text" value="${val.taxRate ? val.taxRate : 0}" name="taxRate" disabled>
                                    </td>
                                    <td>
                                        <input class="form-control feeAmount" type="text" value="${val.feeTypeAmount}" name="feeTypeAmount"  required>
                                    </td>
                                    <td>
                                        <i class="fa fa-trash justify-content-center delete-fee-chalan" data-feeTypeID="${val.feeTypeID}" data-id="${val.voucherID}" data-feeVoucherDetailID="${val.feeVoucherDetailID}" data-toggle="tooltip"  title="Delete" style="font-size:26px; color:red;"></i>
                                    </td>
                                </tr>
                                    `;
                    table.append(table_content);

                });
                var selectedFeeTypes = [];
                selectedFeeTypes = getSeletedFeeTypes();
                removeDisableFromSeletedFeeTypes();
                selectedFeeTypes.forEach(function(val,index){
                    setSeletedFeeTypes(val);
                });
            } else {
                table.append(`<tr><td colspan="3">${messages.recordNotFound}</td></tr>`);
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('getFeeVoucherDetail')
            window.requestForToken(xhr)
            hideProgressBar('p2-feeTypesTable');
          if (xhr.status === 404) {
         }
        }
    });
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function handleApostropheWork(object){
    // console.log(JSON.stringify(object))
    return JSON.stringify(object).replace(/'/g, "&");
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function newTimeTableIterateTable(){
    var newTimeTable =  $('#add-time-table-form-table tbody')
    newTimeTable.empty();
    newTimeTable.append(`
    <tr><td colspan="4" class="text-center"> <strong style="color:#188ae2;font-weight:bold;text-align:center;font-size:25px;"> ${messages.loading}</<strong></td></tr>
    `)
    setTimeout(() => {
        iterateTimeTableData();
    }, 1500);
}
//Show add/edit form
$('#show-add-form-container').on('click', function () {
    idEditButtonClicked = false
    if(__ROUTE__ === 'student-tag'){
        $('#editHiddenField').val('')
        $('#isActiveInput').val(0)
        $('#tagText').val('')
        $('#isActive').prop('checked', false)
    }
    if(__ROUTE__ === 'parents'){
        $('#searchMobileNumber').val('')
        $('#searchParentName').val('')
        $('#checkIsActive').prop('checked', true);
    }
    if(__ROUTE__ === 'conference'){
        hideFields();
        $('#hiddenMeetingID').removeAttr('value')
        $('#endDate').attr('type','hidden')
        $('#endDate').attr('name','')
        $('#startDate').attr('min',moment().format('YYYY-MM-DD'))
        $('#startDate').val(moment().format('YYYY-MM-DD'))

        currentDateTime = moment().add(0,'minutes');
        startMinutes = currentDateTime.format('mm');
        var stTime = round5(startMinutes)
        var matchedValue = stTime
        if(matchedValue <= 9) matchedValue = '0'+matchedValue
        if(matchedValue === 60){
            matchedValue = '05';
        }else if(matchedValue === 00){
            matchedValue = '05'
        }
        startHour = currentDateTime.format('HH');
        $('#startHour').val(startHour)
        $('#startHour').select2()
        $('#startHourInput').val(startHour)
        endHour = currentDateTime.add(2, 'hours');
        endHour = endHour.format('HH');
        $('#startMinute').val(matchedValue)
        $('#startMinute').select2()
        $('#startMinuteInput').val(matchedValue)
        $('#endHour').val(endHour)
        $('#endHour').select2()
        $('#endHourInput').val(endHour)
        currentDateTime = moment().add(10,'minutes');
        endMinutes = currentDateTime.format('mm');
        var enTime = round5(endMinutes);
        if(enTime <= 9) enTime = '0'+enTime
        if(enTime === 60){
            enTime = '05'
        }else if(enTime === 0){
            enTime = '05'
        }
        $('#endMinute').val(enTime)
        $('#endMinute').select2()
        $('#endMinuteInput').val(enTime)



    }
    academicEventCheck = $(this).attr('academic-event')
    if(__ROUTE__ === 'calendar-days'){
        $('.calender-add').removeClass('hidden');
        $('.calender-edit').addClass('hidden');
    }
    if(__ROUTE__ === 'calendar-days'){
        $('.invite-div').hide();
    }

    if(__ROUTE__ === 'class-levels'){
        $('#classLevelName').multiselect('rebuild');
        $('#assessmentOnly').multiselect('rebuild');
    }
    // __ROUTE__ === 'daily-diary' ? $('#hidden-flag').val('') : '';
    __ROUTE__ === 'daily-diary' ? $("#description").text('') : '';
    __ROUTE__ === 'activities' ? $("#filter-users").val('').multiselect('rebuild') : '';
    if(__ROUTE__ === 'student-fee-challan'){
        $('.auto-generate-checkbox').show();
        $('.invoice-number-label').hide();
        $('.invoiceNumber').attr('disabled',true)
    }


    if( __ROUTE__ == 'student-fee-challan'){
        if(classID == ''){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        populateRecurringFee();
        // getStudentLevelFee();
    }
    if( __ROUTE__ == 'student-fee'){
        if(feeTypeID == ''){
            swal(
                messages.pleaseSelectFeeType,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }else{
            getStudentRecurringFee();
            iterateStudentRecurringFee();
        }
    }

    if( __ROUTE__ == 'academic' || __ROUTE__ == 'overall-exam'){
        if(classID == ''){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        if(examTermID === ''){
            swal(
                messages.pleaseSelectExamTermFirst,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        if(subjectID === '' && __ROUTE__ === 'academic'){
            swal(
                messages.pleaseSelectSubject,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
    }
    if( __ROUTE__ === 'assign-activities'){
        activityID = $('#filter-activities').val();
        if(classID == ''){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        if(activityID === ''){
            swal(
                messages.pleaseSelectActivityFirst,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        iterateStudentTable();
    }

    if( __ROUTE__ === 'assign-subjects'){

        if(subjectID === ''){
            swal(
                messages.pleaseSelectSubject,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }

        iterateAssignSubjectsStudentTable();
    }

    if(__ROUTE__ === 'subject-time-table'){
        $('#filter-class-room').multiselect('rebuild');
    }

    if(__ROUTE__ === 'new-time-table'){
        weekdayNo = $("#filter-week-day option:selected").val();
        if(classLevelID == ''){
            swal(
                messages.pleaseSelectClass,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        if(classID == ''){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        if(weekdayNo === ''){
            swal(
                messages.pleaseSelectWeekDay,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }
        newTimeTableIterateTable();
    }
    if(__ROUTE__ == 'nonacademic'){

        if(classID == ''){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        if(examTermID === ''){
            swal(
                messages.pleaseSelectExamTermFirst,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        if(nonSubjectID === ''){
            swal(
                messages.pleaseSelectNonSubjectFirst,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        getNonSubjectLegends(nonSubjectID);
            hideAndShowTable();
            getNonAcademicResult();
            setTimeout(function(){
                iterateStudentListTableForNonAcademic();
             },1000);

    }

    if(__ROUTE__ == 'non-subjects'){
        $('#nonSubjectName').attr('disabled', false);
        $('#submit-add-form-container').removeAttr('disabled');
    }
    if(__ROUTE__ == 'promotions'){
        if(classID == ''){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
        // if(examTermID !== ''){
            // getOverallResult();
            setTimeout(function(){
                iterateStudentListTableForMove();
            },1500)
        // }
    }
    if(__ROUTE__ == 'student-fee-challan'){
        // $('#fee-type-table-body').empty();
        $("#filter-fee-type option").removeAttr("disabled");
    }
    if(__ROUTE__ == 'users'){
        $('#passwordDiv').show();
        $('#password').attr('name','password');
    }
    if(__ROUTE__ == 'contract' || __ROUTE__ == 'terms-conditions' ){
        $('#summernote').summernote('code', '');
        $('#arabicSummernote').summernote('code', '');
    }
    if(schoolID != ''){
        if(__ROUTE__ == 'student-fee-challan'){
            getFeeType();
        }
    }
    showAddFormContainer();
    resetMessages();
});

//Hide add/edit form
$('#hide-add-form-container').on('click', function () {
    idEditButtonClicked = false
    if (optionalFields[__ROUTE__] !== undefined) {
        // tableData.destroy();
        optionalFields[__ROUTE__];
    };

    if(__ROUTE__ === 'conference'){
        $('#endDate').attr('type','hidden')
        $('#endDate').attr('name','')
        isWebLinkExist = false
        hideFields();
    }
    // if(__ROUTE__ === 'students'){
    //     getStudentBySearch()
    // }
    if(__ROUTE__ == 'daily-diary'){
        $('#attachement-div').slideUp(1000);
        $('#document-div').slideUp(1000);
        $('.invite-div').hide();
    }
    if(__ROUTE__ == 'student-fee-challan'){
        $('#fee-type-table-body').empty();
        $("#filter-fee-type option").removeAttr("disabled");
    }
    if(__ROUTE__ == 'non-subjects'){
        $('.non-subjects-legend-form-container').slideUp(1000);
    }
    if(__ROUTE__ === 'new-time-table'){
        getTimeTable();
    }

    if(__ROUTE__ === 'messages'){
        $('#attachmentDiv').removeClass('hidden');
        $('#subject-text').addClass('hidden');
    }

    if(__ROUTE__ === 'parents'){
        $('#taxId').addClass('hidden');
    }

    if(__ROUTE__ === 'quiz-results'){
        $('.hidden-div').addClass('hidden');
        // $('#quizDate').val('');
        $("#filter-students").find('option:first').prop('selected',true);
        $('#filter-students').multiselect('rebuild')
    }

    if(__ROUTE__ === 'subject-time-table'){
        $('#timetableSubjectID').removeAttr('name');
        $('#timetableSubjectID').removeAttr('value');
    }

    $("#multiple option:selected").prop("selected", false);
    $('#multiple').trigger('change');
    hideAddFormContainer();
    editHiddenField.removeAttr('name');
    editHiddenField.removeAttr('value');
    $(this).parents('form:first').trigger('reset');
   __ROUTE__ === 'daily-diary' ? $("#description").text('') : '';

});

$('#hide-academic-event-add-form-container').on('click', function () {
    hideAddFormContainer();
})

$('#reset-add-form').on('click', function () {
    if(__ROUTE__ === 'conference'){
        setTimeout(() => {
            var teacherObj =  $('#filter-class-teachers')
            var studentObj = $('#filter-students')
            var subjectObj  = $('#filter-subjects')
            teacherObj.multiselect('deselectAll');
            teacherObj.multiselect('rebuild')
            studentObj.multiselect('deselectAll',true);
            studentObj.find('option').attr({
                'disabled' : false,
            });
            studentObj.multiselect('rebuild')
            subjectObj.multiselect('deselectAll');
            subjectObj.multiselect('rebuild')
        }, 200);
        // isWebLinkExist = false
        // $('.customWebLinkDiv').hide(500)
        // $('#webLink').removeAttr('data-validation')
        // $('#customLinkCloseBtn').hide()
        // $('.customLinkBtnDiv').html(`
        // <a id="customLinkOpenBtn" class="btn btn-sm btn-primary circle" >Add Custom Link <i class="fa fa-arrow-right"></i> </a>
        // </button>`);
        currentDateTime = moment().add(0,'minutes');
        startMinutes = currentDateTime.format('mm');
        var stTime = round5(startMinutes)
        var matchedValue = stTime
        if(matchedValue <= 9) matchedValue = '0'+matchedValue
        if(matchedValue === 60){
            matchedValue = '05';
        }else if(matchedValue === 00){
            matchedValue = '05'
        }
        startHour = currentDateTime.format('HH');
        $('#startHour').val(startHour)
        $('#startHour').select2()
        $('#startHourInput').val(startHour)
        endHour = currentDateTime.add(2, 'hours');
        endHour = endHour.format('HH');
        $('#startMinute').val(matchedValue)
        $('#startMinute').select2()
        $('#startMinuteInput').val(matchedValue)
        $('#endHour').val(endHour)
        $('#endHour').select2()
        $('#endHourInput').val(endHour)
        currentDateTime = moment().add(10,'minutes');
        endMinutes = currentDateTime.format('mm');
        var enTime = round5(endMinutes);
        if(enTime <= 9) enTime = '0'+enTime
        if(enTime === 60){
            enTime = '05'
        }else if(enTime === 0){
            enTime = '05'
        }
        $('#endMinute').val(enTime)
        $('#endMinute').select2()
        $('#endMinuteInput').val(enTime)
    }
    $(this).parents('form:first').trigger('reset');
    return false;
})

//Make Disable Form Buttons
function disableButtonsToggle() {
    if (submitButton.prop('disabled')) {
        submitButton.removeAttr('disabled');
    } else {
        submitButton.attr('disabled', true);
    }
    if (cancelButton.prop('disabled')) {
        cancelButton.removeAttr('disabled');
    } else {
        cancelButton.attr('disabled', true);
    }
}

//Validate Phone Number
function validatePhoneNumber(event) {
    var regex = new RegExp("^[0-9?.*+*]+$");
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

$('#show-import-excel-form-container').on('click', function(){
    if(__ROUTE__ === 'import-excel'){
        if($('#filter-schools').val() === ''){
            swal(
                messages.pleaseSelectSchool,
                messages.errorMessage,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }

        if($('#filter-branches').val() === ''){
            swal(
                messages.pleaseSelectBranch,
                messages.errorMessage,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }
        $('#add-form-container').hide();
        $('#import-file-form-container').slideDown();
    }
    showImportExcelFormContainer();
});

$('#hide-import-excel-form-container').on('click', function(){
    hideImportExcelFormContainer();
});

function showImportExcelFormContainer() {
    $("#import-file-form-container").slideDown();
    filterFormTitle.text('Import Excel').css({
        'color': '#2fa7ff'
    });
}

function hideImportExcelFormContainer() {
    $("#import-file-form-container").slideUp();
    filterFormTitle.text('Filters').css({
        'color': 'black'
    });
}

// remove empty value from object
function clean(obj) {
    for (var propName in obj) {
        if(__ROUTE__ ===  'student-fee-challan' ){
            if (obj[propName] === undefined || obj[propName] == '') {
                delete obj[propName];
            }
        }else{
            if (obj[propName] === null || obj[propName] === 'undefined' || obj[propName] === '') {
                delete obj[propName];
            }
        }
    }
    return obj;
}

function hideFields(){
    $('#isRecurring').val(0)
    $('#reminderOn').val(0)
    $('#meetingID').removeAttr('value')
    $('#meetingRefNo').removeAttr('value')
    $('#webLink').removeAttr('value')
    $('#recuurence-div').hide()
    $('#reminderTime-div').hide()
    $('#reminderToggle').prop('checked', false);
    $('.weekly').prop('checked', false);
    $('.pattern').prop('checked', false);
    $('#isRecurring').attr('checked', false);
    $('.range-end').attr('checked', false);
    // $('#add-form-error-message').hide()
    var teacherObj =  $('#filter-class-teachers')
    var studentObj = $('#filter-students')
    var subjectObj  = $('#filter-subjects')
    teacherObj.multiselect('deselectAll');
    teacherObj.multiselect('rebuild')
    studentObj.multiselect('deselectAll',true);
    studentObj.find('option').attr({
        'disabled' : false,
    });
    studentObj.multiselect('rebuild')
    subjectObj.multiselect('deselectAll');
    subjectObj.multiselect('rebuild')
    $("input[value=D]").prop('checked', true).trigger('change');
    $('#endDate').val(moment().format('YYYY-MM-D'))
}

function ValidateDecimal(number) {
    var regex = /(?:\d*\.\d{1,2}|\d+)$/;
    if (regex.test(number)) {
        alert("Valid");
    } else {
        alert("Invalid");
    }
}

$(document).on('click', '#loginToastDivCloseBtn', function(){
    // $('#toastMessageDiv').addClass('hidden')
    $('#toastMessageDiv').slideUp(500)
})

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
            b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
  }
