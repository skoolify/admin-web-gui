$(document).ready(function(){
    if($('#filter-users').length > 0){
        getAjaxUsers();
    }
    $('.clockpicker').clockpicker({
        donetext: 'Done'
    });
    getAjaxSchools();
    if(user.isAdmin == 1){
        schoolID = $('#filter-schools').val();
    }
    if(schoolID !== ''){
        getActivities();
    }
})

var bulkArray = [];

function submitActivity(){
    $.ajax({
        url: herokuUrl+'/setActivityArray',
        method: 'POST',
        headers,
        data:{
            'array': bulkArray
        },
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                __ROUTE__ === 'activities' ? $("#filter-users").val('').multiselect('rebuild') : '';
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#activity-form').trigger('reset');
                activityID = '';
                getActivities();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitActivity');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getActivities(){
    let data = {}
    if(activityID !== ''){
        data.activityID = activityID;
    }
    if(branchID !== ''){
        data.branchID = branchID;
    }
    if(userID !== ''){
        data.userID = userID;
    }
    if (tableData) {
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getActivity',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'activityName': response[i].activityName,
                        'instructorName': response[i].fullName,
                        'startTime': response[i].startTime,
                        'endTime': response[i].endTime,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'action' : `
                            <div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${JSON.stringify(response[i])}'><i class="fa fa-pencil"></i></button>
                        ` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-activity" data-activity-id='${response[i].activityID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''} </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getActivities');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'activityName'},
            {'data': 'instructorName'},
            {'data': 'startTime'},
            {'data': 'endTime'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$('#activity-form').on('submit', function(e){
    e.preventDefault();
    if($('#activityName').val() === ''){
        swal(
            messages.pleaseEnterActivityNameFirst,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#startTime').val() === ''){
        swal(
            messages.startTimeIsRequired,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#endTime').val() === ''){
        swal(
            messages.endTimeIsRequired,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    bulkArray = [];
    var formData = $('#filter-form').serializeArray();
    formData = formData.slice(0,2);
    var activitiesData = $('#activity-form').serializeArray();
    let obj = {}
    obj['branchID'] = branchID;
    obj['userID'] = userID;
    activitiesData.forEach(function(val, ind){
        obj[val.name] = val.value;
    })
    bulkArray.push(obj);
    if(bulkArray.length){
        showProgressBar(submitLoader);
        resetMessages();
        submitActivity();
    }
});

$(document).on('click', '.delete-activity', function(){
     activityID = $(this).attr('data-activity-id');
    $.confirm({
        title: messages.confirm,
        content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheActivity+"</br>",
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteActivity();
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

$(document).on('change','#filter-users',function(){
    userID = $(this).val();
    console.log(userID);
    if(userID !== ''){
        getActivities();
    }
})

function deleteActivity(){
    $.ajax({
        url: herokuUrl+'/deleteActivity',
        method: 'POST',
        headers,
        data: {
            activityID: activityID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.activityHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                activityID = '';
                getActivities();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteActivity');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
