
var existingLogo = null;
var isParentExist = false;
var code = undefined;
var tagData = {};
var studentNoteData = {};
var tagsArray = []
var tagsDataArray = []
var isTagEdit = false;
var notesDataArray = {};
$(document).ready(function(){
    getAjaxSchools();
    // getStudents();
    $("#noteTag").select2({
        tags: true,
        width:'280',
        placeholder: "Select a tag",
    });
})

$(document).on('keyup', '#tagsInput', function(){
    var noteTag = $('#tagsInput').val()
    tagsDataArray.forEach(function(value, index){
        if(value.tagText === noteTag){
            $('#tagTitle').val(value.tagText)
            $('#tagUUID').val(value.tagUUID)
        }
    });
    $('#tagText').val(noteTag)
    
})

$(document).on('keyup', '#tagTitle', function(){
    var noteTag = $('#tagTitle').val()
    $('#tagTitleInput').val(noteTag)
})

function resetTagForm(){
    $('#tagsInput').val('')
    $('#tagText').val('')
    $('#studentNote').val('')
    $('#tagTitle').val('')
    $('#isActiveInput').val('')
    $('#tagUUID').val('')
    $('#tagTitleInput').val('')
    $('#isActive').prop('checked', false)
}

$(document).on('click', '#addNewTagBtn', function(){
    if($('#tagText').val() === ''){
        $('.tagFieldDiv').hide()
        resetTagForm()
        $('.addTagFormDiv').slideDown(500)
    }else{
        console.log(tagsDataArray.length)
        if(tagsArray.length > 0){
            var found = tagsArray.indexOf($('#tagText').val())
            if(found === -1){
                submitTag();
            }else{
                $('.successDiv').text($('#tagText').val()+' already exist!')
                $('.tagSuccessDiv').slideDown(500)
                setTimeout(() => {
                    $('.tagSuccessDiv').slideUp(500)
                }, 2000);
            }
        }else{
            submitTag()
        }
    }
})

$(document).on('click', '#editTagBtn', function(){
    isTagEdit = true
    var noteTag = $('#tagsInput').val()
    if(tagsArray.length > 0){
        var found = tagsArray.indexOf(noteTag)
        if(found !== -1){
            tagsDataArray.forEach(function(value, index){
                if(noteTag === value.tagText){
                    $('.tagFieldDiv').hide()
                    $('#tagTitle').val(value.tagText)
                    $('#tagTitleInput').val(value.tagText)
                    $('#tagUUID').val(value.tagUUID)
                    if(value.isActive == '0'){
                        $('#isActiveInput').val(0)
                        $('#isActive').prop('checked', false)
                    }else{
                        $('#isActiveInput').val(1)
                        $('#isActive').prop('checked', true)
                    }
                    $('.addTagFormDiv').slideDown(500)
                }
            });
        }else{
            $('.successDiv').text('Tag not exist!')
            $('.tagSuccessDiv').slideDown(500)
            setTimeout(() => {
                $('.tagSuccessDiv').slideUp(500)
            }, 2000);
        }
    }else{
        $('.successDiv').text('Tag not exist!')
        $('.tagSuccessDiv').slideDown(500)
        setTimeout(() => {
            $('.tagSuccessDiv').slideUp(500)
        }, 2000);
    }
});

$(document).on('click', '.closeTagFormBtn', function(){
    $('.addTagFormDiv').slideUp(500)
    $('.tagFieldDiv').show()
})

$(document).on('click', '.submitTagFormBtn', function(){
    submitTag()
})

$(document).on('change','#noteTag', function(){
    var tagValue = $(this).val()
    if(tagValue == 'createNew'){ 
        submitTag()
    }else if(tagValue == 'createAndEdit'){
    }
})

$(document).on('click','.isActive',function(){
    if($(this).prop('checked')){
        $('#isActiveInput').val(1)
    }else{
        $('#isActiveInput').val(0)
    }
})

function submitTag(){
    tagData = {}
    tagData['branchID'] = branchID
    if($('#tagTitle').val() !== ''){
        tagData['tagText'] = $('#tagTitle').val()
    }else{
        tagData['tagText'] = $('#tagText').val()
    }
    tagData['userID'] = userID 
    if($('#isActiveInput').val() !== '' && $('#isActiveInput').val() != '0'){
        tagData['isActive'] = $('#isActiveInput').val()
    }else{
        tagData['isActive'] = 1
    } 
    if($('#tagUUID').val() !== ''){
        tagData['tagUUID'] = $('#tagUUID').val()
    }

    $.ajax({
        url: herokuUrl+'/setStudentNotesTag',
        method: 'POST',
        headers,
        data: tagData,
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                console.log('tag has been added')
                resetTagForm()
                getTags()
                if(isTagEdit === true){
                    $('.successDiv').text('Tag updated successfully!')
                    $('.tagSuccessDiv').slideDown(500)
                    setTimeout(() => {
                        $('.tagSuccessDiv').slideUp(500)
                    }, 2000);
                }else{
                    $('.successDiv').text('Tag added successfully!')
                    $('.tagSuccessDiv').slideDown(500)
                    setTimeout(() => {
                        $('.tagSuccessDiv').slideUp(500)
                    }, 2000);
                }
                
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('submitTag')
            window.requestForToken(xhr)
        }
    });
}

function getTags(){
    $.ajax({
        url: herokuUrl+'/getActiveStudentTags',
        method: 'POST',
        headers,
        data:{
            branchID:branchID,
        },
        beforeSend: function(){
            tagsArray = []
            tagsDataArray = []
            $('#noteTag').empty()
        },
        success: function(res){
            response = res.response
            console.log(response)
            if(response && !response.internalMessage){
                response.forEach(function(value, index){
                    tagsArray.push(value.tagText)
                    tagsDataArray.push(value)
                    $('#noteTag').prepend(`<option value="${value.tagUUID}">${value.tagText}</option>`)
                })
                autocomplete(document.getElementById("tagsInput"), tagsArray);
                $('#noteTag').append(`
                    <option value=""><a href="">Create New</a></option>
                    <option value="createAndEdit">Create and Edit</option>
                    
                `)
            }   
        },
        error: function(xhr){
            invokedFunctionArray.push('getTags')
            window.requestForToken(xhr)
        }
    });
}

function getStudentNotes(){
    $.ajax({
        url: herokuUrl+'/getStudentNotes',
        method: 'POST',
        headers,
        data:notesDataArray,
        beforeSend: function(){
            tagsArray = []
            tagsDataArray = []
            $('#noteTag').empty()
        },
        success: function(res){
            response = res.response
            console.log(response)
            return
            if(response && !response.internalMessage){
                response.forEach(function(value, index){
                    tagsArray.push(value.tagText)
                    tagsDataArray.push(value)
                })
                autocomplete(document.getElementById("tagsInput"), tagsArray);
            }   
        },
        error: function(xhr){
            invokedFunctionArray.push('getStudentNotes')
            window.requestForToken(xhr)
        }
    });
    
}

$(document).on('click', '.add-note', function(){
    notesDataArray = {}
    resetTagForm()
    var studentUUID = $(this).attr('data-studentUUID');
    $('#studentUUID').val(studentUUID)
    notesDataArray['studentUUID'] = studentUUID
    notesDataArray['branchID'] = branchID
    getTags()
    getStudentNotes()
    $('#addNoteModal').modal('toggle');
    $('#addNoteModal').modal('show');

})

$(document).on('click', '.view-notes-list', function(){
    notesDataArray = {}
    var studentUUID = $(this).attr('data-studentUUID');
    notesDataArray['studentUUID'] = studentUUID
    notesDataArray['branchID'] = branchID
    getStudentNotes()
    $('#notesListModal').modal('toggle');
    $('#notesListModal').modal('show');

})

$(document).on('click','#submitNotesDataBtn', function(){
    submitStudentNote()
})

function submitStudentNote(){
    studentNoteData = {}
    if(tagsArray.length > 0){
        var found = tagsArray.indexOf($('#tagText').val())
        if(found !== -1){
            studentNoteData['tagUUID'] = $('#tagUUID').val()
        }else{
            $('.noteSubmitError').text(`Tag "${$('#tagText').val()}" does not exist!`)
            $('.noteSubmitError').slideDown(500)
            setTimeout(() => {
                $('.noteSubmitError').slideUp(500)
            }, 2000);
            return false
        }
    }
    if($('#studentUUID').val() !== ''){
        studentNoteData['studentUUID'] = $('#studentUUID').val() 
    }
    if($('#studentNote').val() !== ''){
        studentNoteData['studentNotes'] = $('#studentNote').val() 
    }else{
        $('.noteSubmitError').text('Please fill the form first!')
        $('.noteSubmitError').slideDown(500)
        setTimeout(() => {
            $('.noteSubmitError').slideUp(500)
        }, 2000);
        return false
    }
    studentNoteData['userID'] = userID 
    $.ajax({
        url: herokuUrl+'/setStudentNotes',
        method: 'POST',
        headers,
        data: studentNoteData,
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                $('.noteSubmitError').text('Note has been added successfully!')
                $('.noteSubmitError').slideDown(500)
                setTimeout(() => {
                    $('.noteSubmitError').slideUp(500)
                }, 2000);
                resetTagForm()
                setTimeout(() => {
                    $('#addNoteModal').modal('toggle');
                    $('#addNoteModal').modal('hide');
                }, 1000);
                // getStudents()
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('submitTag')
            window.requestForToken(xhr)
        }
    });
}

$(document).on('click', '.parent-detail', function () {
   
    var detailStudentID = $(this).attr('data-recordID');
    if (parentDetailTable) {
        parentDetailTable.destroy();
    }

    parentDetailTable = $('#parent-detail-table').DataTable({
        'ajax'       : {
            url: herokuUrl+'/getParentByStudentID',
            type: 'POST',
            headers,
            beforeSend: function(){
                // $.LoadingOverlay("show");
            },
            data: { 
                studentID: detailStudentID,
            },
            dataSrc: function(res){
                response = parseResponse(res);
                // isParentExist = response.length > 0 ? true : false; 
                var return_data = new Array();  
                for(var i = 0; i< response.length; i++){
                    var regDate = moment.utc(response[i].registrationDate);
                    regDate = regDate.local().format(__DATEFORMAT__);
                    var parentTyp = '';
                    if(response[i].parentType == 'M'){
                        parentTyp = 'Mother';
                    }else if(response[i].parentType == 'F'){
                        parentTyp = 'Father';
                    }else if(response[i].parentType == 'G'){
                        parentTyp = 'Guardian';
                    }else{
                        parentTyp = 'Un known';
                    }
                    // console.log(typeof regDate);
                    return_data.push({
                        'sNo' : i+1,
                        'parentName'   :           `<small>${response[i].parentName}</small>`,
                        'email'        :        `<small>${response[i].email}</small>`,
                        'mobileNumber'   :      `<small>${response[i].mobileNumber}</small>`,
                        'parentCNIC'   :        `<small>${response[i].parentCNIC}</small>`,
                        'parentType'   :        `<small>${parentTyp}</small>`,
                        'registrationDate':     `<small>${ regDate !== null && regDate !== 'null' && regDate !== 'Invalid date' ? regDate : 'Not Registered'}</small>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'parentName'},
            {'data': 'email'},
            {'data': 'mobileNumber'},
            {'data': 'parentCNIC'},
            {'data': 'parentType'},
            {'data': 'registrationDate'}
        ]
    });
//    setTimeout(()=>{
//     $.LoadingOverlay("hide");
//     if(isParentExist){
        $('#fathersDetailModal').modal('toggle');
        $('#fathersDetailModal').modal('show');
//     }else{
//         Swal(
//             'Error!',
//             'Not Registered.',
//             'error'
//         )
//         return false;
//     }
//    },1000)
});
function submitStudent(){
    $.ajax({
        url: herokuUrl+'/setStudents',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);
                tableData.destroy();

                $('#existing-school-logo').addClass('hidden');
                $('#upload-school-logo').removeClass('hidden');
                $('#hiddenPicture').val('');
                $('#existing-logo').attr('src', '');
                $('#reset-logo-image-edit').addClass('hidden');

                $('form#students-form').trigger('reset');
                getStudents();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitStudent')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.responseJSON && xhr.responseJSON !== null){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.reason}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
$('.search-student').click(() => {
    var searchByName = $('#searchByName').val();
    var searchByRollNumber = $('#searchByRollNumber').val();
    if(schoolID === ''){
        $('#filter-schools').addClass('is-invalid');
    }else{
        $('#filter-schools').removeClass('is-invalid');
    } 
    if(branchID === ''){
        $('#filter-branches').addClass('is-invalid');
    }else{
        $('#filter-branches').removeClass('is-invalid');
    } 
    
    
    if(searchByName === '' && searchByRollNumber == '' ){
        $('#searchByRollNumber').addClass('is-invalid');
    }else{
        $('#searchByRollNumber').removeClass('is-invalid');
        getStudentBySearch(searchByName, searchByRollNumber);
    }
});
function getStudentBySearch(searchByName = null, searchByRollNumber = null){
    var data = {
        schoolID: schoolID,
        branchID: branchID,
    };

    if(searchByName){
        data.studentName = searchByName
    }
    if(searchByRollNumber){
        data.rollNo = searchByRollNumber
    }   
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        'ajax'       : {
            url: herokuUrl+'/getStudentSearch',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();                
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'dob' : moment(response[i].dob).format(__DATEFORMAT__),
                        'classShift' : response[i].classShift,
                        'classSection' : `${response[i].classAlias}-${response[i].classSection}`,
                        'rollNo' : response[i].rollNo,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-json='${JSON.stringify(response[i])}'><i class="fa fa-pencil"></i></button>
                        <button class="btn-primary btn btn-xs parent-detail" data-toggle="tooltip" title="View Parent Detail" data-recordID='${response[i].studentID}'><i class="fa fa-list"></i></button>` : ''}`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudentBySearch')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'dob'},
            {'data': 'classShift'},
            {'data': 'classSection'},
            {'data': 'rollNo'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });
}
$('#hide-add-form-container').on('click', function(){
    $('#existing-school-logo').addClass('hidden');
    $('#hiddenPicture').val('');
    $('#existing-logo').attr('src', '');
    $('#upload-school-logo').removeClass('hidden');
});

$(document).on('click', '.list-record-edit', function(){
    let json = JSON.parse($(this).attr('data-json'));
    existingLogo = json.picture;
    $('#upload-school-logo').addClass('hidden');
    $('#existing-school-logo').removeClass('hidden');
    $('#existing-school-logo img').attr('src', 'data:image/*;base64,'+existingLogo);
});

$('#imgupload').on('change', readFile);

$('#existing-logo').click(function(){
    $('#imgupload').trigger('click');
});

$('#camera-icon').click(function(){
    $('#imgupload').trigger('click');
});

$('#reset-logo-image').on('click', function(){
    $('#logoImg').val('');
    $('#hiddenPicture').val('');
    $(this).addClass('hidden');
})

function readFile() {    
    resetMessages();
    $.LoadingOverlay("show");
    var tempPicture = $(this);
    var formData = new FormData();
    var files = this.files[0];
    
    setTimeout(function(){
        formData.append('logoImg', files, 'image');
        formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: __BASE__+'/students/picture',
            type: 'POST',
            data: formData,
            async: false,
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
                showProgressBar(submitLoader);
                disableButtonsToggle();
            },
            success: function(xhr){
                if(xhr){
                    if(xhr.picture){
                        let code = xhr.picture.split('base64,');
                        $('#hiddenPicture').val(code[code.length - 1]);
                        if(existingLogo === null){
    
                            if(tempPicture.val() === ''){
                                $('#hiddenPicture').val('');
                                $('#reset-logo-image').addClass('hidden');
                            }                            
                        
                            if(tempPicture.val() !== ''){
                                $('#reset-logo-image').removeClass('hidden');
                            }
                        }else{
                            if(tempPicture.val() === ''){
                                $('#hiddenPicture').val('');
                                $('#reset-logo-image-edit').addClass('hidden');
                            }
                        
                            if(tempPicture.val() !== ''){
                                $('#reset-logo-image-edit').removeClass('hidden');
                            }
                        }
    
                        if(existingLogo !== null){
                            setTimeout(function(){
                                $('#existing-logo').attr('src', 'data:image/*;base64,'+$("#hiddenPicture").val());
                            }, 700);
                            $('#imgupload').val('');
                        }
                    }
                }
                $.LoadingOverlay("hide");
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            },
            error: function(xhr){
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    let errors = xhr.responseJSON.errors;
                    $.each(errors, function(key, val){
                        $('#add-form-errors').html(`<p class="text-danger"><strong>${val[0]}</strong></p>`);
                    });
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            }
        });
    }, 200)

    return false;
}

$('#logoImg').on('change', readFile);

function getStudents(){
    // if(tableData){
    //     tableData.destroy()
    // }
    tableData = $('#example44').DataTable({
        'ajax'       : {
            url: herokuUrl+'/getAllClassStudents',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: { 
                schoolID: schoolID,
                branchID: branchID,
                classLevelID: classLevelID,
                classID: classID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();                
                for(var i = 0; i< response.length; i++){
                    var dob = moment.utc(response[i].dob);
                   
                    dob = dob.local().format('DD-MMM-YYYY')
                    rollNumber = response[i].rollNo.substring(0, 8)
                    if(response[i].gender === 'M'){
                        gender = 'Male'
                    }else{
                        gender = 'Female'
                    }
                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'dob' : typeof dob !== 'undefined' &&  dob !== 'Invalid date' ? dob : '-',
                        'gender' : gender,
                        // 'classShift' : response[i].classShift,
                        'classSection' : `${response[i].classAlias}-${response[i].classSection}`,
                        'rollNo' : `<span data-toggle="tooltip" title="${response[i].rollNo}">
                        ${response[i].rollNo.length <= 7  ? response[i].rollNo : rollNumber+'...'}
                        </span>`,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''} 
                        <button class="btn-primary btn btn-xs parent-detail" data-toggle="tooltip" title="View Parent Detail" data-recordID='${response[i].studentID}'><i class="fa fa-list"></i></button>

                        <button class="btn-primary btn btn-xs add-note" data-toggle="tooltip" title="Add Note" data-studentUUID='${response[i].studentUUID}'><i class="fa fa-comment"></i></button>

                        <button class="btn-danger btn btn-xs view-notes-list" data-toggle="tooltip" title="View Notes List" data-studentUUID='${response[i].studentUUID}'><i class="fa fa-list"></i></button>`
                         
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudents')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'dob'},
            {'data': 'gender'},
            // {'data': 'classShift'},
            {'data': 'classSection'},
            {'data': 'rollNo'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$('#students-form').on('submit', function(e){
    e.preventDefault();
    let formData = $('#filter-form').serializeArray();
    let moduleData = $('#students-form').serializeArray();
    var obj = {};
    formData.forEach((value, index) => {
        obj[value.name] = value.value;
    });
    moduleData.forEach((value, index) => {
        obj[value.name] = value.value;
    });
    showProgressBar(submitLoader);
    data = clean(obj);
    resetMessages();
    setTimeout(function(){
        submitStudent();
    }, 700);
});
