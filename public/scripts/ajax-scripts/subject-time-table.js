$(document).ready(function(){
    if($('#filter-class-room').length > 0 ){
        getActiveClassRooms();
    }
    getAjaxSchools();
    setClockpickerClass();
})
function setClockpickerClass(){
    $('.clockpicker').each(function(){
        $(this).clockpicker({
            donetext: 'Done'
        });
    })
}
function getSubjectTimeTable(){

    weekdayNo = $('#filter-week-day').val();
    classTeacherID = $('#filter-class-teachers').val();
    data = {};
    if(user.isAdmin === 1 && $('#filter-branches').val() !== ''){
        data.branchID = $('#filter-branches').val();
    }else{
        data.branchID = user.branchID
    }
    if(weekdayNo !== ''){
        data.weekdayNo = weekdayNo;
    }

    if(subjectID !== ''){
        data.subjectID = subjectID;
    }else{
        delete data['subjectID']
    }

    if(classTeacherID !== ''){
        data.userID = classTeacherID;
    }else{
        delete data['userID']
    }

    if(subjectTimeTableData){
        subjectTimeTableData.destroy();
    }

    subjectTimeTableData = $('#example44').DataTable({
        'ajax':{
            url: herokuUrl + '/getSubjectTimeTable',
            method: 'POST',
            headers,
            data,
            dataSrc: function (res) {
                response = parseResponse(res);
                var return_data = new Array();
                for (let i = 0; i < response.length; i++) {
                    return_data.push({
                        'sNo': i + 1,
                        'subjectName': response[i].subjectName,
                        'weekDayName': response[i].weekDayName,
                        'roomID': response[i].roomName,
                        'fullname': response[i].fullname,
                        'startTime': response[i].startTime,
                        'endTime': response[i].endTime,
                        'action' : `
                            <div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${JSON.stringify(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-subject-timetable" data-subject-timetable-id='${response[i].timetableSubjectID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''} </div>`
                        // 'startTime': addCommas(element.feeAmount),
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getSubjectTimeTable')
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
            }
        },
        "columns": [
            {'data': 'sNo'},
            {'data': 'subjectName'},
            {'data': 'weekDayName'},
            {'data': 'roomID'},
            {'data': 'fullname'},
            {'data': 'startTime'},
            {'data': 'endTime'},
            {'data': 'action'},
        ]
    });
}

// $(document).on('click', '.list-record-editing', function(){
//     let json = JSON.parse($(this).attr('data-json'));
//     $('#filter-class-room').append('<option value="'+roomID+'" selected>'+roomID+'</option>')
// });

$(document).on('click', '.delete-subject-timetable', function(){
    timetableSubjectID = $(this).attr('data-subject-timetable-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheSubjectTimeTable+'</br>',
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteSubjectTimeTable();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteSubjectTimeTable(){
    $.ajax({
        url: herokuUrl+'/deleteSubjectTimeTable',
        method: 'POST',
        headers,
        data: {
            timetableSubjectID: timetableSubjectID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.subjectTimeTableHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                timetableSubjectID = '';
                getSubjectTimeTable();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteSubjectTimeTable')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

function submitSubjectTimeTable(){
    weekdayNo = $('#filter-week-day').val();
    if(subjectID === ''){
        swal(
            messages.error,
            messages.pleaseSelectSubject,
            'error'
        );
        return false;
    }
    classTeacherID = $('#filter-class-teachers').val();
    if(classTeacherID !== ''){
        data.userID = classTeacherID
    }

    if(weekdayNo === ''){
        swal(
            messages.error,
            messages.pleaseSelectWeekday,
            'error'
        );
        return false;
    }
    delete data['classTeacherID']
    $.ajax({
        url: herokuUrl+'/setSubjectTimetable',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);

                $('form#subject-time-table-form').trigger('reset');
                getSubjectTimeTable();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitSubjectTimeTable')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$('#subject-time-table-form').on('submit', function(e){
    e.preventDefault();
    let formData = $('#filter-form').serializeArray();
    let moduleData = $('#subject-time-table-form').serializeArray();
    var obj = {};
    formData.forEach((value, index) => {
        obj[value.name] = value.value;
    });
    moduleData.forEach((value, index) => {
        obj[value.name] = value.value;
    });
    data = clean(obj);
    resetMessages();
    setTimeout(function(){
        submitSubjectTimeTable();
    }, 700);
});

$('#filter-week-day').on('change', function(){
    getSubjectTimeTable();
})
