
// var existingLogo = null;
// var code = undefined;
$(document).ready(function(){
    getAjaxSchools();
})

var classArray = [];
var sectionArray = [];
var classSectionArray = [];

$('#filter-branches').on('change', function(){
    branchID = $(this).val()
    getBranchClasses(branchID)
})

function getBranchClasses(branchID = null) {
    let data = {};
    if (branchID) {
        data.branchID = branchID
    }

    $.ajax({
        url: herokuUrl+'/getBranchClasses',
        type: 'POST',
        headers,
        beforeSend: function () {
            classArray = []
            sectionArray = []
            classSectionArray = []
        },
        data,
        success: function (res) {
            if (!res.error) {
                res.response.forEach((value, index) => {
                    classArray.push(value.classAlias)
                    sectionArray.push(value.classSection)
                    classSectionArray.push(value.classAlias+value.classSection)
                })
            }
        },
        error: function (err) {
            invokedFunctionArray.push('getBranchClasses');
            window.requestForToken(err)
            console.log(err);
        }
    })
}

var importData = [];

$('#import-students-excel-form').on('submit', function(e){
    schoolID = $('#filter-schools').val();
    branchID = $('#filter-branches').val();
    if($('#filter-schools').val() === ''){
        swal(
            messages.pleaseSelectSchool,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#filter-branches').val() === ''){
        swal(
            messages.pleaseSelectBranch,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    $.LoadingOverlay("show");
e.preventDefault();
var filterData = $('#filter-form').serializeArray();
var formData = new FormData($(this)[0]);
formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
formData.append('schoolID', schoolID);
formData.append('branchID', branchID);
formData.append('classArray', classArray);
formData.append('sectionArray', sectionArray);
formData.append('classSectionArray', classSectionArray);
setTimeout(() => {
    $.ajax({
        url: __BASE__+'/import-excel/file',
        type: 'POST',
        data: formData,
        async: false,
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function(){
            //disableButtonsToggle();
            // $.LoadingOverlay("show");
        },
        success: function(res){
            if(res.errors){
                if(res.data.length){
                    let response = res.data;
                    importData = response;
                    let excelSheetTable = $('#excel-sheet-table tbody');
                    excelSheetTable.empty();
                    $('#errorDiv').text(messages.error+' : '+messages.pleaseDownloadTheAttachedTemplateToFormatDataAccordingly);
                    $('#errorDiv').removeClass('hidden')

                    response.forEach((val, index) => {
                        excelSheetTable.append(`
                        <tr style="color:${res.comparison[index].error ? 'red' : null}" data-toggle="tooltip" title="${messages.dataDuplicationNotAllowed}">
                            <td> ${ index+1 } </td>
                            <td style="color:${val.studentNameError ? 'red' : null}" data-toggle="tooltip" title="${val.studentNameError ? messages.studentNameIsRequired : ''}"> ${ val.studentName } </td>

                            <td style="color:${val.genderError ? 'red' : null}" data-toggle="tooltip" title="${val.genderError ? messages.genderMustBeMOrF : ''}"> ${ val.gender } </td>

                            <td style="color:${val.classLevelNameError ? 'red' : null}" data-toggle="tooltip" title="${val.classLevelNameError ? messages.class+' '+val.classLevelName+' '+messages.doesNotExist : ''}"> ${ val.classLevelName } </td>


                            <td style="color:${val.classSectionError ? 'red' : null}" data-toggle="tooltip" title="${val.classSectionError ? messages.section+' '+val.classSection+' of '+val.classLevelName+' '+messages.doesNotExist : ''}"> ${ val.classSection } </td>

                            <td style="color:${val.rollNoError ? 'red' : null}" data-toggle="tooltip" title="${val.rollNoError ? messages.rollNumber+' '+val.rollNo+' '+messages.alreadyInUsed : ''}"> ${ val.rollNo } </td>

                            <td style="color:${val.dobError ? 'red' : null}" data-toggle="tooltip" title="${val.dobError ? messages.DOBIsRequired : ''}"> ${ val.dob } </td>

                            <td> ${ val.fatherName } </td>
                            <td> ${ val.fatherCNIC } </td>

                            <td style="color:${val.fatherMobileNoError ? 'red' : null}" data-toggle="tooltip" title="${val.fatherMobileNoError ? messages.fatherMobileNumberLengthMustBeGreater : ''}"> ${ val.fatherMobileNo } </td>
                            <td> ${ val.fatherEmail } </td>
                            <td> ${ val.motherName } </td>
                            <td> ${ val.motherCNIC } </td>

                            <td style="color:${val.motherMobileNoError ? 'red' : null}" data-toggle="tooltip" title="${val.motherMobileNoError ? messages.motherMobileNumberLengthMustBeGreater : ''}"> ${ val.motherMobileNo } </td>

                            <td> ${ val.motherEmail } </td>
                        </tr>`);
                    })
                    $.LoadingOverlay("hide");
                    $('#exampleModal').modal('toggle');
                    $('#exampleModal').modal('show');
                    $('#proceed-excel-sheet').attr('disabled', true)
                }
            }else{
                if(res.data.length){
                    let response = res.data;
                    importData = response;
                    let excelSheetTable = $('#excel-sheet-table tbody');
                    excelSheetTable.empty();
                    $('#errorDiv').addClass('hidden')

                    response.forEach((val, index) => {
                        excelSheetTable.append(`
                        <tr>
                        <td> ${ index+1 } </td>
                        <td style="color:${val.studentNameError ? 'red' : null}" data-toggle="tooltip" title="${val.studentNameError ? messages.studentNameIsRequired : ''}"> ${ val.studentName } </td>
                        <td style="color:${val.genderError ? 'red' : null}" data-toggle="tooltip" title="${val.genderError ? messages.genderMustBeMOrF : ''}"> ${ val.gender } </td>
                        <td style="color:${val.classLevelNameError ? 'red' : null}" data-toggle="tooltip" title="${val.classLevelNameError ? messages.class+' '+val.classLevelName+' '+messages.doesNotExist : ''}"> ${ val.classLevelName } </td>


                        <td style="color:${val.classSectionError ? 'red' : null}" data-toggle="tooltip" title="${val.classSectionError ? messages.section+' '+val.classSection+' of '+val.classLevelName+' '+messages.doesNotExist : ''}"> ${ val.classSection } </td>

                        <td style="color:${val.rollNoError ? 'red' : null}" data-toggle="tooltip" title="${val.rollNoError ? messages.rollNumber+' '+val.rollNo+' '+messages.alreadyInUsed : ''}"> ${ val.rollNo } </td>

                        <td style="color:${val.dobError ? 'red' : null}" data-toggle="tooltip" title="${val.dobError ? messages.DOBIsRequired : ''}"> ${ val.dob } </td>

                        <td> ${ val.fatherName } </td>
                        <td> ${ val.fatherCNIC } </td>

                        <td style="color:${val.fatherMobileNoError ? 'red' : null}" data-toggle="tooltip" title="${val.fatherMobileNoError ? messages.fatherMobileNumberLengthMustBeGreater : ''}"> ${ val.fatherMobileNo } </td>
                        <td> ${ val.fatherEmail } </td>
                        <td> ${ val.motherName } </td>
                        <td> ${ val.motherCNIC } </td>

                        <td style="color:${val.motherMobileNoError ? 'red' : null}" data-toggle="tooltip" title="${val.motherMobileNoError ? messages.motherMobileNumberLengthMustBeGreater : ''}"> ${ val.motherMobileNo } </td>

                        <td> ${ val.motherEmail } </td>
                        </tr>`);
                    })
                    $.LoadingOverlay("hide");
                    $('#exampleModal').modal('toggle');
                    $('#exampleModal').modal('show');
                    $('#proceed-excel-sheet').attr('disabled', false)
                }

            }
        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
            if(xhr.status === 422){
                let errors = xhr.responseJSON.errors;
                $.each(errors, function(key, val){
                    $('#add-form-errors').html(`<p class="text-danger"><strong>${val[0]}</strong></p>`);
                });
            }
        }
    });
},1000)

});

$('#proceed-excel-sheet').on('click', function(){
    UploadParentStudentArray()
})

function UploadParentStudentArray(){

    $.ajax({
        url: herokuUrl+'/UploadParentStudentArray',
        method: 'POST',
        headers,
        data: {'array': importData},
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();

                $('form#import-students-excel-form').trigger('reset');
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

