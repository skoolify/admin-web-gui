var offset = 'Asia/Karachi'
var date = ''

$(document).ready(function(){
    getAjaxSchools();
    date = new Date();
    if(user.isAdmin != 1){
        getFormList()
    }
    offset = Intl.DateTimeFormat().resolvedOptions().timeZone
})

$(document).on('click','.isActiveCheck',function(){
    if($(this).prop('checked')){
        $('#isActive').val(1)
    }else{
        $('#isActive').val(0)
    }
})

function createObjectFromAnotherObject(oldObject){
    var newObject = {};
    for (var propName in oldObject) {
        newObject[propName] = oldObject[propName];
    }
    return newObject;
}

$('#form-management-form').on('submit', function(e){
    e.preventDefault();
    obj = {}
    classLevelID = $('#filter-classLevels').val();
    classID = $('#filter-classes').val();
    studentID = $('#filter-students').val();

    formData = $('#form-management-form').serializeArray();
    formData.forEach(function(value, index){
        if(value.name !== '_token'){
            obj[value.name] = value.value;
        }
    });
    if($("#filter-schools :selected").val() != ''){
        obj.schoolID = $("#filter-schools :selected").val()
    }else{
        obj.schoolID = user.schoolID
    }

    if($("#filter-branches :selected").val() != ''){
        obj.branchID = $("#filter-branches :selected").val()
    }else{
        obj.branchID = user.branchID
    }
    if(!$('#filter-classLevels').find('option:first').prop('selected')){
        if(classLevelID.length === 0){
            swal(
                messages.pleaseSelectClass,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
    }

    if(!$('#filter-classLevels').find('option:first').prop('selected')){
        if(classID.length === 0){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
    }

    if(!$('#filter-classLevels').find('option:first').prop('selected') && !$('#filter-classes').find('option:first').prop('selected')){
        if(studentID.length === 0){
            swal(
                messages.pleaseSelectStudent,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
    }

    if($("#editHiddenField").val() != ''){
        obj.formUUID = $("#editHiddenField").val()
    }

    // if($("#filter-classLevels :selected").val() == 0){
    //     obj.classLevelID = $("#filter-classLevels :selected").val()
    // }

    // if($("#filter-classes :selected").val() == 0){
    //     obj.classID = $("#filter-classes :selected").val()
    // }

    obj.timeZone = offset;
    var bulkArray = [];
    if(messagesClassLevelArray.length > 0){
        messagesClassLevelArray.forEach(function(classLevelsData, classLevelsIndex){
            if(classLevelsData != 0)
                obj.classLevelID = Number(classLevelsData);
            if(typeof messagesClassArray[classLevelsData] != 'undefined' && messagesClassArray[classLevelsData].length > 0 &&  Number(messagesSelectedClassesArray[0]) !== 0){
                messagesClassArray[classLevelsData].forEach((cls, index) => {
                    if(messagesSelectedClassesArray.indexOf(String(cls.classID)) > -1){
                        var classObject = {};
                        classObject = createObjectFromAnotherObject(obj);
                        classObject.classID = Number(cls.classID);
                        if(typeof messagesStudentArray[cls.classID] != 'undefined' && typeof messagesSelectedStudentsArray != 'undefined' && messagesStudentArray[cls.classID].length > 0 && Number(messagesSelectedStudentsArray[0]) !== 0 ){
                            var studentsArray = []
                            var studentObject = {};
                            studentObject =  createObjectFromAnotherObject(classObject);
                            messagesStudentArray[cls.classID].forEach((student, i) => {
                                if(messagesSelectedStudentsArray.indexOf(String(student.studentID)) > -1){
                                    studentsArray.push(Number(student.studentID))
                                }
                            })
                            studentObject.studentList = studentsArray.join(',')
                            bulkArray.push(clean(studentObject));
                        }else{
                            bulkArray.push(clean(classObject));
                        }
                    }
                })
            }else{
                bulkArray.push(clean(obj));
            }
        })
    }else if(messagesSelectedClassesArray.length > 0){
        messagesSelectedClassesArray.forEach((cls, index) => {
            if(messagesSelectedClassesArray.indexOf(String(cls)) > -1){
                var classObject = {};
                classObject = createObjectFromAnotherObject(obj);
                if(cls != 0)
                    classObject.classID = Number(cls);
                if(typeof messagesStudentArray[cls] != 'undefined' && typeof messagesSelectedStudentsArray != 'undefined' && messagesStudentArray[cls].length > 0 && Number(messagesSelectedStudentsArray[0]) !== 0 ){
                    var studentsArray = []
                    var studentObject = {};
                    studentObject =  createObjectFromAnotherObject(classObject);
                    messagesStudentArray[cls].forEach((student, i) => {
                        if(messagesSelectedStudentsArray.indexOf(String(student.studentID)) > -1){
                            studentsArray.push(Number(student.studentID))
                        }
                    })
                    studentObject.studentList = studentsArray.join(',')
                    bulkArray.push(clean(studentObject));
                }else{
                    bulkArray.push(clean(classObject));
                }
            }
        })
    }else{
        bulkArray.push(clean(obj));
    }

    showProgressBar(submitLoader);
    // resetMessages();
    submitForm(bulkArray);
});

function submitForm(bulkArray){
    $.ajax({
        url: herokuUrl+'/setFormArray',
        method: 'POST',
        headers,
        data: {'array': bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('#form-management-form').trigger('reset');
                getFormList();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitForm')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getFormList(){
    data = {}
    if(user.isAdmin == 1){
        if($('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val();
        }else{
            data.schoolID = user.schoolID;
        }

        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            data.branchID = user.branchID;
        }
    }else{
        data.schoolID = user.schoolID;
        data.branchID = user.branchID;
    }

    if(Array.isArray(classLevelID)){
        if(classLevelID.length >= 1 && classLevelID != '0'){
            data.classLevelID = classLevelID.join(',');
        }
    }else{
        if(Number(classLevelID) !== 0)
            data.classLevelID =  classLevelID;
    }

    if(Array.isArray(classID)){
        var index = classID.indexOf('0');
        if(index >= 0){
            classID.splice(index,1);
            if(classID.length)
                data.classID = classID.join(',');
        }
    }else{
        if(Number(classID) !== 0)
            data.classID =  classID;
    }

    if(tableData){
        tableData.destroy();
    }

    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/getFormList',
            type: 'POST',
            headers,
            data,
            beforeSend: function(){
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i < response.length; i++){
                    var startDate = response[i].startDateTime
                    startDate = moment.utc(startDate);
                    startDate = startDate.local().format('DD-MMM-YYYY h:mm:ss A')
                    // startDate = new Date(startDate);
                    // startDate.setHours((startDate.getHours()+5))
                    // startDate.toISOString().slice(0,16)

                    var endDate = response[i].endDateTime
                    endDate = moment.utc(endDate);
                    endDate = endDate.local().format('DD-MMM-YYYY h:mm:ss A')
                    // endDate = new Date(endDate);
                    // endDate.setHours((endDate.getHours()+5))
                    // endDate.toISOString().slice(0,16)

                    return_data.push({
                        'sNo' : i+1,
                        'formName': response[i].formName,
                        // 'formURL': response[i].formURL,
                        'startDate': startDate,
                        'endDate': endDate,
                        'recipientValue': response[i].recipientValue,
                        // 'classSection': response[i].classAlias+'-'+response[i].classSection,
                        // 'schoolName': response[i].schoolName,
                        // 'timezone': response[i].timezone,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        // 'studentList': `<a data-student-list='${response[i].studentList}' class="btn btn-primary btn-sm linked-user-details-button"><i class="fa fa-list"></i> View</a>`,
                        'action' : `
                        <div class="btn-group">
                        ${__ACCESS__ ?
                            `<button class="btn-primary btn btn-xs form-list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-form" data-form-id='${response[i].formUUID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        <div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getFormList');
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                if(xhr.status === 400){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'formName'},
            // {'data': 'formURL'},
            {'data': 'startDate'},
            {'data': 'endDate'},
            {'data': 'recipientValue'},
            // {'data': 'classSection'},
            // {'data': 'schoolName'},
            // {'data': 'timezone'},
            {'data': 'isActive'},
            // {'data': 'studentList'},
            {'data': 'action'}
        ]
    });
}

$(document).on('click','.form-list-record-edit' ,function(){
    if($('#filter-classes').val() == ''){
        swal(
            messages.pleaseSelectSection,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    let data = JSON.parse($(this).attr('data-json'))
    var keys = Object.keys(data);

    keys.forEach((value, index) => {
        if(value === 'formUUID'){
            $('#editHiddenField').val(data[value])
        }

        if(value === 'formName'){
            $('#formName').val(data[value])
        }

        if(value === 'formURL'){
            $('#formURL').val(data[value])
        }

        if(value === 'customVariables'){
            $('#customVariables').val(data[value])
        }

        // if(value === 'classLevelID'){
        //         $("option[value="+data[value]+"]", $('#filter-classLevels')).prop("selected", true);
        //         $('#filter-classLevels').trigger('change');
        //         $('#filter-classLevels').multiselect('refresh');
        // }

        // if(value === 'classID'){
        //     $("option[value="+data[value]+"]", $('#filter-classes')).prop("selected", true);
        //     $('#filter-classes').trigger('change');
        //     $('#filter-classes').multiselect('refresh');
        // }

        if(value === 'studentList'){
            selectedStudents = data[value]
            selectedStudents = selectedStudents.split(',')
            setTimeout(() => {
                selectedStudents.forEach((stdVal, stdInd) => {
                    $("option[value="+stdVal+"]", $('#filter-students')).prop("selected", true);
                })
                $('#filter-students').multiselect('rebuild');
            }, 500)
        }

        if(value === 'isActive'){
            $('#isActive').val(data[value])
            if(data[value] == 1){
                $('#isActiveCheck').prop('checked', true)
            }else{
                $('#isActiveCheck').prop('checked', false)
            }
        }

        if(value === 'startDateTime'){
            startDate = data[value]
            var startDate = new Date(startDate);
            startDate.setHours((startDate.getHours()+5))
            $('#startDateTime').val(startDate.toISOString().slice(0,16))
        }

        if(value === 'endDateTime'){
            endDate = data[value]
            var endDate = new Date(endDate);
            endDate.setHours((endDate.getHours()+5))
            $('#endDateTime').val(endDate.toISOString().slice(0,16))
        }
    })
    hideResultsList();
    showAddFormContainer();
})




$(document).on('click', '.delete-form', function(){
    var  selectedFormID = $(this).attr('data-form-id');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheForm+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteForm(selectedFormID);
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

function deleteForm(selectedFormID){
    $.ajax({
        url: herokuUrl+'/deleteForm',
        method: 'POST',
        headers,
        data: {
            formUUID: selectedFormID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.formHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getFormList();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteForm')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
