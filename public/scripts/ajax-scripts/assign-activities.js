$(document).ready(function(){

    getAjaxSchools();
    if(user.isAdmin == 1){
        schoolID = $('#filter-schools').val();
    }
    if(schoolID !== ''){
        getAssignedActivities();
    }

    if($('#filter-activities').length > 0){
        getAjaxActivities();
    }

})

var bulkArray = [];

function iterateStudentTable(){
    var assignActivityAddTable = $('#assign-activity-table tbody');
        assignActivityAddTable.empty();
        if(studentRecordsArray.length){
        $.each(studentRecordsArray, function(index, value){
            let addedResult;
            if(assignedActivitiesList){
                addedResult = assignedActivitiesList.find(o => o.studentID === value.studentID);
            }
            if(addedResult !== undefined && addedResult !== ''){
                assignActivityAddTable.append(`
                <tr>
                <td style="display:none;">
                    <input class="form-control" name="activityStudentID" type="text" value='${ addedResult.activityStudentID}'>
                    <input class="form-control actionFlag select-flag-${index}"  name="actionFlag" type="text" value="1">
                    <input class="form-control"  name="studentID" type="text" value='${addedResult.studentID}'>
                    <input class="form-control isSelected select-student-${index}" name="isSelected" type="text" value='1'>
                </td>
                <td style="color:#188ae2;font-weight:bold; text-align:center">${index + 1}</td>
                <td>${value.studentName }</td>
                <td>${value.rollNo}</td>
                <td>
                    <div class="customCheckbox checkbox checkbox-aqua">
                        <input id="select-student-${index}" data-id-delete="${addedResult.activityStudentID}" data-flag-id="select-flag-${index}" data-id="select-student-${index}" class="studentCheckBox" type="checkbox" checked>
                        <label for="select-student-${index}">
                        </label>
                    </div>
                </td>
                </tr>`);
            }else{
                assignActivityAddTable.append(`
                <tr>
                    <td style="display:none;">
                        <input class="form-control"  name="activityStudentID" type="text" value='0'>
                        <input class="form-control"  name="studentID" type="text" value='${value.studentID}'>
                        <input class="form-control actionFlag select-flag-${index}"  name="actionFlag" type="text" value="0">

                        <input class="form-control isSelected select-student-${index}" name="isSelected" type="text" value='0'>
                    </td>
                    <td style="color:#188ae2;font-weight:bold;text-align:center;">${index + 1}</td>
                    <td>${value.studentName }</td>
                    <td>${value.rollNo}</td>

                    <td>
                        <div class="customCheckbox checkbox checkbox-aqua">
                            <input id="select-student-${index}" data-flag-id="select-flag-${index}" data-id="select-student-${index}" class="studentCheckBox" type="checkbox">
                            <label for="select-student-${index}">
                            </label>
                        </div>
                    </td>
                </tr>`);
            }
        });
    }
}

$('#activities-student-multiple').on('click', function(){
    if($('.studentCheckBoxAll').prop('checked')){
        $('.studentCheckBoxAll').prop('checked', false);
        $(".isSelected").val(0)
        $(".actionFlag").val(0)
        $('.studentCheckBox').prop('checked', false)
    }else{
        $('.studentCheckBoxAll').prop('checked', true);
        $(".isSelected").val(1)
        $(".actionFlag").val(1)
        $('.studentCheckBox').prop('checked', true)
    }
})

$(document).on('change','.studentCheckBox',function(){
    var selectedDataAttr = $(this).attr('data-id');
    var selectedFlagDataAttr = $(this).attr('data-flag-id');
    var isChecked = $(this).prop('checked');
    if(isChecked){
        $("." + selectedDataAttr).val(1)
        $("." + selectedFlagDataAttr).val(1)
    }else{
        $("." + selectedDataAttr).val(0)
        $("." + selectedFlagDataAttr).val(0)

    }
})

function submitAssignActivity(){
    $.ajax({
        url: herokuUrl+'/setStudentActivitiesArray',
        method: 'POST',
        headers,
        data: {'array': bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#activity-form').trigger('reset');
                swal(
                    messages.activitiesHasBeenAssigned,
                    messages.success,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                hideAddFormContainer();
                getAssignedActivities();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitAssignActivity');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getAssignedActivities(){
    let data = {}
    if(classID !== ''){
        data.classID = classID;
    }
    if(activityID !== ''){
        data.activityID = activityID;
    }
    if(branchID !== ''){
        data.branchID = branchID;
    }
    if(studentID !== ''){
        data.studentID = studentID;
    }
    if(weekdayNo !== ''){
        if(Array.isArray(weekdayNo)){
            weekdayNo = weekdayNo.filter(function (el){
                return el != null && el !== '';
              });
              weekdayNo =  weekdayNo.join(',');
        }
        data.weekdayNo = weekdayNo;
    }
    if (tableData) {
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getStudentActivities',
            type: 'POST',
            headers,
            beforeSend: function(){
                assignedActivitiesList = []
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                assignedActivitiesList = response;
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'rollNo':response[i].rollNo,
                        'weedayName': response[i].weekdayName,
                        'studentName': response[i].studentName,
                        'action' : `
                        <div class="btn-group">
                        ${__DEL__ ? `<button class="btn-danger btn btn-xs delete-assigned-activity" data-assigned-activity-id='${response[i].activityStudentID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        `:''} </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getAssignedActivities');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data':'rollNo'},
            {'data': 'weedayName'},
            {'data':'studentName'},
            {'data':'action'},
        ]
    });

}

$(document).on('change','#filter-activities',function(){
    activityID = $(this).val()
    if(activityID !== ''){
        hideAndShowTable();
        setTimeout(() => {
            iterateStudentTable()
        },1000)
    }else{
        swal(
            messages.pleaseSelectActivity,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        $('#assign-activity-table tbody').empty();
        return false;
    }
});

function hideAndShowTable(){
    $('#assign-activity-table tbody').empty();
    $('#assign-activity-table tbody').append(`
    <tr>
    <td colspan="4"><strong style="color:#188ae2;font-weight:bold"> ${messages.loading}</<strong></td>
    </tr>
    `);
}

$('#filter-students').on('change',function(){
    studentID = $(this).val();
    if(activityID !== ''){
        getAssignedActivities();
    }
})

$('#assign-activities-form').on('submit', function(e){
    e.preventDefault();
    bulkArray = [];
    activityID = $('#filter-activities').val();
    var result = $(this).serializeArray();
    var filtersformData = $('#filter-form').serializeArray();
    var sliecedArray = filtersformData.slice(6, filtersformData.length);
    var chunkedArray = chunkArray(result, 4);
    weekdayNo = weekDays.val();
    if(Array.isArray(weekdayNo)){
        weekdayNo.forEach(function(value,index){
            chunkedArray.forEach((val, index) => {
                var myObject = {};
                myObject['weekdayNo'] = value;
                val.forEach((innerVal, innerIndex) => {
                    myObject["activityID"] = activityID;
                    if(innerVal.name == 'activityStudentID'){
                        if( Number(innerVal.value) > 0){
                            myObject[innerVal.name] = innerVal.value;
                        }
                    }else{
                        myObject[innerVal.name] = innerVal.value;
                    }
                });
                bulkArray.push(myObject);
            });
        })
    }
    bulkArray.forEach((value, index) => {
        for (var propName in value) {
            if(propName === 'isSelected' ){
                    delete bulkArray[index]['isSelected'];
            }
        }
    });
    bulkArray =  _.filter(bulkArray,v => _.keys(v).length !== 0);
    if(bulkArray.length > 0){
        showProgressBar(submitLoader);
        resetMessages();
        submitAssignActivity();
    }else{
        swal(
            messages.pleaseSelectAtLeast1Student,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
});

$(document).on('click', '.delete-assigned-activity', function(){
    activityStudentID = $(this).attr('data-assigned-activity-id');
   $.confirm({
       title: messages.confirm,
       content: messages.areYouSure+" <br/> "+messages.doYouWantToRemoveTheStudentsActivity+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                   deleteAssignedActivity();
               }
           },
           cancel: {
               text: messages.cancelBtn,
               action : function (){
               }
           }
       }
   });

})

function deleteAssignedActivity(){
    $.ajax({
        url: herokuUrl+'/deleteStudentActivities',
        method: 'POST',
        headers,
        data: {
            activityStudentID: activityStudentID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.studentHasBeenDeletedFromThisActivity,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getAssignedActivities();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteAssignedActivity');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
