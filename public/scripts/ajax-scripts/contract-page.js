
$(document).ready(function() {
    $.getJSON("https://api.ipify.org/?format=json", function(e) {
      $('#userIP').val(e.ip);
    });
  });

var herokuUrl1 = 'https://apps.skoolify.app/v1';
var herokuUrl = __BASEURL__;
var token = JSON.parse(localStorage.getItem('apiToken'));
var user = JSON.parse(localStorage.getItem('user'));
var loginBtn = $("#login");

$('#contract-form').on('submit', function(e){
    e.preventDefault();
    contractData = JSON.parse($('#hiddenData').val());
    userData = JSON.parse($('#userData').val());
    userIP = $('#userIP').val();
    if(!$('#contractCheck').prop('checked')){
        $('#contractErrorLabel').slideDown()
        setTimeout(()=>{
            $('#contractErrorLabel').slideUp()
        },2000)
        return false
    }
    if(!$('#chargesCheck').prop('checked')){
        $('#chargesErrorLabel').slideDown()
        setTimeout(()=>{
            $('#chargesErrorLabel').slideUp()
        },2000)
        return false
    }
    if(contractData.collectionID && contractData.collectionID !== ''){
        if(!$('#collectionCheck').prop('checked')){
            $('#collectionErrorLabel').slideDown()
            setTimeout(()=>{
                $('#collectionErrorLabel').slideUp()
            },2000)
            return false
        }
    }
    // if($('#contractCheck').prop('checked') && $('#chargesCheck').prop('checked') && $('#collectionCheck').prop('checked')){
        $('.exportLoader').removeClass('hidden')
        setTimeout(() => {
            submitContractForm()
        }, 1000);
    // }else{
    //     $('#errorLabel').slideDown()
    //     setTimeout(()=>{
    //         $('#errorLabel').slideUp()
    //     },2000)
    // }
});

function submitContractForm(){
    data = {}
    data.schoolID = contractData.schoolID
    data.userID = userData[0].userID
    data.ipAddress = userIP
    data.contractID = contractData.contractID
    data.chargesID = contractData.chargesID
    if(contractData.collectionID !== '' && contractData.collectionID !== null ){
        data.collectionID = contractData.collectionID
    }
    data.contractText = btoa(contractData.contractText)
    data.chargesText = btoa(contractData.chargesText)
    if(contractData.collectionText && contractData.collectionText !== ''){
        data.collectionText = btoa(contractData.collectionText)
    }
    $.ajax({
        url: herokuUrl+'/setContractAcceptance',
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + token.response
        },
        data,
        xhrFields: {
            responseType: 'blob'
        },
        beforeSend: function(){

        },
        success: function(res){
            if(!res.error){
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(res);
                a.href = url;
                a.download = 'Skoolify-Contract.pdf';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
                setTimeout(()=>{
                    $('.exportLoader').addClass('hidden');
                },500)
            }
            localStorage.removeItem('isAccepted')
            getUserDetail();
        },
        error: function(xhr){
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
        }
    });
}

function getUserDetail(){
    $.ajax({
        url: __BASE__+'auth/user/detail',
        method: 'GET',
        success: function(res){
            if(!res.error){
                var branchTemp = false;
                var roleTemp = false;
                if(res.response){
                    response = res.response[0]
                    if(response.isAccepted === 0 && response.IsSignatory === 1 && response.contactExists === 1){
                        localStorage.setItem('user', JSON.stringify(res.response));
                        loginBtn.html('Redirecting...');
                        getSchoolContract(response.userID)
                        return false
                    }else if(response.isAccepted === 0 && response.IsSignatory === 0 && response.contactExists === 1){
                        $("#error-message").show().html(messages.theonlinecontractacceptanceispendingpleaseasktheSuperUserToLoginAndAcceptTheContract);
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html('Login');
                        return false;
                    }else{
                        if(res.response.branchID !== null){
                            branchTemp = true;
                        }
                        if(res.response.roleID !== null){
                            roleTemp = true;
                        }

                        if(!branchTemp){
                            $("#error-message").show().html(messages.sorryThereIsNoBranchAssignedToYou);
                            loginBtn.removeAttr('disabled', false);
                            loginBtn.html('Login');
                            return false;
                        }

                        if(!roleTemp){
                            $("#error-message").show().html(messages.sorryThereIsNoRoleAssignedToYou);
                            loginBtn.removeAttr('disabled', false);
                            loginBtn.html('Login');
                            return false;
                        }
                    }

                }else{
                    if(res.response.branchID == null){
                        $("#error-message").show().html(messages.sorryThereIsNoBranchAssignedToYou);
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html('Login');
                        return false;
                    }

                    if(!res.response.roleID){
                        $("#error-message").show().html(messages.sorryThereIsNoRoleAssignedToYou);
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html('Login');
                        return false;
                    }
                }
                localStorage.setItem('user', JSON.stringify(res.response));
                $('.exportLoader').addClass('hidden')
                window.location.replace(__BASE__+'/auth/authorization');
            }else{
                loginBtn.removeAttr('disabled', false);
                loginBtn.html(messages.next);
                window.invokedFunctionArray.push('getUserDetail')
                window.requestForToken(res)
                $("#error-message").show().html(`<p>${res.message}</p>`);
            }
        },
        error: function(err){
            loginBtn.removeAttr('disabled', false);
            loginBtn.html(messages.next);
            window.invokedFunctionArray.push('getUserDetail')
            window.requestForToken(err)
            $("#error-message").show().html(messages.sorryThereIsSomethingWrongPleaseTryAgain);
        }
    })
}

function getSchoolContract(userID){
    $.ajax({
        url: __BASE__+'/auth/get/contract',
        method: 'POST',
        data: {
            "_token": __CSRF__,
            userID:userID
        },
        success: function(res){
            if(!res.error){
                response = res.response
                localStorage.setItem('contractData', JSON.stringify(response))
                window.location.replace(__BASE__+'/auth/contract-acceptance');
            }else{
                window.invokedFunctionArray.push('getSchoolContract')
                window.requestForToken(res)
            }
        },
        error: function(err){
            window.invokedFunctionArray.push('getSchoolContract')
            window.requestForToken(err)
        }
    })
}
