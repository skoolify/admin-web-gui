
var herokuUrl1 = 'https://apps.skoolify.app/v1';
var herokuUrl = __BASEURL__;
var token = JSON.parse(localStorage.getItem('apiToken'));
var user = JSON.parse(localStorage.getItem('user'));

$('#change-password-form').on('submit', function(e){
    e.preventDefault();
    data = $('#change-password-form').serialize();
    submitForm();
});

function getUserDetail(){
    $.ajax({
        url: __BASE__+'auth/user/detail',
        method: 'GET',
        success: function(res){
            if(!res.error){
                var branchTemp = false;
                var roleTemp = false;
                if(res.response){
                    
                        if(res.response.branchID !== null){
                            branchTemp = true;
                        }
                        if(res.response.roleID !== null){
                            roleTemp = true;
                        }
                        localStorage.setItem('user', JSON.stringify(res.response));
                
                        window.location.replace(__BASE__+'/auth/authorization');
                }
            }else{
                window.invokedFunctionArray.push('getUserDetail')
                window.requestForToken(res)
            }
        },
        error: function(err){
            loginBtn.removeAttr('disabled', false);
            loginBtn.html('Next');
            window.invokedFunctionArray.push('getUserDetail')
            window.requestForToken(err)
            $("#error-message").show().html(messages.sorryThereIsSomethingWrongPleaseTryAgain);
        }
    })
}

function submitForm(){
    let password = $('#newPassword').val();

    if(password !== ''){
        $.ajax({
            url: herokuUrl+'/changePassword',
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + token.response
            },
            data: {
                userID: user.userID,
                newPassword: md5(password)
            },
            beforeSend: function(){
                $('#submit-button').text('Processing...');
                $('#submit-button').attr('disabled', 'disabled');
                
                $('#cancel-button').attr('disabled', 'disabled');
                $('#error-message').empty();
            },
            success: function(res){
                if(res.response === 'success'){
                    $('#error-message').css('display', 'block');
                    $('#error-message').addClass('alert-success')
                    $('#error-message').append(`<p><strong id="add-form-error-message">${messages.passwordSuccessfullyUpdated}</strong></p>`);
    
                    $('form#change-password-form').trigger('reset');
                    getUserDetail();
                }
                $('#submit-button').text(messages.updatePassword);
                $('#submit-button').removeAttr('disabled');
    
                $('#cancel-button').removeAttr('disabled');
    
                setTimeout(function(){
                    $('#error-message').slideUp().css('display', 'none')
                }, 3000);
            },
            error: function(xhr){
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 400){
                    $('#error-message').css('display', 'block');
                    $('#error-message').addClass('alert-danger')
                    $('#error-message').append(`<p><strong id="add-form-error-message">${xhr.responseJSON.userMessage}</strong></p>`);
                }
                $('#submit-button').text(messages.updatePassword);
                $('#submit-button').removeAttr('disabled');
    
                $('#cancel-button').removeAttr('disabled');
    
                setTimeout(function(){
                    $('#error-message').slideUp().css('display', 'none')
                }, 3000);
            }
        });
    }else{
        swal(
            messages.error,
            messages.pleaseEnterPassword,
            'error'
        )
    }

    
}
