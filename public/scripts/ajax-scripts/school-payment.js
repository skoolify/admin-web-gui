var data = {};
var counter = 1;
var paymentDataArray = [];

$(document).ready(function(){
    if($('#filter-schools').length === 0 && $('#filter-branches').length === 0){
        data.branchID = String(branchID);
    }
    if(user.isAdmin !== 1){
        getSchoolOutstandingList()
    }
    getAjaxSchools();
    getAjaxCountries();
})

function getSchoolOutstandingList(){
    let data = {};
    if($('#filter-schools').val() !== undefined && $('#filter-schools').val() !== ''){
        data.schoolID = $('#filter-schools').val();
    }else{
        data.schoolID = user.schoolID;
    }
    if($('#filter-branches').val() !== undefined && $('#filter-branches').val() !== ''){
        data.branchID = $('#filter-branches').val();
    }else{
        data.branchID = user.branchID;
    }
    if(user.isAdmin !== 1){
        data.userID = user.userID
    }
    tableData = $('#dataList').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/getSchoolOutstandingList',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i + 1,
                        'groupName': response[i].schoolName+'-'+response[i].groupName,
                        'billingMonth': moment(response[i].billingFromDate).format('DD-MMM-YYYY')+'<br>'+moment(response[i].billingToDate).format('DD-MMM-YYYY'),
                        'paymentStatus' : response[i].paymentStatus === 'U' ? `<button class="btn btn-sm btn-danger"> ${messages.unpaid} </button>` : `<button class="btn btn-sm btn-primary"> ${messages.paid} </button>`,
                        'totalAmountWithoutTax' : response[i].currencyAlphaCode+' '+addCommas((Math.round(response[i].totalAmountWithoutTax * 100) / 100).toFixed(0)),
                        'totalAmountInlcudingTax' : response[i].currencyAlphaCode+' '+addCommas((Math.round(response[i].totalAmountInlcudingTax * 100) / 100).toFixed(0)),
                        'action' : `
                            <div class="btn-group">
                        ${__ACCESS__ && response[i].paymentStatus === 'U' ? `<button class="btn-primary btn btn-xs listPayNowBtn" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-credit-card"></i> ${messages.payNow}</button>` : ''}
                        <div>`
                    })
                }
                $.LoadingOverlay("hide");
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getSchoolOutstandingList')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'groupName'},
            {'data': 'billingMonth'},
            {'data': 'paymentStatus'},
            {'data': 'totalAmountWithoutTax'},
            {'data': 'totalAmountInlcudingTax'},
            {'data': 'action'},
        ]
    });
}

$(document).on('click','.listPayNowBtn', function(){
    $.LoadingOverlay("show");
    var schoolData = {}
    schoolData = JSON.parse($(this).attr('data-json'))
    $('.dataListDiv').slideUp()
    getSchoolServiceCharges(schoolData)
    $('.invoiceDiv').slideDown()
})

function getSchoolServiceCharges(schoolData){
    if($('#filter-schools').val() !== undefined && $('#filter-schools').val() !== ''){
        data.schoolID = $('#filter-schools').val();
    }else{
        data.schoolID = user.schoolID;
    }
    if($('#filter-branches').val() !== undefined && $('#filter-branches').val() !== ''){
        data.branchID = $('#filter-branches').val();
    }else{
        data.branchID = user.branchID;
    }
    data.billingFromDate = schoolData.billingFromDate
    data.billingToDate = schoolData.billingToDate
    // if(user.isAdmin !== 1){
    //     data.userID = user.userID
    // }
    data.userID = user.userID

    $.ajax({
        url: herokuUrl+'/getSchoolServiceCharges',
        type: 'POST',
        headers,
        beforeSend: function(){
            paymentDataArray = []
            counter = 1
            resetForm()
        },
        data,
        success: function(res){
            paymentDataArray.push(res)
            chargesDetailArray = res.chargesDetails
            totalValuesArray = res.totalValues[0]
            headerDetailsArray = res.chargesHeader[0]
            paymentProviderArray = res.paymentProvider[0]

            $('#access_key').val(paymentProviderArray.access_key)
            $('#transaction_uuid').val(paymentProviderArray.transaction_uuid)
            $('#profile_id').val(paymentProviderArray.profile_id)
            $('#hbl_url').val(paymentProviderArray.providerURL)
            $('#secret_key').val(paymentProviderArray.secret_key)
            $('#currency').val(totalValuesArray.currencyAlphaCode)
            $('#reference_number').val(headerDetailsArray.invoiceNumber)
            $('#amount').val(totalValuesArray.totalAmountInlcudingTax)

            var billingFromDate = moment.utc(headerDetailsArray.billingFromDate);
                billingFromDate = billingFromDate.local().format('DD-MMM-YYYY')

            var billingToDate = moment.utc(headerDetailsArray.billingToDate);
                billingToDate = billingToDate.local().format('DD-MMM-YYYY')

            var invoiceDate = moment.utc(headerDetailsArray.invoiceDate);
                invoiceDate = invoiceDate.local().format('DD-MMM-YYYY')

            var dueDate = moment.utc(headerDetailsArray.dueDate);
                dueDate = dueDate.local().format('DD-MMM-YYYY')

            chargesDetailArray.forEach(function(value,index){
                $('.school-payment-detail-table-body').append(
                    `<tr>
                        <td class="w-73 p-30">${counter++ }</td>
                        <td class="w-200 p-30">${value.branchName}</td>
                        <td class="w-200 p-30">${value.studentCount}</td>
                        <td class="w-200 p-30">${value.currencyAlphaCode+' '+addCommas((Math.round(value.unitPrice * 100) / 100).toFixed(0))}</td>
                        <td class="w-200 p-30">${value.currencyAlphaCode+' '+addCommas((Math.round(value.totalAmountWithoutTax * 100) / 100).toFixed(0))}</td>
                    </tr>`
                );
            })
            $('#totalWoTax').text(`${totalValuesArray.currencyAlphaCode+' '+addCommas((Math.round(totalValuesArray.totalAmountWithoutTax * 100) / 100).toFixed(0))}`);
            $('#taxRate').text(`${totalValuesArray.currencyAlphaCode+' '+addCommas((Math.round(totalValuesArray.taxAmount * 100) / 100).toFixed(0))}`);
            $('#taxAmountSpan').text(`${'('+totalValuesArray.taxRate+'%)'}`);
            $('#totalAfterTax').text(`${totalValuesArray.currencyAlphaCode+' '+addCommas((Math.round(totalValuesArray.totalAmountInlcudingTax * 100) / 100).toFixed(0))}`);

            $('#invoice-date').text(`${invoiceDate}`);
            $('#invoice-number').text(`${headerDetailsArray.invoiceNumber}`);
            $('#payment-frequency').text(`${headerDetailsArray.billingFrequency}`);
            $('#due-date').text(`${dueDate}`);
            // $('#group-name').text(`${totalValuesArray.groupName}`);
            $('#period').text(`${billingFromDate+' TO '+billingToDate}`);
            $('#status').html(`${headerDetailsArray.paymentStatus === 'U' ? `<button class="btn btn-sm btn-danger"> ${messages.unpaid} </button>` : `<button class="btn btn-sm btn-primary"> ${messages.paid} </button>`}`);
            $.LoadingOverlay("hide");
            $('.school-payment-detail-table-body2').show(500)
            $('.buttonsDiv').show(500)
            $.LoadingOverlay("hide");
        },
        error: function(xhr){
            invokedFunctionArray.push('getSchoolServiceCharges')
            window.requestForToken(xhr)
            isInvoke = false;
            if (xhr.status === 404) {
                $.LoadingOverlay("hide");
                $('#no-record-found').empty();
                var trCount = $('#invoice tbody tr').length;
                if(trCount > 0){
                    $('#invoice tbody').append(`
                    <tr id="no-record-found">
                        <td colspan="6">${messages.noRecordFound}</td>
                    </tr>
                `)
                }else{
                    $('#invoice tbody').append(`
                    <tr class="text-center" id="no-record-found">
                        <td style="width: 100vw; max-width: 100vw; margin: auto; background: #fff;" colspan="6">${messages.noDataAvailable}</td>
                    </tr>`)
                    if(!registrationTable){
                        registrationTable = $('#invoice').dataTable({
                            paging:false,
                            "bInfo" : false
                        });
                    }
                }
            }
        }
    })
}

$('#filter-countries').on('change', function(){
    $('#bill_to_address_country').val($('option:selected', this).attr('data-code'))
})

$('#filter-cities').on('change', function(){
    $('#bill_to_address_city').val($('option:selected', this).attr('data-city-name'))
})

$('.payNowBtn').on('click', function(){
    $.getJSON("https://api.ipify.org/?format=json", function(e) {
      $('#customer_ip_address').val(e.ip);
    });
    $('#consumer_id').val(user.mobileNo)
    $('#school-payment-detail-list').slideUp()
    $('#user-details-form').slideDown()
})

$(document).on('click','#proceedBtn', function(){
    let firstName = $('#bill_to_forename').val()
    let lastName = $('#bill_to_surname').val()
    let phone = $('#bill_to_phone').val()
    let email = $('#bill_to_email').val()
    let address = $('#bill_to_address_line1').val()
    let postalCode = $('#bill_to_address_postal_code').val()
    let country = $('#filter-countries').val()
    let city = $('#filter-cities').val()
    let state = $('#bill_to_address_state').val()

    if(firstName === ''){
        swal(
            messages.pleaseEnterFirstName,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(lastName === ''){
        swal(
            messages.pleaseEnterLastName,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(phone === ''){
        swal(
            messages.pleaseEnterPhoneFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(email === ''){
        swal(
            messages.pleaseEnterEmailFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(address === ''){
        swal(
            messages.pleaseEnterAddressFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(postalCode === ''){
        swal(
            messages.pleaseEnterPostalCode,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(country === ''){
        swal(
            messages.pleaseSelectCountry,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(city === ''){
        swal(
            messages.pleaseSelectCity,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(state === ''){
        swal(
            messages.pleaseEnterState,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    $('form#user-detail-form').submit();
})

$('#cancelProceedBtn').on('click', function(){
    $('#user-details-form').slideUp()
    $('#school-payment-detail-list').slideDown()
})

$('.paymentCancelBtn').on('click', function(){
    resetForm()
    $('.buttonsDiv').hide(500)
    $('.invoiceDiv').hide(500)
    $('.school-payment-detail-table-body2').hide(500)
    $('.dataListDiv').slideDown()
})

function resetForm(){
    $('.school-payment-detail-table-body').empty()
    $('#totalWoTax').text('');
    $('#taxRate').text('');
    $('#totalAfterTax').text('');
    $('#invoice-date').text('--');
    $('#invoice-number').text('--');
    $('#payment-frequency').text('--');
    $('#due-date').text('--');
    $('#group-name').text('--');
    $('#period').text('--');
    $('#status').text('--');

    $('#filter-branches').multiselect('deselectAll');
    $('#filter-branches').multiselect('rebuild');
}
