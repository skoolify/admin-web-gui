var studentFeeForm = $('#student-fee-form');
var recurringResult = null;
var recurringResultList = [];

$(document).ready(function () {
    getAjaxSchools();
})

function hideAndShowTable(){
    $('#recurring-add-table tbody').empty();
    $('#recurring-add-table tbody').append(`
    <tr>
    <td colspan="6"><strong style="color:#188ae2;font-weight:bold"> ${messages.Loading}</strong></td>
    </tr>
    `);
}

$(document).on('change','#filter-fee-type',function(){
    feeTypeID = $(this).val();
    if(feeTypeID != ''){
        hideAndShowTable();
        getStudentRecurringFee();
        iterateStudentRecurringFee();
    }
});

$(document).on('change','#filter-students',() => {
    if(feeTypeID != ''){
        hideAndShowTable();
        getStudentRecurringFee();
        iterateStudentRecurringFee();
    }else{
        swal(
            messages.pleaseSelectFeeType,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
});


function getStudentRecurringFee() {
    var params = {
        schoolID: schoolID,
        branchID: branchID,
        classID: classID,
    };
    if(studentID != ''){
        params.studentID = studentID
    }
    if(feeTypeID != ''){
        params.feeTypeID = feeTypeID
    }
    if (recurringResult) {
        recurringResult.destroy();
    }
    recurringResult = $('#recurring-result-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax':{
            url: herokuUrl + '/getRecurringFee',
            method: 'POST',
            headers,
            data: params,
            dataSrc: function (res) {
                response = parseResponse(res);

                var return_data = new Array();
                recurringResultList = [];
                for (let i = 0; i < response.length; i++) {
                    const element = response[i];
                    recurringResultList.push(element);
                    return_data.push({
                        'sNo': i + 1,
                        'studentName': element.studentName,
                        'feeType': element.feeType,
                        'feeAmount': addCommas(element.feeAmount),
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getStudentRecurringFee')
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
            }
        },
        "columns": [
            {
                'data': 'sNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'feeType'
            },
            {
                'data': 'feeAmount'
            },
        ]
    });

}

function iterateStudentRecurringFee(){
    var recurringAddTable = $('#recurring-add-table tbody');
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {
            showProgressBar('p2-students-result-list');
        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                recurringAddTable.empty();
                if(res.data.length){
                    $.each(res.data, function(index, value){
                        let addedResult;

                        if(recurringResultList){
                            addedResult = recurringResultList.find(o => o.studentID === value.studentID);
                        }
                        if(addedResult !== undefined && addedResult !== ''){
                            var insertTable = `
                            <tr>
                            <td style="display:none;">
                                <input class="form-control" name="feeLinkID" type="text" value=' ${ addedResult.feeLinkID } '>
                            </td>
                            <td style="color:#188ae2;font-weight:bold; text-align:center">${index + 1}</td>
                            <td>${value.studentName}</td>
                            <td>${value.rollNo}</td>
                            <td style="display:none;">
                                <input class="form-control" name="studentID" type="text" value=' ${ value.studentID } '>
                            </td>
                            <td>
                                <input onKeyUp="numericFilter(this);" class="form-control" data-validation="required number" data-validation-allowing="float" name="feeAmount" type="text" value="${addedResult.feeAmount}">
                            </td>
                            </tr>`;
                            recurringAddTable.append(insertTable);
                        }else{
                            var nonfilledInsertTable = `
                            <tr>
                                <td style="display:none;">
                                    <input class="form-control" name="feeLinkID" type="text" value='0'>
                                </td>
                                <td style="color:#188ae2;font-weight:bold;text-align:center;">${index + 1}</td>
                                <td>${value.studentName}</td>
                                <td>${value.rollNo}</td>
                                <td style="display:none;">
                                    <input class="form-control" name="studentID" type="text" value=' ${ value.studentID } '>
                                </td>
                                <td>
                                    <input onKeyUp="numericFilter(this);" class="form-control" data-validation="required number" data-validation-allowing="float" name="feeAmount" type="text" value="">
                                </td>
                                </tr>`;

                                recurringAddTable.append(nonfilledInsertTable);
                        }
                        });

                    hideProgressBar('p2-students-result-list');
                }else{
                    recurringAddTable.empty();
                    recurringAddTable.append(`
                        <tr>
                            <td colspan="6" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)

                    hideProgressBar('p2-students-result-list');
                }

                } else {
                    recurringAddTable.empty();
                    recurringAddTable.append(`<tr>
                            <td colspan="6" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                }

        },
        error: function (err) {
            invokedFunctionArray.push('iterateStudentRecurringFee')
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-students-result-list');
        }
    })
}

// studentFeeForm submit
studentFeeForm.on('submit', function (e) {
    e.preventDefault();
    var bulkArray = [];
    var result = $(this).serializeArray();
    var filtersformData = $('#filter-form').serializeArray();
    var sliecedArray = filtersformData.slice(0, filtersformData.length);
    var chunkedArray = chunkArray(result, 3);
    chunkedArray.forEach((val, index) => {
        var myObject = {};
        sliecedArray.forEach((data, formIndex) => {
            if(data.name != 'studentID' && data.name != 'classLevelID' && data.name != 'classID'){
                myObject[data.name] = data.value;
            }
        });
        val.forEach((innerVal, innerIndex) => {
            if(innerVal.name == 'feeLinkID'){
                if( Number(innerVal.value) > 0){
                    myObject[innerVal.name] = innerVal.value;
                }
            }else{
                myObject[innerVal.name] = innerVal.value;
            }
        });

        bulkArray.push(myObject);
    });
    bulkArray.forEach((value, index) => {
        for (var propName in value) {
            if (value[propName] === null || value[propName] === undefined || value[propName] == '') {
                delete bulkArray[index];
            }
        }
    });
    // bulkArray.forEach((value, index) => {
    //     bulkArray[index] = clean(value);
    // });
    bulkArray =  _.filter(bulkArray,v => _.keys(v).length !== 0);
    $.ajax({
        url: herokuUrl + '/setRecurringFeeArray',
        method: 'POST',
        headers,
        data: {
            array: bulkArray
        },
        beforeSend: function () {
            disableButtonsToggle();
            studentFeeForm.find('.form-message').remove();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                studentFeeForm.trigger('reset');
                $('#subject-result-hidden-field').removeAttr('name value');
                swal(
                    messages.successMessage,
                    messages.successfullySubmitted,
                    'success'
                );
                // studentFeeForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);
                getStudentRecurringFee();
                hideAddFormContainer();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                studentFeeForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

            if (xhr.status === 500) {
                studentFeeForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });

});

function numericFilter(txb) {
    txb.value = txb.value.replace(/[^\0-9]/ig, "");
}

