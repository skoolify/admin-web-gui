$(document).ready(function(){
    getAjaxSchools();
    // getExamTermResults();
})

function submitExamSubjectResults(){
    $.ajax({
        url: herokuUrl+'/setExamTermResults',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);

                if(tableData){
                    tableData.destroy();
                };
                // getExamSubjectResults();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitExamSubjectResults');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getExamSubjectResults(){
    tableData = $('#example44').DataTable();
}

$('#exam-term-results-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#exam-subject-results-form').serialize();
    showProgressBar(submitLoader);
    resetMessages();
    submitExamSubjectResults();
});
