var data = {};
var counter = 1;
var requiredCount = 100;
var recordSequenceNo = 0;
var isInvoke = true;
var isFirstTime = true;
var registrationTable = null;

$(document).ready(function(){
    if($('#filter-schools').length === 0 && $('#filter-branches').length === 0){
        data.branchID = String(branchID);
        $('#example44 tbody').empty()
        $('div#loadmoreajaxloader').show();
        getReportsAndAppend();
    }
    getAjaxSchools();
    var lastScrollTop = 0;
    $('.reports-data').scroll(function()
    {
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
            if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
                if(isInvoke){
                    isInvoke = false;
                    $('div#loadmoreajaxloader').show();
                    setTimeout(() => {
                        getReportsAndAppend();
                    },1000)
                }
            }
        }
        lastScrollTop = st;
    })
})

branches.on('change', function(){
    if(branchID != ''){
        $('div#loadmoreajaxloader').show();
        data.branchID = String(branchID);
        $('#search').removeAttr('disabled');
        $('#clear-btn').removeAttr('disabled');
        $('#example44 tbody').empty()
        counter = 1;
        requiredCount = 100;
        recordSequenceNo = 0;
        getReportsAndAppend();
    }else{
        $('#search').attr('disabled', true);
        $('#clear-btn').attr('disabled', true);
    }
});

$('#search-form').on('submit', function(e){
    e.preventDefault();
    var data = {};
    let mobileNo = $('#mobileNo').val();
    if(mobileNo === ''){
        swal(
            messages.pleaseEnterMobileNumber,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    if(branchID === ''){
        swal(
            messages.pleaseSelectBranch,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    data.branchID = String(branchID);
    if(mobileNo !== ''){
        $('div#loadmoreajaxloader').show();
        counter = 1;
        requiredCount = 100;
        recordSequenceNo = 0;
        data.mobileNumber = mobileNo.trim();
        $('#example44 tbody').empty()
        getReportsAndAppend();
    }
});

$('#clear-btn').on('click', function(){
    if(branchID !== ''){
        $('div#loadmoreajaxloader').show();
        counter = 1;
        requiredCount = 100;
        recordSequenceNo = 0;
        isInvoke = true;
        data.branchID = String(branchID);
        $('#mobileNo').val('');
        let mobileNo = $('#mobileNo').val();
        if(mobileNo !== ''){
            data.mobileNumber = mobileNo.trim();
        }else{
            delete data.mobileNumber;
        }
        $('#example44 tbody').empty()
        getReportsAndAppend();
    }
});

function getReportsAndAppend(){
    if(branchID !== ''){
        data.branchID = String(branchID);
        let mobileNo = $('#mobileNo').val();
        if(mobileNo !== ''){
            data.mobileNumber = mobileNo.trim();
        }else{
            delete data.mobileNumber;
        }
        data.recordSequenceNo = recordSequenceNo;
        data.requiredCount = requiredCount;
    }
    $.ajax({
        url: herokuUrl+'/getReportRegistrations',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data,
        success: function(res){

            isInvoke = true;
            response = parseResponse(res);
            recordSequenceNo = Number(recordSequenceNo) + Number(typeof response.length !== 'undefined' ? response.length : 0 );
            overall = res.overallCount;
            $('#overall-total').text(`${overall.totalParentCount}`);
            $('#overall-registered').text(`${overall.registeredParentCount}`);
            $('#overall-unregistered').text(`${overall.unregisteredParentCount}`);
            $('#unique-registered-students').text(`${overall.UniqueRegisteredStudents}`);
            $('#overall-active-students').text(`${overall.ActiveStudents}`);
            $('#overall-non-active-students').text(`${overall.NonActiveStudents}`);
            $('div#loadmoreajaxloader').hide();
            response.forEach(function(value, index){
                var regDate = moment.utc(value.registrationDate);
                regDate = regDate.local().format('DD-MMM-YYYY h:mm A')
                $('#example44 tbody').append(
                    `<tr>
                        <td class="jconfirm-title-c w-73 p-30">${counter ++ }</td>
                        <td class="jconfirm-title-c w-200 p-30">${value.parentName}</td>
                        <td class="jconfirm-title-c w-200 p-30" dir="ltr">${value.mobileNumber}</td>
                        <td class="jconfirm-title-c w-200 p-30"><a data-parentId='${value.parentID}' data-parentName='${value.parentName}' class="btn btn-primary btn-sm linked-student-list-button"><i class="fa fa-list"></i>${messages.list}</a></td>
                        <td class="jconfirm-title-c w-200 p-30">${value.isRegistered == 1 ? `<button class="btn btn-xs btn-success">${messages.registered}</button>` : `<button class="btn btn-xs btn-danger">${messages.unregistered}</button>`}</td>
                        <td class="jconfirm-title-c w-300">${Number(value.showSMS) === 1 ? `<button id="sendSMS" class="btn btn-xs btn-primary" data-mobileNumber="${value.mobileNumber}">Send SMS</button>` : `${ value.registrationDate !== null && value.registrationDate !== '' && typeof value.registrationDate !=='undefined'   ? regDate : ''}`}</td>
                    </tr>`
                    );
            })

            if(!registrationTable){
                registrationTable = $('#example44').dataTable({
                    language: {
                        url: __DATATABLE_LANGUAGE__
                    },
                    paging:false,
                    "bInfo" : false
                });
            }

        },
        error: function(xhr){
            invokedFunctionArray.push('getReportsAndAppend')
            window.requestForToken(xhr)
            isInvoke = false;
            if (xhr.status === 404) {
                $('div#loadmoreajaxloader').hide();
                $('#no-record-found').empty();
                var trCount = $('#example44 tbody tr').length;
                if(trCount > 0){
                    $('#example44 tbody').append(`
                    <tr id="no-record-found">
                        <td colspan="6">${messages.noMoreRecordFound}</td>
                    </tr>
                `)
                }else{
                    $('#example44 tbody').append(`
                    <tr class="text-center" id="no-record-found">
                        <td style="width: 100vw; max-width: 100vw; margin: auto; background: #fff;" colspan="6">${messages.noDataAvailable}</td>
                    </tr>`)
                    if(!registrationTable){
                        registrationTable = $('#example44').dataTable({
                            paging:false,
                            "bInfo" : false
                        });
                    }
                }
            }
        }
    })
}
function getRegistrationReports(){
    if(branchID !== ''){
        data.branchID = String(branchID);
        $('#mobileNo').val('');
        let mobileNo = $('#mobileNo').val();
        if(mobileNo !== ''){
            data.mobileNumber = mobileNo;
        }else{
            delete data.mobileNumber;
        }
        data.recordSequenceNo = recordSequenceNo;
        data.requiredCount = requiredCount;
    }
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/getReportRegistrations',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                overall = res.overallCount;
                $('#overall-total').text(`${overall.totalParentCount}`);
                $('#overall-registered').text(`${overall.registeredParentCount}`);
                $('#overall-unregistered').text(`${overall.unregisteredParentCount}`);
                $('#unique-registered-students').text(`${overall.UniqueRegisteredStudents}`);
                $('#overall-active-students').text(`${overall.ActiveStudents}`);
                $('#overall-non-active-students').text(`${overall.NonActiveStudents}`);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : counter++,
                        'parentName': response[i].parentName,
                        'mobileNumber': response[i].mobileNumber,
                        // 'class': `${response[i].classAlias} - ${response[i].classSection}`,
                        'studentList': `<a data-parentId='${response[i].parentID}' data-parentName='${response[i].parentName}' class="btn btn-primary btn-sm linked-student-list-button"><i class="fa fa-list"></i>${messages.list}</a>`,
                        'registered': response[i].isRegistered == 1 ? `<button class="btn btn-xs btn-success">${messages.registered}</button>` : `<button class="btn btn-xs btn-danger">${messages.unregistered}</button>`,
                        'registrationDate' :Number(response[i].showSMS) === 1 ? `<button id="sendSMS" class="btn btn-xs btn-primary" data-mobileNumber="${response[i].mobileNumber}">Send SMS</button>` : `${ response[i].registrationDate !== null && response[i].registrationDate !== '' && typeof response[i].registrationDate !=='undefined'   ? response[i].registrationDate : ''}`,

                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getRegistrationReports')
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('#overall-total').text('0');
                    $('#overall-registered').text('0');
                    $('#overall-unregistered').text('0');
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'parentName'},
            {'data': 'mobileNumber'},
            // {'data': 'class'},
            {'data': 'studentList'},
            {'data': 'registered'},
            {'data': 'registrationDate'}
        ]
    });

}

$(document).on("click",'#sendSMS', function(){
    var record = [];
    let data = {}
    var tr = $(this).closest('tr');
    var mobileNumber = $(this).attr('data-mobileNumber');
    data['smsType']  = 'INVITE_SMS';
    data['mobileNumber'] = mobileNumber;
    record.push(data)
    $.ajax({
        url: herokuUrl + '/setSMSSendArray',
        method: 'POST',
        headers,
        data: {'array':record},
        success: function (res) {
            if (res.response === 'success') {
                // swal(
                //     'Success',
                //     'Message sent Successfully',
                //     'success'
                // );
                // getRegistrationReports();
                tr.find("td:last").empty().text('Sent');
            }
        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
        }
    });
});

$(document).on('click','.linked-student-list-button',function(){
    if(schoolID === ''){
        swal(
            messages.pleaseSelectSchool,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
  }else{
  $('#linkedStudentDetailModal').modal({
      backdrop: 'static',
      keyboard: false,
      toggle: true,
      }
  );
  $('#studentDetailModal').modal('show');
  if (studenDetailtTable) {
      studenDetailtTable.destroy();
  }
  parentID = $(this).attr('data-parentId');
  var linkedParentName = $(this).attr('data-parentName');
  $('.linked-parent-list-title').text('Students List');
  studenDetailtTable = $('#linked-student-list').DataTable({
    language: {
        url: __DATATABLE_LANGUAGE__
    },
      'ajax': {
          url: herokuUrl+'/getStudentsbyParentID',
          type: 'POST',
          headers,
          beforeSend: function(){
          },
          data: {
              schoolID: schoolID,
              parentID: parentID
          },
          dataSrc: function(res){
              response = parseResponse(res);
              var return_data = new Array();
              for(var i = 0; i< response.length; i++){
                  return_data.push({
                      'sNo' : i+1,
                      'rollNo': response[i].rollNo,
                      'studentName' : response[i].studentName,
                      'dob' : moment(response[i].dob).local().format('DD-MM-YYYY'),
                      'branchName': response[i].branchName,
                      'classShift' : response[i].classShift,
                      'classLevel' : `${response[i].classAlias}-${response[i].classSection}`,
                      'classYear': response[i].classYear,
                      'isActive' : `<label class="label label-sm label-${response[i].isActive == 1 ? 'success' : 'danger'}"> ${response[i].isActive == 1 ? messages.active : messages.inActive}</label>`,


                  })
              }
              return return_data;
          },
          error: function(xhr){
            window.requestForToken(xhr)
            if (xhr.status === 500) {
                console.log(xhr.responseJSON.response.userMessage);
            }
            if (xhr.status === 404) {
                $('.dataTables_empty').html(messages.noDataAvailable);
             }
          }
      },
      "columns"    : [
          {'data': 'sNo'},
          {'data': 'rollNo'},
          {'data': 'studentName'},
          {'data': 'dob'},
          {'data': 'branchName'},
          {'data': 'classShift'},
          {'data': 'classLevel'},
          {'data': 'classYear'},
          {'data': 'isActive'},
      ]
  });
}
});
