var editFormErrors = $('#edit-form-errors');
$(document).ready(function () {
    getAjaxSchools();
    // getTimeTable();
    $('.remove-row-button').hide();


});
var rowCount = 0;
$(document).on('click', '.remove-row-button', function () {
    rowCount = 0;
    var rowCount = $('#add-time-table-form-table tr').length;
    if (rowCount > 1) {
        $(this).closest('tr').remove();
        rowCount = 0;
    }
    // if (rowCount < 10)
    //     $('.time-table-submit-button').attr('disabled', true);

});

$(document).on('click', '.time-table-record-delete', function () {
   
    var tableID = $(this).attr('data-tableId');
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this time table!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
              $(this).closest('tr').fadeOut(1000);
            $.ajax({
                url: herokuUrl + '/deleteTimeTable',
                type: 'POST',
                headers,
                async : false,
                data: {
                    tableID: tableID
                },
                beforeSend: function () {
                    buttonGroup = ''
                },
                success: function (roleResponse) {
                
                    swal(
                        'Deleted!',
                        'Time Table Deleted Successfully .',
                        'success'
                      )

                },
                error:function(xhr){
                    console.log(xhr);

                },
            });
        }
      })
});
$(document).on('click', '#add-new-row', function () {
    rowCount = 0;
    $("#add-time-table-form-table").each(function () {
        var tds = '<tr>';
        jQuery.each($('tr:last td', this), function () {
            tds += '<td>' + $(this).html() + '</td>';
        });
        tds += '</tr>';
        if ($('tbody', this).length > 0) {
            $('tbody', this).append(tds);
        } else {
            $(this).append(tds);
        }
        var rowCount = $('#add-time-table-form-table tr').length;
        if (rowCount > 1) {
            $('.remove-row-button').removeClass('hidden');
        }
        // if (rowCount >= 10)
        //     $('.time-table-submit-button').attr('disabled', false);

        if (rowCount > 10)
            $('#add-time-table-form-table tr:last').remove();
    });
});
subjectsForTimeTable = $('.filter-time-table-subjects');
$(document).on('click', '#pop', function () {
    var row = $(this).attr('data-row');
    $('#' + row).slideToggle("slow");
});
getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID);
// schools.on('change',function(){
//     schoolID = $(this).val();

// getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID);


// });
// branches.on('change',function(){
//     branchID = $(this).val();
//     getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID);

// });
classes.on('change', function () {
    classID = $(this).val();
    getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID);
});
// classLevels.on('change', function () {
//     classLevelID = $(this).val();
//     getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID);
// });

function getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID) {
    if (classLevelID.length > 0) {
        $.ajax({
            url: apiUrl + '/subjects',
            method: 'GET',
            beforeSend: function () {
                $('.filter-time-table-subjects').each(function () {
                    $(this).empty();
                });
                $('.filter-time-table-subjects').each(function () {
                    $(this).append(`<option value="">Select Subject</option>`);
                });
                // subjectsForTimeTable.attr('disabled', true);
                $('.filter-time-table-subjects').each(function () {
                    $(this).attr('disabled');
                });
                showProgressBar('p2-subjects');
            },
            data: {
                classID: classID,
                branchID: branchID,
                schoolID: schoolID,
                classLevelID: classLevelID
            },
            success: function (res) {
                if (!res.error) {
                    $('.filter-time-table-subjects').each(function () {
                        $(this).removeAttr('disabled');
                    });

                    if (res.data.length >= 1) {
                        $.each(res.data, function (index, value) {
                            $('.filter-time-table-subjects').each(function () {
                                if(value.isActive !== undefined && value.isActive === 1 ){
                                    $(this).append(`<option value="${value.subjectID}" ${value.subjectID == subjectID ? 'selected' : ''}>${value.subjectName}</option>`);
                                }
                            })

                        });
                        hideProgressBar('p2-subjects');
                    } else {
                        hideProgressBar('p2-subjects');
                    }
                } else {
                    // subjectsForTimeTable.attr('disabled');
                    $('.filter-time-table-subjects').each(function () {
                        $(this).attr('disabled');
                    });
                    hideProgressBar('p2-subjects');
                }
            },
            error: function (err) {
                window.requestForToken(err)
                console.log(err);
                hideProgressBar('p2-subjects');
            }
        })
    }
}

function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

function submitTimeTable() {
    $.ajax({
        url: herokuUrl + '/setTimeTable',
        method: 'POST',
        headers,
        data,
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);
                editFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);
                tableData.destroy();
                $('form#time-table-form').trigger('reset');
                $('form#edit-time-table-form').trigger('reset');
                getTimeTable();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            invokedFunctionArray.push('submitTimeTable')
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                editFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getTimeTable() {
    tableData = $('#example44').DataTable({
        'ajax': {
            url: herokuUrl + '/getTimeTable',
            type: 'POST',
            headers,
            beforeSend: function () {},
            data: {
                schoolID: schoolID,
                branchID: branchID,
                classID: classID
            },
            dataSrc: function (res) {
                response = parseResponse(res);
                var return_data = new Array();
                for (var i = 0; i < response.length; i++) {
                    var html = "<button class='btn btn-default' id='pop' data-row='" + response[i].tableID + "'>View List</button>";
                    html += "<table class='table table-hover table-bordered' id='" + response[i].tableID + "' style='display:none;margin:auto;margin-top:10px;'><thead><tr><th>Subject</th><th>Times</th></tr></thead>";
                    html += "<tbody>";
                    response[i].timeVar = response[i].time1;
                    if (response[i].subject1 != 'undefined' && response[i].subject1 != null && response[i].time1 != 'undefined' &&  response[i].time1 != null) {
                        var splitedArray = response[i].time1.replace(' ','').split("-");
                        html += `<tr>
                        <td>${response[i].subject1 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject2 != 'undefined' && response[i].subject2 != null && response[i].time2 != 'undefined' &&  response[i].time2 != null) {
                        var splitedArray = response[i].time2.split("-");
                        html += `<tr>
                        <td>${response[i].subject2 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject3 != 'undefined'  && response[i].subject3 != null && response[i].time3 != 'undefined' &&  response[i].time3 != null) {
                        var splitedArray = response[i].time3.split("-");
                        
                        html += `<tr>
                        <td>${response[i].subject3 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject4 != 'undefined'  && response[i].subject4 != null && response[i].time4 != 'undefined' &&  response[i].time4 != null) {
                        var splitedArray = response[i].time4.split("-");
                        html += `<tr>
                        <td>${response[i].subject4 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject5 != 'undefined'  && response[i].subject5 != null && response[i].time5 != 'undefined' &&  response[i].time5 != null) {
                        var splitedArray = response[i].time5.split("-");
                        html += `<tr>
                        <td>${response[i].subject5 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject6 != 'undefined'  && response[i].subject6 != null && response[i].time6 != 'undefined' &&  response[i].time6 != null) {
                        var splitedArray = response[i].time6.split("-");
                        html += `<tr>
                        <td>${response[i].subject6 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject7 != 'undefined'  && response[i].subject7 != null && response[i].time7 != 'undefined' &&  response[i].time7 != null) {
                        var splitedArray = response[i].time7.split("-");
                        html += `<tr>
                        <td>${response[i].subject7 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject8 != 'undefined'  && response[i].subject8 != null && response[i].time8 != 'undefined' &&  response[i].time8 != null) {
                        var splitedArray = response[i].time8.split("-");
                        html += `<tr>
                        <td>${response[i].subject8 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject9 != 'undefined'  && response[i].subject9 != null && response[i].time9 != 'undefined' &&  response[i].time9 != null) {
                        var splitedArray = response[i].time9.split("-");
                        html += `<tr>
                        <td>${response[i].subject9 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        </tr>`;
                    }
                    if (response[i].subject10 != 'undefined'  && response[i].subject10 != null && response[i].time10 != 'undefined' &&  response[i].time10 != null) {
                        var splitedArray = response[i].time10.split("-");
                        html += `<tr>
                        <td>${response[i].subject10 }</td>
                        <td>${ splitedArray[0]} -  ${splitedArray[1]}</td>
                        
                        </tr>`;
                    }
                    html += "</tbody>";
                    html += "</table>";
                    return_data.push({
                        'sNo': i + 1,
                        'weekday': response[i].weekday,
                        'subjects': html,
                        'action': `<button class="btn-primary btn btn-xs time-table-record-edit" data-toggle="tooltip" title="Edit" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>
                        <button class="btn-danger btn btn-xs time-table-record-delete" data-toggle="tooltip" title="Delete" data-tableId='${response[i].tableID}'><i class="fa fa-trash"></i></button>
                    </td>`
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getTimeTable')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns": [{
                'data': 'sNo'
            },
            {
                'data': 'weekday'
            },
            {
                'data': 'subjects'
            },
            {
                'data': 'action'
            }
        ]
    });

}

//time table Edit Record Parsing
$(document).on('click', '.time-table-record-edit', function () {
    var json = JSON.parse($(this).attr('data-json'));
    var keys = Object.keys(json);
    var timeArray = ["time1", "time2", "time3", "time4", "time5", "time6", "time7", "time8", "time9", "time10"];

    if (keys.length >= 1) {
        editHiddenField.attr('name', keys[0]);
        var count = 1;
        keys.forEach((value, index) => {
            if (value == 'isActive') {
                if (json[value] == 1) {
                    $('#activeField0').removeAttr('checked');
                    $('#activeField1').attr('checked', true);
                } else {
                    $('#activeField1').removeAttr('checked');
                    $('#activeField0').attr('checked', true);
                }
            } else {

                if (jQuery.inArray(value, timeArray) != -1) {
                    if(json[value] != null){
                        var splitedArray = json[value].split("-");
                        var timeFrom = 'time-from' + count;
                        var timeTo = 'time-to' + count;
                        $('[name=' + timeFrom + ']').val(splitedArray[0].replace(' ', ''));
                        $('[name=' + timeTo + ']').val(splitedArray[1].replace(' ', ''));
                    }
                    count++;
                }
                $('[name=' + value + ']').val(json[value]);
            }
            if (value.includes('ID')) {
                if (typeof value !== 'undefined') {
                    window[value] = json[value];
                }
            }
        });
    }
    resetMessages();
    hideResultsList();
    showEditFormContainer();

});

function showEditFormContainer() {
    $("#time-table-edit-form-container").slideDown('slow');
    filterFormTitle.text('Create').css({
        'color': '#2fa7ff'
    });
    hideResultsList();
}

function hideEditFormContainer() {
    $("#time-table-edit-form-container").slideUp();
    filterFormTitle.text('Filters').css({
        'color': 'black'
    });
    showResultsList();
}
$('#hide-time-table-edit-form-container').on('click', function () {
    hideEditFormContainer();
    editHiddenField.removeAttr('name');
    editHiddenField.removeAttr('value');
    $(this).parents('form:first').trigger('reset');
});
$('#hide-time-table-edit-form-container').on('click', function () {
    hideAddFormContainer();
    editHiddenField.removeAttr('name');
    editHiddenField.removeAttr('value');
    $(this).parents('form:first').trigger('reset');
});
$('#time-table-form').on('submit', function (e) {
    e.preventDefault();
    var formData = $('#time-table-form').serializeArray();
    var result = chunkArray(formData, 3);
    var timeTableForm = '';
    var count = 0;
    var subjectID;
    var time;
    var counter = 1;
    var totalTime = '';
    result.forEach((val, index) => {
        val.forEach((value, innerIndex) => {
            if (innerIndex == 0) {
                timeTableForm += "subjectID" + counter + "=" + value.value + "&";
            } else if (innerIndex == 1) {
                totalTime += value.value + " - ";
            } else if (innerIndex == 2) {
                totalTime += value.value
                if (index == 9) {
                    timeTableForm += "time" + counter + "=" + totalTime;
                } else {
                    timeTableForm += "time" + counter + "=" + totalTime + "&";
                }
            }
            count = 1;
        })
        totalTime = '';
        counter++;
    })
    data = $('#filter-form').serialize() + '&' + timeTableForm;
    showProgressBar(submitLoader);
    resetMessages();
    submitTimeTable();
});
$('#edit-time-table-form').on('submit', function (e) {
    e.preventDefault();
    var formData = $('#edit-time-table-form').serializeArray();
    var tableId = formData.shift();
    var result = chunkArray(formData, 3);
    var timeTableForm = '';
    var count = 0;
    var subjectID;
    var time;
    var counter = 1;
    var totalTime = '';
    result.forEach((val, index) => {
        val.forEach((value, innerIndex) => {
            if(value.value != ""){
                if (innerIndex == 0) {
                    timeTableForm += "subjectID" + counter + "=" + value.value + "&";
                } else if (innerIndex == 1) {
                    totalTime += value.value + " - ";
                } else if (innerIndex == 2) {
                    totalTime += value.value
                    if (index == 9) {
                        timeTableForm += "time" + counter + "=" + totalTime;
                    } else {
                        timeTableForm += "time" + counter + "=" + totalTime + "&";
                    }
                }
        }
            count = 1;
        })
        totalTime = '';
        counter++;
    })
    data = "tableID=" + tableId.value + '&' + $('#filter-form').serialize() + '&' + timeTableForm;
    showProgressBar(submitLoader);
    resetMessages();
    submitTimeTable();
});
