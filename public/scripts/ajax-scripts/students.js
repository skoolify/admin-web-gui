var existingLogo = null;
var isParentExist = false;
var code = undefined;
var tagData = {};
var studentNoteData = {};
var tagsArray = []
var tagsDataArray = []
var isTagEdit = false;
var notesDataArray = {};
var editStudentNote = false;
$(document).ready(function(){
    getAjaxSchools();
    // getStudents();
    $("#noteTag").select2({
        tags: true,
        width:'280',
        placeholder: messages.selectaTag,
        searchInputPlaceholder: messages.typeHerePressEnterToAddNewTag
    });

    $("#noteTagEdit").select2({
        tags: true,
        width:'280',
        placeholder: messages.selectaTag,
        searchInputPlaceholder: messages.typeHerePressEnterToAddNewTag
    });
})

$(document).on('keyup', '.select2-search__field', function(e){
    if(editStudentNote === true){
        var noteTag = $('.select2-search__field').val()
        tagsDataArray.forEach(function(value, index){
            if(value.tagText === noteTag){
                $('#tagTextEdit').val(value.tagText)
                $('#tagUUIDEdit').val(value.tagUUID)
            }
        });
        $('#tagUUIDEdit').val('')
        $('#tagTextEdit').val(noteTag)
    }else{
        var noteTag = $('.select2-search__field').val()
        tagsDataArray.forEach(function(value, index){
            if(value.tagText === noteTag){
                $('#tagText').val(value.tagText)
                $('#tagUUID').val(value.tagUUID)
            }
        });
        $('#tagUUID').val('')
        $('#tagText').val(noteTag)
    }

})

$(document).on('change', '#noteTag', function(){
    if($('option:selected', this).attr('data-title') !== '' && $('option:selected', this).attr('data-title') !== undefined){
        $('#tagText').val($('option:selected', this).attr('data-title'))
        $('#tagUUID').val($('option:selected', this).val())
    }else{
        $('#tagText').val($('option:selected', this).val())
        $('#tagUUID').val('')
    }
})

$(document).on('change', '#noteTagEdit', function(){
    if($('option:selected', this).attr('data-title') !== '' && $('option:selected', this).attr('data-title') !== undefined){
        $('#tagTextEdit').val($('option:selected', this).attr('data-title'))
        $('#tagUUIDEdit').val($('option:selected', this).val())
    }else{
        $('#tagTextEdit').val($('option:selected', this).val())
        $('#tagUUIDEdit').val('')
    }
})

function resetTagForm(){
    $('.tagsInput').val('')
    $('.tagsInputEdit').val('')
    $('#tagText').val('')
    $('#tagTextEdit').val('')
    $('#studentNote').val('')
    $('#studentNoteEdit').val('')
    $('#studentNotesUUIDEdit').val('')
    $('#studentUUIDEdit').val('')
    $('#tagTitle').val('')
    $('#isActiveInput').val('')
    $('#tagUUID').val('')
    $('#tagUUIDEdit').val('')
    $('#tagTitleInput').val('')
    $('#isActive').prop('checked', false)
}

$(document).on('click', '.closeTagFormBtn', function(){
    $('.addTagFormDiv').slideUp(500)
    $('.tagFieldDiv').show()
})

$(document).on('click', '.submitTagFormBtn', function(){
    submitTag()
})

$(document).on('change','#noteTag', function(){
    var tagValue = $(this).val()
    if(tagValue == 'createNew'){
        submitTag()
    }else if(tagValue == 'createAndEdit'){
    }
})

$(document).on('click','.isActive',function(){
    if($(this).prop('checked')){
        $('#isActiveInput').val(1)
    }else{
        $('#isActiveInput').val(0)
    }
})

$(document).on('click', '.closeBtn', function(){
    resetTagForm()
    $('.notesDataTable').slideDown()
    $('.noteEditForm').slideUp()
})

function submitTag(){
    tagData = {}
    tagData['branchID'] = branchID
    if($('#tagTitle').val() !== ''){
        tagData['tagText'] = $('#tagTitle').val()
    }else{
        tagData['tagText'] = $('#tagText').val()
    }
    tagData['userID'] = userID
    if($('#isActiveInput').val() !== '' && $('#isActiveInput').val() != '0'){
        tagData['isActive'] = $('#isActiveInput').val()
    }else{
        tagData['isActive'] = 1
    }
    if($('#tagUUID').val() !== ''){
        tagData['tagUUID'] = $('#tagUUID').val()
    }

    $.ajax({
        url: herokuUrl+'/setStudentNotesTag',
        method: 'POST',
        headers,
        data: tagData,
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                resetTagForm()
                getTags()
                if(isTagEdit === true){
                    $('.successDiv').text('Tag updated successfully!')
                    $('.tagSuccessDiv').slideDown(500)
                    setTimeout(() => {
                        $('.tagSuccessDiv').slideUp(500)
                    }, 2000);
                }else{
                    $('.successDiv').text('Tag added successfully!')
                    $('.tagSuccessDiv').slideDown(500)
                    setTimeout(() => {
                        $('.tagSuccessDiv').slideUp(500)
                    }, 2000);
                }

            }
        },
        error: function(xhr){
            invokedFunctionArray.push('submitTag')
            window.requestForToken(xhr)
        }
    });
}

function getTags(){
    $.ajax({
        url: herokuUrl+'/getActiveStudentTags',
        method: 'POST',
        headers,
        data:{
            branchID:branchID,
        },
        beforeSend: function(){
            tagsArray = []
            tagsDataArray = []
            $('#noteTag').empty()
            $('#noteTagEdit').empty()
        },
        success: function(res){
            response = res.response
            if(response && !response.internalMessage){
                if(editStudentNote === true){
                    response.forEach(function(value, index){
                        tagsArray.push(value.tagText)
                        tagsDataArray.push(value)
                        $('#noteTagEdit').prepend(`<option value="${value.tagUUID}" data-title="${value.tagText}">${value.tagText}</option>`)
                    })
                    // $('#tagTextEdit').val(response[0].tagText)
                    // $('#tagUUIDEdit').val(response[0].tagUUID)
                }else{
                    response.forEach(function(value, index){
                        tagsArray.push(value.tagText)
                        tagsDataArray.push(value)
                        $('#noteTag').prepend(`<option value="${value.tagUUID}" data-title="${value.tagText}">${value.tagText}</option>`)
                    })
                    $('#tagText').val(response[0].tagText)
                    $('#tagUUID').val(response[0].tagUUID)
                }
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('getTags')
            window.requestForToken(xhr)
        }
    });
}

function getStudentNotes(){
    if (notesDetailTable) {
        notesDetailTable.destroy();
    }
    notesDetailTable = $('#notes-detail-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/getStudentNotes',
            method: 'POST',
            headers,
            data:notesDataArray,
            beforeSend: function(){
            },
            dataSrc: function(res){
                response = res.response
                var return_data = new Array();

                for(var i = 0; i< response.length; i++){
                    var insertionDate = moment.utc(response[i].insertionDate);
                    insertionDate = insertionDate.local().format('DD-MMM-YYYY');
                    return_data.push({
                        'sNo' :`<small>${i+1}</small>`,
                        'fullName'       : `<small>${response[i].fullname}</small>`,
                        'tagText'        : `<small>${response[i].tagText}</small>`,
                        'studentNotes'   : `<small>${response[i].studentNotes}</small>`,
                        'insertionDate'  : `<small>${insertionDate}</small>`,
                        'action' :
                        `<div class="btn-group">
                            ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-note-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                            ${__DEL__ ?
                            `<button class="btn-danger btn btn-xs delete-note" data-note-id='${response[i].studentNotesUUID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>`
                            : ''}
                        </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('#notes-error').empty()
                    $('#notes-error').append(`
                        <tr>
                            <td colspan="6">${messages.noDataAvailable}</td>
                        </tr>
                    `)
                }
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'fullName'},
            {'data': 'tagText'},
            {'data': 'studentNotes'},
            {'data': 'insertionDate'},
            {'data': 'action'}
        ]
    });
}

$(document).on('click','.list-note-edit', function(){
    editStudentNote = true
    getTags()
    noteData = JSON.parse($(this).attr('data-json'))
    $('#tagTextEdit').val(noteData.tagText)
    $('#tagUUIDEdit').val(noteData.tagUUID)
    $('#studentNoteEdit').val(noteData.studentNotes)
    $('#studentUUIDEdit').val(noteData.studentUUID)
    $('#studentNotesUUIDEdit').val(noteData.studentNotesUUID)
    $.LoadingOverlay('show')
    setTimeout(() => {
        $.LoadingOverlay('hide')
        $('#noteTagEdit').val(noteData.tagUUID).trigger('change')
        $('.notesDataTable').slideUp()
        $('.noteEditForm').slideDown()
    }, 1000);
})

$(document).on('click','.cancelEditNote', function(){
    editStudentNote = false
    $('#tagTextEdit').val('')
    $('#tagUUIDEdit').val('')
    $('.notesDataTable').slideDown()
    $('.noteEditForm').slideUp()
})

$(document).on('click', '.delete-note', function(){
    studentNotesUUID = $(this).attr('data-note-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+' <br/> '+messages.youWantToDeleteThisNote+'</br>',
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteStudentNote();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });
})

function deleteStudentNote(){
    $.ajax({
        url: herokuUrl+'/deleteStudentNotes',
        method: 'POST',
        headers,
        data: {
            studentNotesUUID: studentNotesUUID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                studentNotesUUID = '';
                getStudentNotes();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteStudentNote');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('click', '.add-note', function(){
    notesDataArray = {}
    resetTagForm()
    var studentUUID = $(this).attr('data-studentUUID');
    $('#studentUUID').val(studentUUID)
    notesDataArray['studentUUID'] = studentUUID
    notesDataArray['branchID'] = branchID
    getTags()
    getStudentNotes()
    $('#addNoteModal').modal('toggle');
    $('#addNoteModal').modal('show');
})

$(document).on('click', '.view-notes-list', function(){
    // editStudentNote = true
    notesDataArray = {}
    $('#noteModalLabel').empty()
    var studentUUID = $(this).attr('data-studentUUID');
    notesDataArray['studentUUID'] = studentUUID
    notesDataArray['branchID'] = branchID
    var studentName = $(this).attr('data-studentName')
    $('#noteModalLabel').text('Notes List ('+studentName+')')
    getStudentNotes()
    $('#notesListModal').modal('toggle');
    $('#notesListModal').modal('show');

})

$(document).on('click','#submitNotesDataBtn', function(){
    submitStudentNote()
})

$(document).on('click','#updateNotesDataBtn', function(){
    submitStudentNote()
})

function submitStudentNote(){
    studentNoteData = {}
    if(editStudentNote === true){
        if($('#tagUUIDEdit').val() !== ''){
            studentNoteData['tagUUID'] = $('#tagUUIDEdit').val()
        }
        studentNoteData['tagText'] = $('#tagTextEdit').val()
        if($('#studentUUIDEdit').val() !== ''){
            studentNoteData['studentUUID'] = $('#studentUUIDEdit').val()
        }
        if($('#studentNotesUUIDEdit').val() !== ''){
            studentNoteData['studentNotesUUID'] = $('#studentNotesUUIDEdit').val()
        }
        if($('#studentNoteEdit').val() !== ''){
            studentNoteData['studentNotes'] = $('#studentNoteEdit').val()
        }else{
            $('.noteSubmitErrorEdit').text('Please fill the form first!')
            $('.noteSubmitErrorEdit').removeClass('text-success')
            $('.noteSubmitErrorEdit').addClass('text-danger')
            $('.noteSubmitErrorEdit').slideDown(500)
            setTimeout(() => {
                $('.noteSubmitErrorEdit').slideUp(500)
            }, 2000);
            return false
        }
    }else{
        if($('#tagUUID').val() !== ''){
            studentNoteData['tagUUID'] = $('#tagUUID').val()
        }
        studentNoteData['tagText'] = $('#tagText').val()
        if($('#studentUUID').val() !== ''){
            studentNoteData['studentUUID'] = $('#studentUUID').val()
        }
        if($('#studentNote').val() !== ''){
            studentNoteData['studentNotes'] = $('#studentNote').val()
        }else{
            $('.noteSubmitError').text('Please fill the form first!')
            $('.noteSubmitError').removeClass('text-success')
            $('.noteSubmitError').addClass('text-danger')
            $('.noteSubmitError').slideDown(500)
            setTimeout(() => {
                $('.noteSubmitError').slideUp(500)
            }, 2000);
            return false
        }
    }
    if($('#filter-branches').val() !== '' && $('#filter-branches').val() !== undefined){
        studentNoteData['branchID'] = $('#filter-branches').val()
    }else{
        studentNoteData['branchID'] = branchID
    }
    if($('#filter-classes').val() !== '' && $('#filter-classes').val() !== undefined){
        studentNoteData['classID'] = $('#filter-classes').val()
    }else{
        studentNoteData['classID'] = classID
    }
    studentNoteData['userID'] = userID
    $.ajax({
        url: herokuUrl+'/setStudentNotes',
        method: 'POST',
        headers,
        data: studentNoteData,
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                if(editStudentNote === true){
                    $('.noteSubmitErrorEdit').text(messages.noteHasBeenAddedSuccessfully)
                    $('.noteSubmitErrorEdit').removeClass('text-danger')
                    $('.noteSubmitErrorEdit').addClass('text-success')
                    $('.noteSubmitErrorEdit').slideDown(500)
                    setTimeout(() => {
                        $('.noteSubmitErrorEdit').slideUp(500)
                    }, 2000);
                    resetTagForm()
                    $('.notesDataTable').slideDown()
                    $('.noteEditForm').slideUp()
                    editStudentNote = false
                    getStudentNotes()
                }else{
                    $('.noteSubmitError').text(messages.noteHasBeenAddedSuccessfully)
                    $('.noteSubmitError').removeClass('text-danger')
                    $('.noteSubmitError').addClass('text-success')
                    $('.noteSubmitError').slideDown(500)
                    setTimeout(() => {
                        $('.noteSubmitError').slideUp(500)
                    }, 2000);
                    resetTagForm()
                    setTimeout(() => {
                        // $('#addNoteModal').modal('toggle');
                        $('#addNoteModal').modal('hide');
                    }, 1000);
                }
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('submitTag')
            window.requestForToken(xhr)
        }
    });
}

$(document).on('click', '.parent-detail', function () {

    var detailStudentID = $(this).attr('data-recordID');
    if (parentDetailTable) {
        parentDetailTable.destroy();
    }

    parentDetailTable = $('#parent-detail-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getParentByStudentID',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                studentID: detailStudentID,
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    var regDate = moment.utc(response[i].registrationDate);
                    regDate = regDate.local().format(__DATEFORMAT__);
                    var parentTyp = '';
                    if(response[i].parentType == 'M'){
                        parentTyp = messages.mother;
                    }else if(response[i].parentType == 'F'){
                        parentTyp = messages.father;
                    }else if(response[i].parentType == 'G'){
                        parentTyp = messages.guardian;
                    }else{
                        parentTyp = messages.unknown;
                    }
                    return_data.push({
                        'sNo' : i+1,
                        'parentName'   :           `<small>${response[i].parentName}</small>`,
                        'email'        :        `<small>${response[i].email}</small>`,
                        'mobileNumber'   :      `<small>${response[i].mobileNumber}</small>`,
                        'parentCNIC'   :        `<small>${response[i].parentCNIC}</small>`,
                        'parentType'   :        `<small>${parentTyp}</small>`,
                        'registrationDate':     `<small>${ regDate !== null && regDate !== 'null' && regDate !== messages.InvalidDate ? regDate : messages.notRegistered}</small>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'parentName'},
            {'data': 'email'},
            {'data': 'mobileNumber'},
            {'data': 'parentCNIC'},
            {'data': 'parentType'},
            {'data': 'registrationDate'}
        ]
    });
        $('#fathersDetailModal').modal('toggle');
        $('#fathersDetailModal').modal('show');
});
function submitStudent(){
    $.ajax({
        url: herokuUrl+'/setStudents',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();

                $('#existing-school-logo').addClass('hidden');
                $('#upload-school-logo').removeClass('hidden');
                $('#hiddenPicture').val('');
                $('#existing-logo').attr('src', '');
                $('#reset-logo-image-edit').addClass('hidden');

                $('form#students-form').trigger('reset');
                getStudents();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitStudent')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.responseJSON && xhr.responseJSON !== null){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.reason}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
$('.search-student').click(() => {
    searchStudent()
    // var searchByName = $('#searchByName').val();
    // var searchByRollNumber = $('#searchByRollNumber').val();
    // if(schoolID === ''){
    //     $('#filter-schools').addClass('is-invalid');
    // }else{
    //     $('#filter-schools').removeClass('is-invalid');
    // }
    // if(branchID === ''){
    //     $('#filter-branches').addClass('is-invalid');
    // }else{
    //     $('#filter-branches').removeClass('is-invalid');
    // }


    // if(searchByName === '' && searchByRollNumber == '' ){
    //     $('#searchByRollNumber').addClass('is-invalid');
    // }else{
    //     $('#searchByRollNumber').removeClass('is-invalid');
    //     getStudentBySearch(searchByName, searchByRollNumber);
    // }
});

function searchStudent(){
    var searchByName = $('#searchByName').val();
    var searchByRollNumber = $('#searchByRollNumber').val();
    if(schoolID === ''){
        $('#filter-schools').addClass('is-invalid');
    }else{
        $('#filter-schools').removeClass('is-invalid');
    }
    if(branchID === ''){
        $('#filter-branches').addClass('is-invalid');
    }else{
        $('#filter-branches').removeClass('is-invalid');
    }


    if(searchByName === '' && searchByRollNumber == '' ){
        $('#searchByRollNumber').addClass('is-invalid');
    }else{
        $('#searchByRollNumber').removeClass('is-invalid');
        getStudentBySearch(searchByName, searchByRollNumber);
    }
}

function getStudentBySearch(searchByName = null, searchByRollNumber = null){
    var data = {}
    if(user.isAdmin !== 1){
        data.schoolID = schoolID
        data.branchID = branchID
    }else{
        if($('#filter-schools').val() !== null && $('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val()
        }else{
            swal(
                messages.pleaseSelectSchool,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }

        if($('#filter-branches').val() !== null && $('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val()
        }else{
            swal(
                messages.pleaseSelectBranch,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
    }
    // if($('#filter-classLevels').val() !== null && $('#filter-classLevels').val() !== ''){
    //     data.classLevelID = $('#filter-classLevels').val()
    // }else{
    //     Swal.fire({
    //         type: 'error',
    //         title: messages.oops,
    //         text: messages.pleaseSelectClass,
    //       })
    //       return false;
    // }
    if(searchByName){
        data.studentName = searchByName
    }else if($('#searchByName').val() !== ''){
        data.studentName = $('#searchByName').val()

    }
    if(searchByRollNumber){
        data.rollNo = searchByRollNumber
    }else if($('#searchByRollNumber').val() !== ''){
        data.studentName = $('#searchByRollNumber').val()

    }
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getStudentSearch',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'dob' : moment(response[i].dob).format(__DATEFORMAT__),
                        'classShift' : response[i].classShift,
                        'classSection' : `${response[i].classAlias}-${response[i].classSection}`,
                        'rollNo' : response[i].rollNo,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `<button class="btn-primary btn btn-xs parent-detail" data-toggle="tooltip" title="${messages.viewParentDetail}" data-recordID='${response[i].studentID}'><i class="fa fa-list"></i></button>` : ''}`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudentBySearch')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'dob'},
            {'data': 'classShift'},
            {'data': 'classSection'},
            {'data': 'rollNo'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });
}
$('#hide-add-form-container').on('click', function(){
    $('#existing-school-logo').addClass('hidden');
    $('#hiddenPicture').val('');
    $('#existing-logo').attr('src', '');
    $('#upload-school-logo').removeClass('hidden');
});

$(document).on('click', '.list-record-edit', function(){
    let json = JSON.parse($(this).attr('data-json'));
    existingLogo = json.picture;
    $('#upload-school-logo').addClass('hidden');
    $('#existing-school-logo').removeClass('hidden');
    $('#existing-school-logo img').attr('src', 'data:image/*;base64,'+existingLogo);
    $('#filter-classLevels').trigger('change')
});

$('#imgupload').on('change', readFile);

$('#existing-logo').click(function(){
    $('#imgupload').trigger('click');
});

$('#camera-icon').click(function(){
    $('#imgupload').trigger('click');
});

$('#reset-logo-image').on('click', function(){
    $('#logoImg').val('');
    $('#hiddenPicture').val('');
    $(this).addClass('hidden');
})

function readFile() {
    resetMessages();
    $.LoadingOverlay("show");
    var tempPicture = $(this);
    var formData = new FormData();
    var files = this.files[0];

    setTimeout(function(){
        formData.append('logoImg', files, 'image');
        formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: __BASE__+'/students/picture',
            type: 'POST',
            data: formData,
            async: false,
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
                showProgressBar(submitLoader);
                disableButtonsToggle();
            },
            success: function(xhr){
                if(xhr){
                    if(xhr.picture){
                        let code = xhr.picture.split('base64,');
                        $('#hiddenPicture').val(code[code.length - 1]);
                        if(existingLogo === null){

                            if(tempPicture.val() === ''){
                                $('#hiddenPicture').val('');
                                $('#reset-logo-image').addClass('hidden');
                            }

                            if(tempPicture.val() !== ''){
                                $('#reset-logo-image').removeClass('hidden');
                            }
                        }else{
                            if(tempPicture.val() === ''){
                                $('#hiddenPicture').val('');
                                $('#reset-logo-image-edit').addClass('hidden');
                            }

                            if(tempPicture.val() !== ''){
                                $('#reset-logo-image-edit').removeClass('hidden');
                            }
                        }

                        if(existingLogo !== null){
                            setTimeout(function(){
                                $('#existing-logo').attr('src', 'data:image/*;base64,'+$("#hiddenPicture").val());
                            }, 700);
                            $('#imgupload').val('');
                        }
                    }
                }
                $.LoadingOverlay("hide");
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            },
            error: function(xhr){
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    let errors = xhr.responseJSON.errors;
                    $.each(errors, function(key, val){
                        $('#add-form-errors').html(`<p class="text-danger"><strong>${val[0]}</strong></p>`);
                    });
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            }
        });
    }, 200)

    return false;
}

$('#logoImg').on('change', readFile);

$('.view-note').on('click', function(){
    getStudents()
})

function getStudents(){
    // schoolID = 1
    // branchID = 1
    // classLevelID = 221
    // classID = 891
    // if(tableData){
    //     tableData.destroy()
    // }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getAllClassStudents',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                schoolID: schoolID,
                branchID: branchID,
                classLevelID: classLevelID,
                classID: classID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    var dob = moment.utc(response[i].dob);

                    dob = dob.local().format('DD-MMM-YYYY')
                    rollNumber = response[i].rollNo.substring(0, 8)
                    if(response[i].gender === 'M'){
                        gender = 'Male'
                    }else{
                        gender = 'Female'
                    }
                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'dob' : typeof dob !== 'undefined' &&  dob !== 'Invalid date' ? dob : '-',
                        'gender' : gender,
                        // 'classShift' : response[i].classShift,
                        'classSection' : `${response[i].classAlias}-${response[i].classSection}`,
                        'rollNo' : `<span data-toggle="tooltip" title="${response[i].rollNo}">
                        ${response[i].rollNo.length <= 7  ? response[i].rollNo : rollNumber+'...'}
                        </span>`,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `
                            <div class="btn-group btn-success" role="group">
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle btn-sucess" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${messages.action} <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    ${__ACCESS__ ?
                                        `<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-primary btn-lg  list-record-edit dropdown-item" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i> ${messages.edit}
                                        </a>`: '' }
                                        <a data-type="link" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-success btn-lg dropdown-item parent-detail" data-recordID="${response[i].studentID}" data-toggle="tooltip" title="${messages.viewParentDetail}" ><i class="fa fa-list" ></i> ${messages.parentDetail}</a>
                                        <a data-type="unlink" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-warning btn-lg  dropdown-item add-note" data-studentUUID="${response[i].studentUUID}" data-toggle="tooltip" title="${messages.addNote}"><i class="fa fa-comment"></i> ${messages.addNote}</a>
                                        <a  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-danger btn-lg  dropdown-item view-notes-list" data-studentName="${response[i].studentName}" data-studentUUID="${response[i].studentUUID}" data-toggle="tooltip" title="${messages.viewNotesList}"><i class="fa fa-list"></i> ${messages.notesDetail}</a>
                                        `

                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudents')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'dob'},
            {'data': 'gender'},
            // {'data': 'classShift'},
            {'data': 'classSection'},
            {'data': 'rollNo'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$('#students-form').on('submit', function(e){
    e.preventDefault();
    let formData = $('#filter-form').serializeArray();
    let moduleData = $('#students-form').serializeArray();
    var obj = {};
    formData.forEach((value, index) => {
        obj[value.name] = value.value;
    });
    moduleData.forEach((value, index) => {
        obj[value.name] = value.value;
    });
    showProgressBar(submitLoader);
    data = clean(obj);
    resetMessages();
    setTimeout(function(){
        submitStudent();
    }, 700);
});

$(document).on('keypress',function(e) {
    if(e.which == 13) {
        searchStudent()
    }
});

$('#generateUUID').on('click', function(){
    uuid = generateUUID()
    $('#tagID').val(uuid)
})

$(document).on('click','.isActiveCheck',function(){
    if($(this).prop('checked')){
        $('#isActive').val(1)
    }else{
        $('#isActive').val(0)
    }
})

$('#generateQR').on('click', function(){
    data = {}
    data.fullName = $('#studentName').val()
    data.tagString = $('#tagID').val()
    data.codeType = 1
    getQRCode(data)
})


function getQRCode(data){
    $.ajax({
        url: herokuUrl+'/getQRCode',
        method: 'POST',
        headers,
        data,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (response) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(response);
                a.href = url;
                a.download = $('#studentName').val()+'.png';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
                setTimeout(()=>{
                    $('.exportLoader').addClass('hidden');
                },500)
        }
    })
}
