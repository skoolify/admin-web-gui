$(document).ready(function(){
    getAjaxCountries();
    if(countryID !== ''){
        getCities();
    }
})

function submitCity(){
    $.ajax({
        url: herokuUrl+'/setCities',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);
                tableData.destroy();
                getCities();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
            $('form#cities-form').trigger('reset');
        },
        error: function(xhr){
            invokedFunctionArray.push('submitCity');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getCities(){
    if(countryID == ""){
        data = {}
    }else{
        data = {
            countryID: countryID
        }
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getCities',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'cityName': response[i].cityName,
                        'gmtOffset' : response[i].gmtOffset,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getCities');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'cityName'},
            {'data': 'gmtOffset'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

// $(document).on('click', '.list-record-edit', function(){
//     let json = JSON.parse($(this).attr('data-json'));
//     let data = JSON.parse($(this).attr('data-edit-json'))
// });

$('#cities-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#cities-form').serialize();
    showProgressBar(submitLoader);
    resetMessages();
    submitCity();
});
