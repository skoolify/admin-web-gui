$(document).ready(function(){
    getAjaxSchools();
    if(user.isAdmin == 1){
        schoolID = $('#filter-schools').val();
        branchID = $('#filter-branches').val();
    }
    if(schoolID !== '' && branchID !== ''){
        console.log(branchID)
        getRoles();
    }
})

function submitRole(){
    $.ajax({
        url: herokuUrl+'/setRole',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('#roles-form').trigger('reset');
                getRoles();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitRole')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getRoles(){
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getRoles',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                schoolID : schoolID,
                branchID : branchID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'roleName': response[i].roleName,
                        'roleDesc' : response[i].roleDesc !== 'undefined' && response[i].roleDesc ? response[i].roleDesc : '---',
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'userDetails': `<a data-role-id='${response[i].roleID}' data-role-name='${response[i].roleName}' class="btn btn-primary btn-sm linked-user-details-button"><i class="fa fa-list"></i> View</a>`,
                        'action' : `
                        <div class="btn-group">
                        ${__ACCESS__ ?
                            `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-role" data-role-id='${response[i].roleID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        <div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getRoles')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'roleName'},
            {'data': 'roleDesc'},
            {'data': 'isActive'},
            {'data': 'userDetails'},
            {'data': 'action'}
        ]
    });

}

$(document).on('click','.linked-user-details-button',function(){
  $('#linkedUserDetailModal').modal({
      backdrop: 'static',
      keyboard: false,
      toggle: true,
      }
  );
  $('#linkedUserDetailModal').modal('show');
  if (roleLinkedUserTable) {
      roleLinkedUserTable.destroy();
  }
  roleID = $(this).attr('data-role-id');
  roleLinkedUserTable = $('#linked-user-list').DataTable({
    language: {
        url: __DATATABLE_LANGUAGE__
    },
      'ajax': {
          url: herokuUrl+'/getRoleLinkedUsers',
          type: 'POST',
          headers,
          beforeSend: function(){
          },
          data: {
              branchID: branchID,
              roleID: roleID
          },
          dataSrc: function(res){
              response = parseResponse(res);
              var return_data = new Array();
              for(var i = 0; i< response.length; i++){
                  return_data.push({
                      'sNo' : i+1,
                      'name': response[i].fullname,
                      'phone' : response[i].mobileNo,
                      'isActive' : `<label class="label label-sm label-${response[i].isActive == 1 ? 'success' : 'danger'}"> ${response[i].isActive == 1 ? messages.active : messages.inActive}</label>`,
                  })
              }
              return return_data;
          },
          error: function(xhr){
            window.requestForToken(xhr)
            if (xhr.status === 404) {
                $('.dataTables_empty').html(messages.noDataAvailable);
             }
          console.log(xhr);
        }
      },
      "columns"    : [
          {'data': 'sNo'},
          {'data': 'name'},
          {'data': 'phone'},
          {'data': 'isActive'}
      ]
  });
});

$('#roles-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#roles-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
    showProgressBar(submitLoader);
    resetMessages();
    submitRole();
});

$(document).on('click', '.delete-role', function(){
    var  selectedRoleID = $(this).attr('data-role-id');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheRole+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteRole(selectedRoleID);
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

function deleteRole(selectedRoleID){
    $.ajax({
        url: herokuUrl+'/deleteRole',
        method: 'POST',
        headers,
        data: {
            roleID: selectedRoleID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.roleHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getRoles();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteRole')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
