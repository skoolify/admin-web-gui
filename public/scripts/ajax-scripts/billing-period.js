$(document).ready(function () {
    getAjaxSchools();
})

$(document).on('change', '#filter-branches', function(){
    if(branchID){
        getBillingPeriod();
    }
})

var billingPeriodID = 0;
var tr = '';

function getBillingPeriod(){
    if (tableData) {
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getBillingPeriod',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                schoolID: schoolID,
                branchID: branchID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'periodName': response[i].periodName,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>
                        <button class="btn-danger btn btn-xs list-record-delete" data-toggle="tooltip" title="${messages.delete}" data-id='${JSON.stringify(response[i].billingPeriodID)}'><i class="fa fa-trash"></i></button>` : ''}`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getBillingPeriod');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'periodName'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$(document).on('click', '.list-record-delete', function(){
   billingPeriodID = $(this).attr('data-id');
   tr = $(this).closest('tr');
    deleteBillingPeriod();
})

function deleteBillingPeriod(){
    $.confirm({
        title: messages.confirm,
        content: messages.areYouSureYouWantToDelete,
        type: 'red',
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        url: herokuUrl+'/deleteBillingPeriod',
                        method: 'POST',
                        headers,
                        data :{billingPeriodID: billingPeriodID},
                        beforeSend: function(){
                        },
                        success: function(res){
                            if(res.response === 'success'){
                                getBillingPeriod()
                            }
                        },
                        error: function(xhr){
                            invokedFunctionArray.push('deleteBillingPeriod')
                            window.requestForToken(xhr)
                            if (xhr.status === 422) {
                                parseValidationErrors(xhr);
                            }
                            if (xhr.status === 400) {
                                errorMsgShow.html(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                            }

                            if (xhr.status === 500) {
                                errorMsgShow.html(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
                            }
                            hideProgressBar(submitLoader);
                            disableButtonsToggle();
                        }
                    });
                }
            },
            // confirm: function () {

            // },
            cancel: {
               text: messages.cancelBtn,
               action : function (){
               }
           }
        }
    });
}

function submitBillingPeriod(){
    $.ajax({
        url: herokuUrl+'/setBillingPeriod',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#billing-period-form').trigger('reset');
                getBillingPeriod();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitBillingPeriod');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$('#billing-period-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#billing-period-form').serialize();
    showProgressBar(submitLoader);
    resetMessages();
    submitBillingPeriod();
});
