$(document).ready(function(){
    getAjaxSchools();
    // getExamTerm();
})

var  selectedExamTermID = 0;

function submitExamTerm(){
    $.ajax({
        url: herokuUrl+'/setExamTerm',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){

                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('#exam-term-form').trigger('reset');
                getExamTerm();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();

        },
        error: function(xhr){
            invokedFunctionArray.push('submitExamTerm');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getExamTerm(){

    let data = {}
    if(schoolID !== ''){
        data.schoolID = schoolID
    }

    if(branchID !== ''){
        data.branchID = branchID
    }

    if(tableData){
        tableData.destroy();
    }

    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getExamTerm',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'termName': response[i].termName,
                        'termNote' : response[i].termNote,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `
                        <div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-exam-term" data-examTerm-id='${response[i].examsTermID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        <div>
                        `
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getExamTerm');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'termName'},
            {'data': 'termNote'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$('#exam-term-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#exam-term-form').serialize();
    if($('#filter-schools').val() !== ''){
        data['schoolID'] = $('#filter-schools').val()
    }else{
        swal(
            messages.pleaseSelectSchool,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#filter-branches').val() !== ''){
        data['branchID'] = $('#filter-branches').val()
    }else{
        swal(
            messages.pleaseSelectBranch,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if($('#termName').val() !== ''){
        data['termName'] = $('#termName').val()
    }else{
        swal(
            messages.termNameIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if($('#termNote').val() !== ''){
        data['termNote'] = $('#termNote').val()
    }else{
        swal(
            messages.termNoteIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if($('#filter-date-from').val() !== ''){
        data['termStartDate'] = $('#filter-date-from').val()
    }else{
        swal(
            messages.startDateFieldIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if($('#filter-date-to').val() !== ''){
        data['termEndDate'] = $('#filter-date-to').val()
    }else{
        swal(
            messages.endDateFieldIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    showProgressBar(submitLoader);
    resetMessages();
    submitExamTerm();
});

$(document).on('click', '.delete-exam-term', function(){
    selectedExamTermID = $(this).attr('data-examTerm-id');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+" <br/> "+messages.youWantDeleteTheExamTerm+"</br>",
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteExamTerm();
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

function deleteExamTerm(){
    $.ajax({
        url: herokuUrl+'/deleteExamTerm',
        method: 'POST',
        headers,
        data: {
            examsTermID: selectedExamTermID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.examTermHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getExamTerm();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteExamTerm');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
