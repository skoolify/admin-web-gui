var subjectResultForm = $('#academic-form');
var academicList = [];
var totalSubjectResults = 0;
var totalStudents = 0;
var bulkArray = [];
// get all schools
$(document).ready(function () {
    getAjaxSchools();
})

function hideAndShowTable(){
    $('#student-result-table tbody').empty();
    $('#student-result-table tbody').append(`
    <tr>
    <td colspan="6"><strong style="color:#188ae2;font-weight:bold"> ${messages.loading}</<strong></td>
    </tr>
    `);
}

$(document).on('change','#filter-classLevels',function(){
    var assessmentOnlyFlag = $('#filter-classLevels').find(':selected').attr('data-assesstment');
    if(Number(assessmentOnlyFlag > 0 )){
        $('.non-assessment-body').addClass('hidden');
        $('#assessment-case').removeClass('hidden');
        $('#non-assessment-case').addClass('hidden');
    }else{
        $('.non-assessment-body').removeClass('hidden')
        $('#non-assessment-case').removeClass('hidden');
        $('#assessment-case').addClass('hidden');
    }
})

$(document).on('change','#filter-classes',function(){
    if(classID !== ''){
        $.ajax({
            url: apiUrl + '/students',
            method: 'GET',
            data: {
                classID: classID,
                branchID: branchID,
                schoolID: schoolID,
                classLevelID: classLevelID,
                classSection: classSection
            },
            success: function (res) {
                totalStudents = res.data.length;
            },
            error: function(err){
                window.requestForToken(err)
                console.log(err);
            }
        });
    }
});
// total masrks show and hide
$(document).on('change','#filter-subjects',() => {
    if(subjectID !== ''){
        $('#total-marks-div').show();
        hideAndShowTable();
        getAcademicSubjectResult();
        iterateStudentListTableForAcademic();
    }else{
        $('#total-marks-div').hide();
    }
});

$(document).on('change','#filter-exam-term',function(){
    if(examTermID !== ''){
        hideAndShowTable();
        getAcademicSubjectResult();
        iterateStudentListTableForAcademic();
    }
});

$('#show-add-form-container').on('click', function () {
        if(classID == ''){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }
        if(examTermID === ''){
            swal(
                messages.pleaseSelectExamTermFirst,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }

            getAcademicSubjectResult();
            $.LoadingOverlay("show");
            setTimeout(() => {
                iterateStudentListTableForAcademic();
            },1000)
    });


// Academic Result list
function getAcademicSubjectResult() {
    if (academicResult) {
        academicResult.destroy();
    }
    academicResult = $('#academic-result-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax':{
            url: herokuUrl + '/getExamSubjectResult',
            method: 'POST',
            headers,
            data: {
                subjectID: subjectID,
                examsTermID: examTermID,
                classID: classID
            },
            dataSrc: function (res) {
                response = parseResponse(res);
                totalSubjectResults = response.length;
                if(subjectID !== ''){
                    if(response.length && totalSubjectResults < totalStudents){
                        $('.academic-flag').removeClass('label-success');
                        $('.academic-flag').addClass('label-danger').html('Few Results are not submitted.').show(1000);

                    }else if( response.length && Number(totalStudents) == Number(totalSubjectResults)){
                        $('.academic-flag').removeClass('label-danger');
                        $('.academic-flag').addClass('label-success').html('Completed results').show(1000);
                    }else{
                        $('.academic-flag').hide();
                    }
                }

                var return_data = new Array();
                academicList = [];
                for (let i = 0; i < response.length; i++) {
                    const element = response[i];
                    academicList.push(element);
                    return_data.push({
                        'sNo': i + 1,
                        'studentName': element.studentName,
                        'subjectName': element.subjectName,
                        'totalMarks': element.totalMarks,
                        'obtainedMarks': element.obtainedMarks === 0 ? element.obtainedMarks+' (Absent)' : element.obtainedMarks,
                        'grade': element.grade,
                        'notes': element.notes,
                        'action': `${__DEL__ ?
                                    `<button class="btn-danger btn btn-xs delete-result" data-result-id='${element.examSubjectResultID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : '' }`,
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getAcademicSubjectResult');
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
            }
        },
        "columns": [
            {
                'data': 'sNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'subjectName'
            },
            {
                'data': 'totalMarks'
            },
            {
                'data': 'obtainedMarks'
            },
            {
                'data': 'grade'
            },
            {
                'data': 'notes'
            },
            {
                'data': 'action'
            }
        ]
    });

}

$(document).on('click', '.delete-result', function(){
    examSubjectResultID = $(this).attr('data-result-id');
   $.confirm({
       title: messages.confirm,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteThisResult+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteResult();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });
})

function deleteResult(){
    $.ajax({
        url: herokuUrl+'/deleteExamSubjectResult',
        method: 'POST',
        headers,
        data: {
            examSubjectResultID: examSubjectResultID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.resultHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                examSubjectResultID = '';
                if(tableData){
                    tableData.destroy();
                }
                getAcademicSubjectResult();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteResult');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

function iterateStudentListTableForAcademic(){
    var academicAddTable = $('#student-result-table tbody');
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {
            showProgressBar('p2-students-result-list');
        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                academicAddTable.empty();
                var assessmentOnlyFlag = $('#filter-classLevels').find(':selected').attr('data-assesstment');
                if(Number(assessmentOnlyFlag) == 0 ){
                if(res.data.length){
                    $.each(res.data, function(index, value){
                        let addedResult;
                        if(academicList){
                            addedResult = academicList.find(o => o.studentID === value.studentID);
                        }
                        rollNumber = value.rollNo.substring(0, 8)

                        if(addedResult !== undefined && addedResult !== ''){
                            $('#totalMarks').val(addedResult.totalMarks);
                            // console.log('here',typeof addedResult.showMarksOnApp)
                            academicAddTable.append(`
                            <tr>
                            <td style="display:none;">
                                <input class="form-control" name="examSubjectResultID" type="text" value='${addedResult.examSubjectResultID}'>
                            </td>
                            <td style="color:#188ae2;font-weight:bold; text-align:center">${index + 1}</td>
                            <td><span data-toggle="tooltip" title="${value.rollNo}">
                            ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                            </span></td>
                            <td style="display:none;">
                                <input class="form-control" name="studentID" type="text" value='${value.studentID}'>
                            </td>
                            <td>${value.studentName}</td>
                            <td>
                                <input type="hidden" name="isAbsent" id="hidden-isAbsent-${index}" class="hidden-check" value="${addedResult.isAbsent && addedResult.isAbsent !== null && addedResult.isAbsent === 'A' ? addedResult.isAbsent : 'P'}"  />
                                <div  class="checkbox checkbox-icon-blue">
                                    <input id="isAbsent-${index}" data-id="${index}" class="isAbsent" type="checkbox" ${addedResult.isAbsent && addedResult.isAbsent !== null && addedResult.isAbsent === 'A' ? 'checked': ''}>
                                    <label for="isAbsent-${index}">
                                    </label>
                                </div>
                            </td>
                            <td>
                                <input onKeyUp="numericFilter(this);" class="form-control" name="obtainedMarks" id="obtainedMarks-${index}" type="text" value="${addedResult.obtainedMarks !== null && addedResult.obtainedMarks !== 'null' ?  addedResult.obtainedMarks : 0 }" ${addedResult.isAbsent && addedResult.isAbsent !== null && addedResult.isAbsent === 'A' ? 'readonly': ''}>
                            </td>
                            <td>
                                    <input maxlength="2" class="form-control" data-validation="length" data-validation-length="max2" name="grade" type="text" value="${addedResult.grade != '' && addedResult.grade != null && addedResult.grade != 'undefined' ? addedResult.grade : ''}">
                            </td>
                            <td>
                                <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="notes" type="text" value="">${addedResult.notes != '' && addedResult.notes != null && addedResult.notes != 'undefined' ? addedResult.notes : ''}</textarea>
                            </td>
                            <td>
                                <input type="hidden" name="showMarksOnApp" id="hidden-select-subject-result-${index}" class="hidden-check" value="${addedResult.showMarksOnApp}"  />
                                <div  class="checkbox checkbox-icon-blue">
                                    <input id="select-subject-result-${index}" data-id="select-subject-result-${index}" class="subjectResultCheckBox" type="checkbox" ${addedResult.showMarksOnApp === 1 ? 'checked': ''}>
                                    <label for="select-subject-result-${index}" style="margin-left: 48px;">
                                    </label>
                                </div>
                            </td>
                            </tr>`);
                        }else{
                            academicAddTable.append(`
                            <tr>
                                <td style="display:none;">
                                    <input class="form-control" name="examSubjectResultID" type="text" value='0'>
                                </td>
                                <td style="color:#188ae2;font-weight:bold;text-align:center;">${index + 1}</td>
                                <td><span data-toggle="tooltip" title="${value.rollNo}">
                                ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                                </span></td>
                                <td style="display:none;">
                                    <input class="form-control" name="studentID" type="text" value='${value.studentID}'>
                                </td>
                                <td>${value.studentName}</td>
                                <td>
                                    <input type="hidden" name="isAbsent" class="hidden-check" id="hidden-isAbsent-${index}" value="P"  />
                                    <div class="checkbox checkbox-icon-blue">
                                        <input  id="isAbsent-${index}" data-id="${index}" class="isAbsent" type="checkbox">
                                        <label for="isAbsent-${index}">
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <input onKeyUp="numericFilter(this);" class="form-control" name="obtainedMarks" id="obtainedMarks-${index}" type="text">
                                </td>
                                <td>
                                    <input maxlength="2" class="form-control" data-validation="length" data-validation-length="max2" name="grade" type="text">
                                </td>
                                <td>
                                <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="notes" type="text" value=""></textarea>
                                </td>
                                <td>
                                    <input type="hidden" name="showMarksOnApp" class="hidden-check" id="hidden-select-subject-result-${index}" value="0"  />
                                    <div class="checkbox checkbox-icon-blue">
                                        <input  id="select-subject-result-${index}" data-id="select-subject-result-${index}" class="subjectResultCheckBox" type="checkbox">
                                        <label for="select-subject-result-${index}" style="margin-left: 48px;">
                                        </label>
                                    </div>
                                </td>
                            </tr>`);
                        }
                        });
                        // $.LoadingOverlay("hide");
                    hideProgressBar('p2-students-result-list');
                }else{
                    academicAddTable.empty();
                    academicAddTable.append(`
                        <tr>
                            <td colspan="7" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)
                        // $.LoadingOverlay("hide");
                    hideProgressBar('p2-students-result-list');
                }

                } else {
                    academicAddTable.empty();
                    academicAddTable.append(`<tr>
                            <td colspan="7" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.thisClassOnlyHasAssessments}</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                }
            } else {
                academicAddTable.empty();
                academicAddTable.append(`<tr>
                        <td colspan="7" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                    </tr>`)
                hideProgressBar('p2-students-result-list');
            }
            $.LoadingOverlay("hide");
        },
        error: function (err) {
            invokedFunctionArray.push('iterateStudentListTableForAcademic');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-students-result-list');
            $.LoadingOverlay("hide");
        }
    })
}

$(document).on('click','.isAbsent',function(){
    if($(this).prop('checked')){
        let studentID = $(this).attr('data-id');
        $('#hidden-isAbsent-'+studentID).val('A')
        $('#obtainedMarks-'+studentID).attr('readonly', true)
        $('#obtainedMarks-'+studentID).val(0);
    }else{
        let studentID = $(this).attr('data-id');
        $('#hidden-isAbsent-'+studentID).val('P')
        $('#obtainedMarks-'+studentID).attr('readonly', false)
        $('#obtainedMarks-'+studentID).val('');
    }
})

$('#subject-bulk-access').on('click', function(){
    if($('.subjectResultCheckBoxAll').prop('checked')){
        $('.subjectResultCheckBoxAll').prop('checked', false);
        $(".hidden-check").val(0);
        $('.subjectResultCheckBox').prop('checked', false)
    }else{
        $('.subjectResultCheckBoxAll').prop('checked', true);
        $(".hidden-check").val(1);
        $('.subjectResultCheckBox').prop('checked', true)
    }
})

$(document).on('change','.subjectResultCheckBox',function(){
    var selectedDataAttr = $(this).attr('data-id');
    var isChecked = $(this).prop('checked');

    if(isChecked){
        $("#hidden-" + selectedDataAttr).val(1);
    }else{
        $("#hidden-" + selectedDataAttr).val(0);
    }
})

subjectResultForm.on('submit', function (e) {
    e.preventDefault();
    var totalMarks = $('#totalMarks').val();
    if(subjectID === ''){
        swal(
            messages.pleaseSelectSubject,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if(totalMarks === ''){
        swal(
            messages.pleaseSelectTotalMarks,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    var academicEntryFinalArray = [];
    bulkArray = [];
    var result = $(this).serializeArray();
    var filtersformData = $('#filter-form').serializeArray();
    var sliecedArray = filtersformData.slice(4, filtersformData.length);

    var chunkedArray = chunkArray(result, 7);
    chunkedArray.forEach((val, index) => {
        var myObject = {};
        myObject["userID"] = user.userID;
        sliecedArray.forEach((data, formIndex) => {
            myObject[data.name] = data.value ;
        });

        val.forEach((innerVal, innerIndex) => {

            if(innerVal.name == 'examSubjectResultID'){
                if( Number(innerVal.value) > 0){
                    myObject[innerVal.name] = innerVal.value;
                }
            }else{
                myObject[innerVal.name] = innerVal.value;
            }
        });

        bulkArray.push(myObject);
    });
    submitAcademicResultArray();
});

function submitAcademicResultArray(){
    if(bulkArray.length > 0){
        $.ajax({
            url: herokuUrl + '/setExamSubjectResultsArray',
            method: 'POST',
            headers,
            data: {
                array: bulkArray
            },
            beforeSend: function () {
                disableButtonsToggle();
                subjectResultForm.find('.form-message').remove();
                showProgressBar(submitLoader);
            },
            success: function (res) {
                if (res.response === 'success') {
                    subjectResultForm.trigger('reset');
                    $('#subject-result-hidden-field').removeAttr('name value');
                    swal(
                        messages.success,
                        messages.successfullySubmitted,
                        'success'
                    );
                    getAcademicSubjectResult();
                    hideAddFormContainer();
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            },
            error: function (xhr) {
                invokedFunctionArray.push('submitAcademicResultArray');
                window.requestForToken(xhr)
                if (xhr.status === 422) {
                    parseValidationErrors(xhr);
                }
                if (xhr.status === 400) {
                    subjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                }

                if (xhr.status === 500) {
                    subjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            }
        });
    }
}

function numericFilter(txb) {
    txb.value = txb.value.replace(/[^\0-9]/ig, "");
}
