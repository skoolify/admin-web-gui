$('#searchSettlementReport').on('click', function(e){
    e.preventDefault();
    getSettlementSchoolReport();

});

$(document).ready(function(){
    getAjaxSchools();
})

var rowId = 0;

function getSettlementSchoolReport(){
    data = {}
    if(user.isAdmin == 1){
        if($('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val();
        }else{
            data.schoolID = user.schoolID;
        }

        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            data.branchID = user.branchID;
        }
    }else{
        data.schoolID = user.schoolID;
        data.branchID = user.branchID;
    }
    data.isExport = 0;
    dateFromVal = $('#filter-date-from').val();
    dateToVal = $('#filter-date-to').val();

    if(dateFromVal !== ''){
        data.fromDate = dateFromVal
    }
    if(dateToVal !== ''){
        data.toDate = dateToVal
    }

    isVerified = $('#filter-verified').val();
    isSettled = $('#filter-settled').val();

    if(isVerified !== ''){
        data.isVerified = isVerified
    }
    if(isSettled !== ''){
        data.isSettled = isSettled
    }

    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getSettlementSchoolReport',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i < response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'studentName' : response[i].studentName,
                        'class' : response[i].classAlias+'-'+response[i].classSection,
                        'invoiceNo' : response[i].invoiceNumber,
                        'billingPeriod' : response[i].periodName,
                        'tranDate' : moment(response[i].transactionDate).format('DD-MMM-YYYY'),
                        'billedAmount' : addCommas(response[i].billAmount),
                        'fixedPercentage' : response[i].MDRType,
                        'collectionRate' : response[i].MDRRate,
                        'collectionAmount' : addCommas(response[i].MDRAmount),

                        'taxRate' : response[i].taxRate,
                        'taxValue' : response[i].taxValue,
                        'settledAmount' : addCommas(response[i].schoolSettlementAmount),
                        'isSettled' :  `<div class="col-md-6" style="padding: 10px;">
                                            <div class="isSettledDiv-${i+1} checkbox checkbox-aqua d-flex">
                                                <input type="checkbox" class="isSettled" id="isSettled-${i+1}" data-row-id="${i+1}" value="${response[i].isSettled}" data-voucher-UUID="${response[i].voucherUUID}" ${response[i].isSettled == '1' ? "checked" : "none" } ${__IS_ADMIN__ != '1' ? "disabled" : "none" }>
                                                <label for="isSettled-${i+1}"></label>
                                            </div>
                                            <i class="settledLoader-${i+1} hidden fa fa-spinner fa-pulse pull-right mt-3" style="font-size:24px;color: #188ae2;"></i>
                                        </div>`,
                        'settledDate' : response[i].settlementDate != null ? moment(response[i].settlementDate).format('DD-MMM-YYYY') : '--',
                        'isVerified' :  `<div class="col-md-6" style="padding: 10px;">
                                            <div class="isVerifiedDiv-${i+1} checkbox checkbox-aqua d-flex">
                                                <input type="checkbox" class="isVerified" id="isVerified-${i+1}" data-row-id="${i+1}" value="${response[i].isVerified}" data-voucher-UUID="${response[i].voucherUUID}" ${response[i].isVerified == '1' ? "checked" : "none" }>
                                                <label for="isVerified-${i+1}"></label>
                                            </div>
                                            <i class="verifiedLoader-${i+1} hidden fa fa-spinner fa-pulse pull-right mt-3" style="font-size:24px;color: #188ae2;"></i>
                                        </div>`,
                        'verifiedDate' : response[i].verificationDate != null ? moment(response[i].verificationDate).format('DD-MMM-YYYY') : '--',
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getSettlementSchoolReport');
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                if(xhr.status === 400){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data' : 'studentName'},
            {'data' : 'class'},
            {'data' : 'invoiceNo'},
            {'data' : 'billingPeriod'},
            {'data' : 'tranDate'},
            {'data' : 'billedAmount'},
            {'data' : 'fixedPercentage'},
            {'data' : 'collectionRate'},
            {'data' : 'collectionAmount'},
            {'data' : 'taxRate'},
            {'data' : 'taxValue'},
            {'data' : 'settledAmount'},
            {'data' : 'isSettled'},
            {'data' : 'settledDate'},
            {'data' : 'isVerified'},
            {'data' : 'verifiedDate'},
        ]
    });

}

$('#exportSettlementReport').on('click', function(e){
    e.preventDefault();
    getSettlementSchoolReportExport();

});

function getSettlementSchoolReportExport(){
    data = {}
    if(user.isAdmin == 1){
        if($('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val();
        }else{
            data.schoolID = user.schoolID;
        }

        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            data.branchID = user.branchID;
        }
    }else{
        data.schoolID = user.schoolID;
        data.branchID = user.branchID;
    }
    data.isExport = 1;
    dateFromVal = $('#filter-date-from').val();
    dateToVal = $('#filter-date-to').val();

    if(dateFromVal !== ''){
        data.fromDate = dateFromVal
    }
    if(dateToVal !== ''){
        data.toDate = dateToVal
    }

    isVerified = $('#filter-verified').val();
    isSettled = $('#filter-settled').val();

    if(isVerified !== ''){
        data.isVerified = isVerified
    }
    if(isSettled !== ''){
        data.isSettled = isSettled
    }

    $.ajax({
        url: herokuUrl+'/getSettlementSchoolReport',
        method: 'POST',
        headers,
        data,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'settlement-report.xlsx';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
                setTimeout(()=>{
                    $('.exportLoader').addClass('hidden');
                },500)
        },
        error: function(xhr){
            invokedFunctionArray.push('getSettlementSchoolReport');
            window.requestForToken(xhr)
            if(xhr.status === 404){
                swal(
                    messages.errorMessage,
                    messages.noRecordFound,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
            }
            if(xhr.status === 400){
                $('.dataTables_empty').text('No data available');
            }
            console.log(xhr);
        }
    });

}

$(document).on('click', '.isSettled', function(){
    rowId = $(this).attr('data-row-id')
    $('.isSettledDiv-'+rowId).addClass('hidden')
    $('.settledLoader-'+rowId).removeClass('hidden')

    let isSettledId = $(this).attr('id')
    if($('#'+isSettledId).prop('checked')){
        $('#'+isSettledId).val(1)
    }else{
        $('#'+isSettledId).val(0)
    }

    let data = {}
    if(user.isAdmin == 1){
        if($('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val();
        }else{
            data.schoolID = user.schoolID;
        }

        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            data.branchID = user.branchID;
        }
    }else{
        data.schoolID = user.schoolID;
        data.branchID = user.branchID;
    }
    data.voucherUUID = $('#'+isSettledId).attr('data-voucher-UUID')
    data.userUUID = user.userUUID;
    data.isSettled = $('#'+isSettledId).val();
    setSettlementVerification(data);
})

$(document).on('click', '.isVerified', function(){
    rowId = $(this).attr('data-row-id')
    $('.isVerifiedDiv-'+rowId).addClass('hidden')
    $('.verifiedLoader-'+rowId).removeClass('hidden')

    let isVerifiedId = $(this).attr('id')
    if($('#'+isVerifiedId).prop('checked')){
        $('#'+isVerifiedId).val(1)
    }else{
        $('#'+isVerifiedId).val(0)
    }

    let data = {}
    if(user.isAdmin == 1){
        if($('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val();
        }else{
            data.schoolID = user.schoolID;
        }

        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            data.branchID = user.branchID;
        }
    }else{
        data.schoolID = user.schoolID;
        data.branchID = user.branchID;
    }
    data.voucherUUID = $('#'+isVerifiedId).attr('data-voucher-UUID')
    data.userUUID = user.userUUID;
    data.isVerified = $('#'+isVerifiedId).val();
    setSettlementVerification(data);
})

function setSettlementVerification(data){
    $.ajax({
        url: herokuUrl+'/setSettlementVerification',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                if(data.isSettled){
                    $('.isSettledDiv-'+rowId).removeClass('hidden')
                    $('.settledLoader-'+rowId).addClass('hidden')

                    $('.isSettledDiv-'+rowId).addClass('checkbox-aqua-custom')
                    $('.isSettledDiv-'+rowId).addClass('checkbox-custom-border')
                    setTimeout(() => {
                        $('.isSettledDiv-'+rowId).removeClass('checkbox-aqua-custom')
                        $('.isSettledDiv-'+rowId).removeClass('checkbox-custom-border')
                    }, 1500);
                }else{
                    $('.isVerifiedDiv-'+rowId).removeClass('hidden')
                    $('.verifiedLoader-'+rowId).addClass('hidden')

                    $('.isVerifiedDiv-'+rowId).addClass('checkbox-aqua-custom')
                    $('.isVerifiedDiv-'+rowId).addClass('checkbox-custom-border')
                    setTimeout(() => {
                        $('.isVerifiedDiv-'+rowId).removeClass('checkbox-aqua-custom')
                        $('.isVerifiedDiv-'+rowId).removeClass('checkbox-custom-border')
                    }, 1500);
                }
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('setSettlementVerification')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 406){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.reason}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
