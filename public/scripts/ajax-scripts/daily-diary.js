var user = JSON.parse(localStorage.getItem('user'));
$(document).ready(function(){
    getAjaxSchools();
    // getDailyDiarySummary();
})
var classID = '';
var subjectID = '';

function submitDailyDiary(){
    var diaryGUID = null;
    var diaryID = null;
    var bulkArray = [];
    var studentArray = [];
    var students = [];
    if($('#editHiddenField').attr('name')){
        diaryGUID = $('#editHiddenField').val();
    }
    // else if($('#editHiddenField').attr('name') == 'diaryID'){
    //     diaryID = $('#editHiddenField').val();
    // }

    let formData = $('#filter-form').serializeArray();
    var selectedClasses = [];
    var formDataRemainingData = [];
    var classObj;
    formData.forEach(function(value , index){
        if(value.name == 'classID'){
            selectedClasses.push(value);
        }
        if(value.name == 'studentID'){
            studentArray.push(value.value)
        }
    })
    let hiddenAttachment = $('#hiddenAttachment');
    let hiddenDocument = $('#hiddenDocument');

    if(diaryGUID !== null){
        data += '&diaryGUID='+diaryGUID;
    }
    selectedClasses.forEach(function(value, index){
        var obj = {};
        if(diaryGUID !== null){
            obj['diaryGUID']= diaryGUID;
        }
        // if(diaryID !== null){
        //     obj['diaryID']= diaryID;
        // }
        if(formData){
            findedObj = formData.find(o => o.name == 'subjectID');
        }
        if(findedObj !== '' && typeof findedObj !== 'undefined'){
            obj[findedObj.name] = findedObj.value;
        }
        if(hiddenDocument.val() !== ''){
            obj['documentLink'] = hiddenDocument.val();
        }
        if(hiddenAttachment.val() !== ''){
            obj['attachment']=hiddenAttachment.val();
        }
        // if(studentArray !== ''){
        //     studentArray.forEach(function(studentValue, studentIndex){
        //         obj['studentID'] = studentValue.value
        //         obj['diaryDate'] = $('#diaryDate').val();
        //         obj['dueDate'] = $('#dueDate').val();
        //         obj['description'] = $('#description').val();
        //         obj['userID'] = user.userID;
        //         obj[value.name] = value.value;
        //         bulkArray.push(obj);
        //     })
        // }else{
            obj['studentID'] = studentArray.join();
            obj['diaryDate'] = $('#diaryDate').val();
            obj['description'] = $('#description').val();
            if($('#dueDate').val()){
                obj['dueDate'] = $('#dueDate').val();
            }
            obj['subjectFlag'] = $('#hidden-flag').val();
            obj['userID'] = user.userID;
            obj[value.name] = value.value;
            bulkArray.push(obj);
            // }
        });
    $.ajax({
        url: herokuUrl+'/setDailyDiaryArray',
        method: 'POST',
        headers,
        data: {'array': bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
            //showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#daily-diary-form').trigger('reset');
                getDailyDiarySummary();
                hideProgressBar(submitLoader);
                $('#hiddenAttachment').val('');
                $('#hiddenDocument').val('');
                $('#submit-add-form-container').removeAttr('disabled');
                $('#hide-add-form-container').removeAttr('disabled');
                $("#description").text('');
            }else{
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${messages.somethingWentWrongPleaseTryAgain}</strong></p>`);
                location.reload();
            }

        },
        error: function(xhr){
            invokedFunctionArray.push('submitDailyDiary');
            window.requestForToken(xhr)
            $('#hiddenAttachment').val('');
            $('#hiddenDocument').val('');
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

// allow only alphanumeric
$('#description111').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
});

$(document).on('click', '.diary-detail', function () {
    diaryGUID = $(this).attr('data-GUID');
    classID = $(this).attr('data-recordID');
    subjectID = $(this).attr('data-subject-id');
    getDailyDiary();
});

function getDailyDiary(){
    // let data = {}
    // if(classID !== ''){
    //     data.classID = classID
    // }
    // if(subjectID !== ''){
    //     data.subjectID = subjectID;
    // }
    // var classID = $(this).attr('data-recordID');
    if (parentDetailTable) {
        parentDetailTable.destroy();
    }

    parentDetailTable = $('#diary-detail-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
            'ajax'       : {
                url: herokuUrl+'/getDailyDiaryListing',
                type: 'POST',
                headers,
                beforeSend: function(){
                },
                data:{
                    classID:classID,
                    subjectID:subjectID,
                    diaryGUID:diaryGUID
                    },
                dataSrc: function(res){
                    response = parseResponse(res);
                    var return_data = new Array();
                    var currentDiaryDate = moment.utc(response[0].diaryDate)
                    currentDiaryDate = currentDiaryDate.local().format('DD-MMM-YYYY')
                    $('#classSection').text(response[0].classAlias+'-'+response[0].classSection ? response[0].classAlias+'-'+response[0].classSection : '-');
                    $('#divDiaryDate').text(currentDiaryDate ? currentDiaryDate : '-');
                    $('#teacherName').text(response[0].fullName ? response[0].fullName : '-');
                    $('#subjectName').text(response[0].subjectName ? response[0].subjectName : '-');
                    for(var i = 0; i< response.length; i++){

                        var dairyDate = moment.utc(response[i].diaryDate);
                        dairyDate = dairyDate.local().format('DD-MMM-YYYY')

                        var dueDate = moment.utc(response[i].dueDate);
                        dueDate = dueDate.local().format('DD-MMM-YYYY')

                        let diaryDescDetail = response[i].description;
                        let diaryDescription = response[i].description;
                        return_data.push({
                            'sNo' : i+1,
                            'studentName' : response[i].studentName,
                            'description' : diaryDescDetail,
                            'dueDate' : dueDate && dueDate !== null && dueDate !== 'Invalid date' ? `<span style="display:none">${Date.parse(dueDate)}</span>`+ dueDate : '--',
                            'action' : `${response[i].documentLink !== null && response[i].documentLink !== undefined && response[i].documentLink !== 'undefined' ? `
                            <div class="btn-group">
                                <a href="${response[i].documentLink}"  target="_blank" title="Document" class="btn btn-xs btn-warning m-0"><i class="fa fa-file"></i>
                                </a>
                            ` : ''}
                            ${response[i].attachment !== null && response[i].attachment !== undefined && response[i].attachment !== 'undefined' ? `
                            <div class="btn-group">
                                <a href="${response[i].attachment}"  target="_blank" title="Attachment" class="btn btn-xs btn-success m-0"><i class="fa fa-image"></i>
                                </a>
                            ` : ''}
                            ${__ACCESS__ ? `
                            <div class="btn-group">
                            <a class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" data-dismiss="modal" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}' data-description="${diaryDescription}"  ><i class="fa fa-pencil"></i></a>`: ''}
                            ${__DEL__ ? `<a class="btn-danger btn btn-xs delete-diary" data-toggle="tooltip" title="${messages.delete}" data-student-id="${response[i].studentID}" data-id='${response[i].diaryGUID}'><i class="fa fa-trash"></i></a>`: ''}
                            </div>`
                        })
                    }
                    return return_data;
                },
                error: function(xhr){
                    invokedFunctionArray.push('getDailyDiary');
                    window.requestForToken(xhr)
                    console.log(xhr);
                    if(xhr.status === 404){
                        $('.dataTables_empty').text(messages.noDataAvailable);
                    }
                }
            },
            "columns"    : [
                {'data': 'sNo'},
                {'data': 'studentName'},
                {'data': 'description'},
                {'data': 'dueDate'},
                {'data': 'action'}
            ]
        });
        $('#fathersDetailModal').modal('toggle');
        $('#fathersDetailModal').modal('show');
    // }

}

// $('.popup-list-record-edit').on('click', function(){
//     let json = JSON.parse($(this).attr('data-json'));
//     console.log(json,'json')
//     $("#filter-students option[value='" + studentID + "']").prop("selected", true);
//     $('#filter-students').multiselect('rebuild');
// });

$('#documentUpload').on('change', function(){
    if($(this).val() === ''){
        $('#hiddenDocument').val('');
    }
});

$('#attachmentUpload').on('change', function(){
    readURL(this,'diary-attachment');
    if($(this).val() === ''){
        $('#hiddenAttachment').val('');
    }
});

$(document).on('click','.delete-attachment', function(){
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSureYouWantToDelete,
        buttons: {
            confirm: function () {
                $('#hiddenAttachment').val('');
                $('#attachement-div').slideUp(1000);
                $("#attachmentUpload").val('');
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        },
    })
})


function readURL(input,imagTag) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+imagTag).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        $("#attachmentUpload").show();
    }
}
$(document).on('click','.delete-document', function(){
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSureYouWantToDelete,
        buttons: {
            confirm: function () {
                $('#hiddenDocument').val('');
                $('#document-div').slideUp(1000);
            },
            cancel: function () {
            },
        },
    })
})
$(document).on('click','.delete-diary', function(){
    let data = {};
    var diaryGUID = $(this).attr('data-id');
    var studentID = '';
    if($(this).attr('data-student-id')){
        studentID = $(this).attr('data-student-id');
    }

    if(diaryGUID !== ''){
        data.diaryGUID = diaryGUID;
    }
    if(studentID !== ''){
        data.studentID = studentID;
    }

    var tr = $(this).closest('tr');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSureYouWantToDelete,
        buttons: {
            confirm: function () {
                $.ajax({
                    url: herokuUrl + '/deleteDailyDiary',
                    type: 'POST',
                    data: data,
                    dataType:'JSON',
                    headers,
                    success: function(data){
                        if(data.response === 'success'){
                            tr.fadeOut(1000);
                            getDailyDiarySummary();
                        }
                    },
                    error: function(xhr){
                        window.requestForToken(xhr)
                        console.log(xhr);
                    }
                });
            },
            cancel: function () {
            },
        },
    })
});

$('#filter-subjects').on('change', function(){
    flag = $('option:selected', this).attr('attribute');
    if(flag === 'Non-Subject(s)'){
        $('#hidden-flag').val(0)
    }else{
        $('#hidden-flag').val(1)
    }
})

$('#filter-classes').on('change', function(){
    flag = $('option:selected', this).attr('attribute');
    if(flag === 'Non-Subject(s)'){
        $('#hidden-flag').val(0)
    }else{
        $('#hidden-flag').val(1)
    }
})

$('#daily-diary-form').on('submit', function(e){
    studentID = $('#filter-students').val();
    if(!(studentID.length >= 0)){
        swal(
            messages.pleaseSelectStudent,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    if($('#filter-subjects').val() == ''){
        swal(
            messages.pleaseSelectSubject,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    e.preventDefault();
    disableButtonsToggle();
    data = $('#filter-form').serialize()+'&'+$('#daily-diary-form').serialize()+'&userID='+user.userID;
    $.LoadingOverlay("show");
    // console.log(data,'data')
    // return

    resetMessages();
    var formData = new FormData($(this)[0]);
    var att = $('#attachmentUpload').val();
    var doc = $('#documentUpload').val();

    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

    if(att !== ''){
        setTimeout(function(){
            $.ajax({
                url: __BASE__+'/daily-diary/attachment',
                type: 'POST',
                data: formData,
                async: false,
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function(){
                    //disableButtonsToggle();
                },
                success: function(xhr){
                    if(xhr){
                        $('#hiddenAttachment').val(__BASE__+'/attachments/'+xhr.attachment);
                    }
                    if(doc == ''){
                        $.LoadingOverlay("hide");
                        submitDailyDiary();
                    }
                    // hideProgressBar(submitLoader);
                    // disableButtonsToggle();
                },
                error: function(xhr){
                    window.requestForToken(xhr)
                    if(xhr.status === 422){
                        let errors = xhr.responseJSON.errors;
                        $.each(errors, function(key, val){
                            $('#add-form-errors').html(`<p class="text-danger"><strong>${val[0]}</strong></p>`);
                        });
                    }
                    $.LoadingOverlay("hide");
                }
            });
        }, 200)
    }

    if(doc !== ''){
        setTimeout(function(){
            $.ajax({
                url: __BASE__+'/daily-diary/document',
                type: 'POST',
                data: formData,
                dataType:'JSON',
                async: false,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function(){
                    //disableButtonsToggle();
                },
                success: function(xhr){
                    if(xhr){
                        $('#hiddenDocument').val(__BASE__+'/documents/'+xhr.document);
                    }
                    // hideProgressBar(submitLoader);
                    // disableButtonsToggle();
                    submitDailyDiary();
                    // hide-add-form-container
                },
                error: function(xhr){
                    window.requestForToken(xhr)
                    if(xhr.status === 422){
                        let errors = xhr.responseJSON.errors;
                        $.each(errors, function(key, val){
                            $('#add-form-errors').html(`<p class="text-danger"><strong>${val[0]}</strong></p>`);
                        });
                    }
                    $.LoadingOverlay("hide");
                }
            });
        }, 500)
    }
    //hideProgressBar(submitLoader);
    disableButtonsToggle();

    if(doc == '' && att == ''){
        submitDailyDiary();
    }
    $.LoadingOverlay("hide");

});

$('.search-diary').on('click', function(e){
    e.preventDefault()
    let dateFrom = $('#dateFrom').val();
    let dateTo = $('#dateTo').val();
    if(classLevelID === ''){
        swal(
            messages.error,
            messages.pleaseSelectClass,
            'error'
        )
        return false;
    }
    if(dateFrom === '' && dateTo === ''){
        swal(
            messages.error,
            messages.pleaseSelectDatesFirst,
            'error'
        )
        return false;
    }
    getDailyDiarySummary();
});

function getDailyDiarySummary(){
    let fromDate = $('#dateFrom').val();
    let toDate = $('#dateTo').val();
    let data = {}
    let classLevelID = $('#filter-classLevels').val();
    if(classLevelID !== ''){
        data.classLevelID = classLevelID
    }
    if(classID !== ''){
        data.classID = classID
    }
    if(subjectID !== ''){
        data.subjectID = subjectID;
    }
    if(fromDate !== ''){
        data.fromDate = fromDate
    }
    if(toDate !== ''){
        data.toDate = toDate
    }
    if(studentID !== ''){
        data.studentID = studentID
    }

    if(tableData){
            tableData.destroy();
    }
        tableData = $('#example44').DataTable({
            language: {
                url: __DATATABLE_LANGUAGE__
            },
            'ajax'       : {
                url: herokuUrl+'/getDailyDiarySummary',
                type: 'POST',
                headers,
                beforeSend: function(){
                },
                data: data,
                dataSrc: function(res){
                    response = parseResponse(res);
                    var return_data = new Array();
                    for(var i = 0; i< response.length; i++){
                        var dairyDate = moment.utc(response[i].diaryDate);
                        dairyDate = dairyDate.local().format('DD-MMM-YYYY')

                        var dueDate = moment.utc(response[i].dueDate);
                        dueDate = dueDate.local().format('DD-MMM-YYYY')

                        let diaryDescDetail = response[i].description;
                        let diaryDescription = response[i].description;
                        var description = forge.util.encodeUtf8(response[i].description);
                        var description = forge.util.encode64(description);
                        return_data.push({
                            'sNo' : i+1,
                            'fullName' : response[i].fullName,
                            'subjectName' : response[i].subjectName,
                            'classSection' : response[i].classAlias+'-'+response[i].classSection,
                            'description' : `<button class="btn btn-xs btn-primary" style="font-size: 12px;" href="#" id="moreDescription" data-id="${description}" data-toggle="tooltip" title="Tab to view">${messages.description}</button>`,
                            'diaryDate' : dairyDate ? `<span style="display:none">${Date.parse(dairyDate)}</span>`+ dairyDate : '--',
                            'dueDate' : dueDate && dueDate !== 'Invalid date' && (dueDate !== '' || dueDate !== null) ? `<span style="display:none">${Date.parse(dueDate)}</span>`+ dueDate : '--',
                            'classSection' : `${response[i].classAlias}-${response[i].classSection} `,
                            // 'list': `<button class="btn-primary btn btn-xs diary-detail" data-toggle="tooltip" title="View Parent Detail" data-GUID="${response[i].diaryGUID}" data-subject-id="${response[i].subjectID}" data-recordID='${response[i].classID}'><i class="fa fa-list"></i>View</button>`,
                            'action' : `
                            <div class="btn-group">
                                <button class="btn-primary btn btn-xs diary-detail" data-toggle="tooltip" title="${messages.viewStudentDetail}" data-GUID="${response[i].diaryGUID}" data-subject-id="${response[i].subjectID}" data-recordID='${response[i].classID}'><i class="fa fa-list"></i></button>
                            ${response[i].documentLink !== null && response[i].documentLink !== undefined && response[i].documentLink !== 'undefined' ? `
                            <div class="btn-group">
                                <a href="${response[i].documentLink}"  target="_blank" title="Document" class="btn btn-xs btn-warning m-0"><i class="fa fa-file"></i>
                                </a>
                            ` : ''}
                            ${response[i].attachment !== null && response[i].attachment !== undefined && response[i].attachment !== 'undefined' ? `
                            <div class="btn-group">
                                <a href="${response[i].attachment}"  target="_blank" title="Attachment" class="btn btn-xs btn-success m-0"><i class="fa fa-image"></i>
                                </a>
                            ` : ''}
                            ${__ACCESS__ ? `
                            <div class="btn-group">
                            <a class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}' data-description="${diaryDescription}"  ><i class="fa fa-pencil"></i></a>`: ''}
                            ${__DEL__ ? `<a class="btn-danger btn btn-xs delete-diary" data-toggle="tooltip" title="${messages.delete}" data-id='${response[i].diaryGUID}'><i class="fa fa-trash"></i></a>`: ''}
                            </div>`
                        })
                    }
                    return return_data;
                },
                error: function(xhr){
                    invokedFunctionArray.push('getDailyDiarySummary');
                    window.requestForToken(xhr)
                    if(xhr.status === 404){
                        $('.dataTables_empty').text(messages.noDataAvailable);
                    }else if(xhr.status === 400){
                        $('.dataTables_empty').text(messages.noDataAvailable);
                    }else if(xhr.status === 500){
                        $('.dataTables_empty').text(messages.noDataAvailable);
                    }
                }
            },
            "columns"    : [
                {'data': 'sNo'},
                {'data': 'fullName'},
                {'data': 'subjectName'},
                {'data': 'classSection'},
                {'data': 'description'},
                {'data': 'diaryDate'},
                {'data': 'dueDate'},
                // {'data': 'list'},
                {'data': 'action'}
            ]
        });
    // }
}

$(document).on('click', '#moreDescription', function(e){
    e.preventDefault();
    $('#descriptionModalParagraph').empty()
    let description = $(this).attr('data-id');
    description = forge.util.decode64(description);
    description = forge.util.decodeUtf8(description);
    $('#descriptionModalParagraph').append(`<p>${description}</p>`)
    $('#descriptionModal').modal('toggle');
    $('#descriptionModal').modal('show');
})
