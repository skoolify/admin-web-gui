var validated = false;
var loggedIn = false;
var validateBtn = $("#validateUser");
var loginBtn = $("#login");
var domain = __DOMAIN__;
var __REPROCESS__ = false;
var __RETRIES__ = 0;
var __TOKPROCESS__ = true;
var invokedFunctionArray = [];
var __DATA__ = null;

$(function(){

    $(document).ready(function(){
        if(typeof resetLastLink !== undefined && resetLastLink){
            //localStorage.removeItem('last-link');
        }
        getLogo(domain);
        // $('#mobileNo').keyup(function (e) {
        // var key = e.charCode || e.keyCode || 0;
        // $text = $(this);
        //     if ((e.which > 95 && e.which < 106) || (e.which > 47 && e.which < 58)) {
        //         // $('.mobile-no-validation').attr('id','mobileNo');
        //     }else{
        //         $('.mobile-no-validation').removeAttr('id');
        //         $('.mobile-no-validation').removeAttr('maxlength')
        //         let v = $(this).val().replace(/[^\w.]+/g, '');
        //     $(this).val(v);
        //     }
        // });
        // $('#mobileNo').keydown(function (e) {
        //     var key = e.charCode || e.keyCode || 0;
        //     $text = $(this);
        //     if ($text.val().length < 16) {
        //         if ((e.which > 95 && e.which < 106) || (e.which > 47 && e.which < 58)) {
        //             $(this).attr('maxlength','18')
        //             if (key !== 8 && key !== 9) {
        //                 if ($text.val().length === 0) {
        //                     $text.val($text.val() + '+92-(');
        //                 }
        //                 if ($text.val().length === 8) {
        //                     $text.val($text.val() + ') ');
        //                 }
        //                 if ($text.val().length === 13) {
        //                     $text.val($text.val() + '-');
        //                 }
        //                 return (
        //                     key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)
        //                 );
        //             }
        //         } else {
        //         let v = $(this).val().replace(/[^\w.]+/g, '');
        //         $(this).val(v);
        //         }
        //     }
        // });
    });

    $('#login-form').on('submit', function(e){
        $("#error-message").hide();
        e.preventDefault();
        let mobileVal;
        if($('#mobileNo').length){
            mobileVal = $('#mobileNo').val().replace(' ', '');
        }else{
            mobileVal = $('.mobile-no-validation').val().replace(' ', '');
        }
        mobileVal = mobileVal.replace(/[{()}]/g, '');
        mobileVal = mobileVal.replace(/-/g, '');
        $('#hiddenMobileNo').val(mobileVal);
        window.__DATA__ = $(this).serialize()+'&domain='+domain;
        if(!validated){
            validate();
        }else{
            login();
        }
    })
})

function validate(data = null){
    $.ajax({
        url: __BASE__+'auth/validate',
        method: 'POST',
        data: window.__DATA__,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(){
            validateBtn.attr('disabled', true);
            validateBtn.html('Validating...');
        },
        success: function(res){  
            if(!res.error){
                $("#login-password-container").fadeIn(500);
                validateBtn.hide();
                loginBtn.fadeIn();
                validateBtn.attr('type', 'button');
                validateBtn.removeAttr('disabled', false);
                validateBtn.html('Next');
                validated = true;
                $('#password').focus();
            }else{
                window.invokedFunctionArray.push('validate')
                window.requestForToken(res)
                if(res.code !== 503){
                    validateBtn.removeAttr('disabled', false);
                    validateBtn.html('Next');
                    $("#error-message").show().html(`<p>${res.message}</p>`);
                }
                $('#password').focus();
            }
        },
        error: function(err){
            validateBtn.removeAttr('disabled', false);
            validateBtn.html('Next');
            window.invokedFunctionArray.push('validate')
            window.requestForToken(err)
            $("#error-message").show().html('Sorry there is something wrong, please try again');
        }
    })
}

function login(data = null){
    $.ajax({
        url: __BASE__+'auth/login',
        method: 'POST',
        data: window.__DATA__,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(){
            loginBtn.attr('disabled', true);
            loginBtn.html('Logging In...');
        },
        success: function(res){  
            if(!res.error){
                getUserDetail();
            }else{
                if(res.code !== 503){
                    loginBtn.removeAttr('disabled', false);
                    loginBtn.html('Next');
                    $("#error-message").show().html(`<p>${res.message}</p>`);
                }
                window.invokedFunctionArray.push('login')
                window.requestForToken(res)
            }
        },
        error: function(err){
            loginBtn.removeAttr('disabled', false);
            loginBtn.html('Next');
            window.invokedFunctionArray.push('login')
            window.requestForToken(err)
            $("#error-message").show().html('Sorry there is something wrong, please try again');
        }
    })
}

function getUserDetail(){
    $.ajax({
        url: __BASE__+'auth/user/detail',
        method: 'GET',
        success: function(res){
            if(!res.error){
                var branchTemp = false;
                var roleTemp = false;
                if(res.response.length > 1){
                    res.response.forEach((elem, index) => {
                        if(elem.branchID !== null){
                            branchTemp = true;
                        }
                        if(elem.roleID !== null){
                            roleTemp = true;
                        }
                    });

                    if(!branchTemp){
                        $("#error-message").show().html('Sorry there is no branch assigned to you');
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html('Login');
                        return false;
                    }

                    if(!roleTemp){
                        $("#error-message").show().html('Sorry there is no role assigned to you');
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html('Login');
                        return false;
                    }
                }else{
                    if(res.response[0].branchID == null){
                        $("#error-message").show().html('Sorry there is no branch assigned to you');
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html('Login');
                        return false;
                    }

                    if(!res.response[0].roleID){
                        $("#error-message").show().html('Sorry there is no role assigned to you');
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html('Login');
                        return false;
                    }
                }
                                    
                localStorage.setItem('user', JSON.stringify(res.response));
                loginBtn.html('Redirecting...');
                
                window.location.replace(__BASE__+'/auth/authorization');
            }else{
                loginBtn.removeAttr('disabled', false);
                loginBtn.html('Next');
                window.invokedFunctionArray.push('getUserDetail')
                window.requestForToken(res)
                $("#error-message").show().html(`<p>${res.message}</p>`);
            }
        },
        error: function(err){
            loginBtn.removeAttr('disabled', false);
            loginBtn.html('Next');
            window.invokedFunctionArray.push('getUserDetail')
            window.requestForToken(err)
            $("#error-message").show().html('Sorry there is something wrong, please try again');
        }
    })
}

function getLogo(domain){
    $.ajax({
        url: __BASE__+'school-logo',
        method: 'POST',
        data: {
            "_token": __CSRF__,
            domain
        },
        success: function(res){            
            if(!res.error){
                let logo = res.response.schoolLogo.indexOf('base64,') > 0 ? res.response.schoolLogo : 'data:image/*;base64,'+res.response.schoolLogo;
                $('#schoolLogo').attr('src', logo).addClass('animated flipInY');
                if(res.response.maintenanceFlag == 1){
                    $('.field-maintain').css('display', 'none');
                    $('.field-unmaintain').css('display', 'block');
                }

                $('#maintenance-text').text(`${res.response.maintenanceText ? res.response.maintenanceText : ''}`).css('color', 'white');
            }else{
                window.invokedFunctionArray.push('getLogo')
                window.requestForToken(res)
            }
        },
        error: function(err){
            window.invokedFunctionArray.push('getLogo')
            window.requestForToken(err)
        }
    })
}

//Get API Token
function requestForToken(xhr){
    if (xhr.code === 503 || xhr.status === 503) {
        window.__REPROCESS__ = true;
        window.__RETRIES__ = window.__RETRIES__ + 1;
        window.getToken();
    }
}

function getToken(){
    let token = localStorage.getItem('apiToken');
    if (token !== null) {
        updateTockenOnExpiry();
    }
}
function updateTockenOnExpiry(){
    if(window.__TOKPROCESS__ === true){
        $.ajax({
            url: __BASE__ + 'ajax/updateApiToken',
            beforeSend: function(){
                window.__TOKPROCESS__ = false;
            },
            method: 'GET',
            data: {
            },
            success: function (res) {
                if (res.response) {
                    window.token = res.response;
                    localStorage.removeItem('apiToken');
                    localStorage.setItem('apiToken', JSON.stringify(res.response));
                    window.__TOKPROCESS__ = true;
                    if (window.__REPROCESS__ === true ) {
                        if(window.__RETRIES__ !== 0){
                            let retriesArr = invokedFunctionArray.slice((0 - window.__RETRIES__));
                            for (let t = 0; t < retriesArr.length; t++) {
                                window[retriesArr[t]]();
                            }
                            window.__RETRIES__ = 0;
                        }
                    }
                }
            },
            error: function (err) {
                window.requestForToken(err)
                $("#error-message").show('Sorry there is something wrong, please try again');
            }
        })
    }
}
