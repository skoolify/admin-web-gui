var isClassTeacherFlag = '0';
var teacherSubjectLinkID = null;
var deleteTeacherSubjectLinkID = null;
$(document).ready(function(){
    getAjaxSchools();
    // getSubjects();
})

function submitSubject(){
    $.ajax({
        url: herokuUrl+'/setSubjects',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                getSubjects();
                $('#subjects-form').trigger('reset');
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitSubject')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getSubjects(){

    // let route = 'getSubjects';
    var data = {}
    // {
    //     classLevelID: classLevelID
    // };
    // if(user.isAdmin != 1){
    //     data.userID = user.userID;
    //     data.classID = classID;
    //     route = 'getUserSubjects';
    // }

    if(user.isAdmin == 1){
        if(classLevelID != ''){
            data.classLevelID = classLevelID;
        }
        tableData = $('#example44').DataTable({
            language: {
                url: __DATATABLE_LANGUAGE__
            },
            'ajax'       : {
                url: herokuUrl+'/getSubjects',
                type: 'POST',
                headers,
                beforeSend: function(){
                },
                data,
                dataSrc: function(res){
                    response = parseResponse(res);
                    var return_data = new Array();
                    var count = 1;

                    for(var i = 0; i< response.length; i++){
                        var buttonGroup = '';
                        if(typeof response[i].subjectDetails !== 'undefined' && response[i].subjectDetails.length >= 1){
                            response[i].subjectDetails.forEach(function(value, index){
                                if(value.fullName !== null && value.fullName !== 'null' ){
                                    buttonGroup  += `<div class="btn-group">
                                    <button class="btn-primary btn btn-xs assign-teacher" data-subject-id="" data-toggle="tooltip" title="${value.fullName } ( ${value.classSection} )">${value.fullName } ( ${value.classSection} )</button>
                                    <button class="btn-danger btn btn-xs unassign-teacher" data-toggle="tooltip" title="${messages.unassignTeacher}" data-link-id='${value.teacherSubjectLinkID}'><i class="fa fa-remove"></i></button></div></br>`;
                                }
                            });
                        }
                        return_data.push({
                            'sNo' : count++,
                            'subjectName': response[i].subjectName,
                            'fullName': `${buttonGroup !== '' ? buttonGroup : ' ---- '}`,
                            'classSection':  response[i].classAlias,
                            'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                            'action' : `<div class="btn-group"><button class="btn-danger btn btn-xs assign-teacher" data-subject-link-id="${response[i].teacherSubjectLinkID}" data-subject-id="${response[i].subjectID}" data-toggle="tooltip" title="${messages.assignTeacher}">${messages.assignTeacher}</button> ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                            ${__DEL__ ? `
                            <button class="btn-danger btn btn-xs delete-subject" data-subject-id='${response[i].subjectID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                            ` : ''}</div>`
                        })
                        buttonGroup = '';

                    }
                    return return_data;
                },
                error: function(xhr){
                    invokedFunctionArray.push('getSubjects')
                    window.requestForToken(xhr)
                    console.log(xhr);
                }
            },
            "columns"    : [
                {'data': 'sNo'},
                {'data': 'subjectName'},
                {'data': 'fullName'},
                {'data': 'classSection'},
                {'data': 'isActive'},
                {'data': 'action'}
            ]
        });
    }else if(user.isAdmin != 1){
        if(user.userID != ''){
            data.userID = user.userID
        }
        // if(classID != ''){
            // data.classID = user.classID
            // console.log(user,'user')
        // }
        if(classLevelID != ''){
            data.classLevelID = classLevelID
        }
        tableData = $('#example44').DataTable({
            'ajax': {
                url: herokuUrl+'/getUserSubjects2',
                type: 'POST',
                headers,
                beforeSend: function(){
                },
                data,
                dataSrc: function(res){
                    response = res.response;
                    var return_data = new Array();
                    var count = 1;

                    for(var i = 0; i< response.length; i++){
                        var buttonGroup = '';
                        if(typeof response[i].subjectDetails !== 'undefined' && response[i].subjectDetails.length >= 1){

                            response[i].subjectDetails.forEach(function(value, index){
                                if(value.fullname !== null && value.fullname !== 'null' ){
                                    buttonGroup  += `<div class="btn-group">
                                    <button class="btn-primary btn btn-xs assign-teacher" data-subject-id="" data-toggle="tooltip" title="${value.fullname } ( ${value.classSection} )">${value.fullname } ( ${value.classSection} )</button>
                                    <button class="btn-danger btn btn-xs unassign-teacher" data-toggle="tooltip" title="${messages.unassignTeacher}" data-link-id='${value.teacherSubjectLinkID}'><i class="fa fa-remove"></i></button></div></br>`;
                                }
                            });
                        }
                        return_data.push({
                            'sNo' : count++,
                            'subjectName': response[i].subjectName,
                            'fullName': `${buttonGroup !== '' ? buttonGroup : ' ---- '}`,
                            'classSection':  response[i].classAlias,
                            'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                            'action' : `<div class="btn-group"><button class="btn-danger btn btn-xs assign-teacher" data-subject-link-id="${response[i].teacherSubjectLinkID}" data-subject-id="${response[i].subjectID}" data-toggle="tooltip" title="${messages.assignTeacher}">${messages.assignTeacher}</button> ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                            ${__DEL__ ? `
                            <button class="btn-danger btn btn-xs delete-subject" data-subject-id='${response[i].subjectID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                            ` : ''}</div>`
                        })
                        buttonGroup = '';

                    }
                    return return_data;
                },
                error: function(xhr){
                    invokedFunctionArray.push('getSubjects')
                    window.requestForToken(xhr)
                    // console.log(xhr.status);
                    if(xhr.status === 404){
                        $('.dataTables_empty').text(messages.noDataAvailable);
                    }
                }
            },
            "columns"    : [
                {'data': 'sNo'},
                {'data': 'subjectName'},
                {'data': 'fullName'},
                {'data': 'classSection'},
                {'data': 'isActive'},
                {'data': 'action'}
            ]
        });
    }



}

$(document).on('click', '.delete-subject', function(){
    subjectID = $(this).attr('data-subject-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+' <br/> '+messages.youWantToDeleteThisSubject+'</br>',
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteSubject();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteSubject(){
    $.ajax({
        url: herokuUrl+'/deleteSubject',
        method: 'POST',
        headers,
        data: {
            subjectID: subjectID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.subjectHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                subjectID = '';
                if(tableData){
                    tableData.destroy();
                }
                getSubjects();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteSubject');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('change','#filter-classLevels',function(){
    var assessmentOnlyFlag = $('#filter-classLevels').find(':selected').attr('data-assesstment');
    if(Number(assessmentOnlyFlag > 0 )){
        $('.non-assessment-body').addClass('hidden');
        $('#assessment-case').removeClass('hidden');
        $('#non-assessment-case').addClass('hidden');
    }else{
        $('.non-assessment-body').removeClass('hidden')
        $('#non-assessment-case').removeClass('hidden');
        $('#assessment-case').addClass('hidden');
    }
})

$(document).on('click', '.unassign-teacher', function(){
    deleteTeacherSubjectLinkID = $(this).attr('data-link-id');
    if(deleteTeacherSubjectLinkID !== null && deleteTeacherSubjectLinkID !== ''){
        Swal({
            title: messages.areYouSureQuestion,
            text: messages.youWontAbleToRevertThis,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, unassign it!',
            showCloseButton: true
          }).then((result) => {
            if (result.value) {
              unassignTeacher();
            }
          })
    }else{
        swal(
            messages.error,
            messages.invalidTeacher,
            'error'
        )
    }
});


function unassignTeacher() {
    $.ajax({
        url: herokuUrl+'/deleteSubjectTeacher',
        method: 'POST',
        headers,
        data: {
            teacherSubjectLinkID: deleteTeacherSubjectLinkID
        },
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.teacherSuccessfullyUnassigned,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                if(tableData){
                    tableData.destroy();
                }
                getSubjects();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('unassignTeacher')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
                swal(
                    xhr.responseJSON.userMessage,
                    messages.deletedMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                swal(
                    xhr.responseJSON.userMessage,
                    messages.error,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }

            swal(
                xhr.responseJSON.response.userMessage,
                messages.error,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}


$(document).on('click', '.assign-teacher', function(){
    subjectID = $(this).attr('data-subject-id');
    teacherSubjectLinkID = $(this).attr('data-subject-link-id');

    const toast = swal.mixin({
        toast: true,
        position: 'center-end',
        showConfirmButton: false,
        timer: 3000,
    });

    console.log($('#filter-class-teachers > option').length)
    if($('#filter-class-teachers > option').length < 2){
        toast({
            type: 'error',
            title: messages.noTeacherAvailableForAssignment
        })
        return false;
    }

    if(subjectID == ''){
        toast({
            type: 'error',
            title: messages.invalidSubject
        })
        return false;
    }

    // if(classID == ''){
    //     toast({
    //         type: 'error',
    //         title: 'Please select Section'
    //     })
    //     return false;
    // }

    let inputOptionsTeachers = {};
    let inputOptionsClasses = {};

    $("#filter-class-teachers > option").each(function(index) {
        if(this.value !== ''){
            inputOptionsTeachers[index.toString() + '-' +this.value.toString()] = this.text;
        }
    });

    $("#filter-classes > option").each(function() {
        if(this.value !== ''){
            inputOptionsClasses[this.value.toString()] = this.text;
        }
    });

    swal.mixin({
        input: 'select',
        confirmButtonText: 'Next &rarr;',
        showCloseButton: true,
        progressSteps: ['1', '2', '3']
        }).queue([
        {
            title: messages.selectSection,
            inputOptions: inputOptionsClasses,
        },
        {
            title: messages.selectTeacher,
            inputOptions: inputOptionsTeachers,
        },
        {
            title: messages.classTeacher,
            inputOptions: {
                '0' : messages.no,
                '1' : messages.yes,
            },
        }
        ]).then((result) => {

        if (result.value) {
            classID = result.value[0];
            classTeacherID = (result.value[1]).split('-');
            classTeacherID = classTeacherID[classTeacherID.length - 1];
            isClassTeacherFlag = result.value[2];
            assignTeacher();
        }
    });
});

function assignTeacher() {

    let data = {};
    if(teacherSubjectLinkID !== null && teacherSubjectLinkID !== 'null'){
        data.teacherSubjectLinkID = teacherSubjectLinkID;
    }

    data.userID = classTeacherID;
    data.subjectID =  subjectID;
    data.classID = classID;
    data.isClassTeacherFlag = isClassTeacherFlag;

    $.ajax({
        url: herokuUrl+'/assignTeacher',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.successfullyAssigned,
                    messages.successMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                if(tableData){
                    tableData.destroy();
                }
                getSubjects();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('assignTeacher')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }

            swal(
                xhr.responseJSON.response.userMessage,
                messages.errorMessage,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$('#subjects-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#subjects-form').serialize();
    showProgressBar(submitLoader);
    resetMessages();
    submitSubject();
});
