var overallResultForm = $('#overall-result-form');
var termStatus = 0;
var overallList = [];
var isPublishedStatus = 0;
var totalSubjectResults = 0;
var totalStudents = 0;
$(document).ready(function(){
    getAjaxSchools();
    $('#toggle-demo').bootstrapToggle()


  $(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'Enabled',
      off: 'Disabled'
    });
  })

});

students.on('change', function () {
    if(studentID != ''){
        data.studentID = studentID;  
        getOverallResult();
    }
});
$('#show-add-form-container').on('click', function () {
    if(classID == ''){
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Please select Section first!',
          })
          return false;
    }
    if(examTermID === ''){
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Please select Exam term first!',
          })
          return false;
    }
    getOverallResult();
    hideAndShowTable();
    setTimeout(function(){
        iterateStudentListTableForOverall();
    },500)
    
});

$(document).on('change','#filter-exam-term',function(){
    hideAndShowOverAllListTable();
    if(classID == ''){
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Please select Section first!',
          });
          return false;
    }
    if(examTermID !== ''){
        hideAndShowOverAllListTable();
        hideAndShowTable();
    }
    isPublishedStatus = $('option:selected', this).attr('data-isPublished');
    termStatus  =  isPublishedStatus == 0 ? 'Published' : 'UnPublished';
    publishClass = isPublishedStatus == 0 ? 'btn btn-primary btn-round' : 'btn btn-danger btn-round';
    publishToggle(termStatus, publishClass);
   
    if(examTermID !== ''){
        
        getOverallPercentageData();
        setTimeout(function(){
            updateOverallResult();
            iterateStudentListTableForOverall();
        },1000); 
    }else{
        $('#publish-button').hide(1000);
    }
});

function publishToggle(termStatus, publishClass){
    $('#publish-button').html(termStatus);
    $('#publish-button').removeAttr('class');
    $('#publish-button').attr('class',publishClass);
}
$('#publish-button').on('click',function(){
    isPublishedStatus = $('#filter-exam-term :selected').attr('data-isPublished');
    if(examTermID === ''){
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Please select Exam term first!',
          })
          return false;
    }else{
        $.confirm({
            title: 'Confirm!',
            content: `Are you sure! <br/> You want to ${isPublishedStatus == 0 ? 'Published' : 'UnPublished'} the result,after ${isPublishedStatus == 0 ? 'Published' : 'UnPublished'}, It will be ${isPublishedStatus == 0 ? 'visible' : 'remove'} to parents on their app.`,
            type: 'red',
            typeAnimated: true,
            buttons: {
                formSubmit: {
                    text: 'Confirm',
                    btnClass: 'btn-blue',
                    action: function () {
                        termStatusToggle();
                    }
                },
                cancel: function () {
                },
            }
        });
        
    }
});

function termStatusToggle(){
    $.ajax({
        url: herokuUrl + '/setExamTermStatus',
        method: 'POST',
        type:'json',
        headers,
        beforeSend: function () {
        },
        data: {
            examsTermID: examTermID,
            isPublished: isPublishedStatus == 1  ? 0 : 1 
        },
        success: function (res){
            if(res.response === 'success'){
                termStatus  =  isPublishedStatus == 0 ? 'UnPublished' : ' Published';
                publishClass = isPublishedStatus == 0 ? 'btn btn-danger btn-round'  : 'btn btn-primary btn-round';
                $('#filter-exam-term :selected').attr('data-isPublished',  isPublishedStatus == 0 ? 1 : 0);
                publishToggle(termStatus, publishClass)
            }
        },
        error: function(err){
            invokedFunctionArray.push('termStatusToggle')
            window.requestForToken(err)
            console.log(err);
        }
    });
}

function hideAndShowTable(){
    $('#student-overall-result-table tbody').empty();
    $('#student-overall-result-table tbody').append(`
    <tr>
    <td colspan="10"><strong style="color:#188ae2;font-weight:bold"> Loading ...</<strong></td>
    </tr>
    `);
}


function hideAndShowOverAllListTable(){
    var assessmentOnlyFlag = $('#filter-classLevels').find(':selected').attr('data-assesstment');
    if(Number(assessmentOnlyFlag) > 0 ){
        $('#overall-div').hide();
        $('#assesment-div').show();
         $('#overall-result-table-assessment tbody').empty();
          $('#overall-result-table-assessment tbody').append(`
            <tr>
                <td colspan="10"><strong style="color:#188ae2;font-weight:bold"> Loading...</<strong></td>
            </tr>
        `);
    }else{
        $('#overall-div').show();
        $('#assesment-div').hide();
        $('#overall-result-table tbody').empty();
        $('#overall-result-table tbody').append(`
        <tr>
            <td colspan="9"><strong style="color:#188ae2;font-weight:bold"> Loading ...</<strong></td>
        </tr>
    `); 
    }       
}

function updateOverallResult(){
    let wouldUpdateArray = [];
    data.examsTermID = examTermID;
    data.classID = classID;
    $.ajax({
        url: herokuUrl + '/getExamOverallResult',
        method: 'POST',
        type:'json',
        headers,
        data: data,
        beforeSend(){
            overallList = [];
        },
        success: function (res){
            response = parseResponse(res);
            for (let i = 0; i < response.length; i++){
                const element = response[i];
                overallList.push(element);
                var myObject = {};
                let latestRecord ;
                if(__OVERALLRESULT__){
                    latestRecord = __OVERALLRESULT__.find(o => o.studentID === element.studentID);
                }
                if(latestRecord){
                   myObject['examOverallID'] = element.examOverallID;
                   myObject['classID'] = classID;
                   myObject['examsTermID'] = element.examsTermID;
                   myObject['overallPercentage'] = latestRecord.percentAge ? latestRecord.percentAge : 0;
                   if(element.rank != '' && element.rank != null && element.rank != 'undefined'){
                        myObject['rank'] = element.rank;
                   }
                   if(element.overallNotes != '' && element.overallNotes != null && element.overallNotes != 'undefined'){
                        myObject['overallNotes'] = element.overallNotes;
                    }
                   myObject['studentID'] = element.studentID;
                   
                   wouldUpdateArray.push(myObject);
                }
            }
            // if(wouldUpdateArray.length){
            //     $.ajax({
            //         url: herokuUrl + '/setExamTermResultsArray',
            //         method: 'POST',
            //         headers,
            //         data: {
            //             array: wouldUpdateArray
            //         },
            //         beforeSend: function () {
            //         },
            //         success: function (res) {
            //         },
            //         error: function (xhr) {
            //             invokedFunctionArray.push('updateOverallResult')
            //             window.requestForToken(xhr)
            //             console.log(xhr);
            //         }
            //     });
            // }
            getOverallResult();
        },
        error: function(err){
            invokedFunctionArray.push('updateOverallResult')
            window.requestForToken(err)
            console.log(err);
        }
    });
}
// Over All Result
function getOverallResult() {

    var assessmentOnlyFlag = $('#filter-classLevels').find(':selected').attr('data-assesstment');
    if(Number(assessmentOnlyFlag) > 0 ){
        $('#overall-result-table-assessment tbody').empty();
        data.examsTermID = examTermID;
        data.classID = classID;
    if (overAllExamTermAssessmentTableData) {
        overAllExamTermAssessmentTableData.destroy();
    }
    overAllExamTermAssessmentTableData = $('#overall-result-table-assessment').DataTable({
        'ajax':{
            url: herokuUrl + '/getExamOverallResult',
            method: 'POST',
            headers,
            data: data,
            beforeSend(){
                overallList = [];
            },
            dataSrc: function (res) {
                response = parseResponse(res);
                var return_data = new Array();
                totalSubjectResults = response.length;
                // if(user.isAdmin == 1 && Number(totalSubjectResults) > 0 &&  Number(totalStudents) > 0 &&    Number(totalSubjectResults) === Number(totalStudents)){  
                //     $('#publish-button').show(1000);
                // }
                for (let i = 0; i < response.length; i++) {
                    let studentPercentageData;
                    let overallFlag =  `<label class="label label-sm overall-flag label-danger"> InCompleted </label>`;
                    var stausFlag =  ``;
                    const element = response[i];
                    let resultStatus = element.resultStatus && element.resultStatus !== null ? `<label class="label label-sm result-flag ${element.resultStatus === 'P' ? 'label-success' : 'label-danger'}"> ${element.resultStatus === 'P' ? 'Pass' : 'Fail'} </label>` : ''
                    overallList.push(element);
                    if(typeof element.holdFlag != 'undefined' && element.holdFlag == 1 ){
                        stausFlag =  `<label class="label label-sm label-danger">Hold</label>`;
                    }else{
                        stausFlag =  `<label class="label label-sm label-success">Release</label>`;
                    }
                    if(__OVERALLRESULT__){
                        studentPercentageData = __OVERALLRESULT__.find(o => o.studentID === element.studentID);
                    }
                    if(studentPercentageData){
                        overallFlag =  `<label class="label label-sm overall-flag ${Number(studentPercentageData.totalSubjects) <= Number(studentPercentageData.completedResults) ? 'label-success' : 'label-danger'}">  ${Number(studentPercentageData.totalSubjects) <= Number(studentPercentageData.completedResults) ? 'Completed' : 'InCompleted'}  </label>`;
                        studentOverallPercentage = studentPercentageData.percentAge;
                    }
                    
                    return_data.push({
                        'sNo': i + 1,
                        'studentName': element.studentName,
                        'overallNotes': element.overallNotes,
                        'overallGrade': element.overallGrade,
                        'resultStatus': resultStatus+stausFlag,
                        'resultsFlag': overallFlag,
                        'action': `<div class="btn-group">
                            <a class="btn btn-sm btn-success printExamsResult" data-toggle="tooltip" title="Print" data-student-id="${element.studentID}" data-check-isAssessment="1" data-examterm-id="${element.examsTermID}"><i class="fa fa-print"></i></a>
                        </div>`
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getOverallResult')
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
                if(xhr.status === 404){
                    $('.dataTables_empty').text('No data available');
                 }
            }
        },
        "columns": [{
                'data': 'sNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'overallNotes'
            },
            {
                'data': 'overallGrade'
            },
            {
                'data': 'resultStatus'
            },
            {
                'data': 'resultsFlag'
            },
            {
                'data': 'action'
            },
        ]
    });
    
    }else{
            data.examsTermID = examTermID;
            data.classID = classID;
            if (overAllExamTermTableData) {
                overAllExamTermTableData.destroy();
            }
            overAllExamTermTableData = $('#overall-result-table').DataTable({
                'ajax':{
                    url: herokuUrl + '/getExamOverallResult',
                    method: 'POST',
                    headers,
                    data: data,
                    beforeSend(){
                        overallList = [];
                    },
                    dataSrc: function (res) {
                        response = parseResponse(res);
                        var return_data = new Array();
                        totalSubjectResults = response.length;
                        // if(user.isAdmin == 1 && Number(totalSubjectResults) > 0 &&  Number(totalStudents) > 0 &&    Number(totalSubjectResults) === Number(totalStudents)){  
                        //     $('#publish-button').show(1000);
                        // }
                        for (let i = 0; i < response.length; i++) {
                            let studentPercentageData;
                            let overallFlag =  `<label class="label label-sm overall-flag label-danger"> InCompleted </label>`;
                            var stausFlag =  ``;
                            const element = response[i];
                            let resultStatus = element.resultStatus && element.resultStatus !== null ? `<label class="label label-sm result-flag ${element.resultStatus === 'P' ? 'label-success' : 'label-danger'}"> ${element.resultStatus === 'P' ? 'Pass' : 'Fail'} </label>` : ''
                            overallList.push(element);
                            if(typeof element.holdFlag != 'undefined' && element.holdFlag == 1 ){
                                stausFlag =  `<label class="label label-sm label-danger">Hold</label>`;
                            }else if(typeof element.holdFlag != 'undefined' && element.holdFlag == 0 ){
                                stausFlag =  `<label class="label label-sm label-success">Release</label>`;
                            }
                            if(__OVERALLRESULT__){
                                studentPercentageData = __OVERALLRESULT__.find(o => o.studentID === element.studentID);
                            }
                            if(studentPercentageData){
                                var remainingResult = Number(studentPercentageData.totalSubjects) - Number(studentPercentageData.completedResults);
                                overallFlag =  `<label class="label label-sm overall-flag ${Number(studentPercentageData.totalSubjects) <= Number(studentPercentageData.completedResults) ? 'label-success' : 'label-danger'}">  ${Number(studentPercentageData.totalSubjects) <= Number(studentPercentageData.completedResults) ? 'Completed' :    remainingResult+' Subject Missing' }  </label>`;
                                studentOverallPercentage = studentPercentageData.percentAge;
                            }
                            return_data.push({
                                'sNo': i + 1,
                                'studentName': element.studentName,
                                'overallPercentage': element.overallPercentage && element.overallPercentage !== '' ? element.overallPercentage+ ' %' : '',
                                'rank': element.rank,
                                'overallGrade': element.overallGrade,
                                'overallNotes': `<span data-toggle="tooltip" title="${element.overallNotes}">`+(element.overallNotes ? element.overallNotes.substring(0, 10) : '--')+`</span>`,
                                'Detail': `
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-success printExamsResult" data-toggle="tooltip" title="Print" data-student-id="${element.studentID}" data-check-isAssessment="0" data-examterm-id="${element.examsTermID}"><i class="fa fa-print"></i></a>
                                   <button class='btn btn-default btn-xs subject-nonsubject-detail' data-response=' ${ handleApostropheWork(element)}' >View Detail</button>
                               </div>`,
                                'status': resultStatus+stausFlag,
                                'resultsFlag': overallFlag,
                            })
                        }
                        return return_data;
                    },
                    error: function (xhr) {
                        invokedFunctionArray.push('getOverallResult')
                        window.requestForToken(xhr)
                        parseValidationErrors(xhr);
                        if(xhr.status === 404){
                            $('.dataTables_empty').text('No data available');
                         }
                    }
                },
                "columns": [{
                        'data': 'sNo'
                    },
                    {
                        'data': 'studentName'
                    },
                    {
                        'data': 'overallPercentage'
                    },
                    {
                        'data': 'rank'
                    },
                    {
                        'data': 'overallGrade'
                    },
                    {
                        'data': 'overallNotes'
                    },
                    {
                        'data':'Detail'
                    },
                    {
                        'data': 'status'
                    },
                    {
                        'data': 'resultsFlag'
                    },
                ]
            });
    }
    
}

$(document).on('click','.subject-nonsubject-detail',function(){
    $.LoadingOverlay("show");
    var subjectTable = $('#subject-detail-table tbody');
    var nonSubjectTable = $('#non-subject-detail-table tbody');
    var response = JSON.parse($(this).attr('data-response'));
    $('.update-overall-exam-result').attr('data-json',JSON.stringify(response));
    let stausFlag = '';
    studentID = response.studentID
    if(typeof response.holdFlag != 'undefined' && response.holdFlag == 1 ){
        stausFlag =  `<label class="label label-sm label-danger">Hold</label>`;
    }else if(typeof response.holdFlag != 'undefined' && response.holdFlag == 0 ){
        stausFlag =  `<label class="label label-sm label-success">Release</label>`;
    }else{
        stausFlag = ' - '
    }
    let resultStatus = `<label class="label label-sm result-flag ${response.resultStatus === 'P' ? 'label-success' : 'label-danger'}"> ${response.resultStatus === 'P' ? 'Pass' : 'Fail'} </label>`
    $('#percentage').text(response.overallPercentage !== null ? response.overallPercentage+' %' : '') ;
    $('#rankField').val(response.rank);
    $('#passStatus').html(`
    <div class="flex-container">
        <div class="round">
            <input type="checkbox" name="resultStatus" data-index="attendance-check-pf" data-target="P" class="mark-attendance-modal mark-checkbox-p attendance-check-pf" data-isPresent="P" id="checkbox-present" ${response.resultStatus == 'P' ? 'checked' : ''}/>
            <label for="checkbox-present" data-toggle="tooltip" title="Pass"><b class="check-text">P</b></label>
        </div>
        <div class="round round-d">
            <input type="checkbox" name="resultStatus" data-target="A"  data-index="attendance-check-pf" data-isPresent="F" class="mark-attendance-modal mark-checkbox-a attendance-check-pf" id="checkbox-absent" ${response.resultStatus == 'F' ? 'checked' : ''} />
            <label for="checkbox-absent" data-toggle="tooltip" title="Fail" ><b class="check-text">F</b></label>
        </div>
    </div>
    `);
    $('#holdStatus').html(`
        <div class="flex-container">
            <div class="round">
                <input type="checkbox" name="holdReleaseStatus" data-isHoldRelease="0" data-target="R" class="mark-hold-release-modal attendance-check-hr" data-index="attendance-check-hr" id="checkbox-release" ${typeof response.holdFlag != 'undefined' &&( response.holdFlag == 0 || response.holdFlag == null ||  response.holdFlag == 'undefined' || response.holdFlag == ''  ) ? 'checked' : ''}/>
                <label for="checkbox-release" data-toggle="tooltip" title="Publish the Result"><b class="check-text">R</b></label>
            </div>
            <div class="round round-d">
                <input type="checkbox" name="holdReleaseStatus" data-isHoldRelease="1" data-index="attendance-check-hr" data-target="H" class="mark-hold-release-modal attendance-check-hr"  id="checkbox-hold" ${typeof response.holdFlag != 'undefined' && response.holdFlag == 1 ? 'checked' : '' } />
                <label for="checkbox-hold" data-toggle="tooltip" title="Hold the Result"><b class="check-text">H</b></label>
            </div>
        </div>`);
    $('#overallNotesField').val(response.overallNotes);
    $('#student-name').text(response.studentName);
    $('#gradeField').val(response.overallGrade);
    $.ajax({
        url: herokuUrl+'/getExamSubjectResult',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data: {
            subjectID: subjectID,
            examsTermID: examTermID,
            classID: classID,
            studentID: studentID
        },
        success: function(res){
            response = parseResponse(res);
            subjectTable.empty();
            if (response.length > 0) {
                response.forEach((val, index) => {
                    subjectTable.append(`
                    <tr>
                        <td  class="p-2"> ${val.subjectName}</td>
                        <td class="p-2"> ${val.totalMarks}</td>
                        <td  class="p-2"> ${val.obtainedMarks}</td>
                        <td  class="p-2"> ${typeof val.grade !== 'undefined' && val.grade !== 'null' && val.grade !== null ? val.grade : ' - '}</td>
                        <td  class="p-2"> ${typeof val.notes !== 'undefined' && val.notes !== 'null' && val.notes !== null ? val.notes : ' - '}</td>

                    </tr>`);
                })
            } else {
                subjectTable.append('<tr><td colspan="5" style="text-align:center !important">Record Not Found</td></tr>');
            }
        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        }
    });

    $.ajax({
        url: herokuUrl+'/getExamNSResult',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data: {
            examsTermID: examTermID,
            classID: classID,
            nonSubjectID: nonSubjectID,
            studentID: studentID
        },
        success: function(res){
            response = parseResponse(res);
            nonSubjectTable.empty();
            if (response.length > 0) {
                response.forEach((val, index) => {
                    nonSubjectTable.append(`
                    <tr>
                        <td  class="p-2"> ${val.nonSubjectName}</td>
                        <td class="p-2"> ${val.gradeNS}</td>
                        <td  class="p-2"> ${val.notesNS}</td>
                        <td  class="p-2"> ${typeof val.legendValue !== 'undefined' && val.legendValue !== 'null' && val.legendValue !== null ? val.legendValue : ' - '}</td>
                        <td></td>
                    </tr>`);
                })
            } else {
                nonSubjectTable.append('<tr><td colspan="5" style="text-align:center !important">Record Not Found</td></tr>');
            }
        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        }
    });
    $.LoadingOverlay("hide");
    $('#exampleModal').modal('toggle');
    $('#exampleModal').modal('show');
})

$(document).on('click','.update-overall-exam-result',function(e){
    e.preventDefault();
    var bulkArray = [];
    var result = JSON.parse($(this).attr('data-json'));
    var rank = $('#rankField').val();
    var overallGrade = $('#gradeField').val();
    var overallNotesField = $('#overallNotesField').val();
    var passFailStatus =  $('input[name=resultStatus]:checked').attr('data-isPresent');
    var holdReleaseStatus =  $('input[name=holdReleaseStatus]:checked').attr('data-isHoldRelease');
    if(rank !== ''){
        result.rank = rank;
    }
    if(overallGrade !== ''){
        result.overallGrade = overallGrade;
    }
    if(overallNotesField !== ''){
        result.overallNotes = overallNotesField;
    }
    if(passFailStatus !== ''){
        result.resultStatus = passFailStatus;
    }
     if(holdReleaseStatus !== ''){
        result.holdFlag = holdReleaseStatus;
    }
    
    delete result['termName']
    delete result['insertionDate']
    delete result['studentName']
    bulkArray.push(result);
    if(bulkArray.length > 0){
        submitOverallResult(bulkArray);
    }
});
function getOverallPercentageData(){
    $.ajax({
        url: herokuUrl + '/getStudentOverallResults',
        method: 'POST',
        type:'json',
        headers,
        beforeSend: function () {
            __OVERALLRESULT__ = [];
        },
        data: {
            classID: classID,
            examsTermID: examTermID
        },
        success: function (res){
            response = parseResponse(res);
            totalStudents =  response.length;
            for (let i = 0; i < response.length; i++) {
                const element = response[i];
                __OVERALLRESULT__.push(element);
            }
        },
        error: function(err){
            invokedFunctionArray.push('getOverallPercentageData')
            window.requestForToken(err)
            console.log(err);
        }
    });
}
function iterateStudentListTableForOverall(){
    var overallAddTable = $('#student-overall-result-table tbody');
    var overallAddTableHead = $('#student-overall-result-table thead');
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {
            showProgressBar('p2-students-result-list');
        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                overallAddTable.empty();
                overallAddTableHead.empty();
                if(res.data.length){
                    var assessmentOnlyFlag = $('#filter-classLevels').find(':selected').attr('data-assesstment');
                    if(Number(assessmentOnlyFlag > 0 )){
                        overallAddTableHead.html(`
                        <tr>
                            <th>S.No</th>
                            <th>Roll No</th>
                            <th>Student</th>
                            <th class="text-center" width="50%">Assessment Notes</th>
                            <th>Grade</th>
                            <th class="" width="10%" >Results Status</th>
                            <th width="15%">Pass <i data-target="P" class="fa fa-check-square-o passAllIcon"></a></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Pass All</div>
                                    <input  id="pass-all-selected-students" data-id="select-student-result-pass" class="hidden studentCheckBoxAllPass" type="checkbox">
                                    <label for="pass-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                         | Fail <i data-target="F" class="fa fa-check-square-o failAllIcon"></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Fail All</div>
                                    <input  id="fail-all-selected-students" data-id="select-student-result-fail" class="hidden studentCheckBoxAllFail" type="checkbox">
                                    <label for="fail-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                            </th>
                            <th width="20%" >Release <i data-target="0" class="fa fa-check-square-o releaseAllIcon"></a></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Release All</div>
                                    <input  id="release-all-selected-students" data-id="select-student-result-release" class="hidden studentCheckBoxAllRelease" type="checkbox">
                                    <label for="release-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                            | Hold <i data-target="1" class="fa fa-check-square-o holdAllIcon"></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Hold All</div>
                                    <input  id="hold-all-selected-students" data-id="select-student-result-hold" class="hidden studentCheckBoxAllHold" type="checkbox">
                                    <label for="hold-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                            </th>
                        </tr>
                        `);
                        
                        $.each(res.data, function(index, value){
                            let overallFlag = '';
                            rollNumber = value.rollNo.substring(0, 8)
                            let addedResult,studentOverallPercentageData, studentOverallPercentage = 0;
                            if(overallList){
                                addedResult = overallList.find(o => o.studentID === value.studentID);
                            }
                            if(__OVERALLRESULT__){
                                studentOverallPercentageData = __OVERALLRESULT__.find(o => o.studentID === value.studentID);  
                            }
                           
                            if(studentOverallPercentageData){
                                overallFlag =  `<label class="label label-sm overall-flag ${Number(studentOverallPercentageData.totalSubjects) <= Number(studentOverallPercentageData.completedResults) ? 'label-success' : 'label-danger'}" data-toggle="tooltip" title="Missing Result for ${Number(studentOverallPercentageData.totalSubjects) - Number(studentOverallPercentageData.completedResults)} Subject(s)">  ${Number(studentOverallPercentageData.totalSubjects) <= Number(studentOverallPercentageData.completedResults) ? 'Completed' : 'InCompleted'}  </label>`;
                            }
                           
                            if(addedResult !== undefined && addedResult !== ''){
                                overallAddTable.append(`
                                <tr>
                                <td style="display:none;">
                                    <input class="form-control" name="examOverallID" type="text" value='${ addedResult.examOverallID }'>
                                </td>
                                <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                                <td><span data-toggle="tooltip" title="${value.rollNo}">
                                ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                                </span></td>
                                <td style="display:none;">
                                    <input class="form-control" name="studentID" type="text" value='${ value.studentID }'>
                                </td>
                                <td>${value.studentName}</td>
                                <td width="50%">
                                    <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="overallNotes" type="text">${addedResult.overallNotes != '' && addedResult.overallNotes != null && addedResult.overallNotes != 'undefined' ? addedResult.overallNotes : '' }</textarea>
                                </td>
                                <td>
                                    <input maxlength="5" class="form-control" name="overallGrade" type="text" value="${addedResult.overallGrade != '' && addedResult.overallGrade != null && addedResult.overallGrade != 'undefined' ? addedResult.overallGrade : ''}">
                                </td>
                                <td>
                                    ${overallFlag}
                                </td>
                                <td>
                                <div class="flex-container">
                                    <div class="round">
                                        <input type="hidden" name="resultStatus" id="result-status" class="all-values result-status-${index}" value="${addedResult.resultStatus && addedResult.resultStatus != null && addedResult.resultStatus != 'undefined' && addedResult.resultStatus != '' ? (addedResult.resultStatus == 'P' ? 'P' : 'F') : ''}">

                                        <input type="checkbox" data-result-id="result-status-${index}" data-index="attendance-check-${index}" data-target="P" class="mark-attendance mark-attendance-pass mark-checkbox-p attendance-check-${index}" id="checkbox-p-${index}" ${addedResult.resultStatus == 'P' ? 'checked' : ''}/>
                                        <label for="checkbox-p-${index}" data-toggle="tooltip" title="Pass"><b class="check-text">P</b></label>
                                    </div>
                                    <div class="round round-d">
                                        <input type="checkbox" data-result-id="result-status-${index}" data-index="attendance-check-${index}" data-target="F" class="mark-attendance mark-attendance-fail mark-checkbox-a attendance-check-${index}"  id="checkbox-a-${index}" ${addedResult.resultStatus == 'F' ? 'checked' : ''} />
                                        <label for="checkbox-a-${index}" data-toggle="tooltip" title="Fail" ><b class="check-text">F</b></label>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <div class="flex-container" >
                                    <div class="round">
                                        <input id="hold-flag-${index}" type="hidden" name="holdFlag" id="hold-flag" class="all-hold-flags hold-flag-${index}" value="${addedResult.holdFlag && addedResult.holdFlag != null && addedResult.holdFlag != 'undefined' && addedResult.holdFlag != '' ? (addedResult.holdFlag == 1 ? 1 : 0) : 0}">

                                        <input type="checkbox" data-hold-flag-id="hold-flag-${index}"  data-index="release-hold-check-${index}" data-target="0" class="mark-release-all mark-hold-release mark-checkbox-r release-hold-check-${index}" id="checkbox-r-${index}" ${typeof addedResult.holdFlag != 'undefined' &&( addedResult.holdFlag == 0 || addedResult.holdFlag == null ||  addedResult.holdFlag == 'undefined' || addedResult.holdFlag == ''  ) ? 'checked' : ''}/>
                                        <label for="checkbox-r-${index}" data-toggle="tooltip" title="Publish the Result"><b class="check-text">R</b></label>
                                    </div>
                                    <div class="round round-d">
                                        <input type="checkbox" data-hold-flag-id="hold-flag-${index}"  data-index="release-hold-check-${index}" data-target="1" class="mark-hold-all mark-hold-release mark-checkbox-h release-hold-check-${index}"  id="checkbox-h-${index}" ${typeof addedResult.holdFlag != 'undefined' && addedResult.holdFlag == 1 ? 'checked' : '' } />
                                        <label for="checkbox-h-${index}" data-toggle="tooltip" title="Hold the Result"><b class="check-text">H</b></label>
                                    </div>
                                </div>
                                </td>
                                </tr>`);
                                hideProgressBar('p2-students-result-list');
                            }else{
                                overallAddTable.append(`
                                <tr>
                                    <td style="display:none;">
                                        <input class="form-control" name="examOverallID" type="text" value='0'>
                                    </td>
                                    <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                                    <td><span data-toggle="tooltip" title="${value.rollNo}">
                                    ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                                    </span></td>
                                    <td style="display:none;">
                                        <input class="form-control" name="studentID" type="text" value=' ${value.studentID}'>
                                    </td>
                                    <td>${value.studentName}</td>
                                    <td width="50%">
                                        <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="overallNotes" type="text"></textarea>
                                    </td>
                                    <td>
                                        <input maxlength="5" class="form-control" name="overallGrade" type="text">
                                    </td>
                                    <td>
                                        ${overallFlag}
                                    </td>
                                    <td>
                                    <div class="flex-container">
                                        <div class="round">
                                            <input type="hidden" name="resultStatus" id="result-status" class="all-values result-status-${index}" value="">

                                            <input type="checkbox" data-index="attendance-check-${index}" data-target="P" data-result-id="result-status-${index}" class="mark-attendance-pass mark-attendance mark-checkbox-p attendance-check-${index}" data-json='' id="checkbox-p-${index}"/>
                                            <label for="checkbox-p-${index}" data-toggle="tooltip" title="Pass"><b class="check-text">P</b></label>
                                        </div>
                                        <div class="round round-d">
                                            <input type="checkbox" data-result-id="result-status-${index}" data-index="attendance-check-${index}" data-target="F" class="mark-attendance-fail mark-attendance mark-checkbox-a attendance-check-${index}" data-json='' id="checkbox-a-${index}" />
                                            <label for="checkbox-a-${index}" data-toggle="tooltip" title="Fail"><b class="check-text">F</b></label>
                                        </div>
                                    </div>
                                    </td>
                                    <td>
                                        <div class="flex-container">
                                            <div class="round">
                                                <input id="hold-flag-${index}" type="hidden" name="holdFlag" class="all-values hold-flag-${index}" value="1">

                                                <input type="checkbox" data-hold-flag-id="hold-flag-${index}" data-index="release-hold-check-${index}" data-target="0" class="mark-release-all mark-hold-release mark-checkbox-r release-hold-check-${index}" id="checkbox-r-${index}"/>
                                                <label for="checkbox-r-${index}" data-toggle="tooltip" title="Publish the Result"><b class="check-text">R</b></label>
                                            </div>
                                            <div class="round round-d">
                                                <input type="checkbox" data-hold-flag-id="hold-flag-${index}"  data-index="release-hold-check-${index}" data-target="1" class="mark-hold-all mark-hold-release mark-checkbox-h release-hold-check-${index}"  id="checkbox-h-${index}"/>
                                                <label for="checkbox-h-${index}" data-toggle="tooltip" title="Hold the Result"><b class="check-text">H</b></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>`);
                                hideProgressBar('p2-students-result-list');
                            }
                        });
                    }else{
                        overallAddTableHead.html(`
                        <tr>
                            <th>S.No</th>
                            <th>Roll No</th>
                            <th>Student</th>
                            <th class="" width="10%">Percentage</th>
                            <th width="10%">Rank</th>
                            <th width="10%">Grade</th>
                            <th class="text-center" width="20%">Notes</th>
                            <th class="" width="10%" >Results Status</th>
                            <th width="15%">Pass <i data-target="P" class="fa fa-check-square-o passAllIcon"></a></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Pass All</div>
                                    <input  id="pass-all-selected-students" data-id="select-student-result-pass" class="hidden studentCheckBoxAllPass" type="checkbox">
                                    <label for="pass-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                         | Fail <i data-target="F" class="fa fa-check-square-o failAllIcon"></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Fail All</div>
                                    <input  id="fail-all-selected-students" data-id="select-student-result-fail" class="hidden studentCheckBoxAllFail" type="checkbox">
                                    <label for="fail-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                            </th>
                            <th width="15%" >Release <i data-target="0" class="fa fa-check-square-o releaseAllIcon"></a></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Release All</div>
                                    <input  id="release-all-selected-students" data-id="select-student-result-release" class="hidden studentCheckBoxAllRelease" type="checkbox">
                                    <label for="release-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                            | Hold <i data-target="1" class="fa fa-check-square-o holdAllIcon"></i>
                                <div class="hidden checkbox checkbox-aqua d-flex">
                                <div style="margin-right: 15px;">Hold All</div>
                                    <input  id="hold-all-selected-students" data-id="select-student-result-hold" class="hidden studentCheckBoxAllHold" type="checkbox">
                                    <label for="hold-all-selected-students" style="margin-top: 3px; margin-left:11px;">
                                    </label>
                                </div>
                            </th>
                        </tr>
                        

                        
                        `);
                        $.each(res.data, function(index, value){
                            let overallFlag = '';
                            let addedResult,studentOverallPercentageData, studentOverallPercentage = 0;
                            rollNumber = value.rollNo.substring(0, 8)
                            // let overallFlag =  `<label class="label label-sm overall-flag label-danger"> InCompleted </label>`;
                            if(overallList){
                                addedResult = overallList.find(o => o.studentID === value.studentID);
                            }
                            if(__OVERALLRESULT__){
                                studentOverallPercentageData = __OVERALLRESULT__.find(o => o.studentID === value.studentID);  
                            }
                            if(studentOverallPercentageData){
                                overallFlag =  `<label class="label label-sm overall-flag ${Number(studentOverallPercentageData.totalSubjects) <= Number(studentOverallPercentageData.completedResults) ? 'label-success' : 'label-danger'}" data-toggle="tooltip" title="Missing Result for ${Number(studentOverallPercentageData.totalSubjects) - Number(studentOverallPercentageData.completedResults)} Subject(s)">  ${Number(studentOverallPercentageData.totalSubjects) <= Number(studentOverallPercentageData.completedResults) ? 'Completed' : 'InCompleted'}  </label>`;
                            }
                           
                            if(addedResult !== undefined && addedResult !== ''){
                                overallAddTable.append(`
                                <tr>
                                <td style="display:none;">
                                    <input class="form-control" name="examOverallID" type="text" value=' ${ addedResult.examOverallID } '>
                                </td>
                                <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                                <td><span data-toggle="tooltip" title="${value.rollNo}">
                                ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                                </span></td>
                                <td style="display:none;">
                                    <input class="form-control" name="studentID" type="text" value=' ${ addedResult.studentID } '>
                                </td>
                                <td>${addedResult.studentName }</td>
                                <td style="text-align: center;">
                                <label>${studentOverallPercentageData && studentOverallPercentageData.percentAge != undefined  &&  studentOverallPercentageData.percentAge  ? studentOverallPercentageData.percentAge : addedResult.overallPercentage }</label>
                                </td>
                                <td>
                                <input maxlength="5" class="form-control" name="rank" type="text" value="${addedResult.rank != '' && addedResult.rank != null && addedResult.rank != 'undefined' ? addedResult.rank : ''}">
                                </td>
                                <td>
                                <input maxlength="5" class="form-control" name="overallGrade" type="text" value="${addedResult.overallGrade != '' && addedResult.overallGrade != null && addedResult.overallGrade != 'undefined' ? addedResult.overallGrade : ''}">
                                </td>
                                <td>
                                    <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="overallNotes" type="text">${addedResult.overallNotes != '' && addedResult.overallNotes != null && addedResult.overallNotes != 'undefined' ? addedResult.overallNotes : '' }</textarea>
                                </td>
                                <td>
                                    ${overallFlag}
                                </td>
                                <td>
                                <div class="flex-container">
                                    <div class="round">
                                        <input type="hidden" name="resultStatus" id="result-status" class="all-values result-status-${index}" value="${addedResult.resultStatus && addedResult.resultStatus != null && addedResult.resultStatus != 'undefined' && addedResult.resultStatus != '' ? (addedResult.resultStatus == 'P' ? 'P' : 'F') : ''}">

                                        <input type="checkbox" data-result-id="result-status-${index}" data-index="attendance-check-${index}" data-target="P" class="mark-attendance mark-attendance-pass mark-checkbox-p attendance-check-${index}" id="checkbox-p-${index}" ${addedResult.resultStatus == 'P' ? 'checked' : ''}/>
                                        <label for="checkbox-p-${index}" data-toggle="tooltip" title="Pass"><b class="check-text">P</b></label>
                                    </div>
                                    <div class="round round-d">
                                        <input type="checkbox" data-result-id="result-status-${index}" data-index="attendance-check-${index}" data-target="F" class="mark-attendance mark-attendance-fail mark-checkbox-a attendance-check-${index}"  id="checkbox-a-${index}" ${addedResult.resultStatus == 'F' ? 'checked' : ''} />
                                        <label for="checkbox-a-${index}" data-toggle="tooltip" title="Fail" ><b class="check-text">F</b></label>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <div class="flex-container" >
                                    <div class="round">
                                        <input id="hold-flag-${index}" type="hidden" name="holdFlag" id="hold-flag" class="all-hold-flags hold-flag-${index}" value="${addedResult.holdFlag && addedResult.holdFlag != null && addedResult.holdFlag != 'undefined' && addedResult.holdFlag != '' ? (addedResult.holdFlag == 1 ? 1 : 0) : 0}">

                                        <input type="checkbox" data-hold-flag-id="hold-flag-${index}"  data-index="release-hold-check-${index}" data-target="0" class="mark-release-all mark-hold-release mark-checkbox-r release-hold-check-${index}" id="checkbox-r-${index}" ${typeof addedResult.holdFlag != 'undefined' &&( addedResult.holdFlag == 0 || addedResult.holdFlag == null ||  addedResult.holdFlag == 'undefined' || addedResult.holdFlag == ''  ) ? 'checked' : ''}/>
                                        <label for="checkbox-r-${index}" data-toggle="tooltip" title="Publish the Result"><b class="check-text">R</b></label>
                                    </div>
                                    <div class="round round-d">
                                        <input type="checkbox" data-hold-flag-id="hold-flag-${index}"  data-index="release-hold-check-${index}" data-target="1" class="mark-hold-all mark-hold-release mark-checkbox-h release-hold-check-${index}"  id="checkbox-h-${index}" ${typeof addedResult.holdFlag != 'undefined' && addedResult.holdFlag == 1 ? 'checked' : '' } />
                                        <label for="checkbox-h-${index}" data-toggle="tooltip" title="Hold the Result"><b class="check-text">H</b></label>
                                    </div>
                                </div>
                                </td>
                                </tr>`);
                                hideProgressBar('p2-students-result-list');
                            }else{
                                overallAddTable.append(`
                                <tr>
                                    <td style="display:none;">
                                        <input class="form-control" name="examOverallID" type="text" value='0'>
                                    </td>
                                    <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                                    <td><span data-toggle="tooltip" title="${value.rollNo}">
                                    ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                                    </span></td>
                                    <td style="display:none;">
                                        <input class="form-control" name="studentID" type="text" value=' ${ value.studentID }'>
                                    </td>
                                    <td>${value.studentName }</td>
                                    <td style="text-align: center;">
                                    <label>${studentOverallPercentageData && studentOverallPercentageData.percentAge != undefined  &&  studentOverallPercentageData.percentAge ? studentOverallPercentageData.percentAge : addedResult.overallPercentage }</label>
                                    </td>
                                    <td>
                                    <input maxlength="5" class="form-control" name="rank" type="text">
                                    </td>
                                    <td>
                                    <input maxlength="5" class="form-control" name="overallGrade" type="text">
                                    </td>
                                    <td>
                                        <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="overallNotes" type="text"></textarea>
                                    </td>
                                    <td>
                                        ${overallFlag}
                                    </td>
                                    <td>
                                    <div class="flex-container">
                                        <div class="round">
                                            <input type="hidden" name="resultStatus" id="result-status" class="all-values result-status-${index}" value="">

                                            <input type="checkbox" data-index="attendance-check-${index}" data-target="P" data-result-id="result-status-${index}" class="mark-attendance-pass mark-attendance mark-checkbox-p attendance-check-${index}" data-json='' id="checkbox-p-${index}"/>
                                            <label for="checkbox-p-${index}" data-toggle="tooltip" title="Pass"><b class="check-text">P</b></label>
                                        </div>
                                        <div class="round round-d">
                                            <input type="checkbox" data-result-id="result-status-${index}" data-index="attendance-check-${index}" data-target="F" class="mark-attendance-fail mark-attendance mark-checkbox-a attendance-check-${index}" data-json='' id="checkbox-a-${index}" />
                                            <label for="checkbox-a-${index}" data-toggle="tooltip" title="Fail"><b class="check-text">F</b></label>
                                        </div>
                                    </div>
                                    </td>
                                    <td>
                                        <div class="flex-container">
                                            <div class="round">
                                                <input id="hold-flag-${index}" type="hidden" name="holdFlag" class="all-values hold-flag-${index}" value="1">

                                                <input type="checkbox" data-hold-flag-id="hold-flag-${index}" data-index="release-hold-check-${index}" data-target="0" class="mark-release-all mark-hold-release mark-checkbox-r release-hold-check-${index}" id="checkbox-r-${index}"/>
                                                <label for="checkbox-r-${index}" data-toggle="tooltip" title="Publish the Result"><b class="check-text">R</b></label>
                                            </div>
                                            <div class="round round-d">
                                                <input type="checkbox" data-hold-flag-id="hold-flag-${index}"  data-index="release-hold-check-${index}" data-target="1" class="mark-hold-all mark-hold-release mark-checkbox-h release-hold-check-${index}"  id="checkbox-h-${index}"/>
                                                <label for="checkbox-h-${index}" data-toggle="tooltip" title="Hold the Result"><b class="check-text">H</b></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>`);
                                hideProgressBar('p2-students-result-list');
                            }
                            });
                    }
                    
                
                    hideProgressBar('p2-students-result-list');
                }else{
                    overallAddTable.empty();
                    overallAddTable.append(`
                        <tr>
                            <td colspan="10" class="text-center"><strong style="color:#188ae2;font-weight:bold">No Records Found</strong></td>
                        </tr>`)
                        
                    hideProgressBar('p2-students-result-list');
                }
                
                } else {
                    overallAddTable.empty();
                    overallAddTable.append(`<tr>
                            <td colspan="10" class="text-center"><strong style="color:#188ae2;font-weight:bold">No Records Found</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                }
            
        },
        error: function (err) {
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-students-result-list');
        }
    }) 
}

$(document).on('click', '.passAllIcon', function(){
    let targetedValue = $(this).attr('data-target');
    if($('.studentCheckBoxAllPass').prop('checked')){
        $('.studentCheckBoxAllPass').prop('checked', false);
        $('.all-values').val('')
        $('.mark-attendance-pass').val('')
        $('.mark-attendance-pass').prop('checked', false)
    }else{
        $('.studentCheckBoxAllPass').prop('checked', true);
        $('.studentCheckBoxAllFail').prop('checked', false);
        $('.all-values').val(targetedValue)
        $('.mark-attendance-pass').val(targetedValue)
        $('.mark-attendance-pass').prop('checked', true)
        $('.mark-attendance-fail').prop('checked', false)
    }
});

$(document).on('click', '.failAllIcon', function(){
    let targetedValue = $(this).attr('data-target');
    if($('.studentCheckBoxAllFail').prop('checked')){
        $('.studentCheckBoxAllFail').prop('checked', false);
        $('.all-values').val('')
        $('.mark-attendance-fail').val('')
        $('.mark-attendance-fail').prop('checked', false)
    }else{
        $('.studentCheckBoxAllFail').prop('checked', true);
        $('.studentCheckBoxAllPass').prop('checked', false);
        $('.all-values').val(targetedValue)
        $('.mark-attendance-fail').val(targetedValue)
        $('.mark-attendance-fail').prop('checked', true)
        $('.mark-attendance-pass').prop('checked', false)
    }
});

$(document).on('click', '.releaseAllIcon', function(){
    let targetedValue = $(this).attr('data-target');
    if($('.studentCheckBoxAllRelease').prop('checked')){
        $('.studentCheckBoxAllRelease').prop('checked', false);
        $('.all-hold-flags').val(1)
        $('.mark-release-all').val(1)
        $('.mark-release-all').prop('checked', false)
    }else{
        $('.studentCheckBoxAllRelease').prop('checked', true);
        $('.studentCheckBoxAllHold').prop('checked', false);
        $('.all-hold-flags').val(targetedValue)
        $('.mark-release-all').val(targetedValue)
        $('.mark-release-all').prop('checked', true)
        $('.mark-hold-all').prop('checked', false)
    }
});

$(document).on('click', '.holdAllIcon', function(){
    let targetedValue = $(this).attr('data-target');
    if($('.studentCheckBoxAllHold').prop('checked')){
        $('.studentCheckBoxAllHold').prop('checked', false);
        $('.all-hold-flags').val(1)
        $('.mark-hold-all').val(1)
        $('.mark-hold-all').prop('checked', false)
    }else{
        $('.studentCheckBoxAllHold').prop('checked', true);
        $('.studentCheckBoxAllRelease').prop('checked', false);
        $('.all-hold-flags').val(targetedValue)
        $('.mark-hold-all').val(targetedValue)
        $('.mark-hold-all').prop('checked', true)
        $('.mark-release-all').prop('checked', false)
    }
});

$(document).on('click', '.mark-attendance', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    let targetedValue = $(this).attr('data-target');
    let resultId = $(this).attr('data-result-id');
    if(checked){
        $("."+resultId).val(targetedValue)
        $('.' + index).prop('checked', false);
        $(this).prop('checked', true);
    }else{
        $(this).prop('checked', false);
        $("."+resultId).val('')
    }
});

$(document).on('click', '.mark-attendance-modal', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    if(checked){
        $('.' + index).prop('checked', false);
        $(this).prop('checked', true);
    }else{
        $(this).prop('checked', true);
    }
});

$(document).on('click', '.mark-hold-release', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    let targetedValue = $(this).attr('data-target');
    let fieldValue = $(this).attr('data-hold-flag-id')
    if(checked){
        $('.'+fieldValue).val(targetedValue)
        $('.' + index).prop('checked', false);
        $(this).prop('checked', true);
    }else{
        $(this).prop('checked', true);
    }
});

$(document).on('click', '.mark-hold-release-modal', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    if(checked){
        $('.' + index).prop('checked', false);
        $(this).prop('checked', true);
    }else{
        $(this).prop('checked', true);
    }
});
// Subject submit
overallResultForm.on('submit', function (e) {
    e.preventDefault();
    var bulkArray = [];
    var assessmentOnlyFlag = $('#filter-classLevels').find(':selected').attr('data-assesstment');
    var result = $(this).serializeArray();
    var filtersformData = $('#filter-form').serializeArray();
    var sliecedArray = filtersformData.slice(4, filtersformData.length);
    var chunkedArray ;
    if(Number(assessmentOnlyFlag) > 0 ){
        chunkedArray = chunkArray(result, 6);
    }else{
        chunkedArray = chunkArray(result, 7);
    }
    chunkedArray.forEach((val, index) => {
        var myObject = {};
        myObject['classID'] = classID;
        myObject['studentID'] = studentID;
        sliecedArray.forEach((data, formIndex) => {
            myObject[data.name] = data.value ;
        });

        val.forEach((innerVal, innerIndex) => {
            if(innerVal.name == 'examOverallID'){
                if( Number(innerVal.value) > 0){  
                    myObject[innerVal.name] = innerVal.value;
                } 
            }else{
                // if(innerVal.name == "resultStatusPass"){
                //     myObject['resultStatus'] = 'P';
                // }else if(innerVal.name == "resultStatusFail"){
                //     myObject['resultStatus'] = 'F';
                // }
                if(innerVal.name == "resultStatus"){
                    myObject['resultStatus'] = $('#result-status').val();
                }
                if(innerVal.name == "holdFlag"){
                    myObject['holdFlag'] = $('#hold-flag-'+index).val();
                }
                // if(innerVal.name == "holdStatusR"){
                //     myObject['holdFlag'] = "0" ;
                // }else if(innerVal.name == "holdStatusH"){
                //     myObject['holdFlag'] = "1" ;
                // }
                else{
                    myObject[innerVal.name] = innerVal.value;
                }
            }
        });
        bulkArray.push(myObject);
    });
    // bulkArray.forEach((value, index) => {
    //     bulkArray[index] = clean(value);
    // });
    bulkArray.forEach((value, index) => {
        for (var propName in value) { 
           if(propName !== 'resultStatus'){
                if (value[propName] === null || value[propName] === undefined || value[propName] == '') {
                        delete bulkArray[index][propName];
                }
            } 
        }
    });
    bulkArray =  _.filter(bulkArray,v => _.keys(v).length !== 0);
    submitOverallResult(bulkArray);
});

function submitOverallResult(bulkArray){
    $.ajax({
        url: herokuUrl + '/setExamTermResultsArray',
        method: 'POST',
        headers,
        data: {
            array: bulkArray
        },
        beforeSend: function () {
            disableButtonsToggle();
            overallResultForm.find('.form-message').remove();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                $('#exampleModal').modal('hide')
                overallResultForm.trigger('reset');
                swal(
                    'Success',
                    'Successfully Submitted',
                    'success'
                );
                getOverallResult();
                hideAddFormContainer();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                overallResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

            if (xhr.status === 500) {
                overallResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click','.printExamsResult', function(){
    var subjectExamResultTable = $('#subject-result-detail-table-body')
    var subjectExamResultTableWithoutGrade = $('#subject-result-detail-table-body-without-grade')
    var subjectExamResultTableWithoutMarks = $('#subject-result-detail-table-body-without-marks')
    var subjectExamResultTableWithoutMarksAndGrade = $('#subject-result-detail-table-body-without-marks-and-grade')
    var nonSubjectExamResultTable = $('#non-subject-result-detail-table-body')
    var nonSubjectExamResultTableWithoutGrade = $('#non-subject-result-detail-table-body-without-grade')
    var attendanceDetailTableBody = $('#attendance-details-table-body')
    var checkShowMarksOnApp = false;
    var checkSubjectGrade = false;
    var checkNonSubjectGrade = false;
    let data = {}
    studentID = $(this).attr('data-student-id')
    examsTermID = $(this).attr('data-examterm-id')
    isAssessment = $(this).attr('data-check-isAssessment')
    if(classID !== ''){
        data.classID = classID
    }else{
        data.classID = $('#filter-classes').val()
    } 
    // if(classID !== ''){
    //     data.classID = 891;
    // }else{
    //     data.classID = 891;
    // } 
    if(examsTermID !== ''){
        data.examsTermID = examsTermID
    }
    if(studentID !== ''){
        data.studentID = studentID
    }
    $.LoadingOverlay('show')
    $.ajax({
        url: herokuUrl+'/getExamResultReport',
        type: 'POST',
        headers,
        beforeSend: function(){
            subjectExamResultsArray = [];
            nonSubjectExamResultsArray = [];
        },
        data,
        success: function(res){
            let response = res.response;
             subjectExamResultTableWithoutGrade.empty(); 
             subjectExamResultTableWithoutMarks.empty(); 
             subjectExamResultTableWithoutMarksAndGrade.empty(); 
             nonSubjectExamResultTableWithoutGrade.empty(); 
            subjectExamResultTable.empty();
            nonSubjectExamResultTable.empty();
            attendanceDetailTableBody.empty();
            if (response.length > 0) {
                overallExamsResultDetail = response[0].examResultsOverallResponse[0];
                overallSubjectExamResultDetails = response[1].examSubjectsResultResponse;
                overallNonSubjectExamResultDetails = response[2].examNsResultResponse;
                overallExamsAttendanceReport = response[3].examAttendanceResponse;

                if(overallNonSubjectExamResultDetails.length > 0){
                    overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                       if(nonSubjectValue.gradeNS === null ){
                           checkNonSubjectGrade = false
                       }else{
                           checkNonSubjectGrade = true
                       }
                    })
                }

                overallExamsAttendanceReport.forEach((attendanceValue, attendanceIndex)=>{
                    attendanceDetailTableBody.append(`
                        <tr>
                            <td>${attendanceValue.TotalDays && attendanceValue.TotalDays !== 'null' ? attendanceValue.TotalDays : '-'}</td>
                            <td>${attendanceValue.Present && attendanceValue.Present !== 'null' ? attendanceValue.Present : '-'}</td>
                            <td>${attendanceValue.Absent && attendanceValue.Absent !== 'null' ? attendanceValue.Absent : '-'}</td>
                            <td>${attendanceValue.Late && attendanceValue.Late !== 'null' ? attendanceValue.Late : '-'}</td>
                            <td>${attendanceValue.Leaves && attendanceValue.Leaves !== 'null' ? attendanceValue.Leaves : '-'}</td>
                        </tr>
                    `)
                })

                var doc = new jsPDF('p')
                let extension = null;
                var getImage = overallExamsResultDetail.schoolLogo;
                var decodeImage = atob(getImage)
                var lowerCase = decodeImage.toLowerCase();
                if(lowerCase.indexOf('png') !== -1){
                    extension = 'png'
                }else if(lowerCase.indexOf('jpeg') !== -1 || lowerCase.indexOf('jpeg') !== -1){
                    extension = 'jpeg'
                }

                var imgData = `data:image/${extension};base64,`+overallExamsResultDetail.schoolLogo;
                doc.addImage(imgData, 'PNG', 14, 10, 20, 20);
                var checkStringLength = overallExamsResultDetail.schoolName.length;
                
                if(checkStringLength > 31){
                    var splitTitle = doc.splitTextToSize(overallExamsResultDetail.schoolName, 100);
                    doc.setFontSize(18);
                    doc.text(splitTitle, 38, 15, {
                        align : 'center'
                    })
                    
                    doc.setFontSize(14);
                    doc.text(overallExamsResultDetail.branchName, 38, 29, {
                        align : 'center'
                    })
                }else{
                    var splitTitle = doc.splitTextToSize(overallExamsResultDetail.schoolName, 100);
                    doc.setFontSize(18);
                    doc.text(splitTitle, 38, 15, {
                        align : 'center'
                    })
                    
                    doc.setFontSize(14);
                    doc.text(overallExamsResultDetail.branchName, 38, 22, {
                        align : 'center'
                    })
                }
                
                doc.setFontSize(14);
                doc.text(overallExamsResultDetail.termName, 180, 15, {
                    align : 'center'
                })
                
                doc.setFontSize(12);
                doc.text(overallExamsResultDetail.termStartDate+' - '+overallExamsResultDetail.termEndDate, 172, 22, {
                    align : 'center'
                })
                
                // div
                    if(overallExamsResultDetail.rank !== '' && overallExamsResultDetail.rank !== null){
                    doc.setFontSize(12);
                    doc.setFontType("bold");
                    doc.text('Rank', 130, 40, {
                        align : 'left'
                    })
                    
                    doc.setFontType("normal");
                    doc.text(overallExamsResultDetail.rank && overallExamsResultDetail.rank !== 'null' ? ': '+overallExamsResultDetail.rank : ': -', 160, 40, {
                        align : 'left'
                    })
                }
                
                if(overallExamsResultDetail.overallPercentage !== '' && overallExamsResultDetail.overallPercentage !== null){
                        if(overallExamsResultDetail.rank === null){
                            doc.setFontType("bold");
                            doc.text('Percentage', 130, 40, {
                                align : 'left'
                            })
               
                            doc.setFontType("normal");
                            doc.text(overallExamsResultDetail.overallPercentage && overallExamsResultDetail.overallPercentage !== 'null' ? ': '+overallExamsResultDetail.overallPercentage.toFixed(2)+'%' : ': -', 160, 40, {
                                align : 'left'
                            })
                        }else{
                            doc.setFontType("bold");
                            doc.text('Percentage', 130, 46, {
                                align : 'left'
                            })
               
                            doc.setFontType("normal");
                            doc.text(overallExamsResultDetail.overallPercentage && overallExamsResultDetail.overallPercentage !== 'null' ? ': '+overallExamsResultDetail.overallPercentage.toFixed(2)+'%' : ': -', 160, 46, {
                                align : 'left'
                            })
                        }
                }

                    if(overallExamsResultDetail.overallGrade !== '' && overallExamsResultDetail.overallGrade !== null){
                        if(overallExamsResultDetail.overallPercentage === null){
                            doc.setFontType("bold");
                            doc.text('Grade', 130, 46, {
                                align : 'left'
                            })
                            
                            doc.setFontType("normal");
                            doc.text(overallExamsResultDetail.overallGrade && overallExamsResultDetail.overallGrade !== 'null' ? ': '+overallExamsResultDetail.overallGrade : ': -', 160, 46, {
                                align : 'left'
                            })
                        }else if(overallExamsResultDetail.overallPercentage === null){
                            doc.setFontType("bold");
                            doc.text('Grade', 130, 40, {
                                align : 'left'
                            })
                            
                            doc.setFontType("normal");
                            doc.text(overallExamsResultDetail.overallGrade && overallExamsResultDetail.overallGrade !== 'null' ? ': '+overallExamsResultDetail.overallGrade : ': -', 160, 40, {
                                align : 'left'
                            })
                        }else{
                            doc.setFontType("bold");
                            doc.text('Grade', 130, 52, {
                                align : 'left'
                            })
                            
                            doc.setFontType("normal");
                            doc.text(overallExamsResultDetail.overallGrade && overallExamsResultDetail.overallGrade !== 'null' ? ': '+overallExamsResultDetail.overallGrade : ': -', 160, 52, {
                                align : 'left'
                            })
                        }
                }
                
                if(overallExamsResultDetail.fullname !== '' && overallExamsResultDetail.fullname !== null){
                    if(overallExamsResultDetail.overallGrade === null){
                        doc.setFontType("bold");
                        doc.text('Class Teacher', 130, 46, {
                            align : 'left'
                        })
                        
                        var fullname = overallExamsResultDetail.fullname != 'null' && overallExamsResultDetail.fullname != null ? overallExamsResultDetail.fullname : '      -'; 
                        doc.setFontType("normal");
                        doc.text(': '+fullname, 160, 46, {
                            align : 'left'
                        })
                    }else if(overallExamsResultDetail.overallGrade === null && overallExamsResultDetail.overallPercentage === null){
                        doc.setFontType("bold");
                        doc.text('Class Teacher', 130, 40, {
                            align : 'left'
                        })
                        
                        var fullname = overallExamsResultDetail.fullname != 'null' && overallExamsResultDetail.fullname != null ? overallExamsResultDetail.fullname : '      -'; 
                        doc.setFontType("normal");
                        doc.text(': '+fullname, 160, 40, {
                            align : 'left'
                        })
                    }else if(overallExamsResultDetail.overallGrade !== null && overallExamsResultDetail.overallPercentage === null){
                        doc.setFontType("bold");
                        doc.text('Class Teacher', 130, 46, {
                            align : 'left'
                        })
                        
                        var fullname = overallExamsResultDetail.fullname != 'null' && overallExamsResultDetail.fullname != null ? overallExamsResultDetail.fullname : '      -'; 
                        doc.setFontType("normal");
                        doc.text(': '+fullname, 160, 46, {
                            align : 'left'
                        })
                    }else if(overallExamsResultDetail.overallPercentage !== null && overallExamsResultDetail.overallGrade === null){
                        doc.setFontType("bold");
                        doc.text('Class Teacher', 130, 46, {
                            align : 'left'
                        })
                        
                        var fullname = overallExamsResultDetail.fullname != 'null' && overallExamsResultDetail.fullname != null ? overallExamsResultDetail.fullname : '      -'; 
                        doc.setFontType("normal");
                        doc.text(': '+fullname, 160, 46, {
                            align : 'left'
                        })
                    }else{
                        doc.setFontType("bold");
                        doc.text('Class Teacher', 130, 58, {
                            align : 'left'
                        })
                        
                        var fullname = overallExamsResultDetail.fullname != 'null' && overallExamsResultDetail.fullname != null ? overallExamsResultDetail.fullname : '      -'; 
                        doc.setFontType("normal");
                        doc.text(': '+fullname, 160, 58, {
                            align : 'left'
                        })
                    }
                }
                
                // div        
                doc.setFontSize(12);
                doc.setFontType("bold");
                doc.text('Student Name',14, 40, {
                    align : 'left'
                })
                
                var studentName = overallExamsResultDetail.studentName != 'null' && overallExamsResultDetail.studentName != null ? overallExamsResultDetail.studentName : '      -'; 
                doc.setFontType("normal");
                doc.text(': '+studentName, 44, 40, {
                    align : 'left'
                })
                
                doc.setFontType("bold");
                doc.text('Father Name',14, 46, {
                    align : 'left'
                })
                
                var parentName = overallExamsResultDetail.parentName != 'null' && overallExamsResultDetail.parentName != null ? overallExamsResultDetail.parentName : '      -'; 
                doc.setFontType("normal");
                doc.text(': '+parentName, 44, 46, {
                    align : 'left'
                })
                
                doc.setFontType("bold");
                doc.setFontSize(12);
                doc.text('Roll No',14, 52, {
                    align : 'left'
                })
                
                doc.setFontType("normal");
                doc.text(': '+overallExamsResultDetail.rollNo, 44, 52, {
                    align : 'left'
                })
        
                doc.setFontType("bold");
                doc.setFontSize(12);
                doc.text('Class',14, 58, {
                    align : 'left'
                })
                
                doc.setFontType("normal");
                doc.text(': '+overallExamsResultDetail.classAlias+'-'+overallExamsResultDetail.classSection, 44, 58, {
                    align : 'left'
                })
        
                /*Overall Exam Subject Result*/
                overallSubjectExamResultDetails.forEach((subjectValue, subjectIndex) => {
                    if(subjectValue.showMarksOnApp === 0){
                        checkShowMarksOnApp = false;
                    }else{
                        checkShowMarksOnApp = true;
                    }

                    if(subjectValue.grade !== null){
                        checkSubjectGrade = true
                    }else{
                        checkSubjectGrade = false
                    }
                })
                if(isAssessment == '0'){
                    if(overallSubjectExamResultDetails.length > 0){
                        if(checkShowMarksOnApp === true){
                            if(checkSubjectGrade === true){
                                overallSubjectExamResultDetails.forEach((subjectValue, subjectIndex) => {
                                    subjectExamResultTable.append(`
                                     <tr>
                                         <td>${subjectIndex+1}</td>
                                         <td>${subjectValue.subjectName}</td>
                                         <td>${subjectValue.totalMarks && subjectValue.totalMarks !== 'null' ? subjectValue.totalMarks : '-'}</td>
                                         <td>${subjectValue.obtainedMarks && subjectValue.obtainedMarks !== 'null' ? subjectValue.obtainedMarks : '-'}</td>
                                         <td>${subjectValue.grade && subjectValue.grade !== 'null' ? subjectValue.grade : '-'}</td>
                                         <td>${subjectValue.notes && subjectValue.notes !== 'null' ? subjectValue.notes : '-'}</td>
                                     </tr>
                                    `);
                                }) 
                            }else{
                                overallSubjectExamResultDetails.forEach((subjectValue, subjectIndex) => {
                                    subjectExamResultTableWithoutGrade.append(`
                                     <tr>
                                         <td>${subjectIndex+1}</td>
                                         <td>${subjectValue.subjectName}</td>
                                         <td>${subjectValue.totalMarks && subjectValue.totalMarks !== 'null' ? subjectValue.totalMarks : '-'}</td>
                                         <td>${subjectValue.obtainedMarks && subjectValue.obtainedMarks !== 'null' ? subjectValue.obtainedMarks : '-'}</td>
                                         <td>${subjectValue.notes && subjectValue.notes !== 'null' ? subjectValue.notes : '-'}</td>
                                     </tr>
                                    `);
                                })
                            }

                            var subjectTableHeading = doc.autoTableHtmlToJson(document.getElementById("subject-result-detail-table-heading"));
                            doc.autoTable(
                                subjectTableHeading.columns, 
                                subjectTableHeading.data,
                                {
                                    styles:{fontSize:14, textColor:[0,0,0], fillColor:[255,255,255],halign:'center'},
                                    margin:{top:60}
                                },
                            );
                               
                            if(checkSubjectGrade === true){
                                var subjectTable = doc.autoTableHtmlToJson(document.getElementById("subject-result-detail-table"));
                                doc.autoTable(
                                    subjectTable.columns, 
                                    subjectTable.data, 
                                    {  
                                        columnStyles: {
                                            0: { halign: 'center', cellWidth: 12 },
                                            1: { cellWidth: 30 },
                                            2: { halign: 'center', cellWidth: 25 },
                                            3: { halign: 'center', cellWidth: 27 },
                                            4: { cellWidth: 15 },
                                            5: { fontSize: 8 },
                                        },
                                        startY: doc.autoTableEndPosY(),
                                    }
                                );
                            }else{
                                var subjectTable = doc.autoTableHtmlToJson(document.getElementById("subject-result-detail-table-without-grade"));
                                doc.autoTable(
                                    subjectTable.columns, 
                                    subjectTable.data, 
                                    {  
                                        columnStyles: {
                                            0: { halign: 'center', cellWidth: 12 },
                                            1: { cellWidth: 30 },
                                            2: { halign: 'center', cellWidth: 25 },
                                            3: { halign: 'center', cellWidth: 27 },
                                            4: { fontSize: 8 },
                                        },
                                        startY: doc.autoTableEndPosY(),
                                    }
                                );
                            }
                            if(checkNonSubjectGrade === true){
                                if(overallNonSubjectExamResultDetails.length > 0){
                                    overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                                        nonSubjectExamResultTable.append(`
                                        <tr>
                                            <td>${nonSubjectIndex+1}</td>
                                            <td>${nonSubjectValue.nonSubjectName}</td>
                                            <td>${nonSubjectValue.legendValue && nonSubjectValue.legendValue !== 'null' ? nonSubjectValue.legendValue : '-'}</td>
                                            <td>${nonSubjectValue.gradeNS && nonSubjectValue.gradeNS !== 'null' ? nonSubjectValue.gradeNS : '-'}</td>
                                            <td>${nonSubjectValue.notesNS && nonSubjectValue.notesNS !== 'null' ? nonSubjectValue.notesNS : '-'}</td>
                                        </tr>`);
                                    })
                                }
                            }else{
                                if(overallNonSubjectExamResultDetails.length > 0){
                                    overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                                        nonSubjectExamResultTableWithoutGrade.append(`
                                        <tr>
                                            <td>${nonSubjectIndex+1}</td>
                                            <td>${nonSubjectValue.nonSubjectName}</td>
                                            <td>${nonSubjectValue.legendValue && nonSubjectValue.legendValue !== 'null' ? nonSubjectValue.legendValue : '-'}</td>
                                            <td>${nonSubjectValue.notesNS && nonSubjectValue.notesNS !== 'null' ? nonSubjectValue.notesNS : '-'}</td>
                                        </tr>`);
                                    })
                                }
                            }

                                if(checkNonSubjectGrade === true){
                                    if(overallNonSubjectExamResultDetails.length > 0){
                                        var nonSubjectTableHeading = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table-heading"));
                                        doc.autoTable(
                                            nonSubjectTableHeading.columns,
                                            nonSubjectTableHeading.data,
                                            {
                                                styles:{
                                                    halign: 'center',
                                                    fontSize:'14', 
                                                    fillColor:[255,255,255], 
                                                    textColor:[0,0,0]}
                                                },
                                                {
                                                    margin: {
                                                        top: 80
                                                    }
                                                }, 
                                                {
                                                    startY: doc.autoTableEndPosY()
                                        });
                                        overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                                            nonSubjectExamResultTable.append(`
                                            <tr>
                                                <td>${nonSubjectIndex+1}</td>
                                                <td>${nonSubjectValue.nonSubjectName}</td>
                                                <td>${nonSubjectValue.legendValue && nonSubjectValue.legendValue !== 'null' ? nonSubjectValue.legendValue : '-'}</td>
                                                <td>${nonSubjectValue.gradeNS && nonSubjectValue.gradeNS !== 'null' ? nonSubjectValue.gradeNS : '-'}</td>
                                                <td>${nonSubjectValue.notesNS && nonSubjectValue.notesNS !== 'null' ? nonSubjectValue.notesNS : '-'}</td>
                                            </tr>`);
                                        })
                                    }
                                    var nonSubjectTable = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table"));
                                    doc.autoTable(
                                        nonSubjectTable.columns, 
                                        nonSubjectTable.data, 
                                        {
                                            columnStyles: {
                                                0: { halign: 'center', cellWidth: 12 },
                                                1: { cellWidth: 30 },
                                                2: { cellWidth: 20 },
                                                3: { halign: 'center', cellWidth: 15 },
                                                4: { fontSize: 8 },
                                            },
                                            startY: doc.autoTableEndPosY()
                                        }
                                    );
                                }else{
                                    if(overallNonSubjectExamResultDetails.length > 0){
                                        overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                                            nonSubjectExamResultTableWithoutGrade.append(`
                                            <tr>
                                                <td>${nonSubjectIndex+1}</td>
                                                <td>${nonSubjectValue.nonSubjectName}</td>
                                                <td>${nonSubjectValue.legendValue && nonSubjectValue.legendValue !== 'null' ? nonSubjectValue.legendValue : '-'}</td>
                                                <td>${nonSubjectValue.notesNS && nonSubjectValue.notesNS !== 'null' ? nonSubjectValue.notesNS : '-'}</td>
                                            </tr>`);
                                        })
                                        var nonSubjectTable = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table-without-grade"));
                                        doc.autoTable(
                                            nonSubjectTable.columns, 
                                            nonSubjectTable.data, 
                                            {
                                                columnStyles: {
                                                    0: { halign: 'center', cellWidth: 12 },
                                                    1: { cellWidth: 30 },
                                                    2: { cellWidth: 20 },
                                                    3: { fontSize: 8 },
                                                },
                                                startY: doc.autoTableEndPosY()
                                            }
                                        );
                                    }
                                }
                        }else{
                            if(checkSubjectGrade === true){
                                overallSubjectExamResultDetails.forEach((subjectValue, subjectIndex) => {
                                    subjectExamResultTableWithoutMarks.append(`
                                    <tr>
                                        <td>${subjectIndex+1}</td>
                                        <td>${subjectValue.subjectName}</td>
                                        <td>${subjectValue.grade && subjectValue.grade !== 'null' ? subjectValue.grade : '-'}</td>
                                        <td>${subjectValue.notes && subjectValue.notes !== 'null' ? subjectValue.notes : '-'}</td>
                                    </tr>
                                   `);
                               })
                            }else{
                                overallSubjectExamResultDetails.forEach((subjectValue, subjectIndex) => {
                                    subjectExamResultTableWithoutMarksAndGrade.append(`
                                    <tr>
                                        <td>${subjectIndex+1}</td>
                                        <td>${subjectValue.subjectName}</td>
                                        <td>${subjectValue.notes && subjectValue.notes !== 'null' ? subjectValue.notes : '-'}</td>
                                    </tr>
                                   `);
                                })
                            }
                            
                            var subjectTableHeading = doc.autoTableHtmlToJson(document.getElementById("subject-result-detail-table-heading"));
                            doc.autoTable(
                                subjectTableHeading.columns, 
                                subjectTableHeading.data,
                                {
                                    styles:{fontSize:14, textColor:[0,0,0], fillColor:[255,255,255],halign:'center'},
                                    margin:{top:60}
                                },
                            );
                               
                            if(checkSubjectGrade === true){
                                var subjectTable = doc.autoTableHtmlToJson(document.getElementById("subject-result-detail-table-without-marks"));
                                doc.autoTable(
                                    subjectTable.columns, 
                                    subjectTable.data, 
                                    {  
                                        columnStyles: {
                                            0: { halign: 'center', cellWidth: 12 },
                                            1: { cellWidth: 30 },
                                            2: { halign:'center' , cellWidth: 15 },
                                            3: { fontSize: 8 },
                                        },
                                        startY: doc.autoTableEndPosY(),
                                    }
                                );
                            }else{
                                var subjectTable = doc.autoTableHtmlToJson(document.getElementById("subject-result-detail-table-without-marks-and-grade"));
                                doc.autoTable(
                                    subjectTable.columns, 
                                    subjectTable.data, 
                                    {  
                                        columnStyles: {
                                            0: { halign: 'center', cellWidth: 12 },
                                            1: { cellWidth: 30 },
                                            2: { fontSize: 8 },
                                        },
                                        startY: doc.autoTableEndPosY(),
                                    }
                                );
                            }
                            var nonSubjectTableHeading = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table-heading"));
                            doc.autoTable(
                            nonSubjectTableHeading.columns,
                            nonSubjectTableHeading.data,
                            {
                                styles:{
                                    halign: 'center',
                                    fontSize:'14', 
                                    fillColor:[255,255,255], 
                                    textColor:[0,0,0]}
                                },
                                {
                                    margin: {
                                        top: 80
                                    }
                                }, 
                                {
                                    startY: doc.autoTableEndPosY()
                                });
                                if(checkNonSubjectGrade === true){
                                    if(overallNonSubjectExamResultDetails.length > 0){
                                        overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                                            nonSubjectExamResultTable.append(`
                                            <tr>
                                                <td>${nonSubjectIndex+1}</td>
                                                <td>${nonSubjectValue.nonSubjectName}</td>
                                                <td>${nonSubjectValue.legendValue && nonSubjectValue.legendValue !== 'null' ? nonSubjectValue.legendValue : '-'}</td>
                                                <td>${nonSubjectValue.gradeNS && nonSubjectValue.gradeNS !== 'null' ? nonSubjectValue.gradeNS : '-'}</td>
                                                <td>${nonSubjectValue.notesNS && nonSubjectValue.notesNS !== 'null' ? nonSubjectValue.notesNS : '-'}</td>
                                            </tr>`);
                                        })
                                    }
                                    var nonSubjectTable = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table"));
                                    doc.autoTable(
                                        nonSubjectTable.columns, 
                                        nonSubjectTable.data, 
                                        {
                                            columnStyles: {
                                                0: { halign: 'center', cellWidth: 12 },
                                                1: { cellWidth: 30 },
                                                2: { cellWidth: 20 },
                                                3: { halign: 'center', cellWidth: 15 },
                                                4: { fontSize: 8 },
                                            },
                                            startY: doc.autoTableEndPosY()
                                        }
                                    );
                                }else{
                                    if(overallNonSubjectExamResultDetails.length > 0){
                                        overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                                            nonSubjectExamResultTableWithoutGrade.append(`
                                            <tr>
                                                <td>${nonSubjectIndex+1}</td>
                                                <td>${nonSubjectValue.nonSubjectName}</td>
                                                <td>${nonSubjectValue.legendValue && nonSubjectValue.legendValue !== 'null' ? nonSubjectValue.legendValue : '-'}</td>
                                                <td>${nonSubjectValue.notesNS && nonSubjectValue.notesNS !== 'null' ? nonSubjectValue.notesNS : '-'}</td>
                                            </tr>`);
                                        })
                                    }
                                    var nonSubjectTable = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table-without-grade"));
                                    doc.autoTable(
                                        nonSubjectTable.columns, 
                                        nonSubjectTable.data, 
                                        {
                                            columnStyles: {
                                                0: { halign: 'center', cellWidth: 12 },
                                                1: { cellWidth: 30 },
                                                2: { cellWidth: 20 },
                                                3: { fontSize: 8 },
                                            },
                                            startY: doc.autoTableEndPosY()
                                        }
                                    );
                                }
                        }
                        
                    }
                }else{
                    if(overallNonSubjectExamResultDetails.length > 0){
                        overallNonSubjectExamResultDetails.forEach((nonSubjectValue, nonSubjectIndex) => {
                            nonSubjectExamResultTable.append(`
                            <tr>
                                <td>${nonSubjectIndex+1}</td>
                                <td>${nonSubjectValue.nonSubjectName}</td>
                                <td>${nonSubjectValue.legendValue && nonSubjectValue.legendValue !== 'null' ? nonSubjectValue.legendValue : '-'}</td>
                                <td>${nonSubjectValue.gradeNS && nonSubjectValue.gradeNS !== 'null' ? nonSubjectValue.gradeNS : '-'}</td>
                                <td>${nonSubjectValue.notesNS && nonSubjectValue.notesNS !== 'null' ? nonSubjectValue.notesNS : '-'}</td>
                            </tr>`);
                        })
                    }
                    var nonSubjectTableHeading = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table-heading"));
                    doc.autoTable(
                        nonSubjectTableHeading.columns,
                        nonSubjectTableHeading.data,
                        {
                            styles:{fontSize:14, textColor:[0,0,0], fillColor:[255,255,255],halign:'center'},
                            margin:{top:60}
                        });
                            
                            var nonSubjectTable = doc.autoTableHtmlToJson(document.getElementById("non-subject-result-detail-table"));
                            doc.autoTable(
                                nonSubjectTable.columns, 
                                nonSubjectTable.data, 
                                {
                                    columnStyles: {
                                        0: { halign: 'center', cellWidth: 12 },
                                        1: { cellWidth: 30 },
                                        2: { cellWidth: 20 },
                                        3: { halign: 'center', cellWidth: 15 },
                                        4: { fontSize: 8 },
                                    },
                                    startY: doc.autoTableEndPosY()
                                }
                        );
                }
                /*Overall Exam Subject Result End*/
                        
                        var attendanceDetailTableHeading = doc.autoTableHtmlToJson(document.getElementById("attendance-details-table-heading"));
                        doc.autoTable(
                            attendanceDetailTableHeading.columns,
                            attendanceDetailTableHeading.data,
                            {
                                styles:{
                                    halign: 'center',
                                    fontSize:'14', 
                                    fillColor:[255,255,255], 
                                    textColor:[0,0,0]}
                            },
                            {
                                margin: {
                                    top: 70
                                }
                            }, 
                            {
                                startY: doc.autoTableEndPosY()
                            }
                        );
                                
                        var attendanceDetailTableData = doc.autoTableHtmlToJson(document.getElementById("attendance-details"));
                        doc.autoTable(
                            attendanceDetailTableData.columns, 
                            attendanceDetailTableData.data, 
                            {
                                headStyles:{
                                    halign:'center'
                                },
                                columnStyles: {
                                    0: { halign: 'center' },
                                    1: { halign: 'center' },
                                    2: { halign: 'center' },
                                    3: { halign: 'center' },
                                    4: { halign: 'center' },
                                },
                                startY: doc.autoTableEndPosY()
                            }
                        );
                            
                    doc.autoTable({ 
                        html: '#overall-exam-notes'
                    })
    
                    doc.autoTable({
                        head: [
                            [
                                {content: 'Notes:', colspan: 5, styles: {halign: 'left',fillColor: [255, 255, 255], textColor:[0,0,0], fontSize:[14],startY: doc.autoTableEndPosY()}}
                            ]
                        ],
                        body: [[overallExamsResultDetail.overallNotes]],
                    })
                    
                    var str = doc.internal.getNumberOfPages()
                    for(var z = 1; z <= str; z++){
                        doc.rect(3, 3, doc.internal.pageSize.width - 5, doc.internal.pageSize.height - 10, 'S');
                        doc.setFontSize(8);
                        doc.setPage(z); 
                        doc.text(90,295, 'Powered by Skoolify');
                    }
                    doc.save(overallExamsResultDetail.studentName+'_result-sheet.pdf')
                    $.LoadingOverlay('hide')
                } else {
                    subjectResultDetailTable.append('<tr><td colspan="5" style="text-align:center !important">Record Not Found</td></tr>');
                    nonSubjectResultDetailTable.append('<tr><td colspan="5" style="text-align:center !important">Record Not Found</td></tr>');
                }
        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        }
    });
    
})

function getStudentOverallExamResults(){
    let data = {}
    if(classID !== ''){
        data.classID = classID
    } 
    if(examTermID !== ''){
        data.examTermID = examTermID
    }
    if(studentID !== ''){
        data.studentID = studentID
    }
    $.ajax({
        url: herokuUrl+'/getExamResults',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data,
        success: function(res){
            response = parseResponse(res);
            nonSubjectTable.empty();
            if (response.length > 0) {
                response.forEach((val, index) => {
                    nonSubjectTable.append(`
                    <tr>
                        <td  class="p-2"> ${val.nonSubjectName}</td>
                        <td class="p-2"> ${val.gradeNS}</td>
                        <td  class="p-2"> ${val.notesNS}</td>
                        <td  class="p-2"> ${typeof val.legendValue !== 'undefined' && val.legendValue !== 'null' && val.legendValue !== null ? val.legendValue : ' - '}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>comment</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>`);
                })
            } else {
                nonSubjectTable.append('<tr><td colspan="5" style="text-align:center !important">Record Not Found</td></tr>');
            }
        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        }
    });
}


{/* <a class="btn btn-sm btn-success printExamsResult" data-toggle="tooltip" title="Print" data-student-id="${element.studentID}" data-check-isAssessment="0" data-examterm-id="${element.examsTermID}"><i class="fa fa-print"></i></a> */}
