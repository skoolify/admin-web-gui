// const { split } = require("lodash");

var existingLogo = null;
var code = undefined;
var isFormOpen = false;
var bulkArray = '';
$(document).ready(function(){
    // console.log(user,'user')
    getSchools();

    // $('#addContractModal').modal('show');
})

$(document).on('click','.feePaymentAllowed',function(){
    if($(this).prop('checked')){
        $('#feePaymentAllowedInput').val(1)
    }else{
        $('#feePaymentAllowedInput').val(0)
    }
})

$(document).on('click','.showAccNoOnVoucher',function(){
    if($(this).prop('checked')){
        $('#showAccNoOnVoucherInput').val(1)
    }else{
        $('#showAccNoOnVoucherInput').val(0)
    }
})

$(document).on('click','.isActive',function(){
    if($(this).prop('checked')){
        $('#isActiveInput').val(1)
    }else{
        $('#isActiveInput').val(0)
    }
})

$(document).on('click','.selfRegistered',function(){
    if($(this).prop('checked')){
        $('#selfRegisteredInput').val(1)
    }else{
        $('#selfRegisteredInput').val(0)
    }
})

$(document).on('click','.appHomework',function(){
    if($(this).prop('checked')){
        $('#appHomeworkInput').val(1)
    }else{
        $('#appHomeworkInput').val(0)
    }
})

$(document).on('click','.appTimetable',function(){
    if($(this).prop('checked')){
        $('#appTimetableInput').val(1)
    }else{
        $('#appTimetableInput').val(0)
    }
})

$(document).on('click','.appExamQuiz',function(){
    if($(this).prop('checked')){
        $('#appExamQuizInput').val(1)
    }else{
        $('#appExamQuizInput').val(0)
    }
})

$(document).on('click','.appfeeVoucher',function(){
    if($(this).prop('checked')){
        $('#appfeeVoucherInput').val(1)
    }else{
        $('#appfeeVoucherInput').val(0)
    }
})

$(document).on('click','.appAttendance',function(){
    if($(this).prop('checked')){
        $('#appAttendanceInput').val(1)
    }else{
        $('#appAttendanceInput').val(0)
    }
})

$(document).on('click','.appMessage',function(){
    if($(this).prop('checked')){
        $('#appMessageInput').val(1)
    }else{
        $('#appMessageInput').val(0)
    }
})

$(document).on('click','.appCommunicate',function(){
    if($(this).prop('checked')){
        $('#appCommunicateInput').val(1)
    }else{
        $('#appCommunicateInput').val(0)
    }
})

$(document).on('click','.appEvents',function(){
    if($(this).prop('checked')){
        $('#appEventsInput').val(1)
    }else{
        $('#appEventsInput').val(0)
    }
})

$(document).on('click','.appForms',function(){
    if($(this).prop('checked')){
        $('#appFormsInput').val(1)
    }else{
        $('#appFormsInput').val(0)
    }
})

$(document).on('click','.appVanTracking',function(){
    if($(this).prop('checked')){
        $('#appVanTrackingInput').val(1)
    }else{
        $('#appVanTrackingInput').val(0)
    }
})

// $('.clockpicker').clockpicker();
$('.clockpicker').each(function(){
    $(this).clockpicker({
        donetext: 'Done',
        autoclose: true
    });
})

function submitSchool(){
    $.ajax({
        url: herokuUrl+'/setSchools',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            $.LoadingOverlay("hide");
            if(res.response === 'success'){
                $('#logoImg').attr('name','logoImage');
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('#existing-school-logo').addClass('hidden');
                $('#upload-school-logo').removeClass('hidden');
                $('#hiddenSchoolLogo').val('');
                $('#existing-logo').attr('src', '');
                $('#reset-logo-image-edit').addClass('hidden');
                $('#schools-form').trigger('reset');
                getSchools();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitSchool')
            window.requestForToken(xhr)
            $.LoadingOverlay("hide");
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage !== 'undefined' ? xhr.responseJSON.response.userMessage : messages.sorryUnableToProcessPleaseTryAgain}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click', '.list-record-edit', function(){
    let json = JSON.parse($(this).attr('data-json'));
    if(json.feePaymentAllowed === 1){
        $('#feePaymentAllowed').prop('checked', true)
        $('#feePaymentAllowedInput').val(1)
    }else{
        $('#feePaymentAllowed').prop('checked', false)
        $('#feePaymentAllowedInput').val(0)
    }
    if(json.showAccNoOnVoucher === 1){
        $('#showAccNoOnVoucher').prop('checked', true)
        $('#showAccNoOnVoucherInput').val(1)
    }else{
        $('#showAccNoOnVoucher').prop('checked', false)
        $('#showAccNoOnVoucherInput').val(0)
    }
    if(json.appAttendance === 1){
        $('#appAttendance').prop('checked', true)
        $('#appAttendanceInput').val(1)
    }else{
        $('#appAttendance').prop('checked', false)
        $('#appAttendanceInput').val(0)
    }
    if(json.appfeeVoucher === 1){
        $('#appfeeVoucher').prop('checked', true)
        $('#appfeeVoucherInput').val(1)
    }else{
        $('#appfeeVoucher').prop('checked', false)
        $('#appfeeVoucherInput').val(0)
    }
    if(json.appTimetable === 1){
        $('#appTimetable').prop('checked', true)
        $('#appTimetableInput').val(1)
    }else{
        $('#appTimetable').prop('checked', false)
        $('#appTimetableInput').val(0)
    }
    if(json.appMessage === 1){
        $('#appMessage').prop('checked', true)
        $('#appMessageInput').val(1)
    }else{
        $('#appMessage').prop('checked', false)
        $('#appMessageInput').val(0)
    }
    if(json.appHomework === 1){
        $('#appHomework').prop('checked', true)
        $('#appHomeworkInput').val(1)
    }else{
        $('#appHomework').prop('checked', false)
        $('#appHomeworkInput').val(0)
    }
    if(json.appExamQuiz === 1){
        $('#appExamQuiz').prop('checked', true)
        $('#appExamQuizInput').val(1)
    }else{
        $('#appExamQuiz').prop('checked', false)
        $('#appExamQuizInput').val(0)
    }
    if(json.appEvents === 1){
        $('#appEvents').prop('checked', true)
        $('#appEventsInput').val(1)
    }else{
        $('#appEvents').prop('checked', false)
        $('#appEventsInput').val(0)
    }
    if(json.appForms === 1){
        $('#appForms').prop('checked', true)
        $('#appFormsInput').val(1)
    }else{
        $('#appForms').prop('checked', false)
        $('#appFormsInput').val(0)
    }
    if(json.appVanTracking === 1){
        $('#appVanTracking').prop('checked', true)
        $('#appVanTrackingInput').val(1)
    }else{
        $('#appVanTracking').prop('checked', false)
        $('#appVanTrackingInput').val(0)
    }
    if(json.appCommunicate === 1){
        $('#appCommunicate').prop('checked', true)
        $('#appCommunicateInput').val(1)
    }else{
        $('#appCommunicate').prop('checked', false)
        $('#appCommunicateInput').val(0)
    }

    if(json)
    existingLogo = json.schoolLogo;
    $('#upload-school-logo').addClass('hidden');
    $('#existing-school-logo').removeClass('hidden');
    if(existingLogo !== null){
        $('#existing-school-logo img').attr('src', 'data:image/*;base64,'+existingLogo);
    }else{
        $('#existing-school-logo img').attr('src', __BASE__+'/assets/img/logo-2.png');
    }
});

$('#existing-logo').click(function(){
    $('#imgupload').trigger('click');
});

$('#camera-icon').click(function(){
    $('#imgupload').trigger('click');
});

$('#hide-add-form-container').on('click', function(){
    $('#existing-school-logo').addClass('hidden');
    $('#hiddenSchoolLogo').val('');
    $('#existing-logo').attr('src', '');
    $('#upload-school-logo').removeClass('hidden');
});

function readFile() {
    $('#exampleModal').modal('show')
    resetMessages();
    if (this.files && this.files[0]) {
        if((this.files[0].size / 1024) <= 1024){

            var FR= new FileReader();

            FR.addEventListener("load", function(e) {
                if(e.target.result){
                    $('.cr-image').attr('src',e.target.result)
                    imageDemo = $('#upload-demo').croppie({
                        viewport:{
                            width:200,
                            height:200,
                            type:'circle'
                        },
                        boundary:{
                            width:470,
                            height:300,
                        },
                        showZoomer: true,
                        enableResize: false,
                        enableOrientation: false,
                    });

                    imageDemo.croppie('bind', {
                        url:e.target.result
                    }).then(function(){
                        $('.cr-slider').attr({'min':0.5000, 'max':1.5000});
                    });

                    var finalImage = '';
                    $('#crop_image').on('click',function(){
                       imageDemo.croppie('result',{
                            type:'canvas',
                            size:'viewport',
                            quality: 1
                        }).then(function(response){

                            finalImage = response
                            var formData = new FormData();
                            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
                            formData.append('logoImage', finalImage)
                            setTimeout(()=>{
                                $.ajax({
                                    url: __BASE__+'/school/logo',
                                    type: 'POST',
                                    data: formData,
                                    async: false,
                                    dataType:'JSON',
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    beforeSend: function(){
                                    },
                                    success: function(res){
                                        if(res){
                                            $('#hiddenSchoolLogo').val(res.attachment);
                                            $('#logoImg').removeAttr('name');

                                        }
                                    },
                                    error: function(xhr){
                                        window.requestForToken(xhr)
                                        console.log(xhr);
                                    }
                                });
                            },200)

                            $('.cr-image').attr('src','')
                            $('#exampleModal').modal('hide')
                        })
                    })
                    $('.closeModal').on('click',function(){
                        imageDemo.croppie('result',{
                             type:'canvas',
                             size:'original',
                             quality: 1
                        }).then(function(response){
                            finalImage = response
                            var formData = new FormData();
                            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
                            formData.append('logoImage', finalImage)
                            setTimeout(()=>{
                                $.ajax({
                                    url: __BASE__+'/school/logo',
                                    type: 'POST',
                                    data: formData,
                                    async: false,
                                    dataType:'JSON',
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    beforeSend: function(){
                                    },
                                    success: function(res){
                                        if(res){
                                            $('#hiddenSchoolLogo').val(res.attachment);
                                            $('#logoImg').removeAttr('name');

                                        }
                                    },
                                    error: function(xhr){
                                        window.requestForToken(xhr)
                                        console.log(xhr);
                                    }
                                });
                            },200)
                             $('.cr-image').attr('src','')
                             $('#exampleModal').modal('hide')
                        })
                    })
                }
            });

            FR.readAsDataURL( this.files[0] );

            const img = new Image();

            if(existingLogo === null){

                if($(this).val() === ''){
                    $('#hiddenSchoolLogo').val('');
                    $('#reset-logo-image').addClass('hidden');
                }

                if($(this).val() !== ''){
                    $('#reset-logo-image').removeClass('hidden');
                }
            }else{
                if($(this).val() === ''){
                    $('#hiddenSchoolLogo').val('');
                    $('#reset-logo-image-edit').addClass('hidden');
                }

                if($(this).val() !== ''){
                    $('#reset-logo-image-edit').removeClass('hidden');
                }
            }

        }else{
            addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${messages.only100KBImageIsAllowed}</strong></p>`);
            $('#hiddenSchoolLogo').val('');
            $('#logoImg').val('');
            $('#reset-logo-image').addClass('hidden');
        }

        if(existingLogo !== null){
            setTimeout(function(){
                $('#existing-logo').attr('src', 'data:image/*;base64,'+$("#hiddenSchoolLogo").val());
            }, 700);
            $('#imgupload').val('');
        }
    }
}

$('#imgupload').on('change', readFile);

$('#reset-logo-image').on('click', function(){
    $('#logoImg').val('');
    $('#hiddenSchoolLogo').val('');
    $(this).addClass('hidden');
});

$('#reset-logo-image-edit').on('click', function(){
    $('#upload-school-logo').addClass('hidden');
    $('#hiddenSchoolLogo').val(existingLogo);
    $('#existing-school-logo').removeClass('hidden');
    $('#existing-school-logo img').attr('src', 'data:image/*;base64,'+existingLogo);
});

$('#logoImg').on('change', readFile);

function getSchools(){
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getSchools',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {},
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let logo = response[i].schoolLogo !== null ? response[i].schoolLogo.indexOf('base64,') > 0 ? response[i].schoolLogo : 'data:image/*;base64,'+response[i].schoolLogo : '';
                    return_data.push({
                        'logo' : `<img src="${logo}" style="margin: auto" onerror="this.src='${__BASE__+'/assets/img/logo-2.png'}'" class="img-responsive rounded-circle" height="50" width="50" alt="${response[i].schoolName}" />`,
                        'name': response[i].schoolName,
                        'domain' : response[i].domain,
                        'website' : response[i].websiteURL,
                        'onboardingDate' : moment(response[i].onboardingDate).format('LL'),
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `
                        <div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__ACCESS__ ? `<button class="btn-warning btn btn-xs addContractBtn" data-toggle="tooltip" title="${messages.addContract}" data-json='${response[i].schoolID}'><i class="fa fa-dollar"></i></button>` : ''}
                        ${__ACCESS__ ? `<button class="btn-secondary btn btn-xs addCollectionBtn" data-toggle="tooltip" title="${messages.addCollection}" data-json='${response[i].schoolID}'><i class="fa fa-money"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-school" data-school-id='${response[i].schoolID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        <div>
                        `
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getSchools')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'logo'},
            {'data': 'name'},
            {'data': 'domain'},
            {'data': 'website'},
            {'data': 'onboardingDate'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });
}

$(document).on('click','.addContractBtn', function(){
    schoolID = $(this).attr('data-json')
    $('#chargesID').val('')
    $('#collectionID').val('')
    $('#unitPrice').val('')
    $('#startDate').val('')
    $('#endDate').val('')
    $('#chargesNotes').val('')
    $('#collectionRate').val('')
    $('#collectionNotes').val('')
    $('#taxRate').val('')
    $('#isActiveChargesInput').val(0)
    $('#isActiveCollectionInput').val(0)
    $('#isActiveCharges').prop('checked', false)
    $('#isActiveCollection').prop('checked', false)
    // $('#addContractModal').modal('show');
    // getAjaxSchoolBranches()
    getAjaxBranchGroup(schoolID)
    $.LoadingOverlay("show");
    getContractCharges()
})

$('#schools-form').on('submit', function(e){
    e.preventDefault();
    data = {}
    $.LoadingOverlay("show");
    var isEdited = $('#editHiddenField').val();
    var schoolLogoImage =  $('#logoImg').val();
    var hiddenSchoolLogo = $('#hiddenSchoolLogo').val();

    var formData = new FormData($(this)[0]);
    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

    if(schoolLogoImage  !== ''){
        $('#hiddenSchoolLogo').val(hiddenSchoolLogo);
        $('#logoImg').removeAttr('name');
        // submitSchoolData();
        // $.ajax({
        //     url: __BASE__+'/schools/logo',
        //     type: 'POST',
        //     data: formData,
        //     async: false,
        //     dataType:'JSON',
        //     contentType: false,
        //     cache: false,
        //     processData: false,
        //     beforeSend: function(){
        //         //disableButtonsToggle();
        //     },
        //     success: function(resp){
        //         if(resp){
        //             $('#hiddenSchoolLogo').val(resp.attachment);
        //             $('#logoImg').removeAttr('name');
        //             submitSchoolData();
        //         }
        //     },
        //     error: function(xhr){
        //         window.requestForToken(xhr)
        //         console.log(xhr);
        //         if(xhr.status === 422){
        //             let errors = xhr.responseJSON.errors;
        //             $.each(errors, function(key, val){
        //                 $('#add-form-errors').html(`<p class="text-danger"><strong>${val[0]}</strong></p>`);
        //             });
        //         }
        //         $.LoadingOverlay("hide");
        //     }
        // });
    }
    // else if(hiddenSchoolLogo !== '' &&  isEdited !== ''){
        submitSchoolData();
    // }
});
function submitSchoolData(){
    var schoolParameters = {}
    var formsData = $('#schools-form').serializeArray()
    formsData.forEach((value, index) => {
        if(value.name !== 'appHomework' && value.name !== 'appTimetable' && value.name !== 'appExamQuiz' && value.name !== 'appfeeVoucher' && value.name !== 'appAttendance' && value.name !== 'appMessage' && value.name !== 'appCommunicate' && value.name !== 'appEvents' && value.name !== 'weekStartDay' && value.name !== 'studentAttendanceGracePeriod' && value.name !== 'staffAttendanceGracePeriod' && value.name !== 'feePaymentAllowed' && value.name !== 'showAccNoOnVoucher' && value.name !== 'appForms' && value.name !== 'appVanTracking' && value.name !== 'studentArrivalEndTime' && value.name !== 'studentCutOffTime' && value.name !== 'staffArrivalEndTime' && value.name !== 'staffCutOffTime'){
            data[value.name] = value.value
        }
    })
    formsData.forEach((value, index) => {
        if(value.name === 'appHomework'|| value.name === 'appTimetable'|| value.name === 'appExamQuiz'|| value.name === 'appfeeVoucher'|| value.name === 'appAttendance'|| value.name === 'appMessage'|| value.name === 'appCommunicate'|| value.name === 'appEvents' || value.name === 'weekStartDay' || value.name === 'feePaymentAllowed' || value.name === 'showAccNoOnVoucher' || value.name === 'appVanTracking' || value.name === 'appForms' || value.name === 'studentArrivalEndTime' || value.name === 'studentAttendanceGracePeriod' || value.name === 'studentCutOffTime' || value.name === 'staffArrivalEndTime' || value.name === 'staffAttendanceGracePeriod' || value.name === 'staffCutOffTime'){
            if(value.value !== ''){
                schoolParameters[value.name] = value.value
            }
        }
    })
    data['schoolParameters'] = schoolParameters
    data = clean(data);
    showProgressBar(submitLoader);
    resetMessages();
    submitSchool();
    $.LoadingOverlay("hide");
}

$(document).on('click', '.delete-school', function(){
    var  selectedRoleID = $(this).attr('data-school-id');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheSchool+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteSchool(selectedRoleID);
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

function deleteSchool(selectedRoleID){
    $.ajax({
        url: herokuUrl+'/deleteSchool',
        method: 'POST',
        headers,
        data: {
            schoolID: selectedRoleID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.schoolHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getSchools();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteSchool')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$('#show-add-form-container').on('click', function(){
    isFormOpen = true;
    $('#schoolName').attr('disabled', false)
    $('#schoolDomain').attr('disabled', false)
    $('#schoolOwnerName').attr('disabled', false)
    $('#schoolWebsiteURL').attr('disabled', false)
    $('#logoImg').attr('disabled', false)
    $('#activeField1').attr('disabled', false)
    $('#activeField0').attr('disabled', false)
    $('#legalEntityName').attr('disabled', false)
    $('#legalEntityAddress').attr('disabled', false)
    $('#legalEntityPerson').attr('disabled', false)
    $('#legalEntityContactNo').attr('disabled', false)
    $('#feePaymentAllowed').attr('disabled', false)
    $('#filter-week-day').attr('disabled', false)
    $('#studentAttendanceGracePeriod').attr('disabled', false)
    $('#staffAttendanceGracePeriod').attr('disabled', false)
    $('#submit-add-form-container').attr('disabled', false)
    $('#schoolLabelId').addClass('hidden')
})

$('#hide-add-form-container').on('click',function(){
    getSchools()
    isFormOpen = false;

})

$(document).on('blur','#schoolDomain', function(){
    if(isFormOpen === true){
        getDBSchoolsList();
        isFormOpen = true;
    }
})

function getDBSchoolsList(){
    let domainName = $('#schoolDomain').val();
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl+'/getSchools',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data : {},
            dataSrc: function(res){
                response = res.response;
                for(var i = 0; i < response.length; i++){
                    if(response[i].domain === domainName){

                        $('#schoolName').attr('disabled', true)
                        $('#schoolDomain').attr('disabled', true)
                        $('#schoolOwnerName').attr('disabled', true)
                        $('#schoolWebsiteURL').attr('disabled', true)
                        $('#logoImg').attr('disabled', true)
                        $('#activeField1').attr('disabled', true)
                        $('#activeField0').attr('disabled', true)
                        $('#legalEntityName').attr('disabled', true)
                        $('#legalEntityAddress').attr('disabled', true)
                        $('#legalEntityPerson').attr('disabled', true)
                        $('#legalEntityContactNo').attr('disabled', true)
                        $('#feePaymentAllowed').attr('disabled', true)
                        $('#filter-week-day').attr('disabled', true)
                        $('#studentAttendanceGracePeriod').attr('disabled', true)
                        $('#staffAttendanceGracePeriod').attr('disabled', true)
                        $('#submit-add-form-container').attr('disabled', true)
                        $('#schoolLabelId').removeClass('hidden')

                        $('#schoolName').val(response[i].schoolName)
                        $('#schoolDomain').val(response[i].domain)
                        $('#schoolOwnerName').val(response[i].owner)
                        $('#schoolWebsiteURL').val(response[i].websiteURL)
                        $('#logoImg').val(response[i].schoolLogo)
                    }else{

                    }
                }
            },
            error: function(xhr){
                invokedFunctionArray.push('getDBUserList')
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 404){
                   $('.dataTables_empty').text(messages.noDataAvailable);
                }
                if (xhr.status === 500) {
                }
            }
        },
    });
}

$('#logoImg').on('click', function(){
    $('#hiddenSchoolLogo').val('')
})

$(document).on('click', '#isActiveCharges', function(){
    if($(this).prop('checked')){
        $('#isActiveChargesInput').val(1)
    }else{
        $('#isActiveChargesInput').val(0)
    }
})

$(document).on('click', '#isActiveCollection', function(){
    if($(this).prop('checked')){
        $('#isActiveCollectionInput').val(1)
    }else{
        $('#isActiveCollectionInput').val(0)
    }
})


$('#contract-form').on('submit', function(e){
    e.preventDefault();

    var branchGroupID = $('#filter-branch-group').val()
    if(branchGroupID.length === 0){
        $('#hiddenError').html(messages.pleaseSelectBranchGroupFirst)
        $('#hiddenError').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenError').addClass('hidden')
        },2000)
        return false;
    }
    if($('#unitPrice').val() === ''){
        $('#hiddenError').html(messages.priceStudentFieldIsRequired)
        $('#hiddenError').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenError').addClass('hidden')
        },2000)
        return false;
    }
    if($('#startDate').val() === ''){
        $('#hiddenError').html(messages.startDateFieldIsRequired)
        $('#hiddenError').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenError').addClass('hidden')
        },2000)
        return false;
    }
    $('.exportLoader').removeClass('hidden')
    data = {}
    bulkArray = []
        var formData = $('#contract-form').serializeArray();
        formData.forEach(function(value, index){
            data[value.name] = value.value
        })
        data['userUUID'] = user.userUUID
        data['schoolID'] = schoolID
        data['branchGroupID'] = branchGroupID
        bulkArray.push(clean(data))
    setContractGroupCharges(clean(data))
});

function setContractGroupCharges($data){
    $.ajax({
        url: herokuUrl+'/setContractGroupCharges',
        method: 'POST',
        headers,
        data: data,
        beforeSend: function(){
        },
        success: function(res){
            $.LoadingOverlay("hide");
            if(res.response === 'success'){
                $('.exportLoader').addClass('hidden')
                $('#addContractModal').modal('hide');
                getSchools();
                swal(
                    messages.contractHasBeenSubmittedSuccessfully,
                    messages.success,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
                }
        },
        error: function(xhr){
            invokedFunctionArray.push('setContractGroupCharges')
            window.requestForToken(xhr)
            $.LoadingOverlay("hide");
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage !== 'undefined' ? xhr.responseJSON.response.userMessage : messages.sorryUnableToProcessPleaseTryAgain}</strong></p>`);
            }
            hideProgressBar(submitLoader);

        }
    });
}

function getContractCharges(){
    $.ajax({
        url: herokuUrl+'/getContractGroupCharges',
        method: 'POST',
        headers,
        data:{
            schoolID:schoolID,
        },
        beforeSend: function(){
            $('#chargesID').val('')
            $('#unitPrice').val('')
            $('#startDate').val('')
            $('#endDate').val('')
            $('#chargesNotes').val('')
            $('#taxRate').val('')
            $('#isActiveChargesInput').val(0)
            $('#isActiveCharges').prop('checked', false)
        },
        success: function(res){
            response = res.response[0]

            if(response){
                chargesID           = response.chargesID
                chargesNotes        = response.notes
                currencyCode        = response.currencyCode
                endDate             = response.endDate
                isActiveCharges     = response.isActive
                startDate           = response.startDate
                unitPrice           = response.unitPrice
                billFrequency       = response.billFrequency
                branchID            = response.branchID
                taxRate             = response.taxRate
                branchGroupID       = response.branchGroupID
                $('#chargesID').val(chargesID)
                $('#currencyCode').find('option[value='+currencyCode+']').attr('selected','selected')
                $('#unitPrice').val(unitPrice)
                $('#billFrequency').find('option[value='+billFrequency+']').attr('selected','selected')
                $('#startDate').val(startDate)
                $('#endDate').val(endDate)
                $('#chargesNotes').val(chargesNotes)
                $('#taxRate').val(taxRate)
                $("#filter-branches").multiselect('deselectAll', true)
                setTimeout(() => {
                    $("#filter-branch-group").find('option[value="'+branchGroupID+'"]').prop('selected', true);

                    $('#filter-branch-group').multiselect('rebuild');
                }, 1500);

                if(isActiveCharges == '1'){
                    $('#isActiveCharges').prop('checked', true)
                    $('#isActiveChargesInput').val(1)
                }else{
                    $('#isActiveCharges').prop('checked', false)
                    $('#isActiveChargesInput').val(0)
                }
            }
            $.LoadingOverlay("hide");
            $('#addContractModal').modal('show');
        },
        error: function(xhr){
            invokedFunctionArray.push('getContractCharges')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage !== 'undefined' ? xhr.responseJSON.response.userMessage : 'Sorry unable to process. Please try again !'}</strong></p>`);
            }
        }
    });
}

$(document).on('click','.addCollectionBtn', function(){
    schoolID = $(this).attr('data-json')
    $('#chargesID').val('')
    $('#collectionID').val('')
    $('#unitPrice').val('')
    $('#startDate').val('')
    $('#endDate').val('')
    $('#chargesNotes').val('')
    $('#collectionRate').val('')
    $('#collectionNotes').val('')
    $('#taxRate').val('')
    $('#isActiveChargesInput').val(0)
    $('#isActiveCollectionInput').val(0)
    $('#isActiveCharges').prop('checked', false)
    $('#isActiveCollection').prop('checked', false)
    // $('#addContractModal').modal('show');
    // getAjaxSchoolBranches()
    $.LoadingOverlay("show");
    getAjaxSchoolBranches(schoolID)
    getContractCollection()
})

$('#collection-form').on('submit', function(e){
    e.preventDefault();
    var branchID = $('#filter-branches').val()
    if(branchID.length === 0){
        $('#hiddenErrorCollection').html('Please select at least 1 Branch...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    if($('#collectionRate').val() === ''){
        $('#hiddenErrorCollection').html('Rate field is required...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    if($('#collectionNotes').val() === ''){
        $('#hiddenErrorCollection').html('Note field is required...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    if($('#collectionAcBank').val() === ''){
        $('#hiddenErrorCollection').html('Bank Name field is required...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    if($('#collectionAcBranch').val() === ''){
        $('#hiddenErrorCollection').html('Account Branch field is required...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    if($('#collectionAcTitle').val() === ''){
        $('#hiddenErrorCollection').html('Account Title field is required...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    if($('#collectionAcNumber').val() === ''){
        $('#hiddenErrorCollection').html('Account# field is required...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    if($('#startDateCollection').val() === ''){
        $('#hiddenErrorCollection').html('Start Date field is required...')
        $('#hiddenErrorCollection').removeClass('hidden')
        setTimeout(()=>{
            $('#hiddenErrorCollection').addClass('hidden')
        },2000)
        return false;
    }

    $('.exportLoader').removeClass('hidden')
    data = {}
    bulkArray = []
    schoolBranchID = [];
        var formData = $('#collection-form').serializeArray();
        formData.forEach(function(value, index){
            if(value.name === 'branchID' && value.value !== ''){
                schoolBranchID.push(value.value)
            }else{
                data[value.name] = value.value
            }
        })
        data['userUUID'] = user.userUUID
        data['schoolID'] = schoolID
        if(schoolBranchID && schoolBranchID.length > 0){
            schoolBranchID.forEach(function(branchValue, branchIndex){
                data['branchID'] = branchValue
                let finalObj = {}
                finalObj = createObjectFromAnotherObject(data)
                bulkArray.push(clean(finalObj))
            })
        }else{
            bulkArray.push(clean(data))
        }
    setContractCollectionArray()
});

function createObjectFromAnotherObject(oldObject){
    var newObject = {};
    for (var propName in oldObject) {
        newObject[propName] = oldObject[propName];
    }
    return newObject;
}

function getContractCollection(){
    $.LoadingOverlay("hide");
            $('#addCollectionModal').modal('show');
            // return
    $.ajax({
        url: herokuUrl+'/getContractCollection',
        method: 'POST',
        headers,
        data:{
            schoolID:schoolID,
        },
        beforeSend: function(){
            $('#collectionID').val('')
            $('#startDate').val('')
            $('#collectionRate').val('')
            $('#collectionNotes').val('')
            $('#isActiveCollectionInput').val(0)
            $('#isActiveCollection').prop('checked', false)
        },
        success: function(res){
            response = res.response[0]
            if(response){
                collectionID        = response.collectionID
                collectionNotes     = response.notes
                collectionRate      = response.rate
                collectionType      = response.collectionType
                isActiveCollection  = response.isActive
                startDate           = response.startDate
                branchID            = response.branchID
                branchIDs = branchID.split(',')

                $("#filter-branches").multiselect('deselectAll', true)
                setTimeout(() => {
                    branchIDs.forEach(function(value, index){
                        $("#filter-branches").find('option[value="'+value+'"]').prop('selected', true);
                    })
                    $('#filter-branches').multiselect('rebuild');
                }, 1500);
                $('#collectionID').val(collectionID)
                $('#startDate').val(startDate)
                $('#collectionType').find('option[value='+collectionType+']').attr('selected','selected')
                $('#collectionRate').val(collectionRate)
                $('#collectionNotes').val(collectionNotes)
                if(isActiveCollection == '1'){
                    $('#isActiveCollection').prop('checked', true)
                    $('#isActiveCollectionInput').val(1)
                }else{
                    $('#isActiveCollection').prop('checked', false)
                    $('#isActiveCollectionInput').val(0)
                }
            }
            $.LoadingOverlay("hide");
            $('#addCollectionModal').modal('show');
        },
        error: function(xhr){
            invokedFunctionArray.push('getContractCollection')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage !== 'undefined' ? xhr.responseJSON.response.userMessage : messages.sorryUnableToProcessPleaseTryAgain}</strong></p>`);
            }
        }
    });
}

function setContractCollectionArray(){
    $.ajax({
        url: herokuUrl+'/setContractCollectionArray',
        method: 'POST',
        headers,
        data:{'array':bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
        },
        success: function(res){
            $.LoadingOverlay("hide");
            if(res.response === 'success'){
                $('.exportLoader').addClass('hidden')
                $('#addCollectionModal').modal('hide');
                getSchools();
                swal(
                    messages.contractCollectionHasBeenSubmittedSuccessfully,
                    messages.success,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
                }
        },
        error: function(xhr){
            invokedFunctionArray.push('setContractCollectionArray')
            window.requestForToken(xhr)
            $.LoadingOverlay("hide");
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage !== 'undefined' ? xhr.responseJSON.response.userMessage : 'Sorry unable to process. Please try again !'}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
