$(document).ready(function(){
    getAjaxCountries();
    if(cityID !== ''){
        getAreas();
    }
})

function submitArea(){
    $.ajax({
        url: herokuUrl+'/setAreas',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                $('form#areas-form').trigger('reset');
                getAreas();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
            $('form#areas-form').trigger('reset');
        },
        error: function(xhr){
            invokedFunctionArray.push('submitArea');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getAreas(){
    if(cityID == ''){
        data = {}
    }else{
        data = { cityID: cityID };
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getAreas',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'areaName': response[i].areaName,
                        'cityName' : response[i].cityName,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${JSON.stringify(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getAreas');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'areaName'},
            {'data': 'cityName'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$('#areas-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#areas-form').serialize();
    showProgressBar(submitLoader);
    resetMessages();
    submitArea();
});
