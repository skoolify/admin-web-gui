$(document).ready(function(){
    getAjaxCountries();
    getAjaxCities(countryID);
    getAjaxAreas();
    getAjaxSchools();
    // getSchoolBranches();
})

function submitSchoolBranches(){
    $.ajax({
        url: herokuUrl+'/setSchoolBranches',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                areaID = '';
                $('#filter-areas').val('');
                $('#filter-areas').multiselect('rebuild');
                $('form#school-branches-form').trigger('reset');
                getSchoolBranches();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitSchoolBranches')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getSchoolBranches(){
    let data = {};
    if(schoolID !== ''){
        data.schoolID = schoolID
    }
    if(areaID !== ''){
        data.areaID = areaID
    }
    if(tableData){
        tableData.destroy()
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getSchoolBranches',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'code' : response[i].branchCode,
                        'name': response[i].branchName,
                        'city' : response[i].cityName,
                        'area' : response[i].areaName,
                        'principalName' : response[i].principleName,
                        'contact1' : response[i].contact1,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `
                            <div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-branch" data-branch-id='${response[i].branchID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        <div>
                        `
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getSchoolBranches')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'code'},
            {'data': 'name'},
            {'data': 'city'},
            {'data': 'area'},
            {'data': 'principalName'},
            {'data': 'contact1'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$('#school-branches-form').on('submit', function(e){
    e.preventDefault();
    if(cityID === ''){
        swal(
            messages.cityIsRequired,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }else if(areaID === ''){
        swal(
            messages.areaIsRequired,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }else{
        data = $('#filter-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '')+'&'+$('#school-branches-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
        showProgressBar(submitLoader);
        resetMessages();
        submitSchoolBranches();
    }

});

$(document).on('click', '.delete-branch', function(){
    var  selectedBranchID = $(this).attr('data-branch-id');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSur+' <br/> '+messages.youWantToDeleteTheBranch+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteBranch(selectedBranchID);
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

function deleteBranch(selectedBranchID){
    $.ajax({
        url: herokuUrl+'/deleteSchoolBranch',
        method: 'POST',
        headers,
        data: {
            branchID: selectedBranchID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.branchHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getSchoolBranches();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteBranch')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('click','.isActive',function(){
    if($(this).prop('checked')){
        $('#isActiveInput').val(1)
    }else{
        $('#isActiveInput').val(0)
    }
})
