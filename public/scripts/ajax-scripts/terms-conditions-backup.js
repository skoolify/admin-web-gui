$(document).ready(function(){
    getTermAndCondition();
    $('#summernote').summernote({
        height: 200,
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['table', ['table']]
        ]
    });

    // $('#arabicSummernote').summernote({
    //     height: 200,
    //     lang: 'ar-AR',
    //     justifyRight: 'Set right align',
    //     toolbar: [
    //       ['style', ['bold', 'italic', 'underline', 'clear']],
    //       ['font', ['strikethrough', 'superscript', 'subscript']],
    //       ['fontsize', ['fontsize']],
    //       ['color', ['color']],
    //       ['para', ['ul', 'ol', 'paragraph']],
    //       ['height', ['height']],
    //       ['table', ['table']]
    //     ]
    // });

    // var editor = new Jodit('#arabicSummernote', {
    //     direction: 'rtl',
    //     language: 'ar',
    //     i18n: 'ar'
    // })

    new FroalaEditor('#arabicSummernote', {
        direction: 'rtl',
        codeBeautifierOptions: {
            end_with_newline: true,
            indent_inner_html: true,
            extra_liners: "['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre', 'ul', 'ol', 'table', 'dl']",
            brace_style: 'expand',
            indent_char: ' ',
            indent_size: 4,
            wrap_line_length: 0
        }
      })

    // $('#arabicSummernote').each(function () {
    //     var editor = new Jodit(this);
    //     editor.value = '<p>start</p>';
    //     editor.direction = 'rtl';
    // });

    // $('#arabicSummernote').summernote({
    //     height: 200,
    //     focus: true,
    //     toolbar: [
    //         ['style', ['style']],
    //         ['style', ['bold', 'italic', 'underline', 'clear']],
    //         ['fontname', ['fontname']],
    //         ['color', ['color']],
    //         ['para', ['ul', 'ol', 'paragraph']],
    //         ['insert', ['ltr', 'rtl']],
    //         ['insert', ['table']],
    //         ['insert', ['link', 'picture', 'video', 'hr']],
    //         ['view', ['fullscreen', 'codeview', 'help']]
    //     ]
    // });
});
function submitTermAndCondition(){
    $.ajax({
        url: herokuUrl+'/setTnC',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);
                getTermAndCondition();
                $('#term-condition-form').trigger('reset');
                $('.note-editable').html('');
            }
            $('.submitLoader').addClass('hidden')
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitTermAndCondition')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getTermAndCondition(){
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
            'ajax': {
            url: herokuUrl+'/getTnC',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {},
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let en = handleApostropheWork(response[i].tncText);
                    let ar = handleApostropheWork(response[i].tncText_ar);
                    delete response[i].tncText;
                    delete response[i].tncText_ar;
                    return_data.push({
                        'sNo' : i+1,
                        'version': response[i].version ? response[i].version : '-',
                        'insertionDate': moment(response[i].insertionDate).format('DD-MM-YYYY hh:mm A'),
                        'updateDate': moment(response[i].updateDate).format('DD-MM-YYYY hh:mm A'),
                        'isPublished' : `<label class="label label-sm label-${response[i].isPublished ? 'success' : 'danger'}"> ${response[i].isPublished ? 'Published' : 'UnPublished'}</label>`,
                        'addedBy': user.fullName,
                        'action' : `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-tnc='${en}' data-tnc-ar='${ar}' data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getTermAndCondition')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'version'},
            {'data': 'insertionDate'},
            {'data': 'updateDate'},
            {'data': 'isPublished'},
            {'data': 'addedBy'},
            {'data': 'action'}
        ]
    });
}

$('#term-condition-form').on('submit', function(e){
    e.preventDefault();
    $('.submitLoader').removeClass('hidden')
    value = $(this).serialize()
    setTimeout(() => {
        data = value.replace(/[^&]+=\.?(?:&|$)/g, '')+'&userID='+user.userID;
    }, 100);
    showProgressBar(submitLoader);
    resetMessages();
    setTimeout(() => {
        submitTermAndCondition();
    }, 500);
});

