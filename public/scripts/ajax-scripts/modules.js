$(document).ready(function(){
    getModules();
})

function submitModule(){
    $.ajax({
        url: herokuUrl+'/setModule',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                getModules(classID, studentID, subjectID);
                $('form#modules-form').trigger('reset');
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitModule')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getModules(){
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getModules',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                studentID : studentID,
                subjectID : subjectID,
                classID: classID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                $.each(response, function(index, value){
                    if(value.isParent==1)
                    {
                    $('#parentModule').append(`<option value="${value.moduleID}">${value.moduleName}</option>`);
                    }
                });
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'moduleName': response[i].moduleName,
                        'moduleDescription' : response[i].moduleDescription,
                        'moduleURL' : response[i].moduleURL,
                        'isParent' : `<label class="label label-sm label-${response[i].isParent == 1 ? 'success' : 'danger'}"> ${ response[i].isParent == 1 ? messages.yes : messages.no}</label>`,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive == 1 ? 'success' : 'danger'}"> ${ response[i].isActive == 1 ? messages.yes : messages.no}</label>`,
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}`
                    })
                }

                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getModules')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'moduleName'},
            {'data': 'moduleDescription'},
            {'data': 'moduleURL'},
            {'data': 'isParent'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$('#optionsRadios1').on('click', function(){
    $('#optionsRadios2').prop('checked',false)
    let value = $(this).val();
    $('#isParent').val(value)
})

$('#optionsRadios2').on('click', function(){
    $('#optionsRadios1').prop('checked',false)
    let value = $(this).val();
    $('#isParent').val(value)
})

$('#modules-form').on('submit', function(e){
    if($('#isParent').val() == ''){
        swal(
            messages.pleaseSelectTheRadioButton,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    if($('#isParent').val() == 0 && $('#parentModule').val() == 0 ){
        swal(
            messages.pleaseSelectParentModuleFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
    e.preventDefault();
    var obj = {}
    obj['parentModuleID'] = "0";
    var formData = $('#modules-form').serializeArray();
    formData.forEach(function(value,index){
        obj[value.name] = value.value;
    })
    data = clean(obj);
    showProgressBar(submitLoader);
    resetMessages();
    submitModule();
});

$(".parent-check").on('change',function(){
    var opt = $(this).val();
    if(opt === '1'){
        $("#parentModule").attr("disabled",true);
        $("#parentModule").val($("#parentModule option:first").val());
    }else{
        $("#parentModule").removeAttr("disabled");
    }

})
