$(document).ready(function(){
    getContract();
    $('#contractsummernote').summernote({
        height: 200,
        toolbar: [
            // ['style', ['style']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture']],
            // ['view', ['fullscreen', 'codeview']],
            // ['help', ['help']]
        ],
      });
      $('#chargessummernote').summernote({
            height: 200,
            toolbar: [
                // ['style', ['style']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                // ['view', ['fullscreen', 'codeview']],
                // ['help', ['help']]
            ],
      });
      $('#collectionsummernote').summernote({
            height: 200,
            toolbar: [
                // ['style', ['style']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                // ['view', ['fullscreen', 'codeview']],
                // ['help', ['help']]
            ],
      });
    // $('#collectionsummernote').summernote({
    //     height: 200,
    //     toolbar: [
    //         // ['style', ['style']],
    //         ['font', ['bold', 'italic', 'underline', 'clear']],
    //         ['fontname', ['fontname']],
    //         ['color', ['color']],
    //         ['para', ['ul', 'ol', 'paragraph']],
    //         ['height', ['height']],
    //         ['table', ['table']],
    //         ['insert', ['link', 'picture', 'hr']],
    //         ['view', ['fullscreen', 'codeview']],
    //         ['help', ['help']]
    //     ],
    // });
});

var isExist = false;
var check = null;

function getContract(){
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
            'ajax': {
            url: herokuUrl+'/getContract',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {},
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    if(response[i].isPublished == '1'){
                        isExist = true;
                    }

                    let t = JSON.stringify(response[i].contractText);
                    if(response[i].contractText !== 'undefined'){
                        delete response[i].contractText;
                    }
                    let chargesText = JSON.stringify(response[i].chargesText);
                    if(response[i].chargesText !== 'undefined'){
                        delete response[i].chargesText;
                    }
                    let collectionText = JSON.stringify(response[i].collectionText);
                    if(response[i].collectionText !== 'undefined'){
                        delete response[i].collectionText;
                    }
                    var insertionDate = moment.utc(response[i].insertionDate);
                    insertionDate = insertionDate.local().format('DD-MMM-YYYY hh:mm A')
                    insertionDate = insertionDate && insertionDate != 'Invalid date' ? insertionDate : '-'

                    var updatedDate = moment.utc(response[i].updateDate);
                    updatedDate = updatedDate.local().format('DD-MMM-YYYY hh:mm A')

                    updatedDate = updatedDate && updatedDate != 'Invalid date' ? updatedDate : '-'
                    return_data.push({
                        'sNo' : i+1,
                        'version': response[i].version ? response[i].version : '-',
                        'insertionDate': insertionDate,
                        'updateDate': updatedDate,
                        'isPublished' : `<label class="label label-sm label-${response[i].isPublished ? 'success' : 'danger'}"> ${response[i].isPublished ? 'Published' : 'UnPublished'}</label>`,
                        'addedBy': user.fullName,
                        'action' : `<div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-contract='${t}' data-charges='${chargesText}' data-collection='${collectionText}' data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-contract" data-contract-id='${response[i].contractID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getContract');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'version'},
            {'data': 'insertionDate'},
            {'data': 'updateDate'},
            {'data': 'isPublished'},
            {'data': 'addedBy'},
            {'data': 'action'}
        ]
    });
}

// $(document).on('click','.list-record-edit', function(){
//     var json = $(this).attr('data-contract');
//     console.log(json)
//     json = json.slice(1,-1);
//     $(".contract-summernote").summernote('code', atob(json.trim()));
// })

$(document).on('click', '.delete-contract', function(){
    contractID = $(this).attr('data-contract-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteThisContract+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteContract();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });
})

function deleteContract(){
    $.ajax({
        url: herokuUrl+'/deleteContract',
        method: 'POST',
        headers,
        data: {
            contractID: contractID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.contractHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                contractID = '';
                getContract();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteContract');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

function submitContract(){
    $.ajax({
        url: herokuUrl+'/setContract',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                isExist = false;
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                getContract();
                $('#contract-form').trigger('reset');
                $('.note-editable').html('');
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitContract');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$('#contract-form').on('submit', function(e){
    e.preventDefault();
    if($('input[name="isPublished"]').prop('checked')){

        check = 1;
    }else{
        check = 0;
    }
    if(isExist == true && check === 0){
        swal(
            messages.pleaseMakeSureAtleastOneContractInPublishedState,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    // data = $(this).serialize()+'&userID='+user.userID;
    var formData = $(this).serializeArray();
    var bulkArray = [];
    let obj = {}
    obj['userID'] = user.userID
    formData.forEach((value, index)=>{
        if(value.name === 'contractText' || value.name === 'chargesText' || value.name === 'collectionText'){
            obj[value.name] = btoa(unescape(encodeURIComponent(value.value)));
        }else{
            obj[value.name] = value.value;
        }
    })
    bulkArray.push(clean(obj))
    data = bulkArray[0]
    showProgressBar(submitLoader);
    resetMessages();
    submitContract();
});
