$(document).ready(function(){
    getAjaxSchools();
    // console.log(user)
    if(user.isAdmin !== 1){
        getStudentTags()
    }
})

$('#isActive').on('click', function(){
    if($(this).prop('checked')){
        $('#isActiveInput').val(1)
    }else{
        $('#isActiveInput').val(0)
    }
})

function submitTag(){
    if($('#filter-branches').val() !== ''){
        branchID = $('#filter-branches').val()
    }else{
        swal(
            messages.pleaseSelectBranch,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    $.ajax({
        url: herokuUrl+'/setStudentNotesTag',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                // tableData.destroy();
                getStudentTags();
                $('#tag-form').trigger('reset');
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitTag')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getStudentTags(){
    var data = {}
    if($('#filter-branches').val() !== undefined && $('#filter-branches').val() !== ''){
        data.branchID = $('#filter-branches').val()
    }else if(branchID != ''){
        data.branchID = user.branchID;
    }
    tableData = $('#tagTable').DataTable({
        "bDestroy": true,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/getStudentTags',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                var count = 1;

                for(var i = 0; i < response.length; i++){
                    var insertionDate = moment.utc(response[i].insertionDate);
                    insertionDate = insertionDate.local().format('DD-MMM-YYYY');

                    return_data.push({
                        'sNo' : count++,
                        'TagName': response[i].tagText,
                        'insertionDate':  insertionDate,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive && response[i].isActive === 1 ? 'success' : 'danger'}"> ${response[i].isActive && response[i].isActive === 1 ? messages.active : messages.inActive}</label>`,
                        'action' :
                        `<div class="btn-group">
                            ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                            ${__DEL__ ?
                            `<button class="btn-danger btn btn-xs delete-tag" data-tag-id='${response[i].tagUUID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>`
                            : ''}
                        </div>`
                    })

                }
                $.LoadingOverlay("hide");
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudentTags')
                window.requestForToken(xhr)
                if(xhr.status === 404 || xhr.status === 400){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                $.LoadingOverlay("hide");
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'TagName'},
            {'data': 'insertionDate'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });
}

$(document).on('click', '.delete-tag', function(){
    tagUUID = $(this).attr('data-tag-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+' <br/> '+messages.youWantToDeleteThisTag+'</br>',
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteStudentTag();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });
})

function deleteStudentTag(){
    $.ajax({
        url: herokuUrl+'/deleteStudentNotesTag',
        method: 'POST',
        headers,
        data: {
            tagUUID: tagUUID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.tagHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                tagUUID = '';
                if(tableData){
                    tableData.destroy();
                }
                getStudentTags();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteStudentTag');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$('#tag-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#tag-form').serialize()+'&userID='+user.userID;
    showProgressBar(submitLoader);
    submitTag();
});
