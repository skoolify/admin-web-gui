$(document).ready(function(){
    let filterSchools = $('#filter-schools');
    if(filterSchools.length){
        getAjaxSchools();
        if(user.isAdmin == 1){
            schoolID = $('#filter-schools').val();
        }
        if(schoolID !== ''){
            getCalendarDays();
        }
    }else{
        if(schoolID !== ''){
            getCalendarDays();
        }
    }
})

var bulkArray = [];

function submitCalendarDay(){
    $.ajax({
        url: herokuUrl+'/setCalendarDaysArray',
        method: 'POST',
        headers,
        data:{'array':bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#calendar-days-form').trigger('reset');
                getCalendarDays();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitCalendarDay')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 406){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.reason}</strong></p>`);
            }

            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('change','#filter-branches', function(){
    getCalendarDays();
})

function getCalendarDays(){
    data = {}
    dateFromVal = $('#filter-date-from').val();
    dateToVal = $('#filter-date-to').val();
    branchID = $('#filter-branches').val();
    if(schoolID !== ''){
        data.schoolID = schoolID
    }
    if(branchID !== '' && branchID !== undefined){
        data.branchID = branchID
    }else{
        data.branchID = user.branchID
    }
    // if(dateFromVal !== ''){
    //     data.dateFrom = dateFromVal
    // }
    // if(dateToVal !== ''){
    //     data.dateTo = dateToVal
    // }

    if(tableData){
        tableData.destroy();
    }

    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            // url: herokuUrl+'/getCalendarDays',
            url: herokuUrl+'/getCalendarList',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'holidayName': response[i].holidayName,
                        'calendarDate' : moment(response[i].calendarDate).format('DD-MM-YYYY'),
                        'holiday': response[i].isHoliday === 1 ? `<label class="label label-success label-sm">Yes</label>` : `<label class="label label-danger label-sm">No</label>` ,
                        'action' :  `
                        <div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-calendar" data-calendar-id='${response[i].calendarID}' data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></button>
                        ` : ''} </div>
                        `
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getCalendarDays');
                window.requestForToken(xhr)
                if(xhr.status === 404 || xhr.status === 400){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'holidayName'},
            {'data': 'calendarDate'},
            {'data': 'holiday'},
            {'data': 'action'}
        ]
    });

}

$(document).on('click', '.delete-calendar', function(){
    calendarID = $(this).attr('data-calendar-id');
   $.confirm({
       title: messages.confirm,
       content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheCalendar+' </br>',
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteCalendar();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteCalendar(){
    $.ajax({
        url: herokuUrl+'/deleteCalendarDays',
        method: 'POST',
        headers,
        data: {
            calendarID: calendarID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.deletedMessage,
                    messages.calendarHasBeenDeleted,
                    'success',
                    {
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                );
                getCalendarDays();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteCalendar');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$('#calender-filter-date-from').on('change', function(){
    let date = $(this).val()
    if(!$('#multipleDays').prop('checked')){
        $('#calender-filter-date-to').val(date)
    }
})

$('#calendar-days-form').on('submit', function(e){
    e.preventDefault();
    bulkArray = [];
    var calendarID = $('#editHiddenField').val();
    var startDate = $('#calender-filter-date-from').val();
    var endDate = $('#calender-filter-date-to').val();
    var formData = $(this).serializeArray();
    if(schoolID === '' && schoolID == 'null'){
        schoolID = $('#filter-schools').val()
    }

    if(schoolID === ''){
        swal(
            messages.pleaseSelectSchool,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#holidayName').val() === ''){
        swal(
            messages.eventNameIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if(calendarID === ''){
        var diff = Number(moment(endDate).diff(moment(startDate),'days') + 1);
        if(typeof diff !== 'undefined' && Number(diff) > 0 ){
            for(var i = 1; i <= diff ; i++){
                var obj = {}
                obj['calendarDate'] =  i == 1 ? startDate : moment(startDate, "YYYY-MM-DD").add(i - 1,'days').format('YYYY-MM-DD');
                obj['schoolID'] = schoolID;
                if(branchID !== '' && branchID !== 'undefined' && branchID !== undefined){
                    obj['branchID'] = branchID;
                }else{
                    obj['branchID'] = user.branchID
                }
                formData.forEach(function(value, index){
                    if(value.name === 'holidayName'){
                        obj[value.name] = value.value;
                    }
                });
                bulkArray.push(obj);
            }
        }else{
            Swal.fire({
                type: 'error',
                title: messages.oops,
                text: messages.fromDateMustBeLessThanToDate,
            })
            return false;
        }
    }else{
            var obj = {}
            if(calendarID !== ''){
                obj['calendarID'] = calendarID;
            }
            obj['schoolID'] = schoolID;
            if(branchID !== '' && branchID !== 'undefined' && branchID !== undefined){
                obj['branchID'] = branchID;
            }else{
                obj['branchID'] = user.branchID
            }
            formData.forEach(function(value, index){
                if(value.name === 'holidayName' || value.name === 'calendarDate'){
                    obj[value.name] = value.value;
                }
            });
            bulkArray.push(obj);
    }
    if(bulkArray.length){
        showProgressBar(submitLoader);
        resetMessages();
        submitCalendarDay();
    }
});

$('#multipleDays').on('click', function(){
    if($('#multipleDays').prop('checked')){
        $('.endDateDiv').removeClass('hidden')
        $('#calender-filter-date-to').val(__CURRENTDATE__);
    }else{
        let value = $('#calender-filter-date-from').val()
        $('#calender-filter-date-to').val(value)
        $('.endDateDiv').addClass('hidden')
    }
})

