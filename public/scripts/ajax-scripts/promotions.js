var overallList = [];
var moveClassLevels =  $('#filter-move-classLevels');
var moveableStudentsArray = [];
var promotionForm = $('#student-promotion-form');
var errorMsgShow = $('#add-form-errors-tab');
$(document).ready(function(){
    getAjaxSchools();
});

// $(document).on('change','#filter-classes',function(){
//     if(classID == ''){

//     }
// });
// $(document).on('change','#filter-exam-term',function(){
//     if(classID == ''){
//         Swal.fire({
//             type: 'error',
//             title: 'Oops...',
//             text: 'Please select Section first!',
//           });
//           return false;
//     }
//     if(examTermID !== ''){
//         getOverallResult();
//         setTimeout(function(){
//             iterateStudentListTableForMove();
//         },1500)
//     }
// });

function getOverallResult() {
    // data.examsTermID = examTermID;
    if(classLevelID !== ''){
        data.classLevelID = classLevelID;
    }else{
        data.classLevelID = $('#filter-classLevels').val();
    }
    if(classID != ''){
        data.classID = classID;
    }else{
        data.classID = $('#filter-classes').val();
    }
    if (overAllExamTermTableData) {
        overAllExamTermTableData.destroy();
    }
    overAllExamTermTableData = $('#promotion-result-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax':{
            url: herokuUrl + '/getClassActiveStudents',
            method: 'POST',
            headers,
            data: data,
            dataSrc: function (res) {
                response = parseResponse(res);
                var return_data = new Array();
                overallList = [];
                for (let i = 0; i < response.length; i++) {
                    const element = response[i];
                    let overallFlag =  `<label class="label label-sm overall-flag label-${element.resultStatus == 'P' ? 'success' : 'danger' }"> ${element.resultStatus == 'P' ? messages.pass : messages.fail } </label>`;
                    overallList.push(element);
                    return_data.push({
                        'rollNo': element.rollNo,
                        'studentName':  element.studentName,
                        'classSection': element.classAlias+'-'+element.classSection,
                        // 'overallPercentage': element.overallPercentage + ' %',
                        // 'rank': element.rank,
                        // 'resultsFlag': overallFlag,
                        // 'canPublish': canPublish
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getOverallResult')
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
            }
        },
        "columns": [{
                'data': 'rollNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'classSection'
            },
            // {
            //     'data': 'overallPercentage'
            // },
            // {
            //     'data': 'rank'
            // },
            // {
            //     'data': 'resultsFlag'
            // },
            // {
            //     'data': 'action'
            // }
        ]
    });
}

function iterateStudentListTableForMove(){
    var moveStudentTable = $('#move-student-table tbody');
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {
            showProgressBar('p2-students-result-list');
        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            // classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                moveStudentTable.empty();
                if(res.data.length){
                    $.each(res.data, function(index, value){
                        let overallFlag =  `<label class="label label-sm overall-flag label-danger"> ${messages.fail} </label>`;
                        let addedResult;
                        if(overallList){
                            addedResult = overallList.find(o => o.studentID === value.studentID);
                        }
                        if(addedResult !== undefined && addedResult !== ''){
                            overallFlag =  `<label class="label label-sm overall-flag label-${addedResult.resultStatus == "P" ? 'success' : 'danger' }"> ${addedResult.resultStatus == 'P' ? messages.pass : messages.fail } </label>`;
                            insertTable = `
                            <tr>
                                <td style="display:none;">
                                    <input class="form-control" name="studentID" type="text" value=' ${ value.studentID }'>
                                </td>
                                <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                                <td>${value.rollNo}</td>
                                <td>${value.studentName }</td>
                                <td>${value.classAlias } - ${value.classSection }</td>
                                <td>
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg-${index}" class="studentCheckBox" type="checkbox" data-studentId = "${value.studentID}">
                                        <label for="checkboxbg-${index}">
                                        </label>
                                    </div>
                                </td>
                            </tr>`
                            moveStudentTable.append(insertTable);
                        }else{
                            insertTable = `
                            <tr>
                                <td style="display:none;">
                                    <input class="form-control" name="studentID" type="text" value=' ${ value.studentID }'>
                                </td>
                                <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                                <td>${value.rollNo}</td>
                                <td>${value.studentName }</td>
                                <td>${value.classLevel } - ${value.classSection }</td>
                                <td>${value.classYear !== null && value.classYear != 'null' ? value.classYear : ' - ' }</td>
                                <td>0 %</td>
                                <td>-</td>
                                <td>
                                    ${overallFlag}
                                </td>
                                <td>
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg-${index}" class="studentCheckBox" type="checkbox" data-studentId = "${value.studentID}">
                                        <label for="checkboxbg-${index}">
                                        </label>
                                    </div>
                                </td>
                            </tr>`
                        moveStudentTable.append(insertTable);
                        }
                    });

                    hideProgressBar('p2-students-result-list');
                }else{
                    moveStudentTable.empty();
                    moveStudentTable.append(`
                        <tr>
                            <td colspan="9" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)

                    hideProgressBar('p2-students-result-list');
                }

                } else {
                    moveStudentTable.empty();
                    moveStudentTable.append(`<tr>
                            <td colspan="9" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                }

        },
        error: function (err) {
            invokedFunctionArray.push('iterateStudentListTableForMove')
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-students-result-list');
        }
    })
}

// $('.js-example-basic-multiple-limit').select2({
//     tags: true,
//     tokenSeparators: [',', ' '],
//     placeholder: "Select years",
//     allowClear: true,
//     maximumSelectionLength: 3
// });


$(document).on('click','.move',function(){
    errorMsgShow.html('');
    $('#filter-move-classes').empty();
    moveClassLevels.empty();
    moveClassLevels.append(`<option value="" >Select Class</option>`);
    allClasses.forEach(function(value, index){
        // ${value.classLevelID == classLevelID ? 'selected' : ''}
        moveClassLevels.append(`<option value="${value.classLevelID}" >${value.classAlias }</option>`);
    });
    if(moveableStudentsArray.length){
        $('#moveStudentModal').modal({
            backdrop: 'static',
            keyboard: false,
            toggle: true,
            }
        );
        $('#moveStudentModal').modal('show');
    }else{
        swal(
            messages.pleaseSelectStudentToMove,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }
});

$(document).on('click','.studentCheckBox', function(){
    var isChecked = $(this).prop('checked');
    var studentId = $(this).attr('data-studentid');
    if(isChecked){
        moveableStudentsArray.push(studentId);
    }else{
        var index = moveableStudentsArray.indexOf(String(studentId));
        if (index > -1) {
            moveableStudentsArray.splice(index, 1);
        }
    }
});

$('#close-hide-add-form-container').on('click',function(){
    disableButtonsToggle();
})

$('#moveStudentModal').on('hide.bs.modal', function (e) {
    disableButtonsToggle();
})

$('#hide-add-form-container-promotion').on('click',function(){
    $('#move-student-table tbody').empty();
    moveableStudentsArray = [];
    hideAddFormContainer();
})
moveClassLevels.on('change',function(){
    MoveClasses = $('#filter-move-classes');
    classLevelID = $(this).val();
    let data = {
        schoolID: schoolID,
        branchID: branchID
    }

    if(classLevelID !== ''){
        data.classLevelID = classLevelID;
    }

    $.ajax({
        url: apiUrl + '/classes',
        method: 'GET',
        beforeSend: function () {
            MoveClasses.empty();
            MoveClasses.attr('disabled', true);
            showProgressBar('p2-classes-move');
        },
        data: data,
        success: function (res) {
            if (!res.error) {
                MoveClasses.removeAttr('disabled');
                if (res.data.length >= 1) {
                    $('#submit-move-student-form').removeAttr('disabled');
                    $.each(res.data, function (index, value) {
                        MoveClasses.append(`<option value="${value.classID}">${value.classAlias +' - '+ value.classSection }</option>`);
                    });
                    hideProgressBar('p2-classes-move');
                } else {
                    MoveClasses.append(`<option value = ""> ${messages.selectSection}</option>`);
                    hideProgressBar('p2-classes-move');
                }
            } else {
                MoveClasses.attr('disabled');
                hideProgressBar('p2-classes-move');
            }
        },
        error: function (err) {
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-classes-move');
        }
    });
})

$(document).on('change','.filter-move-classes', function(){
    var classID = $(this).val();
    if(classID != ''){
        $('#submit-move-student-form').removeAttr('disabled');
    }
});

$('#move-student-form').on('submit',function(e){
    e.preventDefault();
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSureYouWantToMoveSelectedStudents,
        buttons: {
            confirm: function () {
                var bulkArray = [];
                var selectedClassId  = $('#filter-move-classLevels').val();
                var selectedSectionId  = $('#filter-move-classes').val();
                var myObject = {};
                moveableStudentsArray.forEach((val, index) => {
                    var myObject = {};
                    // myObject["classLevelID"] = selectedClassId;
                    myObject["classID"] = selectedSectionId;
                    // myObject["classYear"] = selectedYears.join("-");
                    myObject['studentID'] = val;
                    bulkArray.push(myObject);
                });
                setTimeout(function(){
                    $.ajax({
                        url: herokuUrl+'/setClassChangeArray',
                        method: 'POST',
                        headers,
                        data :{array: bulkArray},
                        beforeSend: function(){
                        },
                        success: function(res){

                            if(res.response === 'success'){
                                moveableStudentsArray = [];
                                 $('#move-student-table tbody').empty();
                                errorMsgShow.html(`<p class="form-message"><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                                getOverallResult();
                                $('#moveStudentModal').modal('hide');
                                errorMsgShow.html('');
                                disableButtonsToggle();
                                hideAddFormContainer();
                            }
                        },
                        error: function(xhr){
                            window.requestForToken(xhr)
                            if (xhr.status === 422) {
                                parseValidationErrors(xhr);
                            }
                            if (xhr.status === 400) {
                                errorMsgShow.html(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                            }

                            if (xhr.status === 500) {
                                errorMsgShow.html(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
                            }
                            hideProgressBar(submitLoader);
                            disableButtonsToggle();
                        }
                    });
                },200);
            },
            cancel: function () {
            },
        }
    });
})

