$(document).ready(function(){
    getAjaxCountries();
    getAjaxSchools();
    getMessages();
});
var data =  {};
var messagesDataArray = [];

$('.notification-only').on('click',function(){
    if($('.notification-only').prop('checked')){
        $('#attachmentDiv').addClass('hidden');
        $('#subject-text').removeClass('hidden');
    }else{
        $('#subject-text').addClass('hidden');
        $('#attachmentDiv').removeClass('hidden');
    }
});

// branches.on('change', function () {
//     $('#filter-classLevels').prop('selectedIndex',0);
// });

// classLevels.on('change', function () {
//     $('#filter-classes').prop('selectedIndex',0);
// });

// classes.on('change', function () {
//     $('#filter-students').prop('selectedIndex',0);
// });

function createObjectFromAnotherObject(oldObject){
    var newObject = {};
    for (var propName in oldObject) {
        newObject[propName] = oldObject[propName];
    }
    return newObject;
}

function submitMessage(){
    countryID = $('#filter-countries').val()
    cityID = $('#filter-cities').val()
    areaID = $('#filter-areas').val();
    schoolID = $('#filter-schools').val();
    branchID = $('#filter-branches').val();
    classLevelID = $('#filter-classLevels').val();
    classID = $('#filter-classes').val();
    studentID = $('#filter-students').val();
    messageType = $('#filter-message-type').val();
    if(schoolID === ''){
        swal(
            messages.pleaseSelectSchool,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(branchID === ''){
        swal(
            messages.pleaseSelectBranch,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(classLevelID.length === 0){
        swal(
            messages.pleaseSelectClass,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if(!$('#filter-classLevels').find('option:first').prop('selected')){
        if(classID.length === 0){
            swal(
                messages.pleaseSelectSection,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
    }
    if(!$('#filter-classLevels').find('option:first').prop('selected') && !$('#filter-classes').find('option:first').prop('selected')){
        if(studentID.length === 0){
            swal(
                messages.pleaseSelectStudent,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
              return false;
        }
    }

    if(messageType === ''){
        swal(
            messages.pleaseSelectMessageType,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return false;
    }

    if($('#checkboxbg4').prop('checked') && $('#subjectText').val() === ''){
        swal(
            messages.pleaseEnterMessageSubject,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#messageText').val() === ''){
        swal(
            messages.pleaseEnterMessageText,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    var notificationOnly = 0;
    if($('.notification-only').prop('checked')){
        notificationOnly = 1;
        if($('#subjectText').val() === ''){
            swal(
                messages.pleaseSelectSubject,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }
    }
    // showProgressBar(submitLoader);

    var bulkArray = [];
    let user = JSON.parse(localStorage.getItem('user'));
    var formData = $('#filter-form').serializeArray();
    var attachmentUrl = $('#hiddenAttachment').val()
    if(messagesClassLevelArray.length > 0){
        // if(messagesClassLevelArray.length > 0 || (messagesClassLevelArray.length === 0 && user.isAdmin === 0)){
        // if(user.isAdmin === 0 && messagesClassLevelArray.length === 0){
        //     messagesClassLevelArray = messageUserClassLevels
        // }
        messagesClassLevelArray.forEach(function(classLevelsData, classLevelsIndex){
            var obj = {};
            if(countryID !== ''){
                obj.countryID = countryID
            }
            if(cityID !== ''){
                obj.cityID = cityID
            }
            if(areaID !== ''){
                obj.areaID = areaID
            }
            if(schoolID !== ''){
                obj.schoolID = schoolID
            }
            if(branchID !== ''){
                obj.branchID = branchID
            }
            obj.messageText = $('#messageText').val();
            obj.userID = user.userID;
            obj.countryID = countryID;
            obj.notificationOnly = notificationOnly;

            attachmentUrl !== '' ? obj.attachment = attachmentUrl: '';
            formData.forEach(function(value, index){
                if(value.name != 'classLevelID' &&  value.name != "classID" && value.name != 'studentID' && value.name != 'dateFrom' && value.name != 'dateTo' && value.name != 'countryID'){
                    if(notificationOnly === 1){
                        if(value.name === 'messageType') {
                            obj[value.name] = `${$('#subjectText').val()}`
                        }
                    }else {
                        obj[value.name] = value.value;
                    }
                }
            });
            obj.classLevelID = Number(classLevelsData);
            if(typeof messagesClassArray[classLevelsData] != 'undefined' && messagesClassArray[classLevelsData].length > 0 &&  Number(messagesSelectedClassesArray[0]) !== 0){
                messagesClassArray[classLevelsData].forEach((cls, index) => {
                        if(messagesSelectedClassesArray.indexOf(String(cls.classID)) > -1){
                            var classObject = {};
                            classObject = createObjectFromAnotherObject(obj);
                            classObject.classID = Number(cls.classID);
                            if(typeof messagesStudentArray[cls.classID] != 'undefined' && typeof messagesSelectedStudentsArray != 'undefined' && messagesStudentArray[cls.classID].length > 0 && Number(messagesSelectedStudentsArray[0]) !== 0 ){
                                messagesStudentArray[cls.classID].forEach((student, i) => {
                                    if(messagesSelectedStudentsArray.indexOf(String(student.studentID)) > -1){
                                        var studentObject = {};
                                        studentObject =  createObjectFromAnotherObject(classObject);
                                        studentObject.studentID = Number(student.studentID);
                                        bulkArray.push(clean(studentObject));
                                    }
                                })
                            }else{
                                bulkArray.push(clean(classObject));
                            }
                        }
                })
            }else{
                bulkArray.push(clean(obj));
            }
        })
    }else if(messagesSelectedClassesArray.length > 0){
        messagesSelectedClassesArray.forEach((cls, index) => {
            var obj = {};
            if(countryID !== ''){
                obj.countryID = countryID
            }
            if(cityID !== ''){
                obj.cityID = cityID
            }
            if(areaID !== ''){
                obj.areaID = areaID
            }
            if(schoolID !== ''){
                obj.schoolID = schoolID
            }
            if(branchID !== ''){
                obj.branchID = branchID
            }
            obj.messageText = $('#messageText').val();
            obj.userID = user.userID;
            obj.countryID = countryID;
            obj.notificationOnly = notificationOnly;
            attachmentUrl !== '' ? obj.attachment = attachmentUrl: '';
            formData.forEach(function(value, index){
                if(value.name != 'classLevelID' &&  value.name != "classID" && value.name != 'studentID' && value.name != 'dateFrom' && value.name != 'dateTo' && value.name != 'countryID'){
                    if(notificationOnly === 1){
                        if(value.name === 'messageType') {
                            obj[value.name] = `${$('#subjectText').val()}`
                        }
                    }else {
                        obj[value.name] = value.value;
                    }
                }
            });
            if(messagesSelectedClassesArray.indexOf(String(cls)) > -1){
                var classObject = {};
                classObject = createObjectFromAnotherObject(obj);
                classObject.classID = Number(cls);
                if(typeof messagesStudentArray[cls] != 'undefined' && typeof messagesSelectedStudentsArray != 'undefined' && messagesStudentArray[cls].length > 0 && Number(messagesSelectedStudentsArray[0]) !== 0 ){
                    messagesStudentArray[cls].forEach((student, i) => {
                        if(messagesSelectedStudentsArray.indexOf(String(student.studentID)) > -1){
                            var studentObject = {};
                            studentObject =  createObjectFromAnotherObject(classObject);
                            studentObject.studentID = Number(student.studentID);
                            bulkArray.push(clean(studentObject));
                        }
                    })
                }else{
                    bulkArray.push(clean(classObject));
                }
            }
        })
    }else{
        var obj = {};
        if(countryID !== ''){
            obj['countryID'] = countryID
        }
        if(cityID !== ''){
            obj['cityID'] = cityID
        }
        if(areaID !== ''){
            obj['areaID'] = areaID
        }
        if(schoolID !== ''){
            obj['schoolID'] = schoolID
        }
        if(branchID !== ''){
            obj['branchID'] = branchID
        }
        obj['messageText'] = $('#messageText').val();
        obj['userID'] = user.userID;
        obj['countryID'] = countryID;
        obj['notificationOnly'] = notificationOnly;
        attachmentUrl !== '' ? obj.attachment = attachmentUrl: '';
        formData.forEach(function(value, index){
            if(value.name != 'classLevelID' &&  value.name != 'classID' && value.name != 'studentID' && value.name != 'dateFrom' && value.name != 'dateTo' && value.name != 'countryID'){
                if(notificationOnly === 1){
                    if(value.name === 'messageType') {
                        obj[value.name] = `${$('#subjectText').val()}`
                    }
                }else {
                    obj[value.name] = value.value;
                }
                showProgressBar(submitLoader);
            }
        });
        bulkArray.push(clean(obj));
    }
    setTimeout(() => {
        $.ajax({
            url: herokuUrl+'/setMessagesArray',
            method: 'POST',
            headers,
            data: {'array': bulkArray},
            beforeSend: function(){
                disableButtonsToggle();
                showProgressBar(submitLoader);
            },
            success: function(res){
                if(res.response === 'success'){
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                    tableData.destroy()
                    $('#messages-form').trigger('reset');
                    getMessages();
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
                $('#hiddenAttachment').val('')
            },
            error: function(xhr){
                invokedFunctionArray.push('submitMessage');
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 400){
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                }
                if (xhr.status === 500) {
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            }
        });
    },500)

}
$("#messageText").keyup(function(){
    $("#char-count").text($(this).val().length);
  });
function getMessages(){
    data =  {};
    schoolID = typeof $('#filter-schools').val() !== 'undefined' && $('#filter-schools').val() !== '' ? $('#filter-schools').val() : '';
    branchID = typeof $('#filter-branches').val() !== 'undefined' && $('#filter-branches').val() !== '' ? $('#filter-branches').val() : '';
    data.messageDateFrom = dateFromVal;
    data.messageDateTo = dateToVal;
    if(countryID !== ''){
        data.countryID = countryID;
    }else{
        data.countryID = $('#filter-countries').val()
    }
    if(cityID !== ''){
        data.cityID = cityID;
    }else{
        data.cityID = $('#filter-cities').val()
    }
    if(areaID !== ''){
        data.areaID = areaID;
    }else{
        data.areaID = $('#filter-areas').val()
    }
    if(schoolID != ''){
        data.schoolID = schoolID;
    }else{
        data.schoolID = user.schoolID
    }
    if(branchID != ''){
        data.branchID = branchID;
    }else{
        data.branchID = user.branchID
    }
    if(Array.isArray(classLevelID)){
        if(classLevelID.length >= 1 && classLevelID != '0'){
            data.classLevelID = classLevelID.join(',');
        }
    }else{
        if(Number(classLevelID) !== 0)
            data.classLevelID =  classLevelID;
    }

    data.messageType = messageType;
    if(Array.isArray(studentID)){
        var index = studentID.indexOf('0');
        if(index >= 0){
            studentID.splice(index,1);
            if(studentID.length)
                data.studentID = studentID.join(',');
        }
    }else{
        if(Number(studentID) !== 0)
            data.studentID =  studentID;
    }
    if(Array.isArray(classID)){
        var index = classID.indexOf('0');
        if(index >= 0){
            classID.splice(index,1);
            if(classID.length)
                data.classID = classID.join(',');
        }
    }else{
        if(Number(classID) !== 0)
            data.classID =  classID;
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getMessagesSummary',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    var regDate = moment.utc(response[i].insertionDate);
                    regDate = regDate.local().format('DD-MMM-YYYY hh:mm A')
                    let type = response[i].messageType;
                    let lab = {};

                    if(type === 'C'){
                        lab.label = 'success';
                        lab.text = messages.circular;
                    }

                    if(type === 'P'){
                        lab.label = 'primary';
                        lab.text = messages.parentConsent;
                    }

                    if(type === 'I'){
                        lab.label = 'warning';
                        lab.text = messages.invitation;
                    }

                    if(type === 'M'){
                        lab.label = 'danger';
                        lab.text = messages.message;
                    }
                    return_data.push({
                        'sNo' : i+1,
                        'messageText': response[i].messageText,
                        'messageDate' : regDate,
                        'messageType' : `<label class="label label-sm label-${lab.label}"> ${lab.text}</label>`,
                        'studentName' : response[i].recipient,
                        'list': `<button style="margin-left:10px;" data-toggle="tooltip" title="${messages.ViewMessagesList}" class="btn btn-primary btn-sm view-messages-list" data-guiId="${response[i].messageGUID}" data-recipient="${response[i].recipient }" data-message-text="${response[i].messageText}" data-message-date="${regDate}" data-message-type="${lab.text}" data-message-type="${response[i].messageType}"> <i class="fa fa-list"></i>${messages.view} </button>`,
                        'action':`${response[i].attachment !== null && response[i].attachment !== undefined && response[i].attachment !== 'undefined' ? `
                        <div class="btn-group">
                            <a href="${response[i].attachment}" target="_blank" title="${messages.attachment}" class="btn btn-xs btn-success m-0"><i class="fa fa-image" style="padding-top: 5px;"></i>
                            </a>
                        ` : ''}
                        ${__DEL__ ? `<center><button data-message-id="${response[i].messageGUID}" class="btn btn-danger btn-sm delete-message"><i class="fa fa-trash"></i></button></center>` : ''}
                        </div>`

                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getMessages');
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }else if(xhr.status === 400){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                // console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'messageText'},
            {'data': 'messageDate'},
            {'data': 'messageType'},
            {'data': 'studentName'},
            {'data': 'list'},
            {'data': 'action'}
        ]
    });

}
function populateParentTypeAbbrevation(shortForm){
    switch(shortForm) {
        case 'F':
            return messages.father;
            break;
        case 'M':
            return messages.mother;
            break;
        case 'G':
            return messages.guardian;
            break;
        default:
            return messages.unknown;
    }

}
$(document).on('click', '.delete-message', function(){
    messageGUID = $(this).attr('data-message-id');
    $.confirm({
        title: messages.confirm,
        content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheMessage+"</br>",
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteMessage();
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });
});

$(document).on('click','.view-messages-list',function(){
    messagesDataArray = []
    messagesDataArray['messageText'] = $(this).attr('data-message-text');
    messagesDataArray['messageType'] = $(this).attr('data-message-type');
    messagesDataArray['messageDate'] = $(this).attr('data-message-date');
    messagesDataArray['messageUUID'] = $(this).attr('data-guiid');
    let messageType = $(this).attr('data-message-type')
    if(messageType == 'Parent Consent'){
        $('#exportPDFBtn').removeClass('hidden')
    }else{
        $('#exportPDFBtn').addClass('hidden')
    }
    $('#messageDetailModal').modal({
        backdrop: 'static',
        keyboard: false,
        toggle: true,
        }
    );
    $('#messageDetailModal').modal('show');
    if (messageListTable) {
        messageListTable.destroy();
    }
    var messageGUID = $(this).attr('data-guiId');
    var recipientName = $(this).attr('data-recipient');
    $('.message-list-title').text(recipientName + " Detail List ");
        data =  {};
        data.messageGUID = messageGUID;
        if(Array.isArray(studentID)){
            var index = studentID.indexOf('0');
            if(index >= 0){
                studentID.splice(index,1);
                if(studentID.length)
                    data.studentID = studentID.join(',');
            }
        }else{
            if(Number(studentID) !== 0)
                data.studentID =  studentID;
        }
        if(Array.isArray(classID)){
            var index = classID.indexOf('0');
            if(index >= 0){
                classID.splice(index,1);
                if(classID.length)
                    data.classID = classID.join(',');
            }
        }else{
            if(Number(classID) !== 0)
                data.classID =  classID;
        }
    messageListTable = $('#message-detail-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getMessageDetails',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                var j = null;
                var parentResponse = null;
                var consentData = {};
                for(var i = 0; i < response.length; i++){
                    consentData = {};
                    if(response[i].Responses.length > 0){

                        parentResponse = response[i].Responses;

                        for(j = 0; j < parentResponse.length; j++){
                            var regDate = moment.utc(parentResponse[j].consentDate);
                            regDate = regDate.local().format('DD-MMM-YYYY h:mm A')

                            if(parentResponse[j].parentType === 'M' && parentResponse[j].consentResponse === 1){
                                consentData.consentMotherText = parentResponse[j].consentText && parentResponse[j].consentText !== null ? parentResponse[j].consentText : '';

                                consentData.consentMotherDate = regDate && regDate !== null ? regDate : '';

                                consentData.consentMotherResponse = '<label class="label btn-success label-sm">'+parentResponse[j].parentDescription+'</label>'
                            }else if(parentResponse[j].parentType === 'M' && parentResponse[j].consentResponse === 0){
                                consentData.consentMotherDate = regDate && regDate !== null ? regDate : '';
                                consentData.consentMotherText = parentResponse[j].consentText && parentResponse[j].consentText !== null ? parentResponse[j].consentText : '';
                                consentData.consentMotherResponse = '<label class="label btn-danger label-sm">'+parentResponse[j].parentDescription+'</label>'
                            }else if(parentResponse[j].parentType === 'M' && parentResponse[j].consentResponse === null){
                                consentData.consentMotherResponse = ''
                            }

                            if(parentResponse[j].parentType === 'F' && parentResponse[j].consentResponse === 1){
                                consentData.consentFatherDate = regDate && regDate !== null ? regDate : '';
                                consentData.consentFatherText = parentResponse[j].consentText && parentResponse[j].consentText !== null ? parentResponse[j].consentText : '';
                                consentData.consentFatherResponse = '<label class="label btn-success label-sm">'+parentResponse[j].parentDescription+'</label>'
                            }else if(parentResponse[j].parentType === 'F' && parentResponse[j].consentResponse === 0){
                                consentData.consentFatherDate = regDate && regDate !== null ? regDate : '';
                                consentData.consentFatherText = parentResponse[j].consentText && parentResponse[j].consentText !== null ? parentResponse[j].consentText : '';
                                consentData.consentFatherResponse = '<label class="label btn-danger label-sm">'+parentResponse[j].parentDescription+'</label>'
                            }else if(parentResponse[j].parentType === 'F' && parentResponse[j].consentResponse === null ){
                                consentData.consentFatherResponse = ''
                            }

                            if(parentResponse[j].parentType === 'G' && parentResponse[j].consentResponse === 1){
                                consentData.consentGuardianDate = regDate && regDate !== null ? regDate : '';
                                consentData.consentGuardianText = parentResponse[j].consentText && parentResponse[j].consentText !== null ? parentResponse[j].consentText : '';
                                consentData.consentGuardianResponse = '<label class="label btn-success label-sm">'+parentResponse[j].parentDescription+'</label>'
                            }else if(parentResponse[j].parentType === 'G' && parentResponse[j].consentResponse === 0){
                                consentData.consentGuardianDate = regDate && regDate !== null ? regDate : '';
                                consentData.consentGuardianText = parentResponse[j].consentText && parentResponse[j].consentText !== null ? parentResponse[j].consentText : '';
                                consentData.consentGuardianResponse = '<label class="label btn-danger label-sm">'+parentResponse[j].parentDescription+'</label>'
                            }else if(parentResponse[j].parentType === 'G' && parentResponse[j].consentResponse === null){
                                consentData.consentGuardianResponse = ''
                            }
                        }
                        return_data.push({
                            'sNo' : i+1,
                            'studentName' : response[i].studentName,
                            'classAlias' : response[i].classAlias+' '+response[i].classSection,
                            'consentResponse' : (consentData.consentMotherResponse && consentData.consentMotherResponse !== null ? `<span data-toggle="tooltip" title="${consentData.consentMotherDate+' > '+consentData.consentMotherText}">`+consentData.consentMotherResponse+`</span>` : '') +' '+ (consentData.consentFatherResponse && consentData.consentFatherResponse !== null ? `<span data-toggle="tooltip" title="${consentData.consentFatherDate+' > '+consentData.consentFatherText}">`+consentData.consentFatherResponse+`</span>` : '') +' '+ (consentData.consentGuardianResponse && consentData.consentGuardianResponse !== null ? `<span data-toggle="tooltip" title="${consentData.consentGuardianDate+' > '+consentData.consentGuardianText}">`+consentData.consentGuardianResponse+`</span>` : '') ,
                        })
                    }else{
                        return_data.push({
                            'sNo' : i+1,
                            'studentName' : response[i].studentName,
                            'classAlias' : response[i].classAlias+' '+response[i].classSection,
                            'consentResponse' : '',
                        })
                    }
                }
                return return_data;
            },
            error: function(xhr){
                window.requestForToken(xhr)
                console.log(xhr);
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'classAlias'},
            {'data': 'consentResponse'},
        ],
    });
});

$(document).on('click','.view-parent-consent',function(){
        var row = $(this).attr('data-consentID');
        $('#' + row).slideToggle("slow");
});

$('#messages-form').on('submit', function(e){
    e.preventDefault();
    $.LoadingOverlay("show");
    var formData = new FormData($(this)[0]);
    var att = $('#attachmentUpload').val();
    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
    if(att !== ''){
            $.ajax({
                url: __BASE__+'/messages/attachment',
                type: 'POST',
                data: formData,
                async: false,
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function(){
                    //disableButtonsToggle();
                },
                success: function(xhr){
                    if(xhr){
                        $('#hiddenAttachment').val(__BASE__+'/attachments/'+xhr.attachment);
                    }
                },
                error: function(xhr){
                    window.requestForToken(xhr)
                    if(xhr.status === 422){
                        let errors = xhr.responseJSON.errors;
                        $.each(errors, function(key, val){
                            $('#add-form-errors').html(`<p class="text-danger"><strong>${val[0]}</strong></p>`);
                        });
                    }
                    $.LoadingOverlay("hide");
                }
            });
    }

    setTimeout(()=>{
        $.LoadingOverlay("hide");
        resetMessages();
        submitMessage();
    },200)

});

function deleteMessage(){
    $.ajax({
        url: herokuUrl+'/deleteMessages',
        method: 'POST',
        headers,
        data: {
            messageGUID: messageGUID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.messageHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                tableData.destroy();
                messageGUID = '';
                getMessages();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteMessage')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('click', '#exportPDFBtn', function(){
    let messageGUID = messagesDataArray.messageUUID
    let data = {}
    if(messageGUID !== ''){
        data.messageGUID = messageGUID
    }
    $('.exportLoader').removeClass('hidden');

    $.ajax({
        url: herokuUrl+'/getConsentReportExport',
        method: 'POST',
        headers,
        data,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'consent-report.xlsx';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
                setTimeout(()=>{
                    $('.exportLoader').addClass('hidden');
                },500)
        }
    });

    return

})
