$(document).ready(function(){
    getAjaxSchools();

    if($('#filter-schools').length === 0 && $('#filter-branches').length === 0){
        getClassRooms();
    }
});

var data = {};

function submitClassRoom(){
    if($('#filter-branches').val() === ''){
        swal(
            messages.pleaseSelectBranch,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#deviceIdentifier').val() === ''){
        delete data['deviceIdentifier'];
    }

    if($('#roomDesc').val() === ''){
        delete data['roomDesc']
    }

    $.ajax({
        url: herokuUrl+'/setClassRoom',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                // tableData.destroy();
                getClassRooms();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitClassRoom');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$('#class-rooms-form').on('submit', function(e){
    e.preventDefault();
    var isClassable = $('#isClassable').prop("checked") === true ? '1' : '0';
    var isActive = $('#isActive').prop("checked") === true ? 1 :  0;
    data = {};
    let filterData = $('#filter-form').serializeArray();

    let formData = $('#class-rooms-form').serializeArray();
    data['isClassable'] = isClassable;
    data['isActive'] = isActive;
    filterData.forEach((value, index) => {
        data[value.name] = value.value;
    })
    formData.forEach((value, index) => {
        if(value.name !== 'isClassable' && value.name !== 'isActive'){
            data[value.name] = value.value;
        }
    })
    // return false
    showProgressBar(submitLoader);
    resetMessages();
    submitClassRoom();
});

function getClassRooms(){
    data = {};

    if(tableData){
        tableData.destroy();
    }
    if(schoolID !== ''){
        data.schoolID = schoolID
    }

    if(branchID !== ''){
        data.branchID = branchID
    }


    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getClassRoom',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'roomName': response[i].roomName,
                        'deviceIdentifier' : response[i].deviceIdentifier,
                        'isClassable': `<label class="label label-sm label-${response[i].isClassable ? 'success' : 'danger'}"> ${response[i].isClassable ? messages.yes : messages.no}</label>`,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `<div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-classRoom" data-classRoom-id='${response[i].roomID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getClassRooms');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'roomName'},
            // {'data': 'roomDesc'},
            {'data': 'deviceIdentifier'},
            {'data': 'isClassable'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });
}

// function getActiveClassRooms(){
//     data = {};

//     if(tableData){
//         tableData.destroy();
//     }
//     if(schoolID !== ''){
//         data.schoolID = schoolID
//     }

//     if(branchID !== ''){
//         data.branchID = branchID
//     }


//     tableData = $('#example44').DataTable({
//         'ajax'       : {
//             url: herokuUrl+'/getActiveClassRoom',
//             type: 'POST',
//             headers,
//             beforeSend: function(){
//                 showProgressBar('p2-billing-period');
//                 classRoom.attr('disabled', true);
//                 classRoom.html('');
//                 classRoom.find('option').remove();
//                 classRoom.empty();
//                 classRoom.html(`<option value="">Select Class Room<option>`);
//             },
//             data,
//             success: function(res){
//                   response = parseResponse(res);
//                   $.each(response, function (index, value) {
//                       classRoom.append(`<option value="${value.roomID}">${value.roomName}</option>`);
//                   });
//                   $('.filter-time-table-rooms option')
//                   .filter(function() {
//                       return $.trim(this.text).length == 0;
//                   })
//                   .remove();
//                   classRoom.multiselect('rebuild');
//                   },
//             error: function(xhr){
//                 console.log(xhr);
//             }
//         },
//         "columns"    : [
//             {'data': 'sNo'},
//             {'data': 'roomName'},
//             // {'data': 'roomDesc'},
//             {'data': 'deviceIdentifier'},
//             {'data': 'isClassable'},
//             {'data': 'isActive'},
//             {'data': 'action'}
//         ]
//     });
// }

$(document).on('click', '.delete-classRoom', function(){
    roomID = $(this).attr('data-classRoom-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheRoom+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteClassRoom();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteClassRoom(){
    $.ajax({
        url: herokuUrl+'/deleteClassRoom',
        method: 'POST',
        headers,
        data: {
            roomID: roomID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.roomHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                roomID = '';
                getClassRooms();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteClassRoom');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.deletedMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.deletedMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.deletedMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
