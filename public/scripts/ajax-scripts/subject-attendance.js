$(document).ready(function(){
    getAjaxSchools();
})

var attendanceDate = '';
var subjectID = 0;
var timetableSubjectID = 0;
var weekdayNo = 0;
var data = {};
var isMarkedHoliday = false;
var isAttendanceAvailable = false;
var subjectAttendanceDataTable = null;
var dateFromVal = '';
var dateToVal = '';
var formData = [];
var bulkArray = []

function getStudentSubjectAttendance(){
    data = {};
    subjectID = $('#filter-subjects').val();
    attendanceDate = $('#subjectAttendanceDate').val();
    timetableSubjectID = $('#filter-subjects-in-time-table').val();
    if(subjectID !== ''){
        data.subjectID = subjectID;
    }

    if(timetableSubjectID !== ''){
        data.timetableSubjectID = timetableSubjectID;
    }

    if(attendanceDate !== ''){
        data.attendanceDate = attendanceDate;
    }

    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        iDisplayLength: 100,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getStudentSubjectAttendance',
            type: 'POST',
            headers,
            beforeSend: function(){
                studentsInTimeTableArray  = []
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                __SUBJECTATTENDANCE__ = response;
                for(var i = 0; i< response.length; i++){

                    var at = '';
                    let present;
                    let notesText;
                    response[i].data.forEach((val, index) => {
                        let key = Object.keys(val)[0];
                        let key1 = Object.keys(val)[1];
                        let v = val[Object.keys(val)[0]];
                        notesText = val[Object.keys(val)[1]];
                        at += `<li>${moment(key).format('LL')} - ${v}</li>`;
                        if(v == 'Present'){
                            present = 'P'
                        }else if(v == 'Absent'){
                            present = 'A'
                        }else if(v == 'Leave'){
                            present = 'L'
                        }else if(v == 'Late'){
                            present = 'T'
                        }else if(v == 'Day Off'){
                            present = 'O'
                        }
                        // present = v == 'Present' ? '1' : (v == 'Leave') ? '2' : '0';
                    });

                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'rollNo': response[i].rollNo,
                        'notesText' : notesText !== null && notesText !== 'null' ? notesText : '-' ,
                        'attendance' : `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" class=" attendanc" disabled id="checkbox-pp"
                                            ${present == 'P' ? 'checked' : ''} />
                                            <label for="checkbox-pp"><b class="check-text">P</b></label>
                                        </div>
                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" class=" attendanc" disabled id="checkbox-d-aa" ${present == 'A' ? 'checked' : ''}/>
                                            <label for="checkbox-d-aa"><b class="check-text">A</b></label>
                                        </div>
                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-l-ll" ${present == 'L' ? 'checked' : ''} />
                                            <label for="checkbox-l-ll"><b class="check-text">L</b></label>
                                        </div>
                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-t-tt" ${present == 'T' ? 'checked' : ''} />
                                            <label for="checkbox-t-tt"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>
                                        <div class="round round-o" data-toggle="tooltip" title="${messages.offDayAttendance}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-o-oo" ${present == 'O' ? 'checked' : ''} />
                                            <label for="checkbox-o-oo"><b class="check-text">O</b></label>
                                        </div>
                                    </div>`,
                        // 'action' : '<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-json='+JSON.stringify(response[i])+'><i class="fa fa-pencil"></i></button>'
                        // 'action': ''
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudentSubjectAttendance');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'rollNo'},
            {'data': 'notesText'},
            {'data': 'attendance'},
            // {'data': 'action'}
        ]
    });
}

$('#add-subject-attendance').on('click', function(){
    let date = $('#attendanceDate').val();
    $.LoadingOverlay("show");
    getStudentsInTimetable();
    dateFromVal = $('#attendanceDate').val();
    dateToVal = $('#attendanceDate').val();
    getCalendarDays();
    setTimeout(() => {
        $.LoadingOverlay("hide");
        toggleForm()
    },1000)

});

function getCalendarDays(){

    $.ajax({
        url: herokuUrl+'/getCalendarDays',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data: {
            schoolID : schoolID,
            dateFrom: dateFromVal,
            dateTo: dateToVal
        },
        success:function(res){
            if(typeof res.response.length !== 'undefined' &&  res.response.length > 0){
                isMarkedHoliday = true;
            }else{
                isMarkedHoliday = false;
            }
        },
        error:function(err){
            invokedFunctionArray.push('getCalendarDays');
            window.requestForToken(xhr)
            console.log(err)
        },
    })
}

function toggleForm(){
    if(isMarkedHoliday === false && isAttendanceAvailable === false){
        if(classID !== ''){
            $('#subject-attendance').removeClass('hidden').slideDown();
            $('#subject-attendance-list').slideUp();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === false && isAttendanceAvailable === true){
        if(classID !== ''){
            $('#subject-attendance').removeClass('hidden').slideDown();
            $('#subject-attendance-list').slideUp();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === true && isAttendanceAvailable === true){
        if(classID !== ''){
            $('#subject-attendance').removeClass('hidden').slideDown();
            $('#subject-attendance-list').slideUp();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === true && isAttendanceAvailable === false){
        swal(
            messages.error,
            messages.selectedDateIsMarkedAsHoliday,
            'error'
        );
        return false;
    }
}

$('.cancel-subject-attendance-form').on('click', function(){
    unCheckedAlllistedStatusOfSubjectAttendance();
    unCheckedToplistedStatusOfSubjectAttendance();
    $('#add-attendance-form-errors').html('');
    $('#subject-attendance').removeClass('hidden').slideUp();
    $('#subject-attendance-list').slideDown();
})

function unCheckedAlllistedStatusOfSubjectAttendance(){

    $('.mark-checkbox-a').each(function(){
        $(this).prop('checked',false);
    });
    $('.mark-checkbox-p').each(function() {
        $(this).prop('checked',false);
    });
    $('.mark-checkbox-l').each(function(){
        $(this).prop('checked',false);
    });
    $('.mark-checkbox-t').each(function(){
        $(this).prop('checked',false);
    });
}

function unCheckedToplistedStatusOfSubjectAttendance(){
    $('.mark-attendances').each(function(){
        $(this).prop('checked',false);
    });
}
function checkedAlllistedStatusOfAttendance(present = null){
    switch(present){
        case '0':
                $('.mark-checkbox-a').each(function(){
                    $(this).prop('checked',true);
                });
            break;
        case '1':
                $('.mark-checkbox-p').each(function() {
                    $(this).prop('checked',true);
                });
            break;
        case '2':
                $('.mark-checkbox-l').each(function(){
                    $(this).prop('checked',true);
                });
                break;
        case '3':
                $('.mark-checkbox-t').each(function(){
                    $(this).prop('checked',true);
                });
            break;
    }
}

$(document).on('click', '.mark-attendance', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    if(checked){
        $('.' + index).prop('checked', false);
        $(this).prop('checked', true);
    }
});

$(document).on('click', '.mark-attendances', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    let target = $(this).attr('data-target');
    let present = target == 'P' ? '1' : (target == 'L') ? '2' : (target == 'T') ? '3' : '0';
    if(checked){
        $('.check-attendance').prop('checked', false);
        unCheckedAlllistedStatusOfSubjectAttendance();

        $(this).prop('checked', true);
        checkedAlllistedStatusOfAttendance(present);
    }
})

function getStudentsInTimetable(){
    data = {};
    var notes = '';
    subjectID = $('#filter-subjects').val();
    timetableSubjectID = $('#filter-subjects-in-time-table').val();
    if(subjectID !== ''){
        data.subjectID = subjectID;
    }
    if(timetableSubjectID !== ''){
        data.timetableSubjectID = timetableSubjectID;
    }
    if(weekdayNo !== ''){
        data.weekdayNo = weekdayNo;
    }

    if(subjectAttendanceDataTable){
        subjectAttendanceDataTable.destroy();
    }
    subjectAttendanceDataTable = $('#example45').DataTable({
        iDisplayLength: 100,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getStudentsInTimetable',
            type: 'POST',
            headers,
            data: {
                subjectID: subjectID,
                weekdayNo: weekdayNo,
                timetableSubjectID: timetableSubjectID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                console.log(response);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let findedObj;
                    if(__SUBJECTATTENDANCE__){
                        findedObj = __SUBJECTATTENDANCE__.find(o => o.studentID === response[i].studentID);
                    }
                   if(findedObj !== undefined && findedObj !== ''){
                        findedObj.data.forEach((val, index) => {
                        let key = Object.keys(val)[0];
                        notes = val[Object.keys(val)[1]];
                        let v = val[Object.keys(val)[0]];
                        present = v == 'Present' ? '1' : (v == 'Leave') ? '2' : '0';
                        if(v == 'Present'){
                            present = 'P'
                        }else if(v == 'Absent'){
                            present = 'A'
                        }else if(v == 'Leave'){
                            present = 'L'
                        }else if(v == 'Late'){
                            present = 'T'
                        }else if(v == 'Day Off'){
                            present = 'O'
                        }
                    });
                    return_data.push({
                        'checkboxes': `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="P" class="mark-attendance mark-checkbox-p attendance-check-${i}" data-row='${i}' data-json='${JSON.stringify(response[i])}' id="checkbox-p-${i}" ${present == 'P' ? 'checked' : ''} />
                                            <label for="checkbox-p-${i}"><b class="check-text">P</b></label>
                                        </div>

                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="A" class="mark-attendance mark-checkbox-a attendance-check-${i}" data-row='${i}'  data-json='${JSON.stringify(response[i])}' id="checkbox-d-${i}"  ${present == 'A' ? 'checked' : ''}/>
                                            <label for="checkbox-d-${i}"><b class="check-text">A</b></label>
                                        </div>

                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="L" class="mark-attendance mark-checkbox-l attendance-check-${i}" data-row='${i}' data-json='${JSON.stringify(response[i])}' id="checkbox-l-${i}" ${present == 'L' ? 'checked' : ''} />
                                            <label for="checkbox-l-${i}"><b class="check-text">L</b></label>
                                        </div>

                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="T" class="mark-attendance mark-checkbox-t attendance-check-${i}" data-row='${i}' data-json='${JSON.stringify(response[i])}' id="checkbox-t-${i}" ${present == 'T' ? 'checked' : ''} />
                                            <label for="checkbox-t-${i}"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>

                                    </div>`,
                                    'studentName': response[i].studentName,
                                    'rollNo': response[i].rollNo,
                                    'notes': `<input name="notes" value='${typeof notes !== 'undefined' ? notes : '' }' class="form-control"/>`,
                    })
                   }else{
                    return_data.push({
                        'checkboxes': `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="P" class="mark-attendance mark-checkbox-p attendance-check-${i}" data-row='${i}' data-json='${JSON.stringify(response[i])}' id="checkbox-p-${i}" />
                                            <label for="checkbox-p-${i}"><b class="check-text">P</b></label>
                                        </div>

                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="A" class="mark-attendance mark-checkbox-a attendance-check-${i}" data-row='${i}' data-json='${JSON.stringify(response[i])}' id="checkbox-d-${i}" />
                                            <label for="checkbox-d-${i}"><b class="check-text">A</b></label>
                                        </div>

                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="L" class="mark-attendance mark-checkbox-l attendance-check-${i}" data-row='${i}' data-json='${JSON.stringify(response[i])}' id="checkbox-l-${i}" />
                                            <label for="checkbox-l-${i}"><b class="check-text">L</b></label>
                                        </div>

                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="T" class="mark-attendance mark-checkbox-t attendance-check-${i}" data-row='${i}' data-json='${JSON.stringify(response[i])}' id="checkbox-t-${i}" />
                                            <label for="checkbox-t-${i}"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>

                                    </div>`,
                        'studentName': response[i].studentName,
                        'rollNo': response[i].rollNo,
                        'notes': '<input name="notes" class="form-control"/>',
                    })
                   }

                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudentsInTimetable');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'checkboxes'},
            {'data': 'studentName'},
            {'data': 'rollNo'},
            {'data': 'notes'}
        ]
    });
}

$('#bulk-subject-attendance-form').on('submit', function(e){
    e.preventDefault();
    if(classID !== ''){
        var offDayAttendanceNote = $('#offDayNote').val();
        if($('#offDayNoteCheckBox').prop("checked") === true ){
            if(offDayAttendanceNote.length == 0){
                $('#offDayNote').addClass('custom-validation-error');
                $('.custom-validation-error-msg').removeClass('hidden')
                return false;
            }else{
                $('#offDayNote').removeClass('custom-validation-error');
                $('.custom-validation-error-msg').addClass('hidden')
            }
        }
    bulkArray = [];
    let user = JSON.parse(localStorage.getItem('user'));
    let date = $('#attendanceDate').val();
    var notesData = $(this).serializeArray();
    notesData.shift();
    timetableSubjectID = $('#filter-subjects-in-time-table').val();
        $( '.mark-attendance').each( function(index,val){
            var batchNo = $('#filter-batch-no').val();
            let attendance = {};
            let target = $(this).attr('data-target');
            let nodeIndex = $(this).attr('data-row');
            let checked = $(this).prop('checked');
            let obj = JSON.parse($(this).attr('data-json'));
            let present = target == 'P' ? 'P' : (target == 'L') ? 'L' : (target == 'T') ? 'T' : 'A';
            if(checked){
                attendance.isPresent = present;
                attendance.userID = user.userID;
                attendance.studentID = obj.studentID;
                attendance.attendanceDate = date;
                attendance.timetableSubjectID = timetableSubjectID;
                attendance.batchNo = batchNo;
                attendance.notes = '';
                if(typeof notesData[nodeIndex] !== 'undefined' ){
                    attendance.notes = notesData[nodeIndex]['value'];
                }
                bulkArray.push(attendance);
            }
        })
        setTimeout(function(){
            showProgressBar(submitLoader);
            resetMessages();
            setSubjectAttendanceArray();
        }, 500);
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }

});

function setSubjectAttendanceArray(){
    $.ajax({
        url: herokuUrl+'/setSubjectAttendanceArray',
        method: 'POST',
        headers,
        data: {
            'array': bulkArray
        },
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                $('#add-attendance-form-errors').html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                swal(
                    'Success',
                    messages.successfullySubmitted,
                    'success'
                );
                $('#offDayMarkingModal').modal('hide');
                getStudentSubjectAttendance();
                setTimeout(function(){
                    $('#subject-attendance').removeClass('hidden').slideUp();
                    $('#subject-attendance-list').slideDown();
                    $('#add-attendance-form-errors').html('');
                },1000)

            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('setSubjectAttendanceArray');
            window.requestForToken(xhr);
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

