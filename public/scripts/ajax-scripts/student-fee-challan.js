var deleteVoucherID = null;
var deleteVoucherIDForItem = null;
var voucherDetailID = null;
var studentLevelFeeArray = [];
var studentFeeTypesArray = [];
var bulkArray = [];
var mainObject = {};
var table = $('#fee-detail-table tbody');
var feeChallanUrl = '';
var voucherUUID = '';
var publishedStatus = '';
$(document).ready(function () {
    getAjaxSchools();
    if(user.isAdmin === 0)
        getActiveBillingPeriod()
})

$(document).on('change', '#payment-status-filter', function(){
    getStudentFeeChallans()
});

$(document).on('click', '#viewChallanNoteButton', function(){
    $('#noteTableRow').slideDown();
    $('#viewChallanNoteButton').hide()
    $('#invoicechallanNote').html('<button id="closeChallanNoteButton" class="btn btn-sm btn-danger circle"><i class="fa fa-arrow-up"></i></button>');

})

$(document).on('click', '#closeChallanNoteButton', function(){
    $('#noteTableRow').slideUp();
    $('#closeChallanNoteButton').hide()
    $('#invoicechallanNote').html('<button id="viewChallanNoteButton" class="btn btn-sm btn-primary circle"><i class="fa fa-arrow-down"></i> </button>');
})

$(document).on('click', '.fee-detail', function () {
    $('#noteTableRow').hide();
    if(__ROUTE__ == 'student-fee-challan'){
        $.LoadingOverlay("show");
    }
    $('#success-span').html('');

    var table = $('#fee-detail-table tbody');
    var result = JSON.parse($(this).attr('data-response').replace(/&/g, "\'"));
    if(result.isPublished == 1){
        $('#publishedToggle').attr('voucher-uuid', result.voucherUUID)
        $('#publishedToggle').attr('billing-period-id', result.billingPeriodID)
        $('#publishedToggle').attr('checked', true)
        $('#publishedToggleSpan').addClass('green')
        $('#publishedToggle').val(result.isPublished)
    }else{
        $('#publishedToggle').attr('voucher-uuid', result.voucherUUID)
        $('#publishedToggle').attr('billing-period-id', result.billingPeriodID)
        $('#publishedToggle').attr('checked', false)
        $('#publishedToggle').val(result.isPublished)
        $('#publishedToggleSpan').removeClass('green')
    }

    let paidAmount = result.paidAmount !== null && result.paidAmount != '0' ? 'PKR '+addCommas((Math.round(result.paidAmount * 100) / 100).toFixed(0)) : ''

    $('#billing-period-modal').text(result.periodName);
    $('#taxAmount').text( result.currencyCode+' '+addCommas(result.taxAmount.toFixed(2)));
    $('#amountBeforeTax').text( result.currencyCode+' '+addCommas(result.feeAmountBeforeTax.toFixed(2)));
    $('#student-name').text(result.studentName);
    $('#paymentStatus').html(`<label class="label label-sm label-${result.paymentStatus !== 'null' && result.paymentStatus !== 'null' && result.paymentStatus == 'P' && result.paidAmount !== null ? 'success' : 'danger'}"> ${result.paymentStatus !== 'null' && result.paymentStatus == 'P' ? messages.paid+' '+paidAmount : messages.unpaid}</label>`)
    var percentageSign = result.penaltyType !== 'null' && result.penaltyType == 'P'  ? ' % ' : '' ;
    var penaltyAmount = result.penaltyType !== 'null' && result.penaltyType == 'P'  ? Number(result.penaltyValue) : result.penaltyValue ;
    $('#invoiceNumbers').text(result.invoiceNumber ? result.invoiceNumber : '-');
    $('#invoiceDates').text(result.invoiceDate ? moment(result.invoiceDate).format('DD-MMM-YYYY') : '-');
    $('#invoicechallanNote').html('<button id="viewChallanNoteButton" class="btn btn-sm btn-primary circle"><i class="fa fa-arrow-down"></i> </button>');

    var voucherNote = '';
    if(result.voucherNote){
        voucherNote = atob(result.voucherNote)
        voucherNote = voucherNote.substr(1,voucherNote.length-1)
        voucherNote = voucherNote.substr(0,voucherNote.length-1)

        var base64Rejex = /^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/i;
        var isBase64Valid = base64Rejex.test(voucherNote); // base64Data is the base64 string

        if (isBase64Valid) {
            // true if base64 formate
            voucherNote = atob(voucherNote)
            voucherNote = voucherNote.substr(1,voucherNote.length-1)
            voucherNote = voucherNote.substr(0,voucherNote.length-1)
        }
    }

    $('#hiddenFeeChallanNote').html(voucherNote ? voucherNote : '-');
    $('#invoiceDueDate').text(result.dueDate ? moment(result.dueDate).format('DD-MMM-YYYY') : '-');
    $('#invoicePenaltyValue').text(addCommas(penaltyAmount) + percentageSign);
    $('#invoiceTaxAmountDue').text(result.currencyCode+' '+addCommas(result.feeAmountAfterTax.toFixed(2)));
    // $('#invoiceTaxAmountDue').text(result.feeAmountAfterTax && result.paidAmount === null ?  result.currencyCode+' '+addCommas(result.feeAmountAfterTax.toFixed(2))  : '---');
    $('#invoiceTaxAmountAfterDue').text(result.currencyCode+' '+addCommas(result.feeAmountAfterDueDate.toFixed(2)));
    // $('#invoiceTaxAmountAfterDue').text(result.feeAmountAfterDueDate && result.paidAmount === null ? result.currencyCode+' '+addCommas(result.feeAmountAfterDueDate.toFixed(2)) : '---');
    $('#invoicePaymentDate').text(result.paymentDate ? moment(result.paymentDate).format('DD-MMM-YYYY') : '-' );
    $.ajax({
        url: herokuUrl+'/getFeeVoucherDetail',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data: {
            billingPeriodID: result.billingPeriodID,
            studentID : result.studentID,
            classID: result.classID
        },
        success: function(res){
            response = parseResponse(res);
            table.empty();
            if (response.length > 0) {
                response.forEach((val, index) => {
                    table.append(`
                    <tr>
                        <td  class="p-2"> ${ val.feeType } </td>
                        <td class="p-2">${result.currencyCode} ${ addCommas(val.feeTypeAmount) }  </td>
                        <td  class="p-2"> ${result.paymentStatus !== 'null' && result.paymentStatus !== 'P' ? `${__DEL__ ? `<i data-voucherID="${val.voucherID}" data-feeVoucherDetailID = "${val.feeVoucherDetailID}" data-toggle="tooltip" title="${messages.delete}" class="delete-voucher-item fa fa-trash text-danger"></i>` : '' }` : '-'}</td>
                    </tr>`);
                })
            } else {
                table.append(`<tr><td colspan="3" style="text-align:center !important">${messages.recordNotFound}</td></tr>`);
            }
            if(__ROUTE__ == 'student-fee-challan'){
                $.LoadingOverlay("hide");
            }
            $('#exampleModal').modal('toggle');
            $('#exampleModal').modal('show');

        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        //   if (xhr.status === 404) {
            // if(__ROUTE__ == 'student-fee-challan'){
                $.LoadingOverlay("hide");
            // }
            $('#exampleModal').modal('toggle');
            $('#exampleModal').modal('show');
        //  }
        }
    });
});
$(document).ready(function () {
    $("#invoiceNumber").keypress(function (e) {
       if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          $("#errmsg").html("Only Digits allowed").show().fadeOut("slow");
            return false;
      }
     });
  });
  function getStudentLevelFee(){
    $.ajax({
        url: herokuUrl+'/getStudentLevelFee',
        type: 'POST',
        headers,
        beforeSend: function(){
            studentLevelFeeArray = [];
        },
        data: {
            classID: classID,
            feeTypeID : feeTypeID
        },
        success: function(res){
            $('#assigned-recurring-error').empty();
            $('#submit-add-form-container').attr('disabled', false);
                $('#hide-add-form-container').removeAttr('disabled', false);
            if(res.response.length){
                response = parseResponse(res);
                $('#submit-add-form-container').attr('disabled', true);
                $('#hide-add-form-container').removeAttr('disabled', true);
                response.forEach(function(value, index){
                    $('#assigned-recurring-error').append(`
                    <li>${messages.recurringFeeIsNotAssignTo} ${value.studentName} </li>
                    `);
                });
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('getStudentLevelFee')
            window.requestForToken(xhr)
            if (xhr.status === 404) {
               console.log(xhr.responseJSON.response.internalMessage);
            }
        }
    });
}

function populateRecurringFee(){
    showProgressBar('p2-feeTypesTable');
    console.log(feeTypeArray,'feeTypeArray')
    feeTypeArray.forEach(function(value, index){
        if(value.isRecurring == 1){
            feeTypeID = value.feeTypeID;
            getStudentLevelFee();
                $("#multiple option[value='" + value.feeTypeID + "']").prop("selected", true);
                $('#multiple').trigger('change');
        }
    });
    hideProgressBar('p2-feeTypesTable');
}
function enableDisabledFeeTypeDropdown(){
    var selectedFeeTypes = [];
    selectedFeeTypes = getSeletedFeeTypes();
    removeDisableFromSeletedFeeTypes();
    selectedFeeTypes.forEach(function(val,index){
        setSeletedFeeTypes(val);
    });
}

function submitFeeChallan() {
    $.ajax({
        url: herokuUrl + '/' + feeChallanUrl,
        method: 'POST',
        headers,
        data,
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            $.LoadingOverlay("hide");
            if (res.response === 'success') {
                enableDisabledFeeTypeDropdown();
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
            }
            studentID = '';
            getStudentFeeChallans();
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            invokedFunctionArray.push('submitFeeChallan')
            window.requestForToken(xhr)
            $.LoadingOverlay("hide");
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if (xhr.status === 500) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            enableDisabledFeeTypeDropdown();
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
function deleteFeeChallanDetail(voucherID, feeVoucherDetailID) {

    $.ajax({
        url: herokuUrl + '/setFeeVoucherDelete',
        method: 'POST',
        headers,
        data:{
            voucherID: voucherID,
            feeVoucherDetailID: feeVoucherDetailID
        },
        success: function (res) {

        },
        error: function (xhr) {
            invokedFunctionArray.push('deleteFeeChallanDetail')
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

        }
    });
}

billingPeriod.on('change', function () {
    billingPeriodID = $(this).val();
    // $('#print-all-vouchers').removeClass('hidden')
    // $('#publish-all-vouchers').removeClass('hidden')
    if (billingPeriodID != '') {
        // if(classID != ''){
            getStudentFeeChallans();
        // }else{
        //     Swal.fire({
        //         type: messages.error,
        //         title: messages.oops,
        //         text: 'Please select Section first!',
        //       })
        //       return false;
        // }
    }
});

// classes.on('change', function () {
//     if (billingPeriodID != '') {
//         if(classID != ''){
//             getStudentFeeChallans();
//         }else{
//             Swal.fire({
//                 type: messages.error,
//                 title: messages.oops,
//                 text: 'Please select Section first!',
//               })
//               return false;
//         }
//     }
// });

students.on('change', function () {
    studentID = $(this).val();
    if (billingPeriodID != '') {
        getStudentFeeChallans();
    }
});
function calculateTotal(){
    var sum = 0;
    var sumWithTax = 0;
    var feeAmount;
    var amountAfterPanelty  = 0;
    var amountAfterPaneltyWithTax = 0;
    $('.feeAmount').each(function(){
        if($(this).val() !== ''){
            if(isNaN($(this).val()) === false){
                feeAmount = parseFloat($(this).val());
                sum += feeAmount;

            }
        }
        var taxAmount = parseFloat($(this).closest('tr').find('.taxRate').val());
        sumWithTax += feeAmount + ((feeAmount * taxAmount) / 100 );
    });
    populateTotalAmountFields(sum, sumWithTax);
    var penaltyType                   = $("input[name='penaltyType']:checked").val();
    var penaltyAmount                 = parseFloat($('#penaltyAmount').val());
    if(penaltyAmount != "" && penaltyAmount != null && isNaN(penaltyAmount) === false ){
        if(penaltyType == 'P'){
            amountAfterPanelty = sum +  ( sum * penaltyAmount ) / 100;
            amountAfterPaneltyWithTax = sumWithTax +  ( sumWithTax * penaltyAmount ) / 100;
            populateTotalAmountFieldsAfterDue(amountAfterPanelty,amountAfterPaneltyWithTax);
        }else if(penaltyType == 'F'){
            amountAfterPanelty = sum + penaltyAmount ;
            amountAfterPaneltyWithTax = sumWithTax +   penaltyAmount ;
            populateTotalAmountFieldsAfterDue(amountAfterPanelty,amountAfterPaneltyWithTax);
        }
    }
    populateTotalAmountFieldsAfterDue(amountAfterPanelty, amountAfterPaneltyWithTax);

}
function populateTotalAmountFields(amount, tax){
    $("#totalAmountDue").val(amount);
    $("#taxAmountDue").val(tax);
}

function populateTotalAmountFieldsAfterDue(amount, tax){
    $("#totalAmountAfter").val(amount);
    $("#taxAmountAfterDue").val(tax);
}

$(document).on('keyup','.feeAmount',function(){
    calculateTotal();
})
$(document).on('keyup','.penaltyAmount1',function(event){

    if(event.which >= 37 && event.which <= 40) return;
});
$('.penaltyAmount').keypress(function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
  });
$(document).on('change','.radio-button',function(){
    var amountAfterPanelty = 0;
    var amountAfterPaneltyWithTax = 0;
    // $('.text-danger').addClass('hide-error-text');
    var penaltyAmount  = parseFloat($('.penaltyAmount').val());
    var totalAmount    = parseFloat($("#totalAmountDue").val());
    var amountAfterTax =  parseFloat($("#taxAmountDue").val());
    var penaltyType    = $("input[name='penaltyType']:checked").val();
    if(penaltyAmount != "" && penaltyAmount != null  && isNaN(penaltyAmount) === false ){
        if(penaltyType == 'P'){
            amountAfterPanelty = totalAmount +  ( totalAmount * penaltyAmount ) / 100;
            amountAfterPaneltyWithTax = amountAfterTax +  ( amountAfterTax * penaltyAmount ) / 100;
            $("#totalAmountAfter").val(amountAfterPanelty);
            $("#taxAmountAfterDue").val(amountAfterPaneltyWithTax);
        }else if(penaltyType == 'F'){
            amountAfterPanelty = totalAmount + penaltyAmount ;
            amountAfterPaneltyWithTax = amountAfterTax +   penaltyAmount ;
            $("#totalAmountAfter").val(amountAfterPanelty);
            $("#taxAmountAfterDue").val(amountAfterPaneltyWithTax);
        }
    }else{
        // $('.text-danger').removeClass('hide-error-text');
    }

})
$(document).on('keyup','.penaltyAmount',function(){
    var amountAfterPanelty = 0;
    var amountAfterPaneltyWithTax = 0;
    $('.text-danger').addClass('hide-error-text');
    var penaltyAmount  = parseFloat($(this).val());
    var totalAmount    = parseFloat($("#totalAmountDue").val());
    var amountAfterTax =  parseFloat($("#taxAmountDue").val());
    var penaltyType    = $("input[name='penaltyType']:checked").val();
    if(penaltyAmount != "" && penaltyAmount != null && isNaN(penaltyAmount) === false ){

        if(penaltyType == 'P'){
            amountAfterPanelty = totalAmount +  ( totalAmount * penaltyAmount ) / 100;
            amountAfterPaneltyWithTax = amountAfterTax +  ( amountAfterTax * penaltyAmount ) / 100;
            populateTotalAmountFieldsAfterDue(amountAfterPanelty,amountAfterPaneltyWithTax);
        }else if(penaltyType == 'F'){
            amountAfterPanelty = totalAmount + penaltyAmount ;
            amountAfterPaneltyWithTax = amountAfterTax +   penaltyAmount ;
            populateTotalAmountFieldsAfterDue(amountAfterPanelty,amountAfterPaneltyWithTax);
        }
    }else{
        $('.text-danger').removeClass('hide-error-text');
    }

})

$('.fee-type-add-button').on('click',function(){

    if(schoolID != ''){
            $.ajax({
                url: apiUrl + '/feeTypes',
                method: 'GET',
                beforeSend: function () {
                    showProgressBar('p2-feeTypesTable');
                },
                data: {
                    schoolID: schoolID
                },
                success: function (res) {
                    if (!res.error) {
                        hideProgressBar('p2-feeTypesTable');
                        if (res.data.length >= 1) {

                            var table = `
                                <tr>
                                <td style="display:none;">
                                <input class="form-control hiddenFeeTypeId" type="text" value="" name="feeTypeID">
                                </td>
                                    <td>
                                    <select name=""  class="form-control filter-select fee-type-filter">
                                    <option>${messages.selectFeeType}</option>`;
                                $.each(res.data, function (index, value) {
                                    if(Number(value.isActive) == 1 && Number(value.isRecurring) != 1){
                                        table += `<option value="${value.feeTypeID}" data-feeTypeId="${value.feeTypeID}" data-taxRate="${value.taxRate}" data-feeTypeName="${value.feeType}">${value.feeType}</option>`;
                                    }
                                });
                            table += ` </select>
                                    <div id="p2-feeTypes" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                    </td>
                                    <td>
                                        <input class="form-control taxRate" type="text" value="" name="taxRate" disabled>
                                    </td>
                                    <td>
                                        <input class="form-control feeAmount" data-validation="number" data-validation-allowing="float" type="text" value="" name="feeTypeAmount" readonly required>
                                    </td>
                                    <td>
                                        <i class="fa fa-trash justify-content-center delete-fee-chalan" data-toggle="tooltip"  title="${messages.delete}" style="font-size:26px; color:red;"></i>
                                    </td>
                                </tr>
                                    `;
                        $('#fee-type-table-body').append(table);
                            enableDisabledFeeTypeDropdown();
                            hideMultipleProgressBar('fee-type-filter');
                        } else {
                            hideMultipleProgressBar('fee-type-filter');
                        }
                    } else {
                        hideProgressBar('p2-feeTypesTable');
                    }
                },
                error: function (err) {
                    window.requestForToken(err)
                    console.log(err);
                    hideProgressBar('p2-feeTypesTable');
                }
            })

            $(`select option[value=""]`).attr("disabled","disabled");
        }else{
        Swal({
            type: messages.error,
            title: messages.oops,
            text: messages.pleaseSelectSchool
        })
        $('#filter-schools').focus();
        $('#filter-schools').css();

    }
})
var previous = -1;
function getSeletedFeeTypes(){
    var selectedFeeTypes = [];
    $('.fee-type-filter').each(function(){
        if($(this).find(':selected').val() > -1 ){
            selectedFeeTypes.push($(this).find(':selected').val());
        }
    });
    return selectedFeeTypes;
}

function setSeletedFeeTypes(optionValue){
    $('.fee-type-filter').each(function(){
        $(this).find("option[value=" + optionValue + "]").attr('disabled', 'disabled');
    });
}

function removeDisableFromSeletedFeeTypes(optionValue){
    $('.fee-type-filter').each(function(){
        $(this).find("option").removeAttr('disabled');
    });
}

$(document).on('change','.fee-type-filter',function () {
    enableDisabledFeeTypeDropdown();
    if ($(this).find(':selected').val() > -1) {
        let currentFeeType = $(this).find(':selected').val()
        $(this).closest('tr').find('.hiddenFeeTypeId').val(currentFeeType);
        $(this).css('border','1px solid green');
        $(this).closest('tr').find('.feeAmount').removeAttr('readonly');
        $(this).closest('tr').find('.feeAmount').removeAttr('disabled');
        var feeTypeName = $(this).find(':selected').attr('data-feeTypeName');
        var taxRate = $(this).find(':selected').attr('data-taxRate');
        $(this).closest('tr').find('.taxRate').val(taxRate);
        $(this).closest('tr').find('.delete-fee-chalan').attr('data-id', $(this).val());
    } else {
        $(this).css('border','1px solid red');
        $(this).closest('tr').find('.feeAmount').attr('disabled', true);
    }
})

$(document).on('click','.delete-fee-chalan',function(){
    // var rowCount                      = 0;
    var voucherID  = $(this).attr('data-id');
    var feeTypeId = $(this).attr('data-feeTypeID');
    var feeVoucherDetailID               = $(this).attr('data-feeVoucherDetailID');

    if(feeVoucherDetailID != '' && feeVoucherDetailID !== 'undefined' && voucherID !== 'undefined' && voucherID != '' ){
        deleteFeeChallanDetail(voucherID, feeVoucherDetailID);
    }
    $("select").find("option[value=" + feeTypeId + "]").removeAttr('disabled');
    $(this).closest('tr').remove();
})

function getStudentFeeChallans() {
    var obj = {};
    if(billingPeriodID != ''){
        obj.billingPeriodID = billingPeriodID;
    }
    if(branchID != ''){
        obj.branchID = branchID;
    }
    if(classLevelID != ''){
        obj.classLevelID = classLevelID;
    }
    if(classID != ''){
        obj.classID = classID;
    }
    studentID = $('#filter-students').val()
    if(studentID != null && studentID != ''){
        obj.studentID = studentID;
    }
    if($('#payment-status-filter').val() != ''){
        obj.paymentStatus = $('#payment-status-filter').val();
    }
    if (tableData) {
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl + '/getFeeVoucher',
            type: 'POST',
            headers,
            beforeSend: function () {
            },
            data: obj,
            dataSrc: function (res) {
                response = parseResponse(res);
                var return_data = new Array();
                if(response.length > 0){
                    $('#print-all-vouchers').removeClass('hidden')
                    $('#download-csv').removeClass('hidden')
                    for (var j = 0; j < response.length; j++) {
                        if(response[j].isPublished == 1){
                            $('#unpublish-all-vouchers').removeClass('hidden')
                            $('#publish-all-vouchers').addClass('hidden')
                            publishedStatus = '0';

                            $('#unpublish-all-vouchers').attr('data-billing-period-id',response[j].billingPeriodID);
                            $('#unpublish-all-vouchers').attr('data-isPublished',response[j].isPublished);
                        }else{
                            $('#unpublish-all-vouchers').addClass('hidden')
                            $('#publish-all-vouchers').removeClass('hidden')
                            publishedStatus = '1';
                            $('#publish-all-vouchers').attr('data-billing-period-id',response[j].billingPeriodID);
                            $('#publish-all-vouchers').attr('data-isPublished',response[j].isPublished);
                        }

                        $('#print-all-vouchers').attr('href',__BASE__+'/student-fee-challan/'+response[j].billingPeriodID+'/voucher?schoolID='+schoolID+'&branchID='+branchID+'&classID='+classID)
                        rollNumber = response[j].rollNo.substring(0, 8)
                                return_data.push({
                                    'rollNo': `<span data-toggle="tooltip" title="${response[j].rollNo}">
                                    ${response[j].rollNo.length <= 7  ? response[j].rollNo : rollNumber+'...'}
                                    </span>`,
                                    'studentName':response[j].studentName,
                                    'invoiceDate': response[j].invoiceDate ? moment(response[j].invoiceDate).format('DD-MMM-YYYY') : '-',
                                    'feeAmountAfterTax': response[j].currencyCode+' '+addCommas((Math.round(response[j].feeAmountAfterTax * 100) / 100).toFixed(0)),
                                    // 'feeAmountAfterTax': response[j].channelFlag == '1' ? '---' : response[j].currencyCode+' '+addCommas((Math.round(response[j].feeAmountAfterTax * 100) / 100).toFixed(0)),
                                    'feeAmountAfterDueDate': response[j].currencyCode+' '+addCommas((Math.round(response[j].feeAmountAfterDueDate * 100) / 100).toFixed(0)),
                                    // 'feeAmountAfterDueDate':response[j].channelFlag == '1' ? '---' : response[j].currencyCode+' '+addCommas((Math.round(response[j].feeAmountAfterDueDate * 100) / 100).toFixed(0)),
                                    'Paid/Unpaid': `${
                                        !__ACCESS__  ?
                                        `<label class="label label-sm label-${response[j].paymentStatus !== 'null' && response[j].paymentStatus == 'P' ? 'success' : response[j].paymentStatus == 'R' ? 'warning' : 'danger'}"> ${response[j].paymentStatus !== 'null' && response[j].paymentStatus == 'P' ? messages.paid : response[j].paymentStatus == 'R' ? messages.underReview : messages.unpaid}</label>` : response[j].channelFlag == '1' && response[j].paymentStatus != 'R' ? `<span class="btn btn-success btn-sm" data-toggle="tooltip" title="${response[j].channelType}"><b>${messages.paid}</b> ${response[j].paidAmount && response[j].paidAmount !== null && response[j].paidAmount !== 0 ? 'PKR '+addCommas((Math.round(response[j].paidAmount * 100) / 100).toFixed(0)) : ''}</span>` : response[j].paymentStatus == 'R' ? `<span class="btn btn-warning btn-sm" data-toggle="tooltip" title="${messages.authorizationPending}">${messages.underReview}</span>` : `<label style="margin-top: 10px; margin-left: 24px; margin-bottom: -2px;" class="switchToggle"><input type="checkbox" class="payment-checkbox payment-checkbox-${j}" data-payment-status="payment-checkbox-${j}" data-voucherUUID="${response[j].voucherUUID}" data-voucherID = "${response[j].voucherID}" ${ response[j].paymentStatus != null && response[j].paymentStatus == "P" ? "checked" : "" } data-voucher='${handleApostropheWork(response[j])}'><span class="slider green round"></span></label>`
                                    }`,
                                    'Detail': `<button class='btn btn-default btn-xs fee-detail' data-response=' ${ handleApostropheWork(response[j])}' >${messages.viewDetail}<span><i class="fa fa-circle" style="color:${response[j].isPublished == 0 ? '#dc3545' : '#64bd63'};" data-toggle="tooltip" title="${response[j].isPublished == 0 ? messages.unPublished : messages.published}"></i></span></button>`,
                                    'action': `
                                   <div class="btn-group">
                                   <a  class="btn btn-sm btn-success" data-toggle="tooltip" title="${messages.print}" href="${__BASE__}/student-fee-challan/${response[j].billingPeriodID}/voucher?schoolID=${schoolID}&branchID=${branchID}&classID=${classID !== '' ? classID : response[j].classID }&studentID=${response[j].studentID}" target="_blank"><i class="fa fa-print"></i></a>
                                   ${__ACCESS__  ?
                                    `${  response[j].paymentStatus !== 'P' ?
                                    `<button class="btn-primary btn btn-xs record-edit list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[j])}'><i class="fa fa-pencil"></i></button>` : '' } ` : '' }
                                     ${__DEL__ && response[j].channelFlag == '1' ? '' : `<button class="btn-danger btn btn-xs delete-challan" data-challan-id='${response[j].voucherUUID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>`}
                                    </div>`
                                })
                    }
                    return return_data;
                }
            },
            error: function (xhr) {
                invokedFunctionArray.push('getStudentFeeChallans')
                window.requestForToken(xhr)
                if (xhr.status === 500) {
                    console.log(xhr.responseJSON.response.userMessage);
                }
                if (xhr.status === 404) {
                    $('#print-all-vouchers').addClass('hidden')
                    $('#publish-all-vouchers').addClass('hidden')
                    $('#unpublish-all-vouchers').addClass('hidden')
                    $('.dataTables_empty').html(messages.noDataAvailable);
                 }

            }
        },
        "columns": [
            {
                'data': 'rollNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'invoiceDate'
            },

            {
                'data': 'feeAmountAfterTax'
            },
            {
                'data': 'feeAmountAfterDueDate'
            },
            {
                'data': 'Paid/Unpaid'
            },
            {
                'data': 'Detail'
            },
            {
                'data': 'action'
            },
        ]
    });
}
{/* <a  class="btn btn-sm btn-success" href="${__BASE__}/student-fee-challan/${response[j].billingPeriodID}/voucher?schoolID=${schoolID}&branchID=${branchID}&classID=${classID}&studentID=${response[j].studentID}" target="_blank">Print PDF</a> */}

$(document).on('change', '.payment-checkbox', function(){
    var isChecked = $(this).prop('checked');
    var paymentStatusClass = $(this).attr('data-payment-status');
    var voucherID = $(this).attr('data-voucherID');
    var voucherUUID = $(this).attr('data-voucherUUID');
    var paymentStatus = isChecked ? 'P' : 'U';
    var voucherData = JSON.parse($(this).attr('data-voucher'));
    var amountBeforeDueDate = voucherData.feeAmountAfterTax
    var amountAfterDueDate = voucherData.feeAmountAfterDueDate
    var currencyCode = voucherData.currencyCode
    var dueDate = voucherData.dueDate
    var currentDate = moment(new Date()).unix()
    dueDate = moment(dueDate).unix()
    var due = "";
    if(currentDate > dueDate){
        due = "selected"
    }

    if(isChecked === true){
        $.confirm({
            title: messages.confirm,
            content: messages.areYouSure+'! <br/> '+messages.youWantChangeThePaymentStatus+'!</br>' +
            '<form action="" class="formName">' +
            '<div class="form-group"></br>' +
            '<label>'+messages.paymentDate+'</label>' +
            '<input type="date" class="date form-control datepicker" value="'+__CURRENTDATE__+'" data-validation="required" required /><span class="date-error hidden" style="color:red !important;">'+messages.dateIsRequired+'</span>' +
            '<label class="mt-2">'+messages.amount+'</label>' +
            '<select name="paidAmount" id="paidAmount" class="form-control">' +
                '<option value="'+amountBeforeDueDate+'">'+currencyCode+' '+addCommas(amountBeforeDueDate.toFixed(2))+' ('+messages.beforeDueDate+')'+'</option>'+
                '<option value="'+amountAfterDueDate+'" '+due+'>'+currencyCode+' '+addCommas(amountAfterDueDate.toFixed(2))+' ('+messages.afterDueDate+')'+'</option>'+
            '</select>' +
            '<label class="mt-2">'+messages.paymentSource+'</label>' +
            '<select name="channelType" id="channelType" class="form-control">' +
                '<option value="Cash">'+messages.cash+'</option>'+
                '<option value="Pay Order">'+messages.payOrder+'</option>'+
                '<option value="Bank Transfer">'+messages.bankTransfer+'</option>'+
                '<option value="Bank Deposit">'+messages.bankDeposit+'</option>'+
            '</select>' +
            '</div>' +
            '</form>',
            type: 'red',
            typeAnimated: true,
            buttons: {
                formSubmit: {
                    text: messages.submit,
                    btnClass: 'btn-blue',
                    action: function () {
                        var date = this.$content.find('.date').val();
                        var paidAmount = this.$content.find('#paidAmount').val();
                        var channelType = this.$content.find('#channelType').val();
                        if(!date){
                            $('.date').css({
                                "border": "1px solid red"
                            });
                            $('.date-error').removeClass('hidden');
                            return false;
                        }
                        $('.date').css({
                            "border": "1px solid green"
                        });
                        $('.date-error').addClass('hidden');
                        updateFeeVoucherPaymentStatus(paymentStatus, voucherUUID, paymentStatusClass, paidAmount, channelType, date);
                    }
                },
                cancel: {
                    text: messages.no,
                    action : function (){
                        if(paymentStatus == 'U'){
                            $('.'+paymentStatusClass).prop("checked", true)
                        }else{
                            $('.' +paymentStatusClass).prop("checked", false)
                        }
                    }
                }
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }else{
        $.confirm({
            title: messages.confirm,
            content: messages.areYouSure+'! <br/> '+messages.youWantToMarkTheVoucherUnpaid+'!</br>' +
            '<form action="" class="formName">' +
            // '<div class="form-group"></br>' +
            // '<label>Payment Date</label>' +
            // '<input type="date" class="date form-control datepicker" value="'+__CURRENTDATE__+'" data-validation="required" required /><span class="date-error hidden" style="color:red !important;">Date is Required.</span>' +
            // '</div>' +
            '</form>',
            type: 'red',
            typeAnimated: true,
            buttons: {
                formSubmit: {
                    text: messages.yes,
                    btnClass: 'btn-blue',
                    action: function () {
                        // var date = this.$content.find('.date').val();
                        // if(!date){
                        //     $('.date').css({
                        //         "border": "1px solid red"
                        //     });
                        //     $('.date-error').removeClass('hidden');
                        //     return false;
                        // }
                        // $('.date').css({
                        //     "border": "1px solid green"
                        // });
                        // $('.date-error').addClass('hidden');
                        updateFeeVoucherPaymentStatus(paymentStatus, voucherUUID, paymentStatusClass);
                    }
                },
                cancel: {
                    text: messages.no,
                    action : function (){
                        if(paymentStatus == 'U'){
                            $('.'+paymentStatusClass).prop("checked", true)
                        }else{
                            $('.' +paymentStatusClass).prop("checked", false)
                        }
                    }
                }
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }

})

function updateFeeVoucherPaymentStatus(paymentStatus, voucherUUID, paymentStatusClass, paidAmount, channelType, paymentDate){
    let data = {}
    data.voucherUUID = voucherUUID
    data.paymentStatus = paymentStatus
    if(paidAmount !== undefined){
        data.paidAmount = paidAmount
    }
    if(channelType !== undefined){
        data.channelType = channelType
    }
    if(channelType !== undefined){
        data.paymentDate = paymentDate
    }
    $.ajax({
        url: herokuUrl+'/setFeeStatus',
        method: 'POST',
        headers,
        data,
        // data: {
        //     voucherUUID: voucherUUID,
        //     paymentStatus:paymentStatus,
        //     paymentDate: paymentDate,
        //     paidAmount:paidAmount,
        //     channelType:channelType
        // },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.updated,
                    messages.feeVoucherHasBeenUpdated,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getStudentFeeChallans();
                if(paymentStatus == 'P'){
                    $('.'+paymentStatusClass).prop("checked", true)
                }
            }
        },
        error: function(xhr){
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.response.userMessage}`,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.response.userMessage}`,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.response.userMessage}`,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getStudentFeeChallans();
                return false;
            }
        }
    });
}
$(document).on('change','.fee-status-filter',function(){
    var status = $(this).val();
    if(status == 'P'){
        $('#paymentDate').removeAttr('readonly');
        $('#paymentDate').val(getDate());
        $('#paymentDateDiv').show(1000);
    }else{
        $('#paymentDate').attr('readonly',true);
        $('#paymentDate').val('');
    }

})


$(document).on('click', '.delete-challan', function(){
    deleteVoucherID = $(this).attr('data-challan-id');
    if(deleteVoucherID !== null && deleteVoucherID !== ''){
        Swal({
            title: messages.areYouSureQuestion,
            text: messages.youWontAbleToRevertThis,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: messages.yesDeleteIt,
            showCloseButton: true
          }).then((result) => {
            if (result.value) {
              deleteChallan();
            }
          })
    }else{
        swal(
            messages.error,
            messages.invalidChallan,
            'error'
        )
    }
})

$(document).on('click', '.delete-voucher-item', function(){
    deleteVoucherIDForItem = $(this).attr('data-voucherID');
    feeVoucherDetailID = $(this).attr('data-feeVoucherDetailID');
    var tr = $(this).closest('tr')
    if(deleteVoucherIDForItem !== null && feeVoucherDetailID !== null ){
        $.ajax({
            url: herokuUrl+'/setFeeVoucherDelete',
            method: 'POST',
            headers,
            data: {
                voucherID: deleteVoucherIDForItem,
                feeVoucherDetailID: feeVoucherDetailID
            },
            beforeSend: function(){
                disableButtonsToggle();
                showProgressBar(submitLoader);
            },
            success: function(res){
                var successSpan = $('#success-span')
                tr.fadeOut(1000);
                if(res.response === 'success'){
                    successSpan.html(`<p><strong class="text-sucess"> ${messages.feeDeletedSuccessfully}</strong></p>`);
                }
            },
            error: function(xhr){
                window.requestForToken(xhr)
                var errorSpan = $('#error-span')
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                    errorSpan.html(`<p><strong class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                    return false;
                }
                if(xhr.status === 400){
                    errorSpan.html(`<p><strong class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                    errorSpan.html(`<p><strong class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                    return false;
                }

                errorSpan.html(`<p><strong class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
        });
    }
})


function deleteChallan() {
    $.ajax({
        url: herokuUrl+'/setFeeVoucherDelete',
        method: 'POST',
        headers,
        data: {
            voucherUUID: deleteVoucherID
        },
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                Swal(
                    messages.deletedMessage,
                    messages.voucherHasBeenDeleted,
                    'success'
                )
                studentID = '';
                getStudentFeeChallans();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteChallan')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
                Swal(
                    messages.deletedMessage,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                )
                return false;
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                Swal(
                    messages.deletedMessage,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                )
                return false;
            }

            swal(
                messages.error,
                `${xhr.responseJSON.response.userMessage}`,
                'error'
            )
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

$('#student-fee-challan-form').on('submit', function (e) {
    e.preventDefault();
    if(!$('#invoiceNumberCheckBox').prop('checked') && $('#invoiceNumber').val() === ''){
        Swal(
            messages.errorMessage,
            messages.pleaseFillTheInvoiceNumberFirst,
            'error'
        )
        return false;
    }
    if($('#invoiceDate').val() === ''){
        Swal(
            messages.errorMessage,
            messages.pleaseFillTheInvoiceDateFirst,
            'error'
        )
        return false;
    }
    if($('#dueDate').val() === ''){
        Swal(
            messages.errorMessage,
            messages.pleaseFillTheDueDateFirst,
            'error'
        )
        return false;
    }
    if($('#penaltyValue').val() === ''){
        Swal(
            messages.errorMessage,
            messages.pleaseFillThePenaltyValueFirst,
            'error'
        )
        return false;
    }
    removeDisableFromSeletedFeeTypes();
    $.LoadingOverlay("show");
    feeChallanUrl = 'setFeeVoucherGenerate';
    setTimeout( () => {
        bulkArray = [];
        mainObject = {};
        var editHiddenField  = $('#editHiddenField').val();
        if(editHiddenField){
            mainObject['voucherID'] = editHiddenField;
        }
        var filtersArray = $('#filter-form').serializeArray();
        var feeChalanFormData = $(this).serializeArray();


        if(editHiddenField){
            feeChallanUrl = 'setFeeVoucher';
            var invoiceStartInfoArray  = feeChalanFormData.splice(0 , 7);
            var selectedAdditionalFeeTypeArray  = feeChalanFormData;
            filtersArray.forEach((value, index) => {
                mainObject[value.name]  = value.value;
            });
            invoiceStartInfoArray.forEach((value, index) => {
                // if($('#invoiceNumberCheckBox').prop('checked')){
                //     mainObject['invoiceStartNo']  =  null;
                // }
                mainObject[value.name]  = value.value;
            });
            var chunkedArray = chunkArray(feeChalanFormData, 3);
            chunkedArray.forEach((val, index) => {
                var detailObject = {};
                val.forEach((innerVal, innerIndex) => {
                    if(innerVal.name != 'feeType' && innerVal.name != 'taxRate'){
                        detailObject[innerVal.name] = innerVal.value;
                    }
                });
                bulkArray.push(detailObject);
            });
            mainObject['voucherDetails'] = bulkArray.length ? bulkArray : [];
        }else{
            var invoiceStartInfoArray;
             if($('#invoiceNumberCheckBox').prop('checked')){
                invoiceStartInfoArray  = feeChalanFormData.splice(0 , 5);
            }else{
                invoiceStartInfoArray  = feeChalanFormData.splice(0 , 6);
            }
            var selectedAdditionalFeeTypeArray  = feeChalanFormData;
            filtersArray.forEach((value, index) => {
                mainObject[value.name]  = value.value;
            });
            invoiceStartInfoArray.forEach((value, index) => {
                if(value.name == 'invoiceNumber'){
                    mainObject['invoiceStartNo']  = value.value;
                }else if(value.name == 'penaltyValue'){
                    mainObject['penaltyAmount']  = value.value;
                }else{
                    mainObject[value.name]  = value.value;
                }
            });
            selectedAdditionalFeeTypeArray.forEach((value, index) => {
                mainObject[value.name]  = value.value;
            });
            var chunkedArray = chunkArray(feeChalanFormData, 2);
            chunkedArray.forEach((val, index) => {
                var detailObject = {};
                val.forEach((innerVal, innerIndex) => {
                    if(innerVal.name != 'feeType' && innerVal.name != 'taxRate' ){
                        detailObject[innerVal.name] = innerVal.value;
                    }
                });
                bulkArray.push(detailObject);
            });
        }
        mainObject.voucherDetails = bulkArray.length ? bulkArray : [];
        setTimeout(()=>{
            data = clean(mainObject);
            showProgressBar(submitLoader);
            resetMessages();
            submitFeeChallan();
        },1000)
    },500)
});

$(document).on('change','.invoiceCheckBox',function(){
    if($(this).prop('checked')){
        $('.invoiceNumber').attr('disabled',true)
    }else{
        $('.invoiceNumber').removeAttr('disabled')
    }
})

// $(document).on('click', '#openNotesModal', function(){
//     $('#voucherNoteTextArea').summernote({
//         callbacks: {
//             onInit: function() {
//                 setTimeout(function(){
//                     $('.note-btn-group').removeClass('w-100')
//                 },1000)
//             }
//           },
//         height: 200,
//         toolbar: [
//           ['style', ['bold', 'italic', 'underline', 'clear']],
//           ['font', ['strikethrough', 'superscript', 'subscript']],
//         //   ['fontsize', ['fontsize']],
//           ['color', ['color']],
//           ['para', ['ul', 'ol', 'paragraph']],
//         //   ['height', ['height']],
//         //   ['table', ['table']]
//         ]
//       });
// });

$(document).on('click', '#openNotesModal', function(){
    $('#voucherNoteTextArea').summernote({
        callbacks: {
            onInit: function() {
                setTimeout(function(){
                    $('.note-btn-group').removeClass('w-100')
                },1000)
            }
          },
        height: 200,
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
        //   ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
        //   ['height', ['height']],
        //   ['table', ['table']]
        ]
    });
    $('#notesModal').modal({
        backdrop: 'static',
        keyboard: false,
        toggle: true,
        }
    );
})

$('#saveVoucherNote').on('click', function(){
    voucherNote = $('#voucherNoteTextArea').val()
    $('#voucherNote').val(voucherNote)
})

$(document).on('click', '#download-csv', function(){
    let data = {}
    if(user.isAdmin === 1){
        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            Swal(
                messages.errorMessage,
                messages.pleaseSelectBillingPeriod,
                'error'
            )
            return false
        }
    }else{
        if(branchID != ''){
            data.branchID = branchID;
        }
    }
    if($('#billing-period-filter').val() != ''){
        data.billingPeriodID = $('#billing-period-filter').val();
    }
    // else{
    //     Swal(
    //         messages.errorMessage,
    //         messages.pleaseSelectBranch,
    //         'error'
    //     )
    //     return false
    // }

    if($('#filter-classLevels').val() != ''){
        data.classLevelID = $('#filter-classLevels').val();
    }

    if($('#filter-classes').val() != null && $('#filter-classes').val() != ''){
        data.classID = $('#filter-classes').val();
    }
    // else{
    //     Swal(
    //         'Error!',
    //         'Please select billing period first.',
    //         'error'
    //     )
    //     return false
    // }
    if($('#filter-students').val() != null && $('#filter-students').val() != ''){
        data.studentID = $('#filter-students').val();
    }

    if($('#payment-status-filter').val() != ''){
        data.paymentStatus = $('#payment-status-filter').val();
    }

    $('.exportLoader').removeClass('hidden');
    $.ajax({
        url: herokuUrl+'/getFeeVoucherExport',
        method: 'POST',
        headers,
        data,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'fee-voucher-status.xlsx';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
                setTimeout(()=>{
                    $('.exportLoader').addClass('hidden');
                },500)
        }
    });
})

$(document).on('click', '#publish-all-vouchers', function(){
    voucherUUID = '';
    billingPeriodID = $(this).attr('data-billing-period-id');
    isPublished = 1;
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+' <br/> '+messages.youWantToPublishIt+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                setFeeVoucherStatus();
                }
            },
            cancel: function (){
            },
        }
    });
})

$(document).on('click', '#unpublish-all-vouchers', function(){
    voucherUUID = '';
    billingPeriodID = $(this).attr('data-billing-period-id');
    isPublished = 0;
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+' <br/> '+messages.youWantToUnPublishIt+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                setFeeVoucherStatus();
                }
            },
            cancel: function (){
            },
        }
    });
})

$('#publishedToggle').on('change', function(){
    voucherUUID = '';
    isCheckedValue = $(this).val()
    billingPeriodID = $(this).attr('billing-period-id')
    voucherUUID = $(this).attr('voucher-uuid')

    if(isCheckedValue == 1){
        $('#publishedToggleSpan').removeClass('green')
        isPublished = 0;
        $.confirm({
            title: messages.confirmMessage,
            content: messages.areYouSure+' <br/> '+messages.youWantToUnPublishIt+'</br>',
            type: 'red',
            typeAnimated: true,
            buttons: {
                formSubmit: {
                    text: messages.confirm,
                    btnClass: 'btn-blue',
                    action: function () {
                    setSingleFeeVoucherStatus();
                    }
                },
                cancel: function (){
                    if(isCheckedValue == 1){
                        $('#publishedToggle').prop("checked", true)
                        $('#publishedToggleSpan').addClass('green')
                    }else{
                        $('#publishedToggle').prop("checked", false)
                        $('#publishedToggleSpan').removeClass('green')
                    }
                },
            }
        });
    }else{
        $('#publishedToggleSpan').addClass('green')
        isPublished = 1;
        $.confirm({
            title: messages.confirmMessage,
            content: messages.areYouSure+' <br/> '+messages.youWantToPublishIt+'</br>',
            type: 'red',
            typeAnimated: true,
            buttons: {
                formSubmit: {
                    text: messages.confirm,
                    btnClass: 'btn-blue',
                    action: function () {
                    setSingleFeeVoucherStatus();
                    }
                },
                cancel: function (){
                    if(isCheckedValue == 0){
                        $('#publishedToggle').prop("checked", false)
                        $('#publishedToggleSpan').removeClass('green')
                    }else{
                        $('#publishedToggle').prop("checked", true)
                        $('#publishedToggleSpan').addClass('green')
                    }
                },
            }
        });
    }
})

function setFeeVoucherStatus(){
    data = {}
    data.isPublished = isPublished
    data.billingPeriodID = billingPeriodID
    if(voucherUUID !== ''){
        data.voucherUUID = voucherUUID
    }
    if(classID !== ''){
        data.classID = classID
    }
    $.ajax({
        url: herokuUrl+'/setFeeVoucherStatus',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
        },
        success: function(res){
            if(publishedStatus == 1){
                if(res.response === 'success'){
                    Swal(
                        messages.successMessage,
                        messages.feeVoucherPublished,
                        'success'
                    )
                }
            }else{
                if(res.response === 'success'){
                    Swal(
                        messages.successMessage,
                        messages.feeVoucherUnPublished,
                        'success'
                    )
                }
            }
            $('#exampleModal').modal('hide')

            studentID = '';
            getStudentFeeChallans();
        },
        error: function(xhr){
            invokedFunctionArray.push('setFeeVoucherStatus');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                Swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.response.userMessage}`,
                    'error'
                )
                return false;
            }
            if(xhr.status === 400){
                Swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.response.userMessage}`,
                    'error'
                )
                return false;
            }
            if(xhr.status === 500){
                Swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.reason}`,
                    'error'
                )
                return false;
            }
        }
    });
}

function setSingleFeeVoucherStatus(){
    data = {}
    data.isPublished = isPublished
    data.billingPeriodID = billingPeriodID
    if(voucherUUID !== ''){
        data.voucherUUID = voucherUUID
    }
    if(classID !== ''){
        data.classID = classID
    }
    $.ajax({
        url: herokuUrl+'/setFeeVoucherStatus',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
        },
        success: function(res){
            if(publishedStatus == 1){
                if(res.response === 'success'){
                    $('#successLabel').slideDown()
                    setTimeout(()=>{
                        $('#successLabel').slideUp()
                    },2000)
                }
            }else{
                if(res.response === 'success'){
                    $('#successLabel').slideDown()
                    setTimeout(()=>{
                        $('#successLabel').slideUp()
                    },2000)
                }
            }
            $('#exampleModal').modal('show')

            studentID = '';
            getStudentFeeChallans();
        },
        error: function(xhr){
            invokedFunctionArray.push('setFeeVoucherStatus');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                Swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.response.userMessage}`,
                    'error'
                )
                return false;
            }
            if(xhr.status === 400){
                Swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.response.userMessage}`,
                    'error'
                )
                return false;
            }
            if(xhr.status === 500){
                Swal(
                    messages.errorMessage,
                    `${xhr.responseJSON.reason}`,
                    'error'
                )
                return false;
            }
        }
    });
}

$(document).on('click', '.closeModalDiv', function(){
    $('#publishedToggle').removeAttr('voucher-uuid')
})

$(document).on('click', '.cancelBtnModal', function(){
    $('#publishedToggle').removeAttr('voucher-uuid')
})
