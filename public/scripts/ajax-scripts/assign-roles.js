var formData = null;
$(document).ready(function(){
    getAjaxSchools();
    // getUserList();
})

function submitUser(){
    $.ajax({
        url: herokuUrl+'/setUser',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                $('form#users-form').trigger('reset');
                getUserList();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitUser');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status !== 400 && xhr.status !== 422){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getUserList(){
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getUserList',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                schoolID: schoolID,
                branchID: branchID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'fullName': response[i].fullName ? response[i].fullName : '--',
                        'userName' : response[i].userName,
                        'email' : response[i].email,
                        'roleName' : response[i].roleName ? response[i].roleName : '--',
                        'action' : `${__ACCESS__ ? `<button class="btn-primary btn btn-xs assign-role-button" data-user-id="${response[i].userID}" data-toggle="tooltip" title="${messages.assignRole}" data-json='${JSON.stringify(response[i])}'><i class="fa fa-asterisk"></i></button>` : ''}`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getUserList');
                window.requestForToken(xhr)
                console.log(xhr);
                if(xhr.status !== 400 && xhr.status !== 422){
                    swal(
                        messages.error,
                        `${xhr.responseJSON.userMessage}`,
                        'error'
                    );
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'fullName'},
            {'data': 'userName'},
            {'data': 'email'},
            {'data': 'roleName'},
            {'data': 'action'}
        ]
    });

}

function setAssignRoles(){
    $.ajax({
        url: herokuUrl+'/setAssignRoles',
        method: 'POST',
        headers,
        data: formData,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                alert(messages.roleHasBeenSuccessfullyAssigned);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status !== 400 && xhr.status !== 422){
                swal(
                    messages.error,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                );
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click', '.assign-role-button', function(){
    formData = $('#filter-form').serialize();
    let userID = $(this).attr('data-user-id');
    if(roleID == ''){
        swal(
            messages.error,
            messages.pleaseSelectRole,
            'error'
        );
        return false;
    }

    if(userID != ''){
        formData = formData+'&userID='+userID;
        setAssignRoles();
    }else{
        swal(
            messages.error,
            messages.invalidUser,
            'error'
        );
    }
});
