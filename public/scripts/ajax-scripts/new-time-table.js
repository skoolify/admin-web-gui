var editFormErrors = $('#edit-form-errors');
subjectsForTimeTable = $('.filter-time-table-subjects');
var bulkArray = [];
var checkSubjectTypeArray = [];
$(document).ready(function () {
    getAjaxSchools();
    $('.remove-row-button').hide();
    weekdayNo = $("#filter-week-day option:selected").val();
});
var rowCount = 0;
var timetableTimeID  = '';
var currentTr = '';
$(document).on('click', '.remove-row-button', function(){
    timetableTimeID  = $(this).attr('data-timetableTimeID');
    tableID  = $(this).attr('data-tableID');
    currentTr = $(this).closest('tr');
    if(typeof timetableTimeID !== "undefined"){
        $.confirm({
            title: messages.confirmMessage,
            content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheTimeTable+'</br>',
            type: 'red',
            typeAnimated: true,
            buttons: {
                formSubmit: {
                    text: messages.confirm,
                    btnClass: 'btn-blue',
                    action: function () {
                        deleteTimeTableRecord();
                    }
                },
                cancel: function (){
                },
            }
        });
    }else{
        rowCount = 0;
        var rowCount = $('#add-time-table-form-table tr').length;
        if (rowCount > 1) {
            $(this).closest('tr').remove();
            rowCount = 0;
        }
    }

});

function deleteTimeTableRecord(){
    $.ajax({
        url: herokuUrl+'/deleteTimeTableTimings',
        method: 'POST',
        headers,
        data: {
            timetableTimeID: timetableTimeID,
            tableID: tableID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                currentTr.empty();
                rowCount = 0;
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteTimeTableRecord')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
$(document).on('click', '.time-table-record-delete', function () {

    var tableID = $(this).attr('data-tableId');
    swal({
        title: messages.areYouSureQuestion,
        text: messages.YouWontBeAbleToRevertThisTimeTable,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: messages.yesDeleteIt
      }).then((result) => {
        if (result.value) {
              $(this).closest('tr').fadeOut(1000);
            $.ajax({
                url: herokuUrl + '/deleteTimeTable',
                type: 'POST',
                headers,
                async : false,
                data: {
                    tableID: tableID
                },
                beforeSend: function () {
                    buttonGroup = ''
                },
                success: function (roleResponse) {

                    swal(
                        messages.deletedMessage,
                        messages.timeTableDeletedSuccessfully,
                        'success'
                      )

                },
                error:function(xhr){
                    console.log(xhr);

                },
            });
        }
      })
});
$(document).on('click', '#add-new-row', function () {
    rowCount = 0;
    $("#add-time-table-form-table").each(function () {
        var tds = '<tr>';
        jQuery.each($('tr:last td', this), function () {
            tds += '<td>' + $(this).html() + '</td>';
        });
        tds += '</tr>';
        if($('tbody', this).length > 0) {
            $('tbody', this).append(tds);
        }else{
            $(this).append(tds);
        }
        setRemoveButton()
    });
    $('#add-time-table-form-table tr:last').children('td:first').find('.tableID').val(0);
    $('#add-time-table-form-table tr:last').children('td:first').find('.filter-time-table-subjects').val('')
    $('#add-time-table-form-table tr:last').find('td:eq(1)').find("input[name='startTime']").val('')
    $('#add-time-table-form-table tr:last').find('td:eq(2)').find("input[name='endTime']").val('')
    setClockpickerClass();
});

$(document).on('change','.filter-time-table-subjects',function(){
    // filterTimeTableSubjects();
})
function getSeletedOptions(className){
    var selectedOptionValueArray = [];
    $('.'+ className).each(function(){
        if($(this).find(':selected').val() > -1 && $(this).find(':selected').val() !== ""  ){
            selectedOptionValueArray.push($(this).find(':selected').val());
        }
    });
    return selectedOptionValueArray;
}

function setSeletedOptions(className, optionValue){
    $('.' +  className).each(function(){
        $(this).find("option[value=" + optionValue + "]").attr('disabled', true);
    });
}

function removeDisableAttrFromSeletedDropdown(className){
    $('.' + className).each(function(){
        $(this).find("option").removeAttr('disabled');
    });
}

function setRemoveButton(){
    var rowCount = $('#add-time-table-form-table tr').length;
    if (rowCount > 1) {
        $('.remove-row-button').removeClass('hidden');
    }
    if (rowCount > 10)
        $('#add-time-table-form-table tr:last').remove();
}

$(document).on('click', '#pop', function () {
    var row = $(this).attr('data-row');
    $('#' + row).slideToggle("slow");
});
// getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID);

classes.on('change', function(){
    classID = $(this).val();
    getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID);
});

function getAjaxTimeTableSubjects(classID, branchID, schoolID, classLevelID) {
    let data = {};
    if(user.userID !== ''){
        data.userID = user.userID

    }
    if(schoolID !== ''){
        data.schoolID = schoolID

    }
    if(branchID !== ''){
        data.branchID = branchID
    }
    if(classLevelID !== ''){
        data.classLevelID = classLevelID
    }
    if(classID !== ''){
        data.classID = classID
    }
    if (classLevelID.length > 0) {
        $.ajax({
            // url: apiUrl + '/subjects',
            // method: 'GET',
            url: herokuUrl+'/getUserAllSubjects',
            method: 'POST',
            headers,
            beforeSend: function () {
                $('.filter-time-table-subjects').each(function () {
                    $(this).empty();
                });
                $('.filter-time-table-subjects').each(function () {
                    $(this).append(`<option value="">${messages.selectSubject}</option>`);
                });
                $('.filter-time-table-subjects').each(function () {
                    $(this).attr('disabled');
                });
                showProgressBar('p2-subjects');
                subjectArray = []
                checkSubjectTypeArray = []
            },
            data,
            // data: {
            //     classID: classID,
            //     branchID: branchID,
            //     schoolID: schoolID,
            //     classLevelID: classLevelID
            // },
            success: function (res) {
                if (!res.error) {

                    $('.filter-time-table-subjects').each(function () {
                        $(this).removeAttr('disabled');
                    });

                    if (res.response) {
                        let obj = {
                            'Subjects': res.response.subjects.length ? res.response.subjects : [],
                            'NonSubjects': res.nonsubjects.length ? res.nonsubjects : []
                        }

                        obj.Subjects.forEach(function(value, index){
                            subjectArray.push(value)
                        })
                        obj.NonSubjects.forEach(function(value, index){
                            subjectArray.push(value)
                        })
                        subjectArray.forEach(function(value, index) {
                            // console.log(value,'value')
                            checkSubjectTypeArray.push(value)
                            $('.filter-time-table-subjects').each(function () {
                                if(value.isActive !== undefined && value.isActive === 1 ){
                                    $(this).append(`<optgroup label="Swedish Cars"></optgroup>`)
                                    $(this).append(`<option value="${value.subjectID}" ${value.subjectID == subjectID ? 'selected' : ''}>${value.subjectName}</option>`);
                                }
                            })

                        });
                        hideProgressBar('p2-subjects');
                    } else {
                        hideProgressBar('p2-subjects');
                    }
                } else {
                    // subjectsForTimeTable.attr('disabled');
                    $('.filter-time-table-subjects').each(function () {
                        $(this).attr('disabled');
                    });
                    hideProgressBar('p2-subjects');
                }
            },
            error: function (err) {
                invokedFunctionArray.push('getAjaxTimeTableSubjects')
                window.requestForToken(err)
                console.log(err);
                hideProgressBar('p2-subjects');
            }
        })
    }
}

function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

function submitTimeTable() {
    $.ajax({
        url: herokuUrl + '/setTimeTableArray',
        method: 'POST',
        headers,
        data: {'array': bulkArray},
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                editFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#time-table-form').trigger('reset');
                $('form#edit-time-table-form').trigger('reset');
                getTimeTable();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                editFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$('#filter-week-day').on('change', function(){
    weekdayNo = $(this).val();
    getTimeTable();
});

function getTimeTable() {
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl + '/getTimeTableTimings',
            type: 'POST',
            headers,
            beforeSend: function () {
                timeTableArray = []
            },
            data: {
                classID: classID,
                weekdayNo:weekdayNo
            },
            dataSrc: function (res) {
                response = parseResponse(res);
                timeTableArray = response;
                var return_data = new Array();
                for (var i = 0; i < response.length; i++) {
                    return_data.push({
                        'sNo': i + 1,
                        'teacher': response[i].fullname,
                        'subjects': response[i].subjectName,
                        'startTime': response[i].startTime,
                        'endTime': response[i].endTime
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getTimeTable')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns": [{
                'data': 'sNo'
            },
            {
                'data': 'teacher'
            },
            {
                'data': 'subjects'
            },
            {
                'data': 'startTime'
            },
            {
                'data': 'endTime'
            }
        ]
    });

}
function setClockpickerClass(){
    $('.clockpicker').each(function(){
        $(this).clockpicker({
            donetext: 'Done'
        });
    })
}

function iterateTimeTableData(){
    var timeTableData = $('#add-time-table-form-table tbody')
    timeTableData.empty();
    if(typeof timeTableArray !== 'undefined' && timeTableArray.length > 0){
        timeTableArray.forEach(function(value, index){
            timeTableTrs = '';
            timeTableTrs +=
                `<tr>
                    <td width="35%">
                        <div class="col-lg p-t-20">
                            <div class="form-group">
                                <label for="Subject">Subject</label>
                                <input type="hidden" name="timetableTimeID" value="${value.timetableTimeID}" />
                                <input type="hidden" name="tableID" class="tableID" id="tableID-${index}" value="${value.tableID}" />
                                <select id="filter-time-table-subjects-${index}" name="subjectID"  class="form-control filter-select filter-time-table-subjects"  data-validation  data-validation-error-msg="${messages.subjectIsRequired}" style="height:49px !important">
                                    <option value="">${messages.selectSubject}</option>`;
                                    subjectArray.forEach(function(subVal, subIndex){
                                        timeTableTrs += `
                                        <option value="${subVal.subjectID}" ${subVal.subjectID == value.subjectID ? 'selected' : '' } >${subVal.subjectName}</option>`;
                                    })
                        timeTableTrs += `</select>
                                <div id="p2-subjects" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-lg p-t-20">
                            <div class="form-group">
                                <label for="startTime" >Time From
                                </label>
                                <div class="input-group clockpicker">
                                    <input class = "form-control clockpicker" name="startTime"  type = "text" id = "startTime" placeholder="Time From" value="${value.startTime}"  data-validation="required" data-validation-error-msg="${messages.startTimeIsRequired}" autocomplete="off"/>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-lg p-t-20">
                            <div class="form-group">
                                <label for="endTime" >Time To</label>
                                <div class="input-group clockpicker">
                                    <input class = "form-control clockpicker" name="endTime"  type = "text" id="endTime" placeholder="Time To" value="${value.endTime}" data-validation="required" data-validation-error-msg="${messages.endTimeIsRequired}" autocomplete="off" />
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-lg p-t-20">
                            <div class="form-group">
                                <button type="button" data-timetableTimeID="${value.timetableTimeID}" data-tableID="${value.tableID}" style="margin-top:35px;height:35px;" class="btn btn-danger btn-xs d-inline remove-row-button hidden" data-toggle="tooltip" title="${messages.removeRow}"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </td>
                </tr>
                `;
                timeTableData.append(timeTableTrs);
        })
        setRemoveButton();
        // setTimeout(() => {
        //     filterTimeTableSubjects();
        // },1000)

    }else{
        timeTableTrs = '';
            timeTableTrs +=
                `<tr>
                <td width="35%">
                    <div class="col-lg-12 p-t-20">
                        <div class="form-group">
                            <label for="Subject">Subject</label>
                            <input type="hidden" name="tableID" id="tableID" value="0" />
                            <input type="hidden" name="timetableTimeID" id="tableID" value="0" />
                            <select id="filter-time-table-subjects" name="subjectID" style="height:49px !important" class="form-control filter-select filter-time-table-subjects"  data-validation-error-msg="${messages.subjectIsRequired}">
                                <option value="">${messages.selectSubject}</option>`;
                                subjectArray.forEach(function(subVal, subIndex){
                                    timeTableTrs += `
                                    <option value="${subVal.subjectID}" >${subVal.subjectName}</option>`;
                                })
                    timeTableTrs += `</select>
                            <div id="p2-subjects" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="col-lg p-t-20">
                        <div class="form-group">
                            <label for="startTime" >Time From
                            </label>
                            <div class="input-group clockpicker">
                                <input class = "form-control" name="startTime"  type = "text" id = "startTime" placeholder="Time From" value=""  data-validation="required" data-validation-error-msg="${messages.startTimeIsRequired}" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="col-lg p-t-20">
                        <div class="form-group">
                            <label for="endTime" >Time To</label>
                            <div class="input-group clockpicker">
                                <input class = "form-control clockpicker" name="endTime"  type = "text" id = "endTime" placeholder="Time To" value="" data-validation="required" data-validation-error-msg="${messages.endTimeIsRequired}" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="col-lg p-t-20">
                        <div class="form-group">
                            <button type="button" style="margin-top:35px;height:35px;" class="btn btn-danger btn-xs d-inline remove-row-button hidden" data-toggle="tooltip" title="${messages.removeRow}"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </td>
            </tr>`;
            timeTableData.append(timeTableTrs);
            $('#time-table-form').removeAttr('data-isedit');
    }
    setClockpickerClass();

}

function filterTimeTableSubjects(){
    var selectedOptionValues = getSeletedOptions('filter-time-table-subjects');
    removeDisableAttrFromSeletedDropdown('filter-time-table-subjects');
    selectedOptionValues.forEach(function(val,index){
        setSeletedOptions('filter-time-table-subjects',val);
    });
}

//time table Edit Record Parsing
$(document).on('click', '.time-table-record-edit', function () {
    var json = JSON.parse($(this).attr('data-json'));
    var keys = Object.keys(json);
    var timeArray = ["time1", "time2", "time3", "time4", "time5", "time6", "time7", "time8", "time9", "time10"];

    if (keys.length >= 1) {
        editHiddenField.attr('name', keys[0]);
        var count = 1;
        keys.forEach((value, index) => {
            if (value == 'isActive') {
                if (json[value] == 1) {
                    $('#activeField0').removeAttr('checked');
                    $('#activeField1').attr('checked', true);
                } else {
                    $('#activeField1').removeAttr('checked');
                    $('#activeField0').attr('checked', true);
                }
            } else {

                if (jQuery.inArray(value, timeArray) != -1) {
                    if(json[value] != null){
                        var splitedArray = json[value].split("-");
                        var timeFrom = 'time-from' + count;
                        var timeTo = 'time-to' + count;
                        $('[name=' + timeFrom + ']').val(splitedArray[0].replace(' ', ''));
                        $('[name=' + timeTo + ']').val(splitedArray[1].replace(' ', ''));
                    }
                    count++;
                }
                $('[name=' + value + ']').val(json[value]);
            }
            if (value.includes('ID')) {
                if (typeof value !== 'undefined') {
                    window[value] = json[value];
                }
            }
        });
    }
    resetMessages();
    hideResultsList();
    showEditFormContainer();

});

function showEditFormContainer() {
    $("#time-table-edit-form-container").slideDown('slow');
    filterFormTitle.text(messages.create).css({
        'color': '#2fa7ff'
    });
    hideResultsList();
}

function hideEditFormContainer() {
    $("#time-table-edit-form-container").slideUp();
    filterFormTitle.text(messages.filters).css({
        'color': 'black'
    });
    showResultsList();
}

$('#hide-time-table-edit-form-container').on('click', function () {
    hideEditFormContainer();
    editHiddenField.removeAttr('name');
    editHiddenField.removeAttr('value');
    $(this).parents('form:first').trigger('reset');
});

$('#hide-time-table-edit-form-container').on('click', function () {
    hideAddFormContainer();
    editHiddenField.removeAttr('name');
    editHiddenField.removeAttr('value');
    $(this).parents('form:first').trigger('reset');
});

$('#time-table-form').on('submit', function (e) {
    e.preventDefault();
    var isEdit = $(this).attr('data-isedit');
    classID = $('#filter-classes').val();
    bulkArray = [];
    var timeTables = $('#time-table-form').serializeArray();
    var chunkedArray = chunkArray(timeTables, 5);
    var val = '';
    var subjectId = null;
    chunkedArray.forEach((val, index) => {
        subjectId = null
        val.forEach((res, i) => {
            if(res.name === 'subjectID'){
                subjectId = res.value;
            }
        })
        for(var i = 0; i < checkSubjectTypeArray.length;i++){
            if(checkSubjectTypeArray[i].subjectID == subjectId){
                let obj = {}
                obj['classID'] = classID;
                obj['weekdayNo'] = weekdayNo;
                obj['subjectType'] = checkSubjectTypeArray[i].subjectType;
                val.forEach((value, innerIndex) => {
                    if(value.name == 'tableID' || value.name == 'timetableTimeID'){
                        if( Number(value.value) > 0){
                            obj[value.name] = value.value;
                        }
                    } else{
                        obj[value.name] = value.value
                    }
                })
                bulkArray.push(obj);
            }
        }
    });
    if(bulkArray.length > 0){
        showProgressBar(submitLoader);
        resetMessages();
        submitTimeTable();
    }
});


$('#edit-time-table-form').on('submit', function (e) {
    e.preventDefault();
    var formData = $('#edit-time-table-form').serializeArray();
    var tableId = formData.shift();
    var result = chunkArray(formData, 3);
    var timeTableForm = '';
    var count = 0;
    var subjectID;
    var time;
    var counter = 1;
    var totalTime = '';
    result.forEach((val, index) => {
        val.forEach((value, innerIndex) => {
            if(value.value != ""){
                if (innerIndex == 0) {
                    timeTableForm += "subjectID" + counter + "=" + value.value + "&";
                } else if (innerIndex == 1) {
                    totalTime += value.value + " - ";
                } else if (innerIndex == 2) {
                    totalTime += value.value
                    if (index == 9) {
                        timeTableForm += "time" + counter + "=" + totalTime;
                    } else {
                        timeTableForm += "time" + counter + "=" + totalTime + "&";
                    }
                }
        }
            count = 1;
        })
        totalTime = '';
        counter++;
    })
    data = "tableID=" + tableId.value + '&' + $('#filter-form').serialize() + '&' + timeTableForm;
    showProgressBar(submitLoader);
    resetMessages();
    submitTimeTable();
});
