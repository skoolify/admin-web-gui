$(document).ready(function(){
    getTermAndCondition();

    new FroalaEditor('#floaraEditor', {
        direction: 'ltr',
        heightMin: 200,
        heightMax: 200,
        placeholderText: false,
        codeBeautifierOptions: {
            end_with_newline: true,
            indent_inner_html: true,
            extra_liners: "['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre', 'ul', 'ol', 'table', 'dl']",
            brace_style: 'expand',
            indent_char: ' ',
            indent_size: 4,
            wrap_line_length: 0
        }
    })

    new FroalaEditor('#arabicFloaraEditor', {
        direction: 'rtl',
        placeholderText: false,
        // placeholderText: 'اطبع شيئا',
        language: 'ar',
        heightMin: 200,
        heightMax: 200,
        codeBeautifierOptions: {
            end_with_newline: true,
            indent_inner_html: true,
            extra_liners: "['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre', 'ul', 'ol', 'table', 'dl']",
            brace_style: 'expand',
            indent_char: ' ',
            indent_size: 4,
            wrap_line_length: 0
        }
    })
});

function submitTermAndCondition(){
    $.ajax({
        url: herokuUrl+'/setTnC',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);
                getTermAndCondition();
                $('#term-condition-form').trigger('reset');
                $('.note-editable').html('');
            }
            $('.submitLoader').addClass('hidden')
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitTermAndCondition')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getTermAndCondition(){
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
            'ajax': {
            url: herokuUrl+'/getTnC',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {},
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let en = handleApostropheWork(response[i].tncText);
                    let ar = handleApostropheWork(response[i].tncText_ar);
                    delete response[i].tncText;
                    delete response[i].tncText_ar;
                    return_data.push({
                        'sNo' : i+1,
                        'version': response[i].version ? response[i].version : '-',
                        'insertionDate': moment(response[i].insertionDate).format('DD-MM-YYYY hh:mm A'),
                        'updateDate': moment(response[i].updateDate).format('DD-MM-YYYY hh:mm A'),
                        'isPublished' : `<label class="label label-sm label-${response[i].isPublished ? 'success' : 'danger'}"> ${response[i].isPublished ? 'Published' : 'UnPublished'}</label>`,
                        'addedBy': user.fullName,
                        'action' : `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-tnc='${en}' data-tnc-ar='${ar}' data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getTermAndCondition')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'version'},
            {'data': 'insertionDate'},
            {'data': 'updateDate'},
            {'data': 'isPublished'},
            {'data': 'addedBy'},
            {'data': 'action'}
        ]
    });
}

$('#term-condition-form').on('submit', function(e){
    e.preventDefault();
    $('.submitLoader').removeClass('hidden')
    // console.log($('#floaraEditor').val(),'FloaraEditor')
    // console.log($('#arabicFloaraEditor').val(),'arabicFloaraEditor')
    // data.tncText = btoa($('#floaraEditor').val())
    // data.tncText_ar = btoa($('#arabicFloaraEditor').val())
    // if($('#editHiddenField').val() !== ''){
    //     data.tncID = $('#editHiddenField').val()
    // }
    // data.version = $('#version').val()
    // data.isPublished = $('#isPublished0').val()
    // data.userID = user.userID

    value = $(this).serialize()
    // console.log(value,'value')
    // return
    setTimeout(() => {
        data = value.replace(/[^&]+=\.?(?:&|$)/g, '')+'&userID='+user.userID;
    }, 100);
    showProgressBar(submitLoader);
    resetMessages();
    setTimeout(() => {
        submitTermAndCondition();
    }, 500);
});

