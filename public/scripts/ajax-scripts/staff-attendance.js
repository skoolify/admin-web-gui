$('#searchSettlementReport').on('click', function(e){
    e.preventDefault();
    getStaffAttendenceSummary();

});

$(document).ready(function(){
    getAjaxSchools();
})

var rowId = 0;
var table = $('#staff-attendance-detail-table tbody');

function getStaffAttendenceSummary(){
    data = {}
    if(user.isAdmin == 1){
        if($('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val();
        }else{
            data.schoolID = user.schoolID;
        }

        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            data.branchID = user.branchID;
        }
    }else{
        data.schoolID = user.schoolID;
        data.branchID = user.branchID;
    }
    dateFromVal = $('#filter-date-from').val();
    dateToVal = $('#filter-date-to').val();

    if(dateFromVal !== ''){
        data.dateFrom = dateFromVal
    }
    if(dateToVal !== ''){
        data.dateTo = dateToVal
    }

    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/getStaffAttendenceSummary',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i < response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'staffName' : response[i].fullName,
                        'month' : response[i].monthName,
                        'present' : response[i].Present,
                        'absent' : response[i].Absent,
                        'late' : response[i].Late,
                        'leave' : response[i].Leave,
                        'holiday' : response[i].Holiday,
                        'total' : response[i].Total,
                        'detail' : `<button class='btn btn-default btn-xs view-detail' data-response='${JSON.stringify(response[i])}'>${messages.viewDetail}</button>`,
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStaffAttendenceSummary');
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                if(xhr.status === 400){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data' : 'staffName'},
            {'data' : 'month'},
            {'data' : 'present'},
            {'data' : 'absent'},
            {'data' : 'late'},
            {'data' : 'leave'},
            {'data' : 'holiday'},
            {'data' : 'total'},
            {'data' : 'detail'},
        ]
    });
}

$(document).on('click', '.view-detail', function(){
    obj = JSON.parse($(this).attr('data-response'))
    getStaffAttendenceReport(obj)
})

function getStaffAttendenceReport(obj){
    data = {}
    $.LoadingOverlay("show");
    data.userUUID = obj.userUUID
    if(user.isAdmin == 1){
        if($('#filter-schools').val() !== ''){
            data.schoolID = $('#filter-schools').val();
        }else{
            data.schoolID = user.schoolID;
        }

        if($('#filter-branches').val() !== ''){
            data.branchID = $('#filter-branches').val();
        }else{
            data.branchID = user.branchID;
        }
    }else{
        data.schoolID = user.schoolID;
        data.branchID = user.branchID;
    }

    dateFromVal = $('#filter-date-from').val();
    dateToVal = $('#filter-date-to').val();

    if(dateFromVal !== ''){
        data.dateFrom = dateFromVal
    }
    if(dateToVal !== ''){
        data.dateTo = dateToVal
    }

    $.ajax({
        url: herokuUrl+'/getStaffAttendenceReport',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data,
        success: function(res){
            response = parseResponse(res);
            table.empty();
            if (response.length > 0) {
                response.forEach((val, index) => {
                    table.append(`
                    <tr>
                        <td  class="p-2"> ${ val.fullname } </td>
                        <td  class="p-2"> ${ moment(val.attendanceDate).format('DD-MMM-YYYY') } </td>
                        <td  class="p-2"> ${ val.Arrival } </td>
                        <td  class="p-2"> ${ val.Departure } </td>
                        <td  class="p-2"> ${ val.attendanceStatus } </td>
                    </tr>`);
                })
            } else {
                table.append(`<tr><td colspan="3" style="text-align:center !important">${messages.recordNotFound}</td></tr>`);
            }
            $.LoadingOverlay("hide");
            $('#exampleModal').modal('toggle');
            $('#exampleModal').modal('show');

        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        //   if (xhr.status === 404) {
            // if(__ROUTE__ == 'student-fee-challan'){
                $.LoadingOverlay("hide");
            // }
            $('#exampleModal').modal('toggle');
            $('#exampleModal').modal('show');
        //  }
        }
    });
}
