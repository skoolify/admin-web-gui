var examOverallID = null;
var subjectsArr = [];
var nonSubjectsArr = [];
var subjectsListTable = $('#subjects-list-table');
var nonSubjectsListTable = $('#non-subjects-list-table');
var nonSubjectSelect = $('.non-subjects-list');
var addNonSubject = $('#add-non-subject');
var addSubject = $('#add-subject');
var subjectsFieldList = ['subjectId', 'totalMarks', 'obtainedMarks', 'grade', 'notes'];
var nonSubjectsFieldList = ['nonSubjectID', 'gradeNS', 'notesNS'];
var overallResultListTable = $('#overall-result-list-table');
var overallResultForm = $('#overall-result-form');
var subjectResultListTable = $('#subject-result-list-table');
var subjectResultForm = $('#subject-result-form');
var nonSubjectResultListTable = $('#non-subject-result-list-table');
var nonSubjectResultForm = $('#non-subject-result-form');
var classLevelDropdown = $('.filter-classLevels');
// Ready Function
$(document).ready(function () {
    getAjaxSchools();
    $('#total-marks-div').addClass('hidden');
})

addSubject.on('click', function () {
    let obj = {};
    var found = false;
    subjectsFieldList.forEach((value, index) => {
        let v = $('[name=' + value + ']').val();
        if (value === 'subjectId') {
            obj.subjectName = $('[name=' + value + '] option:selected').text();
        }
        obj[value] = v;
    });
    subjectsArr.forEach((val, index) => {
        if (val.subjectId == obj.subjectId) {
            found = true;
        }
    });
    if (!found) {
        subjectsArr.push(obj);
    }
    iterateSubjectResults();
});
// Subject result Tab
$('#subject-result-link').click(function(){
    // $("#filter-classes option:selected").prop("selected", false);
    $('.student-subject-and-nonsubject-div').hide();
    $('#student-filter').addClass('hidden');
    $('#non-subject').addClass('hidden');
    $('#total-marks-div').removeClass('hidden');
    $('#subject').removeClass('hidden');
    // $('#student-result-table tbody').empty();
    // $('#student-result-table tbody').append('<tr><td colspan="4"><strong>Select Class                          First</<strong></td></tr>');
    $('.result-upper-heading').text(messages.subjectResults);

});
// Non-Subject Result Tab
$('#non-subject-result-link').click(function(){
    // $("#filter-classes option:selected").prop("selected", false);
    $('.student-subject-and-nonsubject-div').hide();
    $('#student-filter').addClass('hidden');
    $('#subject').addClass('hidden');
    $('#non-subject').removeClass('hidden');
    $('#total-marks-div').addClass('hidden');
    // $('#student-non-subject-result-table tbody').empty();
    // $('#student-non-subject-result-table tbody').append('<tr><td colspan="4"><strong>Select Class                          First</<strong></td></tr>');
    $('.result-upper-heading').text(messages.nonSubjectResults);

});
// OverAll Subject Tab
$('#overall-result-link').click(function(){
    $('.student-subject-and-nonsubject-div').hide();
    $('#subject').removeClass('hidden');
    $('#non-subject').removeClass('hidden');
    $('#total-marks-div').addClass('hidden');
    $('#student-filter').removeClass('hidden');
    $('.result-upper-heading').text(messages.overallResults);
});

$('#show-non-subject-result-add-form-container').on('click',function(){
     $("#non-subject-result-form").slideDown();
    filterFormTitle.text(messages.create).css({
        'color': '#2fa7ff'
    });
    $('#non-subject-results-list').slideUp();
});

$('#hide-non-subject-add-form-container').on('click', function () {
    $('#non-subject-result-form').slideUp();
    $('#non-subject-results-list').slideDown();
    editHiddenField.removeAttr('name');
    editHiddenField.removeAttr('value');
    filterFormTitle.text('Filters').css({
        'color': 'black'
    });
    $(this).parents('form:first').trigger('reset');
});

$('#show-subject-result-add-form-container').on('click',function(){
    $('#subject-result-form').trigger('reset');
    $('#subject-result-add-fields').hide();
    $('#subject-result-list-container').show();
    $("#subject-result-form").slideDown();
   filterFormTitle.text(messages.create).css({
       'color': '#2fa7ff'
   });
   $('#subject-results-list').slideUp();
});

$('#hide-subject-add-form-container').on('click', function () {
   $('#subject-result-form').slideUp();
   $('#subject-results-list').slideDown();
   $('#subject-result-hidden-field').removeAttr('name');
   $('#subject-result-hidden-field').removeAttr('value');
   filterFormTitle.text('Filters').css({
       'color': 'black'
   });
   $(this).parents('form:first').trigger('reset');
});

$('#show-overall-result-add-form-container').on('click',function(){
    overallResultForm.slideDown();

   filterFormTitle.text(messages.create).css({
       'color': '#2fa7ff'
   });
   $('#overall-result-list').slideUp();
});

$('#hide-overall-subject-add-form-container').on('click', function () {
    overallResultForm.slideUp();
    $('#overall-result-list').slideDown();
    editHiddenField.removeAttr('name');
    editHiddenField.removeAttr('value');
    filterFormTitle.text('Filters').css({
        'color': 'black'
    });
    $(this).parents('form:first').trigger('reset');
});


addNonSubject.on('click', function () {
    let obj = {};
    var found = false;
    nonSubjectsFieldList.forEach((value, index) => {
        let v = $('[name=' + value + ']').val();
        if (value === 'nonSubjectID') {
            obj.nonSubjectName = $('[name=' + value + '] option:selected').text();
        }
        obj[value] = v;
    });

    nonSubjectsArr.forEach((val, index) => {
        if (val.nonSubjectID == obj.nonSubjectID) {
            found = true;
        }
    });
    if (!found) {
        nonSubjectsArr.push(obj);
    }

    iterateNonSubjectResults();
});

$(document).on('change','#filter-subjects',function(){
    subjectID = $(this).val();
    getSubjectResult();
});

$(document).on('change','#filter-non-subjects',function(){
    nonSubjectID = $(this).val();
    getNonSubjectResult();
});
$(document).on('change','#filter-classes', function () {
    showProgressBar('p2-students-result-list');
    showProgressBar('p2-non-subject-students-result-list');
    classID = $(this).val();
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {

        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                $('#student-result-table tbody').empty();
                $('#student-non-subject-result-table tbody').empty();
                if(res.data.length){
                    $.each(res.data, function(index, value){
                        $('#student-result-table tbody').append('<tr><td style="display:none;"><input class="form-control" name="studentID" type="text" value=' + value.studentID + '></td><td>' + value.studentName + '</td><td><input class="form-control" name="obtainedMarks" type="text"></td><td><input class="form-control" name="grade" type="text"></td><td><input class="form-control" name="notes" type="text"></td></tr>');
                        $('#student-non-subject-result-table tbody').append('<tr><td style="display:none;"><input class="form-control" name="studentID" type="text" value=' + value.studentID + '></td><td>' + value.studentName + '</td><td><input class="form-control" name="gradeNS" type="text"></td><td><input class="form-control" name="notesNS" type="text"></td></tr>');

                    });
                    hideProgressBar('p2-students-result-list');
                    hideProgressBar('p2-non-subject-students-result-list');
                }else{
                    $('#student-result-table tbody').empty();
                    $('#student-non-subject-result-table tbody').empty();
                    $('#student-result-table tbody').append(`<tr>
                            <td colspan="4" class="text-center"><strong style="">${messages.noStudentFound}</strong></td>
                        </tr>`)
                        $('#student-non-subject-result-table tbody').append(`<tr>
                            <td colspan="4" class="text-center"><strong style="">${messages.noStudentFound}</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                    hideProgressBar('p2-non-subject-students-result-list');
                }

                } else {
                    $('#student-result-table tbody').empty();
                    $('#student-result-table tbody').append(`<tr>
                            <td colspan="5" class="text-center"><strong>${messages.noStudentFound}</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                    hideProgressBar('p2-non-subject-students-result-list');
                }

        },
        error: function (err) {
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-students-result-list');
            hideProgressBar('p2-non-subject-students-result-list');
        }
    })

});

$(document).on('click', ".delete-result-row", function () {
    var id = $(this).attr('data-id');
    $tr = $(this).closest('tr').fadeOut(1000);
    $('#filter-students')
    $("#filter-students option[value=" + id + "]").attr({
        'data-isSelected': '0',
        "disabled": false
    });

});

function submitExamTermResults() {

    $.ajax({
        url: herokuUrl + '/setExamTermResults',
        method: 'POST',
        headers,
        data,
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                getSubjectResult();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            invokedFunctionArray.push('submitExamTermResults');
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

// Over All Result
function getOverallResult() {
    if (overAllExamTermTableData) {
        overAllExamTermTableData.destroy();
    }
    overAllExamTermTableData = $('#overall-result-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax':{
            url: herokuUrl + '/getExamOverallResult',
            method: 'POST',
            headers,
            data: {
                examsTermID: examTermID,
                classID: classID
            },
            dataSrc: function (res) {
                response = parseResponse(res);


                var return_data = new Array();
                for (let i = 0; i < response.length; i++) {
                    const element = response[i];
                    return_data.push({
                        'sNo': i + 1,
                        'studentName':  `<button style="margin-left:10px;" class="btn btn-primary btn-sm view-student-subjects-list" data-id="${element.studentID}" data-name="${element.studentName}"> <i class="fa fa-list" ></i>     ${messages.list} </button>`+element.studentName ,
                        'overallNotes': element.overallNotes,
                        'overallPercentage': element.overallPercentage,
                        'rank': element.rank,
                        'action': `<div class="btn-group">
                        <button class="btn-primary btn btn-xs list-edit-overall-result" data-json='${handleApostropheWork(element)}' data-toggle="tooltip" title="${messages.edit}">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </div>`
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getOverallResult');
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
            }
        },
        "columns": [{
                'data': 'sNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'overallNotes'
            },
            {
                'data': 'overallPercentage'
            },
            {
                'data': 'rank'
            },
            {
                'data': 'action'
            }
        ]
    });

}
// Subject Result
function getSubjectResult() {
    if (examTermTableData) {
        examTermTableData.destroy();
    }
    examTermTableData = $('#subject-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax':{
            url: herokuUrl + '/getExamSubjectResult',
            method: 'POST',
            headers,
            data: {
                subjectID: subjectID,
                examsTermID: examTermID,
                classID: classID
            },
            dataSrc: function (res) {
                response = parseResponse(res);

                if (response.length <= 0) {
                    subjectResultListTable.append(`
                    <tr>
                        <td colspan="8">${messages.noRecordsFound}</td>
                    </tr>`)
                    return false;
                }
                var return_data = new Array();
                for (let i = 0; i < response.length; i++) {
                    const element = response[i];
                    return_data.push({
                        'sNo': i + 1,
                        'studentName': element.studentName,
                        'subjectName': element.subjectName,
                        'totalMarks': element.totalMarks,
                        'obtainedMarks': element.obtainedMarks,
                        'grade': element.grade,
                        'notes': element.notes,
                        'action': `<div class="btn-group">
                        <button class="btn-primary btn btn-xs list-edit-subject-result" data-json='${JSON.stringify(element)}' data-toggle="tooltip" title="${messages.edit}">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </div>`
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getSubjectResult');
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
            }
        },
        "columns": [
            {
                'data': 'sNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'subjectName'
            },
            {
                'data': 'totalMarks'
            },
            {
                'data': 'obtainedMarks'
            },
            {
                'data': 'grade'
            },
            {
                'data': 'notes'
            },
            {
                'data': 'action'
            }
        ]
    });

}
// Non Subject Result
function getNonSubjectResult() {
    if (nonExamTermTableData) {
        nonExamTermTableData.destroy();
    }
    nonExamTermTableData = $('#non-subject-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax':{
            url: herokuUrl + '/getExamNSResult',
            method: 'POST',
            headers,
            data: {
                examsTermID: examTermID,
                classID: classID,
                nonSubjectID: nonSubjectID
            },
            beforeSend: function () {
                nonSubjectResultListTable.empty();
            },
            dataSrc: function (res) {

                response = parseResponse(res);

                if (response.length <= 0) {
                    nonSubjectResultListTable.append(`
                    <tr>
                        <td colspan="8">${messages.noRecordsFound}</td>
                    </tr>`)
                    return false;
                }
                var return_data = new Array();
                for (let i = 0; i < response.length; i++) {
                    const element = response[i];

                    return_data.push({
                        'sNo': i + 1,
                        'studentName': element.studentName,
                        'nonSubjectName': element.nonSubjectName,
                        'notesNS': element.notesNS,
                        'gradeNS': element.gradeNS,
                        'action': `<div class="btn-group">
                        <button class="btn-primary btn btn-xs list-edit-non-subject-result" data-json='${JSON.stringify(element)}' data-toggle="tooltip" title="${messages.edit}">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </div>`
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getNonSubjectResult');
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
            }
        },
        "columns": [{
                'data': 'sNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'nonSubjectName'
            },
            {
                'data': 'notesNS'
            },
            {
                'data': 'gradeNS'
            },
            {
                'data': 'action'
            }
        ]
    });

}
// Overall submit
overallResultForm.on('submit', function (e) {
    e.preventDefault();
    var filtersformData = $('#filter-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
    var hiddenFieldOverall = $('#editHiddenFieldOverall').val();
    let data = filtersformData+'&'+$(this).serialize().replace(/[^&]+=\.?(?:&|$)/g, '') ;
    $.ajax({
        url: herokuUrl + '/setExamTermResults',
        method: 'POST',
        headers,
        data,
        beforeSend: function () {
            disableButtonsToggle();
            overallResultForm.find('.form-message').remove();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                overallResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                // getOverallResult();
                overallResultForm.trigger('reset');
                overallResultForm.find('.hidden-field').removeAttr('name value');
            }
            getOverallResult();
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                overallResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

            if (xhr.status === 500) {
                overallResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });

});
// Subject submit
subjectResultForm.on('submit', function (e) {
    e.preventDefault();
    var hiddenFieldSubject = $('#editHiddenFieldSubject');
    var bulkArray = [];
    var result = $(this).serializeArray();
    // console.log(result);
    // return false;
    // var shiftLeftedArray = result.shift();
    var filtersformData = $('#filter-form').serializeArray();
    // console.log("filtersformData array ");
    // console.log(filtersformData);

    filtersformData.forEach((val,index) => {
        if(val.name == 'nonSubjectID'){
            filtersformData.splice(index, 1);
        }

    });
    // console.log("nonSubjectID array ");
    // console.log(filtersformData);

    var sliecedArray = filtersformData.slice(4, filtersformData.length);

    // console.log("sliced array ");
    // console.log(sliecedArray);

    var chunkedArray = chunkArray(result, 4);

    // console.log("chunked array ");
    // console.log(chunkedArray);

    chunkedArray.forEach((val, index) => {
        var myObject = {};
        myObject["userID"] = user.userID;
        sliecedArray.forEach((data, formIndex) => {
            myObject[data.name] = data.value ;
        });

        val.forEach((innerVal, innerIndex) => {
            myObject[innerVal.name] = innerVal.value;
        });

        bulkArray.push(myObject);
    });
    // if(bulkArray[0]){
    //     if(bulkArray[0].obtainedMarks == ''){
    //         bulkArray.splice(0, 1);
    //     }
    // }
    // console.log(bulkArray);
    // return false;
    $.ajax({
        url: herokuUrl + '/setExamSubjectResultsArray',
        method: 'POST',
        headers,
        data: {
            array: bulkArray
        },
        beforeSend: function () {
            disableButtonsToggle();
            subjectResultForm.find('.form-message').remove();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                subjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                getSubjectResult();
                subjectResultForm.trigger('reset');
                $('#subject-result-hidden-field').removeAttr('name value')
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                subjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

            if (xhr.status === 500) {
                subjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });

});
// Non Subject submit
nonSubjectResultForm.on('submit', function (e) {
    e.preventDefault();
    var hiddenFieldNonSubject = $('#editHiddenFieldNonSubject');
    var bulkArray = [];
    var result = $(this).serializeArray();
    var filtersformData = $('#filter-form').serializeArray();
    filtersformData.forEach((val,index) => {
        if(val.name == 'subjectID'){
            filtersformData.splice(index, 1);
        }else if(val.name == "totalMarks"){
            filtersformData.splice(index, 1);
        }

    });
    var sliecedArray = filtersformData.slice(4, filtersformData.length);
    var chunkedArray = chunkArray(result, 3);
    chunkedArray.forEach((val, index) => {
        var myObject = {};
        myObject["userID"] = user.userID;
        var count = 9;
        sliecedArray.forEach((data, formIndex) => {
            myObject[data.name] = data.value;
        });
        val.forEach((innerVal, innerIndex) => {
            myObject[innerVal.name] = innerVal.value;
        });
        bulkArray.push(myObject);
    });
    $.ajax({
        url: herokuUrl + '/setExamNonSubjectResultsArray',
        method: 'POST',
        headers,
        data :{array: bulkArray},
        beforeSend: function () {
            disableButtonsToggle();
            nonSubjectResultForm.find('.form-message').remove();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                nonSubjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);

                nonSubjectResultForm.trigger('reset');
                $('#non-subject-result-hidden-field').removeAttr('name value')
            }
            getNonSubjectResult();
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                nonSubjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }

            if (xhr.status === 500) {
                nonSubjectResultForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });

});

$(document).on('click', '.list-edit-overall-result', function () {
    let json = JSON.parse($(this).attr('data-json'));
    let keys = Object.keys(json);
    keys.forEach((key, index) => {
        if (key === 'examOverallID') {
            $('#editHiddenFieldOverall').attr('name', key).val(json[key]);
        } else {
            $('[name=' + key + ']').val(json[key]);
        }
    });
    overallResultForm.slideDown();

   filterFormTitle.text(messages.create).css({
       'color': '#2fa7ff'
   });
   $('#overall-result-list').slideUp();


});

$(document).on('click', '.list-edit-subject-result', function () {
    var fieldsNameArray = ['studentName','obtainedMarks','grade','notes'];
    let json = JSON.parse($(this).attr('data-json'));
    let keys = Object.keys(json,'subject result');
    keys.forEach((key, index) => {
        if (key === 'examOverallID') {
            $('#editHiddenFieldSubject').attr('name', key).val(json[key]);
        } else if (key === 'examSubjectResultID') {
            $('#subject-result-hidden-field').attr('name', key).val(json[key]);
        } else {
            $('[name=' + key + ']').val(json[key]);
        }
    });
    $("#subject-result-form").slideDown();
   filterFormTitle.text(messages.create).css({
       'color': '#2fa7ff'
   });
   $('#subject-result-list-container').hide();
   $('#subject-results-list').slideUp();
    $('#subject-result-add-fields').show();


});

$(document).on('click', '.list-edit-non-subject-result', function () {

    let json = JSON.parse($(this).attr('data-json'));
    let keys = Object.keys(json,'subject result');
    keys.forEach((key, index) => {
        if (key === 'examOverallID') {
            $('#editHiddenFieldSubject').attr('name', key).val(json[key]);
        } else if (key === 'examSubjectResultID') {
            $('#subject-result-hidden-field').attr('name', key).val(json[key]);
        } else {
            $('[name=' + key + ']').val(json[key]);
        }
    });
    $("#non-subject-results-list").hide();
   filterFormTitle.text(messages.create).css({
       'color': '#2fa7ff'
   });
   $('#non-subject-result-edit-div').show();
   $('#subject-results-list').slideUp();
    $('#subject-result-add-fields').show();


});

$('#hide-subject-edit-form-container').on('click',function(){
    $('#subject-result-add-fields').hide();
    $('#subject-result-list-container').hide();
    $('#subject-results-list').slideDown();
    resetMessages();

});
$('#hide-non-subject-edit-form-container').on('click',function(){

    $('#non-subject-result-edit-div').hide();
    $('#non-subject-result-form').hide();
    $('#non-subject-results-list').slideDown();
    $(".error-div").empty();
    resetMessages();

});

$(document).on('click', '.list-edit-non-subject-result', function () {
    let json = JSON.parse($(this).attr('data-json'));
    let keys = Object.keys(json);

    keys.forEach((key, index) => {
        if (key === 'examOverallID') {
            $('#editHiddenFieldNonSubject').attr('name', key).val(json[key]);
        } else if (key === 'examNSResultID') {
            $('#non-subject-result-hidden-field').attr('name', key).val(json[key]);
        } else {
            $('[name=' + key + ']').val(json[key]);
        }
    });


});

function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

function submitSubjectResult() {

        $.ajax({
            url: herokuUrl + '/setExamSubjectResults',
            method: 'POST',
            headers,
            data,
            beforeSend: function () {
                disableButtonsToggle();
                showProgressBar(submitLoader);
            },
            success: function (res) {

                if (res.response === 'success') {
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
                $('#subject-result-edit-form').trigger('reset');
                getSubjectResult();
            },
            error: function (xhr) {
                invokedFunctionArray.push('submitSubjectResult');
                window.requestForToken(xhr)
                if (xhr.status === 422) {
                    parseValidationErrors(xhr);
                }
                if (xhr.status === 400) {
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            }
        });

}

function submitNonSubjectResult() {

    $.ajax({
        url: herokuUrl + '/setExamNonSubjectResults',
        method: 'POST',
        headers,
        data,
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if (res.response === 'success') {
                $(".error-div").html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('.non-subject-edit-form').trigger('reset');
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
            getNonSubjectResult();



        },
        error: function (xhr) {
            invokedFunctionArray.push('submitNonSubjectResult');
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$('.reset-form').on('click', function () {
    let formId = $(this).attr('data-form');
    $('#' + formId).trigger('reset');
    $('#' + formId + ' input:hidden').removeAttr('name value');
});



$('#exam-term-results-form').on('submit', function (e) {
    e.preventDefault();
    var overallNotes = $('#overallNotes').val();
    var overallPercentage = $('#overallPercentage').val();
    var rank = $('#rank').val();
    var isPublished = $('input[name=isPublished]').val();
    data = $('#filter-form').serialize() + '&overallNotes=' + overallNotes + '&overallPercentage=' + overallPercentage + '&rank=' + rank + '&isPublished=' + isPublished;
    showProgressBar(submitLoader);
    resetMessages();
    submitExamTermResults();
});

// subject result update submit
$('#subject-result-edit-form').on('submit', function (e) {
    e.preventDefault();

    data = $('#filter-form').serialize()+'&'+$(this).serialize()+'&userID='+user.userID;
    resetMessages();
    submitSubjectResult();

});

// non subject result upadte submit
$('#non-subject-result-edit-form').on('submit', function (e) {
    e.preventDefault();

    data = $('#filter-form').serialize()+'&'+$(this).serialize()+'&userID='+user.userID;
    submitNonSubjectResult();

});


$(document).on('click','.view-student-subjects-list',function(){
     studentID = $(this).attr('data-id');
     var studentName =  $(this).attr('data-name');
     if(studentID > 0 && classID > 0 && subjectID > 0 && examTermID > 0){
         if(studentSubjectTable){
            studentSubjectTable.destroy();
         }
        studentSubjectTable = $('#student-subject-table').DataTable({
            language: {
                url: __DATATABLE_LANGUAGE__
            },
            'ajax':{
                url: herokuUrl + '/getExamSubjectResult',
                method: 'POST',
                headers,
                data: {
                    examsTermID: examTermID,
                    classID: classID,
                    studentID: 261
                },
                beforeSend: function () {
                    subjectResultListTable.empty();
                },
                dataSrc: function (res) {
                    subjectResultListTable.empty();
                    response = parseResponse(res);

                    if (response.length <= 0) {
                        subjectResultListTable.append(`
                        <tr>
                            <td colspan="8">${messages.noRecordsFound}</td>
                        </tr>`)
                        return false;
                    }
                    var return_data = new Array();
                    for (let i = 0; i < response.length; i++) {
                        const element = response[i];
                        return_data.push({
                            'sNo': i + 1,
                            // 'studentName': element.studentName,
                            'subjectName': element.subjectName,
                            'totalMarks': element.totalMarks,
                            'obtainedMarks': element.obtainedMarks,
                            'grade': element.grade,
                            'notes': element.notes
                        })
                    }
                    return return_data;
                },
                error: function (xhr) {
                    window.requestForToken(xhr)
                    parseValidationErrors(xhr);
                }
            },
            "columns": [{
                    'data': 'sNo'
                },
                {
                    'data': 'subjectName'
                },
                {
                    'data': 'totalMarks'
                },
                {
                    'data': 'obtainedMarks'
                },
                {
                    'data': 'grade'
                },
                {
                    'data': 'notes'
                }
            ]
        });
        if(studentNonSubjectTable){
            studentNonSubjectTable.destroy();
         }
        studentNonSubjectTable = $('#student-non-subject-table').DataTable({
            language: {
                url: __DATATABLE_LANGUAGE__
            },
            'ajax':{
                url: herokuUrl + '/getExamNSResult',
                method: 'POST',
                headers,
                data: {
                    examsTermID: examTermID,
                    classID: classID,
                    studentID: studentID
                },
                dataSrc: function (res) {
                    nonSubjectResultListTable.empty();
                    response = parseResponse(res);

                    if (response.length <= 0) {
                        nonSubjectResultListTable.append(`
                        <tr>
                            <td colspan="8">${messages.noRecordsFound}</td>
                        </tr>`)
                        return false;
                    }
                    for (let i = 0; i < response.length; i++) {
                        const element = response[i];
                        var return_data = new Array();
                        return_data.push({
                            'sNo': i + 1,
                            // 'studentName': element.studentName,
                            'nonSubjectName': element.nonSubjectName,
                            'notesNS': element.notesNS,
                            'gradeNS': element.gradeNS
                        })
                    }
                    return return_data;
                },
                error: function (xhr) {
                    window.requestForToken(xhr)
                    parseValidationErrors(xhr);
                }
            },
            "columns": [{
                    'data': 'sNo'
                },
                // {
                //     'data': 'studentName'
                // },
                {
                    'data': 'nonSubjectName'
                },
                {
                    'data': 'notesNS'
                },
                {
                    'data': 'gradeNS'
                }
            ]
        });
        $("#student-subject-result").text(studentName + messages.subjectResults).css({
            'color': '#2fa7ff'
        });
        $("#student-non-subject-result").text(studentName + messages.nonSubjectResults).css({
            'color': '#2fa7ff'
        });
        $('.student-subject-and-nonsubject-div').show();
     }else{
        $('.view-list-error').show().html(messages.pleaseMakeSureSelectedStudentIdClassIDSubjectIDExamTermID);
        setTimeout(function(){
            $('.view-list-error').hide(1000).html('');
        }, 3000);
     }

})
$(document).on('click','.student-result-list',function(){
    $('.student-subject-and-nonsubject-div').hide();
});
