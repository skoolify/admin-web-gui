var mobileNumber = '';
var getMobileNumber = '';
var parentRecord = null;
var editing = false;

$(document).ready(function(){
    $("#parentCNIC").on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
     });
     getAjaxCountries();
    getAjaxSchools();
})

function submitParent(){
    $.ajax({
        url: herokuUrl+'/setParents',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                // tableData.destroy();
                $('form#parents-form').trigger('reset');
                $('.phone-error').hide();
                $('#phone-error').text('')
                $('#phoneNumberLabel').removeAttr('style')
                mobileNumber = $('#searchMobileNumber').val()
                if(mobileNumber !== ''){
                    getMobileNumber = mobileNumber
                    getParents();
                    editing = false;
                }
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
            setTimeout(() => {
                getParents(data.mobileNumber);
            }, 500);
        },
        error: function(xhr){
            invokedFunctionArray.push('submitParent')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response}, ${xhr.responseJSON.reason}</strong></p>`);
            }
            if (xhr.status === 500) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${messages.unableToProcessPleaseTryAgain} </strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click','.clear-parent',function(){
    $('#searchMobileNumber').val('');
    $('#searchParentName').val('');
    $('#searchParentCNIC').val('');
    if(tableData){
        tableData
          .clear()
          .draw();
      }
})

$(document).on('blur','#searchMobileNumber', function(){
    $('#searchParentName').val('');
    $('#searchParentCNIC').val('');
})

$(document).on('blur','#searchParentName', function(){
    $('#searchParentCNIC').val('');
})

function getSearchParents(){
    let mobileNumber = $('#searchMobileNumber').val();
    let parentName = $('#searchParentName').val();
    let parentCNIC = $('#searchParentCNIC').val();
    let data = {}
    if(parentName !== ''){
        data.parentName = parentName
    }
    if(mobileNumber !== ''){
        data.mobileNumber = mobileNumber
    }
    if(parentCNIC !== ''){
        data.parentCNIC = parentCNIC
    }
    if(branchID !== ''){
        data.branchID = branchID
    }else{
        data.branchID = $('#filter-branches').val()
    }
    if(schoolID !== ''){
        data.schoolID = schoolID
    }
    let editHiddenField = $('#editHiddenField');
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl+'/getParent',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                if(response.length >= 1){
                    parentRecord = response[0];
                    var keys = Object.keys(parentRecord);
                    if(editing){
                        keys.forEach((value, index) => {
                            submitButton.attr('disabled', true);
                            $('[name='+value+']').val(parentRecord[value]);
                            $('[name='+value+']').attr('disabled', true);
                            if(value.includes('ID')){
                                if(typeof value !== 'undefined'){
                                    window[value] = parentRecord[value];
                                }
                            }
                        });
                    }
                }
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    var parentType = '';
                    if(response[i].parentType != 'null'){
                        if(response[i].parentType == 'M'){
                            parentType = `<label class="label label-sm label-danger">${messages.mother}</label>`
                        }else if(response[i].parentType == 'F'){
                            parentType = `<label class="label label-sm label-success">${messages.father}</label>`
                        }else if(response[i].parentType == 'G'){
                            parentType = `<label class="label label-sm label-danger">${messages.guardian}</label>`
                        }
                    }
                    return_data.push({
                        'sNo' : i+1,
                        'parentName': response[i].parentName,
                        'parentCNIC' : response[i].parentCNIC,
                        'mobileNumber' : response[i].mobileNumber,
                        'email' : response[i].email,
                        'parentType' : parentType,
                        'isRegistered' : `<label class="label label-sm label-${response[i].isRegistered ? 'success' : 'danger'}"> ${response[i].isRegistered ? messages.yes : messages.no}</label>`,
                        'studentList': `<a data-parentId='${response[i].parentID}' data-parentName='${response[i].parentName}' class="btn btn-primary btn-sm linked-student-list-button"><i class="fa fa-list"></i>${messages.list}</a>`,
                        'action' : `
                            <div class="btn-group btn-success" role="group">
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle btn-sucess" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${messages.action} <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    ${__ACCESS__ ?
                                        `<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-primary btn-lg  list-record-edit dropdown-item" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i> ${messages.edit}
                                        </a>`: '' }
                                        <a data-type="link" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-success btn-lg dropdown-item link-unlink-student-button-modal" data-linkedParentId="${response[i].parentID}" ><i class="fa fa-link" ></i> ${messages.LinkStudent}</a>
                                        <a data-type="unlink" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-warning btn-lg  dropdown-item link-unlink-student-button-modal" data-linkedParentId="${response[i].parentID}"><i class="fa fa-unlink" ></i> ${messages.UnlinkStudent}</a>
                                        ${__DEL__ ? `
                                            <a  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-danger btn-lg  dropdown-item delete-parent" data-parent-id="${response[i].parentID}" data-parent-mobile="${response[i].mobileNumber}"><i class="fa fa-trash"></i> ${messages.DeleteParent}</a>
                                        ` : '' }
                                    </div>
                                </div>
                            </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getSearchParents')
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 404){
                   $('.dataTables_empty').text('no data available');
                }
                if (xhr.status === 500) {
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'parentName'},
            {'data': 'parentCNIC'},
            {'data': 'mobileNumber'},
            {'data': 'email'},
            {'data': 'parentType'},
            {'data': 'isRegistered'},
            {'data': 'studentList'},
            {'data': 'action'}
        ]
    });
}

function getParents(mobileNumber = null){
    if(mobileNumber === null){
        mobileNumber = $('#phoneNumber').val()
    }
    let data = {}
    if(mobileNumber !== ''){
        data.mobileNumber = mobileNumber
    }else{
        data.mobileNumber = getMobileNumber
    }
    if(branchID !== ''){
        data.branchID = branchID
    }else{
        data.branchID = $('#filter-branches').val()
    }
    if(schoolID !== ''){
        data.schoolID = schoolID
    }
    let editHiddenField = $('#editHiddenField');
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl+'/getParent',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                if(response.length >= 1){
                    parentRecord = response[0];
                    var keys = Object.keys(parentRecord);
                    if(editing){
                        keys.forEach((value, index) => {
                            submitButton.attr('disabled', true);
                            if(value === 'addressLine1' || value === 'addressLine2'){
                                $('[name='+value+']').val(atob(parentRecord[value]));
                            }else{
                                $('[name='+value+']').val(parentRecord[value]);

                            }
                            $('[name='+value+']').attr('disabled', true);
                            if(value.includes('ID')){
                                if(typeof value !== 'undefined'){
                                    window[value] = parentRecord[value];
                                }
                            }
                        });
                    }
                }
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    if(response[i].parentType == 'M'){
                        parentType = `<label class="label label-sm label-danger">${messages.mother}</label>`
                    }else if(response[i].parentType == 'F'){
                        parentType = `<label class="label label-sm label-success">${messages.father}</label>`
                    }else if(response[i].parentType == 'G'){
                        parentType = `<label class="label label-sm label-danger">${messages.guardian}</label>`
                    }
                    return_data.push({
                        'sNo' : i+1,
                        'parentName': response[i].parentName,
                        'parentCNIC' : response[i].parentCNIC,
                        'mobileNumber' : response[i].mobileNumber,
                        'email' : response[i].email,
                        'parentType' : parentType,
                        'isRegistered' : `<label class="label label-sm label-${response[i].isRegistered ? 'success' : 'danger'}"> ${response[i].isRegistered ? messages.yes : messages.no}</label>`,
                        'studentList': `<a data-parentId='${response[i].parentID}' data-parentName='${response[i].parentName}' class="btn btn-primary btn-sm linked-student-list-button"><i class="fa fa-list"></i>${messages.list}</a>`,
                        'action' : `
                            <div class="btn-group btn-success" role="group">
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle btn-sucess" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ${messages.action} <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    ${__ACCESS__ ?
                                        `<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-primary btn-lg  list-record-edit dropdown-item" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i> ${messages.edit}
                                        </a>`: '' }
                                        <a data-type="link" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-success btn-lg dropdown-item link-unlink-student-button-modal" data-linkedParentId="${response[i].parentID}" ><i class="fa fa-link" ></i> ${messages.LinkStudent}</a>
                                        <a data-type="unlink" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-warning btn-lg  dropdown-item link-unlink-student-button-modal" data-linkedParentId="${response[i].parentID}"><i class="fa fa-unlink" ></i> ${messages.UnlinkStudent}</a>
                                        ${__DEL__ ? `
                                            <a  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-danger btn-lg  dropdown-item delete-parent" data-parent-id="${response[i].parentID}" data-parent-mobile="${response[i].mobileNumber}"><i class="fa fa-trash"></i> ${messages.DeleteParent}</a>
                                        ` : '' }
                                    </div>
                                </div>
                            </div>`
                    })
                }
                // disableButtonsToggle();
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getParents')
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 404){
                   $('.dataTables_empty').text(messages.noDataAvailable);
                }
                if (xhr.status === 500) {
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'parentName'},
            {'data': 'parentCNIC'},
            {'data': 'mobileNumber'},
            {'data': 'email'},
            {'data': 'parentType'},
            {'data': 'isRegistered'},
            {'data': 'studentList'},
            {'data': 'action'}
        ]
    });
}

function linkParentStudent(){
    if(studentID != '' && parentID != ''){
        $.ajax({
            url: herokuUrl+'/setParentStudentLink',
            method: 'POST',
            headers,
            data: {
                parentID: parentID,
                studentID: studentID
            },
            beforeSend: function(){
            },
            success: function(res){
                emptyVariable();
                if(res.response === 'success'){
                    $('#linkedStudentAssignModal').modal('hide');
                    Swal({
                        position: 'center',
                        type: 'success',
                        title: messages.ParentLinkSuccessfully,
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            },
            error: function(xhr){
                invokedFunctionArray.push('linkParentStudent')
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 400){
                    $('#link-form-errors').html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                }

            }
        });
    }else{
        $('#link-form-errors').html(`<p><strong id="add-form-error-message" class="text-danger">${messages.studentParentAreRequired}</strong></p>`);
    }

}

function emptyVariable(){
    classLevelID = ''
    classID = ''
    studentID = ''
}

function unLinkParentStudent(){
    if(studentID != '' && parentID != ''){
        $.ajax({
            url: herokuUrl+'/setParentStudentDeLink',
            method: 'POST',
            headers,
            data: {
                parentID: parentID,
                studentID: studentID
            },
            beforeSend: function(){
            },
            success: function(res){
                emptyVariable();
                if(res.response === 'success'){
                    $('#linkedStudentAssignModal').modal('hide');
                    $('#linkedStudentDetailModal').modal('hide');
                    Swal({
                        position: 'center',
                        type: 'success',
                        title: messages.ParentUnlinkSuccessfully,
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            },
            error: function(xhr){
                invokedFunctionArray.push('unLinkParentStudent')
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 400){
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                }
            }
        });
    }else{
        addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${messages.studentParentAreRequired}</strong></p>`);
    }
}

$('.search-parent').on('click', function(){
    getAjaxCities(countryID);
    let mobileNumber = $('#searchMobileNumber').val();
    let parentName = $('#searchParentName').val();
    let parentCNIC = $('#searchParentCNIC').val();
    if(mobileNumber === '' && parentName === '' && parentCNIC === ''){
        swal(
            messages.error,
            messages.PleaseEnterMobileNumberORParentName,
            'error'
        )
        return false;
    }
    if( mobileNumber.length > 0 && mobileNumber[0] !== '+'){
        $('.phone-error').html(messages.pleaseEnterMobileNoWithCountryCodePrecededWithSign);
        $('.phone-error').show(200)
        $(this).addClass('phone-imput-error').focus()
        $('#searchMobileNumber').css("color", "#dd4b39")
        $(this).css("border-color","#00a65a")
        return false;
    }else{
        $(this).removeClass('phone-imput-error')
        $('#searchMobileNumber').removeAttr('style')
        $('#phoneNumberLabel').css("color", "#00a65a")
        $('.phone-error').hide(200);
        $('.phone-error').html('');
    }
        getSearchParents();
});

$('#filter-form').on('submit', function(e){
    e.preventDefault();
    return false;
})

$('#parents-form').on('submit', function(e){
    if($('#phoneNumber').val() == ''){
        swal(
            messages.pleaseEnterPhoneFirst,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#parentName').val() == ''){
        swal(
            messages.pleaseEnterFirstName,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#parentType').val() == ''){
        swal(
            messages.parentType,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('.tax-beneficiary').prop('checked') && $('#TaxID').val() == ''){
        swal(
            messages.pleaseEnterTheTaxIDFirst,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    e.preventDefault();
    let studentID = $('#filter-students').val();
    if( mobileNumber.length === 0 || mobileNumber[0] !== '+'){
        $('.phone-error').html(messages.pleaseEnterMobileNoWithCountryCodePrecededWithSign);
        $('.phone-error').show(200);
        $(this).addClass('phone-imput-error').focus()
        $('#phoneNumberLabel').css("color", "#dd4b39")
        $(this).css("border-color","#00a65a")
    }else if(mobileNumber.length < 12){
        $('.phone-error').html(messages.PleaseEnterCompleteNumber);
        $('.phone-error').show(200);
        $(this).addClass('phone-imput-error').focus()
        $('#phoneNumberLabel').css("color", "#dd4b39")
    }else{
        $(this).removeClass('phone-imput-error')
        $('#phoneNumberLabel').css("color", "#00a65a")
        $('.phone-error').hide(200);
        $('.phone-error').html('');
    }
    data = $('#parents-form').serializeArray();
    var obj = {};
    data.forEach((value, index) => {
        // if((value.name == 'addressLine1' && value.name != '') || (value.name == 'addressLine2' && value.name != '')){
        //     obj[value.name] = btoa(value.value);
        // }else{
            obj[value.name] = value.value;
        // }
    });
    data = clean(obj)
    showProgressBar(submitLoader);
    resetMessages();
    submitParent();

});

$('#checkIsActive').on('click',function(){
    if($('#checkIsActive').prop('checked')){
        $('#isActive').val(1);
    }else{
        $('#isActive').val(0);
    }
});

$(document).on('click','.linked-student-list-button',function(){
      if(schoolID === ''){
        swal(
            messages.pleaseSelectSchool,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
    }else{
    $('#linkedStudentDetailModal').modal({
        backdrop: 'static',
        keyboard: false,
        toggle: true,
        }
    );
    $('#linkedStudentDetailModal').modal('show');
    if (parentLinkedStudentTable) {
        parentLinkedStudentTable.destroy();
    }
    parentID = $(this).attr('data-parentId');
    var linkedParentName = $(this).attr('data-parentName');
    $('.linked-parent-list-title').text(linkedParentName + " linked Student List");
    parentLinkedStudentTable = $('#linked-student-list').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl+'/getStudentsbyParentID',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                schoolID: schoolID,
                parentID: parentID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'rollNo': response[i].rollNo && response[i].rollNo.length > 8 ? `<span data-toggle="tooltip" title="${response[i].rollNo}">${response[i].rollNo.substring(0, 8)}...</span>` : response[i].rollNo,
                        'studentName' : response[i].studentName,
                        'dob' : response[i].dob && response[i].dob !== messages.InvalidDate && response[i].dob !== '0000-00-00' ? moment(response[i].dob).format('DD-M-YYYY') : '--',
                        'branchName': response[i].branchName,
                        'classShift' : response[i].classShift,
                        'classLevel' : `${response[i].classAlias}-${response[i].classSection}`,
                        'classYear': response[i].classYear,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive == 1 ? 'success' : 'danger'}"> ${response[i].isActive == 1 ? messages.active : messages.inActive}</label>`,
                        'unlink' : `<button class="btn btn-sm btn-warning unlink-from-list" data-student-id="${response[i].studentID}" data-parent-id="${response[i].parentID}"> ${messages.Unlink} </button>`,


                    })
                }
                return return_data;
            },
            error: function(xhr){
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'rollNo'},
            {'data': 'studentName'},
            {'data': 'dob'},
            {'data': 'branchName'},
            {'data': 'classShift'},
            {'data': 'classLevel'},
            {'data': 'classYear'},
            {'data': 'isActive'},
            {'data': 'unlink'}
        ]
    });
}
});

$('.tax-beneficiary').on('click',function(){
    if($('.tax-beneficiary').prop('checked')){
        $('#taxId').removeClass('hidden');
        $('.tax-beneficiary').val(1)
    }else{
        $('#taxId').addClass('hidden');
        $('.tax-beneficiary').val(0)
        $('#TaxID').val('')
    }
});

$(document).on('click', '.link-unlink-student-button-modal',function(){
    let filtersArray = ["filter-classes", "filter-students"]
    filtersArray.forEach(function(value, index){
        $('#'+value).empty();
        $('#'+value).find('option').remove();
        $('#'+value).multiselect('rebuild');
        $('#'+value).multiselect('refresh');
    })
    parentID = $(this).attr('data-linkedParentId');
    let type = $(this).attr('data-type')
    let title = '';
    let btn = '';
    if(type === 'link'){
        title = messages.LinkStudent
        btn = 'btn-primary'
    }else{
        title = messages.UnlinkStudent
        btn = 'btn-warning'
    }
    $('.link-unlink-submit').removeClass('btn-primary btn-warning').addClass(btn).attr('data-type',type)
    $('.link-unlink-submit').text(title);
    $('.link-unlink-title').text(title);
    $('#linkedStudentAssignModal').modal({
        backdrop: 'static',
        keyboard: false,
        toggle: true,
        }
    )
    if (classLevels.length && type === 'link') {
        $('#class-div').show()
        $('#section-div').show()
        $('.link-unlink-submit').removeAttr('disabled');
        getAjaxClassLevels();
    }else{
        $('#class-div').hide()
        $('#section-div').hide()
        getLinkedStudent();
    }
});

function getLinkedStudent(){
    $.ajax({
        url: herokuUrl+'/getStudentsbyParentID',
        type: 'POST',
        headers,
        data: {
            schoolID: schoolID,
            parentID: parentID
        },
        beforeSend: function(){
            $('#filter-students').empty();
            $('#filter-students').html('');
            $('#filter-students').find('oprtion').remove();
            $('#filter-students').multiselect('refresh')
            $('#filter-students').multiselect('rebuild')
        },
        success: function(res){
            response = parseResponse(res);
            if(typeof response !== 'undefined' && response.length > 0){
                $('.link-unlink-submit').removeAttr('disabled');
                $('#filter-students').append(`<option value="">${messages.SelectStudent}</option>`);
                for(var i = 0; i< response.length; i++){
                    $('#filter-students').append(
                        `<option value="${response[i].studentID}"> ${response[i].studentName}</option>`
                    )

                    $('#filter-students').multiselect('refresh');
                    $('#filter-students').multiselect('rebuild');
                }
                $('.multiselect-container').css({"max-height": "200px !important"});
            }else{
                $('.link-unlink-submit').attr('disabled',true)
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('getLinkedStudent')
            window.requestForToken(xhr)
           console.log(xhr)
        }
    });
}

$(document).on('click','.unlink-from-list',function(){
    parentID = $(this).attr('data-parent-id');
    studentID = $(this).attr('data-student-id');
    unLinkParentStudent();
})

$(document).on('click', '.link-unlink-submit',function(){
    var type = $(this).attr('data-type')
   if(classLevelID === '' && type === 'link'){
        $('.class-level-error').html('Class is required')
    }else if(classID === '' && type === 'link' ){
        $('.class-error').html('Section is required')
        $('.class-level-error').html('')
    }else if(studentID === '' && type === 'link'){
        $('.student-error').html('Student is required')
        $('.class-error').html('')
        $('.class-level-error').html('')
    }else{
        $('.student-error').html('')
        $('.class-error').html('')
        $('.class-level-error').html('')
        if(type === 'link'){
            linkParentStudent();
        }else{
            if(studentID === ''){
                $('.student-error').html(messages.studentIsRequired)
            }else{
                $('.student-error').html('');
                unLinkParentStudent();
            }

        }
    }
});

$(document).on('click', '#unlink-student-button',function(){
    var student_id = $('#filter-students').val();
    if(student_id !== '' && student_id.length > 0){
        unLinkParentStudent();
        // editing = false;
    }else{
        swal(
            messages.pleaseSelectStudent,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
    }

});

$('#reset-add-form').on('click', function(){
    if(parentRecord){
        var keys = Object.keys(parentRecord);
        keys.forEach((value, index) => {
            $('[name='+value+']').removeAttr('disabled');
        });
        parentRecord = null;
        submitButton.removeAttr('disabled');
        cancelButton.removeAttr('disabled');
    }
});

$('#show-add-form-container').on('click', function(){
    if(parentRecord){
        var keys = Object.keys(parentRecord);
        keys.forEach((value, index) => {
            $('[name='+value+']').removeAttr('disabled');
        });
        parentRecord = null;
        submitButton.removeAttr('disabled');
        cancelButton.removeAttr('disabled');
    }
    // if(parentRecord){

        // var keys = Object.keys(parentRecord);
        // keys.forEach((value, index) => {
        //     $('[name='+value+']').removeAttr('disabled');
        // });

    // }else{
    //     submitButton.removeAttr('disabled');
    //     cancelButton.removeAttr('disabled');
    // }
})

$(document).on('click', '.delete-parent', function(){
    parentID = $(this).attr('data-parent-id');
    parentMobile = $(this).attr('data-parent-mobile');

    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheParent+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteParent();
                },
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

function deleteParent(){
    $.ajax({
        url: herokuUrl+'/deleteParent',
        method: 'POST',
        headers,
        data: {
            parentID: parentID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.parentHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getParents(parentMobile);
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteParent')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('click', '.list-record-edit', function(){
    editing = false;
})

$(document).on('click', '#show-add-form-container', function(){
    editing = true;
})

$(document).on('click', '#submit-add-form-container', function(){
    editing = false;
})

$(document).on('click', '#hide-add-form-container', function(){
    editing = false;
})

$(document).on('click', '#reset-add-form', function(){
    editing = false;
})

$(document).on('blur','#phoneNumber', function(){
    mobileNumber = $(this).val();
    if(mobileNumber !== ''){
        if(editing === true){
            if( mobileNumber.length === 0 || mobileNumber[0] !== '+'){
                $('.phone-error').html(messages.pleaseEnterMobileNoWithCountryCodePrecededWithSign);
                $('.phone-error').show(200);
                $(this).addClass('phone-imput-error')
                // $(this).addClass('phone-imput-error').focus()
                $('#phoneNumberLabel').css("color", "#dd4b39")
                $(this).css("border-color","#00a65a")
            }else if(mobileNumber.length < 12){
                $('.phone-error').html(messages.PleaseEnterCompleteNumber);
                $('.phone-error').show(200);
                $(this).addClass('phone-imput-error')
                // $(this).addClass('phone-imput-error').focus()
                $('#phoneNumberLabel').css("color", "#dd4b39")
            }else{
                $(this).removeClass('phone-imput-error')
                $('#phoneNumberLabel').css("color", "#00a65a")
                $('.phone-error').hide(200);
                $('.phone-error').html('');
                editing = true;
            }
            getParents();
        }
    }
})
