

var data = {};

$(document).ready(function(){
    getAjaxSchools();
});

$('.search-attendance-report').on('click', function(){
    dateFromVal = $('#filter-date-from').val();
    dateToVal = $('#filter-date-to').val();
    if(classID === ''){
        swal(
            messages.pleaseSelectClassSectionFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if(dateFromVal === ''){
        swal(
            messages.pleaseSelectStartDateFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if(dateToVal === ''){
        swal(
            messages.pleaseSelectEndDateFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    data.classID = classID;
    data.fromDate = dateFromVal;
    data.toDate = dateToVal;
    getAttendanceReport();
});

function getAttendanceReport(){
    if(tableData){
        tableData.destroy();
    }

    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getAttendanceReport',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);

                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'class': response[i].classAlias+'-'+response[i].classSection,
                        'totalDays': response[i].TotalDays,
                        'presents': response[i].Present,
                        'absents': response[i].Absent,
                        'lates': response[i].Late,
                        'leaves': response[i].Leaves,
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getAttendanceReport');
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'class'},
            {'data': 'totalDays'},
            {'data': 'presents'},
            {'data': 'absents'},
            {'data': 'lates'},
            {'data': 'leaves'}

        ]
    });
}

$(document).on('click','.clear-attendance-report',function(){
    $('#filter-date-from').val('');
    $('#filter-date-to').val('');
    if(tableData){
        tableData
          .clear()
          .draw();
      }
})
