
var isRecurrence = false
var isReminder = isEdit =  false
var validation = true
var recurrenceID = meetingID = ""
var startTime = endTime = currentDateTime =  ''
var meetingCheckArray = [];
var isExist = false;
// var isWebLinkExist = false;

$(document).ready(function(){
    isExist = false;
    $('#startDate').attr('min',moment().format('YYYY-MM-DD'))
    $('#startDate').val(moment().format('YYYY-MM-DD'))
    if($('#endDate').length){
        $('#endDate').attr('min',moment().format('YYYY-MM-DD'))
        $('#endDate').val(moment().format('YYYY-MM-D'))
    }
    getTimeZones();
    calculateTime();
    setMinutesDropdown();
    weekNo = []
    getAjaxSchools();
    if(user.isAdmin == 1){
        schoolID = $('#filter-schools').val();
    }else{
        getConferrences()
    }
})

function submitConferrence(dataObj){
    $.ajax({
        url: herokuUrl+'/setMeetingInvite',
        method: 'POST',
        headers,
        data: dataObj,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            $.LoadingOverlay("hide");
            if(res.response === 'success'){
                $("#filter-instructor").val('');
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                // setTimeout(()=>{
                    $('#add-form-error-message').hide()
                // },1000)
                $('#add-form-error-message').show()
                $('form#conference-form').trigger('reset');
                window.hideFields();
                getConferrences();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            $.LoadingOverlay("hide");
            invokedFunctionArray.push('submitConferrence');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 500){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getConferrences(){
    let data = {}
    if(branchID !== ''){
        data.branchID = branchID;
    }
    if(classLevelID !== ''){
        data.classLevelID = classLevelID;
    }
    if(classID !== ''){
        data.classID = classID;
    }

    if (tableData) {
        tableData.destroy();
    }
    var uri = 'getMeetingInvite';
    if(user.isAdmin !== 1){
        data.hostUserID = user.userID
        uri = 'getUserMeetingInvite';
    }

    // meetingCheckArray = []
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/'+ uri,
            type: 'POST',
            headers,
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                // meetingCheckArray = response;
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    var obj = {}
                    var startDateTime = moment(response[i].startDateTime).unix()
                    var endDateTime = moment(response[i].endDateTime).unix()
                    obj['hostUserID'] = response[i].hostUserID
                    obj['startDateTime'] = startDateTime
                    obj['endDateTime'] = endDateTime
                    obj['meetingID'] = response[i].meetingID
                    obj['classID'] = response[i].classID
                    meetingCheckArray.push(obj)
                    var startTime = moment.utc(response[i].startTime, 'HH:mm:ss');
                    var endTime = moment.utc(response[i].endTime, 'HH:mm:ss')
                    var duration = moment.duration(endTime.diff(startTime));
                    var uriToAppend = `&isHost=1&platform=web&name=${response[i].hostName}`;
                    return_data.push({
                        'sNo' : i+1,
                        'hostUserName': response[i].hostName,
                        'classSection': `${response[i].classAlias} - ${response[i].classSection}`,
                        'subjectName': response[i].subjectName !== null ? response[i].subjectName : '-',
                        'startTime': `${moment(response[i].startDate).format("DD-MMM-YYYY")} <br> ${ moment(response[i].startTime, "HH:mm").format("hh:mm A")} ( ${moment.utc(+duration).format('H:mm')} Hrs)`,
                        'detail': `
                                    <button class='btn btn-success btn-xs conference-detail'
                                    data-response=' ${ handleApostropheWork(response[i])}' >
                                    ${messages.viewDetail} </button>
                                    <br>
                                    <button class='btn btn-primary btn-xs invite-list'
                                    data-meetingID='${response[i].meetingID }'>
                                    ${messages.viewInvites} </button>
                                    <br>
                                    <a class='btn btn-success btn-xs' target="_blank" href="${response[i].webLink}" >
                                    <span class="tooltiptext" id="myTooltip">${messages.startMeeting}</span>
                                </a>
                                    `,
                        'action' : `<div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs conference-edit" data-meeting-id="${response[i].meetingID}" data-toggle="tooltip"  title="${messages.edit}" data-json='${JSON.stringify(response[i])}'><i class="fa fa-pencil"></i></button>

                        ` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-meeting" data-meetingID='${response[i].meetingID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        <input type="text"  class="offscreen" aria-hidden="true" style="border:0 !important; background:none !important"  readonly value="${response[i].webLink}" id="myInput-${i}" >
                        <button class='btn btn-primary btn-xs copy' data-id="myInput-${i}" >
                            <span class="tooltiptext" id="myTooltip">${messages.copyURL}</span>
                        </button>
                        </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getActivities');
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'hostUserName'},
            {'data': 'classSection'},
            {'data': 'subjectName'},
            {'data': 'startTime'},
            {'data': 'detail'},
            {'data': 'action'}
        ]
    });
}

$(document).on('click', '.conference-detail', function(){

    var data = JSON.parse($(this).attr('data-response'));
    $('.recurring-tr').addClass('hidden')
    if(data.isRecurring){
        $.ajax({
            url: herokuUrl+'/getMeetingRecurrence',
            method: 'POST',
            headers,
            data: {
                meetingID : data.meetingID,
            },
            beforeSend: function(){
            },
            success: function(res){
                response = parseResponse(res);
                var recurrenceDetailTable = $('#recurrence-detail-table tbody')
                if(typeof response !== 'undefined' && response.length >= 1){
                    $('.recurrence-list').show(500);
                    recurrenceDetailTable.empty();
                    response.forEach(function(value, index){
                        recurrenceDetailTable.append(`
                            <tr>
                                <td>${value.weekdayName}</td>
                                <td>${ moment(value.startDateTime).format("DD-MMM-YYYY")}</td>
                                <td>${ moment(value.startTime, "HH:mm").format("hh:mm A")}</td>
                                <td>${ moment(value.endTime, "HH:mm").format("hh:mm A")} </td>
                                <td>
                                ${__DEL__ ? `
                                <button class="btn-danger btn btn-xs delete-meeting-occurence" data-recurrenceID='${value.recurrenceID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                                ` : ''}</td>
                            </tr>
                        `);
                    });
                }
            },
            error: function(xhr){
                invokedFunctionArray.push('getMeetingRecurrence');
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    swal(
                        xhr.responseJSON.response.userMessage,
                        messages.errorMessage,
                        'error',{
                            buttons:true,//The right way
                            buttons: messages.ok, //The right way to do it in Swal1
                        }
                    )
                    return false;
                }
                if(xhr.status === 400){
                    swal(
                        xhr.responseJSON.response.userMessage,
                        messages.errorMessage,
                        'error',{
                            buttons:true,//The right way
                            buttons: messages.ok, //The right way to do it in Swal1
                        }
                    )
                    return false;
                }
                if(xhr.status === 500){
                    swal(
                        xhr.responseJSON.reason,
                        messages.errorMessage,
                        'error',{
                            buttons:true,//The right way
                            buttons: messages.ok, //The right way to do it in Swal1
                        }
                    )
                    return false;
                }
            }
        });
        var weekDaysPill = '';
        var weekDays = data.weekdayNo;
        if(weekDays){
            weekDays = weekDays.split(',')
            var weekdayName = data.weekdayName;
            weekdayName = weekdayName.split(',')
            weekDays.forEach(function(value, index){
                weekDaysPill += `<label class="label font-weight-bold label-sm label-primary m-1"> ${weekdayName[index]} </label>`
            });
            $('.recurring-tr').removeClass('hidden')
        }
        $('#weekdayNo').html(weekDaysPill);
    }
    var mt = data.meetingTopic && data.meetingTopic != 'null' ? data.meetingTopic : ''
    $('#meetingTopic').text(mt.replace(/&/g, "'"));
    // $('#meetingLink').text(data.webLink);
    $('#meetingLink').html(`<a href="${data.webLink}" target="_blank">${data.webLink}</a>`);
    $('#meetingPassword').text(data.meetingPassword);
    $('#hostName').html(data.hostName)
    var desp = data.agendaDesc && data.agendaDesc != 'null' ? data.agendaDesc : '';
    $('#agendaDesc').text(desp.replace(/&/g, "'"));
    // $('#agendaDesc').text(desp && desp != 'null' ? desp.replace(/&/g, "'") : '');
    $('#statusTd').html(`<label class="label label-sm label-${data.isActive ? 'success' : 'danger'}"> ${data.isActive ? messages.active : messages.inActive}</label>`);
    $('#startDateDetail').text(moment(data.startDate).format("DD-MMM-YYYY"));
    $('#endDateDetail').text(data.endDate !== null ?  moment(data.endDate).format("DD-MMM-YYYY") : '-');
    $('#reminderOn2').html( data.reminderOn ? data.reminderTime : `<label class="label label-sm font-weight-bold label-${data.reminderOn ? 'success' : 'danger'}"> ${data.reminderOn ? messages.yes : messages.no}</label>`);
    $('#isRecurring2').html(data.isRecurring ? `<label class="label font-weight-bold label-sm label-primary"> ${data.recurringType === 'W' ? 'Weekly' : 'Daily'}</label>` :  `<label class="label font-weight-bold label-sm label-${data.isRecurring ? 'success' : 'danger'}"> ${data.isRecurring ? messages.yes : messages.no}</label>`);
    $('#conferenceDetailModal').modal('toggle');
    $('#conferenceDetailModal').modal('show');

})

$(document).on('click', '.invite-list', function(){
    var meetingID = $(this).attr('data-meetingID');
    if (inviteListTable) {
        inviteListTable.destroy();
    }
    inviteListTable = $('#invite-list-table').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax' : {
            url: herokuUrl+'/getMeetingInvitees',
            type: 'POST',
            headers,
            data:{
                meetingID: meetingID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'rollNo': `${response[i].rollNo} `
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getMeetingInvitees');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'rollNo'},
        ]
    });
    $('#conferenceInvitesModal').modal('toggle');
    $('#conferenceInvitesModal').modal('show');

})

$(document).on('click', '#viewChallanNoteButton', function(){
    $('#noteTableRow').slideDown();
    $('#viewChallanNoteButton').hide()
    $('#invoicechallanNote').html(`
    <button id="closeChallanNoteButton" class="btn btn-sm btn-danger circle">
        <i class="fa fa-arrow-up"></i>
    </button>`);

})

$(document).on('click', '#closeChallanNoteButton', function(){
    $('#noteTableRow').slideUp();
    $('#closeChallanNoteButton').hide()
    $('#invoicechallanNote').html(`
    <button id="viewChallanNoteButton" class="btn btn-sm btn-primary circle">
        <i class="fa fa-arrow-down"></i>
    </button>`);
})


function createObjectFromAnotherObject(oldObject){
    var newObject = {};
    for (var propName in oldObject) {
        newObject[propName] = oldObject[propName];
    }
    return newObject;
}

$('#conference-form').on('submit', function(e){
    e.preventDefault();
    if($("#filter-schools option:selected").val() == ''){
        swal(
            messages.pleaseSelectSchool,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return  false;
    }

    if($("#filter-branches option:selected").val() == ''){
        swal(
            messages.pleaseSelectBranch,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return  false;
    }

    if($("#filter-classLevels option:selected").val() == ''){
        swal(
            messages.pleaseSelectClass,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return   false;
    }

    if($("#filter-classes option:selected").val() == ''){
        swal(
            messages.pleaseSelectSection,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return   false;
    }

    if($("#filter-class-teachers option:selected").val() == ''){
        swal(
            messages.pleaseSelectTeacher,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return  false;
    }
    if($("#filter-students option:selected").val() == '' || $("#filter-students option:selected").val() === undefined){
        swal(
            messages.pleaseSelectAtleastOneStudent,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return  false;
    }
    if($('#meetingTopic').val() == ''){
        swal(
            messages.pleaseEntertheMeetingTopic,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return  false;
    }
    if($('#startDate').val() == ''){
        swal(
            messages.pleaseSelectStartDateFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return  false;
    }
    if($('#webLink').val() == ''){
        swal(
            messages.pleaseEnterTheWeblinkFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
          return  false;
    }
    comparingArray()
    if(isExist === true){
        return  false;
    }
    $.LoadingOverlay("show");
    var formData = $('#filter-form').serializeArray();
    var conferenceData = $(this).serializeArray();
    var obj = dataObj = {}
    obj['isActive'] = 1
    obj['isRecurring']  = $('#isRecurring').val()
    obj['reminderOn'] = $('#reminderOn').val()
    if(idEditButtonClicked ){
        obj['meetingRefNo'] = 'meetingRefNo';
        // obj['webLink'] = 'webLink'
    }
    obj['startTime'] = `${$('#startHourInput').val()}:${$('#startMinuteInput').val()}`
    obj['endTime'] = `${$('#endHourInput').val()}:${$('#endMinuteInput').val()}`
    obj['createdBy'] = user.userID;
    if(user.isAdmin === 1){
        obj['roomName'] =  $('#filter-class-teachers option:selected').text();
    }else{
        obj['roomName'] =  user.fullName
    }
    obj['studentIDList'] = studentID;
    formData.forEach(function(val, ind){
        if(val.name !== 'schoolID' && val.name !== 'studentID'){
            obj[val.name] = val.value;
        }
    })
    if(user.isAdmin !== 1)
        obj['hostUserID'] = user.userID
    dataObj = populateObject(obj, conferenceData);
    if(!idEditButtonClicked){
        delete dataObj['meetingRefNo']
        // delete dataObj['webLink']
        delete dataObj['meetingID']
    }
    // if(isWebLinkExist === true && $('#webLink').val() !== ''){
        obj['webLink'] = $('#webLink').val()
    // }else{
    //     delete dataObj['webLink']
    // }
    if(isRecurrence && dataObj['recurrenceRangeType'] === "D"){
        delete dataObj['noOfRecurrence']
    }else if(isRecurrence && dataObj['recurrenceRangeType'] === "O"){
        delete dataObj['endDate']
    }
    if(isRecurrence && dataObj['recurringType'] === "W")
        dataObj['weekdayNo'] = weekNo.join(',');
    showProgressBar(submitLoader);
    resetMessages();
    submitConferrence(dataObj);
});

function populateObject(obj, conferenceData){
    var newObject = {};
    var fieldsArray = [
        'numberOfRecurence',
        'recurrenceEndDate',
        'recurringEndDate',
        'recurringStartDate',
        'isRecurring'
    ]
    var extraFieldsArray = [
        'recurrenceEndDate',
        'recurringStartDate',
        'recurringEndDate'
    ];
    newObject = createObjectFromAnotherObject(obj);
    conferenceData.forEach(function(val, ind){
        if(val.name !== 'startHour' && val.name !== 'startMinute' &&
        val.name !== 'endHour' && val.name !== 'endMinute'){
            if(val.name === 'userID'){
                newObject['hostUserID'] = val.value;
            }else{
                if(isRecurrence && jQuery.inArray( val.name, fieldsArray ) >= 0){
                    if(jQuery.inArray( val.name, extraFieldsArray ) < 0){
                        newObject[val.name] = val.value;
                    }
                }else if (jQuery.inArray( val.name, fieldsArray ) < 0){
                    newObject[val.name] = val.value;
                }
            }
        }
    })
    return newObject;
}

$(document).on('click', '.delete-meeting', function(){
    meetingID = $(this).attr('data-meetingID');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheMeeting+"</br>",
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteMeeting();
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

$(document).on('click', '.delete-meeting-occurence', function(){
    recurrenceID = $(this).attr('data-recurrenceID');
    var tr = $(this).closest('tr')
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheOccurence+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                   deleteMeetingOcccurence(tr);
               }
           },
           cancel: function (){
           },
       }
   });

})

function deleteMeetingOcccurence(tr){
    $.ajax({
        url: herokuUrl+'/deleteMeetingOccurence',
        method: 'POST',
        headers,
        data: {
            recurrenceID : recurrenceID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                // $('#conferenceDetailModal').modal('toggle');
                // $('#conferenceDetailModal').modal('hide');
                // Swal(
                //     'Deleted!',
                //     'Meeting Occurence has been removed.',
                //     'success'
                // )

                // recurrenceID = '';
                // getConferrences();
                tr.fadeOut('slow');
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteMeetingOccurence');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

function deleteMeeting(){
    $.ajax({
        url: herokuUrl+'/deleteMeetingInvite',
        method: 'POST',
        headers,
        data: {
            meetingID : meetingID,
        },
        beforeSend: function(){
        },
        success: function(res){
            meetingCheckArray.forEach(function(value, index){
                var keys = Object.keys(value)
                keys.forEach(function(keyValue,keyIndex){
                    if(value[keyValue] === meetingID){
                        if (index > -1) {
                            meetingCheckArray.splice(index, 1);
                        }
                    }
                })
            })
            if(res.response === 'success'){
                swal(
                    messages.meetingHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                setTimeout(() => {
                    swal.close()
                }, 1000);
                meetingID = '';
                getConferrences();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteMeetingInvite');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('change', '.pattern', function(){
    var type = $(this).attr('data-name');
    var weeklyDiv = $('.weekly-div');
    $("input.weekly:checkbox:not(:checked)")
    if(type === 'weekly'){
        weeklyDiv.show();
        weeklyDiv.removeClass('hidden');
    }else if(type === 'daily'){
        weeklyDiv.hide();
        weeklyDiv.addClass('hidden');
    }
});

$(document).on('change', '.range-end', function(){
    var val = $(this).val();
    if(val === 'O'){
        $("#iteration").attr('data-validation','required')
        $("#endDate").removeAttr('data-validation');
        // $('#endDate').attr('type','hidden')
        $('#endDate').attr('name','')
    }else{
        $('#endDate').attr('type','date')
        $('#endDate').attr('name','endDate')
        $('#iteration').val('')
        $("#iteration").removeAttr('data-validation');
        $("#endDate").attr('data-validation','required')
    }
    var fieldToEnable = $(this).attr('data-fieldIdToEnable');
    var fieldsToDisable = $(this).attr('data-fieldIdsToDiable');
    fieldsToDisable = fieldsToDisable.length >= 1 ? fieldsToDisable.split(",") : [];
    fieldToEnable = fieldToEnable.length >= 1 ? fieldToEnable.split(",") : [];
    if(typeof fieldsToDisable !== 'undefined' && typeof fieldToEnable !== 'undefined' ){
            toggleActiveInactive(fieldsToDisable, true);
            toggleActiveInactive(fieldToEnable, false);
    }
});

function disableCheckboxes(className){
    $('.' + className).each((i, item) => {
        $(this).prop('checked', false);
    })
}

function toggleActiveInactive(array, onOff){
    if(array.length >= 1)
        array.forEach((val, index) => {
            if(val === 'addonCalender' && onOff === true ){
                $('#' + val).hide();
            }else if(val === 'addonCalender' && onOff === false ){
                $('#' + val).show();
            }else{
                $('#' + val).attr('disabled', onOff);
            }
        });
}


$('#isGeneralMeeting').on('change', function(){
    if($(this).prop('checked')){
        $('#subject').hide(1000)
    }else{
        $('#subject').show(1000)
    }
});

$('#recurrenceToggle').on('change', function(){
    if($(this).prop('checked')){

        isRecurrence = true
        $('#isRecurring').val(1)
        $('#endDate').attr('type','date')
        $('#endDate').attr('name','endDate')
        $('#recuurence-div').show()
    }else{
        $('#isRecurring').val(0)
        $('#endDate').attr('name','')
        $('#endDate').attr('type','hidden')
        $('#recuurence-div').hide()
        isRecurrence = false
    }
});

$('#reminderToggle').on('change', function(){
    if($(this).prop('checked')){
        isReminder = true
        $('#reminderTime-div').show()
        $('#reminderOn').val(1)
    }else{
        $('#reminderOn').val(0)
        $('#reminderTime-div').hide()
        isReminder = false
    }
});

$(document).on('change', '.weekly', function(){
    var no = $(this).attr('data-no');
    if($(this).prop('checked')){
        weekNo.push(no)
    }else{
        delete weekNo.pop(no)
        // if (index > -1){
        //     weekNo.splice(index, 1);
        // }
    }
})

function round5(x)
{
    return Math.ceil(x/5)*5;
}

$(document).on('click', '.copy', function(){
    var id = $(this).attr('data-id')
    var copyText = document.getElementById(id);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy")
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  });

function CopyToClipboard(containerid) {
    if (document.selection) {
    var range = document.body.createTextRange();
    range.moveToElementText(document.getElementById(containerid));
    range.select().createTextRange();
    document.execCommand("copy");
    } else if (window.getSelection) {
    var range = document.createRange();
    range.selectNode(document.getElementById(containerid));
    window.getSelection().addRange(range);
    document.execCommand("copy");

    }
}

$('#startDate').on('change', function(){
    if($('#endDate').length){
        $('#endDate').val($(this).val())
        $('#endDate').attr('min',$(this).val())
    }
    calculateTime();
    setMinutesDropdown();
})

$('#startHour').on('change', function(){
    $('#startHourInput').val($(this).val())
    // comparingArray()
    startHour = $(this).val()
    endHour = $('#endHour').val()
    if(startHour > endHour){
        $('#endHour').removeAttr('selected')
        $('#endHour').select2();
        $('#endHour').val(startHour)
        $('#endHourInput').val(startHour)
        $('#endHour').select2();
    }
    $('#endHour option').removeAttr('disabled');
    startHour = $(this).val()
    enableDisableEndHourOption(startHour)
    if(!idEditButtonClicked){
        setMinutesDropdown();
    }
})

$('#endHour').on('change', function(){
    $('#endHourInput').val($(this).val())
    // comparingArray()
    // setMinutesDropdown();
    if(!idEditButtonClicked){
        setEndMinutesDropdown();
    }
})

$('#startMinute').on('change', function(){
    $('#startMinuteInput').val($(this).val())
    // comparingArray()
    startMinute = $(this).val()
    endMinute = $('#endMinute').val()
    if(startMinute > endMinute){
        $('#endMinute').removeAttr('selected')
        $('#endMinute').select2();
        $('#endMinute').val(startMinute)
        $('#endMinute').select2();
    }
    $('#endMinute option').removeAttr('disabled');
    var startMinute = parseInt($(this).val())
    var endMinute = parseInt($("#endMinute option:selected").val())
    if(startMinute <= endMinute){
        var newEndTime = startMinute + 5;
        newEndTime =  newEndTime < 9 ? '0' + newEndTime.toString() : newEndTime;
        $("#endMinute").val(newEndTime).select2()
    }
    enableDisableEndMinuteOption(startMinute)
})

$('#endMinute').on('change', function(){
    $('#endMinuteInput').val($(this).val())
    // comparingArray()
})

function enableDisableEndHourOption(endHour){
    selectedStartDate = $('#startDate').val()
    currentDateTime = moment().add(0,'minutes');
    currentDate = currentDateTime.format('YYYY-MM-DD');
    if(selectedStartDate <= currentDate){
        for (let loopLength = 0; loopLength < endHour; loopLength++) {
            var matchedValue = loopLength
            if(loopLength <=  9) matchedValue = '0'+loopLength
            $('#endHour').find("option[value=" + matchedValue + "]").attr('disabled', 'disabled');
        }
        $('#endHour').select2();
    }else{
        setTimeout(() => {
            let selectedHourStart = $('#startHour').val()
            if(selectedHourStart == ''){
                selectedHourStart = $('#endHourInput').val()
            }
            if(idEditButtonClicked === true){
                selectedHourStart = selectedHourStart
                for (let loopLength = 0; loopLength < selectedHourStart; loopLength++) {
                    var matchedValue = loopLength
                    if(loopLength <=  9) matchedValue = '0'+loopLength
                    $('#endHour').find("option[value=" + matchedValue + "]").attr('disabled', 'disabled');
                }
                $('#endHour').select2();
            }else{
                for (let loopLength = 0; loopLength < selectedHourStart; loopLength++) {
                    var matchedValue = loopLength
                    if(loopLength <=  9) matchedValue = '0'+loopLength
                    $('#endHour').find("option[value=" + matchedValue + "]").attr('disabled', 'disabled');
                }
                $('#endHour').select2();
            }
        }, 200);
    }

}

function enableDisableStartHourOption( startHour ){
    for (let loopLength = 0; loopLength < startHour; loopLength++) {
        var matchedValue = loopLength
        if(loopLength <=  9) matchedValue = '0'+loopLength
        $('#startHour').find("option[value=" + matchedValue + "]").attr('disabled', 'disabled');
    }
    $('#startHour').select2();
}


function calculateTime(){
    $('#startHour option').removeAttr('disabled');
    $('#endHour option').removeAttr('disabled');
    currentDateTime = moment().add(0,'minutes');
    startHour = currentDateTime.format('HH');
    endHour = currentDateTime.add(2, 'hours');
    endHour = endHour.format('HH');
    $('#startHour').val(startHour);
    $('#endHour').val(endHour);
    setMinutesDropdown();
    var startDateSelected = $('#startDate').val();
    var currentDate = currentDateTime.format('YYYY-MM-DD');
    $('#endDate').val(currentDateTime.format('YYYY-MM-DD'))
    if(moment(startDateSelected).diff(moment(currentDate), 'days') === 0){
        enableDisableStartHourOption(startHour);
    }
    enableDisableEndHourOption(startHour);
    $('#startHour').select2();
    $('#endHour').select2();

}

function setEndMinutesDropdown(){
    // $('#startMinute option').removeAttr('disabled');
    $('#endMinute option').removeAttr('disabled');
    currentDateTime = moment().add(0,'minutes');
    startMinutes = currentDateTime.format('mm');
    currentDateTime = moment().add(10,'minutes');
    endMinutes = currentDateTime.format('mm');
    var stTime = round5(startMinutes)
    var enTime = round5(endMinutes);
    if(stTime === 60){
        stTime = '05';
    }else if(stTime === 0){
        stTime = '05'
    }

    if(enTime === 60){
        enTime = '05'
    }else if(enTime === 0){
        enTime = '05'
    }
    // $('#startMinute').val(stTime);
    // $('#startMinuteInput').val(stTime);
    $('#endMinute').val(enTime);
    var startDateSelected = $('#startDate').val();
    var currentDate = currentDateTime.format('YYYY-MM-DD');
    $('#endDate').val(currentDateTime.format('YYYY-MM-DD'))
    if(moment(startDateSelected).diff(moment(currentDate), 'days') === 0){
        enableDisableStartMinuteOption(stTime);
    }
    enableDisableEndMinuteOption(stTime);
    // $('#startMinute').select2();
    $('#endMinute').select2();
}

function setMinutesDropdown(){
    $('#startMinute option').removeAttr('disabled');
    $('#endMinute option').removeAttr('disabled');
    currentDateTime = moment().add(0,'minutes');
    startMinutes = currentDateTime.format('mm');
    currentDateTime = moment().add(10,'minutes');
    endMinutes = currentDateTime.format('mm');
    var stTime = round5(startMinutes)
    var enTime = round5(endMinutes);
    if(stTime === 60){
        stTime = '05';
    }else if(stTime === 0){
        stTime = '05'
    }

    if(enTime === 60){
        enTime = '05'
    }else if(enTime === 0){
        enTime = '05'
    }
    $('#startMinute').val(stTime);
    $('#startMinuteInput').val(stTime);
    $('#endMinute').val(enTime);
    var startDateSelected = $('#startDate').val();
    var currentDate = currentDateTime.format('YYYY-MM-DD');
    $('#endDate').val(currentDateTime.format('YYYY-MM-DD'))
    if(moment(startDateSelected).diff(moment(currentDate), 'days') === 0){
        enableDisableStartMinuteOption(stTime);
    }
    enableDisableEndMinuteOption(stTime);
    $('#startMinute').select2();
    $('#endMinute').select2();
}

function enableDisableEndMinuteOption( endMinute ){
    selectedStartHour = $('#startHour').val()
    selectedEndHour = $('#endHour').val()

    if(selectedEndHour <= selectedStartHour){
        for (let loopLength = 0; loopLength <= endMinute; loopLength++) {
        var matchedValue = loopLength
        if(loopLength <= 9) matchedValue = '0'+loopLength
        if(loopLength % 5 === 0)
            $('#endMinute').find("option[value=" + matchedValue + "]").attr('disabled', 'disabled');
        }
        $('#endMinute').select2();
    }
}

function enableDisableStartMinuteOption( startMinute ){
    var startHour = $('#startHour').val()
    currentDateTime = moment().add(0,'minutes');
    currentHour = currentDateTime.format('HH');
    if(startHour <= currentHour){
        for (let loopLength = 0; loopLength < startMinute; loopLength++) {
            var matchedValue = loopLength
            if(loopLength <= 9) matchedValue = '0'+loopLength

            $('#startMinute').find("option[value=" + matchedValue + "]").attr('disabled', 'disabled');
        }
        $('#startMinute').select2();
    }
}

$(document).on('keyup', '#iteration', function(){
    $('.range-end').attr('checked', false);
    $("input[value=O]").prop('checked', true).trigger('change');
})

$(document).on('change', '#endDate', function(){
   $('.range-end').attr('checked', false);
    $("input[data-value=D]").prop('checked', true).trigger('change');
})

// $(document).on('click', '.conference-edit', function(){

// })

function comparingArray(){
    isExist = false;
    meetingTeacherID = Number($('#filter-class-teachers option:selected').val())
    var classLevelID = Number($('#filter-classLevels option:selected').val())
    var classID = Number($('#filter-classes option:selected').val())
    meetingStartDate = $('#startDate').val()
    meetingEndDate = $('#startDate').val()
    meetingStartTime = `${$('#startHour option:selected').val()}:${$('#startMinute option:selected').val()}:00`
    meetingEndTime = `${$('#endHour option:selected').val()}:${$('#endMinute option:selected').val()}:00`
    var startDateTime = meetingStartDate+' '+meetingStartTime;
    startDateTime = moment(startDateTime).unix()
    var endDateTime = meetingEndDate+' '+meetingEndTime;
    endDateTime = moment(endDateTime).unix()
    if(!idEditButtonClicked){
        meetingCheckArray.forEach(function(value, index){
            // if((startDateTime >= value.startDateTime  && startDateTime <= value.endDateTime) || (endDateTime >= value.startDateTime && endDateTime <= value.endDateTime) || (startDateTime >= value.startDateTime  && startDateTime <= value.endDateTime && value.hostUserID === meetingTeacherID) || (endDateTime >= value.startDateTime && endDateTime <= value.endDateTime && value.hostUserID === meetingTeacherID) || (startDateTime <= value.startDateTime && endDateTime >= value.endDateTime && value.hostUserID === meetingTeacherID)){
            if((startDateTime >= value.startDateTime  && startDateTime <= value.endDateTime && value.classID === classID) || (endDateTime >= value.startDateTime && endDateTime <= value.endDateTime && value.classID === classID) || (startDateTime >= value.startDateTime  && startDateTime <= value.endDateTime && value.hostUserID === meetingTeacherID) || (endDateTime >= value.startDateTime && endDateTime <= value.endDateTime && value.hostUserID === meetingTeacherID) || (startDateTime <= value.startDateTime && endDateTime >= value.endDateTime && value.hostUserID === meetingTeacherID)){
                isExist = true
                swal(
                    messages.meetingAlreadyExistInSameTimeInterval,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        });
    }else{
        var meetingID = $('.conference-edit').attr('data-meeting-id')
        meetingCheckArray.forEach(function(value, index){
            var keys = Object.keys(value)
            keys.forEach(function(keyValue,keyIndex){
                if(value[keyValue] === meetingID){
                    if (index > -1) {
                        meetingCheckArray.splice(index, 1);
                    }
                }
            })
        })
        meetingCheckArray.forEach(function(value, index){
            if(findInArray(value, meetingID) !== -1){
                if(
                    (startDateTime >= value.startDateTime && startDateTime <= value.endDateTime && value.classID === classID) ||
                    (endDateTime >= value.startDateTime && endDateTime <= value.endDateTime && value.classID === classID) ||
                    (startDateTime >= value.startDateTime  && startDateTime <= value.endDateTime && value.hostUserID === meetingTeacherID) ||
                    (endDateTime >= value.startDateTime && endDateTime <= value.endDateTime && value.hostUserID === meetingTeacherID) ||
                    (startDateTime <= value.startDateTime && endDateTime >= value.endDateTime && value.hostUserID === meetingTeacherID)
                ){
                    isExist = true
                    swal(
                        messages.meetingAlreadyExistInSameTimeInterval,
                        messages.oops,
                        'error',{
                            buttons:true,//The right way
                            buttons: messages.ok, //The right way to do it in Swal1
                        }
                    )
                    return false;
                }
            }
        });
    }
}

function findInArray(ar, val) {
    for (var i = 0,len = ar.length; i < len; i++) {
        if ( ar[i] === val ) { // strict equality test
            return i;
        }
    }
    return -1;
}

$(document).on('click', '.conference-edit', function () {
    if($('#filter-classes').val() === ''){
        swal(
            messages.pleaseSelectSection,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    hideFields()
    $('#add-form-error-message').hide()
    idEditButtonClicked = true
    var json = JSON.parse($(this).attr('data-json').replace(/&/g, "\'"));
    var keys = Object.keys(json);
    keys.forEach((value, index) => {
        if(value == 'hostUserID') {
            $('#filter-class-teachers').val(json[value]);
            $('#filter-class-teachers').multiselect('rebuild');
        }else if (value == 'studentIDList') {
            if(json[value] != '0'){
                    var students = json[value].split(',');
                    $("#filter-students").multiselect('deselectAll', true)
                    students.forEach(function(stID, stInd){
                        $("#filter-students").find('option[value="'+stID+'"]').prop('selected', true);
                    });
                    $('#filter-students').multiselect('rebuild');
            }else{
                $("#filter-students").find('option[value="'+json[value]+'"]').prop('selected', true);
                $('#filter-students').find('option').not('option:first').attr({
                    'disabled' : true,
                });
                $('#filter-students').multiselect('rebuild');
            }
        }else if (value == 'subjectID') {
            $('#filter-subjects').val(json[value]);
            $('#filter-subjects').multiselect('rebuild');
        }else if(value == 'meetingTopic'){
            $('.meetingTopic').val(json[value])
        }else if(value == 'agendaDesc'){
            $('.agendaDesc').val(json[value])
        }
        else if(value == 'webLink'){
            $('.webLink').val(json[value])
            // var checkURL = new URL(json[value])
            // if(checkURL.hostname != 'www.infinitystudio.pk'){
            //     $('.customWebLinkDiv').show(500)
            //     $('#customLinkOpenBtn').hide()
            //     $('.customLinkBtnDiv').html(`
            //     <button id="customLinkCloseBtn" class="btn btn-sm btn-danger circle">
            //     Remove Custom Link <i class="fa fa-arrow-left"></i>
            //     </button>`);
                // if(checkURL.hostname == 'meet.google.com'){
                //     $('.weblinkRadios').prop('checked', false)
                //     $('#meetRadioBtn').prop('checked', true)
                // }else{
                //     $('.weblinkRadios').prop('checked', false)
                //     $('#zoomRadioBtn').prop('checked', true)
                // }
            // }else{
            //     $('.customWebLinkDiv').hide(500)
            //     $('#customLinkCloseBtn').hide()
            //     $('.customLinkBtnDiv').html(`
            //     <a id="customLinkOpenBtn" class="btn btn-sm btn-primary circle" >Add Custom Link <i class="fa fa-arrow-right"></i> </a>
            //     </button>`);
            // }
        }
        else if (value == 'reminderTime') {
            $('#filter-reminderTime').val(json[value]).trigger('change');
        }else if(value == 'startDate'){
            $('#startDate').val(json[value]).trigger('change');
        }else if (value == 'startTime') {
            var startTimeArray = json[value].split(':');
            $('#startHour').val(startTimeArray[0]).select2();
            $('#startMinute').val(startTimeArray[1]).select2();
            $('#startHourInput').val(startTimeArray[0])
            $('#startMinuteInput').val(startTimeArray[1])
        }else if (value == 'endTime') {
            var endTimeArray = json[value].split(':');
            $('#endHour').val(endTimeArray[0]).select2();
            $('#endMinute').val(endTimeArray[1]).select2();
            $('#endHourInput').val(endTimeArray[0])
            $('#endMinuteInput').val(endTimeArray[1])
        }else if (value == 'isRecurring') {
            setTimeout(() => {
                if (json[value] === 1) {
                    $('#recurrenceToggle').prop('checked', true);
                    $('#recuurence-div').show()
                    $('#isRecurring').val(1)
                    $('#endDate').attr('type','date')
                    $('#endDate').attr('name','endDate')
                } else {
                    $('#isRecurring').val(0)
                    $('#endDate').attr('name','')
                    $('#endDate').attr('type','hidden')
                    $('#recurrenceToggle').attr('checked', false);
                    $('#recuurence-div').hide()
                }
            }, 500);
        }else if (value == 'recurringType') {
            $('.pattern').prop('checked', false);
            $(".weekly-div").hide();
            if(json[value] === 'W'){
                $(".weekly-div").show();
                $("input[value=" + json[value] + "]").prop('checked', true).trigger('change');
            }else if(json[value] === 'D'){
                    $(".weekly-div").hide();
                    $("input[value=" + json[value] + "]").prop('checked', true).trigger('change');
            }
        }else if (value == 'weekdayNo') {
            $('.weekly').prop('checked', false);
            if(json[value]){
                var weekDayNos = json[value].split(',');
                weekNo = weekDayNos
                weekDayNos.forEach(function(WeekValue, index){
                    $("input[value=" + WeekValue + "]").prop('checked', true);
                });
            }
        }else if(value == 'recurrenceRangeType'){
            $('.range-end').removeAttr('checked');
            if(json[value] == 'D'){
                    $('#iteration').attr('disabled','disabled')
                    $("#radiobg5").prop('checked', true);

            }else if(json[value] == 'O'){
                setTimeout(() => {
                    $('#iteration').removeAttr('disabled')
                    $('#endDate').attr('disabled','disabled')
                    $("#radiobg4").prop('checked', true);
                }, 500);
            }
        }else if(value == 'noOfRecurrence'){
            setTimeout(() => {
                $('#iteration').val(json[value])
            }, 500);
        }else if(value == 'endDate'){
            $('#endDate').val(json[value]).trigger('change')
        }else if (value == 'reminderOn') {
            $('#reminderTime-div').hide()
            $('#reminderToggle').prop('checked', false);
            if (json[value] == 1) {
                $('#reminderOn').val(json[value])
                $('#reminderTime-div').show();
                $('#reminderToggle').prop('checked', true);
            }else{
                $('#reminderOn').val(json[value])
                $('#reminderTime-div').hide();
                $('#reminderToggle').removeAttr('checked');
            }
        }else if(value == 'meetingID'){
            $('#meetingID').val(json[value])
        }else if(value == 'meetingRefNo'){
            $('#meetingRefNo').val(json[value])
        }else if(value == 'webLink'){
            $('#webLink').val(json[value])
        }
    })
        hideResultsList();
        showAddFormContainer();
})


// $(document).on('click', '#customLinkOpenBtn', function(){
//     isWebLinkExist = true
//     $('.customWebLinkDiv').show(500)
//     $('#webLink').attr('data-validation','required')
//     $('#customLinkOpenBtn').hide()
//     $('.customLinkBtnDiv').html(`
//     <button id="customLinkCloseBtn" class="btn btn-sm btn-danger circle">
//     Remove Custom Link <i class="fa fa-arrow-left"></i>
//     </button>`);
// })

// $(document).on('click', '#customLinkCloseBtn', function(){
//     isWebLinkExist = false
//     $('.customWebLinkDiv').hide(500)
//     $('#webLink').removeAttr('data-validation')
//     $('#customLinkCloseBtn').hide()
//     $('.customLinkBtnDiv').html(`
//     <a id="customLinkOpenBtn" class="btn btn-sm btn-primary circle" >Add Custom Link <i class="fa fa-arrow-right"></i> </a>
//     </button>`);
// })

// $(document).on('click', '#show-add-form-container', function(){
//     isWebLinkExist = false
// })
