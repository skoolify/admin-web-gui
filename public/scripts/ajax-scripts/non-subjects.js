$(document).ready(function(){
    getAjaxSchools();
    // getNonSubjects();
})

function submitNonSubject(){
    $.ajax({
        url: herokuUrl+'/setNonSubjects',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            let returnedID = Number(res.response[0].nonSubjectID);
            if(res.response.length > 0 && returnedID > 0){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                // $('#non-subjects-form').trigger('reset');
                $('#nonSubjectName').attr('disabled', true);

                if(classLevelID === '' || classLevelID.length == 0 ){
                    swal(
                        messages.error,
                        messages.pleaseSelectClass,
                        'error'
                    );
                }else{
                    getNonSubjectLegends(returnedID);
                    $('.non-subjects-legend-form-container').slideDown(1000);
                }
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
            getNonSubjects();
            $('#submit-add-form-container').attr('disabled',true);
        },
        error: function(xhr){
            invokedFunctionArray.push('submitNonSubject')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
function getNonSubjectLegends(nonSubjectID){
    $.ajax({
        url: herokuUrl+'/getNonSubjectLegends',
        type: 'POST',
        headers,
        beforeSend: function(){
            showProgressBar('p2-legend');
            $('#legends-table tbody').empty();
        },
        data: {
            nonSubjectID: nonSubjectID,
            classLevelID : classLevelID
        },
        success: function(res){
            response = parseResponse(res);
            populateLegendTable(response,nonSubjectID);
            hideProgressBar('p2-legend');

        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        }

    });
}
function getNonSubjects(){
    data = {};
    if(tableData){
        tableData.destroy();
    }

    if(user.isAdmin == 1){
        if(classLevelID != ''){
            data.classLevelID = classLevelID;
        }
        if(user.userID != ''){
            data.userID = user.userID;
        }
        tableData = $('#example44').DataTable({
            language: {
                url: __DATATABLE_LANGUAGE__
            },
            'ajax'       : {
                url: herokuUrl+'/getNonSubjects',
                // url: herokuUrl+'/getUserNonSubjects',
                // url: herokuUrl+'/getUserNonSubjectsWithTeacher',
                type: 'POST',
                headers,
                beforeSend: function(){
                    showProgressBar('p2-legend');
                },
                data,
                dataSrc: function(res){
                    hideProgressBar('p2-legend');
                    // response = parseResponse(res);
                    response = res.response;
                    var return_data = new Array();
                    if(response){
                        for(var i = 0; i< response.length; i++){
                            var buttonGroup = '';
                            if(typeof response[i].subjectDetails !== 'undefined' && response[i].subjectDetails.length >= 1){
                                response[i].subjectDetails.forEach(function(value, index){
                                    if(value.fullname !== null && value.fullname !== 'null' ){
                                        buttonGroup  += `<div class="btn-group">
                                        <button class="btn-primary btn btn-xs assign-teacher" data-subject-id="" data-toggle="tooltip" title="${value.fullname } ( ${value.classSection} )">${value.fullname } ( ${value.classSection} )</button>
                                        <button class="btn-danger btn btn-xs unassign-teacher" data-toggle="tooltip" title="${messages.unassignTeacher}" data-link-id='${value.nonSubjectLinkID}'><i class="fa fa-remove"></i></button></div></br>`;
                                    }
                                });
                            }
                            return_data.push({
                                'sNo' : i+1,
                                'nonSubjectName': response[i].nonSubjectName,
                                'fullName': `${buttonGroup !== '' ? buttonGroup : ' ---- '}`,
                                'classSection':  response[i].classAlias,
                                'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                                'action' : ` <div class="btn-group">
                                <button class="btn-danger btn btn-xs assign-teacher" data-non-subject-id="${response[i].nonSubjectID}" data-non-subject-link-id="${response[i].nonSubjectLinkID ? response[i].nonSubjectLinkID : ''}" data-toggle="tooltip" title="${messages.assignTeacher}">${messages.assignTeacher}</button>
                                ${__ACCESS__ ? `
                                    <button class="btn-primary btn btn-xs list-record-edit legend-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                                ${__DEL__ ? `
                                <button class="btn-danger btn btn-xs delete-non-subject" data-nonSubject-id='${response[i].nonSubjectID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                                ` : ''}
                                </div>`

                            })
                        }
                    }
                    return return_data;
                },
                error: function(xhr){
                    invokedFunctionArray.push('getNonSubjects')
                    window.requestForToken(xhr)
                    if(xhr.status === 404){
                        $('.dataTables_empty').text(messages.noDataAvailable);
                    }
                }
            },
            "columns"    : [
                {'data': 'sNo'},
                {'data': 'nonSubjectName'},
                {'data': 'fullName'},
                {'data': 'classSection'},
                {'data': 'isActive'},
                {'data': 'action'}
            ]
        });
    }else{
        if(classLevelID != ''){
            data.classLevelID = classLevelID;
        }
        if(user.userID != ''){
            data.userID = user.userID;
        }
        tableData = $('#example44').DataTable({
            language: {
                url: __DATATABLE_LANGUAGE__
            },
            'ajax'       : {
                // url: herokuUrl+'/getNonSubjects',
                // url: herokuUrl+'/getUserNonSubjects',
                url: herokuUrl+'/getUserNonSubjectsWithTeacher',
                type: 'POST',
                headers,
                beforeSend: function(){
                    showProgressBar('p2-legend');
                },
                data,
                dataSrc: function(res){
                    hideProgressBar('p2-legend');
                    // response = parseResponse(res);
                    response = res.response;
                    var return_data = new Array();
                    if(response){
                        for(var i = 0; i< response.length; i++){
                            var buttonGroup = '';
                            if(typeof response[i].nonSubjectDetails !== 'undefined' && response[i].nonSubjectDetails.length >= 1){
                                response[i].nonSubjectDetails.forEach(function(value, index){
                                    if(value.fullname !== null && value.fullname !== 'null' ){
                                        buttonGroup  += `<div class="btn-group">
                                        <button class="btn-primary btn btn-xs assign-teacher" data-subject-id="" data-toggle="tooltip" title="${value.fullname } ( ${value.classSection} )">${value.fullname } ( ${value.classSection} )</button>
                                        <button class="btn-danger btn btn-xs unassign-teacher" data-toggle="tooltip" title="${messages.unassignTeacher}" data-link-id='${value.nonSubjectLinkID}'><i class="fa fa-remove"></i></button></div></br>`;
                                    }
                                });
                            }
                            return_data.push({
                                'sNo' : i+1,
                                'nonSubjectName': response[i].nonSubjectName,
                                'fullName': `${buttonGroup !== '' ? buttonGroup : ' ---- '}`,
                                'classSection':  response[i].classAlias,
                                'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? 'Active' : 'In-Active'}</label>`,
                                'action' : ` <div class="btn-group">
                                <button class="btn-danger btn btn-xs assign-teacher" data-non-subject-id="${response[i].nonSubjectID}" data-non-subject-link-id="${response[i].nonSubjectLinkID ? response[i].nonSubjectLinkID : ''}" data-toggle="tooltip" title="${messages.assignTeacher}">${messages.assignTeacher}</button>
                                ${__ACCESS__ ? `
                                    <button class="btn-primary btn btn-xs list-record-edit legend-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                                ${__DEL__ ? `
                                <button class="btn-danger btn btn-xs delete-non-subject" data-nonSubject-id='${response[i].nonSubjectID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                                ` : ''}
                                </div>`

                            })
                        }
                    }
                    return return_data;
                },
                error: function(xhr){
                    invokedFunctionArray.push('getNonSubjects')
                    window.requestForToken(xhr)
                    if(xhr.status === 404){
                        $('.dataTables_empty').text(messages.noDataAvailable);
                    }
                }
            },
            "columns"    : [
                {'data': 'sNo'},
                {'data': 'nonSubjectName'},
                {'data': 'fullName'},
                {'data': 'classSection'},
                {'data': 'isActive'},
                {'data': 'action'}
            ]
        });
    }

}

$(document).on('click', '.assign-teacher', function(){

    nonSubjectID = $(this).attr('data-non-subject-id');
    nonSubjectLinkID = $(this).attr('data-non-subject-link-id');

    const toast = swal.mixin({
        toast: true,
        position: 'center-end',
        showConfirmButton: false,
        timer: 3000,
    });

    if($('#filter-class-teachers > option').length < 2){
        toast({
            type: 'error',
            title: messages.noTeacherAvailableForAssignment
        })
        return false;
    }

    if(nonSubjectID == ''){
        toast({
            type: 'error',
            title: messages.invalidSubject
        })
        return false;
    }

    // if(classID == ''){
    //     toast({
    //         type: 'error',
    //         title: 'Please select Section'
    //     })
    //     return false;
    // }

    let inputOptionsTeachers = {};
    let inputOptionsClasses = {};

    $("#filter-class-teachers > option").each(function(index) {
        if(this.value !== ''){
            inputOptionsTeachers[index.toString() + '-' +this.value.toString()] = this.text;
        }
    });

    $("#filter-classes > option").each(function() {
        if(this.value !== ''){
            inputOptionsClasses[this.value.toString()] = this.text;
        }
    });

    swal.mixin({
        input: 'select',
        confirmButtonText: messages.next+' &rarr;',
        showCloseButton: true,
        progressSteps: ['1', '2', '3']
        }).queue([
        {
            title: messages.selectSection,
            inputOptions: inputOptionsClasses,
        },
        {
            title: messages.selectTeacher,
            inputOptions: inputOptionsTeachers,
        },
        {
            title: messages.classTeacher,
            inputOptions: {
                '0' : messages.no,
                '1' : messages.yes,
            },
        }
        ]).then((result) => {

        if (result.value) {
            classID = result.value[0];
            classTeacherID = (result.value[1]).split('-');
            classTeacherID = classTeacherID[classTeacherID.length - 1];
            isClassTeacherFlag = result.value[2];
            assignTeacher();
        }
    });
});

function assignTeacher() {

    let data = {};
    // if(nonSubjectLinkID !== null && nonSubjectLinkID !== 'null'){
    //     data.nonSubjectLinkID = nonSubjectLinkID;
    // }

    data.userID = classTeacherID;
    data.nonsubjectID =  nonSubjectID;
    data.classID = classID;
    data.isClassTeacherFlag = isClassTeacherFlag;

    $.ajax({
        url: herokuUrl+'/assignNSTeacher',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.successfullyAssigned,
                    messages.successMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                // if(tableData){
                //     tableData.destroy();
                // }
                getNonSubjects();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('assignTeacher')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }

            swal(
                xhr.responseJSON.response.userMessage,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click', '.unassign-teacher', function(){
    deleteTeacherSubjectLinkID = $(this).attr('data-link-id');
    if(deleteTeacherSubjectLinkID !== null && deleteTeacherSubjectLinkID !== ''){
        Swal({
            title: messages.areYouSureQuestion,
            text: messages.youWontAbleToRevertThis,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: messages.yesUnassignIt,
            showCloseButton: true
          }).then((result) => {
            if (result.value) {
              unassignTeacher();
            }
          })
    }else{
        swal(
            messages.error,
            messages.invalidTeacher,
            'error'
        )
    }
});

function unassignTeacher() {
    $.ajax({
        url: herokuUrl+'/deleteNSTeacher',
        method: 'POST',
        headers,
        data: {
            nonSubjectLinkID: deleteTeacherSubjectLinkID
        },
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.teacherSuccessfullyUnassigned,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                // if(tableData){
                //     tableData.destroy();
                // }
                getNonSubjects();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('unassignTeacher')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
                swal(
                    xhr.responseJSON.userMessage,
                    messages.deletedMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                swal(
                    xhr.responseJSON.userMessage,
                    messages.deletedMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }

            swal(
                xhr.responseJSON.response.userMessage,
                messages.deletedMessage,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click','.list-record-edit' ,function(){
    let data = JSON.parse($(this).attr('data-json'))
    $('#nonSubjectName').val(data.nonSubjectName)
    $('#nonSubjectName').attr('disabled', false);
    $('#submit-add-form-container').removeAttr('disabled');
})

$(document).on('click', '.delete-non-subject', function(){
    var  nonSubjectID = $(this).attr('data-nonSubject-id');
    $.confirm({
        title: messages.confirmMessage,
        content: messages.areYouSure+' <br/> '+messages.youWantToDeleteTheNonSubject+'</br>',
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteNonSubject(nonSubjectID);
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})

function deleteNonSubject(nonSubjectID){
    $.ajax({
        url: herokuUrl+'/deleteNonSubject',
        method: 'POST',
        headers,
        data: {
            nonSubjectID: nonSubjectID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.nonSubjectHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getNonSubjects();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteNonSubject')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('click', '.legend-edit', function(){
    var nonSubjectJson = JSON.parse($(this).attr('data-json'));
    if(typeof nonSubjectJson !== undefined){
        $('.non-subjects-legend-form-container').slideDown(1000);
        getNonSubjectLegends(nonSubjectJson.nonSubjectID);
    }
})
$('#non-subjects-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#non-subjects-form').serialize();
    if($('#filter-classLevels').val() !== ''){
        data['classLevelID'] = $('#filter-classLevels').val()
    }else{
        swal(
            messages.pleaseSelectClass,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#nonSubjectName').val() !== ''){
        data['nonSubjectName'] = $('#nonSubjectName').val()
    }else{
        swal(
            messages.nonSubjectName,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    showProgressBar(submitLoader);
    resetMessages();
    submitNonSubject();
});

$(document).on('click', '#hide-non-subject-legend-container',function(){
    $('.non-subjects-legend-form-container').slideUp(1000);
});

$('#submit-add-form-container').on('click',function(){
    $('.non-subjects-legend-form-container').slideDown(1000);
});


$(document).on('click', '.legend-submit',function(){
       var legendValue = $(this).closest('tr').find('.legend').val();
       var nonSubjectID = $(this).closest('tr').find('.legend').attr('data-nonsubjectid');
       var isActive =  $(this).closest('tr').find('.isActive').prop('checked') === true ? 1 : 0;
       if(legendValue === '' || legendValue.length == 0){
            $(this).closest('tr').find('.legend').addClass('show-legend-error');
            $(this).closest('tr').find('.legend-error').removeClass('hidden');
       }else{
            setNonSubjectLegends(legendValue,nonSubjectID,isActive);
            $(this).closest('tr').find('.legend').removeClass('show-legend-error');
            $(this).closest('tr').find('.legend-error').addClass('hidden');
       }


});
$(document).on('click', '.update-legend-value',function(){
    var legendValue = $(this).closest('tr').find('.legend').val();
    var nonSubjectID = $(this).closest('tr').find('.legend').attr('data-nonsubjectid');
    var nsLegendID = $(this).closest('tr').find('.legend').attr('data-nsLegendID');
    var isActive =  $(this).closest('tr').find('.isActive').prop('checked') === true ? 1 : 0;
    if(legendValue === '' || legendValue.length == 0){
        $(this).closest('tr').find('.legend').addClass('show-legend-error');
        $(this).closest('tr').find('.legend-error').removeClass('hidden');
   }else{
        setNonSubjectLegends(legendValue, nonSubjectID, isActive, nsLegendID );
        $(this).closest('tr').find('.legend').removeClass('show-legend-error');
        $(this).closest('tr').find('.legend-error').addClass('hidden');
   }


});
function setNonSubjectLegends(legendValue,nonSubjectID,isActive, nsLegendID = null){
    var data = {};
    if(nsLegendID)
        data.nsLegendID = nsLegendID;
    if(legendValue)
        data.legendValue = legendValue;
    if(nonSubjectID)
        data.nonSubjectID = nonSubjectID;
    data.isActive = isActive
    $.ajax({
        url: herokuUrl+'/setNonSubjectLegends',
        type: 'POST',
        headers,
        beforeSend: function(){
            $('#legends-table tbody').empty();
            showProgressBar('p2-legend');
        },
        data,
        success: function(res){
            if(res.response === 'success' ){
                getNonSubjectLegends(nonSubjectID);
                hideProgressBar('p2-legend');
            }

        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        }

    });
}
function populateLegendTable(existingLegendArray, nonSubjectID ){
    $('#legends-table tbody').empty();
    existingLegendArray.forEach((value, index) => {
        if(value.legendValue !== null){
            $('#legends-table tbody').append(`
            <tr>
                <td>
                    <input class="form-control legend" data-nonSubjectID = ${value.nonSubjectID}  data-nsLegendID = ${value.nsLegendID} type="text" name="legendValue" value="${value.legendValue}">
                    <span class="legend-error hidden" >${messages.thisFieldIsRequired}</span>
                </td>
                <td>
                    <label class="switchToggle" style="margin-top: 6px;margin-bottom: 0px;margin-left: 14px;">
                        <input class="isActive" type="checkbox" id="isActive" name="isActive" ${value.isActive == 1 ? 'checked' : ''} >
                        <span class="slider green round" style="width:60px !important"></span>
                    </label>

                    </td>
                <td>
                    <button type="button" class="btn btn-primary btn-sm update-legend-value"><i class="fa fa-pencil"></i> ${messages.update}</button>
                </td>
            </tr>
            `);
        }
    });
    $('#legends-table tbody').append(`
        <tr>
            <td>
                <input class="form-control legend" type="text" data-nonSubjectID = ${nonSubjectID} name="legendValue" value="">
                <span class="legend-error hidden" >${messages.thisFieldIsRequired}</span>
            </td>
            <td>
                <label class="switchToggle" style="margin-top: 6px;margin-bottom: 0px;margin-left: 14px;">
                    <input type="checkbox" class="isActive" id="isActive" name="isActive">
                    <span class="slider green round" style="width:60px !important"></span>
                </label>
            </td>
            <td>
            <button type="button" class="btn btn-primary btn-sm legend-submit" style="width: 97px;"> <i class="fa fa-plus"> </i> ${messages.save}</button>
            </td>
        </tr>
    `);
}
