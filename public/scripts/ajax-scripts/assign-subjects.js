$(document).ready(function(){
    var bulkArray = [];
    getAjaxSchools();
})

var bulkArray = [];

function getAssignedSubjects(){
    let data = {};

    if(subjectID)
        data.subjectID = subjectID;

    if(studentID)
        data.studentID = studentID;

    if(timetableSubjectID)
        data.timetableSubjectID = timetableSubjectID;

    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getStudentSubjects',
            type: 'POST',
            headers,
            beforeSend: function(){
                assignedSubjectArray = []
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                assignedSubjectArray = response;
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        // 'classSection' : `${response[i].classAlias}-${response[i].classSection}`,
                        'teacherName' : `${response[i].fullName}`,
                        'subjectName' : response[i].subjectName,
                        'weekDay' : response[i].WeekdayName,
                        'startTime' : response[i].startTime,
                        'endTime' : response[i].endTime,
                        'room' : response[i].roomName,
                        'action' : `${__ACCESS__ ? `
                        <div class="checkbox checkbox-red">
                        <input id="unlink-student-${i}" data-id="${response[i].studentSubjectID}" class="delete-assigned-subject select-student-${i}" type="checkbox">
                        <label for="unlink-student-${i}">
                        </label>
                    </div>` : ''}`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getAssignedSubjects');
                window.requestForToken(xhr)
                if(xhr.status === 404){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                 }
                // console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            // {'data': 'classSection'},
            {'data': 'teacherName'},
            {'data': 'subjectName'},
            {'data': 'weekDay'},
            {'data': 'startTime'},
            {'data': 'endTime'},
            {'data': 'room'},
            {'data': 'action'}
        ]
    });
}
function hideAndShowAssignSubjectTable(){
    $('#assign-subjects-table tbody').empty();
    $('#assign-subjects-table tbody').append(`
    <tr>
        <td colspan="4"><strong style="color:#188ae2;font-weight:bold"> ${messages.loading}</<strong></td>
    </tr>
    `);
}

function iterateAssignSubjectsStudentTable(){
    var assignSubjectAddTable = $('#assign-subjects-table tbody');
        assignSubjectAddTable.empty();
        if(studentRecordsArray.length){
        $.each(studentRecordsArray, function(index, value){
            let addedResult;
            if(assignedSubjectArray){
                addedResult = assignedSubjectArray.find(o => o.studentID === value.studentID);
            }
            if(addedResult !== undefined && addedResult !== ''){
                assignSubjectAddTable.append(`
                <tr>
                <td style="display:none;">
                    <input class="form-control" name="studentSubjectID" type="text" value='${ addedResult.studentSubjectID}'>
                    <input class="form-control"  name="studentID" type="text" value='${addedResult.studentID}'>
                    <input class="form-control actionFlag select-flag-${index}"  name="actionFlag" type="text" value="1">
                    <input class="form-control isSelected select-student-${index}" name="isSelected" type="text" value='1'>
                </td>
                <td style="color:#188ae2;font-weight:bold; text-align:center">${index + 1}</td>
                <td>${value.studentName }</td>
                <td>${value.rollNo}</td>
                <td>
                        <div class="checkbox checkbox-aqua">
                            <input id="select-student-${index}" data-flag-id="select-flag-${index}" data-id="select-student-${index}" class="studentCheckBox select-student-${index}" type="checkbox" checked>
                            <label for="select-student-${index}">
                            </label>
                        </div>
                </td>
                </tr>`);
            }else{
                assignSubjectAddTable.append(`
                <tr>
                    <td style="display:none;">
                        <input class="form-control"  name="studentSubjectID" type="text" value='0'>
                        <input class="form-control"  name="studentID" type="text" value='${value.studentID}'>
                        <input class="form-control actionFlag select-flag-${index}"  name="actionFlag" type="text" value="0">
                        <input class="form-control isSelected select-student-${index}" name="isSelected" type="text" value='0'>
                    </td>
                    <td style="color:#188ae2;font-weight:bold;text-align:center;">${index + 1}</td>
                    <td>${value.studentName }</td>
                    <td>${value.rollNo}</td>

                    <td>

                            <div class="checkbox checkbox-aqua">
                                <input id="select-student-${index}" data-flag-id="select-flag-${index}" data-id="select-student-${index}" class="studentCheckBox select-student-${index}" type="checkbox">
                                <label for="select-student-${index}">
                                </label>
                            </div>
                    </td>
                </tr>`);
            }
        });
    }
}

$('#activities-student-multiple').on('click', function(){
    if($('.studentCheckBoxAll').prop('checked')){
        $('.studentCheckBoxAll').prop('checked', false);
        $('.isSelected').val(0)
        $('.actionFlag').val(0)
        $('.studentCheckBox').val(0)
        $('.studentCheckBox').prop('checked', false)
    }else{
        $('.studentCheckBoxAll').prop('checked', true);
        $('.isSelected').val(1)
        $('.actionFlag').val(1)
        $('.studentCheckBox').val(1)
        $('.studentCheckBox').prop('checked', true)
    }
})

$(document).on('change','.studentCheckBox',function(){
    var selectedDataAttr = $(this).attr('data-id');
    var selectedFlagDataAttr = $(this).attr('data-flag-id');
    var isChecked = $(this).prop('checked');
    if(isChecked){
        $("." + selectedDataAttr).val(1)
        $("." + selectedFlagDataAttr).val(1)
    }else{
        $("." + selectedDataAttr).val(0)
        $("." + selectedFlagDataAttr).val(0)
    }
})

$('#assign-subjects-form').on('submit', function(e){
    e.preventDefault();
    bulkArray = [];
    timetableSubjectID = $('#filter-subjects-in-time-table').val()
    if(timetableSubjectID === ''){
        swal(
            messages.pleaseSelectTimetableSubjectFirst,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false
    }
    var result = $(this).serializeArray();
    var chunkedArray = chunkArray(result, 4);

    chunkedArray.forEach((val, index) => {
        var myObject = {};
        myObject["subjectID"] = subjectID;
        myObject["timetableSubjectID"] = timetableSubjectID;
        val.forEach((innerVal, innerIndex) => {

            if(innerVal.name == 'studentSubjectID'){
                if( Number(innerVal.value) > 0){
                    myObject[innerVal.name] = innerVal.value;
                }
            }else{
                myObject[innerVal.name] = innerVal.value;
            }
        });
        bulkArray.push(myObject);
    });



    bulkArray.forEach((value, index) => {
        for (var propName in value) {
            if(propName === 'isSelected' ){
                    delete bulkArray[index]['isSelected'];
            }
        }
    });

    bulkArray =  _.filter(bulkArray,v => _.keys(v).length !== 0);

    if(bulkArray.length > 0){
        showProgressBar(submitLoader);
        resetMessages();
        submitAssignedSubject();
    }
});

function submitAssignedSubject(){
    $.ajax({
        url: herokuUrl+'/assignSubjectToStudentArray',
        method: 'POST',
        headers,
        data: {'array': bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#assign-subjects-form').trigger('reset');
                swal(
                    messages.subjectHasBeenAssigned,
                    messages.success,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                hideAddFormContainer();
                getAssignedSubjects();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click', '#delete-assigned-subjects', function(){
    if(selectedForDelete.length === 0){
        swal(
            messages.pleaseSelectForDelete,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    $.confirm({
        title: messages.confirm,
        content: messages.areYouSure+" <br/> "+messages.youWantToRemoveTheSubject+"</br>",
        type: 'red',
        typeAnimated: true,
        buttons: {
            formSubmit: {
                text: messages.confirm,
                btnClass: 'btn-blue',
                action: function () {
                    deleteAssignedSubject();
                }
            },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
        }
    });

})
$(document).on('click', '.delete-assigned-subject', function(){
    var selectedRecord = $(this).attr('data-id');
    if($(this).prop('checked')){
        selectedForDelete.push({
            'studentSubjectID': selectedRecord
        });
    }else{
        var removeIndex = selectedForDelete.map(function(item) { return item.studentSubjectID; }).indexOf(selectedRecord);
        selectedForDelete.splice(removeIndex, 1);
    }
});

function deleteAssignedSubject(studentSubjectID){

    $.ajax({
        url: herokuUrl+'/deleteSubjectToStudentArray',
        method: 'POST',
        headers,
        data: {
            'array': selectedForDelete,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                selectedForDelete = []
                swal(
                    messages.subjectHasBeenRemoved,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getAssignedSubjects();
            }
        },
        error: function(xhr){
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}
