let dat = {};
var ctx = document.getElementById("graph").getContext('2d');
var fromDate = $('#fromDate');
var toDate = $('#toDate');
var graphLabel = 0;
$(document).ready(function(){
    dat.branchID = branchID;
    dat.fromDate = fromDate.val();
    dat.toDate = toDate.val();
    getAjaxSchools();
    getGraph();
});

branches.on('change', function(){
    if(branchID != ''){
        dat.branchID = String(branchID);
        dat.fromDate = fromDate.val();
        dat.toDate = toDate.val();
        getGraph();
    }
});

fromDate.on('change', function(){
    if(branchID != ''){
        dat.fromDate = $(this).val();
        dat.toDate = toDate.val();
        dat.branchID = branchID;
        getGraph();
    }
});

toDate.on('change', function(){
    if(branchID != ''){
        dat.branchID = branchID;
        dat.fromDate = fromDate.val();
        dat.toDate = $(this).val();
        getGraph();
    }
});

function getGraph(){
    $.ajax({
        url: herokuUrl+'/getReportDatewiseRegistrations',
        type: 'POST',
        headers,
        beforeSend: function(){
            $('#graph').addClass('hidden');
            $('#no-data-text').addClass('hidden');
            $('#loading-text').removeClass('hidden');
        },
        data: dat,
        success: function(res){
            response = parseResponse(res);
            if(typeof response.length !== 'undefined' && response.length > 0){
            console.log(response)
            overall = res.overallCount;
            $('#overall-total').text(`${overall.totalCount}`);
            $('#overall-registered').text(`${overall.registeredCount}`);
            $('#overall-unregistered').text(`${overall.unregisteredCount}`);
            $('#overall-active-students').text(`${overall.ActiveStudents}`);
            $('#overall-non-active-students').text(`${overall.NonActiveStudents}`);
            let dates = [];
            let totals = [];
            graphLabel = res.overallCount.registeredCount;
            response.forEach((elem, index) => {
                dates.push(moment(elem.registrationDate).format('MMM D, YY'));
                totals.push(elem.dailyCount);
            });
            var myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                labels: dates,
                datasets: [
                        {
                            label: messages.totalInstalls+' '+graphLabel,
                            fill: false,
                            borderWidth: 5,
                            borderColor: '#66a0ff',
                            data: totals
                        }
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: messages.appInstalls
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                autoSkip: true,
                                maxRotation: 0
                            },
                        }]
                    }
                }
            });

            $('#graph').removeClass('hidden');
            $('#loading-text').addClass('hidden');
            $('#no-data-text').addClass('hidden');
        }else{
                $('#no-data-text').removeClass('hidden');
                $('#loading-text').addClass('hidden');
            }
        },
        error: function(err){
            invokedFunctionArray.push('getGraph');
            window.requestForToken(err)
            console.log(err);
        }
    })
}
