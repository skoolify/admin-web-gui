$(document).ready(function(){
    getAjaxSchools();
    // getClassLevels();
})

function submitClassLevel(){
    $.ajax({
        url: herokuUrl+'/setClassLevels',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                // tableData.destroy();
                getClassLevels();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitClassLevel');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getClassLevels(){
    let data = {};

    if(schoolID !== ''){
        data.schoolID = schoolID
    }

    if(branchID !== ''){
        data.branchID = branchID
    }

    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl+'/getClassLevels',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'classLevelName': response[i].classLevelDesc,
                        // 'classAlias': response[i].classAlias,
                        'assessmentOnly' : `<label class="label label-sm label-${response[i].assessmentOnly ? 'success' : 'danger'}"> ${response[i].assessmentOnly ? messages.yes : messages.no}</label>`,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `<div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-classLevel" data-classLevel-id='${response[i].classLevelID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}
                        </div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getClassLevels');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'classLevelName'},
            // {'data': 'classAlias'},
            {'data': 'assessmentOnly'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

function deleteClassLevel(){
    $.ajax({
        url: herokuUrl+'/deleteClassLevel',
        method: 'POST',
        headers,
        data: {
            classLevelID: classLevelID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.classHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                classLevelID = '';
                getClassLevels();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteClassLevel');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('click', '.delete-classLevel', function(){
    classLevelID = $(this).attr('data-classLevel-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheClass+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteClassLevel();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

$('#class-levels-form').on('submit', function(e){
    e.preventDefault();
    if($('#isSubjectLevelAttendance1').prop('checked')){
        $('#isSubjectLevelAttendanceHidden').val(1);
    }else{
        $('#isSubjectLevelAttendanceHidden').val(0);
    }

    if($('#isActive1').prop('checked')){
        $('#isActiveHidden').val(1);
    }else{
        $('#isActiveHidden').val(0);
    }
    data = $('#filter-form').serialize()+'&'+$('#class-levels-form').serialize();

    if($('#filter-schools').val() !== ''){
        data['schoolID'] = $('#filter-schools').val()
    }

    if($('#filter-branches').val() !== ''){
        data['branchID'] = $('#filter-branches').val()
    }

    if($('#classLevelName').val() !== ''){
        data['classLevelName'] = $('#classLevelName').val()
    }else{
        swal(
            messages.classNameIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#classAlias').val() !== ''){
        data['classAlias'] = $('#classAlias').val()
    }else{
        swal(
            messages.alias,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#assessmentOnly').val() !== ''){
        data['assessmentOnly'] = $('#assessmentOnly').val()
    }else{
        swal(
            messages.assessmentOnly,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    showProgressBar(submitLoader);
    resetMessages();
    submitClassLevel();
});


// $('#isSubjectLevelAttendance').on('change', function(){
//     if($('#isSubjectLevelAttendance').prop('checked')){
//         $('#isSubjectLevelAttendance').val(1);
//     }else{
//         $('#isSubjectLevelAttendance').val(0);
//     }
// })

// $('#isActive').on('change', function(){
//     if($('#isActive').prop('checked')){
//         $('#isActive').val(1);
//     }else{
//         $('#isActive').val(0);
//     }
// })
