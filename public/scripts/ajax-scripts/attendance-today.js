var user = JSON.parse(localStorage.getItem('user'));
$(document).ready(function(){
    getAjaxSchools();
    // getClassStudents();
})

function submitAttendance(){
    $.ajax({
        url: herokuUrl+'/setAttendance',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitAttendance');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getClassStudents(){
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getClassStudents',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data: {
                classID: classID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'present' : `<div class="flex-container" style="justify-content: center">
                                        <div class="round">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="P" class="mark-attendance attendance-check-${i}" data-json='${JSON.stringify(response[i])}' id="checkbox-${i}" />
                                            <label for="checkbox-${i}"><b class="check-text">P</b></label>
                                        </div>

                                        <div class="round round-d">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="A" class="mark-attendance attendance-check-${i}" data-json='${JSON.stringify(response[i])}' id="checkbox-d-${i}" />
                                            <label for="checkbox-d-${i}"><b class="check-text">A</b></label>
                                        </div>

                                        <div class="round round-l">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="L" class="mark-attendance attendance-check-${i}" data-json='${JSON.stringify(response[i])}' id="checkbox-l-${i}" />
                                            <label for="checkbox-l-${i}"><b class="check-text">L</b></label>
                                        </div>
                                    </div>`,
                        'studentName': response[i].studentName,
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getClassStudents');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'present'},
            {'data': 'studentName'}
        ]
    });

}

$(document).on('click', '.mark-attendance', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    let target = $(this).attr('data-target');
    let obj = JSON.parse($(this).attr('data-json'));
    let present = target == 'P' ? '1' : (target == 'L') ? '2' : '0';
    data = $('#filter-form').serialize()+'&'+'&userID='+user.userID+'&studentID='+obj.studentID+'&isPresent='+present;

    if(checked){
        $('.'+index).prop('checked', false);
        $(this).prop('checked', true);
        submitAttendance();
    }
})

$('#attendance-form').on('submit', function(e){
    e.preventDefault();
    submitAttendance();
});
