var index = '0';
var role = null;
$(document).ready(function(){
    let swt = localStorage.getItem('switch');
    
    if(swt !== null){
        window.location.replace(__BASE__+'auth/login');
    }else{
        var roles = JSON.parse(localStorage.getItem('user'));
        var multiRoleSelect = $('#multiRoleSelect');
        var proceedBtn = $('#proceed-btn');

        multiRoleSelect.empty();
        multiRoleSelect.append(`<option value="">${messages.selectRole}</option>`)
        if(roles.length > 1){
            $('#multi-select-container').css('display', 'block');
            $('#modules-loader').css('display', 'none');
            roles.forEach((element, index) => {
                multiRoleSelect.append(`<option value="${index}">${element.roleName} (${element.branchName})</option>`)
            });
        }else{
            role = roles[0];
            getModules();
        }

        proceedBtn.on('click', function(){
            index = multiRoleSelect.val().toString();
            role = roles[index];
            if(role){
                $('#multi-select-container').css('display', 'none');
                $('#modules-loader').css('display', 'block');
                getModules();
            }
        });

        //setTimeout(function(){
            //window.location.replace(__BASE__+'modules');
            //$('#loader-logo, #loader-text').removeAttr('id').addClass('fadeOutUpper');
        //}, 3000)
    }
})

$(window).bind('beforeunload',function(){
    localStorage.setItem('switch', '1');
});

function getModules(){
    $.ajax({
        url: __BASE__+'auth/userModules',
        method: 'GET',
        data: {
            index
        },
        beforeSend: function(){
        },
        success: function(res){
            if(!res.error){
                localStorage.setItem('user', JSON.stringify(role));
                let lastLink = localStorage.getItem('last-link');
                var requiredModules = res.requiredModules;
                var reqModules = [];

                Object.keys(requiredModules).forEach((r, index) => {
                    reqModules.push(requiredModules[r]);
                });
                // new work for payment check start
                user = JSON.parse(localStorage.getItem('user'))
                if(user.isBlocked === 1 && user.feePaymentAllowed === 1){
                    window.location.replace(__BASE__+'school-payment');
                    return
                }
                // new work for payment check end
                if(lastLink){
                    if(reqModules.indexOf(lastLink) >= 0){
                        window.location.replace(__BASE__+lastLink);
                    }else{
                        window.location.replace(__BASE__+res.module);
                    }
                }else{
                    window.location.replace(__BASE__+res.module);
                }
                $('#loader-logo, #loader-text').removeAttr('id').addClass('fadeOutUpper');
            }else if(res.error && res.code == 404){
                $('#loader-logo').hide();
                $('#loader-text').html(`${res.message}`);
                $('#logout-btn').css('display', 'block');
            }else{
                window.location.replace(__BASE__+'403');
            }
        },
        error: function(err){
            invokedFunctionArray.push('getModules');
            return false;
        }
    })
}