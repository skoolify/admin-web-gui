$(document).ready(function(){
    if(user.isAdmin != 1){
        getAjaxClassLevels()
    }
    let filterSchools = $('#filter-schools');
    if(filterSchools.length){
        getAjaxSchools();
        if(user.isAdmin == 1){
            schoolID = $('#filter-schools').val();
        }
        if(schoolID !== ''){
            getAcademicEvents();
        }
    }else{
        // if(schoolID !== ''){
        //     getAcademicEvents();
        // }
    }
})

var bulkArray = [];

function submitAcademicEvent(){
    $.ajax({
        url: herokuUrl+'/setAcademicEventsArray',
        method: 'POST',
        headers,
        data:{'array':bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#academic-calendar-form').trigger('reset');
                getAcademicEvents();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitAcademicEvent')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if(xhr.status === 406){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.reason}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

// $(document).on('change','#filter-branches', function(){
//     getAcademicEvents();
// })
$(document).on('change','#filter-date-from', function(){
    getAcademicEvents();
})
$(document).on('change','#filter-date-to', function(){
    getAcademicEvents();
})

function getAcademicEvents(){
    data = {}
    classLevelID = $('#filter-classLevels').val();
    classID = $('#filter-classes').val();
    dateFromVal = $('#filter-date-from').val();
    dateToVal = $('#filter-date-to').val();
    branchID = $('#filter-branches').val();
    if(schoolID !== ''){
        data.schoolID = schoolID
    }
    if(branchID !== '' && branchID !== undefined){
        data.branchID = branchID
    }else{
        data.branchID = user.branchID
    }
    if(Array.isArray(classLevelID)){
        if(classLevelID.length >= 1 && classLevelID != '0'){
            data.classLevelID = classLevelID.join(',');
        }
    }else{
        if(Number(classLevelID) !== 0)
            data.classLevelID =  classLevelID;
    }
    if(Array.isArray(classID)){
        if(classID.length >= 1 && classID != '0'){
            data.classID = classID.join(',');
        }
    }else{
        if(Number(classID) !== 0)
            data.classID =  classID;
    }
    if(dateFromVal !== ''){
        data.dateFrom = dateFromVal
    }
    if(dateToVal !== ''){
        data.dateTo = dateToVal
    }

    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getAcademicEvents',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'eventName': response[i].eventName,
                        'eventDate' : moment(response[i].eventDate).format('DD-MM-YYYY'),
                        'classSection': response[i].classAlias+'-'+response[i].classSection,
                        'action' :  `
                        <div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-event" data-event-id='${response[i].eventID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''} </div>
                        `
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getAcademicEvents');
                window.requestForToken(xhr)
                if(xhr.status === 400){
                    $('.dataTables_empty').text(messages.noDataAvailable);
                }
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'eventName'},
            {'data': 'eventDate'},
            {'data': 'classSection'},
            {'data': 'action'}
        ]
    });

}

$(document).on('click', '.delete-event', function(){
    eventID = $(this).attr('data-event-id');
   $.confirm({
       title: messages.confirm,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheEvent+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteEvent();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteEvent(){
    $.ajax({
        url: herokuUrl+'/deleteAcademicEvents',
        method: 'POST',
        headers,
        data: {
            eventID: eventID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.eventHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                getAcademicEvents();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteEvent');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

function createObjectFromAnotherObject(oldObject){
    var newObject = {};
    for (var propName in oldObject) {
        newObject[propName] = oldObject[propName];
    }
    return newObject;
}

$('#academic-calendar-form').on('submit', function(e){
    e.preventDefault();
    bulkArray = [];
    var eventID = $('#editHiddenField').val();
    var eventDate = $('#event-date').val();
    classLevelID = $('#filter-classLevels').val();
    classID = $('#filter-classes').val();
    var formData = $(this).serializeArray();
    if(classLevelID.length === 0){
        swal(
            messages.pleaseSelectClass,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if(classID.length === 0){
        swal(
            messages.pleaseSelectSection,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    if(classID !== ''){
        if(Array.isArray(classID)){
            classID = classID.filter(function (el) {
                return el != null && el !== '';
            });
            classID.forEach(function(value, index){
                if(messagesSelectedClassesArray.indexOf(String(value)) == -1){
                    messagesSelectedClassesArray.push(value)
                }
            })
            classID =  classID.join(',');
        }
    }
    if(messagesClassLevelArray.length > 0){
        messagesClassLevelArray.forEach(function(classLevelsData, classLevelsIndex){
            var obj = {};
            obj.userID = user.userID;
            obj.classLevelID = Number(classLevelsData);
            formData.forEach(function(value, index){
                obj[value.name] = value.value;
            });
            if(typeof messagesClassArray[classLevelsData] != 'undefined' && messagesClassArray[classLevelsData].length > 0 &&  Number(messagesSelectedClassesArray[0]) !== 0){
                messagesClassArray[classLevelsData].forEach((cls, index) => {
                    if(messagesSelectedClassesArray.indexOf(String(cls.classID)) > -1){
                        var classObject = {};
                        classObject = createObjectFromAnotherObject(obj);
                        classObject.classID = Number(cls.classID);
                        bulkArray.push(clean(classObject));
                    }
                })
            }else{
                bulkArray.push(clean(obj));
            }
        })
    }else{
        var obj = {};
        obj.userID = user.userID;
        obj.classLevelID = Number(classLevelID);
        obj.classID = Number(classID);
        formData.forEach(function(value, index){
            obj[value.name] = value.value;
        });
        bulkArray.push(clean(obj));
    }
    if(bulkArray.length){
        showProgressBar(submitLoader);
        resetMessages();
        submitAcademicEvent();
    }
});
