var roleAccessRightsBulk = new Array();
$(document).ready(function(){
    getAjaxSchools();
    getAjaxModules();
    //getRoleAccessRights();
})

function submitRoleAccessRights(){
    $.ajax({
        url: herokuUrl+'/setRoleAccessRights',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                $('form#role-access-rights-form').trigger('reset');
                $('form#role-access-rights-form input').removeAttr('checked');
                moduleID = '';
                getRoleAccessRights();
                getPreAssignedRights();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitRoleAccessRights')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function submitRoleAccessRightsBulk(){
    let temp = new Array();
    for (let index = 0; index < roleAccessRightsBulk.length; index++) {
        const element = roleAccessRightsBulk[index];
        if(typeof element !== 'undefined'){
            let json = { ...element };
            temp.push(json);
        }
    }
    $.ajax({
        url: herokuUrl+'/setRoleAccessRightsArray',
        method: 'POST',
        headers,
        data: {
            array: temp
        },
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
            $('#submit-access-rights').attr('disabled', true);
            $('#cancel-access-rights').attr('disabled', true);
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.success,
                    messages.successfullySubmitted,
                    'success'
                );
                $('#submit-access-rights').removeAttr('disabled', true);
                $('#cancel-access-rights').removeAttr('disabled', true);
                $('form#role-access-rights-form').trigger('reset');
                $('form#bulk-access-rights-form').trigger('reset');
                if(tableData){
                    tableData.destroy();
                }
                getRoleAccessRights();
                getPreAssignedRights();
                $('#modules-list').addClass('hidden').slideUp();
                $('#results-list').removeClass('hidden').slideDown();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitRoleAccessRightsBulk')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                swal(
                    messages.error,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                );
            }
            $('#submit-access-rights').removeAttr('disabled', true);
            $('#cancel-access-rights').removeAttr('disabled', true);
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getRoleAccessRights(){
    let data = {};
    if(roleID !== ''){
        data.roleID = roleID;
    }
    if(schoolID !== ''){
        data.schoolID = schoolID;
    }
    if(moduleID !== ''){
        data.moduleID = moduleID;
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getRoleAccessRights',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let modulee = $('#filter-modules option[value='+response[i].moduleID+']').text();
                    return_data.push({
                        'sNo' : i+1,
                        'moduleName' : modulee,
                        'readAccess': `<label class="label label-sm label-${response[i].readAccess ? 'primary' : 'danger'}"> ${response[i].readAccess ? messages.yes : messages.no}</label>`,
                        'addAccess' : `<label class="label label-sm label-${response[i].addAccess ? 'primary' : 'danger'}"> ${response[i].addAccess ? messages.yes : messages.no}</label>`,
                        'editAccess' : `<label class="label label-sm label-${response[i].editAccess ? 'primary' : 'danger'}"> ${response[i].editAccess ? messages.yes : messages.no}</label>`,
                        'deleteAccess' : `<label class="label label-sm label-${response[i].deleteAccess ? 'primary' : 'danger'}"> ${response[i].deleteAccess ? messages.yes : messages.no}</label>`,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `<button class="btn-primary btn btn-xs list-record-edit-custom" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getRoleAccessRights')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'moduleName'},
            {'data': 'readAccess'},
            {'data': 'addAccess'},
            {'data': 'editAccess'},
            {'data': 'deleteAccess'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

function iterateModulesArray(){
    $('#modules-list tbody').empty();
    let counter = 1;
    Object.keys(__MODULES__).forEach(function (key, i) {
        if(__MODULES__[key] !== undefined){
            let findedObj;
            if(submittedRights){
                findedObj = submittedRights.find(o => o.moduleID === __MODULES__[key].moduleID);
            }
            let subChildrens;
            let childrens = __MODULES__[key].childrens;
            if(findedObj !== undefined && findedObj !== ''){
                $('#modules-list tbody').append(`<tr id="module-${__MODULES__[key].moduleURL}" class="blue-row">
                <td>${__MODULES__[key].moduleURL}</td>
                <td>${__MODULES__[key].moduleName}</td>
                ${`<td class="text-center">
                        <div class="checkbox checkbox-icon-blue p-0">
                            <input id="readAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right readAccessCheckbox" name="readAccess" value="${__MODULES__[key].moduleID}" ${findedObj.readAccess === 1 ? 'checked' : ''}>
                            <label for="readAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-green p-0">
                            <input id="addAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right addAccessCheckbox" name="addAccess" value="${__MODULES__[key].moduleID}" ${findedObj.addAccess === 1 ? 'checked' : ''}>
                            <label for="addAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-yellow p-0">
                            <input id="editAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right editAccessCheckbox" name="editAccess" value="${__MODULES__[key].moduleID}" ${findedObj.editAccess === 1 ? 'checked' : ''}>
                            <label for="editAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-red p-0">
                            <input id="deleteAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right deleteAccessCheckbox" name="deleteAccess" value="${__MODULES__[key].moduleID}" ${findedObj.deleteAccess === 1 ? 'checked' : ''}>
                            <label for="deleteAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>`
                }
            </tr>`)
            }else{
                $('#modules-list tbody').append(`<tr id="module-${__MODULES__[key].moduleURL}" class="blue-row">
                <td>${__MODULES__[key].moduleURL}</td>
                <td>${__MODULES__[key].moduleName}</td>
                ${`<td class="text-center">
                        <div class="checkbox checkbox-icon-blue p-0">
                            <input id="readAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right readAccessCheckbox" name="readAccess" value="${__MODULES__[key].moduleID}">
                            <label for="readAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-green p-0">
                            <input id="addAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right addAccessCheckbox" name="addAccess" value="${__MODULES__[key].moduleID}">
                            <label for="addAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-yellow p-0">
                            <input id="editAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right editAccessCheckbox" name="editAccess" value="${__MODULES__[key].moduleID}">
                            <label for="editAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-red p-0">
                            <input id="deleteAccess${__MODULES__[key].moduleID}" type="checkbox" data-index="${__MODULES__[key].moduleID}" class="assign-right deleteAccessCheckbox" name="deleteAccess" value="${__MODULES__[key].moduleID}">
                            <label for="deleteAccess${__MODULES__[key].moduleID}"></label>
                        </div>
                    </td>`
                }
            </tr>`)
            }

            childrens.forEach((child, i) => {
                let childFindedObj;
                subChildrens = undefined;
                if(key === 'management' && child.hasOwnProperty('subChildrens')){
                    subChildrens =  __MODULES__[key].childrens[i].subChildrens;
                }
                if(submittedRights){
                    childFindedObj = submittedRights.find(o => o.moduleID === child.moduleID);
                }
                if(childFindedObj !== undefined && childFindedObj !== ''){
                    $('#modules-list tbody').append(`<tr id="module-${child.moduleURL}" class="${subChildrens !== undefined ? 'green-row' : '' }">
                    <td>${subChildrens !== undefined ? child.moduleName : counter }</td>
                    <td>${child.moduleName}</td>
                    ${`<td class="text-center">
                            <div class="checkbox checkbox-icon-blue p-0">
                                <input id="readAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right readAccessCheckbox" name="readAccess" value="${child.moduleID}" ${childFindedObj.readAccess === 1 ? 'checked' : ''}>
                                <label for="readAccess${child.moduleID}"></label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-icon-green p-0">
                                <input id="addAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right addAccessCheckbox" name="addAccess" value="${child.moduleID}" ${childFindedObj.addAccess === 1 ? 'checked' : ''}>
                                <label for="addAccess${child.moduleID}"></label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-icon-yellow p-0">
                                <input id="editAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right editAccessCheckbox" name="editAccess" value="${child.moduleID}" ${childFindedObj.editAccess === 1 ? 'checked' : ''}>
                                <label for="editAccess${child.moduleID}"></label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-icon-red p-0">
                                <input id="deleteAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right deleteAccessCheckbox" name="deleteAccess" value="${child.moduleID}" ${childFindedObj.deleteAccess === 1 ? 'checked' : ''}>
                                <label for="deleteAccess${child.moduleID}"></label>
                            </div>
                        </td>`
                    }
                </tr>`)

                }else{

                $('#modules-list tbody').append(`<tr id="module-${child.moduleURL}">
                    <td>${counter}</td>
                    <td>${child.moduleName}</td>
                    ${`<td class="text-center">
                            <div class="checkbox checkbox-icon-blue p-0">
                                <input id="readAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right readAccessCheckbox" name="readAccess" value="${child.moduleID}">
                                <label for="readAccess${child.moduleID}"></label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-icon-green p-0">
                                <input id="addAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right addAccessCheckbox" name="addAccess" value="${child.moduleID}">
                                <label for="addAccess${child.moduleID}"></label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-icon-yellow p-0">
                                <input id="editAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right editAccessCheckbox" name="editAccess" value="${child.moduleID}">
                                <label for="editAccess${child.moduleID}"></label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox checkbox-icon-red p-0">
                                <input id="deleteAccess${child.moduleID}" type="checkbox" data-index="${child.moduleID}" class="assign-right deleteAccessCheckbox" name="deleteAccess" value="${child.moduleID}">
                                <label for="deleteAccess${child.moduleID}"></label>
                            </div>
                        </td>`
                    }
                </tr>`)
                }
                if(subChildrens !== undefined && subChildrens !== ''){
                    subChildrens.forEach((subChild, i) => {
                        let subChildFindedObj;
                        if(submittedRights){
                            subChildFindedObj = submittedRights.find(o => o.moduleID === subChild.moduleID);
                        }
                        if(subChildFindedObj !== undefined && subChildFindedObj !== ''){
                            $('#modules-list tbody').append(`<tr id="module-${subChild.moduleURL}">
                                <td>${counter}</td>
                                <td>${subChild.moduleName}</td>
                                ${`<td class="text-center">
                                        <div class="checkbox checkbox-icon-blue p-0">
                                            <input id="readAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right readAccessCheckbox" name="readAccess" value="${subChild.moduleID}" ${subChildFindedObj.readAccess === 1 ? 'checked' : ''}>
                                            <label for="readAccess${subChild.moduleID}"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="checkbox checkbox-icon-green p-0">
                                            <input id="addAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right addAccessCheckbox" name="addAccess" value="${subChild.moduleID}" ${subChildFindedObj.addAccess === 1 ? 'checked' : ''}>
                                            <label for="addAccess${subChild.moduleID}"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="checkbox checkbox-icon-yellow p-0">
                                            <input id="editAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right editAccessCheckbox" name="editAccess" value="${subChild.moduleID}" ${subChildFindedObj.editAccess === 1 ? 'checked' : ''}>
                                            <label for="editAccess${subChild.moduleID}"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="checkbox checkbox-icon-red p-0">
                                            <input id="deleteAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right deleteAccessCheckbox" name="deleteAccess" value="${subChild.moduleID}" ${subChildFindedObj.deleteAccess === 1 ? 'checked' : ''}>
                                            <label for="deleteAccess${subChild.moduleID}"></label>
                                        </div>
                                    </td>`
                            }
                        </tr>`)
                        }else{
                            $('#modules-list tbody').append(`<tr id="module-${subChild.moduleURL}">
                            <td>${counter}</td>
                            <td>${subChild.moduleName}</td>
                            ${`<td class="text-center">
                                    <div class="checkbox checkbox-icon-blue p-0">
                                        <input id="readAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right readAccessCheckbox" name="readAccess" value="${subChild.moduleID}" >
                                        <label for="readAccess${subChild.moduleID}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-icon-green p-0">
                                        <input id="addAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right addAccessCheckbox" name="addAccess" value="${subChild.moduleID}" >
                                        <label for="addAccess${subChild.moduleID}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-icon-yellow p-0">
                                        <input id="editAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right editAccessCheckbox" name="editAccess" value="${subChild.moduleID}" >
                                        <label for="editAccess${subChild.moduleID}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="checkbox checkbox-icon-red p-0">
                                        <input id="deleteAccess${subChild.moduleID}" type="checkbox" data-index="${subChild.moduleID}" class="assign-right deleteAccessCheckbox" name="deleteAccess" value="${subChild.moduleID}" >
                                        <label for="deleteAccess${subChild.moduleID}"></label>
                                    </div>
                                </td>`
                        }
                    </tr>`)
                        }
                        counter = counter + 1;
                    });
                }
                counter = counter + 1;
            });

        }
    });
}

function iterateModules(){
    let counter = 0;
    $('#filter-modules option').each(function(i){
        if(this.value !== ''){
            counter = counter + 1;
            $('#modules-list tbody').append(`<tr class="${$(this).attr('data-parent') == 1 ? 'blue-row' : ''}">
                <td>${counter}</td>
                <td>${this.text}</td>
                ${ $(this).attr('data-parent') ?
                    `<td class="text-center">
                        <div class="checkbox checkbox-icon-blue p-0">
                            <input id="readAccess${i}" type="checkbox" data-index="${i}" class="assign-right readAccessCheckbox" name="readAccess" value="${this.value}">
                            <label for="readAccess${i}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-green p-0">
                            <input id="addAccess${i}" type="checkbox" data-index="${i}" class="assign-right addAccessCheckbox" name="addAccess" value="${this.value}">
                            <label for="addAccess${i}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-yellow p-0">
                            <input id="editAccess${i}" type="checkbox" data-index="${i}" class="assign-right editAccessCheckbox" name="editAccess" value="${this.value}">
                            <label for="editAccess${i}"></label>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="checkbox checkbox-icon-red p-0">
                            <input id="deleteAccess${i}" type="checkbox" data-index="${i}" class="assign-right deleteAccessCheckbox" name="deleteAccess" value="${this.value}">
                            <label for="deleteAccess${i}"></label>
                        </div>
                    </td>` : '<td colspan="4"></td>'
                }
            </tr>`)
        }
    });
}

let access = new Array('addAccess', 'editAccess', 'deleteAccess', 'readAccess');

$(document).on('click', '.assign-right', function(){

    let index = $(this).attr('data-index');

    if(typeof roleAccessRightsBulk[index] === 'undefined'){
        roleAccessRightsBulk[index] = new Array();
        roleAccessRightsBulk[index]['addAccess'] = '0';
        roleAccessRightsBulk[index]['editAccess'] = '0';
        roleAccessRightsBulk[index]['deleteAccess'] = '0';
        roleAccessRightsBulk[index]['readAccess'] = '0';
    }

    let name = $(this).attr('name');
    let value = '0';
    if($('#filter-roles').val() !== ''){
        if($(this).is(":checked")){
            value = '1';
        }

        roleAccessRightsBulk[index]['moduleID'] = $(this).val();
        roleAccessRightsBulk[index][name] = value;
        roleAccessRightsBulk[index]['roleID'] = $('#filter-roles').val();

    }else{
        swal(
            messages.error,
            messages.pleaseSelectRole,
            'error'
        );
        return false;
    }
});

$('#submit-access-rights').on('click', function(){
    if($('#filter-roles').val() !== ''){
        submitRoleAccessRightsBulk();
    }else{
        swal(
            messages.error,
            messages.pleaseSelectRole,
            'error'
        )
    }
});

$('#role-access-rights-form').on('submit', function(e){
    e.preventDefault();
    let moduleID = $('#filter-modules').val();
    let roleID = $('#filter-roles').val();
    let formDefault = [
        {
            'name': messages.readAccess,
            'value': '0'
        },
        {
            'name': messages.addAccess,
            'value': '0'
        },
        {
            'name': messages.editAccess,
            'value': '0'
        },
        {
            'name': messages.deleteAccess,
            'value': '0'
        }
    ]
    let getRoleAccessRightsForm = $('#role-access-rights-form').serializeArray();
    if(moduleID == '' || roleID  == ''){
        swal(
            messages.error,
            messages.moduleRoleAreRequired,
            'error'
        )
        return false;
    }
    data = 'roleID='+roleID+'&moduleID='+moduleID;
    let editHiddenField = $('#editHiddenField');
    if(editHiddenField.val() !== '' && editHiddenField.attr('name')){
        getRoleAccessRightsForm.splice(0, 1);
    }
     getRoleAccessRightsForm.forEach((res, index) => {
        if(typeof formDefault[index] !== 'undefined'){
            formDefault.forEach((result, InnerIndex) => {
                if(res.name == result.name  && res.value == 1){
                    result.value = '1'
                }
            });
        }
    });

    formDefault.forEach((res, index) => {
        data += '&'+res.name+'='+res.value;
    });
    if(editHiddenField.val() !== '' && editHiddenField.attr('name')){
        data += '&accessID='+editHiddenField.val();
    }
    showProgressBar(submitLoader);
    resetMessages();
    submitRoleAccessRights();
});

$('#add-access-rights').on('click', function(){

    if(user.isAdmin == 0){
        schoolID = user.schoolID;

    }else{
        schoolID = $('#filter-schools').val();
    }
    roleID = $('#filter-roles').val();
    if( schoolID === '' || schoolID.length == 0 ){
        swal(
            messages.error,
            'Please Select School',
            'error'
        );
        }else if(roleID === '' || roleID.length == 0 ){

        swal(
            messages.error,
            messages.pleaseSelectRole,
            'error'
        );
    }else{
        getPreAssignedRights();
        $('#results-list').addClass('hidden').slideUp();
        $('#modules-list').removeClass('hidden').slideDown();
        $('#modules-list tbody').empty();
        $('#modules-list tbody').append(`<tr><td colspan="6" class="text-center text-success" style="font-weight:bold"> ${messages.loading}</td></tr>`);
    }


});
function getPreAssignedRights(){
    $.ajax({
        url: herokuUrl+'/getRoleAccessRights',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data:{
            schoolID: schoolID,
            roleID: roleID
        },
        success: function(res){
            submittedRights = [];
            roleAccessRightsBulk = [];
            response = parseResponse(res);
            response.forEach(function(val, ind){
                submittedRights.push(val);
                delete val['schoolID']
                delete val['isActive']
                roleAccessRightsBulk[val.moduleID] = val
            });
            setTimeout(function(){ iterateModulesArray(); }, 1500);
        },
        error: function(xlr){
            invokedFunctionArray.push('getPreAssignedRights')
            window.requestForToken(xlr)
            console.log(xlr);
        }
    });
}

$('#cancel-access-rights').on('click', function(){
    $('#modules-list').addClass('hidden').slideUp();
    $('#results-list').removeClass('hidden').slideDown();
});

$(document).on('click', '.list-record-edit-custom', function(){
    var json = JSON.parse($(this).attr('data-json'));
    var keys = Object.keys(json);
    if(keys.length >= 1){
        editHiddenField.attr('name', keys[0]);
        let checkboxKeys = ['addAccess', 'readAccess', 'editAccess', 'deleteAccess'];
        keys.forEach((value, index) => {
            if(checkboxKeys.indexOf(value) > -1){
                if(json[value] == 1){
                    $('[name='+value+']').attr('checked', true);
                }else{
                    $('[name='+value+']').removeAttr('checked');
                }
            }else{
                $('[name='+value+']').val(json[value]);
            }
            if(value.includes('ID')){
                if(typeof value !== 'undefined'){
                    window[value] = json[value];
                }
            }
        });
    }
    resetMessages();
    hideResultsList();
    showAddFormContainer();

});

// $(document).on('click', '.bulk-access', function(){
//     let key = $(this).attr('data-key');
//     if($(this).prop('checked')){
//         $('.'+key+'Checkbox').prop('checked', true)
//     }else{
//         $('.'+key+'Checkbox').prop('checked', false)
//     }

//     $('.'+key+'Checkbox').each(function(){
//         let index = $(this).attr('data-index');
//         let name = $(this).attr('name');
//         console.log(index,'index')
//         console.log(roleAccessRightsBulk[index],'roleAccessRightsBulk[index]')
//         if(typeof roleAccessRightsBulk[index] === 'undefined'){
//             roleAccessRightsBulk[index][name] = 0;
//         }

//         roleAccessRightsBulk[index]['moduleID'] = $(this).val();
//         roleAccessRightsBulk[index][name] = 0;
//         roleAccessRightsBulk[index]['roleID'] = $('#filter-roles').val();
//     });
// })

$(document).on('click', '.bulk-access', function(){
    let key = $(this).attr('data-key');
    // console.log(roleAccessRightsBulk.length)
    // console.log($('.'+key+'Checkbox:checked').length)
    // console.log(roleAccessRightsBulk)
    // let index = $(this).attr('data-index');
    // let name = $(this).attr('name');
    // console.log(index,'index')
    // console.log(roleAccessRightsBulk[index],'roleAccessRightsBulk[index]')
    if ($('.'+key+'Checkbox:checked').length > 0){
        // console.log('if')
        if($('#filter-roles').val() !== ''){
            $('.'+key+'Checkbox').each(function(){
                let index = $(this).attr('data-index');
                if(typeof roleAccessRightsBulk[index] === 'undefined'){
                    roleAccessRightsBulk[index] = new Array();
                    roleAccessRightsBulk[index]['addAccess'] = '0';
                    roleAccessRightsBulk[index]['editAccess'] = '0';
                    roleAccessRightsBulk[index]['deleteAccess'] = '0';
                    roleAccessRightsBulk[index]['readAccess'] = '0';
                }

                let name = $(this).attr('name');

                roleAccessRightsBulk[index]['moduleID'] = $(this).val();
                roleAccessRightsBulk[index][name] = '0';
                roleAccessRightsBulk[index]['roleID'] = $('#filter-roles').val();
            });

        }else{
            swal(
                messages.error,
                messages.pleaseSelectRole,
                'error'
            );
            return false;
        }
        $('.'+key+'Checkbox').prop('checked', false);
    }else{
        // console.log('else')
        if($('#filter-roles').val() !== ''){
            $('.'+key+'Checkbox').each(function(){
                let index = $(this).attr('data-index');
                if(typeof roleAccessRightsBulk[index] === 'undefined'){
                    roleAccessRightsBulk[index] = new Array();
                    roleAccessRightsBulk[index]['addAccess'] = '0';
                    roleAccessRightsBulk[index]['editAccess'] = '0';
                    roleAccessRightsBulk[index]['deleteAccess'] = '0';
                    roleAccessRightsBulk[index]['readAccess'] = '0';
                }

                let name = $(this).attr('name');

                roleAccessRightsBulk[index]['moduleID'] = $(this).val();
                roleAccessRightsBulk[index][name] = '1';
                roleAccessRightsBulk[index]['roleID'] = $('#filter-roles').val();
            });

        }else{
            swal(
                messages.error,
                messages.pleaseSelectRole,
                'error'
            );
            return false;
        }

        $('.'+key+'Checkbox').prop('checked', true);
    }
});
