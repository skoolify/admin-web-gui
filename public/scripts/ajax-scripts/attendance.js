var user = JSON.parse(localStorage.getItem('user'));
var studentsArray = [];
var offDayAttendanceIds = [];
var studentsArrayInTimeTable = [];
var isMarkedHoliday = false;
var isAttendanceAvailable = false;
var attendanceDataTable = null;
var data = {};
var schoolID = 0;
var dateFromVal = '';
var dateToVal = '';
var issubjectlevelattendance = 0;
var bulkArray = [];

$(document).ready(function(){
    getAjaxSchools();
    $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd',  maxDate: '0'});
    // getAttendance();
})

$(document).on('change','#filter-classLevels',function(){
    issubjectlevelattendance = $('#filter-classLevels').find(':selected').attr('data-issubjectlevelattendance');
    if(Number(issubjectlevelattendance > 0 )){
        $('#subject-in-time-table').removeClass('hidden');
        $('#batch-no').removeClass('hidden');
        $('#subject-attendance-table-div').removeClass('hidden');
        $('#subject-dropdown-div').removeClass('hidden');
        $('#attendance-table-div').addClass('hidden');
    }else{
        $('#subject-in-time-table').addClass('hidden');
        $('#batch-no').addClass('hidden');
        $('#subject-dropdown-div').addClass('hidden');
        $('#attendance-table-div').removeClass('hidden');
        $('#subject-attendance-table-div').addClass('hidden');
    }
})


function submitAttendance(){
    $.ajax({
        url: herokuUrl+'/setAttendanceArray',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                $('#add-attendance-form-errors').html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                swal(
                    messages.success,
                    messages.successfullySubmitted,
                    'success'
                );
                $('#offDayMarkingModal').modal('hide');
                getAttendance();
                setTimeout(function(){
                    $('#attendance-today').removeClass('hidden').slideUp();
                    $('#results-list').slideDown();
                    $('#add-attendance-form-errors').html('');
                },1000)

            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitAttendance');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getAttendance(){
    data = {};
    if(studentID !== ''){
        data.studentID = studentID;
    }

    if(classID !== ''){
        data.classID = classID;
    }

    if($('#attendanceDate').val() !== ''){
        data.dateFrom = $('#attendanceDate').val();
    }

    if($('#attendanceDate').val() !== ''){
        data.dateTo = $('#attendanceDate').val();
    }
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        iDisplayLength: 100,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getAttendance',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                __ATTENDANCE__ = [];
                response = parseResponse(res);
                if(res.response.length > 0){
                    isAttendanceAvailable = true;
                }else{
                    isAttendanceAvailable = false;
                }
                var return_data = new Array();
                __ATTENDANCE__ = response;
                for(var i = 0; i< response.length; i++){

                    var at = '';
                    let present;
                    let notesText;

                    response[i].data.forEach((val, index) => {
                        let key = Object.keys(val)[0];
                        let key1 = Object.keys(val)[1];
                        let v = val[Object.keys(val)[0]];
                        notesText = val[Object.keys(val)[1]];
                        at += `<li>${moment(key).format('LL')} - ${v}</li>`;
                        if(v == messages.present){
                            present = 'P'
                        }else if(v == messages.absent){
                            present = 'A'
                        }else if(v == messages.leave){
                            present = 'L'
                        }else if(v == messages.late){
                            present = 'T'
                        }else if(v == messages.offDayAttendance){
                            present = 'O'
                        }
                    });

                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'teacherName' : response[i].teacherName,
                        'notesText' : notesText !== null && notesText !== 'null' ? notesText : '-' ,
                        'attendance' : `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" class=" attendanc" disabled id="checkbox-pp"
                                            ${present == 'P' ? 'checked' : ''} />
                                            <label for="checkbox-pp"><b class="check-text">P</b></label>
                                        </div>
                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" class=" attendanc" disabled id="checkbox-d-aa" ${present == 'A' ? 'checked' : ''}/>
                                            <label for="checkbox-d-aa"><b class="check-text">A</b></label>
                                        </div>
                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-l-ll" ${present == 'L' ? 'checked' : ''} />
                                            <label for="checkbox-l-ll"><b class="check-text">L</b></label>
                                        </div>
                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-t-tt" ${present == 'T' ? 'checked' : ''} />
                                            <label for="checkbox-t-tt"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>
                                        <div class="round round-o" data-toggle="tooltip" title="${messages.offDayAttendance}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-o-oo" ${present == 'O' ? 'checked' : ''} />
                                            <label for="checkbox-o-oo"><b class="check-text">O</b></label>
                                        </div>
                                    </div>`,
                        // 'action' : '<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="Edit" data-json='+handleApostropheWork(response[i])+'><i class="fa fa-pencil"></i></button>'
                        // 'action': ''
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getAttendance');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'teacherName'},
            {'data': 'notesText'},
            {'data': 'attendance'},
            // {'data': 'action'}
        ]
    });

}

function getStudentSubjectAttendance(){
    data = {};
    subjectID = $('#filter-subjects').val();
    attendanceDate = $('#attendanceDate').val();
    timetableSubjectID = $('#filter-subjects-in-time-table').val();
    if(subjectID !== ''){
        data.subjectID = subjectID;
    }

    if(timetableSubjectID !== ''){
        data.timetableSubjectID = timetableSubjectID;
    }

    if(attendanceDate !== ''){
        data.attendanceDate = attendanceDate;
    }

    if(batchNo !== ''){
        data.batchNo = batchNo;
    }

    if(subjectAttandanceTable){
        subjectAttandanceTable.destroy();
    }
    subjectAttandanceTable = $('#subject-attendance-table').DataTable({
        iDisplayLength: 100,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getStudentSubjectAttendance',
            type: 'POST',
            headers,
            beforeSend: function(){
                studentsInTimeTableArray = [];
                offDayAttendanceIds = [];
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                __SUBJECTATTENDANCE__ = response;
                for(var i = 0; i< response.length; i++){
                   offDayAttendanceIds.push(response[i])
                    var at = '';
                    let present;
                    let notesText;
                    response[i].data.forEach((val, index) => {
                        let key = Object.keys(val)[0];
                        let key1 = Object.keys(val)[1];
                        let v = val[Object.keys(val)[0]];
                        notesText = val[Object.keys(val)[1]];
                        at += `<li>${moment(key).format('LL')} - ${v}</li>`;
                        if(v == messages.present){
                            present = 'P'
                        }else if(v == messages.absent){
                            present = 'A'
                        }else if(v == messages.leave){
                            present = 'L'
                        }else if(v == messages.late){
                            present = 'T'
                        }else if(v == messages.offDayAttendance){
                            present = 'O'
                        }
                        // present = v == messages.present ? '1' : (v == messages.leave) ? '2' : '0';
                    });

                    return_data.push({
                        'sNo' : i+1,
                        'studentName': response[i].studentName,
                        'rollNo': response[i].rollNo,
                        'notesText' : notesText !== null && notesText !== 'null' ? notesText : '-' ,
                        'attendance' : `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" class=" attendanc" disabled id="checkbox-pp"
                                            ${present == 'P' ? 'checked' : ''} />
                                            <label for="checkbox-pp"><b class="check-text">P</b></label>
                                        </div>
                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" class=" attendanc" disabled id="checkbox-d-aa" ${present == 'A' ? 'checked' : ''}/>
                                            <label for="checkbox-d-aa"><b class="check-text">A</b></label>
                                        </div>
                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-l-ll" ${present == 'L' ? 'checked' : ''} />
                                            <label for="checkbox-l-ll"><b class="check-text">L</b></label>
                                        </div>
                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-t-tt" ${present == 'T' ? 'checked' : ''} />
                                            <label for="checkbox-t-tt"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>
                                        <div class="round round-o" data-toggle="tooltip" title="${messages.offDayAttendance}">
                                            <input type="checkbox" class="attendanc" disabled id="checkbox-o-oo" ${present == 'O' ? 'checked' : ''} />
                                            <label for="checkbox-o-oo"><b class="check-text">O</b></label>
                                        </div>
                                    </div>`,
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudentSubjectAttendance');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'rollNo'},
            {'data': 'notesText'},
            {'data': 'attendance'},
        ]
    });
}

function getClassStudents(){
    if(attendanceDataTable){
        attendanceDataTable.destroy();
    }
    attendanceDataTable = $('#attendance-add-table').DataTable({
        iDisplayLength: 100,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getClassStudents',
            type: 'POST',
            headers,
            beforeSend: function(){
                studentsArray = [];
            },
            data: {
                classID: classID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                studentsArray = response;
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let findedObj;
                    if(__ATTENDANCE__){
                        findedObj = __ATTENDANCE__.find(o => o.studentID === response[i].studentID);
                    }
                   if(findedObj !== undefined && findedObj !== ''){
                    findedObj.data.forEach((val, index) => {
                        let key = Object.keys(val)[0];
                        let v = val[Object.keys(val)[0]];
                        present = v == messages.present ? '1' : (v == messages.leave) ? '2' : '0';
                        if(v == messages.present){
                            present = 'P'
                        }else if(v == messages.absent){
                            present = 'A'
                        }else if(v == messages.leave){
                            present = 'L'
                        }else if(v == messages.late){
                            present = 'T'
                        }else if(v == messages.offDayAttendance){
                            present = 'O'
                        }
                    });
                    return_data.push({
                        'checkboxes': `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="P" class="mark-attendance mark-checkbox-p attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-p-${i}" ${present == 'P' ? 'checked' : ''} data-recordID = "${findedObj.attendanceID}" />
                                            <label for="checkbox-p-${i}"><b class="check-text">P</b></label>
                                        </div>

                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="A" class="mark-attendance mark-checkbox-a attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-d-${i}"  ${present == 'A' ? 'checked' : ''} data-recordID = "${findedObj.attendanceID}"/>
                                            <label for="checkbox-d-${i}"><b class="check-text">A</b></label>
                                        </div>

                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="L" class="mark-attendance mark-checkbox-l attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-l-${i}" ${present == 'L' ? 'checked' : ''} data-recordID = "${findedObj.attendanceID}" />
                                            <label for="checkbox-l-${i}"><b class="check-text">L</b></label>
                                        </div>

                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="T" class="mark-attendance mark-checkbox-t attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-t-${i}" ${present == 'T' ? 'checked' : ''} data-recordID = "${findedObj.attendanceID}" />
                                            <label for="checkbox-t-${i}"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>

                                    </div>`,
                                    'studentName': response[i].studentName,
                    })
                   }else{
                    return_data.push({
                        'checkboxes': `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="P" class="mark-attendance mark-checkbox-p attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-p-${i}" data-recordID = "0" />
                                            <label for="checkbox-p-${i}"><b class="check-text">P</b></label>
                                        </div>

                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="A" class="mark-attendance mark-checkbox-a attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-d-${i}" data-recordID = "0" />
                                            <label for="checkbox-d-${i}"><b class="check-text">A</b></label>
                                        </div>

                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="L" class="mark-attendance mark-checkbox-l attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-l-${i}" data-recordID = "0" />
                                            <label for="checkbox-l-${i}"><b class="check-text">L</b></label>
                                        </div>

                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" data-index="attendance-check-${i}" data-target="T" class="mark-attendance mark-checkbox-t attendance-check-${i}" data-json='${handleApostropheWork(response[i])}' id="checkbox-t-${i}" data-recordID = "0" />
                                            <label for="checkbox-t-${i}"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>

                                    </div>`,
                        'studentName': response[i].studentName,
                    })
                   }

                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getClassStudents');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'checkboxes'},
            {'data': 'studentName'}
        ]
    });

}

$('#filter-classes').on('change', function(){
    if(classID !== ''){
        getAttendance();
    }
});

$(document).on('change', '#filter-batch-no', function(){
    batchNo = $(this).val();
    if(Number(issubjectlevelattendance > 0 &&  timetableSubjectID !== '' && batchNo !== '')){
        $.LoadingOverlay("show");
        getStudentSubjectAttendance();
        getStudentsInTimetable();
        getCalendarDays();
        setTimeout(() => {
            $.LoadingOverlay("hide");
        },1000)
    }
});


$('#attendanceDate').on('change', function(){
    dateFromVal = $(this).val();
    dateToVal = $(this).val();
    if(Number(issubjectlevelattendance > 0 &&  timetableSubjectID !== '')){
        $.LoadingOverlay("show");
        getStudentSubjectAttendance();
        getStudentsInTimetable();
        getCalendarDays();
        setTimeout(() => {
            $.LoadingOverlay("hide");
        },1000)
    }else{
        if(classID !== '')
            getAttendance();
    }
})

function getCalendarDays(){
    if(schoolID == 0){
        schoolID = user.schoolID
    }
    $.ajax({
        url: herokuUrl+'/getCalendarDays',
        type: 'POST',
        headers,
        beforeSend: function(){
        },
        data: {
            schoolID : schoolID,
            dateFrom: dateFromVal,
            dateTo: dateToVal
        },
        success:function(res){
            // return
            if(typeof res.response.length !== 'undefined' &&  res.response.length > 0){
                res.response.forEach((value, index)=>{
                    if(value.isHoliday === 0){
                        isMarkedHoliday = false;
                    }else{
                        isMarkedHoliday = true;
                    }
                })
            }
        },
        error:function(err){
            invokedFunctionArray.push('getCalendarDays');
            window.requestForToken(err)
            console.log(err)
        },
    })
}

$(document).on('click', '.mark-attendance', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    if(checked){
        $('.' + index).prop('checked', false);
        $(this).prop('checked', true);
    }
});

$(document).on('click', '.mark-subject-attendance', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    if(checked){
        $('.' + index).prop('checked', false);
        $(this).prop('checked', true);
    }
});

$(document).on('click', '.mark-subject-attendances', function(){
    let checked = $(this).prop('checked');
    let target = $(this).attr('data-target');
    let present = target == 'P' ? '1' : (target == 'L') ? '2' : (target == 'T') ? '3' : '0';
    if(checked){
        $('.check-subject-attendance').prop('checked', false);
        unCheckedAlllistedStatusOfSubjectAttendance();

        $(this).prop('checked', true);
        checkedAlllistedStatusOfSubjectAttendance(present);
    }
})

$(document).on('click', '.mark-attendances', function(){
    let checked = $(this).prop('checked');
    let index = $(this).attr('data-index');
    let target = $(this).attr('data-target');
    let present = target == 'P' ? '1' : (target == 'L') ? '2' : (target == 'T') ? '3' : '0';
    if(checked){
        $('.check-attendance').prop('checked', false);
        unCheckedAlllistedStatusOfAttendance();

        $(this).prop('checked', true);
        checkedAlllistedStatusOfAttendance(present);
    }
})

$('#cancel-subject-attendance-today').on('click', function(){
    unCheckedAlllistedStatusOfSubjectAttendance();
    unCheckedToplistedStatusOfSubjectAttendance();
    $('#add-subject-attendance-form-errors').html('');
    processSlideUpForm();
})

$('.cancel-subject-attendance-form').on('click', function(){
    unCheckedAlllistedStatusOfSubjectAttendance();
    unCheckedToplistedStatusOfSubjectAttendance();
    $('#add-subject-attendance-form-errors').html('');
    processSlideUpForm();
})



function unCheckedAlllistedStatusOfSubjectAttendance(){
    $('.mark-subject-checkbox-a').each(function(){
        $(this).prop('checked',false);
    });
    $('.mark-subject-checkbox-p').each(function() {
        $(this).prop('checked',false);
    });
    $('.mark-subject-checkbox-l').each(function(){
        $(this).prop('checked',false);
    });
    $('.mark-subject-checkbox-t').each(function(){
        $(this).prop('checked',false);
    });
}

function unCheckedToplistedStatusOfSubjectAttendance(){
    $('.mark-subject-attendances').each(function(){
        $(this).prop('checked',false);
    });
}

function checkedAlllistedStatusOfAttendance(present = null){
    switch(present){
        case '0':
                $('.mark-checkbox-a').each(function(){
                    $(this).prop('checked',true);
                });
            break;
        case '1':
                $('.mark-checkbox-p').each(function() {
                    $(this).prop('checked',true);
                });
            break;
        case '2':
                $('.mark-checkbox-l').each(function(){
                    $(this).prop('checked',true);
                });
                break;
        case '3':
                $('.mark-checkbox-t').each(function(){
                    $(this).prop('checked',true);
                });
            break;
    }
}

function checkedAlllistedStatusOfSubjectAttendance(present = null){
    switch(present){
        case '0':
                $('.mark-subject-checkbox-a').each(function(){
                    $(this).prop('checked',true);
                });
            break;
        case '1':
                $('.mark-subject-checkbox-p').each(function() {
                    $(this).prop('checked',true);
                });
            break;
        case '2':
                $('.mark-subject-checkbox-l').each(function(){
                    $(this).prop('checked',true);
                });
                break;
        case '3':
                $('.mark-subject-checkbox-t').each(function(){
                    $(this).prop('checked',true);
                });
            break;
    }
}

$(document).on('change','#offDayNoteCheckBox',  function(){
    if($(this).prop("checked") === false ){
        $('#offDayNote').val('');
        $('#offDayNoteDiv').addClass('hidden');
        $('.attendance-add-table').slideDown();
    }else{
        $('#offDayNote').val('');
        $('#offDayNoteDiv').removeClass('hidden');
        $('.attendance-add-table').slideUp();
    }
});

function unCheckedAlllistedStatusOfAttendance(){

    $('.mark-checkbox-a').each(function(){
        $(this).prop('checked',false);
    });
    $('.mark-checkbox-p').each(function() {
        $(this).prop('checked',false);
    });
    $('.mark-checkbox-l').each(function(){
        $(this).prop('checked',false);
    });
    $('.mark-checkbox-t').each(function(){
        $(this).prop('checked',false);
    });
}

function unCheckedToplistedStatusOfAttendance(){
    $('.mark-attendances').each(function(){
        $(this).prop('checked',false);
    });
}

$('#attendance-form').on('submit', function(e){
    e.preventDefault();
});

$('#add-attendance-today').on('click', function(){
    let checkSubjectLevelAttendance = $('#filter-classLevels').find(':selected').attr('data-issubjectlevelattendance');
    if(checkSubjectLevelAttendance == 1 && $('#filter-subjects').val() == ''){
        swal(
            messages.error,
            messages.pleaseSelectSubject,
            'error'
        );
        return false;
    }
    if(checkSubjectLevelAttendance == 1 && $('#filter-subjects-in-time-table').val() == ''){
        swal(
            messages.error,
            messages.pleaseSelectTimetableSubject,
            'error'
        );
        return false;
    }
    let date = $('#attendanceDate').val();
    $('.check-subject-attendance').prop('checked', false);
    $('.check-attendance').prop('checked', false);
    $.LoadingOverlay("show");
    dateFromVal = $('#attendanceDate').val();
    dateToVal = $('#attendanceDate').val();
    if(typeof issubjectlevelattendance !== 'undefined' && Number(issubjectlevelattendance > 0 )){
        getStudentsInTimetable();
        getCalendarDays();
        setTimeout(() => {
            $.LoadingOverlay("hide");
            subjectToggleForm()
        },1000)
    }else{
        getClassStudents();
        getCalendarDays();
        setTimeout(() => {
            $.LoadingOverlay("hide");
            attendanceToggleForm()
        },1000)
    }
});

function processSlideDownForm(){
    if(typeof issubjectlevelattendance !== 'undefined' && Number(issubjectlevelattendance > 0 )){
        $('#subject-attendance').removeClass('hidden').slideDown();
        $('#subject-attendance-table-div').removeClass('hidden');
        $('#attendance-table-div').addClass('hidden');
    }else{
        $('#attendance-today').removeClass('hidden').slideDown();
        $('#subject-attendance-table-div').addClass('hidden');
        $('#attendance-table-div').removeClass('hidden');
    }
    $('#results-list').slideUp();
}

function processSlideUpForm(){
    if(typeof issubjectlevelattendance !== 'undefined' && Number(issubjectlevelattendance > 0 )){
        $('#subject-attendance-table-div').removeClass('hidden');
        $('#attendance-table-div').addClass('hidden');
    }else{
        $('#subject-attendance-table-div').addClass('hidden');
        $('#attendance-table-div').removeClass('hidden');
    }
    $('#subject-attendance').slideUp();
    $('#attendance-today').slideUp();
    $('#results-list').slideDown();
}

function subjectToggleForm(){
    if(isMarkedHoliday === false && isAttendanceAvailable === false){
        if(classID !== ''){
           processSlideDownForm();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === false && isAttendanceAvailable === true){
        if(classID !== ''){
            processSlideDownForm();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === true && isAttendanceAvailable === true){
        if(classID !== ''){
            processSlideDownForm();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === true && isAttendanceAvailable === false){
        swal(
            messages.error,
            messages.selectedDateIsMarkedAsHoliday,
            'error'
        );
        return false;
    }
}

function getStudentsInTimetable(){
    data = {};
    var notes = '';
    subjectID = $('#filter-subjects').val();
    timetableSubjectID = $('#filter-subjects-in-time-table').val();
    if(subjectID !== ''){
        data.subjectID = subjectID;
    }
    if(timetableSubjectID !== ''){
        data.timetableSubjectID = timetableSubjectID;
    }
    if(weekdayNo !== ''){
        data.weekdayNo = weekdayNo;
    }
    if(batchNo !== ''){
        data.batchNo = batchNo;
    }

    if(subjectAttendanceDataTable){
        subjectAttendanceDataTable.destroy();
    }
    subjectAttendanceDataTable = $('#subject-attendance-add-table').DataTable({
        iDisplayLength: 100,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getStudentsInTimetable',
            type: 'POST',
            headers,
            beforeSend: function(){
                studentsArrayInTimeTable = [];
            },
            data: {
                subjectID: subjectID,
                weekdayNo: weekdayNo,
                timetableSubjectID: timetableSubjectID
            },
            dataSrc: function(res){
                response = parseResponse(res);
                studentsArrayInTimeTable = response;
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let findedObj;
                    if(__SUBJECTATTENDANCE__){
                        findedObj = __SUBJECTATTENDANCE__.find(o => o.studentID === response[i].studentID);
                    }
                   if(findedObj !== undefined && findedObj !== ''){
                        findedObj.data.forEach((val, index) => {
                        let key = Object.keys(val)[0];
                        notes = val[Object.keys(val)[1]];
                        let v = val[Object.keys(val)[0]];
                        present = v == messages.present ? '1' : (v == messages.leave) ? '2' : '0';
                        if(v == messages.present){
                            present = 'P'
                        }else if(v == messages.absent){
                            present = 'A'
                        }else if(v == messages.leave){
                            present = 'L'
                        }else if(v == messages.late){
                            present = 'T'
                        }else if(v == messages.offDayAttendance){
                            present = 'O'
                        }
                    });
                    return_data.push({
                        'checkboxes': `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="P" class="mark-subject-attendance mark-subject-checkbox-p subject-attendance-check-${i}" data-row='${i}' data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-p-${i}" ${present == 'P' ? 'checked' : ''} data-recordID = "${findedObj.attendanceID}"  />
                                            <label for="subject-checkbox-p-${i}"><b class="check-text">P</b></label>
                                        </div>

                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="A" class="mark-subject-attendance mark-subject-checkbox-a subject-attendance-check-${i}" data-row='${i}'  data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-d-${i}"  ${present == 'A' ? 'checked' : ''} data-recordID = "${findedObj.attendanceID}" />
                                            <label for="subject-checkbox-d-${i}"><b class="check-text">A</b></label>
                                        </div>

                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="L" class="mark-subject-attendance mark-subject-checkbox-l subject-attendance-check-${i}" data-row='${i}' data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-l-${i}" ${present == 'L' ? 'checked' : ''}  data-recordID = "${findedObj.attendanceID}" />
                                            <label for="subject-checkbox-l-${i}"><b class="check-text">L</b></label>
                                        </div>

                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="T" class="mark-subject-attendance mark-subject-checkbox-t subject-subject-attendance-check-${i}" data-row='${i}' data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-t-${i}" ${present == 'T' ? 'checked' : ''} data-recordID = "${findedObj.attendanceID}"  />
                                            <label for="subject-checkbox-t-${i}"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>

                                    </div>`,
                                    'studentName': response[i].studentName,
                                    'rollNo': response[i].rollNo,
                                    'notes': `<input maxlength="100" name="notes" data-validation="length" data-validation-length="max100" value='${typeof notes !== 'undefined' ? notes : '' }' class="form-control"/>`,
                    })
                   }else{
                    return_data.push({
                        'checkboxes': `<div class="flex-container" style="justify-content: center">
                                        <div class="round" data-toggle="tooltip" title="${messages.present}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="P" class="mark-subject-attendance mark-subject-checkbox-p subject-attendance-check-${i}" data-row='${i}' data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-p-${i}" data-recordID = "0" />
                                            <label for="subject-checkbox-p-${i}"><b class="check-text">P</b></label>
                                        </div>

                                        <div class="round round-d" data-toggle="tooltip" title="${messages.absent}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="A" class="mark-subject-attendance mark-subject-checkbox-a subject-attendance-check-${i}" data-row='${i}' data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-d-${i}" data-recordID = "0"/>
                                            <label for="subject-checkbox-d-${i}"><b class="check-text">A</b></label>
                                        </div>

                                        <div class="round round-l" data-toggle="tooltip" title="${messages.leave}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="L" class="mark-subject-attendance mark-subject-checkbox-l subject-attendance-check-${i}" data-row='${i}' data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-l-${i}" data-recordID = "0"/>
                                            <label for="subject-checkbox-l-${i}"><b class="check-text">L</b></label>
                                        </div>

                                        <div class="round round-t" data-toggle="tooltip" title="${messages.late}">
                                            <input type="checkbox" data-index="subject-attendance-check-${i}" data-target="T" class="mark-subject-attendance mark-subject-checkbox-t subject-attendance-check-${i}" data-row='${i}' data-json='${handleApostropheWork(response[i])}' id="subject-checkbox-t-${i}" data-recordID = "0"/>
                                            <label for="subject-checkbox-t-${i}"><b class="late-text check-text">${messages.late}</b></label>
                                        </div>

                                    </div>`,
                        'studentName': response[i].studentName,
                        'rollNo': response[i].rollNo,
                        'notes': '<input maxlength="100" name="notes" data-validation="length" data-validation-length="max100" class="form-control"/>',
                    })
                   }

                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getStudentsInTimetable');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'checkboxes'},
            {'data': 'studentName'},
            {'data': 'rollNo'},
            {'data': 'notes'}
        ]
    });
}

function attendanceToggleForm(){
    if(isMarkedHoliday === false && isAttendanceAvailable === false){
        if(classID !== ''){
            $('#attendance-today').removeClass('hidden').slideDown();
            $('#results-list').slideUp();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === false && isAttendanceAvailable === true){
        if(classID !== ''){
            $('#attendance-today').removeClass('hidden').slideDown();
            $('#results-list').slideUp();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === true && isAttendanceAvailable === true){
        if(classID !== ''){
            $('#attendance-today').removeClass('hidden').slideDown();
            $('#results-list').slideUp();
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }
    }else if(isMarkedHoliday === true && isAttendanceAvailable === false){
        swal(
            messages.error,
            messages.selectedDateIsMarkedAsHoliday,
            'error'
        );
        return false;
    }
}

$('#cancel-attendance-today').on('click', function(){
    unCheckedAlllistedStatusOfAttendance();
    unCheckedToplistedStatusOfAttendance();
    $('#add-attendance-form-errors').html('');
    processSlideUpForm();
})

$('#cancel-attendance-form').on('click', function(){
    unCheckedAlllistedStatusOfAttendance();
    unCheckedToplistedStatusOfAttendance();
    $('#add-attendance-form-errors').html('');
    $('#attendance-today').removeClass('hidden').slideUp();
    $('#results-list').slideDown();
})

// $(document).on('click', '#mark-off-day-attendance', function(){
//     if(classID !== ''){
//         $('#offDayNote').removeClass('custom-validation-error');
//         $('.custom-validation-error-msg').addClass('hidden');
//         $('#offDayMarkingModal').modal();
//     }else{
//         swal(
//             'Error',
//             'Please Select Section',
//             'error'
//         );
//         return false;
//     }
// });

$(document).on('click', '#mark-off-day-attendance', function(){
    let date = $('#attendanceDate').val();
    $('.check-subject-attendance').prop('checked', false);
    $('.check-attendance').prop('checked', false);
    dateFromVal = $('#attendanceDate').val();
    dateToVal = $('#attendanceDate').val();
    if(typeof issubjectlevelattendance !== 'undefined' && Number(issubjectlevelattendance > 0 )){
        $('#offDayNote').removeClass('custom-validation-error');
        $('.custom-validation-error-msg').addClass('hidden');
        $('#offDayMarkingModal').modal();
        getStudentsInTimetable();
        getCalendarDays();
    }else{
        $('#offDayNote').removeClass('custom-validation-error');
        $('.custom-validation-error-msg').addClass('hidden');
        $('#offDayMarkingModal').modal();
        getClassStudents();
        getCalendarDays();
    }
// }
    // if(classID !== ''){
    //     $('#offDayNote').removeClass('custom-validation-error');
    //     $('.custom-validation-error-msg').addClass('hidden');
    //     $('#offDayMarkingModal').modal();
    // }else{
    //     swal(
    //         'Error',
    //         'Please Select Section',
    //         'error'
    //     );
    //     return false;
    // }
});

$(document).on('click','.submit-off-day-attendance', function(){
    let checkSubjectLevelAttendance = $('#filter-classLevels').find(':selected').attr('data-issubjectlevelattendance');
    if(checkSubjectLevelAttendance == 1){
        let timetableSubjectID = $('#filter-subjects-in-time-table').val();
        let batchNo = $('#filter-batch-no').val();
        if(classID !== ''){
            var offDayAttendanceNote = $('#offDayNote').val();
            if(offDayAttendanceNote.length == 0){
                $('#offDayNote').addClass('custom-validation-error');
                $('.custom-validation-error-msg').removeClass('hidden')
                return false;
            }else{
                $('#offDayNote').removeClass('custom-validation-error');
                $('.custom-validation-error-msg').addClass('hidden');
                bulkArray = [];
                let user = JSON.parse(localStorage.getItem('user'));
                let date = $('#attendanceDate').val();
                if(offDayAttendanceIds.length > 0){
                    let attendance = {};
                    offDayAttendanceIds.forEach(function(value, index){
                        let attendance = {};
                        attendance.attendanceID = value.attendanceID
                        attendance.isPresent = 'O';
                        attendance.userID = user.userID;
                        attendance.studentID = value.studentID;
                        attendance.classID = classID;
                        attendance.timetableSubjectID = timetableSubjectID;
                        attendance.batchNo = batchNo;
                        attendance.attendanceDate = date;
                        attendance.notes = offDayAttendanceNote;
                        bulkArray.push(attendance);
                    })
                }else{
                    studentsArrayInTimeTable.forEach(function(value, index){
                        let attendance = {};
                        attendance.isPresent = 'O';
                        attendance.userID = user.userID;
                        attendance.studentID = value.studentID;
                        attendance.classID = classID;
                        attendance.timetableSubjectID = timetableSubjectID;
                        attendance.batchNo = batchNo;
                        attendance.attendanceDate = date;
                        attendance.notes = offDayAttendanceNote;
                        bulkArray.push(attendance);
                    });
                }

                data['array'] = bulkArray;
                setTimeout(function(){
                    showProgressBar(submitLoader);
                    resetMessages();
                    setSubjectAttendanceArray();
                }, 500);

            }
            }else{
                swal(
                    messages.error,
                    messages.pleaseSelectSection,
                    'error'
                );
                return false;
            }
    }else{
        if(classID !== ''){
            var offDayAttendanceNote = $('#offDayNote').val();
            if(offDayAttendanceNote.length == 0){
                $('#offDayNote').addClass('custom-validation-error');
                $('.custom-validation-error-msg').removeClass('hidden')
                return false;
            }else{
                $('#offDayNote').removeClass('custom-validation-error');
                $('.custom-validation-error-msg').addClass('hidden');
                bulkArray = [];
                let user = JSON.parse(localStorage.getItem('user'));
                let date = $('#attendanceDate').val();
                studentsArray.forEach(function(value, index){
                    let attendance = {};
                    attendance.isPresent = 'O';
                    attendance.userID = user.userID;
                    attendance.studentID = value.studentID;
                    attendance.classID = value.classID;
                    attendance.attendanceDate = date;
                    attendance.notes = offDayAttendanceNote;
                    bulkArray.push(attendance);
                });
                data['array'] = bulkArray;
                setTimeout(function(){
                    showProgressBar(submitLoader);
                    resetMessages();
                    submitAttendance();
                }, 500);

            }
            }else{
                swal(
                    messages.error,
                    messages.pleaseSelectSection,
                    'error'
                );
                return false;
            }
    }

})

$('#bulk-attendance-form').on('submit', function(e){
    e.preventDefault();
    data = {};
    if(classID !== ''){
        var offDayAttendanceNote = $('#offDayNote').val();
        if($('#offDayNoteCheckBox').prop("checked") === true ){
            if(offDayAttendanceNote.length == 0){
                $('#offDayNote').addClass('custom-validation-error');
                $('.custom-validation-error-msg').removeClass('hidden')
                return false;
            }else{
                $('#offDayNote').removeClass('custom-validation-error');
                $('.custom-validation-error-msg').addClass('hidden')
            }
        }
    bulkArray = [];
    let user = JSON.parse(localStorage.getItem('user'));
    let date = $('#attendanceDate').val();
        $( '.mark-attendance').each( function(){
            let attendance = {};
            let target = $(this).attr('data-target');
            let checked = $(this).prop('checked');
            let obj = JSON.parse($(this).attr('data-json'));
            let attendanceID = $(this).attr('data-recordID');
            let present = target == 'P' ? 'P' : (target == 'L') ? 'L' : (target == 'T') ? 'T' : 'A';
            if(checked){
               if(typeof attendanceID !== 'undefined' && Number(attendanceID) > 0 )
                    attendance.attendanceID = attendanceID;
                attendance.isPresent = present;
                attendance.userID = user.userID;
                attendance.studentID = obj.studentID;
                attendance.classID = obj.classID;
                attendance.attendanceDate = date;
                bulkArray.push(attendance);
            }
        })
        if(bulkArray && bulkArray.length > 0){
            data['array'] = bulkArray;
        }else{
            swal(
                messages.pleaseSelectMarkAtleast1Student,
                messages.oops,
                'error',{
                    buttons:true,//The right way
                    buttons: messages.ok, //The right way to do it in Swal1
                }
            )
            return false;
        }
        setTimeout(function(){
            showProgressBar(submitLoader);
            resetMessages();
            submitAttendance();
        }, 500);
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }

});

$('#bulk-subject-attendance-form').on('submit', function(e){
    e.preventDefault();
    if(classID !== ''){
        data = {};
        var offDayAttendanceNote = $('#offDayNote').val();
        if($('#offDayNoteCheckBox').prop("checked") === true ){
            if(offDayAttendanceNote.length == 0){
                $('#offDayNote').addClass('custom-validation-error');
                $('.custom-validation-error-msg').removeClass('hidden')
                return false;
            }else{
                $('#offDayNote').removeClass('custom-validation-error');
                $('.custom-validation-error-msg').addClass('hidden')
            }
        }
    bulkArray = [];
    let user = JSON.parse(localStorage.getItem('user'));
    let date = $('#attendanceDate').val();
    var notesData = $(this).serializeArray();
    notesData.shift();
    timetableSubjectID = $('#filter-subjects-in-time-table').val();
        $( '.mark-subject-attendance').each( function(index,val){
            var batchNo = $('#filter-batch-no').val();
            let attendance = {};
            let target = $(this).attr('data-target');
            let nodeIndex = $(this).attr('data-row');
            let checked = $(this).prop('checked');
            let obj = JSON.parse($(this).attr('data-json'));
            let attendanceID =  $(this).attr('data-recordID');
            let present = target == 'P' ? 'P' : (target == 'L') ? 'L' : (target == 'T') ? 'T' : 'A';
            if(checked){
                if(typeof attendanceID !== 'undefined' && Number(attendanceID) > 0 )
                    attendance.attendanceID = attendanceID;
                attendance.isPresent = present;
                attendance.userID = user.userID;
                attendance.classID = classID;
                attendance.studentID = obj.studentID;
                attendance.attendanceDate = date;
                attendance.timetableSubjectID = timetableSubjectID;
                attendance.batchNo = batchNo;
                attendance.notes = '';
                if(typeof notesData[nodeIndex] !== 'undefined' ){
                    attendance.notes = notesData[nodeIndex]['value'];
                }
                bulkArray.push(attendance);
            }
        })
        if(bulkArray.length === 0){
            swal(
                messages.error,
                messages.pleaseMarkAttendanceFirst,
                'error'
            );
            return false;
        }
        setTimeout(function(){
            showProgressBar(submitLoader);
            resetMessages();
            setSubjectAttendanceArray();
        }, 500);
        }else{
            swal(
                messages.error,
                messages.pleaseSelectSection,
                'error'
            );
            return false;
        }

});

function setSubjectAttendanceArray(){
    $.ajax({
        url: herokuUrl+'/setSubjectAttendanceArray',
        method: 'POST',
        headers,
        data: {
            'array': bulkArray
        },
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                $('#add-attendance-form-errors').html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                swal(
                    messages.success,
                    messages.successfullySubmitted,
                    'success'
                );
                $('#offDayMarkingModal').modal('hide');
                getStudentSubjectAttendance();
                setTimeout(function(){
                    $('#add-subject-attendance-form-errors').html('');
                    processSlideUpForm();
                },1000)

            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('setSubjectAttendanceArray');
            window.requestForToken(xhr);
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

