var nonAcademicResultListTable = $('non-academic-result-list-table');
var nonAcademicForm = $('#non-academic-form');
var nonAcademicList = [];
var  legendsArray;
var totalNonSubjectResults = 0;
var totalStudents = 0;
var existingLegendArray = [];
var bulkArray = [];

// get all schools
$(document).ready(function () {
    getAjaxSchools();
})

$(document).on('change','#filter-exam-term',function(){
    if(examTermID !== ''){
        hideAndShowTable();
        getNonAcademicResult();
        iterateStudentListTableForNonAcademic();
    }
});
function hideAndShowTable(){
    $('#student-non-academic-result-table tbody').empty();
    $('#student-non-academic-result-table tbody').append(`
    <tr>
    <td colspan="6"><strong style="color:#188ae2;font-weight:bold"> ${messages.loading}</<strong></td>
    </tr>
    `);
}
$(document).on('change','#filter-non-subjects',() => {
    if(nonSubjectID !== ''){
        hideAndShowTable();
        getNonSubjectLegends(nonSubjectID);
        getNonAcademicResult();
        setTimeout(function(){
            iterateStudentListTableForNonAcademic();
         },1000);
    }
});


$(document).on('change','#filter-classes',function(){
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            totalStudents = res.data.length;
        },
        error: function(err){
            window.requestForToken(err)
            console.log(err);
        }
    });
});

function getNonSubjectLegends(nonSubjectID){
    $('.p2-legends-result-list').removeClass('hidden');
    $.ajax({
        url: herokuUrl+'/getNonSubjectLegends',
        type: 'POST',
        headers,
        beforeSend: function(){
            showProgressBar('p2-legend');
            $('#legends-table tbody').empty();
        },
        data: {
            nonSubjectID: nonSubjectID,
            classLevelID : classLevelID
        },
        success: function(res){
            response = parseResponse(res);
            existingLegendArray = response;
        },
        error: function(xhr){
            window.requestForToken(xhr)
            console.log(xhr);
        }
    });
}


// Non Academic Result
function getNonAcademicResult() {
    if (nonAcademicResult) {
        nonAcademicResult.destroy();
    }
    nonAcademicResult = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax':{
            url: herokuUrl + '/getExamNSResult',
            method: 'POST',
            headers,
            data: {
                examsTermID: examTermID,
                classID: classID,
                nonSubjectID: nonSubjectID
            },
            beforeSend: function () {
                nonAcademicResultListTable.empty();
                nonAcademicList = [];
            },
            dataSrc: function (res) {
                response = parseResponse(res);
                if(nonSubjectID !== ''){
                    if(response.length && response.length < totalStudents){
                        $('.non-academic-flag').addClass('label-danger').html(messages.fewResultsAreNotSubmitted).show(1000);
                        $('.non-academic-flag').removeClass('label-success');
                    }else if( response.length && totalStudents == response.length){
                        $('.non-academic-flag').addClass('label-success').html(messages.completedResults).show(1000);
                        $('.non-academic-flag').removeClass('label-danger');
                    }else{
                        $('.non-academic-flag').hide();
                    }
                }
                var return_data = new Array();
                for (let i = 0; i < response.length; i++) {
                    const element = response[i];
                    nonAcademicList.push(element);
                    return_data.push({
                        'sNo': i + 1,
                        'studentName': element.studentName,
                        'nonSubjectName': element.nonSubjectName,
                        'notesNS': element.notesNS,
                        'gradeNS': element.gradeNS,
                        'action': `${__DEL__ ?
                            `<button class="btn-danger btn btn-xs delete-result" data-result-id='${element.examNSResultID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                            ` : '' }`,
                    })
                }
                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getNonAcademicResult')
                window.requestForToken(xhr)
                parseValidationErrors(xhr);
            }
        },
        "columns": [{
                'data': 'sNo'
            },
            {
                'data': 'studentName'
            },
            {
                'data': 'nonSubjectName'
            },
            {
                'data': 'notesNS'
            },
            {
                'data': 'gradeNS'
            },
            {
                'data': 'action'
            }
        ]
    });
}

$(document).on('click', '.delete-result', function(){
    examNSResultID = $(this).attr('data-result-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteThisResult+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteNsubjectResult();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });
})

function deleteNsubjectResult(){
    $.ajax({
        url: herokuUrl+'/deleteExamNonSubjectResult',
        method: 'POST',
        headers,
        data: {
            examNSResultID: examNSResultID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.resultHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                examNSResultID = '';
                if(tableData){
                    tableData.destroy();
                }
                getNonAcademicResult();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteNsubjectResult');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                   xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                   xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

function iterateStudentListTableForNonAcademic(){
    var nonAcademicAddTable = $('#student-non-academic-result-table tbody');
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {
            showProgressBar('p2-students-result-list');
        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                nonAcademicAddTable.empty();
                if(res.data.length){
                    $.each(res.data, function(index, value){
                        let addedResult;
                        if(nonAcademicList){
                            addedResult = nonAcademicList.find(o => o.studentID === value.studentID);
                        }
                        rollNumber = value.rollNo.substring(0, 8)
                        if(addedResult !== undefined && addedResult !== ''){
                            var insertTable = `
                            <tr>
                            <td style="display:none;">
                                <input class="form-control" name="examNSResultID" type="text" value=' ${ addedResult.examNSResultID } '>
                            </td>
                            <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                            <td><span data-toggle="tooltip" title="${value.rollNo}">
                                ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                                </span></td>
                            <td style="display:none;">
                                <input class="form-control" name="studentID" type="text" value=' ${ value.studentID } '>
                            </td>
                            <td>${value.studentName }</td>
                            <td>
                                <div>
                                    <select name="nsLegendID"  class="form-control legend-filters" >
                                        <option>Select Legend</option>`;
                                        existingLegendArray.forEach((value, index) => {
                                                if(value.legendValue !== null){
                                                    insertTable +=  `<option value="${value.nsLegendID}" ${addedResult.nsLegendID &&  addedResult.nsLegendID == value.nsLegendID ? 'selected' : ''}>${value.legendValue}</option>`;
                                                }else{
                                                    insertTable +=  `<option value="0" style="color:red !important">${messages.legendsNotFound}</option>`;
                                                    $(this).attr('style',"border: 1px solid red");
                                                }
                                            });

                            insertTable += `
                                    </select>
                                    <div class="p2-legends-result-list mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100% !important">
                                    </div>
                                </div>
                            </td>
                            <td>
                            <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="notesNS" type="text" value="">${addedResult.notesNS != '' && addedResult.notesNS != null && addedResult.notesNS != 'undefined' ? addedResult.notesNS : ''}</textarea>
                            </td>
                            <td>
                                    <input maxlength="2" class="form-control" data-validation="length" data-validation-length="max2" name="gradeNS" type="text" value="${addedResult.gradeNS != '' && addedResult.gradeNS != null && addedResult.gradeNS != 'undefined' ? addedResult.gradeNS : ''}">
                            </td>
                            </tr>`;

                            nonAcademicAddTable.append(insertTable);
                        }else{
                            var nonfilledInsertTable = `
                            <tr>
                            <td style="display:none;">
                                <input class="form-control" name="examNSResultID" type="text" value='0'>
                            </td>
                            <td style="color:#188ae2;font-weight:bold">${index + 1}</td>
                            <td><span data-toggle="tooltip" title="${value.rollNo}">
                                ${value.rollNo.length <= 7  ? value.rollNo : rollNumber+'...'}
                                </span></td>
                            <td style="display:none;">
                                <input class="form-control" name="studentID" type="text" value=' ${ value.studentID } '>
                            </td>
                            <td>${value.studentName }</td>
                            <td>
                                <div>
                                    <select name="nsLegendID"  class="form-control legend-filters" >
                                        <option value="0">${messages.selectLegend}</option>`;
                                        existingLegendArray.forEach((value, index) => {

                                            // $(this).removeAttr('style');
                                            if(value.legendValue !== null){
                                                nonfilledInsertTable +=  `<option value="${value.nsLegendID}">${value.legendValue}</option>`;
                                            }else{
                                                nonfilledInsertTable +=  `<option value="0" style="color:red !important">${messages.legendsNotFound}</option>`;
                                                $(this).attr('style',"border: 1px solid red");
                                            }
                                        });
                            nonfilledInsertTable  +=  `
                                </select>
                                <div class="p2-legends-result-list mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100% !important">
                                </div>
                            </div>
                            <td>
                                <textarea maxlength="250" class="form-control" data-validation="length" data-validation-length="max250" name="notesNS" type="text"></textarea>
                            </td>
                            <td>
                                <input maxlength="2" class="form-control" data-validation="length" data-validation-length="max2" name="gradeNS" type="text">
                            </td>
                            </tr>`;
                            nonAcademicAddTable.append(nonfilledInsertTable);
                        }
                        });

                    hideProgressBar('p2-students-result-list');
                }else{
                    nonAcademicAddTable.empty();
                    nonAcademicAddTable.append(`
                        <tr>
                            <td colspan="6" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)

                    hideProgressBar('p2-students-result-list');
                }

                } else {
                    nonAcademicAddTable.empty();
                    nonAcademicAddTable.append(`<tr>
                            <td colspan="6" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                }

        },
        error: function (err) {
            invokedFunctionArray.push('iterateStudentListTableForNonAcademic')
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-students-result-list');
        }
    })
}

nonAcademicForm.on('submit', function (e) {
    e.preventDefault();
    var result = $(this).serializeArray();
    var filtersformData = $('#filter-form').serializeArray();
    var sliecedArray = filtersformData.slice(4, filtersformData.length);
    var chunkedArray = chunkArray(result, 5);
    chunkedArray.forEach((val, index) => {
        var myObject = {};
        myObject["userID"] = user.userID;
        sliecedArray.forEach((data, formIndex) => {
            myObject[data.name] = data.value;
        });
        val.forEach((innerVal, innerIndex) => {
            if(innerVal.name == 'examNSResultID'){
                if( Number(innerVal.value) > 0){
                    myObject[innerVal.name] = innerVal.value;
                }
            }else{
                myObject[innerVal.name] = innerVal.value;
            }
        });
        bulkArray.push(myObject);
    });
    bulkArray.forEach((value, index) => {
        for (var propName in value) {
            if(propName === 'gradeNS'){
                if (value[propName] === null || value[propName] === undefined || value[propName] == '') {
                    delete bulkArray[index]['gradeNS'];
                }
            }
        }
    });
    // bulkArray.forEach((value, index) => {
    //     bulkArray[index] = clean(value);
    // });
    bulkArray =  _.filter(bulkArray,v => _.keys(v).length !== 0);
    submitNonAcademic();
});

function submitNonAcademic(){
    if(bulkArray.length > 0){
        $.ajax({
            url: herokuUrl + '/setExamNonSubjectResultsArray',
            method: 'POST',
            headers,
            data :{array: bulkArray},
            beforeSend: function () {
                disableButtonsToggle();
                nonAcademicForm.find('.form-message').remove();
                showProgressBar(submitLoader);
            },
            success: function (res) {
                if (res.response === 'success') {
                    // nonAcademicForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-success">Successfully Submitted</strong></p>`);

                    nonAcademicForm.trigger('reset');
                    $('#non-subject-result-hidden-field').removeAttr('name value');
                    swal(
                        messages.success,
                        messages.successfullySubmitted,
                        'success'
                    );
                    getNonAcademicResult();
                    hideAddFormContainer();
                }

                hideProgressBar(submitLoader);
                disableButtonsToggle();
            },
            error: function (xhr) {
                window.invokedFunctionArray.push('submitNonAcademic')
                window.requestForToken(xhr)
                if (xhr.status === 422) {
                    parseValidationErrors(xhr);
                }
                if (xhr.status === 400) {
                    nonAcademicForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                }

                if (xhr.status === 500) {
                    nonAcademicForm.append(`<p class="form-message"><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.response.userMessage}</strong></p>`);
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            }
        });
    }
}
