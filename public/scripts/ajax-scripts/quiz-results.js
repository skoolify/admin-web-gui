var bulkData = new Array();
var quizDate = '';
var quizResultArray = new Array();
var quizTotalMarks = '';
$(document).ready(function(){
    getAjaxSchools();
    // getQuizResults();
})

function submitQuizResult(){
    $.ajax({
        url: herokuUrl+'/setQuizResults',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                $('form#quiz-results-form').trigger('reset');
                $('.hidden-div').addClass('hidden');
                $("#filter-students").find('option:first').prop('selected',true);
                $('#filter-students').multiselect('rebuild')
                studentID = '';
                getQuizResults();
            }
            $('#add-quiz-results-table').addClass('hidden')
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitQuizResult')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function iterateQuizResultAddList(){
    let data = {};
    if(classID !== ''){
        data.classID = classID
    }

    if(branchID !== ''){
        data.branchID = branchID
    }

    if(schoolID !== ''){
        data.schoolID = schoolID
    }

    if(classLevelID !== ''){
        data.classLevelID = classLevelID;
    }
   if(quizResultAddForm){
        quizResultAddForm.destroy()
    }
    quizResultAddForm =  $('#quiz-results-add-table').DataTable({
        iDisplayLength: 100,
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl + '/getClassStudents',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                var obtainedMarks = 0;
                for(var i = 0; i< response.length; i++){
                    var addedData = quizResultArray.find(o => o.studentID === response[i].studentID);
                    rollNumber = response[i].rollNo.substring(0, 8)

                    if(addedData){
                        var checkDecimal = addedData.marksObtained.split('.')
                        if(checkDecimal[1] > 0){
                            obtainedMarks = parseFloat(addedData.marksObtained).toFixed(2)
                        }else{
                            obtainedMarks = parseFloat(addedData.marksObtained).toFixed(0)
                        }
                        return_data.push({
                            'rollNo': `<input type="hidden" name="quizID" value="${addedData.quizID}" /><input type="hidden" name="studentID" value="${response[i].studentID}" /> <span data-toggle="tooltip" title="${response[i].rollNo}">
                            ${response[i].rollNo.length <= 7  ? response[i].rollNo : rollNumber+'...'}
                            </span>`,
                            'studentName': response[i].studentName,
                            'marksObtained' : `<input type="number" placeholder="Obtained Marks" min="0" max="100" class="form-control obtainMarks" name="marksObtained" data-validation="required" value="${obtainedMarks !== 'null' && obtainedMarks !== null? obtainedMarks : 0 }" data-validation-error-msg="${messages.obtainMarkIsRequired}"/>`,
                            'comments' : `<textarea class="form-control" placeholder="Comments" maxlength="250" name="comments">${addedData.comments !== 'null' && addedData.comments !== null ? addedData.comments  : '' }</textarea>`
                        })
                    }else{
                        return_data.push({
                            'rollNo': `<input type="hidden" name="quizID" value="0" /><input type="hidden" name="studentID" value="${response[i].studentID}" /> <span data-toggle="tooltip" title="${response[i].rollNo}">
                            ${response[i].rollNo.length <= 7  ? response[i].rollNo : rollNumber+'...'}
                            </span>`,
                            'studentName': response[i].studentName,
                            'marksObtained' : `<input type="number" placeholder="Obtained Marks" min="0" max="100" class="form-control obtainMarks" name="marksObtained" data-validation="required" data-validation-error-msg="${messages.obtainMarkIsRequired}"/>`,
                            'comments' : '<textarea maxlength="250" class="form-control" placeholder="Comments" name="comments"></textarea>'
                        })
                    }
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('iterateQuizResultAddList')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'rollNo'},
            {'data': 'studentName'},
            {'data': 'marksObtained'},
            {'data': 'comments'},
        ]
    });
}
$(document).on('blur', '#marksTotal', function(){
    var totalMarks = Number($(this).val());
    if( totalMarks > 0 ){
        $('.marksTotal-error').addClass('hidden');
    }
});
$(document).on('blur', '.obtainMarks', function(){
    var marks = Number($(this).val());
    var totalMarks = Number($('#marksTotal').val());
    if( totalMarks > 0 ){
        $('.marksTotal-error').addClass('hidden');
        if(marks > totalMarks ){
            swal(
                messages.error,
                `${messages.obtainMarksAlwaysLessThanOrEqualTo} ${totalMarks}`,
                'error'
            );
            $(this).val('');
            $(this).focus();
        }
    }else{
        $('.marksTotal-error').removeClass('hidden');
        $(this).val('');
        $('#marksTotal').focus();
    }
})
function getQuizResults(){
    let data = {};
    if(studentID !== ''){
        data.studentID = studentID
    }

    if(subjectID !== ''){
        data.subjectID = subjectID
    }

    if(classID !== ''){
        data.classID = classID
    }

    if(quizDate !== ''){
        data.quizDate = quizDate;
    }

    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getQuizResults',
            type: 'POST',
            headers,
            beforeSend: function(){
                quizResultArray = [];
                quizTotalMarks = '';
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                var showGraphArray = new Array();
                var obtainedMarks = 0;
                for(var i = 0; i< response.length; i++){
                    showGraphArray.push(response[i].showGraph)
                    quizTotalMarks = response[i].marksTotal;
                    quizResultArray.push(response[i])
                    rollNumber = response[i].rollNo.substring(0, 8)
                    var checkDecimal = response[i].marksObtained.split('.')
                    if(checkDecimal[1] > 0){
                        obtainedMarks = parseFloat(response[i].marksObtained).toFixed(2)
                    }else{
                        obtainedMarks = parseFloat(response[i].marksObtained).toFixed(0)
                    }
                    return_data.push({
                        'sNo' : `<span data-toggle="tooltip" title="${response[i].rollNo}">
                        ${response[i].rollNo.length <= 7  ? response[i].rollNo : rollNumber+'...'}
                        </span>`,
                        'studentName': response[i].studentName,
                        'subjectName': response[i].subjectName,
                        'marksTotal' : response[i].marksTotal,
                        'marksObtained' : obtainedMarks,
                        'comments' : response[i].comments ? response[i].comments : `<label class="label label-sm label-danger"> ${messages.noComments}</label>`,
                        'quizDate' : response[i].quizDate ? `<span style="display:none">${Date.parse(response[i].quizDate)}</span>`+ moment(response[i].quizDate).format('DD-MM-YYYY') : '--',
                        'action' : `<div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-quiz" data-quiz-id='${response[i].quizID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}</div>`
                    })
                }
                if(showGraphArray && showGraphArray.length > 0){
                    if(jQuery.inArray(0,showGraphArray)!== -1){
                        $('#showGraph').val(0);
                        $('#showGraphInput').attr('checked', false);
                    }else{
                        $('#showGraph').val(1);
                        $('#showGraphInput').attr('checked', true);
                    }
                }
                return return_data;

            },
            error: function(xhr){
                invokedFunctionArray.push('getQuizResults')
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'subjectName'},
            {'data': 'marksTotal'},
            {'data': 'marksObtained'},
            {'data': 'comments'},
            {'data': 'quizDate'},
            {'data': 'action'}
        ]
    });
    iterateQuizResultAddList();

}

$(document).on('click', '.delete-quiz', function(){
    quizID = $(this).attr('data-quiz-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+' <br/> '+messages.youWantToDeleteThisQuiz+'</br>',
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteQuiz();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteQuiz(){
    $.ajax({
        url: herokuUrl+'/deleteQuiz',
        method: 'POST',
        headers,
        data: {
            quizID: quizID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.classHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                quizID = '';
                if(tableData){
                    tableData.destroy();
                }
                getQuizResults();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteQuiz');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.error,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.error,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.error,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}


function submitQuizResultBulk(){
    if(bulkData.length > 0){
    var bulkLoader = $('#bulk-quiz-result-loader');
    var submitBtn = $('#submit-add-quiz-result');
    var cancelBtn = $('#cancel-add-quiz-result');
    $.ajax({
        url: herokuUrl+'/setQuizResultsArray',
        method: 'POST',
        headers,
        data: {
            array: bulkData
        },
        beforeSend: function(){
            bulkLoader.removeClass('hidden');
            submitBtn.attr('disabled', true);
            cancelBtn.attr('disabled', true);
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.success,
                    messages.successfullySubmitted,
                    'success'
                )
                tableData.destroy();
                $('form#bulk-quiz-results-form').trigger('reset');
                $('.hidden-div').addClass('hidden');
                $('#marksTotal').val('');
                $('#filter-students').closest('.col-md-3').css('display', 'block');
                $('#add-quiz-results-table').addClass('hidden').slideUp();
                $('#results-list').slideDown();

                getQuizResults();
            }
            bulkLoader.addClass('hidden');
            submitBtn.removeAttr('disabled');
            cancelBtn.removeAttr('disabled');
        },
        error: function(xhr){
            invokedFunctionArray.push('submitQuizResultBulk')
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                swal(
                    messages.error,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                )
            }
            bulkLoader.addClass('hidden');
            submitBtn.removeAttr('disabled');
            cancelBtn.removeAttr('disabled');
        }
    });
    }else{
        swal(
            messages.error,
            messages.enterAtleastOneResult,
            'error'
        )
    }
}

$('#quizDate').on('change', function(){
    quizDate = $(this).val();
    if(classID !== ''){
        if(tableData){
            tableData.destroy();
        }
        getQuizResults();
    }
})


$('#add-add-quiz-result').on('click', function(){
    if($('#quizDate').val() === ''){
        swal(
            messages.pleaseSelectDateFirst,
            messages.errorMessage,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    $('.hidden-div').removeClass('hidden');
    if(quizTotalMarks > 0){
        $('#marksTotal').val(quizTotalMarks);
    }
    var time = moment().format('YYYY-MM-DD');
    $('#quizDate').val();
    $('#results-list').slideUp();
    $('#add-quiz-results-table').removeClass('hidden').slideDown();
    $('form#bulk-quiz-results-form').trigger('reset');
    students.attr('disabled', true);
    $('#filter-students').closest('.col-md-3').css('display', 'none');
    classID !== '' ? iterateQuizResultAddList() : '' ;
});

$('#cancel-add-quiz-result').on('click', function(){
    $('.hidden-div').addClass('hidden');
    $('#marksTotal').val('');
    $('#add-quiz-results-table').slideUp();
    $('#results-list').slideDown();
    $('form#bulk-quiz-results-form').trigger('reset');
    students.removeAttr('disabled');
    $('#filter-students').closest('.col-md-3').css('display', 'block');
});

$(document).on('click', '#showGraphInput', function(){
    if($(this).prop('checked')){
        $('#showGraph').val(1)
    }else{
        $('#showGraph').val(0)
    }
})

$('#submit-add-quiz-result').on('click', function(){
    if(subjectID !== ''){
        var formData = $('#bulk-quiz-results-form').serializeArray();

        var removedformData = formData.splice(0,1);
        let bulkQuizResults = chunkArray(formData, 4);
        bulkData = new Array();
        let rowLength = $('#quiz-results-add-tbody tr').length;
        let totalMarks = $('#marksTotal').val();
        let quizDate = $('#quizDate').val();
        let showGraph = $('#showGraph').val();
        // let showGraphChecked = showGraph.is(':checked');
        let pass = true;
        if(totalMarks !== '' && quizDate !== ''){
            bulkQuizResults.forEach((value, index) => {
                let temp = {};
                value.forEach((innerVal, innerIndex) => {
                    if(innerVal.name == 'quizID'){
                        if( Number(innerVal.value) > 0){
                            temp[innerVal.name] = innerVal.value;
                        }
                    }else{
                        temp[innerVal.name] = innerVal.value;
                    }
                })
                temp.quizDate = quizDate;
                temp.subjectID = subjectID;
                temp.classID = classID;
                temp.marksTotal = totalMarks;
                temp.showGraph = showGraph;
                bulkData.push(temp);


            })
            bulkData.forEach((value, index) => {
                for (var propName in value) {
                    if(propName === 'marksObtained'){
                        if (value[propName] === null || value[propName] === undefined || value[propName] == '') {
                            delete bulkData[index];
                        }
                    }
                }
            });
            bulkData.forEach((value, index) => {
                bulkData[index] = clean(value);
            });
            bulkData =  _.filter(bulkData,v => _.keys(v).length !== 0);
            submitQuizResultBulk();
        }else{
            swal(
                messages.error,
                messages.pleaseFillTotalMarksQuizDate,
                'error'
            )
        }
    }else{
        swal(
            messages.error,
            messages.pleaseSelectSubject,
            'error'
        )
        return false;
    }
});

$('#quiz-results-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '')+'&'+$('#quiz-results-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
    console.log(data,'data')
    showProgressBar(submitLoader);
    resetMessages();
    submitQuizResult();
});
