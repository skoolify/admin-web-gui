var mobileNumber = '';
var editing = false;

$(document).ready(function(){
    $("#parentCNIC").on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
     });;
    getAjaxSchools();
})

$(document).on('click','.clear-data',function(){
    $('#searchMobileNumber').val('');
    if(tableData){
        tableData
          .clear()
          .draw();
      }
})

$(document).on('blur','#searchMobileNumber', function(){
    $('#searchParentName').val('');
})

function getOTPDetails(){
    console.log('junaid')
    let mobileNumber = $('#searchMobileNumber').val();
    if(mobileNumber !== ''){
        data.mobileNumber = mobileNumber
    }
    if(branchID !== ''){
        data.branchID = branchID
    }else{
        data.branchID = $('#filter-branches').val()
    }
    if(schoolID !== ''){
        data.schoolID = schoolID
    }
    let editHiddenField = $('#editHiddenField');
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax': {
            url: herokuUrl+'/getOTPDetails',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                if(response.length >= 1){
                    parentRecord = response[0];
                    var keys = Object.keys(parentRecord);
                    if(editing){
                        keys.forEach((value, index) => {
                            submitButton.attr('disabled', true);
                            $('[name='+value+']').val(parentRecord[value]);
                            $('[name='+value+']').attr('disabled', true);
                            if(value.includes('ID')){
                                if(typeof value !== 'undefined'){
                                    window[value] = parentRecord[value];
                                }
                            }
                        });
                    }
                }
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'parentName': response[i].parentName,
                        'mobileNumber' : response[i].mobileNumber,
                        'otp' : response[i].otpString,
                        'action' : `<button class="btn btn-sm btn-primary viewBtn" data-json='${handleApostropheWork(response[i])}'>${messages.view} <i class="fa fa-eye"></i></button>`
                    })
                }
                // disableButtonsToggle();
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getOTPDetails')
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 404){
                   $('.dataTables_empty').text(messages.noDataAvailable);
                }
                if (xhr.status === 500) {
                }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'parentName'},
            {'data': 'mobileNumber'},
            {'data': 'otp'},
            {'data': 'action'}
        ]
    });
}

$(document).on('click','.viewBtn',function(){
    var otpData = JSON.parse($(this).attr('data-json'))
    let otpToken = $('#otpToken')
    let otpDevice = $('#otpDevice')
    let otpAppVersion = $('#otpAppVersion')
    let otpTokenStatus = $('#otpTokenStatus')
    let otpOsVersion = $('#otpOsVersion')
    let otpSourceUUID = $('#otpSourceUUID')
    // otpToken.html(`<button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" data-placement="top" title="${otpData.deviceToken}">
    // View</button>`)
    otpToken.text(otpData.deviceToken)
    otpDevice.text(otpData.deviceType)
    otpAppVersion.text(otpData.appVersion)
    // otpTokenStatus.text(otpData.TokenStatus)
    otpTokenStatus.html(`<button class="btn btn-sm ${otpData.TokenStatus && otpData.TokenStatus === 'Active' ? 'btn-success' : 'btn-danger'}">${otpData.TokenStatus}</button>`)
    otpOsVersion.text(otpData.osVersion)
    otpSourceUUID.text(otpData.sourceUUID)
    $('#viewOtpDetailModal').modal('show');
});


$('.search-otp').on('click', function(){
    getAjaxCities(countryID);
    let mobileNumber = $('#searchMobileNumber').val();
    if(mobileNumber === ''){
        swal(
            messages.error,
            messages.pleaseEnterMobileNumber,
            'error'
        )
        return false;
    }
    if( mobileNumber.length > 0 && mobileNumber[0] !== '+'){
        $('.phone-error').html(messages.pleaseEnterMobileNoWithCountryCodePrecededWithSign);
        $('.phone-error').show(200)
        $(this).addClass('phone-imput-error').focus()
        $('#searchMobileNumber').css("color", "#dd4b39")
        $(this).css("border-color","#00a65a")
        return false;
    }else{
        $(this).removeClass('phone-imput-error')
        $('#searchMobileNumber').removeAttr('style')
        $('#phoneNumberLabel').css("color", "#00a65a")
        $('.phone-error').hide(200);
        $('.phone-error').html('');
    }
        getOTPDetails();
});

$('#filter-form').on('submit', function(e){
    e.preventDefault();
    return false;
})

$('#reset-add-form').on('click', function(){
    if(parentRecord){
        var keys = Object.keys(parentRecord);
        keys.forEach((value, index) => {
            $('[name='+value+']').removeAttr('disabled');
        });
        parentRecord = null;
        submitButton.removeAttr('disabled');
        cancelButton.removeAttr('disabled');
    }
});
