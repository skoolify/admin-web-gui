$(document).ready(function(){
    getAjaxSchools();
})

$(document).on('click','.isActive',function(){
    if($(this).prop('checked')){
        $('#isActiveInput').val(1)
    }else{
        $('#isActiveInput').val(0)
    }
})

function iterateStudentListTableForAcademic(){
    var academicAddTable = $('#student-result-table tbody');
    $.ajax({
        url: apiUrl + '/students',
        method: 'GET',
        beforeSend: function () {
            showProgressBar('p2-students-result-list');
        },
        data: {
            classID: classID,
            branchID: branchID,
            schoolID: schoolID,
            classLevelID: classLevelID,
            classSection: classSection
        },
        success: function (res) {
            if (!res.error) {
                academicAddTable.empty();
                if(res.data.length){
                    $.each(res.data, function(index, value){
                        let addedResult;
                        if(academicList){
                            addedResult = academicList.find(o => o.studentID === value.studentID);
                        }
                        if(addedResult !== undefined && addedResult !== ''){
                            academicAddTable.append(`
                            <tr>
                            <td style="display:none;">
                                <input class="form-control" name="examSubjectResultID" type="text" value=' ${ addedResult.examSubjectResultID } '>
                            </td>
                            <td style="color:#188ae2;font-weight:bold; text-align:center">${index + 1}</td>
                            <td>${value.rollNo}</td>
                            <td style="display:none;">
                                <input class="form-control" name="studentID" type="text" value=' ${ value.studentID } '>
                            </td>
                            <td>${value.studentName }</td>
                            <td>
                                <input class="form-control" name="obtainedMarks" type="text" value="${addedResult.obtainedMarks}">
                            </td>
                            <td>
                                    <input class="form-control" name="grade" type="text" value="${addedResult.grade != '' && addedResult.grade != null && addedResult.grade != 'undefined' ? addedResult.grade : ''}">
                            </td>
                            <td>
                                <textarea class="form-control" name="notes" type="text" value="">${addedResult.notes != '' && addedResult.notes != null && addedResult.notes != 'undefined' ? addedResult.notes : ''}</textarea>
                            </td>
                            </tr>`);
                        }else{
                            academicAddTable.append(`
                            <tr>
                                <td style="display:none;">
                                    <input class="form-control" name="examSubjectResultID" type="text" value='0'>
                                </td>
                                <td style="color:#188ae2;font-weight:bold;text-align:center;">${index + 1}</td>
                                <td>${value.rollNo}</td>
                                <td style="display:none;">
                                    <input class="form-control" name="studentID" type="text" value=' ${ value.studentID } '>
                                </td>
                                <td>${value.studentName }</td>
                                <td>
                                    <input class="form-control" name="obtainedMarks" type="text">
                                </td>
                                <td>
                                    <input class="form-control" name="grade" type="text">
                                </td>
                                <td>
                                <textarea class="form-control" name="notes" type="text" value=""></textarea>
                                </td>
                            </tr>`);
                        }
                        });

                    hideProgressBar('p2-students-result-list');
                }else{
                    academicAddTable.empty();
                    academicAddTable.append(`
                        <tr>
                            <td colspan="6" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)

                    hideProgressBar('p2-students-result-list');
                }

                } else {
                    academicAddTable.empty();
                    academicAddTable.append(`<tr>
                            <td colspan="6" class="text-center"><strong style="color:#188ae2;font-weight:bold">${messages.noRecordsFound}</strong></td>
                        </tr>`)
                    hideProgressBar('p2-students-result-list');
                }

        },
        error: function (err) {
            invokedFunctionArray.push('iterateStudentListTableForAcademic');
            window.requestForToken(err)
            console.log(err);
            hideProgressBar('p2-students-result-list');
        }
    })
}

function submitFeeType(){
    console.log(data)
    // return
    $.ajax({
        url: herokuUrl+'/setFeeTypes',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                $('form#fee-type-form').trigger('reset');
                getFeeTypes();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitFeeType');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getFeeTypes(){
    data = {}
    schoolID = $('#filter-schools').val()
    branchID = $('#filter-branches').val()
    if(schoolID !== '' && schoolID !== 'undefined' && schoolID !== undefined){
        data.schoolID = schoolID
    }else{
        data.schoolID = user.schoolID
    }
    if(branchID !== '' && branchID !== 'undefined' && branchID !== undefined){
        data.branchID = branchID
    }else{
        data.branchID = user.branchID
    }
    // if (tableData) {
    //     tableData.destroy();
    // }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getFeeTypes',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'feeType': response[i].feeType,
                        'taxRate': response[i].taxRate,
                        'discount': response[i].discountRate,
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'isRecurring' : `<label style="display: block; text-align: right;" class="text-center center m-auto label label-sm label-${response[i].isRecurring ? 'success' : 'danger'}"> ${response[i].isRecurring ? 'Yes' : 'No'}</label>`,
                        'action' : `<div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `<button class="btn-danger btn btn-xs delete-fee-type" data-fee-type-id='${response[i].feeTypeID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}</div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getFeeTypes');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'feeType'},
            {'data': 'taxRate'},
            {'data': 'discount'},
            {'data': 'isActive'},
            {'data': 'isRecurring'},
            {'data': 'action'}
        ]
    });

}

$(document).on('click', '.delete-fee-type', function(){
    feeTypeID = $(this).attr('data-fee-type-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteThisFeeType+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteFeeType();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteFeeType(){
    $.ajax({
        url: herokuUrl+'/deleteFeeType',
        method: 'POST',
        headers,
        data: {
            feeTypeID: feeTypeID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.feeTypeHasBeenDeleted,
                    messages.deletedMessage,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                feeTypeID = '';
                if(tableData){
                    tableData.destroy();
                }
                getFeeTypes();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteFeeType');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.oops,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$(document).on('change','#isRecurring',  function(){
        var feeTypeValue = $('#feeType').val();
        var isEdit = Number($('#editHiddenField').val());
        if($('#isRecurring').prop("checked") === false && isEdit > 0 ){
            $.confirm({
                title: messages.confirmMessage,
                content: messages.areYouSure+' <br/> <b>'+feeTypeValue+'</b> '+messages.forAllStudentsWillBeRemovedFromRecurringList,
                buttons: {
                    confirm: function () {
                        $('#isRecurring').prop("checked", false)
                    },
                    cancel: function () {
                        $('#isRecurring').prop("checked", true)
                    },
                },
            })
        }
});

$('#fee-type-form').on('submit', function(e){
    e.preventDefault();
    var isRecurring = 0;
    if($('#isRecurring').prop("checked") == true){
        isRecurring = 1;
    }
    data = $('#filter-form').serialize()+'&'+$('#fee-type-form').serialize()+'&isRecurring='+isRecurring;
    if($('#filter-schools').val() !== ''){
        data['schoolID'] = $('#filter-schools').val()
    }else{
        swal(
            messages.pleaseSelectSchool,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#filter-branches').val() !== ''){
        data['branchID'] = $('#filter-branches').val()
    }else{
        swal(
            messages.pleaseSelectBranch,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#feeType').val() !== ''){
        data['feeType'] = $('#feeType').val()
    }else{
        swal(
            messages.feeTypeIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    showProgressBar(submitLoader);
    resetMessages();
    submitFeeType();
});
