$(document).ready(function(){
    getAjaxSchools();
    // getCommRequests();
})

function getCommRequests(){

    let data = {};
    let route = '';
    if(user.isAdmin === 1){
        classLevelID = $('#filter-classLevels').val();
        classID = $('#filter-classes').val();
        route = '/getCommRequest';
        if(schoolID != ''){
            data.schoolID = schoolID;
        }

        if(branchID != ''){
            data.branchID = branchID;
        }

        if(classLevelID !== '' && classLevelID !== null){
            data.classLevelID = classLevelID
        }

        if(classID !== '' && classID !== null){
            data.classID = classID;
        }

        if(studentID != ''){
            data.studentID = studentID;
        }
    }else{
        classID = $('#filter-classes').val();
        route = '/getUserCommunications';
        data.userID = user.userID
        data.branchID = user.branchID
        if(classLevelID != ''){
            data.classLevelID = classLevelID
        }

        if(classID != ''){
            data.classID = classID;
        }

        if(studentID != ''){
            data.studentID = studentID;
        }

    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+route,
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    let type = response[i].commType;
                    let lab = {};
                    if(type === 'A'){
                        lab.label = 'success';
                        lab.text = messages.appreciation;
                        lab.des = messages.appreciation;
                    }

                    if(type === 'C'){
                        lab.label = 'danger';
                        lab.text = messages.complaints;
                        lab.des = messages.complaints;
                    }

                    if(type === 'P'){
                        lab.label = 'primary';
                        lab.text = messages.PTM;
                        lab.des = messages.parentTeacherMeeting;
                    }

                    if(type === 'L'){
                        lab.label = 'warning';
                        lab.text = messages.leaveRequest;
                        lab.des = messages.leaveRequest;
                    }

                    return_data.push({
                        'sNo' : i+1,
                        'studentName':`<span class="${response[i].isRead == 0 ? 'bold-class'  : 'normal-class'}">${response[i].studentName}</span>`,
                        'classSection':`<span class="${response[i].isRead == 0 ? 'bold-class'  : 'normal-class'}">${response[i].classAlias } - ${response[i].classSection} </span>`,
                        'parentName': `<span class="${response[i].isRead == 0 ? 'bold-class'  : 'normal-class'}">${response[i].parentName}</span>`,
                        'parentType' : `<span class="${response[i].isRead == 0 ? 'bold-class'  : 'normal-class'}">${populateParentTypeAbbrevation(response[i].parentType)}</span>`,
                        'commType' : `<span class="${response[i].isRead == 0 ? 'bold-class'  : 'normal-class'}"><label class="label label-sm label-${lab.label}" data-toggle="tooltip" title="${lab.des}"> ${lab.text}</label></span>`,
                        'insertionDate' : `<span class="${response[i].isRead == 0 ? 'bold-class'  : 'normal-class'}"><span style="display:none">${Date.parse(response[i].insertionDate)}</span>${moment(moment.utc(response[i].insertionDate).toDate()).format(__DATEFORMAT__)}</span>`,
                        'action' : `<button class="btn-primary btn btn-xs view-message" data-toggle="tooltip" title="${messages.view}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-eye"></i> ${messages.READ}</button>`,
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getCommRequests');
                window.requestForToken(xhr)
                console.log(xhr);
                if (xhr.status === 404) {
                    $('.dataTables_empty').html(messages.noDataAvailable);
                 }
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'studentName'},
            {'data': 'classSection'},
            {'data': 'parentName'},
            {'data': 'parentType'},
            {'data': 'commType'},
            {'data': 'insertionDate'},
            {'data': 'action'}

        ]
    });

}
function populateParentTypeAbbrevation(shortForm){
    switch(shortForm) {
        case 'F':
            return messages.father;
            break;
        case 'M':
            return messages.mother;
            break;
        case 'G':
            return messages.guardian;
            break;
        default:
            return messages.unknown;
    }

}
function populateMessageTypeAbbrevation(shortForm){
    switch(shortForm) {
        case 'A':
            return messages.appreciation;
            break;
        case 'C':
            return messages.complaints;
            break;
        case 'P':
            return messages.PTM;
            break;
        case 'L':
            return messages.leaveRequest;
            break;

        default:
            return messages.unknown;
    }

}
$(document).on('click', '.view-message', function(){
    $(this).closest('tr').find('span').removeClass('bold-class');
    var replyMessage;
    var json  = JSON.parse($(this).attr('data-json'));
    data = {};
    if(data.communicationListID != ''){
        data.communicationListID = json.communicationListID
    }
    if(data.schoolID != ''){
        data.schoolID = json.schoolID
    }
    if(data.branchID != ''){
        data.branchID = json.branchID
    }
    data.status = 1;
    submitsetCommsIsRead();
    data = {};
    const toast = swal.mixin({
        toast: true,
        position: 'center-end',
        showConfirmButton: false,
        timer: 3000,
    });
    swal.mixin({
        confirmButtonText: messages.reply+'  &rarr;',
        cancelButtonText: messages.unread,
        showCancelButton: true,
        cancelButtonColor: '#d33',
        showCloseButton: true,
        progressSteps: ['1', '2']
        }).queue([
        {
            title: messages.message,
            html: `<p>${json.commText}</p>`,
        },
        {
            title: messages.replyToMessage,
            input: 'textarea',
            confirmButtonText: messages.send,
            showCancelButton: false,
            inputPlaceholder: messages.enterTextMessage,
            inputAttributes: {
                maxlength: 500,
                autocapitalize: 'off',
                autocorrect: 'off'
            }
        }
        ]).then((result) => {
            if(result.dismiss == 'cancel'){
                data = {};
                if(data.communicationListID != ''){
                    data.communicationListID = json.communicationListID
                }
                if(data.schoolID != ''){
                    data.schoolID = json.schoolID
                }
                if(data.branchID != ''){
                    data.branchID = json.branchID
                }
                data.status = 0;
                submitsetCommsIsRead();
            }
            if (result.value) {
            replyMessage = result.value[1];
            if(replyMessage === ''){
                toast({
                        type: 'error',
                        title: messages.pleaseEnterTextMessages
                    })
                    return false;
            }else{
                json['messageText'] = replyMessage;
                json['messageType'] = 'M';
                json['userID'] = user.userID;
                delete json['commText'];
                data = json;
                var bulkArray = [];
                bulkArray.push(data);
                submitMessage(bulkArray);
            }
        }
    });
    $('.swal2-close').text('x');
});

function submitMessage(bulkArray){
    bulkArray[0]['countryID'] = countryID;
    bulkArray[0]['notificationOnly'] = 0;
    delete bulkArray[0]["communicationListID"];
    delete bulkArray[0]["studentName"];
    delete bulkArray[0]["parentID"];
    delete bulkArray[0]["parentName"];
    delete bulkArray[0]["classLevelName"];
    delete bulkArray[0]["classSection"];
    delete bulkArray[0]["parentType"];
    delete bulkArray[0]["commType"];
    delete bulkArray[0]["isRead"];
    delete bulkArray[0]["insertionDate"];
    delete bulkArray[0]["classAlias"];
    $.ajax({
        url: herokuUrl+'/setMessagesArray',
        method: 'POST',
        headers,
        data:{'array':bulkArray},
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.messageHasBeenSent,
                    messages.success,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function submitsetCommsIsRead(){
    $.ajax({
        url: herokuUrl+'/setCommsIsRead',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                if(data.status == '0'){
                    swal(
                        messages.messageHasBeenMarkedAsUnread,
                        messages.success,
                        'success',{
                            buttons:true,//The right way
                            buttons: messages.ok, //The right way to do it in Swal1
                        }
                    )
                    if(tableData){
                        tableData.destroy();
                    }
                    getCommRequests();
                }
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitsetCommsIsRead');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
