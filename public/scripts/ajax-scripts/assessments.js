var assessment = null;
var assessmentDetailErrors = $('#assessment-details-form-errors');
var bulkData = new Array();
var assessmentDate = moment().format('YYYY-MM-DD');
$(document).ready(function(){
    getAjaxSchools();
    //getAssessments();
})

function submitAssessment(){
    let data = {}
    data.classID = classID;
    data.studentID = studentID;
    data.assessmentDate = $('#assessmentDate').val();
    data.assessmentNotes = $('#assessmentNotes').val();

    let isPublish = '1';
    if($('#activeField2').is(':checked')){
        isPublish = '0';
    }

    if($('#activeField1').is(':checked')){
        isPublish = '1';
    }

    data.isPublish = isPublish;
    if(editHiddenField.attr('name') && editHiddenField.val() != ''){
        data.assessmentID = editHiddenField.val();
    }
    $.ajax({
        url: herokuUrl+'/setAssessment',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                getAssessments();
                $('form#assessments-form').trigger('reset');
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitAssessment');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function submitAssessmentsBulk(){
    var bulkLoader = $('#bulk-assessments-loader');
    var submitBtn = $('#submit-add-assessments');
    var cancelBtn = $('#cancel-add-assessments');

    for (let i = 0; i < bulkData.length; i++) {
        let data = bulkData[i];
        $.ajax({
            url: herokuUrl+'/setAssessment',
            method: 'POST',
            headers,
            data,
            beforeSend: function(){
                disableButtonsToggle();
                showProgressBar(submitLoader);
            },
            success: function(res){
                if(res.response === 'success'){
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            },
            error: function(xhr){
                invokedFunctionArray.push('submitAssessmentsBulk');
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 400){
                    addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
                }
                hideProgressBar(submitLoader);
                disableButtonsToggle();
            }
        });
    }

    bulkLoader.addClass('hidden');
    submitBtn.removeAttr('disabled');
    cancelBtn.removeAttr('disabled');

    swal(
        messages.success,
        messages.successfullySubmitted,
        'success'
    )

    tableData.destroy();
    getAssessments();
    $('form#bulk-assessments-form').trigger('reset');
}

function submitAssessmentDetail() {
    $.ajax({
        url: herokuUrl+'/setAssessmentDetails',
        method: 'POST',
        headers,
        data: {
            assessmentID: assessment.assessmentID,
            nonSubjectID: nonSubjectID,
            gradeNS: $('#gradeNS').val(),
            notesNS: $('#notesNs').val()
        },
        beforeSend: function(){
            disableAssessmentDetailsButtons();
            showProgressBar('assessment-submit-loader');
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                assessmentDetailErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                tableData.destroy();
                $('form#assessments-detail-form').trigger('reset');
                getAssessments();
            }
            hideProgressBar('assessment-submit-loader');
            hideProgressBar(submitLoader);
            disableAssessmentDetailsButtons();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitAssessmentDetail');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                assessmentDetailErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar('assessment-submit-loader');
            hideProgressBar(submitLoader);
            disableAssessmentDetailsButtons();
        }
    });
}

function getAssessments(){
    let data = {};
    if(classID !== ''){
        data.classID = classID
    }

    if(studentID !== ''){
        data.studentID = studentID
    }

    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/getAssessment',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                if(response.length >= 1){
                    assessment = response[response.length - 1];
                }
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'nonSubjectName': response[i].nonSubjectName ? response[i].nonSubjectName : '---',
                        'notesNS' : response[i].notesNS ? response[i].notesNS : '---',
                        'gradeNS' : response[i].gradeNS ? response[i].gradeNS : '---',
                        'assessmentNotes' : response[i].assessmentNotes,
                        'assessmentDate' : moment(response[i].assessmentDate).format(__DATEFORMAT__),
                        'action' : `<div class="btn-group"><button class="btn-success btn btn-xs add-assessment-detail" data-toggle="tooltip" title="${messages.addAssessmentDetail}" data-assessment-id='${response[i].assessmentID}' data-grade-ns='${response[i].gradeNS ? response[i].gradeNS : ''}' data-notes-ns='${response[i].notesNS ? response[i].notesNS : ''}'><i class="fa fa-file"></i></button>${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${JSON.stringify(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}</div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push('getAssessments');
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'nonSubjectName'},
            {'data': 'notesNS'},
            {'data': 'gradeNS'},
            {'data': 'assessmentNotes'},
            {'data': 'assessmentDate'},
            {'data': 'action'}
        ]
    });

}

function disableAssessmentDetailsButtons(){
    if($('#submit-assessment-detail-container').prop('disabled')){
        $('#submit-assessment-detail-container').removeAttr('disabled');
    }else{
        $('#submit-assessment-detail-container').attr('disabled', true);
    }
    if($('#hide-assessment-detail-container').prop('disabled')){
        $('#hide-assessment-detail-container').removeAttr('disabled');
    }else{
        $('#hide-assessment-detail-container').attr('disabled', true);
    }
}

$('#add-add-assessments').on('click', function(){
    $('#results-list').slideUp();
    $('#add-assessments-table').removeClass('hidden').slideDown();
    $('form#bulk-assessments-form').trigger('reset');
    students.attr('disabled', true);
    $('#filter-students').closest('.col-md-3').css('display', 'none');
});

$('#cancel-add-assessments').on('click', function(){
    $('#add-assessments-table').slideUp();
    $('#results-list').slideDown();
    $('form#bulk-assessments-form').trigger('reset');
    students.removeAttr('disabled');
    $('#filter-students').closest('.col-md-3').css('display', 'block');
});

$('#submit-add-assessments').on('click', function(){
    let bulkAssessmentsResults = chunkArray($('#bulk-assessments-form').serializeArray(), 2);
    bulkData = new Array();
    let rowLength = $('#assessments-add-tbody tr').length;
    let assessmentDate = $('#assessmentDate').val();
    let pass = true;
    if(assessmentDate !== ''){
        //let subjectID = $('#filter-subjects').val();
        for(var i = 0; i < rowLength; i++){
            let temp = {};
            bulkAssessmentsResults[i].forEach((res, index) => {
                if(res.value !== ''){
                    temp[res.name] = res.value;
                }else{
                    swal(
                        messages.error,
                        messages.pleaseFillAllNotes,
                        'error'
                    )
                    pass = false;
                }
                if(res.name === 'studentID'){
                    if($('#isPublish'+res.value).is(":checked")){
                        temp.isPublish = '1';
                    }else{
                        temp.isPublish = '0';
                    }
                }
            });
            temp.assessmentDate = assessmentDate;
            temp.classID = classID;
            bulkData.push(temp);
        }
        if(pass){
            submitAssessmentsBulk();
        }
    }else{
        swal(
            messages.error,
            messages.pleaseFillAssessmentDate,
            'error'
        )
    }

    return false;

});

$('#assessments-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize()+'&'+$('#assessments-form').serialize();
    showProgressBar(submitLoader);
    resetMessages();
    submitAssessment();
});

$('#assessments-detail-form').on('submit', function(e){
    e.preventDefault();
    data = $(this).serialize();
    showProgressBar(submitLoader);
    resetMessages();
    submitAssessmentDetail();
});


$(document).on('click', '.add-assessment-detail', function(){
    let assessmentID = $(this).attr('data-assessment-id');
    let gradeNS = $(this).attr('data-grade-ns');
    let notesNs = $(this).attr('data-notes-ns');
    $('#gradeNS').val(gradeNS);
    $('#notesNs').val(notesNs);
    $('#hiddenAssessmentID').attr('name', 'assessmentID');
    $('#hiddenAssessmentID').val(assessmentID);
    hideResultsList();
    $('#assessment-detail-form-container').slideDown();
});


$('#hide-assessment-detail-container').on('click', function(){
    $('#hiddenAssessmentID').removeAttr('name');
    $('#hiddenAssessmentID').val('');
    $('#assessment-detail-form-container').slideUp();
    showResultsList();
})
