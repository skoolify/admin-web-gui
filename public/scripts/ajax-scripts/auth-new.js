var validated = false;
var loggedIn = false;
var validateBtn = $("#validateUser");
var loginBtn = $("#login");
var domain = __DOMAIN__;
var __REPROCESS__ = false;
var __RETRIES__ = 0;
var __TOKPROCESS__ = true;
var invokedFunctionArray = [];
var __DATA__ = null;

$(function(){

    $(document).ready(function(){
        if(typeof resetLastLink !== undefined && resetLastLink){
            //localStorage.removeItem('last-link');
        }
        getLogo(domain);
    });

    $('#login-form').on('submit', function(e){
        $("#error-message").hide();
        e.preventDefault();
        let mobileVal;
        if($('#mobileNo').length){
            mobileVal = $('#mobileNo').val().replace(' ', '');
        }else{
            mobileVal = $('.mobile-no-validation').val().replace(' ', '');
        }
        mobileVal = mobileVal.replace(/[{()}]/g, '');
        mobileVal = mobileVal.replace(/-/g, '');
        $('#hiddenMobileNo').val(mobileVal);
        window.__DATA__ = $(this).serialize()+'&domain='+domain;
        if(!validated){
            validate();
        }else{
            login();
        }
    })
})

function validate(data = null){
    $.ajax({
        url: __BASE__+'auth/validate',
        method: 'POST',
        data: window.__DATA__,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(){
            validateBtn.attr('disabled', true);
            validateBtn.html(messages.validating);
        },
        success: function(res){
            if(!res.error){

                    $("#login-password-container").fadeIn(500);
                    validateBtn.hide();
                    loginBtn.fadeIn();
                    validateBtn.attr('type', 'button');
                    validateBtn.removeAttr('disabled', false);
                    validateBtn.html(messages.next);
                    validated = true;
                    $('#password').focus();

            }else{
                window.invokedFunctionArray.push('validate')
                window.requestForToken(res)
                if(res.code !== 503){
                    validateBtn.removeAttr('disabled', false);
                    validateBtn.html(messages.next);
                    $("#error-message").show().html(`<p>${res.message}</p>`);
                }
                $('#password').focus();
            }
        },
        error: function(err){
            validateBtn.removeAttr('disabled', false);
            validateBtn.html(messages.next);
            window.invokedFunctionArray.push('validate')
            window.requestForToken(err)
            $("#error-message").show().html(messages.sorryThereIsSomethingWrongPleaseTryAgain);
        }
    })
}

function login(data = null){
    $.ajax({
        url: __BASE__+'auth/login',
        method: 'POST',
        data: window.__DATA__,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(){
            loginBtn.attr('disabled', true);
            loginBtn.html(messages.loggingIn);
        },
        success: function(res){
            if(!res.error){
                if(res.response && res.response.lastLoginDate === null){

                        let obj = {
                            userID: res.response.userID
                        };
                        localStorage.setItem('user', JSON.stringify(obj))
                        window.location.replace(__BASE__+'/auth/password?f=1');
                        return false;
                    }else{
                        getUserDetail();
                    }

            }else{
                if(res.code !== 503){
                    loginBtn.removeAttr('disabled', false);
                    loginBtn.html(messages.next);
                    $("#error-message").show().html(`<p>${res.message}</p>`);
                }
                window.invokedFunctionArray.push('login')
                window.requestForToken(res)
            }
        },
        error: function(err){
            loginBtn.removeAttr('disabled', false);
            loginBtn.html(messages.next);
            window.invokedFunctionArray.push('login')
            window.requestForToken(err)
            $("#error-message").show().html(messages.sorryThereIsSomethingWrongPleaseTryAgain);
        }
    })
}

function getUserDetail(){
    $.ajax({
        url: __BASE__+'auth/user/detail',
        method: 'GET',
        success: function(res){
            if(!res.error){
                var branchTemp = false;
                var roleTemp = false;
                if(res.meetingDetails && res.meetingDetails !== null){
                    meetingDetails = res.meetingDetails
                    localStorage.setItem('meetingDetails', JSON.stringify(meetingDetails));
                }
                if(res.response){
                    response = res.response[0]
                    if(response.messageAlert !== null){
                        paymentMessageAlert = response.messageAlert
                        localStorage.setItem('paymentMessageAlert', JSON.stringify(paymentMessageAlert));
                    }
                    if(response.isAccepted === 0 && response.IsSignatory === 1 && response.contactExists === 1){
                        localStorage.setItem('isAccepted', res.response[0].isAccepted);
                        localStorage.setItem('user', JSON.stringify(res.response));
                        loginBtn.html('Redirecting...');
                        getSchoolContract(response.userID)
                        return false
                    }else if(response.isAccepted === 0 && response.IsSignatory === 0 && response.contactExists === 1){
                        localStorage.setItem('toast', JSON.stringify(messages.theOnlineContractAcceptanceIsPendingPleaseAsk+''+response.superUserName+' '+messages.toLoginAndAcceptTheContract));
                    }else if(response.isBlocked === 1 && response.feePaymentAllowed === 0){
                        $("#error-message").show().html(response.messageAlert);
                            loginBtn.removeAttr('disabled', false);
                            loginBtn.html(messages.login);
                            return false;
                    }else{
                        if(res.response.branchID !== null){
                            branchTemp = true;
                        }
                        if(res.response.roleID !== null){
                            roleTemp = true;
                        }

                        if(!branchTemp){
                            $("#error-message").show().html(messages.sorryThereIsNoBranchAssignedToYou);
                            loginBtn.removeAttr('disabled', false);
                            loginBtn.html(messages.login);
                            return false;
                        }

                        if(!roleTemp){
                            $("#error-message").show().html(messages.sorryThereIsNoRoleAssignedToYou);
                            loginBtn.removeAttr('disabled', false);
                            loginBtn.html(messages.login);
                            return false;
                        }
                    }
                }else{
                    if(res.response.branchID == null){
                        $("#error-message").show().html(messages.sorryThereIsNoBranchAssignedToYou);
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html(messages.login);
                        return false;
                    }

                    if(!res.response.roleID){
                        $("#error-message").show().html(messages.sorryThereIsNoRoleAssignedToYou);
                        loginBtn.removeAttr('disabled', false);
                        loginBtn.html(messages.login);
                        return false;
                    }
                }
                localStorage.setItem('user', JSON.stringify(res.response));
                loginBtn.html(messages.redirecting);
                // if(response.isBlocked === 1 && response.feePaymentAllowed === 1){
                //     console.log('junaid')
                //     loginBtn.html('Redirecting...');
                //     window.location.replace(__BASE__+'/school-payment');
                //     return
                // }
                // console.log(__BASE__)
                // return
                window.location.replace(__BASE__+'auth/authorization');
            }else{
                loginBtn.removeAttr('disabled', false);
                loginBtn.html(messages.next);
                window.invokedFunctionArray.push('getUserDetail')
                window.requestForToken(res)
                $("#error-message").show().html(`<p>${res.message}</p>`);
            }
        },
        error: function(err){
            loginBtn.removeAttr('disabled', false);
            loginBtn.html(messages.next);
            window.invokedFunctionArray.push('getUserDetail')
            window.requestForToken(err)
            $("#error-message").show().html(messages.sorryThereIsSomethingWrongPleaseTryAgain);
        }
    })
}

function getSchoolContract(userID){
    $.ajax({
        url: __BASE__+'auth/get/contract',
        method: 'POST',
        data: {
            "_token": __CSRF__,
            userID:userID
        },
        success: function(res){
            if(!res.error){
                response = res.response
                var keys = Object.keys(response);
                var obj = {}
                keys.forEach((key,index)=>{
                    if(key === 'contractText' || key === 'chargesText' || key === 'collectionText'){
                        obj[key] = atob(response[key])
                    }else{
                        obj[key] = response[key]
                    }
                })
                localStorage.setItem('contractData', JSON.stringify(obj))
                window.location.replace(__BASE__+'auth/contract-acceptance');
            }else{
                $("#error-message").show().html(messages.sorryThereIsNoContractDetails);
                loginBtn.removeAttr('disabled', false);
                loginBtn.html(messages.login);
                return false;
            }
        },
        error: function(err){
            window.invokedFunctionArray.push('getSchoolContract')
            window.requestForToken(err)
        }
    })
}

function getLogo(domain){
    $.ajax({
        url: __BASE__+'school-logo',
        method: 'POST',
        data: {
            "_token": __CSRF__,
            domain
        },
        success: function(res){
            if(!res.error){
                let logo = res.response.schoolLogo.indexOf('base64,') > 0 ? res.response.schoolLogo : 'data:image/*;base64,'+res.response.schoolLogo;
                $('#schoolLogo').attr('src', logo).addClass('animated flipInY');
                if(res.response.maintenanceFlag == 1){
                    $('.field-maintain').css('display', 'none');
                    $('.field-unmaintain').css('display', 'block');
                }

                $('#maintenance-text').text(`${res.response.maintenanceText ? res.response.maintenanceText : ''}`).css('color', 'white');
            }else{
                window.invokedFunctionArray.push('getLogo')
                window.requestForToken(res)
            }
        },
        error: function(err){
            window.invokedFunctionArray.push('getLogo')
            window.requestForToken(err)
        }
    })
}

function requestForToken(xhr){
    if (xhr.code === 503 || xhr.status === 503) {
        window.__REPROCESS__ = true;
        window.__RETRIES__ = window.__RETRIES__ + 1;
        window.getToken();
    }
}

function getToken(){
    let token = localStorage.getItem('apiToken');
    if (token !== null) {
        updateTockenOnExpiry();
    }
}
function updateTockenOnExpiry(){
    if(window.__TOKPROCESS__ === true){
        $.ajax({
            url: __BASE__ + 'ajax/updateApiToken',
            beforeSend: function(){
                window.__TOKPROCESS__ = false;
            },
            method: 'GET',
            data: {
            },
            success: function (res) {
                if (res.response) {
                    window.token = res.response;
                    localStorage.removeItem('apiToken');
                    localStorage.setItem('apiToken', JSON.stringify(res.response));
                    window.__TOKPROCESS__ = true;
                    if (window.__REPROCESS__ === true ) {
                        if(window.__RETRIES__ !== 0){
                            let retriesArr = invokedFunctionArray.slice((0 - window.__RETRIES__));
                            for (let t = 0; t < retriesArr.length; t++) {
                                window[retriesArr[t]]();
                            }
                            window.__RETRIES__ = 0;
                        }
                    }
                }
            },
            error: function (err) {
                window.requestForToken(err)
                $("#error-message").show(messages.sorryThereIsSomethingWrongPleaseTryAgain);
            }
        })
    }
}

$('.switchLanguageBtn').on('click', function(){
    let lang = '';
    let form = $('#switch-language-form')
    lang = $(this).attr('data-value');
    $('#language').val(lang);
    form.submit();
})
