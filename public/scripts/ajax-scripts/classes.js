$(document).ready(function(){
    getAjaxSchools();
    // getClasses();
})

function submitClass(){
    $.ajax({
        url: herokuUrl+'/setClasses',
        method: 'POST',
        headers,
        data,
        beforeSend: function(){
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
                $('form#classes-form').trigger('reset');
                getClasses();
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        },
        error: function(xhr){
            invokedFunctionArray.push('submitClass');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                parseValidationErrors(xhr);
            }
            if(xhr.status === 400){
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function getClasses(){

    let route = 'getClasses';
    let data = {};

    data.schoolID = schoolID;
    data.branchID = branchID;
    if(classLevelID !== ''){
        if(__ROUTE__ !== 'classes'){
            data.classLevelID = classLevelID;
            classLevelID = '';
        }
        data.classLevelID = classLevelID;

    }

    if(classTeacherID != null && classTeacherID !== ''){
        data.classTeacherID = classTeacherID;
    }

    if(user.isAdmin != 1){
        data = {};
        data.userID = user.userID;
        data.classLevelID = classLevelID;
        route = 'getUserClasses';
    }
    if(tableData){
        tableData.destroy()
    }

    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        'ajax'       : {
            url: herokuUrl+'/'+route,
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = parseResponse(res);
                var return_data = new Array();
                for(var i = 0; i< response.length; i++){
                    return_data.push({
                        'sNo' : i+1,
                        'classAlias': response[i].classAlias,
                        'classSection' : response[i].classSection,
                        'classShift' : (response[i].classShift == 'M' ? 'Morning' : (response[i].classShift == 'A' ? 'Afternoon' : 'Evening')),
                        'TeacherName': response[i].TeacherName ? response[i].TeacherName : "-",
                        'isActive' : `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action' : `<div class="btn-group">
                        ${__ACCESS__ ? `<button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}'><i class="fa fa-pencil"></i></button>` : ''}
                        ${__DEL__ ? `
                        <button class="btn-danger btn btn-xs delete-section" data-section-id='${response[i].classID}' data-toggle="tooltip" title="${messages.delete}"><i class="fa fa-trash"></i></button>
                        ` : ''}</div>`
                    })
                }
                return return_data;
            },
            error: function(xhr){
                invokedFunctionArray.push(route);
                window.requestForToken(xhr)
                console.log(xhr);
            }
        },
        "columns"    : [
            {'data': 'sNo'},
            {'data': 'classAlias'},
            {'data': 'classSection'},
            {'data': 'classShift'},
            {'data': 'TeacherName'},
            {'data': 'isActive'},
            {'data': 'action'}
        ]
    });

}

$(document).on('click', '.delete-section', function(){
    classID = $(this).attr('data-section-id');
   $.confirm({
       title: messages.confirmMessage,
       content: messages.areYouSure+" <br/> "+messages.youWantToDeleteTheClass+"</br>",
       type: 'red',
       typeAnimated: true,
       buttons: {
           formSubmit: {
               text: messages.confirm,
               btnClass: 'btn-blue',
               action: function () {
                deleteClass();
               }
           },
            cancel: {
                text: messages.cancelBtn,
                action : function (){
                }
            }
       }
   });

})

function deleteClass(){
    $.ajax({
        url: herokuUrl+'/deleteClass',
        method: 'POST',
        headers,
        data: {
            classID: classID,
        },
        beforeSend: function(){
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.classHasBeenDeleted,
                    messages.delete,
                    'success',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                classID = '';
                // if(tableData){
                //     tableData.destroy();
                // }
                getClasses();
            }
        },
        error: function(xhr){
            invokedFunctionArray.push('deleteClass');
            window.requestForToken(xhr)
            if(xhr.status === 422){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 400){
                swal(
                    xhr.responseJSON.response.userMessage,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
            if(xhr.status === 500){
                swal(
                    xhr.responseJSON.reason,
                    messages.errorMessage,
                    'error',{
                        buttons:true,//The right way
                        buttons: messages.ok, //The right way to do it in Swal1
                    }
                )
                return false;
            }
        }
    });
}

$('#classes-form').on('submit', function(e){
    e.preventDefault();
    data = $('#filter-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '')+'&'+$('#classes-form').serialize();

    if($('#filter-classLevels').val() !== ''){
        data['classLevelID'] = $('#filter-classLevels').val()
    }else{
        swal(
            messages.pleaseSelectClass,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#classSection').val() !== ''){
        data['classSection'] = $('#classSection').val()
    }else{
        swal(
            messages.classSectionIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }

    if($('#classShift').val() !== ''){
        data['classShift'] = $('#classShift').val()
    }else{
        swal(
            messages.classShiftIsRequired,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
    showProgressBar(submitLoader);
    resetMessages();
    submitClass();
});
