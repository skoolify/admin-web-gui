var resetPasswordUserID = null;
var resetPasswordNewPassword = '12345678';
var usersArray = [];
var isFormOpen = false;
var allPages;
var oTable;
$(document).ready(function () {
    getAjaxSchools();
    if(__SCHOOLS__ != 1 && __BRANCHES__ != 1){
        getUserList();
    }
    // console.log(generateUUID());
    // console.log(generateUUID().length);
})

$('#show-add-form-container').on('click', function(){
    isFormOpen = true;
    $('#fullName').attr('disabled', false)
    $('#mobileNo').attr('disabled', false)
    $('#emailAddress').attr('disabled', false)
    $('#activeField1').attr('disabled', false)
    $('#activeField0').attr('disabled', false)
    $('#isAdmin1').attr('disabled', false)
    $('#isAdmin0').attr('disabled', false)
    $('#submit-add-form-container').attr('disabled', false)
    $('#mobileNo').attr('readonly', false)
    $('#checkIsSignatory').attr('disabled', false)
    $('#passwordInput').attr('disabled', false)
    $('#mobileNoLabel').addClass('hidden')
})

$('#hide-add-form-container').on('click',function(){
    getUserList();
    isFormOpen = false;

})

$(document).on('click', '.role-record-delete', function () {

    var roleLinkId = $(this).attr('data-id');
    swal({
        title: messages.areYouSureQuestion,
        text: messages.youWontBeAbleToRevertThisRole,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: messages.yesDeleteIt
      }).then((result) => {
        if (result.value) {
              $(this).parent().fadeOut(1000);
            $.ajax({
                url: herokuUrl + '/setDeAssignRoles',
                type: 'POST',
                headers,
                async : false,
                data: {
                    roleLinkID: roleLinkId
                },
                beforeSend: function () {
                    buttonGroup = ''
                },
                success: function (roleResponse) {

                    swal(
                        messages.deletedMessage,
                        messages.roleDeletedSuccessfully,
                        'success'
                      )

                },
                error:function(xhr){
                    console.log(xhr);

                },
            });
        }
      })
});

$('#checkIsSignatory').on('click',function(){
    if($('#checkIsSignatory').prop('checked')){
        $('#isSignatory').val(1);
    }else{
        $('#isSignatory').val(0);
    }
});

$('#feePaymentAllowedCheckbox').on('click',function(){
    if($('#feePaymentAllowedCheckbox').prop('checked')){
        $('#feePaymentAllowed').val(1);
    }else{
        $('#feePaymentAllowed').val(0);
    }
});

function setAssignRolesArray(bulkArray){
    $.ajax({
        url: herokuUrl + '/setAssignRolesArray',
        method: 'POST',
        headers,
        data:{'array': bulkArray},
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function(res){
            if(res.response === 'success'){
                swal(
                    messages.successMessage,
                    messages.rolesHaveBeenSuccessfullyAssigned
                );
                branchID = $('#filter-branches').val();
                getUserList();
                // alert('Role has been successfully assigned');
            }
            $('#selectAll').prop('checked', false)
            usersArray = [];
            hideProgressBar(submitLoader);
            disableButtonsToggle();

        },
        error: function (xhr) {
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                swal(
                    messages.error,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                );
                // addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if (xhr.status !== 400 && xhr.status !== 422) {
                swal(
                    messages.error,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                );
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

function submitUser(roles, selectBoxesValues, isEdit) {
    $.ajax({
        url: herokuUrl + '/setUser',
        method: 'POST',
        headers,
        data,
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            let { response } = res;
            if (response['userID']) {
            addFormErrors.html(`<p><strong id="add-form-error-message" class="text-success">${messages.successfullySubmitted}</strong></p>`);
            $('form#users-form').trigger('reset');
            getUserList();
            hideProgressBar(submitLoader);
            disableButtonsToggle();
            }
        },
        error: function (xhr) {
            window.invokedFunctionArray.push('submitUser')
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}
function setSeletedrolesList(){
    if(jsonArray.length > 0){
        jsonArray.forEach(function(val,index){
            if($('#filter-branches').val() == val.branchID){
                $('.users-role-filters').find("option[value=" + val.roleID + "]").attr('selected', true);
            }

            });
    }
}

$('.users-role-filters').on('change',function(){
    var selectedRolesArray = $(this).val();
    jsonArray.forEach((value, index) => {
        if(selectedRolesArray.indexOf(String(value.roleID)) === -1 ){
            deleteRolesArray.push(value);
        }
    });
})

$('#filter-active-inactive').on('change', function(){
    getUserList();
})

function getUserList() {
    var buttonGroup = ''
    let data = {};
    if(schoolID !== ''){
        data.schoolID = schoolID
    }

    if($('#filter-active-inactive').val() !== ''){
        data.isActive = $('#filter-active-inactive').val()
    }

    // if(branchID !== ''){
    //     data.branchID = branchID
    // }else{
    //     data.branchID = $('#filter-branches').val();
    // }
    if(tableData){
        tableData.destroy();

    }
    tableData = $('#example44').DataTable({
        language: {
            url: __DATATABLE_LANGUAGE__
        },
        initComplete: function() {
            $(this.api().table().container()).find('input').parent().wrap('<form>').parent().attr('autocomplete', 'off');
        },
        'ajax': {
            url: herokuUrl + '/getUserList',
            type: 'POST',
            headers,
            beforeSend: function () {
                buttonGroup = ''
            },
            data,
            dataSrc: function (res) {
                response = parseResponse(res);
                response = response[0].userData;
                var return_data = new Array();
                for (var i = 0; i < response.length; i++) {
                    buttonGroup = '';
                    var  rolesArray = [];
                    var rolesArrayResponse = '';
                    if(response[i].userRoleData !== undefined){
                       rolesArray = Array.isArray(response[i].userRoleData) ? response[i].userRoleData : 0;
                       rolesArrayResponse = rolesArray;
                        if(rolesArray.length){
                            for (var j = 0; j < rolesArray.length; j++) {
                                buttonGroup  += `<div class="btn-group">
                                <button class="btn-primary btn btn-xs assign-teacher" data-subject-id="" data-toggle="tooltip" title="${rolesArray[j].roleName }">${rolesArray[j].roleName }</button>
                                <button class="btn-danger btn btn-xs role-record-delete" data-toggle="tooltip" title="${messages.delete}" data-id='${rolesArray[j].roleLinkID }'><i class="fa fa-times"></i></button></div></br>`;
                                }
                        }
                    }
                    return_data.push({
                        'sNo': i + 1,
                        'fullName': response[i].fullName ? response[i].fullName : '--',
                        'mobileNo': response[i].mobileNo,
                        'roleName': buttonGroup !== '' ? buttonGroup : ' - ',
                        'isActive': `<label class="label label-sm label-${response[i].isActive ? 'success' : 'danger'}"> ${response[i].isActive ? messages.active : messages.inActive}</label>`,
                        'action': `${__ACCESS__ ? `<div class="btn-group"><button class="btn-danger btn btn-xs reset-password" data-user-id="${response[i].userID}" data-toggle="tooltip" title="${messages.resetPassword}"><i class="fa fa-unlock-alt"></i></button>
                        <button class="btn-primary btn btn-xs list-record-edit" data-toggle="tooltip" title="${messages.edit}" data-json='${handleApostropheWork(response[i])}' data-roles='${JSON.stringify(response[i].userRoleData)}'><i class="fa fa-pencil"></i></button>
                        </div>` : ''}`,
                        'select': `<div style="display: contents;" class="col-lg-3">
                                        <div class="checkbox checkbox-aqua">
                                            <input id="selectSingleBox-${i}" data-user-id="${response[i].userID}" class="user-checkbox" type="checkbox" name="test" >
                                            <label for="selectSingleBox-${i}">
                                            </label>
                                        </div>
                                    </div>`,
                    });
                }

                return return_data;
            },
            error: function (xhr) {
                invokedFunctionArray.push('getUserList')
                window.requestForToken(xhr)
                console.log(xhr);
                if (xhr.status === 404) {
                    $('.dataTables_empty').html(messages.noDataAvailable);
                 }
            }
        },
        "columns": [{
                'data': 'sNo'
            },
            {
                'data': 'fullName'
            },
            {
                'data': 'mobileNo'
            },
            {
                'data': 'roleName'
            },
            {
                'data': 'isActive'
            },
            {
                'data': 'action'
            },
            {
                'data': 'select'
            }
        ],
        "ordering": false
        // "select": [
        //     { "ordering": false, "aTargets": [0,5] }
        // ]
    });
}


$('body').on('click', '#selectAll', function () {
    allPages = tableData.cells().nodes()
    if ($(this).prop('checked')) {
        $('input[type="checkbox"]', allPages).prop('checked', true);
        $('input[type="checkbox"]', allPages).each(function(i){
            usersArray.push($(this).attr('data-user-id'));
        });
    } else {
        $('input[type="checkbox"]', allPages).prop('checked', false);
        usersArray = []
    }
})

$(document).on('change','.user-checkbox',function(){
    if($(this).prop('checked')){
        usersArray.push($(this).attr('data-user-id'));
        $(this).attr('data-user-id')
    }else{
        var index =  usersArray.indexOf(String($(this).attr('data-user-id'))) ;
        usersArray.splice(index, 1);
    }
})

$(document).on('click', '.assign-role', function(){
    $('#filter-branches-branches').val('').multiselect('rebuild');
    $('#filter-roles').val('').multiselect('rebuild');
    if(usersArray.length){
        $('.settingModal').modal();
    }else{
        swal(
            messages.pleaseSelectUserFirst,
            messages.oops,
            'error',{
                buttons:true,//The right way
                buttons: messages.ok, //The right way to do it in Swal1
            }
        )
        return false;
    }
});

$(document).on('click', '#saveChanges', function(){
    var bulkArray = [];
    var roles = $('#filter-roles').val();
    if(Array.isArray(roles)){
        roles = roles.filter(function (el) {
            return el != null && el !== '';
          });
    }
    if(Number(user.isAdmin) === 1 ){
        branchID = $('#filter-branches-branches').val();
    }

    usersArray.forEach(function(value, index){
        roles.forEach(function(val, ind){
            var obj = {}
            obj['userID'] = value
            obj['schoolID'] = schoolID
            obj['branchID'] = branchID
            obj['roleID'] = val;
            bulkArray.push(obj)
        })
    });
    if(bulkArray.length){
        setAssignRolesArray(bulkArray);
    }
});

$(document).on('change', '#filter-branches-branches', function(){
    branchID = $(this).val();
    if(branchID !== ''){
        getAjaxRoles();
    }
});

$("#mobileNo").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^a-zA-Z0-9\.]/, ""));
 });

 $(document).on('blur','#mobileNo', function(){
    if(isFormOpen === true){
        getDBUserList();
        isFormOpen = true;
    }
})

function getDBUserList(){
    let data = {};
    if(schoolID !== ''){
        data.schoolID = schoolID
    }

    let userName = $('#mobileNo').val();
    if(tableData){
        tableData.destroy();
    }
    tableData = $('#example44').DataTable({
        'ajax': {
            url: herokuUrl+'/getUserList',
            type: 'POST',
            headers,
            beforeSend: function(){
            },
            data,
            dataSrc: function(res){
                response = res.response[0].userData;
                for(var i = 0; i < response.length; i++){
                    if(response[i].mobileNo === userName){
                        $('#submit-add-form-container').attr('disabled', true);
                        $('#fullName').val(response[i].fullName)
                        $('#mobileNoLabel').removeClass('hidden')
                        $('#fullName').attr('disabled', true)
                        $('#mobileNo').attr('disabled', true)
                        $('#emailAddress').val(response[i].email)
                        $('#emailAddress').attr('disabled', true)
                        $('#checkIsSignatory').attr('disabled', true)
                        $('#activeField1').attr('disabled', true)
                        $('#activeField0').attr('disabled', true)
                        $('#isAdmin1').attr('disabled', true)
                        $('#passwordInput').attr('disabled', true)
                        $('#isAdmin0').attr('disabled', true)
                    }else{

                    }
                }
            },
            error: function(xhr){
                invokedFunctionArray.push('getDBUserList')
                window.requestForToken(xhr)
                if(xhr.status === 422){
                    parseValidationErrors(xhr);
                }
                if(xhr.status === 404){
                   $('.dataTables_empty').text(messages.noDataAvailable);
                }
                if (xhr.status === 500) {
                }
            }
        },
    });
}

$('#users-form').on('submit', function (e) {
    e.preventDefault();
    // if(user.isAdmin == 0){
    //     branchID = user.branchID;
    // }else{
    //     branchID = $('#filter-branches').val();
    // }
    // if(branchID === undefined || branchID.length == 0){
    //     Swal({
    //         type: messages.error,
    //         title: messages.oops,
    //         text: 'BranchID is Required!'
    //       })
    //     return false;
    // }
    let password = md5($('#passwordInput').val());
    $('#password').val(password);
    let mobileVal;
    var isEdit = $('#editHiddenField').val();
    var roles = $('#multiple').val();
    var filtersFormData = $('#filter-form').serialize();
    // $('#hiddenMobileNo').val(mobileVal);
    data = $('#filter-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '') + '&' + $('#users-form').serialize().replace(/[^&]+=\.?(?:&|$)/g, '') + '&createdByUserID=' + user.userID;
    showProgressBar(submitLoader);
    resetMessages();
    submitUser(roles, filtersFormData, isEdit);
});


$(document).on('click', '.reset-password', function(){
    resetPasswordUserID = $(this).attr('data-user-id');

    swal.mixin({
        input: 'password',
        confirmButtonText: 'Next &rarr;',
        showCloseButton: true,
        inputValidator: (value) => {
            return !value && messages.pleaseEnterPassword
        },
        progressSteps: ['*']
        }).queue([
        {
            title: 'Enter Password'
        },
        ]).then((result) => {
        if (result.value) {
            resetPasswordNewPassword = md5(result.value[0]);
            resetPassword();
        }
    });
});


function resetPassword(){
    $.ajax({
        url: herokuUrl + '/resetPassword',
        method: 'POST',
        headers,
        data: {
            userID: resetPasswordUserID,
            newPassword: resetPasswordNewPassword,
            updateUserID: user.userID
        },
        beforeSend: function () {
            disableButtonsToggle();
            showProgressBar(submitLoader);
        },
        success: function (res) {
            if(res.response === 'success'){
                swal(
                    messages.successMessage,
                    messages.successfullyReset,
                    'success'
                );

                resetPasswordUserID = null;
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();

        },
        error: function (xhr) {
            invokedFunctionArray.push('resetPassword')
            window.requestForToken(xhr)
            if (xhr.status === 422) {
                parseValidationErrors(xhr);
            }
            if (xhr.status === 400) {
                addFormErrors.html(`<p><strong id="add-form-error-message" class="text-danger">${xhr.responseJSON.userMessage}</strong></p>`);
            }
            if (xhr.status !== 400 && xhr.status !== 422) {
                swal(
                    messages.error,
                    `${xhr.responseJSON.userMessage}`,
                    'error'
                );
            }
            hideProgressBar(submitLoader);
            disableButtonsToggle();
        }
    });
}

$(document).on('click','.isActiveCheck',function(){
    if($(this).prop('checked')){
        $('#isActive').val(1)
    }else{
        $('#isActive').val(0)
    }
})

$(document).on('click','.isAdminCheck',function(){
    if($(this).prop('checked')){
        $('#isAdmin').val(1)
    }else{
        $('#isAdmin').val(0)
    }
})

$('#generateUUID').on('click', function(){
    uuid = generateUUID()
    $('#tagID').val(uuid)
})

$('#generateQR').on('click', function(){
    data = {}
    data.fullName = $('#fullName').val()
    data.tagString = $('#tagID').val()
    data.codeType = 1
    getQRCode(data)
})


function getQRCode(data){
    $.ajax({
        url: herokuUrl+'/getQRCode',
        method: 'POST',
        headers,
        data,
        xhrFields: {
            responseType: 'blob'
        },
        success: function (response) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(response);
                a.href = url;
                a.download = $('#fullName').val()+'.png';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
                setTimeout(()=>{
                    $('.exportLoader').addClass('hidden');
                },500)
        }
    })
}
