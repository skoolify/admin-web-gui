$(function(){
    var cities = $("#branchCity");
    var countries = $("#branchCountry");
    var areas = $("#branchArea");

    countries.on('change', function(){
        var countryID = $(this).val();
        getAjaxCities(cities, countryID);
    })

    cities.on('change', function(){
        var cityID = $(this).val();
        getAjaxAreas(areas, cityID);
    })
    
})

function getAjaxCities(cities, countryID){
    $.ajax({
        url: '{{ url("/ajax/cities") }}',
        method: 'GET',
        beforeSend: function(){
            cities.empty();
            cities.append(`<option value="">Select City</option>`);
            cities.attr('disabled', true);
            showProgressBar('p2-cities');
        },
        data: {
            countryID
        },
        success: function(res){
            if(!res.error){
                cities.removeAttr('disabled');
                if(res.data.length >= 1){
                    $.each(res.data, function(index, value){
                        cities.append(`<option value="${value.cityID}">${value.cityName}</option>`);
                    });
                    hideProgressBar('p2-cities');
                }else{
                    cities.append(`<option value="">No City Found</option>`);
                    cities.attr('disabled', true);
                    hideProgressBar('p2-cities');
                }
            }else{
                cities.attr('disabled');
                hideProgressBar('p2-cities');
            }
        },
        error: function(err){
            console.log(err);
            hideProgressBar('p2-cities');
        }
    })
}

function getAjaxAreas(areas, cityID){
    
    $.ajax({
        url: '{{ url("/ajax/areas") }}',
        method: 'GET',
        beforeSend: function(){
            areas.empty();
            areas.append(`<option value="">Select Area</option>`);
            areas.attr('disabled', true);
            showProgressBar('p2-areas');
        },
        data: {
            cityID
        },
        success: function(res){
            if(!res.error){
                areas.removeAttr('disabled');
                if(res.data.length >= 1){
                    $.each(res.data, function(index, value){
                        areas.append(`<option value="${value.areaID}">${value.areaName}</option>`);
                    })
                    hideProgressBar('p2-areas');
                }else{
                    areas.append(`<option value="">No Area Found</option>`);
                    areas.attr('disabled', true);
                    hideProgressBar('p2-areas');
                }
            }else{
                areas.attr('disabled');
                hideProgressBar('p2-areas');
            }
        },
        error: function(err){
            console.log(err);
            hideProgressBar('p2-areas');
        }
    })
}

function hideProgressBar(id){
    $("#"+id).addClass('hidden');
}

function showProgressBar(id){
    $("#"+id).removeClass('hidden');
}