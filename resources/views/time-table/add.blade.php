<!-- add section start-->
<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
    <div class="col-sm-12 col-md-12 col-xl-12">
    <button type="button" style="margin-top:35px;height:35px;" class="btn btn-primary btn-sm d-inline pull-right" id="add-new-row" data-toggle="tooltip" title="Add New Row"><i class="fa fa-plus"></i >@lang('messages.add-new-row')</button>
    </div>
        <form method="POST" id="time-table-form" action="{{ url('time-table/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>

            <div class="card-body row">
            <table class="table" style="width:100%" width="100%" id="add-time-table-form-table">
                <tbody>
                    <tr>
                        <td>
                            <div class="col-lg p-t-20">
                                <div class="form-group {{ $errors->has('marksObtained') ? 'has-error' :'' }}">
                                    <label for="Subject">Subject

                                    </label>
                                    <select name="subjectId"  class="form-control filter-select filter-time-table-subjects" data-validation="required" data-validation-error-msg="Subject is required">
                                        <option value="">Select Subject</option>
                                    </select>
                                    {!! $errors->first('marksObtained','<span class="help-block">:message</span>') !!}
                                    <div id="p2-subjects" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg p-t-20 ">
                                <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                                    <label for="comments" >Time From

                                    </label>
                                    <input class = "form-control" name="time-from"  type = "text" id = "time-from" placeholder="Time From" value="{{ old('time-from') }}"  data-validation="required" data-validation-error-msg="Start time is required" />
                                    {!! $errors->first('comments','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg p-t-20">
                                <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                                    <label for="comments" >Time To

                                    </label>
                                    <input class = "form-control" name="time-to"  type = "text" id = "time-to" placeholder="Time To" value="{{ old('time-to') }}" data-validation="required" data-validation-error-msg="End time is required" />
                                    {!! $errors->first('comments','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg p-t-20">
                                <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                                    <button type="button" style="margin-top:35px;height:35px;" class="btn btn-danger btn-xs d-inline remove-row-button hidden" data-toggle="tooltip" title="Remove Row"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="col-lg-12 p-t-20 text-center">
                <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink time-table-submit-button">Submit</button>
                <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
            </div>
            <div class="col-lg-12 p-t-20" id="edit-form-errors">
            </div>

            </div>
        </form>

    </div>
</div>
<!-- add section end  -->

<!-- edit section start -->

<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="time-table-edit-form-container">
    <div class="card-box">
        <form method="POST" id="edit-time-table-form" action="{{ url('time-table/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <input type="hidden" id="editHiddenField" value="" />
            <div class="card-body row">
            <table class="table" style="width:100%" width="100%" id="edit-time-table-form-table">
                <tbody>
                @for ($i = 1; $i < 11; $i++)
                    <tr>
                        <td>
                            <div class="col-lg p-t-20">
                                <div class="form-group {{ $errors->has('marksObtained') ? 'has-error' :'' }}">
                                    <label for="Subject">Subject
                                            <span class="required"> * </span>
                                    </label>
                                    <select name="subjectID{{$i}}"  class="form-control filter-select filter-time-table-subjects">
                                        <option value="">Select Subject</option>
                                    </select>
                                    {!! $errors->first('marksObtained','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg p-t-20 ">
                                <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                                    <label for="comments" >Time From
                                        <span class="required"> * </span>
                                    </label>
                                    <input class = "form-control" name="time-from{{$i}}"  type = "text" id = "time-from" placeholder="Time From" value="{{ old('time-from') }}"  />
                                    {!! $errors->first('comments','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg p-t-20">
                                <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                                    <label for="comments" >Time To
                                        <span class="required"> * </span>
                                    </label>
                                    <input class = "form-control" name="time-to{{$i}}"  type = "text" id = "time-to" placeholder="Time To" value="{{ old('time-to') }}"  />
                                    {!! $errors->first('comments','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg p-t-20">
                                <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                                    <button type="button" style="margin-top:35px;height:35px;" class="btn btn-danger btn-xs d-inline remove-row-button hidden" data-toggle="tooltip" title="Remove Row"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endfor
                </tbody>
            </table>
            <div class="col-lg-12 p-t-20 text-center">
                <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink time-table-submit-button" >Submit</button>
                <button type="button" id="hide-time-table-edit-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
            </div>
            <div class="col-lg-12 p-t-20" id="add-form-errors">
            </div>

            </div>
        </form>

    </div>
</div>

<!-- edit section end -->
