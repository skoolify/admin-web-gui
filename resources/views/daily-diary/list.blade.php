@extends('master')

@section('title', Lang::get('messages.daily-diary-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<style>
   .card-box:hover {
 /*    box-shadow: 0 1px 5px 1px rgba(0,0,0,0.14), 0 3px 14px 2px rgba(0,0,0,0.12), 0 5px 5px -3px rgba(0,0,0,0.2);
    transition: all 150ms linear; */
    -webkit-transform: none !important;
    transform: none !important;
    box-shadow: 0 20px 20px rgba(0,0,0,.1);
    -moz-box-shadow: 0 20px 20px rgba(0,0,0,.1);
    -webkit-box-shadow: 0 20px 20px rgba(0,0,0,.1);
}
</style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.daily-diary-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter-multiple')
                            @include('partials.components.subjects-filter')
                            @include('partials.components.students-filter-multiple')
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="">@lang('messages.from')</label>
                                    <input type="date" name="dateFrom" value="{{ \Carbon\Carbon::now()->startOfMonth()->toDateString() }}" id="dateFrom" class="form-control" />
                                    <div id="p2-date-from" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="">@lang('messages.to')</label>
                                    <input type="date" name="dateTo" value="{{ \Carbon\Carbon::now()->endOfMonth()->toDateString() }}" id="dateTo" class="form-control" />
                                    <div id="p2-date-to" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            @if(!(session()->get('user')->isAdmin))
                            <div class="col-lg-1 justify-content-center align-self-center">
                                <button class="btn btn-primary search-diary align-middle mt-3" type="submit"><i class="fa fa-search"></i> @lang('messages.search')</button>
                            </div>
                            @endif
                        </div>
                        @if(session()->get('user')->isAdmin)
                        <div class="row">
                            <div class="col-lg-12 justify-content-center align-self-center">
                                <button class="btn btn-primary search-diary align-middle mt-3" type="submit" style="float:right;"><i class="fa fa-search"></i> @lang('messages.search')</button>
                            </div>
                        </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>

        @include('daily-diary.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.daily-diary-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.full-name')</th>
                                    <th>@lang('messages.subject')</th>
                                    <th>@lang('messages.class')</th>
                                    <th>@lang('messages.description')</th>
                                    <th>@lang('messages.diary-date')</th>
                                    <th>@lang('messages.due-date')</th>
                                    {{-- <th>List</th> --}}
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="fathersDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color:green" > @lang('messages.diary-detail')</h4>
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close" >
          &times;
        </button>
      </div>
      <div class="container-fluid ">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight: 600;">@lang('messages.class')</h5>
                    </div>
                    <div class="col-md-8">
                        <h5 style="font-weight: 400;" id="classSection"></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight: 600;">@lang('messages.diary-date')</h5>
                    </div>
                    <div class="col-md-8">
                        <h5 style="font-weight: 400;" id="divDiaryDate"></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight: 600;">@lang('messages.teacher-name')</h5>
                    </div>
                    <div class="col-md-8">
                        <h5 style="font-weight: 400;" id="teacherName"></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <h5 style="font-weight: 600;">@lang('messages.subject')</h5>
                    </div>
                    <div class="col-md-8">
                        <h5 style="font-weight: 400;" id="subjectName"></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <table class="table table-striped table-bordered table-hover table-checkable" id="diary-detail-table" style="width:100% !important">
                    <thead style="width:100% !important">
                        <tr>
                            <th><small>@lang('messages.s-number')</small></th>
                            <!-- <th><small>Teacher Name</small></th> -->
                            <!-- <th><small>Subject Name</small></th> -->
                            <th><small>@lang('messages.student-name')</small></th>
                            <th><small>@lang('messages.description')</small></th>
                            <!-- <th><small>Diary Date</small></th> -->
                            <th><small>@lang('messages.due-date')</small></th>
                            <!-- <th><small>Class</small></th> -->
                            <th><small>@lang('messages.action-column')</small></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6">@lang('messages.record-not-found')</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="descriptionModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" style="color:red;" id="exampleModalLabel">@lang('messages.description')</h5>
          <button type="button" style="height: 30px;" class="btn btn-danger btn-sm mr-1" data-dismiss="modal" aria-label="Close">x
          </button>
        </div>
        <div id="descriptionModalParagraph" class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('messages.close')</button>
        </div>
      </div>
    </div>
</div>

@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/node-forge@0.7.0/dist/forge.min.js"></script>
@endpush
