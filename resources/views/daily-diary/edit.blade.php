@extends('master')

@section('title', 'Edit User')


@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />

@endsection


@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => 'Users Management'))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header>Edit User</header>
                </div>
                
                <form method="POST" action="{{ url('users/edit/'.$record->userID) }}">
                    @csrf
                    
                    <div class="card-body row">
                        <div class="col-lg-4 p-t-20"> 
                            <div class="form-group {{ $errors->has('fullName') ? 'has-error' :'' }}">
                                <label for="fullName">Full Name
                                    <span class="required"> * </span>
                                </label>
                                <input class = "form-control" name="fullName" type = "text" id = "fullName" placeholder="Full Name" value="{{ $record->fullname }}" required />
                                {!! $errors->first('fullName','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-lg-4 p-t-20"> 
                            <div class="form-group {{ $errors->has('mobileNo') ? 'has-error' :'' }}">
                                <label for="mobileNo">Mobile No.
                                        <span class="required"> * </span>
                                </label>
                                <input class = "form-control" name="mobileNo" type = "text" id = "mobileNo" placeholder="Mobile No." value="{{ $record->mobileNo ?? '' }}" required  />
                                {!! $errors->first('mobileNo','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-lg-4 p-t-20"> 
                            <div class="form-group {{ $errors->has('password') ? 'has-error' :'' }}">
                                <label for="password">Password
                                    <span class="required"> * </span>
                                </label>
                                <input class = "form-control" name="password" type = "text" id = "password" placeholder="Password" value=""  />
                                {!! $errors->first('password','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>


                        <div class="col-lg-4 p-t-20"> 
                            <div class="form-group {{ $errors->has('emailAddress') ? 'has-error' :'' }}">
                                <label for="emailAddress">Email Address
                                        <span class="required"> * </span>
                                </label>
                                <input class = "form-control" name="emailAddress" type = "email" id = "emailAddress" placeholder="Email Address" value="{{ $record->email ?? '' }}" required  />
                                {!! $errors->first('emailAddress','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-lg-4 p-t-20">
                            <div class="form-group {{ $errors->has('school') ? 'has-error' :'' }}">
                                <label for="school">School
                                    <span class="required"> * </span>
                                </label>
                                <select name="school" id="school" class="form-control" required>
                                    <option value="">Select School</option>
                                    @foreach ($schools as $school)
                                        <option value="{{ $school->schoolID }}" {{ $record->schoolID == $school->schoolID ? 'selected' : '' }}>{{ $school->schoolName }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('school','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-lg-3 p-t-20">
                            <label for="">Admin</label>
                            <div class="radio p-0">
                                <input type="radio" name="admin" id="admin1" value="1" {{ isset($record->isAdmin) && $record->isAdmin == 1 ? 'checked' : '' }}>
                                <label for="admin1">
                                    Yes
                                </label>
                            </div>
                            <div class="radio p-0">
                                <input type="radio" name="admin" id="admin2" value="0" {{ isset($record->isAdmin) && $record->isAdmin == 0 ? 'checked' : 'checked' }}>
                                <label for="admin2">
                                    No
                                </label>
                            </div>
                        </div>
                        @include('partials.active-field', array('isActive' => $record->isActive))
                    </form>
                    
                    
                    <div class="col-lg-12 p-t-20 text-center"> 
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
