<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="daily-diary-form" action="{{ url('daily-diary/add') }}" enctype="multipart/form-data">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('diaryDate') ? 'has-error' :'' }}">
                        <label for="diaryDate" >@lang('messages.diary-date')
                        </label>
                        <input class = "form-control" name="diaryDate" type = "date" id = "diaryDate" placeholder="Diary Date" value="{{ \Carbon\Carbon::now()->toDateString() }}" data-validation="required"  data-validation-error-msg="Diary Date is required." />
                        {!! $errors->first('diaryDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-6 p-t-20">
                    <div class="form-group {{ $errors->has('description') ? 'has-error' :'' }}">
                        <label for="description" >@lang('messages.description')
                        </label>
                        <input id="hidden-flag" type="hidden" name="flag">
                        <textarea maxlength="1000" class = "form-control" name="description" rows="5" id = "description" ddata-validation="required length" data-validation-length="max1000" >{{ old('description') }}</textarea>
                        {!! $errors->first('description','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('dueDate') ? 'has-error' :'' }}">
                        <label for="dueDate" >@lang('messages.due-date')
                        </label>
                        <input class = "form-control" name="dueDate" type = "date" id = "dueDate" placeholder="Due Date" value="" />
                        {!! $errors->first('dueDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                <div id="document-div" style="display:none;">
                        <a  class="delete-document" data-toggle="tooltip" title="Delete Document">x</a>
                        <a  class='' id="diary-document" data-toggle="tooltip" target="_blank" title="Click to download"><u style="color:blue !important">@lang('messages.document')</u></a>
                        
                    </div>
                    <label class="control-label col-md-12">@lang('messages.document')</label>
                    <div class="col-md-12">
                        <input type="file" class="form-control" id="documentUpload" name="documentUpload" accept=".doc,.docx,.ppt,.pptx,.txt,.pdf,.xlsx,.xls" />
                        <input type="hidden" id="hiddenDocument" style="height:auto !important" name="documentLink" value="" />
                        <p><small><em>@lang('messages.ms-file-validation')</em></small></p>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <label class="control-label col-md-12">@lang('messages.attachment')</label>
                    <div class="col-md-12">
                        <input type="file" class="form-control" style="height:auto !important" id="attachmentUpload" name="attachmentUpload" accept="image/x-png,image/gif,image/jpeg" />
                        <input type="hidden" id="hiddenAttachment" name="attachment" value="" />
                        <p><small><em>@lang('messages.image-validation')</em></small></p>
                    </div>
                </div>

                <div class="col-lg-2 p-t-20">
                    <div id="attachement-div" style="display:none;">
                        <a  class="delete-attachment" data-toggle="tooltip" title="Delete Attachment">x</a>
                        <img width="50" height="50" id="diary-attachment"  src="http://www.rangerwoodperiyar.com/images/joomlart/demo/default.jpg"   id="merchant-logo-tag" alt="Logo" class="img-thumbnail img-fluid pull-right justify-content-end">
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
