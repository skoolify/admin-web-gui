<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        <div class="card-body">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <form method="POST" id="form-management-form" action="" role="form">
                @csrf
                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.name')</label>
                    <div class="input-group col-md-8">
                        <input class="form-control formName" maxlength="100" name="formName" id="formName" type="text" value="" autocomplete="off" data-validation="required" placeholder="{{Lang::get('messages.name')}}" required>
                    </div>
                </div>

                <input type="hidden" id="editHiddenField" value="" />

                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.link')</label>
                    <div class="input-group col-md-8">
                        <input class="form-control formURL" maxlength="100" name="formURL" id="formURL" type="url" value="" autocomplete="off" required data-validation="required" placeholder="{{Lang::get('messages.link')}}">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.start-date')</label>
                    <div class="input-group col-md-8">
                        <input type="datetime-local" name="startDateTime" required value="" id="startDateTime" class="form-control" />
                        <div id="p2-date-from" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.end-date')</label>
                    <div class="input-group col-md-8">
                        <input type="datetime-local" name="endDateTime" required value="" id="endDateTime" class="form-control" />
                        <div id="p2-date-to" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>

                {{-- <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.timezone')</label>
                    <div class="input-group col-md-8">
                        <select name="timezone" id="filter-timezone" data-placeholder="Select Timezone" class="form-control select2" style="width: 100%" data-validation="required">
                        @foreach($timeZones as $key => $zone)
                            <option value="{{$zone}}" {{ $zone === 'Asia/Karachi' ? 'selected' : ''}}>{{$zone}}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}

                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.customVariable')</label>
                    <div class="input-group col-md-8">
                        <input class="form-control customVariables" maxlength="100" name="customVariables" id="customVariables" type="text" value="" autocomplete="off" data-validation="required" placeholder="{{Lang::get('messages.customVariable')}}">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.status-column')</label>
                    <div class="checkbox checkbox-aqua d-flex">
                        <input type="hidden"  name="isActive" id="isActive" value="0">
                        <input class="isActiveCheck" id="isActiveCheck" type="checkbox">
                        <label for="isActiveCheck" id="check-box-right">
                        <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                        <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-default">@lang('messages.cancel-btn')</button>
                        <button type="button" id="reset-add-form" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-danger">@lang('messages.reset')</button>
                </div>
                <div class="col-lg-12 p-t-20" id="add-form-errors"></div>
            </form>
            </div>
        </div>
    </div>
</div>
