@extends('master')

@section('title', Lang::get('messages.forms'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.forms')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @if (session()->get('user')->isAdmin == 1)
                                @include('partials.components.schools-filter')
                                @include('partials.components.branches-filter')
                            @endif
                            @include('partials.components.class-levels-filter-multiple')
                            @include('partials.components.classes-filter-multiple')
                            @include('partials.components.students-filter-multiple')

                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('form-management.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <header>@lang('messages.formsList')</header>
                            </div>
                            @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                                <div class="col-md-2 pl-0">

                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table form-list-table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.formName')</th>
                                    {{-- <th>@lang('messages.url')</th> --}}
                                    <th>@lang('messages.start-date')</th>
                                    <th>@lang('messages.end-date')</th>
                                    <th>@lang('messages.recipient')</th>

                                    {{-- <th>@lang('messages.end-date')</th> --}}
                                    {{-- <th>@lang('messages.class')</th>
                                    <th>@lang('messages.school')</th>
                                    <th>@lang('messages.timezone')</th> --}}
                                    <th>@lang('messages.status-active')</th>
                                    {{-- <th>@lang('messages.action-column')</th> --}}
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
