@extends('master')

@section('title', Lang::get('messages.attendance-report'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.attendance-report')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            <div class="col-md-3 col-sm-12 col-xs-12">  
                                <div class="form-group">
                                    <label for="">@lang('messages.from')</label>
                                    <input type="date" name="dateFrom" value="{{ \Carbon\Carbon::now()->startOfMonth()->toDateString() }}" id="filter-date-from" class="form-control" />
                                    <div id="p2-date-from" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">  
                                <div class="form-group">
                                    <label for="">@lang('messages.to')</label>
                                    <input type="date" name="dateTo" value="{{ \Carbon\Carbon::now()->endOfMonth()->toDateString() }}" id="filter-date-to" class="form-control" />
                                    <div id="p2-date-to" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-lg-1 justify-content-center align-self-center">
                                {{-- <div class="form-group"> --}}
                                    <button class="btn btn-primary search-attendance-report align-middle mt-3" type="button"><i class="fa fa-search"></i> @lang('messages.search')</button>
                                {{-- </div> --}}
                            </div>

                            <div class="col-lg-1 justify-content-center align-self-center ml-4">
                               <button class="btn btn-warning clear-attendance-report align-middle mt-3" type="button"><i class="fa fa-refresh"></i> @lang('messages.clear') </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.attendance-report-list')</header>
                    
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>@lang('messages.s-number')</th>
                                    <th>@lang('messages.student-name')</th>
                                    <th>@lang('messages.class')</th>
                                    <th>@lang('messages.total-days')</th>
                                    <th>@lang('messages.presents')</th>
                                    <th>@lang('messages.absents')</th>
                                    <th>@lang('messages.lates')</th>
                                    <th>@lang('messages.leaves')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="add-quiz-results-table">
            <div id="bulk-quiz-result-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-box">

                <div class="card-head">
                    <header>@lang('messages.add-quiz-results')</header>
                    <!-- <div class="btn-group pull-right"> -->
                        <button type="button" class="btn btn-primary pull-right mr-2" id="submit-add-quiz-result">@lang('messages.submit-btn')</button>
                        <button type="button" class="btn btn-danger pull-right mr-2" id="cancel-add-quiz-result">@lang('messages.cancel')</button>
                    <!-- </div> -->
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <form action="#" id="bulk-quiz-results-form">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="quiz-results-add-table" style="width: 100% !important;" >
                                <thead>
                                    <tr>
                                    <!-- <th style="display:none;">testing</th> -->
                                        <th>@lang('messages.roll-number')</th>
                                        <th>@lang('messages.student-name')</th>
                                        <th>@lang('messages.obtained-marks')</th>
                                        <th>@lang('messages.comments')</th>
                                    </tr>
                                </thead>
                                <tbody id="quiz-results-add-tbody">
                                   
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
@endpush
