
<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="subject-time-table-form" action="{{ url('students/add') }}" enctype="multipart/form-data">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />
                <input type="hidden" id="timetableSubjectID" value="" />

                <div class="col-lg p-t-20">
                    @include('partials.components.class-room-filter')  
                </div>

                <div class="col-lg p-t-20 ">
                    <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                        <label for="comments" >@lang('messages.time-from')
                            <span class="required"> * </span>
                        </label>
                        <input class = "form-control clockpicker" name="startTime"  type = "text" id = "startTime" placeholder="{{Lang::get('messages.time-from')}}" value="{{ old('startTime') }}"  />
                        {!! $errors->first('comments','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg p-t-20">
                    <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                        <label for="comments" >@lang('messages.time-to')
                            <span class="required"> * </span>
                        </label>
                        <input class = "form-control clockpicker" name="endTime"  type = "text" id = "endTime" placeholder="{{Lang::get('messages.time-to')}}" value="{{ old('endTime') }}"  />
                        {!! $errors->first('comments','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>

@push('extra-scripts')
<!-- dropzone -->
<script src="{{ asset('assets/plugins/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/plugins/dropzone/dropzone-call.js') }}" ></script>
@endpush
