<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="modules-form" action="{{ url('modules/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20"> 
                    <div class="form-group {{ $errors->has('moduleName') ? 'has-error' :'' }}">
                        <label for="moduleName">@lang('messages.module-name')
                        </label>
                        <input maxlength="50" class = "form-control" name="moduleName" type = "text" id = "moduleName" value="{{ old('moduleName') }}" data-validation="required length" data-validation-length="max50" placeholder="{{Lang::get('messages.module-name')}}">
                        {!! $errors->first('moduleName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20"> 
                    <div class="form-group {{ $errors->has('moduleUrl') ? 'has-error' :'' }}">
                        <label for="moduleUrl">@lang('messages.module-url')
                        </label>
                        <input class = "form-control" name="moduleURL" type = "text" id = "moduleUrl" value="{{ old('moduleUrl') }}" data-validation="required length" data-validation-length="max250" placeholder="{{Lang::get('messages.module-url')}}">
                        {!! $errors->first('moduleUrl','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('moduleDescription') ? 'has-error' :'' }}">
                        <label for="moduleDescription" >@lang('messages.module-description')
                        </label>
                        <textarea maxlength="225" class = "form-control" name="moduleDescription" id = "moduleDescription" data-validation="required length" data-validation-length="max225" placeholder="{{Lang::get('messages.module-description')}}">{{ old('moduleDescription') }}</textarea>
                        {!! $errors->first('moduleDescription','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('parentModule') ? 'has-error' :'' }}">
                        <label for="parentModule">@lang('messages.parent-module')</label>
                        <select name="parentModuleID" id="parentModule" class="form-control">
                            <option value="0">@lang('messages.select-parent-module')</option>
                        </select>
                        {!! $errors->first('parentModule','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


                <div class="col-lg-3 p-t-20">
                    <input type="hidden" name="isParent" id="isParent" value="" data-validation="required">
                    <label for="">@lang('messages.parent')</label>
                    <div class="radio p-0">
                        <input type="radio" class="parent-check" id="optionsRadios1" value="1">
                        <label for="optionsRadios1">
                            @lang('messages.yes')
                        </label>
                        <br>
                    </div>
                    <div class="radio p-0">
                        <input type="radio" class="parent-check" id="optionsRadios2" value="0">
                        <label for="optionsRadios2">
                            @lang('messages.no')
                        </label>
                    </div>
                </div>

                @include('partials.active-field')

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
