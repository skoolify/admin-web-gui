@extends('master')

@section('title', Lang::get('messages.time-table-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/bootstrap-clockpicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/jquery-clockpicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/jquery-clockpicker.min.css') }}" rel="stylesheet" type="text/css" />
@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.time-table-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.weeks-day-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('new-time-table.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.time-table-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn') / <i class="fa fa-pencil"> </i> @lang('messages.edit-btn')</button>
                    @endif
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>                                    
                                    <th>@lang('messages.teacher')</th>
                                    <th>@lang('messages.subject')</th>
                                    <th>@lang('messages.start-time')</th>
                                    <th>@lang('messages.end-time')</th>
                                    <!-- <th>Actions</th> -->
                                </tr>
                            </thead>
                            <tbody>

                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{asset('assets/js/bootstrap-clockpicker.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-clockpicker.js')}}"></script>
<script src="{{asset('assets/js/jquery-clockpicker.min.js')}}"></script>
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>

@endpush
