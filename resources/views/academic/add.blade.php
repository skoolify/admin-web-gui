<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        <form method="POST" id="academic-form" action="{{ url('academic/add') }}">
            <div class="container-fluid">
                <div class="col-lg-12">
                <div id="p2-students-result-list" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100% !important">
                    </div>
                    <table class="table table-striped  table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="student-result-table">
                        <thead>
                            <tr>
                                <th width="10%">@lang('messages.s-number')</th>
                                <th width="8%">@lang('messages.roll-number')</th>
                                <th width="10%">@lang('messages.student')</th>
                                <th width="10%">@lang('messages.absent')</th>
                                <th width="10%">@lang('messages.marks-obtain')</th>
                                <th width="5%">@lang('messages.grade')</th>
                                <th width="40%" class="text-center">@lang('messages.note')</th>
                                <!-- <th width="17%"> -->
                                <th class="text-center subject-bulk-access" id="subject-bulk-access" data-key="readAccess">@lang('messages.app-visibility') <i class="fa fa-check-square-o"></i>
                                    <div class="hidden checkbox checkbox-aqua d-flex">
                                    <div style="margin-right: 15px;">@lang('messages.app-visibility')</div>
                                        <input  id="select-subject-result-all" data-id="select-subject-result" class="hidden subjectResultCheckBoxAll" type="checkbox">
                                        <label for="select-subject-result-all" style="margin-top: 3px; margin-left:11px;">
                                        </label>
                                    </div>
                                </th>
                                <!-- </th> -->
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="7"><strong style="color:#188ae2;font-weight:bold">@lang('messages.select-exam-term-first')</<strong></td>
                        </tr>
                        </tbody>
                    </table>
                   
                    <div class="col-lg-12 p-t-20" id="add-form-errors"></div>
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                        <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                    </div>
                </div>
            </div>
        </form>   
    </div>
</div>

