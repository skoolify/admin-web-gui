@extends('master')

@section('title', Lang::get('messages.academic-result-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.academic-result-management')))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.exam-term-filter')
                            @include('partials.components.subjects-filter')
                            <div class="col-md-3 col-sm-12 col-xs-12" style="display:none" id="total-marks-div">  
                                <div class="form-group">
                                    <label for="totalMarks">@lang('messages.total-marks')
                                        <span class="required"> * </span>
                                    </label>
                                    <input onKeyUp="numericFilter(this);" class = "form-control" name="totalMarks"  type = "text" id = "totalMarks" data-validation="number" data-validation-allowing="float" placeholder="Total Marks" value="{{ old('totalMarks') }}"  />
                                    {!! $errors->first('totalMarks','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('academic.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif
                    <div id="non-assessment-case" class="card-head">
                        <header>@lang('messages.academic-result-list')</header>
                        @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                        <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')  / <i class="fa fa-pencil"></i> @lang('messages.edit-btn')</button>
                        @endif
                    </div>
                    <div id="assessment-case" class="hidden col-md-12 text-center card-head">
                        <span class="text-center m-auto"><strong>@lang('messages.alert')</strong> @lang('messages.academic-alert-text')</span>
                    </div>
                
                <div class="card-body non-assessment-body" >
                <div class="row">
                    <div class="offset-md-4 col-md-4">
                        <label class="center text-center justify-content-center label label-lg academic-flag" style="display:none;"> </label>    
                    </div>
                </div>
                
                    <div class="table-scrollable">
                        <div class="table-scrollable" id="academic-results-list">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="academic-result-table">
                                <thead>
                                    <tr>
                                        <th>#</th>      
                                        <th>@lang('messages.name')</th>                              
                                        <th>@lang('messages.subject')</th>
                                        <th>@lang('messages.total-marks')</th>
                                        <!-- <th>Absent</th> -->
                                        <th>@lang('messages.obtained-marks')</th>
                                        <th>@lang('messages.grade')</th>
                                        <th>@lang('messages.notes')</th>
                                        <th>@lang('messages.action-column')</th>
                                    </tr>
                                </thead>
                                <tbody id="subject-result-list-table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script>
    function numericFilter(txb) {
        txb.value = txb.value.replace(/[^\0-9]/ig, "");
    }
</script>
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
@endpush
