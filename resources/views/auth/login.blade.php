@extends('auth.master')

@push('extra-css')
<link href="{{ asset('assets/fonts/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/Croppie-2.6.4/croppie.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/fonts/material-design-icons/material-icon.css') }}" rel="stylesheet" type="text/css" />
<!--bootstrap -->
<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<!-- data tables -->
<link href="{{ asset('assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- Material Design Lite CSS -->
<link rel="stylesheet" href="{{ asset('assets/plugins/material/material.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/material_style.css') }}">
<!-- Theme Styles -->
<link href="{{ asset('assets/css/theme/full/theme_style.css') }}" rel="stylesheet" id="rt_style_components" type="text/css" />
<link href="{{ asset('assets/css/theme/full/style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/sweetalert.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/theme/full/theme-color.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

@if(App::getLocale() === 'ar')
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
@endif
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }
        .center{margin:auto!important;}
        @media only screen and (max-width: 600px) {
            .wrap-login100 {
                width: 100%;
            }
        }
    </style>
    <script>
    var resetLastLink = '{{ Request::query("logout") }}';
    </script>
@endpush

@section('title', 'Login')

@section('content')

<div class="limiter">

    <div class="container-login100 page-background">
            {{-- <div class="container">
                    <div class="row">
                        <div style="margin:auto;">
                            <img src="{{ asset('assets/img/logo-single-small.png') }}" class="img-responsive animated tada delay-5s mt-5" alt="Skoolify" />
                        </div>
                    </div>
               </div> --}}
        <div class="container mt-3">
            <div class="row">
                <div style="margin:auto;">
                    <img src="{{ asset('assets/img/logo-single-small.png') }}" class="img-responsive animated tada delay-5s" alt="Skoolify" />
                </div>
            </div>
            <div class="wrap-login100 mt-100vh center">
                {{-- <div class="row">
                    <li class="dropdown language-switch">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img src="../assets/img/flags/{{App::getLocale() === 'ar' ? 'ae' : 'gb'}}.png" class="position-left" alt=""> {{App::getLocale() === 'ar' ? 'Arabic' : 'English'}} <span class="fa fa-angle-down"></span>
                        </a>
                        <form method="get" id="switch-language-form">
                        <input type="hidden" name="lang" value="{{ Request::query('lang') ?? 'en' }}" id="language">
                            <ul class="{{App::getLocale() === 'ar' ? 'language-dropdown-login' : 'none'}} dropdown-menu">
                                <li>
                                    <a class="switchLanguageBtn" data-value="en"><img src="../assets/img/flags/gb.png" alt=""> English</a>
                                </li>
                                <li>
                                    <a class="switchLanguageBtn" data-value="ar"><img src="../assets/img/flags/ae.png" alt=""> Arabic</a>
                                </li>
                            </ul>
                        </form>
                    </li>
                </div> --}}
                <form class="login100-form validate-form" action="#" id="login-form" novalidate>
                    <span class="login100-form-logo">
                        <img alt="" id="schoolLogo" height="120px" onerror="this.src='{{ asset('assets/img/logo-2.png') }}'" src="{{ asset('assets/img/logo-loader.gif') }}" />
                    </span>
                    <span class="login100-form-title p-b-34 p-t-27 field-maintain">
                        @lang('messages.login')
                    </span>
                    <span class="login100-form-title p-b-34 p-t-27 field-unmaintain" style="display: none">
                        MAINTENANCE
                    </span>
                    {{-- <span class="login100-form-title p-b-34 p-t-10 field-unmaintain" id="maintenance-text"></span> --}}
                    <div class="wrap-input100 validate-input field-maintain" data-validate = "Enter Mobile No." id="login-mobileNo-container">
                        <input class="input100 mobile-no-validation" type="text" id="mobileNo"  value="" placeholder="Username / Mobile No." required>
                        <input type="hidden" name="mobileNo" id="hiddenMobileNo"  />
                        <!-- <span class="focus-input100" data-placeholder="&#xf207;"></span> -->
                    </div>
                    <div class="wrap-input100 validate-input field-maintain" data-validate="Enter Password" id="login-password-container" style="display:none">
                        <input class="input100" type="password" id="password" name="password" value="" placeholder="Password" required>
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="alert alert-danger field-maintain" id="error-message" style="display:none">
                    </div>

                    <div class="container-login100-form-btn field-maintain">
                        <button class="login100-form-btn" type="submit" id="validateUser">
                            @lang('messages.login.next.btn')
                        </button>

                        <button class="login100-form-btn" type="submit" id="login" style="display:none">
                            @lang('messages.login.login.btn')
                        </button>
                    </div>

                    <br/>
                    <center><p><strong id="maintenance-text"></strong></p></center>

                    @csrf
                </form>
            </div>
        <div>
    </div>
</div>

@endsection

@push('extra-scripts')
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/popper/popper.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.min.js')}}"></script>
<script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<!-- bootstrap -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<!-- data tables -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/table/table_data.js') }}" ></script>
<!-- Common js-->
<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/layout.js') }}"></script>
<script src="{{ asset('assets/js/theme-color.js') }}"></script>
<!-- Material -->
<script src="{{ asset('assets/plugins/material/material.min.js') }}"></script>
<!-- end js include path -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script> -->
<script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/moment.js') }}"></script>
<script src="{{ asset('assets/js/moment-timezone-with-data.js') }}"></script>

<!-- <script src="https://momentjs.com/downloads/moment-timezone-with-data.js"></script> -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{asset('assets/js/jquery.form-validator.min.js')}}"></script>
<script src="{{ asset('scripts/global.js') }}"></script>
<script src="{{ asset('scripts/list-functions.js') }}"></script>
<script src="{{ asset('scripts/dependent-dropdown-list.js') }}"></script>
<script src="{{ asset('scripts/js-md5.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script> -->
<script src="{{asset('assets/plugins/summernote/summernote.js')}} "></script>
<script src="{{asset('assets/js/pages/summernote/summernote-data.js')}}" ></script>
<script src="{{asset('assets/js/jquery-confirm.min.js')}}" ></script>
<script src="{{asset('assets/js/bootstrap-multiselect.js')}}" ></script>
<script src="{{asset('assets/js/loadingoverlay.min.js')}}"></script>
<script src="{{asset('assets/plugins/Croppie-2.6.4/croppie.js')}}"></script>
    <script>
        var __BASE_AJAX__ = '{{ url("/ajax/") }}';
        localStorage.removeItem('user');
        localStorage.removeItem('apiToken');
        localStorage.removeItem('switch');
        localStorage.setItem('apiToken', JSON.stringify(@json(session()->get('apiToken'))));
    </script>

    <script src="{{ asset('scripts/ajax-scripts/auth-new.js') }}"></script>
@endpush
