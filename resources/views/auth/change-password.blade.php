@extends('auth.master')

@push('extra-css')
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }
        .center{margin:auto!important;}
        @media only screen and (max-width: 600px) {
            .wrap-login100 {
                width: 100%;
            }
        }
    </style>
@endpush

@section('title', Lang::get('messages.changePassword'))

@section('content')

<div class="limiter">

    <div class="container-login100 page-background">

        <div class="container mt-3">
            <div class="row">
                <div style="margin:auto;">
                    <img src="{{ asset('assets/img/logo-single-small.png') }}" class="img-responsive" alt="Skoolify" />
                </div>
            </div>
            <div class="wrap-login100 mt-100vh center">
                <form class="login100-form validate-form" action="#" id="change-password-form" novalidate>
                    <span class="login100-form-logo">
                        <img alt="" id="schoolLogo" height="120px" onerror="this.src='{{ asset('assets/img/logo-2.png') }}'" src="{{ session()->get('schoolLogo') }}" />
                    </span>
                    <span class="login100-form-title p-b-10 p-t-27">
                        @lang('messages.changePassword')
                    </span>
                    @if(Request::has('f') && !empty(Request::query('f')))
                    <div class="text-center p-b-34">
                        <b class="text-white">@lang('messages.sinceThisIsYourFirstTimeLoginPleaseChangeThePasswordForSecurityReasons')</b>
                    </div>
                    @else
                        <!-- <h4> {{session()->get('user')->lastLoginDate}} 123</h4> -->
                    @endif

                    <div class="wrap-input100 validate-input field-maintain" data-validate = "Enter Password" id="login-mobileNo-container">
                        <input class="input100 mobile-no-validation" name="newPassword" type="password" id="newPassword"  value="" placeholder="{{Lang::get('messages.newPassword')}}" required>
                    </div>

                    <input type="hidden" name="userID" value="{{ session()->get('user')->userID }}" />


                    <div class="alert field-maintain" id="error-message" style="display:none">
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn" type="submit" id="submit-button">@lang('messages.updatePassword')</button>
                        <br />
                    </div>
                    <hr />
                    <div class="container-login100-form-btn">
                        <a class="login100-form-btn" id="cancel-button" href="{{ url()->previous() }}">@lang('messages.cancel-btn') / @lang('messages.goBack')</a>
                    </div>
                </form>
            </div>
        <div>
    </div>
</div>

@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
