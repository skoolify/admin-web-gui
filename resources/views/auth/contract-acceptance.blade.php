@extends('auth.master')

@push('extra-css')
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }
        .center{margin:auto!important;}
        @media only screen and (max-width: 600px) {
            .wrap-login100 {
                width: 100%;
            }
        }

        .hidden{
            display: none;
        }

nav > .nav.nav-tabs{

border: none;
  color:#fff;
  background:#272e38;
  border-radius:0;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
border: none;
  padding: 18px 25px;
  color:#fff;
  background:#272e38;
  border-radius:0;
}

nav > div a.nav-item.nav-link.active:after
{
content: "";
position: relative;
bottom: -60px;
left: -10%;
border: 15px solid transparent;
border-top-color: #2fa8ff ;
}
.tab-content{
background: #fdfdfd;
  line-height: 25px;
  border: 1px solid #ddd;
  border-top:5px solid #2fa8ff;
  border-bottom:5px solid #2fa8ff;
  padding:30px 25px;
}

nav > div a.nav-item.nav-link:hover,
nav > div a.nav-item.nav-link:focus
{
border: none;
  background: #2fa8ff;
  color:#fff;
  border-radius:0;
  transition:background 0.20s linear;
}
    </style>
@endpush

@section('title', 'Contract')

@section('content')

<div class="container-login100 page-background">
    <div class="container">
        {{-- <div class="row">
            <div style="margin:auto;">
                <img src="{{ asset('assets/img/logo-single-small.png') }}" class="img-responsive" alt="Skoolify" />
            </div>
        </div> --}}
        <div class="row">
            <div class="col-xs-12 ">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="click nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" style="background-color:#2fa8ff;"><b>Contract</b></a>
                    <a class="click nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><b>Rate & Charges (Schedule 2 & 2A)</b></a>
                    @if(session()->get('contractData')['collectionID'] && session()->get('contractData')['collectionID'] !== '' && session()->get('contractData')['collectionID'] !== null )
                        <a class="click nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><b>Fee Collection Charges (Schedule 2B)</b></a>
                    @endif
                    </div>
                </nav>
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent" style="width: 1140px;">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row" style="width:1000px;margin-left:60px;">
                            <div class="col-md-12">
                                {!!session()->get('contractData')['contractText']!!}
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" style="padding: 10px;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="contractCheck">
                                        <label class="custom-control-label" for="contractCheck">I agree to the above terms & conditions.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="row" style="width:1000px;margin-left:60px;">
                            <div class="col-md-12">
                                {!!session()->get('contractData')['chargesText']!!}
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" style="padding: 10px;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="chargesCheck">
                                        <label class="custom-control-label" for="chargesCheck">I agree to the above terms & conditions.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(session()->get('contractData')['collectionID'] && session()->get('contractData')['collectionID'] !== '' && session()->get('contractData')['collectionID'] !== null )
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="row" style="width:1000px;margin-left:60px;">
                                <div class="col-md-12">
                                    {!!session()->get('contractData')['collectionText']!!}
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6" style="padding: 10px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="collectionCheck">
                                            <label class="custom-control-label" for="collectionCheck">I agree to the above terms & conditions.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <form action="" id="contract-form">
            <input type="hidden" id="userData" value="{{json_encode(session()->get('user'))}}" >
            <input type="hidden" id="userIP" value="">
            <input type="hidden" name="data" id="hiddenData" value="{{json_encode(session()->get('contractData'))}}">
            <div class="row mt-2">
                {{-- <div class="col-md-4" style="padding: 10px;">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="contractCheck">
                        <label class="custom-control-label" for="contractCheck">I agree to the above terms & conditions.</label>
                    </div>
                </div> --}}
                <div class="col-md-9">
                    <label id="contractErrorLabel" style="font-weight: 500;padding-top: 10px; color:red;display:none;" for="">Please select the checkbox in Contract section!</label>
                    <label id="chargesErrorLabel" style="font-weight: 500;padding-top: 10px; color:red;display:none;" for="">Please select the checkbox in Rate & Charges section!</label>
                    <label id="collectionErrorLabel" style="font-weight: 500;padding-top: 10px; color:red;display:none;" for="">Please select the checkbox in Fee Collection Charges section!</label>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-sm acceptBtn" style="float: right;
                    margin-top: 6px;">Accept & Continue</button><i class="exportLoader hidden fa fa-spinner fa-pulse pull-right mt-2 mr-2" style="font-size:24px;color: #188ae2;"></i>
                </div>
            </div>
        </form>
    </div>
<div>

@endsection

@push('extra-scripts')
<script>
    $(document).on('click','.click', function(){
        var x = document.getElementsByClassName("click");
        let v = 'style="background-color:#2fa8ff;"'
        $('.click').attr('style','background-color:#272e38;' )
        $(this).attr('style','background-color:#2fa8ff;' )
    })
</script>
<script src="{{ asset('scripts/ajax-scripts/contract-page.js?version='.time()) }}"></script>
@endpush
