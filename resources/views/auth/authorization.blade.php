@extends('auth.master')

@push('extra-css')
    @if(App::getLocale() === 'ar' )
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    @endif
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }

        #loader-logo {
            -webkit-animation: action 1s infinite  alternate;
            animation: action 1s infinite  alternate;
        }
        @-webkit-keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }
        @keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }


        .animate {
            -webkit-animation-duration: 3s;
            animation-duration: 3s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
         }

         @-webkit-keyframes fadeOutUpper {
            0% {
               opacity: 1;
               -webkit-transform: translateY(0);
            }
            100% {
               opacity: 0;
               -webkit-transform: translateY(-20px);
            }
         }

         @keyframes fadeOutUpper {
            0% {
               opacity: 1;
               transform: translateY(0);
            }
            100% {
               opacity: 0;
               transform: translateY(-20px);
            }
         }

         .fadeOutUpper {
            -webkit-animation-name: fadeOutUpper;
            animation-name: fadeOutUpper;
         }

        #loader-text{
            color: #525252;
        }

        .center{
            margin:auto!important;
        }

    </style>
@endpush

@section('title', 'Authorization')

@section('content')

<div class="limiter">
    <div class="container-login100 page-background">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center" id="multi-select-container" style="display: none;">
                    <h3>
                        @lang('messages.please-select-role')
                    </h3>
                    <form action="#" class="mt-5">
                        <div class="form-group col-md-4 center">
                            <select name="role" id="multiRoleSelect" class="form-control">
                            </select>
                        </div>
                        <div class="form-group col-md-4 center">
                            <button type="button" id="proceed-btn" class="btn btn-primary btn-block mt-3">
                                @lang('messages.proceed-btn')
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 text-center" id="modules-loader">
                    <img src="{{ asset('assets/img/logo-single-small.png') }}" id="loader-logo" class="animate img-responsive" alt="Skoolify" />
                    <h1 id="loader-text" class="animate">
                        @lang('messages.please-wait')
                    </h1>
                    <div class="col-md-4 offset-md-4 col-xs-12 col-sm-12">
                        <br />
                        <a href="{{ url('auth/login') }}" id="logout-btn" class="btn btn-danger btn-block" style="display: none;">
                            @lang('messages.logout-btn')
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('extra-scripts')
<script src="{{asset('scripts/language/'.App::getLocale().'.js')}}"></script>
<script src="{{ asset('scripts/ajax-scripts/authorization.js') }}"></script>
@endpush
