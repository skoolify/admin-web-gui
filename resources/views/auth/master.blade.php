<!DOCTYPE html>
<html dir="{{App::getLocale() === 'ar' ? 'rtl' : 'none'}}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Skoolify" />
    <meta name="author" content="Skoolify" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Skoolify | @yield('title')</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('assets/plugins/iconic/css/material-design-iconic-font.min.css') }}" />
    <!-- bootstrap -->
	<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="{{ asset('assets/css/pages/extra_pages.css') }}">
	<!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}" />
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
    <script>
    var invokedFunctionArray = [];
    var __BASE__ = '{{ asset("/") }}';
    var __DOMAIN__ = '{{ isset($domain) ? $domain : '' }}';
    var __CSRF__ = '{{ csrf_token() }}';
    var __BASEURL__ = '{{ env("API_URL") }}/{{env("API_VERSION")}}';
    var __BASE_AJAX__ = '{{ url("/ajax/") }}';
    var __COMM_ACCESS__ = '{{ Authorization::check("comm-requests", "edit", Authorization::getParent("comm-requests"), true) }}';
    var __ROUTE__ = '{{ Route::currentRouteName() }}';
    </script>

    @stack('extra-css')
</head>
<body>

    @yield('content')

    <!-- start js include path -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}" ></script>
    <!-- bootstrap -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/extra-pages/pages.js') }}" ></script>
    <!-- end js include path -->

    <!-- Custom JS -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script src="{{ asset('scripts/js-md5.min.js?version='.time()) }}"></script>
    <script src="{{ asset('assets/js/moment.js') }}"></script>
    <script src="{{asset('scripts/language/'.App::getLocale().'.js')}}"></script>

    @stack('extra-scripts')
</body>
</html>
