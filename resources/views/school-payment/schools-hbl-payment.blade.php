@extends('auth.master')

@push('extra-css')
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }

        #loader-logo {
            -webkit-animation: action 1s infinite  alternate;
            animation: action 1s infinite  alternate;
        }
        @-webkit-keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }
        @keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }


        .animate {
            -webkit-animation-duration: 3s;
            animation-duration: 3s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
         }
         
         @-webkit-keyframes fadeOutUpper {
            0% {
               opacity: 1;
               -webkit-transform: translateY(0);
            }
            100% {
               opacity: 0;
               -webkit-transform: translateY(-20px);
            }
         }
         
         @keyframes fadeOutUpper {
            0% {
               opacity: 1;
               transform: translateY(0);
            }
            100% {
               opacity: 0;
               transform: translateY(-20px);
            }
         }
         
         .fadeOutUpper {
            -webkit-animation-name: fadeOutUpper;
            animation-name: fadeOutUpper;
         }

        #loader-text{
            color: #525252;
        }

        .center{
            margin:auto!important;
        }

    </style>
@endpush

@section('title', 'Secure Payment')

@section('content')

<div class="limiter">
    <div class="container-login100 page-background">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" id="payment-modules-loader">
                    <img src="{{ asset('assets/img/payment-loader.gif') }}" id="loader-logo" class="animate img-responsive" alt="Skoolify" />
                    <h5 id="payment-loader-text" class="animate">@lang('messages.connecting')</h5>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- {{dd($data)}} --}}
<div>
    <form id="school_payment_form" action="{{$data['hbl_url']}}" method="POST">
        <input type="hidden" name="access_key" value="{{$data['access_key']}}">
        <input type="hidden" name="profile_id" value="{{$data['profile_id']}}">
        <input type="hidden" name="transaction_uuid" value="{{$data['transaction_uuid']}}">
        <input type="hidden" name="signed_field_names" value="{{$data['signed_field_names']}}">
        <input type="hidden" name="unsigned_field_names" value="">
        <input type="hidden" name="signed_date_time" value="{{$data['signed_date_time']}}">
        <input type="hidden" name="locale" value="en">
        <input type="hidden" name="transaction_type" value="sale">
        <input type="hidden" name="reference_number" id="reference_number" value="{{$data['reference_number']}}">
        <input type="hidden" name="amount" id="amount" value="{{$data['amount']}}">
        <input type="hidden" name="currency" id="currency" value="{{$data['currency']}}">
        <input type="hidden" name="bill_to_address_city" value="{{$data['bill_to_address_city']}}">
        <input type="hidden" name="bill_to_address_country" value="{{$data['bill_to_address_country']}}">
        <input type="hidden" name="bill_to_address_line1" value="{{$data['bill_to_address_line1']}}">
        <input type="hidden" name="bill_to_address_postal_code" value="{{$data['bill_to_address_postal_code']}}">
        <input type="hidden" name="bill_to_address_state" value="{{$data['bill_to_address_state']}}">
        <input type="hidden" name="bill_to_forename" value="{{$data['bill_to_forename']}}">
        <input type="hidden" name="bill_to_phone" value="{{$data['bill_to_phone']}}">
        <input type="hidden" name="bill_to_surname" value="{{$data['bill_to_surname']}}">
        <input type="hidden" name="bill_to_email" value="{{$data['bill_to_email']}}">
        <input type="hidden" name="consumer_id" value="{{$data['consumer_id']}}">
        <input type="hidden" name="customer_ip_address" value="{{$data['customer_ip_address']}}">
        <input type="hidden" name="merchant_defined_data1" value="MobileApp">
        <input type="hidden" name="merchant_defined_data2" value="YES">
        <input type="hidden" name="merchant_defined_data4" value="Skoolify">
        <input type="hidden" name="merchant_defined_data5" value="YES">
        <input type="hidden" name="merchant_defined_data7" value="0">
        <input type="hidden" name="merchant_defined_data20" value="NO">        
        <input type="hidden" name="signature" value="{{$data['hash']}}" />
    </form>
</div>

@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/schools-hbl-payment.js') }}"></script>
@endpush
