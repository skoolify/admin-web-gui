@extends('master')

@section('title', Lang::get('messages.school-payment'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<style>
    .card-box:hover {
     -webkit-transform: none !important;
     transform: none !important;
     box-shadow: 0 20px 20px rgba(0,0,0,.1);
     -moz-box-shadow: 0 20px 20px rgba(0,0,0,.1);
     -webkit-box-shadow: 0 20px 20px rgba(0,0,0,.1);
 }
 </style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.school-payment')))

    <div class="row">
        @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
        @endif
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="#" id="search-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12" id="school-payment-detail-list">
            <div class="card-box">

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.school-payment')</header>
                </div>
                {{-- Listing --}}
                <div class="card-body dataListDiv">
                    <div class="table-scrollable" id="dataListTable" >
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="dataList" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="width:200px;">@lang('messages.school-name')</th>
                                    <th>@lang('messages.billing-period')</th>
                                    <th>@lang('messages.status-column')</th>
                                    <th>@lang('messages.net-amount')</th>
                                    <th>@lang('messages.payable-amount')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody class="dataListTableBody">

                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="card-body invoiceDiv" style="display: none;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <p>@lang('messages.invoice-date')</p>
                                </div>
                                <div class="col-md-8">
                                    <p><strong id="invoice-date">--</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <p>@lang('messages.invoice-number')</p>
                                </div>
                                <div class="col-md-8">
                                    <p><strong id="invoice-number">--</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <p>@lang('messages.payment-frequency')</p>
                                </div>
                                <div class="col-md-8">
                                    <p><strong id="payment-frequency">--</strong></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <p>@lang('messages.due-date')</p>
                                </div>
                                <div class="col-md-9">
                                    <p><strong id="due-date">--</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <p>@lang('messages.period')</p>
                                </div>
                                <div class="col-md-9">
                                    <p><strong id="period">--</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <p>@lang('messages.status-column')</p>
                                </div>
                                <div class="col-md-9">
                                    <p><strong id="status">--</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-scrollable" id="school-payment-detail-table-div" >
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="invoice" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th class="w-73">#</th>
                                    <th class="w-200">@lang('messages.branch-name')</th>
                                    <th class="w-200">@lang('messages.total-students')</th>
                                    <th class="w-200">@lang('messages.unit-rate')</th>
                                    <th class="w-200">@lang('messages.net-amount')</th>
                                </tr>
                            </thead>
                            <tbody class="school-payment-detail-table-body">

                            </tbody>
                            <tbody style="font-weight: 500;border-top: 2px solid; display:none;" class="school-payment-detail-table-body2">
                                <tr>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;">@lang('messages.total-amount')</td>
                                    <td style="border: 0px;background: white;" id="totalWoTax"></td>
                                </tr>
                                <tr>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;">@lang('messages.tax-amount') <span id="taxAmountSpan"></span>:</td>
                                    <td style="border: 0px;background: white;" id="taxRate"></td>
                                </tr>
                                <tr >
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;"></td>
                                    <td style="border: 0px;background: white;">@lang('messages.payable-amount')</td>
                                    <td style="border: 0px;background: white;" id="totalAfterTax"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row pull-right buttonsDiv" style="display: none;">
                        <button type="submit" class="btn btn-primary mr-3 payNowBtn">@lang('messages.pay-now')</button>
                        <button type="submit" class="btn btn-danger mr-3 paymentCancelBtn">@lang('messages.cancel-btn')</button>
                    </div>
                </div>
            </div>
        </div>




        <div class="col-sm-12 col-md-12 col-xl-12" id="user-details-form" style="display: none;" >
            <div class="card-box">
                <div class="card-head">
                    <header>@lang('messages.creditDebitCardBillingDetails')</header>
                </div>
                <div class="card-body">
                    <form id="user-detail-form" action="{{route('submit.school.payment.form')}}" method="POST">
                        @csrf
                        <input type="hidden" id="access_key" name="access_key">
                        <input type="hidden" id="profile_id" name="profile_id">
                        <input type="hidden" id="transaction_uuid" name="transaction_uuid">
                        <input type="hidden" name="signed_field_names" value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_address_city,bill_to_address_country,bill_to_address_line1,bill_to_address_postal_code,bill_to_address_state,bill_to_forename,bill_to_phone,bill_to_surname,bill_to_email,consumer_id,customer_ip_address,merchant_defined_data1,merchant_defined_data2,merchant_defined_data4,merchant_defined_data5,merchant_defined_data7,merchant_defined_data20">
                        <input type="hidden" name="unsigned_field_names">
                        <input type="hidden" name="signed_date_time">
                        <input type="hidden" name="locale" value="en">
                        <input type="hidden" name="transaction_type" value="sale">
                        <input type="hidden" name="reference_number" id="reference_number" value="">
                        <input type="hidden" name="amount" id="amount" value="">
                        <input type="hidden" name="currency" id="currency" value="">
                        <input type="hidden" name="bill_to_address_city" id="bill_to_address_city" value="">
                        <input type="hidden" name="bill_to_address_country" id="bill_to_address_country" value="">
                        <div class="card-body row">
                            <div class="form-group col-lg-4 p-t-20 {{ $errors->has('bill_to_forename') ? 'has-error' :'' }}">
                                <label for="bill_to_forename" class="control-label">@lang('messages.first-name')
                                </label>
                                <input maxlength="100" type="text" class="form-control" name="bill_to_forename" type="text" value="{{ old('bill_to_forename') }}" placeholder="{{Lang::get('messages.first-name')}}" data-validation="required length" data-validation-length="max100"  id="bill_to_forename" />
                                {!! $errors->first('bill_to_forename','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group col-lg-4 p-t-20 {{ $errors->has('bill_to_surname') ? 'has-error' :'' }}">
                                <label for="bill_to_surname" class="control-label">@lang('messages.last-name')
                                </label>
                                <input maxlength="100" type="text" class="form-control" name="bill_to_surname" type="text" value="{{ old('bill_to_surname') }}" placeholder="{{Lang::get('messages.last-name')}}" data-validation="required length" data-validation-length="max100"  id="bill_to_surname" />
                                {!! $errors->first('bill_to_surname','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group col-lg-4 p-t-20 {{ $errors->has('bill_to_phone') ? 'has-error' :'' }}">
                                <label for="bill_to_phone" class="control-label">@lang('messages.contact-no')
                                </label>
                                <input maxlength="100" type="text" class="form-control" name="bill_to_phone" type="text" value="{{ old('bill_to_phone') }}" placeholder="{{Lang::get('messages.contact-no')}}" data-validation="required length" data-validation-length="max100"  id="bill_to_phone" />
                                {!! $errors->first('bill_to_phone','<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="col-lg-4 p-t-20">
                                <div class="form-group {{ $errors->has('bill_to_email') ? 'has-error' :'' }}">
                                    <label for="bill_to_email" >@lang('messages.email')
                                    </label>
                                    <input maxlength="100" class = "form-control" name="bill_to_email" type="email" value="{{ old('bill_to_email') }}" id="bill_to_email" placeholder="{{Lang::get('messages.email')}}" data-validation="required email length" data-validation-length="max100"  />
                                    {!! $errors->first('bill_to_email','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-4 p-t-20">
                                <div class="form-group {{ $errors->has('bill_to_address_line1') ? 'has-error' :'' }}">
                                    <label for = "mdl-bill_to_address_line1" >@lang('messages.address')
                                    </label>
                                    <textarea maxlength="100" class = "form-control" name="bill_to_address_line1" id = "bill_to_address_line1" data-validation="required length" data-validation-length="max100" placeholder="{{Lang::get('messages.address')}}">{{ old('bill_to_address_line1') }}</textarea>
                                    {!! $errors->first('bill_to_address_line1','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-4 p-t-20">
                                <div class="form-group {{ $errors->has('bill_to_address_postal_code') ? 'has-error' :'' }}">
                                    <label for="bill_to_address_postal_code" >@lang('messages.postal-code')
                                    </label>
                                    <input maxlength="100" class = "form-control" name="bill_to_address_postal_code" type="text" value="{{ old('bill_to_address_postal_code') }}" id="bill_to_address_postal_code" placeholder="{{Lang::get('messages.postal-code')}}" data-validation="required email length" data-validation-length="max100"  />
                                    {!! $errors->first('bill_to_address_postal_code','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-4 p-t-20">
                                <div class="form-group">
                                    <label for="">@lang('messages.country')</label>
                                    <select id="filter-countries" class="form-control filter-select">
                                        <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.country')</option>
                                        @foreach($countries as $country)
                                        <option {{ session()->get('tempCountryID') == $country->countryID ? 'selected' : '' }} value="{{ $country->countryID }}" data-code="{{$country->countryTwoAlphaCode}}">{{ $country->countryName }}</option>
                                        @endforeach
                                    </select>
                                    <div id="p2-countries" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 p-t-20">
                                <div class="form-group">
                                    <label for="">@lang('messages.city')</label>
                                    <select id="filter-cities" class="form-control filter-select">
                                        <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.city')</option>
                                        @foreach($cities as $city)
                                            <option {{ session()->get('tempBranchID') == $city->cityID ? 'selected' : '' }} value="{{ $city->cityID }}" data-city-name="{{$city->cityName}}">{{ $city->cityName }}</option>
                                        @endforeach
                                    </select>
                                    <div id="p2-cities" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 p-t-20">
                                <div class="form-group {{ $errors->has('bill_to_address_state') ? 'has-error' :'' }}">
                                    <label for="bill_to_address_state" >@lang('messages.state')
                                    </label>
                                    <input maxlength="100" class = "form-control" name="bill_to_address_state" type="text" value="{{ old('bill_to_address_state') }}" id="bill_to_address_state" placeholder="{{Lang::get('messages.state')}}" data-validation="required email length" data-validation-length="max100"  />
                                    {!! $errors->first('bill_to_address_state','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <input type="hidden" name="secret_key" id="secret_key">
                            <input type="hidden" id="hbl_url" name="hbl_url">
                            <input type="hidden" name="consumer_id" id="consumer_id">
                            <input type="hidden" name="customer_ip_address" id="customer_ip_address">
                            {{-- hard coded fields --}}
                            <input type="hidden" name="merchant_defined_data1" value="MobileApp">
                            <input type="hidden" name="merchant_defined_data2" value="YES">
                            <input type="hidden" name="merchant_defined_data4" value="Skoolify">
                            <input type="hidden" name="merchant_defined_data5" value="YES">
                            <input type="hidden" name="merchant_defined_data7" value="0">
                            <input type="hidden" name="merchant_defined_data20" value="NO">
                            <div class="col-lg-12 p-t-20 text-center">
                                <a id="proceedBtn" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">@lang('messages.proceed')</a>
                                <button type="button" id="cancelProceedBtn" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                            </div>
                            <div class="col-lg-12 p-t-20" id="add-form-errors">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="studentDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.linked-student-list')</h4>
        <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
      </div>
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12" style = "max-height:400px;overflow:auto;">
                <table class="table table-striped table-bordered table-responsive-lg table-responsive-xs table-hover table-checkable" id="linked-student-list">
                    <thead >
                        <tr>
                            <th>#</th>
                            <th>@lang('messages.roll-number')</th>
                            <th>@lang('messages.name')</th>
                            <th>@lang('messages.dob')</th>
                            <th>@lang('messages.branch')</th>
                            <th>@lang('messages.shift')</th>
                            <th>@lang('messages.class')</th>
                            <th>@lang('messages.year')</th>
                            <th>@lang('messages.status-active')</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://unpkg.com/@webcreate/infinite-ajax-scroll/dist/infinite-ajax-scroll.min.js"></script>
@endpush
