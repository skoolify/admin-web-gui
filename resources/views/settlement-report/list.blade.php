@extends('master')

@section('title', Lang::get('messages.settlementReport'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.settlementReport')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="settlement-report-form">
                    <div class="card-body">
                        <div class="row">
                            @if (session()->get('user')->isAdmin == 1)
                                @include('partials.components.schools-filter')
                                @include('partials.components.branches-filter')
                            @endif
                            @include('partials.components.date-from-filter')
                            @include('partials.components.settlement-date-to-filter')
                            @include('partials.components.verified-unverified-filter')
                            @include('partials.components.settled-unsettled-filter')

                            <div class="col-lg-3 justify-content-center align-self-center mt-3">
                                <button class="btn btn-primary align-middle" type="submit" id="searchSettlementReport"><i class="fa fa-search"></i> @lang('messages.search')</button>
                                <button class="btn btn-warning" id="exportSettlementReport" type="button" title="Clear"><i class="fa fa-refresh"></i> @lang('messages.export-excel')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <header>@lang('messages.settlementList')</header>
                            </div>
                            @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                                <div class="col-md-2 pl-0">

                                </div>
                                <div class="col-md-1">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table settlement-report-table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.student')</th>
                                    <th>@lang('messages.class')</th>
                                    <th>@lang('messages.invoice-number')</th>
                                    <th>@lang('messages.billing-period')</th>
                                    <th>@lang('messages.tranDate')</th>
                                    <th>@lang('messages.billedAmount')</th>
                                    <th>@lang('messages.fixOrAge')</th>
                                    <th>@lang('messages.rate')</th>
                                    <th>@lang('messages.collectionAmount')</th>
                                    <th>@lang('messages.tax-rate')</th>
                                    <th>@lang('messages.taxValue')</th>
                                    <th>@lang('messages.settledAmount')</th>
                                    <th>@lang('messages.settled')</th>
                                    <th>@lang('messages.settledDate')</th>
                                    <th>@lang('messages.verified')</th>
                                    <th>@lang('messages.verifiedDate')</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
