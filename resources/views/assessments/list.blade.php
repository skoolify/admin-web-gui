@extends('master')

@section('title', 'Assessments')

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => 'Assessments Management'))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> Filters</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.students-filter')

                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('assessmentDate') ? 'has-error' :'' }}">
                                    <label for="assessmentDate">Assessment Date
                                            <span class="required"> * </span>
                                    </label>
                                    <input class = "form-control" name="assessmentDate" type = "date" id = "assessmentDate" placeholder="Assessment Date" value="{{ \Carbon\Carbon::now()->toDateString() }}" required />
                                    {!! $errors->first('assessmentDate','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('assessments.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>Assessments List</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <!-- <div class="btn-group "> -->
                        <button type="button" class="btn btn-primary pull-right btn-round" id="add-add-assessments"><i class="fa fa-plus"></i> ADD NEW</button>
                    <!-- </div> -->
                    @endif
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>                                    
                                    <th>Non Subject Name</th>
                                    <th>Non Subject Notes</th>
                                    <th>Non Subject Grade</th>
                                    <th>Assessment Notes</th>
                                    <th>Assessment Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="add-assessments-table">
            <div id="bulk-assessments-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-box">

                <div class="card-head">
                    <header>Add Assessments</header>
                    <!-- <div class="btn-group "> -->
                        <button type="button" class="btn btn-primary pull-right mr-2" id="submit-add-assessments">Submit</button>
                        <button type="button" class="btn btn-danger pull-right mr-1" id="cancel-add-assessments">Cancel</button>
                    <!-- </div> -->
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <form action="#" id="bulk-assessments-form">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="assessments-add-table" style="width: 100% !important;">
                                <thead>
                                    <tr>
                                        <th>Roll #</th>
                                        <th>Student Name</th>
                                        <th>Notes</th>
                                        <th class="text-center">Publish</th>
                                    </tr>
                                </thead>
                                <tbody id="assessments-add-tbody">
                                    <tr>
                                        <td colspan="4" class="text-center"><strong>NO STUDENTS FOUND</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
