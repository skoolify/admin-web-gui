<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="assessments-form" action="{{ url('assessments/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('assessmentNotes') ? 'has-error' :'' }}">
                        <label for="assessmentNotes" >Assessment Notes</label>
                        <textarea class = "form-control" name="assessmentNotes" id = "assessmentNotes" required>{{ old('assessmentNotes') }}</textarea>
                        {!! $errors->first('assessmentNotes','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


                <div class="col-lg-3 p-t-20">
                    <label for="">Published</label>
                    <div class="radio p-0">
                        <input type="radio" name="isPublish" id="activeField1" data-status-1="1" value="1" checked />
                        <label for="activeField1">
                            Yes
                        </label>
                    </div>
                    <div class="radio p-0">
                        <input type="radio" name="isPublish" id="activeField0" data-status-0="0" value="0" />
                        <label for="activeField0">
                            No
                        </label>
                    </div>
                </div>


                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>

<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="assessment-detail-form-container">
    <div class="card-box">
        
        <form method="POST" id="assessments-detail-form" action="{{ url('assessments-detail/add') }}">
            <div id="assessment-submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="hiddenAssessmentID" value="" />

                <div class="col-md-12">
                    <h4>Assessment Detail</h4>
                    <hr>
                </div>

                @include('partials.components.non-subjects-filter')

                <div class="col-lg-3 p-t-0">
                    <div class="form-group {{ $errors->has('gradeNS') ? 'has-error' :'' }}">
                        <label for="gradeNS">Grade Non-Subjects
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="gradeNS" type = "text" id = "gradeNS" placeholder="Grade Non-Subject" />
                        {!! $errors->first('gradeNS','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


                <div class="col-lg-3 p-t-0">
                    <div class="form-group {{ $errors->has('notesNs') ? 'has-error' :'' }}">
                        <label for="notesNs" >Notes Non-Subject</label>
                        <textarea class = "form-control" name="notesNs" id = "notesNs">{{ old('notesNs') }}</textarea>
                        {!! $errors->first('notesNs','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-assessment-detail-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                    <button type="button" id="hide-assessment-detail-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                </div>

                <div class="col-lg-12 p-t-20" id="assessment-details-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>

