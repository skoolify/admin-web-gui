<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="import-file-form-container">
    <div class="card-box">
        <form method="POST" id="import-students-excel-form" action="{{ route('excel.import') }}" enctype="multipart/form-data">
        @csrf
            <div class="card-body row">

                <div class="col-lg-3 p-t-20">
                    <div class="form-group">
                        <label for="studentName">@lang('messages.upload-excel-file'):
                        </label>
                        <input accept=".xls,.xlsx,.csv" class="form-control" name="studentsExcelFile" type="file" id="studentsExcelFile" data-validation="required"/>
                        <p><small><em>@lang('messages.excel-file-format')</em></small></p>
                    </div>
                </div>

                <div class="col-lg-3" style="margin-top: 52px;">
                    <div class="form-group">
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary" href="{{ asset('/assets/sheets/sample-sheet.xlsx') }}" download="sample-sheet.xlsx"><i class="fa fa-download  "></i> @lang('messages.download-template')</a>
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-import-excel-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-import-excel-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>
                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
    </div>
</div>

@push('extra-scripts')
<!-- dropzone -->
<script src="{{ asset('assets/plugins/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/plugins/dropzone/dropzone-call.js') }}" ></script>
@endpush
