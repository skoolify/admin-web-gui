@extends('master')

@section('title', Lang::get('messages.import-excel-file'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone -->
<link href="{{ asset('assets/plugins/dropzone/dropzone.css') }}" rel="stylesheet" media="screen">
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.import-excel-file')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                        </div>
                    </div>
                </form>
              
            </div>
        </div>
       
        @include('import-excel.import')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.upload-excel-file')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-import-excel-form-container"><i class="fa fa-plus"></i> @lang('messages.import-excel')</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
       <div style="text-align:center">  
           <h4 class="" id="billing-period-modal" style="color:#337ab7;font-weight:bold;position: absolute;right: 0;left: 0;" > @lang('messages.preview-data')</h4>

        </div>
        <a class="" data-dismiss="modal" aria-label="Close" style="z-index:1000">
          <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
        </a>
      </div>
      <div id="errorDiv" style="color:red;text-align: center;">
          
      </div>
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"  style="height:400px; overflow-y:scroll; margin-bottom:20px;">
                <table class="table table-border table-hover" id="excel-sheet-table">
                    <thead id="tableHead">
                        <tr>
                            <th>@lang('messages.s-number')</th>
                            <th>@lang('messages.student-name')</th>
                            <th>@lang('messages.gender')</th>
                            <th>@lang('messages.class')</th>
                            <th>@lang('messages.class-section')</th>
                            <th>@lang('messages.roll-number')</th>
                            <th>@lang('messages.dob')</th>
                            <th>@lang('messages.father-name')</th>
                            <th>@lang('messages.father-cnic')</th>
                            <th>@lang('messages.father-mb-number')</th>
                            <th>@lang('messages.father-email')</th>
                            <th>@lang('messages.mother-name')</th>
                            <th>@lang('messages.mother-cnic')</th>
                            <th>@lang('messages.mother-mb-number')</th>
                            <th>@lang('messages.mother-email')</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  class="p-2 text-center center" colspan="12">@lang('messages.record-not-found')</td>
                        </tr>
                    </tbody>
                </table>
                <span id="success-span" style="color:green"></span>
                <span id="error-span" style="color:red" ></span>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" id="proceed-excel-sheet" class="btn btn-primary" data-dismiss="modal">@lang('messages.proceed')</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
@endpush
