<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="school-branches-form" action="{{ url('school-branches/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />
                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.branch-details')</h3><hr>
                </div>
                <div class="form-group col-lg-4 p-t-20 {{ $errors->has('branchName') ? 'has-error' :'' }}">
                    <label for="branchName" class="control-label">@lang('messages.branch-name') 
                    </label>
                    <input maxlength="100" type="text" class="form-control" name="branchName" type="text" value="{{ old('branchName') }}" placeholder="{{Lang::get('messages.branch-name')}}" data-validation="required length" data-validation-length="max100"  id="branchName" />
                    {!! $errors->first('branchName','<span class="help-block">:message</span>') !!}
                </div>
                <div class=" form-group col-lg-4 p-t-20 {{ $errors->has('branchCode') ? 'has-error' :'' }}"> 
                    <label for="branchCode" >@lang('messages.branch-code')</label>
                    <input maxlength="45" class="form-control" name="branchCode" type="text" value="{{ old('branchCode') }}" data-validation="length" data-validation-length="max45" id="branchCode" placeholder="{{Lang::get('messages.branch-code')}}" />
                    {!! $errors->first('branchCode','<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group col-lg-4 p-t-20 {{ $errors->has('branchPrincipleName') ? 'has-error' :'' }}"> 
                    <label for="branchPrincipleName" >@lang('messages.branch-principle-name')
                    </label>
                    <input maxlength="100" class = "form-control" name="principleName" value="{{ old('branchPrincipleName') }}" type="text" id="branchPrincipleName" placeholder="{{Lang::get('messages.branch-principle-name')}}" data-validation="required length" data-validation-length="max100"  />
                    {!! $errors->first('branchPrincipleName','<span class="help-block">:message</span>') !!}
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchEmail') ? 'has-error' :'' }}">
                        <label for="branchEmail" >@lang('messages.branch-email')
                        </label>
                        <input maxlength="100" class = "form-control" name="email" type="email" value="{{ old('branchEmail') }}" id = "branchEmail" data-validation="required email length" data-validation-length="max100" placeholder="{{Lang::get('messages.branch-email')}}" />
                        {!! $errors->first('branchEmail','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchContact1') ? 'has-error' :'' }}" >
                        <label for="branchContact1" >@lang('messages.contact-1')
                        </label>
                        <input class="form-control" name="contact1" type="text" value="{{ old('branchContact1') }}" id = "branchContact1" data-validation="required number length" data-validation-length="max45" maxlength="45" placeholder="{{Lang::get('messages.contact-1')}}" />
                        {!! $errors->first('branchContact1','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchContact2') ? 'has-error' :'' }}">
                        <label for="branchContact2" >@lang('messages.contact-2')</label>
                        <input class="form-control" name="contact2" type="text" value="{{ old('branchContact2') }}"  id = "branchContact2" data-validation-length="max45" placeholder="{{Lang::get('messages.contact-2')}}"/>
                        {!! $errors->first('branchContact2','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchContact3') ? 'has-error' :'' }}">
                        <label for="branchContact3" >@lang('messages.contact-3')</label>
                        <input class="form-control" name="contact3" type="text" value="{{ old('branchContact3') }}"  id = "branchContact3" data-validation-length="max45" placeholder="{{Lang::get('messages.contact-3')}}"/>
                        {!! $errors->first('branchContact3','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                    
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchGpsCoords') ? 'has-error' :'' }}" required>
                        <label for="branchGpsCoords" >@lang('messages.GPS-coordinates')
                        </label>
                        <input maxlength="100" class="form-control" name="gpsCordinates" type="text" value="{{ old('branchGpsCoords') }}"  id = "branchGpsCoords" data-validation="required length" data-validation-length="max100" placeholder="{{Lang::get('messages.GPS-coordinates')}}" />
                        {!! $errors->first('branchGpsCoords','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchAddress1') ? 'has-error' :'' }}">
                        <label for = "mdl-branchAddress1" >@lang('messages.address-1')
                        </label>
                        <textarea maxlength="100" class = "form-control" name="address1" id = "branchAddress1" data-validation="required length" data-validation-length="max100" placeholder="{{Lang::get('messages.address-1')}}">{{ old('branchAddress1') }}</textarea>
                        {!! $errors->first('branchAddress1','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchAddress2') ? 'has-error' :'' }}">
                        <label for="branchAddress2" >@lang('messages.address-2')</label>
                        <textarea maxlength="100" class = "form-control" data-validation="length" data-validation-length="max100" name="address2" id = "branchAddress2" placeholder="{{Lang::get('messages.address-2')}}">{{ old('branchAddress2') }}</textarea>
                        {!! $errors->first('branchAddress2','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('branchAddress3') ? 'has-error' :'' }}">
                        <label for="branchAddress3" >@lang('messages.address-3')</label>
                        <textarea maxlength="100" class = "form-control" data-validation="length" data-validation-length="max100" name="address3" id = "branchAddress3" placeholder="{{Lang::get('messages.address-3')}}">{{ old('branchAddress3') }}</textarea>
                        {!! $errors->first('branchAddress3','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group">
                        <label for="marksTotal"></label>
                    </div>
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="isActive" id="isActiveInput" value="0">
                        <input class="isActive" id="isActive" type="checkbox">
                        <label for="isActive"><p style="margin-top: -3px;">@lang('messages.is-active')</p></label>
                    </div>
                </div>


                {{-- @include('partials.active-field') --}}

                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.bank-details')</h3><hr>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('bankName') ? 'has-error' :'' }}">
                        <label for="bankName">@lang('messages.bank-name')<span class=""></span>
                        </label>
                        <input maxlength="200" class="form-control" data-validation=" length" data-validation-length="max200" name="bankName" type="text" id="bankName" placeholder="{{Lang::get('messages.bank-name')}}" value=""  />
                        {!! $errors->first('bankName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('bankBranch') ? 'has-error' :'' }}">
                        <label for="bankBranch">@lang('messages.branch-name')<span class=""></span>
                        </label>
                        <input maxlength="200" class="form-control" data-validation=" length" data-validation-length="max200" name="bankBranch" type="text" id="bankBranch" placeholder="{{Lang::get('messages.branch-name')}}" value=""  />
                        {!! $errors->first('bankBranch','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('bankAccNo') ? 'has-error' :'' }}">
                        <label for="bankAccNo">@lang('messages.account-number')<span class=""></span>
                        </label>
                        <input maxlength="200" class="form-control" data-validation=" length" data-validation-length="max200" name="bankAccNo" type="text" id="bankAccNo" placeholder="{{Lang::get('messages.account-number')}}" value=""  />
                        {!! $errors->first('bankAccNo','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
