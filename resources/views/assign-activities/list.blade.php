@extends('master')

@section('title', Lang::get('messages.assign-activity-heading'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.assign-activity-heading')))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="">@lang('messages.assign-activity-filter-activities')</label>
                                    <select name="activityID" id="filter-activities" class="form-control filter-select" >
                                        <option value="">{{ isset($all) && $all ? $all : 'Select' }} @lang('messages.assign-activity-dropdown')</option>
                                    </select>
                                    <div id="p2-activities" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                <label for="marksTotal">@lang('messages.assign-activity-week-day')
                                            <span class="required"> * </span>
                                    </label>
                                    <input type="hidden" class="ids-for-delete" name="idsForDelete[]">
                                    <select name="weekdayNo" id="filter-week-day" multiple class="form-control filter-select">
                                        <option value="1">@lang('messages.sunday')</option>
                                        <option value="2">@lang('messages.monday')</option>
                                        <option value="3">@lang('messages.tuesday')</option>
                                        <option value="4">@lang('messages.wednesday')</option>
                                        <option value="5">@lang('messages.thursday')</option>
                                        <option value="6">@lang('messages.friday')</option>
                                        <option value="7">@lang('messages.saturday')</option>
                                    </select>
                                    {!! $errors->first('marksTotal','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            @include('partials.components.students-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('assign-activities.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif
                    <div id="non-assessment-case" class="card-head">
                        <header>@lang('messages.assign-activity-list')</header>
                        @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                        <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn') / <i class="fa fa-pencil"></i> @lang('messages.edit-btn')</button>
                        @endif
                    </div>
                
                <div class="card-body non-assessment-body" >
                    <div class="table-scrollable">
                        <div class="table-scrollable" >
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="example44">
                                <thead>
                                    <tr>
                                        <th width="10%">#</th>  
                                        <th>@lang('messages.roll-number')</th>     
                                        <th>@lang('messages.day')</th>   
                                        <th>@lang('messages.student')</th>
                                        <th>@lang('messages.action-column')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>


@endpush
