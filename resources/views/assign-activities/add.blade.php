<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="assign-activities-form" action="#" role="form"> 
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">
                    <table class="table table-striped  table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="assign-activity-table">
                        <thead>
                            <tr>
                                <th width="3%">@lang('messages.s-number')</th>
                                <th width="10%">@lang('messages.student')</th>
                                <th width="10%">@lang('messages.roll-number')</th>
                                <!-- <th width="10%">Select</th> -->
                                <th width="10%" class="activities-student-multiple" id="activities-student-multiple" data-key="readAccess">@lang('messages.select-all') <i class="fa fa-check-square-o"></i>
                                    <div class="hidden checkbox checkbox-aqua d-flex">
                                    <div style="margin-right: 15px;">@lang('messages.select-all')</div>
                                        <input  id="select-student-result-all" data-id="select-student-result" class="hidden studentCheckBoxAll" type="checkbox">
                                        <label for="select-student-result-all" style="margin-top: 3px; margin-left:11px;">
                                        </label>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6"><strong style="color:#188ae2;font-weight:bold">Select Section First</<strong></td>
                        </tr>
                        </tbody>
                    </table>
                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
