@extends('master')

@section('title', Lang::get('messages.students-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone -->
<link href="{{ asset('assets/plugins/dropzone/dropzone.css') }}" rel="stylesheet" media="screen">
<style>
    * {
      box-sizing: border-box;
    }

    body {
      font: 16px Arial;
    }

    /*the container must be positioned relative:*/
    .autocomplete {
      position: relative;
      display: inline-block;
    }

    input {
      border: 1px solid #d6d5d5;
      background-color: #ffffff;
      padding: 10px;
      font-size: 16px;
    }

    input[type=text] {
      background-color: #ffffff;
      width: 100%;
    }

    input[type=submit] {
      background-color: #2fa8ff;
      color: #fff;
      cursor: pointer;
    }

    .autocomplete-items {
      position: absolute;
      margin-left: 15px;
      width: 262px;
      border: 1px solid #d2d6de;
      border-bottom: none;
      border-top: none;
      z-index: 99;
      /*position the autocomplete items to be the same width as the container:*/
      top: 100%;
      left: 0;
      right: 0;
    }

    .autocomplete-items div {
      padding: 10px;
      cursor: pointer;
      background-color: #fff;
      border-bottom: 1px solid #d4d4d4;
    }

    /*when hovering an item:*/
    .autocomplete-items div:hover {
      background-color: #e9e9e9;
    }

    /*when navigating through the items using the arrow keys:*/
    .autocomplete-active {
      background-color: DodgerBlue !important;
      color: #ffffff;
    }
    </style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.students-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                        <div class="col-lg-3">
                            <div class="form-group {{ $errors->has('searchByName') ? 'has-error' :'' }}">
                                <label for="searchByName">@lang('messages.name')
                                </label>
                                <input class = "form-control" name="searchByName" type = "text" id = "searchByName" placeholder="@lang('messages.student-name')" value="{{ old('searchByName') }}"  />
                                <div class="invalid-feedback">
                                    @lang('messages.required-field-error')
                                </div>
                                {!! $errors->first('searchByName','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group {{ $errors->has('searchByRollNumber') ? 'has-error' :'' }}">
                                <label for="searchByRollNumber">@lang('messages.roll-number')
                                </label>
                                <input class = "form-control" name="searchByRollNumber" type = "text" id = "searchByRollNumber" placeholder="@lang('messages.studentRollNo')" value="{{ old('searchByRollNumber') }}"  />
                                <div class="invalid-feedback">
                                    @lang('messages.roll-number-error')
                                </div>
                                {!! $errors->first('searchByRollNumber','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-lg-3 justify-content-center align-self-center">
                            {{-- <div class="form-group"> --}}
                                <button class="btn btn-primary search-student align-middle custom-float-right" style="
    margin-top: 19px;" type="button"><i class="fa fa-search" ></i> @lang('messages.search')</button>
                            {{-- </div> --}}
                        </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        @include('students.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.studentsList')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    {{-- <button type="button" class="btn btn-round btn-danger pull-right add-note" id="show-add-form-container" data-recordID="1" data-studentUUID="11b0f25f-7ab0-11ea-94fd-22000a29cd86"><i class="fa fa-plus"></i> TEST</button> --}}
                    {{-- <button type="button" class="btn btn-round btn-danger pull-right view-note" id="show-add-form-container"><i class="fa fa-plus"></i> NOTES</button> --}}
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.student-name')</th>
                                    <th class="dob-width">@lang('messages.dob')</th>
                                    <th>@lang('messages.gender')</th>
                                    <th>@lang('messages.class-section')</th>
                                    <th>@lang('messages.roll-number')</th>
                                    <th>@lang('messages.status-active')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="fathersDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color:green" > @lang('messages.parents-detail')</h4>
        <a class="closeModalDiv" data-dismiss="modal" aria-label="Close" style="z-index:1000">
          <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
          </a>
      </div>
      <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12 ">
                <table class="table table-striped table-bordered table-hover table-checkable" id="parent-detail-table" style="width:100% !important">
                    <thead style="width:100% !important">
                        <tr>
                            <th><small>@lang('messages.s-number')</small></th>
                            <th><small>@lang('messages.name')</small></th>
                            <th><small>@lang('messages.email')</small></th>
                            <th><small>@lang('messages.phone-number')</small></th>
                            <th><small>@lang('messages.cnic')</small></th>
                            <th><small>@lang('messages.type')</small></th>
                            <th><small>@lang('messages.reg-date')</small></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6">@lang('messages.record-not-found')</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addNoteModal" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel" style="color:green" > @lang('messages.add-note')</h4>
          <a class="closeModalDiv" data-dismiss="modal" aria-label="Close" style="z-index:1000">
            <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
            </a>
        </div>
        <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="row">
                    <label class="col-md-1 pt-2">@lang('messages.tags')</label>
                    <div class="col-md-9">
                      <input type="hidden" id="tagText" name="tagText">
                      <input type="hidden" id="tagUUID" name="tagUUID">
                        <select class="form-control tagsInput" id="noteTag">
                        </select>
                    </div>
                    <div class="col-md-2" style="margin-top:3px;">
                      <a href="student-tag" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                    </div>
                  </div>
              </div>
                <div class="col-md-12 mb-2 mt-2">
                    {{-- <div class="row text-danger pt-2 tagSuccessDiv" style="display: none;">
                        <div class="col-md-1"></div>
                        <div class="col-md-11 successDiv">New tag added successfully!</div>
                    </div> --}}
                </div>
                <div class="col-md-12 tagFieldDiv">
                    <div class="row">
                        <label class="col-md-1 pt-2">@lang('messages.note')</label>
                        <div class="col-md-11">
                            <input type="hidden" name="" id="studentUUID">
                            <textarea class="form-control" name="" id="studentNote" cols="60" rows="5"></textarea>
                        </div>
                    </div>
                </div>
          </div>
      </div>
        <div class="modal-footer tagFieldDiv">
            <div class="noteSubmitError" style="display: none;"></div>
            <button type="button" class="btn btn-primary" id="submitNotesDataBtn">@lang('messages.submit-btn')</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.cancel-btn')</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="notesListModal" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="noteModalLabel" style="color:green" > @lang('messages.notes-list')</h4>
          <a class="closeModalDiv" data-dismiss="modal" aria-label="Close" style="z-index:1000">
            <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
            </a>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 noteEditForm" style="display: none;">
              <div class="row">
                <div class="col-md-12">
                    <div class="row">
                      <label class="col-md-1 pt-2">@lang('messages.tags')</label>
                      <div class="col-md-11">
                        <input type="hidden" id="tagTextEdit" name="tagText">
                        <input type="hidden" id="tagUUIDEdit" name="tagUUID">
                          <select class="form-control tagsInputEdit" id="noteTagEdit">
                          </select>
                      </div>
                    </div>
                </div>
                  <div class="col-md-12 mb-2 mt-2">

                  </div>
                  <div class="col-md-12 tagFieldDivEdit">
                      <div class="row">
                          <label class="col-md-1 pt-2">@lang('messages.note')</label>
                          <div class="col-md-11">
                              <input type="hidden" name="" id="studentUUIDEdit">
                              <input type="hidden" name="" id="studentNotesUUIDEdit">
                              <textarea class="form-control" name="" id="studentNoteEdit" cols="60" rows="5"></textarea>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="modal-footer tagFieldDiv">
                <div class="noteSubmitError"></div>
                  <button type="button" class="btn btn-primary" id="updateNotesDataBtn" >@lang('messages.submit-btn')</button>
                  <button type="button" class="btn btn-danger cancelEditNote">@lang('messages.cancel-btn')</button>
              </div>
            </div>
            <div class="col-md-12 notesDataTable">
                <table class="table table-striped table-bordered table-hover table-checkable" id="notes-detail-table" style="width:100% !important">
                    <thead style="width:100% !important">
                        <tr>
                            <th>@lang('messages.s-number')</th>
                            <th>@lang('messages.teacher-name')</th>
                            <th>@lang('messages.tag')</th>
                            <th>@lang('messages.notes')</th>
                            <th>@lang('messages.date')</th>
                            <th>@lang('messages.action-column')</th>
                        </tr>
                    </thead>
                    <tbody id="notes-error">

                    </tbody>
                </table>
            </div>
          </div>
      </div>
        <div class="modal-footer tagFieldDiv">
            <div class="text-danger noteSubmitError" style="display: none;"></div>
          <button type="button" class="btn btn-danger closeBtn" data-dismiss="modal">@lang('messages.close')</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="{{ asset('assets/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/select2-searchInputPlaceholder.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
@endpush
