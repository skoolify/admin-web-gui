
<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="students-form" action="{{ url('students/add') }}" enctype="multipart/form-data">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('studentName') ? 'has-error' :'' }}">
                        <label for="studentName">@lang('messages.student-name')
                        </label>
                        <input maxlength="100" class ="form-control" name="studentName" type= "text" id ="studentName" placeholder="{{Lang::get('messages.student-name')}}" data-validation="required length" data-validation-length="max100" value="{{old('studentName') }}"  />
                        {!! $errors->first('studentName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('classYear') ? 'has-error' :'' }}">
                        <label for="dob">@lang('messages.dob')
                        </label>
                        <input class = "form-control" name="dob" type = "date" id = "dob" placeholder="DOB" value="{{ old('dob') }}"  data-validation="required" data-validation-error-msg="Date of birth is required." />
                        {!! $errors->first('dob','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('rollNo') ? 'has-error' :'' }}">
                        <label for="rollNo">@lang('messages.roll-number')
                        </label>
                        <input maxlength="50" class = "form-control" name="rollNo" type = "text" id = "rollNo" placeholder="{{Lang::get('messages.roll-number')}}" value="{{ old('rollNo') }}" data-validation="required length" data-validation-length="max50" />
                        {!! $errors->first('rollNo','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('classYear') ? 'has-error' :'' }}">
                        <label for="dob">@lang('messages.enrolment-date')
                        </label>
                        <input class="form-control" name="enrollDate" type="date" id="enrollDate" value="{{ old('enrollDate') }}"  data-validation="required" data-validation-error-msg="{{Lang::get('messages.enrolment-date-is-required')}}" />
                        {!! $errors->first('enrollDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-3 p-t-20">
                    <div class="form-group">
                        <label for="">@lang('messages.gender')</label>
                        <select name="gender" id="gender" class="form-control" data-validation="required" data-validation-error-msg="{{Lang::get('messages.gender-is-required')}}">
                            <option value="">@lang('messages.select-gender')</option>
                            <option value="M">@lang('messages.male')</option>
                            <option value="F">@lang('messages.female')</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20" id="upload-school-logo">
                    <div class="form-group {{ $errors->has('picture') ? 'has-error' :'' }}">
                        <label class="control-label col-md-12" for="picture">@lang('messages.picture') <button type="button" class="btn btn-xs btn-danger pull-right hidden" id="reset-logo-image" title="Remove Image"><i class="fa fa-trash"></i></button> <button type="button" class="btn btn-xs btn-danger pull-right hidden" id="reset-logo-image-edit"  title="Remove Image"><i class="fa fa-trash"></i></button></label>
                        <input class = "form-control" type = "file" id = "logoImg" name = "logoImg" accept="image/x-png,image/gif,image/jpeg" placeholder="Picture" value="{{ old('picture') }}"  />
                        <input type="hidden" name="picture" id="hiddenPicture" />
                        {!! $errors->first('picture','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('tagID') ? 'has-error' :'' }}">
                        <label for="tagID">@lang('messages.tagId')
                        </label>
                        <input maxlength="36" class = "form-control" name="tagID" type = "text" id = "tagID" placeholder="{{Lang::get('messages.tagId')}}" value="{{ old('tagID') }}" data-validation="required length" data-validation-length="max36" />
                        {!! $errors->first('tagID','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <img src="{{asset('assets/img/id-card.png')}}" width="35" alt="" id="generateUUID" style="margin-top:35px;" data-toggle="tooltip" data-placement="top" title="Generate UUID">
                    <img class="ml-2" src="{{asset('assets/img/qrcode.png')}}" width="27" alt="" id="generateQR" style="margin-top:35px;" data-toggle="tooltip" data-placement="top" title="Generate QR">
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="checkbox checkbox-aqua" style="padding-left:3px;margin-top:35px">
                        <input type="hidden" name="isActive" id="isActive" value="0" />
                        <input class="isActiveCheck" id="isActiveCheck" type="checkbox">
                        <label for="isActiveCheck">
                        <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                        </label>
                    </div>
                </div>


                <div class="col-lg-3 p-t-20 hidden" id="existing-school-logo">
                    <label class="control-label col-md-12">@lang('messages.picture') <button type="button" class="btn btn-xs btn-primary pull-right" id="camera-icon"  title="Edit Image"><i class="fa fa-camera"></i></button></label>
                    <input type="file" id="imgupload" style="display:none" name = "logoImg" accept="image/x-png,image/gif,image/jpeg" />
                    <img src="" style="margin: auto" class="img-responsive" id="existing-logo" height="75" width="75" alt="Picture" />
                </div>

                {{-- @include('partials.active-field') --}}

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>

@push('extra-scripts')
<!-- dropzone -->
<script src="{{ asset('assets/plugins/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('assets/plugins/dropzone/dropzone-call.js') }}" ></script>
@endpush
