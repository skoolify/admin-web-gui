<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="contract-form">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('roleName') ? 'has-error' :'' }}">
                        <label for="roleName">@lang('messages.version')
                        </label>
                        <input class = "form-control" name="version" type = "text" id = "version" value="{{ old('version') }}" data-validation="required" data-validation-error-msg="Version is required.">
                        {!! $errors->first('roleName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @include('partials.published-field')
                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.contract')</h3><hr>
                </div>
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('contract') ? 'has-error' :'' }}">
                        <textarea name="contractText" id="contractsummernote" style="width:100%;height:400px;" data-id="contract" class="contract-summernote" placeholder="Contract" data-validation="required" data-validation-error-msg="Description is required.">
                        </textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.fee-charges')</h3><hr>
                </div>
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('contract') ? 'has-error' :'' }}">
                        <textarea name="chargesText" id="chargessummernote" style="width:100%;height:400px;" data-id="charges" class="charges-summernote" placeholder="Charges Text" data-validation="required" data-validation-error-msg="Description is required.">
                        </textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.collection-charges')</h3><hr>
                </div>
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('contract') ? 'has-error' :'' }}">
                        <textarea name="collectionText" id="collectionsummernote" style="width:100%;height:400px;" data-id="collection" class="collection-summernote" placeholder="Collection Text" data-validation="required" data-validation-error-msg="Description is required.">
                        </textarea>
                    </div>
                </div>
                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
