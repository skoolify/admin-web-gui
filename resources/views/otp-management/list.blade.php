@extends('master')
@section('title', Lang::get('messages.OTP-management'))
@section('extra-css')
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.OTP-management')))
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('searchMobileNumber') ? 'has-error' :'' }}">
                                    <label for="searchMobileNumber">@lang('messages.mobile-number')</label>
                                    <input class = "form-control" name="searchMobileNumber" type = "text" id = "searchMobileNumber" placeholder="+9230012345678" value="{{ old('searchMobileNumber') }}"  />
                                    {!! $errors->first('searchMobileNumber','<span class="help-block">:message</span>') !!}
                                    <p id="phone-error" class="text-danger phone-error"></p>
                        {!! $errors->first('mobileNo','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-1 justify-content-center align-self-center">
                                <button class="btn btn-primary search-otp align-middle mt-3" type="submit"><i class="fa fa-search"></i> @lang('messages.search')</button>
                            </div>
                            <div class="col-lg-1 justify-content-center align-self-center ml-4">
                                <button class="btn btn-warning clear-data align-middle mt-3" type="button"><i class="fa fa-refresh"></i> @lang('messages.clear') </button>
                             </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>                                    
                                    <th>@lang('messages.name')</th>
                                    <th>@lang('messages.mobile')</th>
                                    <th>@lang('messages.OTP')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewOtpDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document" style="width: 600px;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.OTP-detail')</h4>
        <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
      </div>
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12" style = "max-height:400px;overflow:auto;">
                <table class="table" id="fee-detail">
                    <tbody>
                        <tr>
                            <td class="p-1 br-1">@lang('messages.token')</td>
                            <td class="p-1" id="otpToken" style="word-break: break-all;"> </td>
                        </tr>
                        <tr>
                            <td class="p-1 br-1">@lang('messages.device')</td>
                            <td class="p-1" id="otpDevice"> </td>
                        </tr>
                        <tr>
                            <td class="p-1 br-1">@lang('messages.app-version')</td>
                            <td class="p-1" id="otpAppVersion"> </td>
                        </tr>
                        <tr>
                            <td class="p-1 br-1">@lang('messages.token-status')</td>
                            <td class="p-1" id="otpTokenStatus"> </td>
                        </tr>
                        <tr>
                            <td class="p-1 br-1">@lang('messages.OS-version')</td>
                            <td class="p-1" id="otpOsVersion"> </td>
                        </tr>
                        <tr>
                            <td class="p-1 br-1">@lang('messages.Source-UUID')</td>
                            <td class="p-1" id="otpSourceUUID"> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
