<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="users-form" action="{{ url('users/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />
                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('fullName') ? 'has-error' :'' }}">
                        <label for="fullName">@lang('messages.full-name')
                        </label>
                        <input maxlength="100" class = "form-control" name="fullName" type = "text" id = "fullName" placeholder="{{Lang::get('messages.full-name')}}" value="{{ old('fullName') }}" data-validation="required length" data-validation-length="max100" />
                        {!! $errors->first('fullName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('mobileNo') ? 'has-error' :'' }}">
                        <label for="mobileNo">@lang('messages.username-mobile')
                        </label>
                        {{-- <div class="row">
                            <div class="col-md-12"> --}}
                                <input class = "form-control mobile-no-validation" type = "text" id = "mobileNo" placeholder="{{Lang::get('messages.username-mobile')}}" name ="mobileNo" value="{{ old('mobileNo') }}" data-validation="required" data-validation-error-msg="Mobile number / username is required"  />
                                <label class="hidden" id="mobileNoLabel">@lang('messages.user-already-exist')</label>
                                <!-- name="userName" -->
                                <!-- <input type="hidden" name="mobileNo" id="hiddenMobileNo"  /> -->
                            {{-- </div>
                        </div> --}}
                        {!! $errors->first('mobileNo','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20" id="passwordDiv" >
                    <div class="form-group {{ $errors->has('password') ? 'has-error' :'' }}">
                        <label for="password">@lang('messages.password')
                        </label>
                        <input maxlength="100" class = "form-control" type = "password" id = "passwordInput" placeholder="{{Lang::get('messages.password')}}" data-validation="required length" data-validation-length="max100"  />
                        <input name="password" type = "hidden" id = "password" />
                        {!! $errors->first('password','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


                <div class="col-lg-3 p-t-20">
                    @if(session()->get('user')->isAdmin === 1)
                    <div class="checkbox checkbox-aqua">
                            <input type="hidden" name="isAdmin" id="isAdmin" value="0" />
                            <input class="isAdminCheck" id="isAdminCheck" type="checkbox">
                            <label for="isAdminCheck">
                            <p style="margin-top: -3px;">@lang('messages.admin')</p>
                            </label>
                        </div>
                    @endif
                    <div class="checkbox checkbox-aqua" style="margin-top:{{session()->get('user')->isAdmin != 1 ? '35px' : '0px'}}">
                        <input type="hidden" name="isActive" id="isActive" value="0" />
                        <input class="isActiveCheck" id="isActiveCheck" type="checkbox">
                        <label for="isActiveCheck">
                        <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                        </label>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('emailAddress') ? 'has-error' :'' }}">
                        <label for="emailAddress">@lang('messages.email-address')
                        </label>
                        <input maxlength="100" class = "form-control" name="emailAddress" type = "email" id = "emailAddress" placeholder="{{Lang::get('messages.email-address')}}" value="{{ old('emailAddress') }}" data-validation="length" data-validation-length="max100" />
                        {!! $errors->first('emailAddress','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('tagID') ? 'has-error' :'' }}">
                        <label for="tagID">@lang('messages.tagId')
                        </label>
                        <input maxlength="36" class = "form-control" name="tagID" type = "text" id = "tagID" placeholder="{{Lang::get('messages.tagId')}}" value="{{ old('tagID') }}" data-validation="required length" data-validation-length="max36" />
                        {!! $errors->first('tagID','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <img src="{{asset('assets/img/id-card.png')}}" width="35" alt="" id="generateUUID" style="margin-top:35px;" data-toggle="tooltip" data-placement="top" title="Generate UUID">
                    <img class="ml-2" src="{{asset('assets/img/qrcode.png')}}" width="27" alt="" id="generateQR" style="margin-top:35px;" data-toggle="tooltip" data-placement="top" title="Generate QR">
                </div>



                <div class="col-lg-3 p-t-20">
                    @if(session()->get('user')->isAdmin === 1)
                        <div class="checkbox checkbox-aqua">
                            <input type="hidden" name="isSignatory" id="isSignatory" value="0" />
                            <input class="notification-only" id="checkIsSignatory" type="checkbox">
                            <label for="checkIsSignatory">
                            <p style="margin-top: -3px;">@lang('messages.signatory')</p>
                            </label>
                        </div>

                        <div class="checkbox checkbox-aqua">
                            <input type="hidden" name="feePaymentAllowed" id="feePaymentAllowed" value="0" />
                            <input class="feePaymentAllowed" id="feePaymentAllowedCheckbox" type="checkbox">
                            <label for="feePaymentAllowedCheckbox">
                            <p style="margin-top: -3px;">@lang('messages.payment-allowed')</p>
                            </label>
                        </div>
                    @endif
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
