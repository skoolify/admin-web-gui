@extends('master')

@section('title', Lang::get('messages.users-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/select2/css/select2.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.users-management')))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.activeInactive-filter')
                            {{-- @include('partials.components.branches-filter') --}}
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('users.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.usersList')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>

                    <button type="button" class="btn btn-round btn-warning assign-role pull-right" ><i class="fa fa-plus"></i> @lang('messages.assign-roles')</button>
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="min-width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.full-name')</th>
                                    <!-- <th>User Name</th> -->
                                    <th>@lang('messages.username-mobile')</th>
                                    <th>@lang('messages.roles')</th>
                                    <th>@lang('messages.status-active')</th>
                                    <th>@lang('messages.action-column')</th>
                                    <th>
                                        <div class="col-lg-3 p-t-20">
                                            <div class="">
                                                <input id="selectAll" type="checkbox" >
                                                <label for="isSelect">
                                                     @lang('messages.all')
                                                </label>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade settingModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
            <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.assign-roles')</h4>
            <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
            </button>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:red">&times;</span>
            </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid"  style = "min-height:300px;overflow:auto;">
            <div class="row">
                @if (Authorization::check("school-branches", "read", Authorization::getParent("school-branches"), true))
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="">@lang('messages.branch')</label>
                        <select name="branchID" id="filter-branches-branches" class="form-control filter-select">
                            <option value="">{{ isset($all) && $all ? $all : 'Select' }} @lang('messages.branch')</option>
                        </select>
                        <div class="invalid-feedback">
                            @lang('messages.required-field-error')
                        </div>
                        <div id="p2-branches" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                @else
                <input type="hidden" name="branchID" value="{{ session()->get('user')->branchID }}" />
                @endif
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="">@lang('messages.role')</label>
                        <select name="roleID" id="filter-roles" multiple class="form-control filter-select">
                            <option value="">@lang('messages.select-role')</option>
                        </select>
                        <div id="p2-roles" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" id="saveChanges" class="btn btn-primary" data-dismiss="modal">@lang('messages.submit-btn')</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.cancel-btn')</button>
      </div>
    </div>
  </div>
</div>

@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.js')}}" ></script>
<script src="{{ asset('assets/js/pages/select2/select2-init.js')}}" ></script>
@endpush
