<!-- start sidebar menu -->
<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll" class="left-sidemenu">
            <ul class="sidemenu  page-header-fixed" style="height:90vh; overflow-y:scroll;" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>
                <li class="sidebar-user-panel">
                    <div class="user-panel" style="padding-bottom: 0px;padding-top: 10px;">
                    </div>
                    <div class="user-panel" style="padding-bottom: 0px;">
                        <div class="container">
                            <div class="row">
                                <p class="text-left" style="margin-bottom: 0px;">
                                    <span class="txtOnline"><b>@lang('messages.sidebar-role-tab')</b> {{ session()->get('user')->roleName }}</span>
                                </p>
                            </div>

                            <div class="row">
                                <p class="text-left" style="margin-bottom: 0px;">
                                    <span class="txtOnline"><b>@lang('messages.sidebar-branch-tab')</b> {{ session()->get('user')->branchName }}</span>
                                </p>
                            </div>

                            <div class="row">
                                <p class="text-left">
                                    <span class="txtOnline"><b>@lang('messages.sidebar-last-login-tab')</b>  <span dir="ltr">{{ \Carbon\Carbon::parse(session()->get('lastLoginDate'))->setTimezone('Asia/Karachi')->format('d-M-y h:i A') }}</span></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                @if (session()->get('user')->isBlocked === 1 && session()->get('user')->feePaymentAllowed === 1)

                @else
                    @foreach (session()->get('permissions') as $item)
                        @if (Authorization::check($item->moduleURL, 'read', $item->parent))
                            @if (isset($item->childrens))
                                <!-- @if (count($item->childrens) >= 1) -->
                                    <li class="nav-item {{ isset($item->childrens[Route::currentRouteName()]) ? $item->childrens[Route::currentRouteName()]->parent === $item->moduleURL ? ' active open' : '' : '' }}">
                                        <a href="#" class="nav-link nav-toggle">
                                            <span class="title">{{ App::getLocale() == 'ar' ? $item->moduleName_ar : $item->moduleName }}</span>
                                            <span class="arrow"></span>
                                        </a>
                                        @if (count($item->childrens) >= 1)
                                            <ul class="sub-menu" style="display: {{ isset($item->childrens[Route::currentRouteName()]) ? $item->childrens[Route::currentRouteName()]->parent === $item->moduleURL ? 'block' : 'none' : 'none' }}">
                                                @foreach ($item->childrens as $child)
                                                    @if (Authorization::check($child->moduleURL, 'read', $child->parent, true))
                                                        @if (Route::has($child->moduleURL))
                                                            <li class="nav-item {{ in_array(Route::currentRouteName(), array($child->moduleURL, $child->moduleURL.'.add', $child->moduleURL.'.edit')) ? 'active' : '' }}">
                                                                <a href="{{ route($child->moduleURL) }}" class="nav-link">
                                                                    {{-- <a href="{{ route($child->moduleURL, ['lang' => App::getLocale()]) }}" class="nav-link"> --}}
                                                                    <span class="title">{{ App::getLocale() == 'ar' ? $child->moduleName_ar : $child->moduleName }}
                                                                        @if($child->moduleURL == 'comm-requests')
                                                                        <span class="badge badge-light btn btn-primary comm-notification"></span>
                                                                        @endif
                                                                    </span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                <!-- @endif -->
                            @endif
                        @endif
                    @endforeach

                @endif
            </ul>
        </div>
    </div>
</div>
    <!-- end sidebar menu -->
