<!-- start header -->
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <!-- logo start -->
        <div class="page-logo" style="background: #ffffff;">
            {{-- <a href="{{ route('modules') }}"> --}}
                <img src="{{ asset('assets/img/logo-single-small.png') }}" height="50" alt="Skoolify">
            {{-- </a> --}}
        </div>
        <!-- logo end -->
        <ul class="nav navbar-nav navbar-left in">
            <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
        </ul>

        <!-- start mobile menu -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
       <!-- end mobile menu -->
        <!-- start header menu -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                @if(isset($_SESSION['country']) && $_SESSION['country'] != 'Pakistan')
                    <li class="dropdown language-switch">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            {{-- <img src="../assets/img/flags/{{App::getLocale() === 'ar' ? 'ae' : 'gb'}}.png" class="position-left" alt="">  --}}
                            {{App::getLocale() === 'ar' ? Lang::get('messages.arabic') : Lang::get('messages.english')}} <span class="fa fa-angle-down"></span>
                        </a>
                        <form action="{{route('switch.language')}}" method="get" id="switch-language-form">
                            <input type="hidden" name="lang" value="{{ Request::query('lang') ?? 'en' }}" id="language">
                            <ul class="{{App::getLocale() === 'ar' ? 'language-dropdown-login' : 'none'}} dropdown-menu">
                                <li>
                                    <a class="switchLanguageBtn" data-value="en">
                                        {{-- <img src="../assets/img/flags/gb.png" alt=""> --}}
                                        @lang('messages.english')
                                    </a>
                                </li>
                                <li>
                                    <a class="switchLanguageBtn" data-value="ar">
                                        {{-- <img src="../assets/img/flags/ae.png" alt=""> --}}
                                        @lang('messages.arabic')
                                    </a>
                                </li>
                            </ul>
                        </form>
                        {{-- <ul class="{{App::getLocale() === 'ar' ? 'language-dropdown' : 'none'}} dropdown-menu">
                            <li>
                                <a  data-value="en" href="{{ route(Route::currentRouteName(), ['lang' => 'en']) }}"><img src="../assets/img/flags/gb.png" alt=""> @lang('messages.english')</a>
                            </li>
                            <li>
                                <a  data-value="ar" href="{{ route(Route::currentRouteName(), ['lang' => 'ar']) }}"><img src="../assets/img/flags/ae.png" alt=""> @lang('messages.arabic')</a>
                            </li>
                        </ul> --}}
                        {{-- <form action="{{route('set-session-lang')}}" method="post" id="language-form">
                            @csrf
                            <input type="hidden" name="language" id="language-input">
                        </form> --}}
                    </li>
                @endif
                 <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle " src="{{ session()->get('schoolLogo') }}" onerror="this.src='{{ asset('assets/img/logo-2.png') }}'" />
                        <span class="username username-hide-on-mobile"> {{ session()->get('user')->fullName }} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        @if(count(session()->get('roles')) > 1)
                        <li>
                            <a id="changeRoleBtn" href="#" data-toggle="modal" data-target="#changeRoleModal">
                                <i class="fa fa-refresh"></i> @lang('messages.change-role')
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ url('auth/password') }}">
                                <i class="icon-lock"></i> @lang('messages.change-password')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/auth/login?logout=1') }}" id="btnLogout">
                                <i class="icon-logout"></i> @lang('messages.log-out') </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="toastMessageDiv" class="page-header-inner" style="background-color: #2fa8ff; display:none;">
        <div class="row">
            <div class="col-md-11">
                <center><b><div id="loginToast" style="color:white; padding-top:3px;"></div></b></center>
            </div>
            <div class="col-md-1">
                <a type="submit" id="loginToastDivCloseBtn" style="float:right;padding-right: 10px;color: white;font-size:18px;"><i class="fa fa-close"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="changeRoleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 370px !important;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">@lang('messages.please-select-role')</h5>
            <i class="fa fa-close text-danger" data-dismiss="modal" aria-label="Close" aria-hidden="true"></i>
        </div>
        <div class="modal-body">
            <select class="form-control" id="selectRoles">
                @foreach(session()->get('roles') as $key => $role)
            <option value="{{$key}}" {{ $role->roleID === session()->get('user')->roleID && $role->branchID === session()->get('user')->branchID ? 'disabled' : '' }}>{{$role->roleName.'('.$role->branchName.')'}}</option>
                @endforeach
            </select>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="modal-footer">
                    <a href="" id="proceedBtn" class="btn btn-primary btn-block mt-3">@lang('messages.proceed-btn')</a>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
      </div>
    </div>
  </div>

@push('extra-scripts')
<script>
    $(document).on('change', '#selectRoles', function(){
        let index = $('#selectRoles').val()
        route = '{{route("switch.role","id")}}'
        route = route.replace("id", index)
        $('#proceedBtn').attr('href', route)
    })
    $(document).on('click', '#changeRoleBtn', function(){
        let index = $('#selectRoles').val()
        route = '{{route("switch.role","id")}}'
        route = route.replace("id", index)
        $('#proceedBtn').attr('href', route)
    })
    $(document).on('click','.switchLanguageBtn', function(){
    let lang = '';
    let form = $('#switch-language-form')
    lang = $(this).attr('data-value');
    $('#language').val(lang);
    form.submit();
})
</script>
@endpush
<!-- end header -->
