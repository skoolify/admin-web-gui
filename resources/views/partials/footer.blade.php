<!-- start footer -->
<div class="page-footer">
    <div class="page-footer-inner"> {{ date('Y') }} &copy; @lang('messages.copyright-reserved') - <a href="https://skoolify.app" target="_blank">@lang('messages.skoolify')</a> 
        <!-- <a href="mailto:info@skoolify.com" target="_top" class="makerCss">Skoolify</a> -->
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- end footer -->