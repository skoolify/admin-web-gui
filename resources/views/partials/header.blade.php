<!-- start header -->
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <!-- logo start -->
        <div class="page-logo" style="background: #ffffff;">
            <a href="{{ route('modules') }}">
                <img src="{{ asset('assets/img/logo-single-small.png') }}" height="50" alt="Skoolify">
            </a>
        </div>
        <!-- logo end -->
        <ul class="nav navbar-nav navbar-left in">
            <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
        </ul>
        <!-- start mobile menu -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- end mobile menu -->
        <!-- start header menu -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                {{-- <li><a href="javascript:;" class="fullscreen-btn"><i class="fa fa-arrows-alt"></i></a></li> --}}

                <!-- start manage user dropdown -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle " src="{{ session()->get('schoolLogo') }}" onerror="this.src='{{ asset('assets/img/logo-2.png') }}'" />
                        <span class="username username-hide-on-mobile"> {{ session()->get('user')->fullName }} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        {{-- <li>
                            <a href="user_profile.html">
                                <i class="icon-user"></i> Profile </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-settings"></i> Settings
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-directions"></i> Help
                            </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="lock_screen.html">
                                <i class="icon-lock"></i> Lock
                            </a>
                        </li> --}}
                        <li>
                            <a href="{{ url('auth/password') }}">
                                <i class="icon-lock"></i> @lang('messages.change-password')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('auth/login') }}">
                                <i class="icon-logout"></i> @lang('messages.log-out') </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- start horizontal menu -->
    <div class="navbar-custom">
        <div class="hor-menu hidden-sm hidden-xs">
            <ul class="nav navbar-nav">

                <li class="mega-menu-dropdown ">
                    <a href="{{ url('/schools') }}" class="dropdown-toggle"> <i class="material-icons">@lang('messages.business')</i>  @lang('messages.schools')
                        <i class="fa fa-angle-down"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 200px;">
                        <li>
                            <div class="mega-menu-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="mega-menu-submenu">
                                            <li>
                                                <a href="{{ url('/schools') }}" class="nav-link"> <span class="title">@lang('messages.allSchools')</span></a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/schools/add') }}" class="nav-link"> <span class="title">@lang('messages.addSchool')</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>

                <li class="mega-menu-dropdown">
                    <a href="" class="dropdown-toggle"> <i class="material-icons">@lang('messages.star')</i>@lang('messages.content')
                        <i class="fa fa-angle-down"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li class="dropdown-submenu">
                            <a href="javascript:;">
                                <i class="fa fa-globe"></i> @lang('messages.countries')
                            </a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="{{ url('/countries') }}" class="nav-link">
                                        <span class="title">List</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/countries/add') }}" class="nav-link">
                                        <span class="title">@lang('messages.add')</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu">
                            <a href="javascript:;">
                                <i class="fa fa-building"></i> @lang('messages.cities')
                            </a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="{{ url('/cities') }}" class="nav-link">
                                        <span class="title">@lang('messages.list')</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{ url('/cities/add') }}" class="nav-link">
                                        <span class="title">@lang('messages.add')</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu">
                            <a href="javascript:;">
                                <i class="fa fa-road"></i> @lang('messages.areas')
                            </a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="{{ url('/areas') }}" class="nav-link">
                                        <span class="title">@lang('messages.list')</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{ url('/areas/add') }}" class="nav-link">
                                        <span class="title">@lang('messages.add')</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <!-- end horizontal menu -->
</div>
<!-- end header -->
