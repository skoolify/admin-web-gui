
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">{{ $pageTitle }}</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="#"></a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <?php $segments = ''; ?>
            @foreach(Request::segments() as $key => $segment)
                <?php $segments .= '/'.$segment; ?>
                <li>
                    <a class="{{ (count(Request::segments())) !== ($key + 1) ? 'parent-item' : 'active' }}" href="{{ $segments }}">{{$pageTitle}} {!! (count(Request::segments())) !== ($key + 1) ? '<i class="fa fa-angle-right"></i>' : '' !!} </a>
                    {{-- <a class="{{ (count(Request::segments())) !== ($key + 1) ? 'parent-item' : 'active' }}" href="{{ $segments }}">{{ucfirst($segment)}} {!! (count(Request::segments())) !== ($key + 1) ? '<i class="fa fa-angle-right"></i>' : '' !!} </a> --}}
                </li>
            @endforeach
        </ol>
    </div>
</div>
