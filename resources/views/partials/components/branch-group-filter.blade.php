@if (Authorization::check("school-branches", "read", Authorization::getParent("school-branches"), true))
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">Branch</label>
        <select name="branchGroupID" id="filter-branch-groups" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : 'Select' }} Branch Group</option>
            @foreach($branches as $branch)
                <option value="{{ $branch->branchGroupID }}">{{ $branch->branchName }}</option>
            @endforeach
        </select>
        <div class="invalid-feedback">
            This field is required
        </div>
        <div id="p2-branchGroup" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
@else
<input type="hidden" name="branchGroupID" value="{{ session()->get('user')->branchID }}" />
@endif
