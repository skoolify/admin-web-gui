<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.fee-type')</label>
        <select name="feeTypeID" id="filter-fee-type" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.fee-type')</option>
            @foreach($feetypes as $feeType)
                <option {{ session()->get('tempFeeTypeID') == $feeType->feeTypeID ? 'selected' : '' }} value="{{ $feeType->feeTypeID }}">{{ $feeType->feeType }}</option>
            @endforeach
        </select>
        <div id="p2-feeTypes" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
