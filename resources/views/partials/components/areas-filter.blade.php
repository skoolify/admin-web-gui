@if (Authorization::check('areas'))
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.area')</label>
        <select name="areaID" id="filter-areas" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.area')</option>
            @foreach($areas as $area)
                <option {{ session()->get('tempAreaID') == $area->areaID ? 'selected' : '' }} value="{{ $area->areaID }}">{{ $area->areaName }}</option>
            @endforeach
        </select>
        <div id="p2-areas" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
@endif
