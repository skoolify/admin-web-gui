<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
    <label for="marksTotal">@lang('messages.week-day')
                <span class="required"> * </span>
        </label>
        <select name="weekdayNo" id="filter-week-day" class="form-control filter-select">
            <option value="1">@lang('messages.sunday')</option>
            <option value="2">@lang('messages.monday')</option>
            <option value="3">@lang('messages.tuesday')</option>
            <option value="4">@lang('messages.wednesday')</option>
            <option value="5">@lang('messages.thursday')</option>
            <option value="6">@lang('messages.friday')</option>
            <option value="7">@lang('messages.saturday')</option>
        </select>
        {!! $errors->first('marksTotal','<span class="help-block">:message</span>') !!}
    </div>
    </div>
</div>