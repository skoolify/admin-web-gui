@if (Authorization::check('groups'))
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.group')</label>
        <select name="groupID" id="filter-groups" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.group')</option>
            @foreach($groups as $group)
                <option {{ session()->get('tempGroupID') == $group->groupName ? 'selected' : '' }} value="{{ $group->groupName }}">{{ $group->groupName }}</option>
            @endforeach
        </select>
        <div id="p2-groups" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
@endif
