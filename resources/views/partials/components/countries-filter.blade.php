@if (Authorization::check('countries'))
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.country')</label>
        <select name="countryID" id="filter-countries" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.country')</option>
            @foreach($countries as $country)
                <option {{ session()->get('tempCountryID') == $country->countryID ? 'selected' : '' }} value="{{ $country->countryID }}">{{ $country->countryName }}</option>
            @endforeach
        </select>
        <div id="p2-countries" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
@endif
