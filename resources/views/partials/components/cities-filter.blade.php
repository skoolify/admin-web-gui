@if (Authorization::check('cities'))
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.city')</label>
        <select name="cityID" id="filter-cities" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.city')</option>
            @foreach($cities as $city)
                <option {{ session()->get('tempBranchID') == $city->cityID ? 'selected' : '' }} value="{{ $city->cityID }}">{{ $city->cityName }}</option>
            @endforeach
        </select>
        <div id="p2-cities" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
@endif
