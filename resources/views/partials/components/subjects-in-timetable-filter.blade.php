<div class="col-md-3 col-sm-12 col-xs-12" id="subject-in-time-table">
    <div class="form-group">
        <label for="">@lang('messages.time-table-subjects')</label>
        <select name="timetableSubjectID" id="filter-subjects-in-time-table" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.time-table-subjects')</option>
        </select>
        <div id="p2-subjectsInTimeTable" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>