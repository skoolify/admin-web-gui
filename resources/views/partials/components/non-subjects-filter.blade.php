<div class="col-md-3 col-sm-12 col-xs-12" id="non-subject">
    <div class="form-group">
        <label for="">@lang('messages.non-subject')</label>
        <select name="nonSubjectID" id="filter-non-subjects" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.non-subject')</option>
            @foreach($nonSubjects as $nonSubject)
                <option {{ session()->get('tempNonSubjectID') == $nonSubject->nonSubjectID ? 'selected' : '' }} value="{{ $nonSubject->nonSubjectID }}">{{ $nonSubject->subjectName }}</option>
            @endforeach
        </select>
        <div id="p2-non-subjects" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>