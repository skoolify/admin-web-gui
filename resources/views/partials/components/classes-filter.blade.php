<div class="col-md-3 col-sm-12 col-xs-12" style="display: {{ Route::currentRouteName() === 'subjects' || Route::currentRouteName() === 'non-subjects' ? 'none' : '' }}">
    <div class="form-group">
        <label for="">@lang('messages.section')</label>
        <select name="classID" id="filter-classes" class=" form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.section')</option>
            @foreach($classes as $class)
                <option {{ session()->has('tempClassID') ? (session()->get('tempClassID') == $class->classID ? 'selected' : '') : ''}} value="{{ $class->classID }}">{{ $class->classSection }}</option>
            @endforeach
        </select>
        <div class="invalid-feedback">
            @lang('messages.required-field-error')
        </div>
        <div id="p2-classes" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
