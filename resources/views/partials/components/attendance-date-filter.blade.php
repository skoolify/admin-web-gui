<div class="col-md-3 col-sm-12 col-xs-12">  
    <div class="form-group">
        <label for="">@lang('messages.date')</label>
        <input type="date" name="attendanceDate" max="{{ \Carbon\Carbon::now()->now()->toDateString() }}" value="{{ \Carbon\Carbon::now()->now()->toDateString() }}" id="filter-attendance-date" class="form-control" />
        <div id="p2-attendance-date" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>