<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.status-column')</label>
        <select class="form-control" id="filter-active-inactive">
            <option value="1">@lang('messages.status-active')</option>
            <option value="0">@lang('messages.status-inactive')</option>
        </select>
    </div>
</div>
