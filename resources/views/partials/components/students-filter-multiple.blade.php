<div class="col-md-3 col-sm-12 col-xs-12" id="student-filter">
    <div class="form-group">
        <label for="">@lang('messages.student')</label>
        <select name="studentID" id="filter-students" data-live-search="true"  class="form-control filter-select" multiple>
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.student')</option>
            @foreach($students as $student)
                <option {{ session()->get('tempStudentID') == $student->studentID ? 'selected' : '' }} value="{{ $student->studentID }}">{{ $student->studentName }}</option>
            @endforeach
        </select>
        <div id="p2-students" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
