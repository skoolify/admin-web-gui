<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.from')</label>
        <input type="date" name="dateFrom" value="{{ \Carbon\Carbon::now()->startOfMonth()->toDateString() }}" id="filter-date-from" class="form-control" />
        <div id="p2-date-from" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
