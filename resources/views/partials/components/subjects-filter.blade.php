<div class="col-md-3 col-sm-12 col-xs-12" id="subject">
    <div class="form-group">
        <label for="">@lang('messages.subject')</label>
        <select name="subjectID" id="filter-subjects" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.subject')</option>
            @foreach($subjects as $subject)
                <option {{ session()->get('tempSubjectID') == $subject->subjectID ? 'selected' : '' }} value="{{ $subject->subjectID }}">{{ $subject->subjectName }}</option>
            @endforeach
        </select>
        <div id="p2-subjects" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
