<div class="col-md-3 col-sm-12 col-xs-12" style="display: {{ Route::currentRouteName() === 'role-access-rights' ? 'none' : '' }}">
    <div class="form-group">
        <label for="">@lang('messages.module')</label>
        <select name="moduleID" id="filter-modules" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.module')</option>
        </select>
        <div id="p2-modules" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>