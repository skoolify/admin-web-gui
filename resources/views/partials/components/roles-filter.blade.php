<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.role')</label>
        <select name="roleID" id="filter-roles" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.role')</option>
            @foreach($roles as $role)
                <option {{ session()->get('tempRoleID') == $role->roleID ? 'selected' : '' }} value="{{ $role->roleID }}">{{ $role->roleName }}</option>
            @endforeach
        </select>
        <div id="p2-roles" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>