<div class="col-md-3 col-sm-12 col-xs-12">  
    <div class="form-group">
        <label for="billingPeriodID">@lang('messages.billing-period')
                <span class="required"> * </span>
        </label>
        <select name="billingPeriodID" id="billing-period-filter" class="form-control filter-select">
        <option value="">@lang('messages.select') @lang('messages.billing-period')</option>
        </select>
        <div id="p2-billing-period" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
        {!! $errors->first('billingPeriodID','<span class="help-block">:message</span>') !!}
    </div>
</div>