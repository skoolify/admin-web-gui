<div class="col-md-3 col-sm-12 col-xs-12">  
    <div class="form-group">
        <label for="">@lang('messages.to')</label>
        <input type="date" name="dateTo" value="{{ \Carbon\Carbon::now()->endOfYear()->toDateString() }}" id="filter-date-to" class="form-control" />
        <div id="p2-date-to" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>