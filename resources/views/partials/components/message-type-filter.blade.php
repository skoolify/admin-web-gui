<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.messages-type')</label>
        <select name="messageType" id="filter-message-type" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.messages-type')</option>
            <option value="C">@lang('messages.circular')</option>
            <option value="P">@lang('messages.parent-consent')</option>
            <option value="I">@lang('messages.invitation')</option>
            <option value="M">@lang('messages.message')</option>
        </select>
        <div id="p2-message-type" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
