<div class="col-md-3 col-sm-12 col-xs-12" style="display: {{ Route::currentRouteName() === 'subjects' || Route::currentRouteName() === 'non-subjects' ? 'none' : '' }}">
    <div class="form-group">
        <label for="">@lang('messages.teacher')</label>
        <select name="classTeacherID" id="filter-class-teachers" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.teacher')</option>
        </select>
        <div id="p2-class-teachers" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>