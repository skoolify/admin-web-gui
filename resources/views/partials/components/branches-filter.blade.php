@if (Authorization::check("school-branches", "read", Authorization::getParent("school-branches"), true))
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.branch')</label>
        <select name="branchID" id="filter-branches" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.branch')</option>
            @foreach($branches as $branch)
                <option value="{{ $branch->branchID }}">{{ $branch->branchName }}</option>
            @endforeach
        </select>
        <div class="invalid-feedback">
            @lang('messages.required-field-error')
        </div>
        <div id="p2-branches" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
@else
<input type="hidden" name="branchID" value="{{ session()->get('user')->branchID }}" />
@endif
