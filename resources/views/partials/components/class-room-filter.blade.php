<div class="col-md-12 col-sm-12 col-xs-12">  
    <div class="form-group">
        <label for="roomID">@lang('messages.class-room')
                <span class="required"> * </span>
        </label>
        <select name="roomID" id="filter-class-room" class="form-control filter-select">
        <option value="">@lang('messages.select') @lang('messages.class-room')</option>
        </select>
        <div id="p2-class-room" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
        {!! $errors->first('roomID','<span class="help-block">:message</span>') !!}
    </div>
</div>

