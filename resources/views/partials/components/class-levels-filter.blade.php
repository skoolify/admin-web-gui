<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.classes')</label>
        <select name="classLevelID" id="filter-classLevels" class="form-control filter-select h-40">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.class')</option>
            @foreach($classLevels as $level)
                <option {{ session()->has('temp') ? session()->get('temp')->classLevelID == $level->classLevelID ? 'selected' : '' : '' }} value="{{ $level->classLevelID }}">{{ $level->classAlias }}</option>
            @endforeach
        </select>
        <div id="p2-classLevels" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>
