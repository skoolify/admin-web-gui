<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.settlement')</label>
        <select class="form-control select2" id="filter-settled">
            <option value="">@lang('messages.pleaseSelect')</option>
            <option value="1">@lang('messages.settled')</option>
            <option value="0">@lang('messages.Unsettled')</option>
        </select>
    </div>
</div>
