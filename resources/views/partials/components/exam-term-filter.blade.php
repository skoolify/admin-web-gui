<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.exam-term')</label>
        <select name="examsTermID" id="filter-exam-term" class="form-control filter-select">
            <option value="">@lang('messages.select') @lang('messages.exam-term')</option>
        </select>
        <div id="p2-exam-term" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
    </div>
</div>