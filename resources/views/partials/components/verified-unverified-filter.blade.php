<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.verification')</label>
        <select class="form-control select2" id="filter-verified">
            <option value="">@lang('messages.pleaseSelect')</option>
            <option value="1">@lang('messages.verified')</option>
            <option value="0">@lang('messages.unVerified')</option>
        </select>
    </div>
</div>
