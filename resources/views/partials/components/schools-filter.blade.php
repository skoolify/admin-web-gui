@if (Authorization::check("schools", "read", Authorization::getParent("schools"), true))
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="">@lang('messages.school')</label>
        <select name="schoolID" id="filter-schools" data-native-menu="false" class="form-control filter-select">
            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.school')</option>
            @foreach($schools as $school)
                <option value="{{ $school->schoolID }}">{{ $school->schoolName }}</option>
            @endforeach
        </select>
        <div class="invalid-feedback">
            @lang('messages.required-field-error')
        </div>
        <div id="p2-schools" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%">
    </div>
    </div>
</div>
@else
<input type="hidden" name="schoolID" value="{{ session()->get('user')->schoolID }}" />
@endif
