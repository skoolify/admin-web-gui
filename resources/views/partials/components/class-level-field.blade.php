<div class="form-group {{ $errors->has('classLevelName') ? 'has-error' :'' }}">
    <label for="classLevelName">@lang('messages.class-name')
        <span class="required"> * </span>
    </label>
    <select name="classLevelName" id="classLevelName" class="form-control" required>
        <option value="">@lang('messages.select') @lang('messages.class-name')</option>
        @foreach (Config::get('constants.classLevels') as $key => $level)
        <option value="{{ $key }}" {{ isset($recordLevel) ? ($recordLevel == $key ? 'selected' : '') : '' }}>{{ $level }}</option>
        @endforeach
    </select>
    {!! $errors->first('classLevelName','<span class="help-block">:message</span>') !!}
</div>
