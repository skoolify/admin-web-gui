<div class="col-lg-3 p-t-20" id="margin-top">
    <div class="radio p-0 d-flex">
        <input type="radio" name="isActive" id="activeField1" data-status-1="1" value="1" checked />
        <label for="activeField1" id="radio-right">
            @lang('messages.status-active')
        </label>
    </div>
    <div class="radio p-0 d-flex">
        <input type="radio" name="isActive" id="activeField0" data-status-0="0" value="0" />
        <label for="activeField0" id="radio-right">
            @lang('messages.status-inactive')
        </label>
    </div>
</div>
