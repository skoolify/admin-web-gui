<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="fee-type-form" action="{{ url('fee-type/add') }}" role="form">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('feeType') ? 'has-error' :'' }}">
                        <label for="feeType">@lang('messages.fee-type')</label>
                        <input maxlength="50" class = "form-control" name="feeType" type = "text" id = "feeType" value="{{ old('feeType') }}" data-validation="required length" data-validation-length="max50" placeholder="{{Lang::get('messages.fee-type')}}">
                        {!! $errors->first('feeType','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('taxRate') ? 'has-error' :'' }}">
                        <label for="taxRate">@lang('messages.tax-rate')</label>
                        <input onKeyUp="numericFilter(this);" class = "form-control" name="taxRate" type = "text" id = "taxRate" value="0" data-validation="required number">
                        {!! $errors->first('feeType','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('discountRate') ? 'has-error' :'' }}">
                        <label for="discountRate">@lang('messages.discount')(%)</label>
                        <input onKeyUp="numericFilter(this);" class = "form-control" name="discountRate" type = "text" id = "discountRate" value="0" data-validation="required number">
                        {!! $errors->first('feeType','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="checkbox checkbox-aqua d-flex">
                        <input type="hidden"  name="isActive" id="isActiveInput" value="1">
                        <input class="isActive" id="isActive" type="checkbox" checked>
                        <label for="isActive" id="check-box-right">
                            {{-- @lang('messages.status-active') --}}
                            <p class="fee-type-active">@lang('messages.status-active')</p>
                        </label>
                    </div>
                    <div class="checkbox checkbox-aqua d-flex">
                        <input id="isRecurring" class="studentCheckBox" type="checkbox" >
                        <label for="isRecurring" id="check-box-right">
                            @lang('messages.recurring')
                        </label>
                    </div>
                </div>
                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
@push('extra-scripts')
<script>
    function numericFilter(txb) {
        txb.value = txb.value.replace(/[^\0-9]/ig, "");
    }
</script>
@endpush
