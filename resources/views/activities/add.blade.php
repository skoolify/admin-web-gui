<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="activity-form" action="{{ url('activities/add') }}" role="form">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">
                <input type="hidden" id="editHiddenField" value="" />
                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('feeType') ? 'has-error' :'' }}">
                        <label for="activityName">@lang('messages.activity-name')</label>
                        <input maxlength="100" class = "form-control" name="activityName" type = "text" id = "activityName" value="{{ old('activityName') }}" data-validation="required length" data-validation-length="max100" placeholder="{{Lang::get('messages.activity-name')}}" >
                        {!! $errors->first('activityName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-3 p-t-20">
                <div class="form-group">
                    <label for="">@lang('messages.activity-start-time')</label>
                    <div class="input-group clockpicker">
                        <input type="text" class="form-control" name="startTime" id="startTime" placeholder="{{Lang::get('messages.activity-start-time')}}" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col-lg-3 p-t-20">
                <div class="form-group">
                    <label for="">@lang('messages.activity-end-time')</label>
                    <div class="input-group clockpicker">
                        <input type="text" class="form-control"  placeholder="{{Lang::get('messages.activity-end-time')}}"  name="endTime" id="endTime" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="col-lg-3 p-t-20">
                <div class="radio p-t-20 d-flex">
                    <input type="radio" name="isActive" id="activeField1" data-status-1="1" value="1" checked />
                    <label for="activeField1" id="radio-right">
                        @lang('messages.status-active')
                    </label>
                </div>
                <div class="radio p-t-5 d-flex">
                    <input type="radio" name="isActive" id="activeField0" data-status-0="0" value="0" />
                    <label for="activeField0"  id="radio-right">
                        @lang('messages.status-inactive')
                    </label>
                </div>
            </div>
            <div class="col-lg-3 p-t-20" >
                    <div class="form-group">
                        <label for="">@lang('messages.activity-instructor')</label>
                        <select name="userID" id="filter-users" class="form-control filter-select">
                            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.activity-instructor')</option>
                        </select>
                        <div class="invalid-feedback">
                            @lang('messages.required-field-error')
                        </div>
                        <div id="p2-users" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
