@extends('master')

@section('title', Lang::get('messages.app-installs'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<style>
    .card-box:hover {
     -webkit-transform: none !important;
     transform: none !important;
     box-shadow: 0 20px 20px rgba(0,0,0,.1);
     -moz-box-shadow: 0 20px 20px rgba(0,0,0,.1);
     -webkit-box-shadow: 0 20px 20px rgba(0,0,0,.1);
 }
 </style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.app-installs')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="fromDate">@lang('messages.from')</label>
                                    <input class = "form-control" name="fromDate" type = "date" id = "fromDate" placeholder="From Date" value="{{ \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d') }}" required  />
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="toDate">@lang('messages.to')</label>
                                    <input class = "form-control" name="toDate" type = "date" id = "toDate" placeholder="To Date" value="{{ \Carbon\Carbon::now()->endOfMonth()->format('Y-m-d') }}" required  />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('fee-type.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                
                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.parent-registration-status')</header>
                </div>
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <p>@lang('messages.total') - <strong id="overall-total">0</strong></p>
                        </div>
                        <div class="col-md-4 text-center">
                            <p>@lang('messages.registered') - <strong id="overall-registered">0</strong></p>
                        </div>
                        <div class="col-md-4 text-right">
                            <p>@lang('messages.un-registered') - <strong id="overall-unregistered">0</strong></p>
                        </div>
                        <div class="col-md-4">
                            <p>@lang('messages.active-students') - <strong id="overall-active-students">0</strong></p>
                        </div>
                        <div class="col-md-8 text-right">
                            <p>@lang('messages.inactive-students') - <strong id="overall-non-active-students">0</strong></p>
                        </div>
                    </div>
                    <h3 class="text-center hidden" id="loading-text">@lang('messages.please-wait')</h3>
                    <h3 class="text-center hidden" id="no-data-text">@lang('messages.no-data-available')</h3>
                    <canvas id="graph" class="hidden"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="{{ asset('assets/js/chart.min.js') }}"></script>
@endpush
