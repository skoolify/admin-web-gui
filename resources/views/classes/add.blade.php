<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="classes-form" action="{{ url('classes/add') }}">

            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20"> 
                    <div class="form-group {{ $errors->has('classSection') ? 'has-error' :'' }}">
                        <label for="classSection">@lang('messages.section')
                        </label>
                        <input maxlength="100" class = "form-control" name="classSection" type = "text" id = "classSection" value="{{ old('classSection') }}" data-validation="required length" data-validation-length="max100" placeholder="{{Lang::get('messages.section')}}">
                        {!! $errors->first('classSection','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20"> 
                    <div class="form-group {{ $errors->has('classShift') ? 'has-error' :'' }}">
                        <label for="classShift">@lang('messages.class-shift')
                        </label>
                        <select name="classShift" id="classShift" class="form-control" required>                            
                            <option value="M">@lang('messages.morning')</option>
                            <option value="A">@lang('messages.afternoon')</option>
                            <option value="E">@lang('messages.evening')</option>
                        </select>
                        {!! $errors->first('classShift','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                {{-- <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('classTeacher') ? 'has-error' :'' }}">
                        <label for="classTeacher">Class Teacher
                            <span class="required"> * </span>
                        </label>
                        <select name="classTeacher" id="classTeacher" class="form-control" >
                            <option value="">Select Teacher</option>
                            @foreach ($classTeachers as $teacher)
                                <option value="{{ $teacher->userID }}">{{ $teacher->userName }}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('classTeacher','<span class="help-block">:message</span>') !!}
                    </div>
                </div> --}}

                @include('partials.active-field')

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
