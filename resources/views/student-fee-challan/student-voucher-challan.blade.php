<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{$title}}</title>
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .grid-container {
            display: grid;grid-template-columns: auto auto auto; margin-bottom:10px;
            }
        .grid-item{
            border-right: 1px solid #3c3c3c;
            position: relative;
            }
            .bb-1{border-bottom: 1px solid #bbbbbb;}
            .bb-dot-1{border-bottom:2px dashed #333333}

            @media print {

            .pagebreak {
                clear: both;
                page-break-after: always;
            }
            .grid-container {
                display: grid;
                grid-template-columns: auto auto auto;
                margin-bottom:10px;
                grid-gap: 8px;
                margin-left:15px;
            }
            .copy-tab{
                color: #000 !important;
                background-color: #a6a8a9 !important;
                /* margin-top: -18px !important; */
                margin-right:50px !important;
            }
            table{
                font-size: 17px;
                width: 478px !important;
                margin-left:10px;
            }
            div, u{
                font-size:17px;
            }
            p{
                font-size:12px;
            }
            div h2{
                font-size:26px;
                font-weight: 600;
            }
            div p{
                font-size:18px !important;
            }
            div img{
                margin-top: 95px !important;
                width: 110px;
                height: 110px
            }
            div svg{
                margin-top:77px;
                margin-left:-3px;
                width: 150px;
                height: 150px;
            }

            div{
                width:490px !important;
            }

            .th-fee-type{
                width: 190px !important;
            }

            .th-tax{
                width:184px !important;
            }

            .px-2{
                font-size:14px !important;
                text-align: justify;
            }

            .div-top{
                margin-top: -90px;
            }

            .div-school-name{
                margin-top: -20px !important;
            }

            .div-fee-voucher{
                margin-top:-18px !important;
            }

            .div-billing{
                margin-top: -10px !important;
                margin-bottom: -12px !important;
            }

            .custom-margin-top {
                margin-top: 110px !important;
            }

            .bank-detail-div{
                width:190px !important;
                margin-left: 90px !important;
                padding-top: 2px !important;
                /* margin-left: 87px !important; */
                /* border: none !important; */
            }

            .bank-detail-font {
                font-size: 13px !important;
            }

            .bank-detail-label{
                padding-top: 0px !important;
                margin-bottom: .0rem !important;
            }

        }
            .px-2{
                text-align: justify;
                font-size: 14px
            }
            .bb-dot-1{border-bottom:2px dashed #333333}
            th,td{
                padding:0px!important;
            }
            table{margin-bottom:2px!important;}

            .watermark{
                position: absolute;
                z-index: 99;
                left: 0;
                right: 0;
                margin: auto;
                top: 50%;
                width: 80%;
                opacity: 0.8;
                height: 300px;
            }
    </style>
  </head>

  <body>
@if(isset($data) && $data && $code === 200)
    @foreach($data as $key => $value)
        @if(isset($value[0]) && isset($value[0]->feeChallan) && $value[0]->feeChallan)
            @foreach($value[0]->feeChallan as $innerKey => $feeChallan)
            <div style="border-left: 2px solid;" class="grid-container">
                <div class="grid-item">
                    @if ($feeChallan->paymentStatus && $feeChallan->paymentStatus == 'P')
                        <img class="watermark" src="{{asset('assets/img/paid-stamp.png')}}" alt="">
                    @endif
                    <div class="container">
                        <div class="py-3 text-center">
                        <label class="m-auto copy-tab" disabled><b>Parents Copy</b></label>
                            <div class="row div-top">
                                <div class="col-md-3 col-sm-1">
                                    <img style="margin-top:20px;" class="mr-auto mb-4" src="data:image/*;base64,{{$feeChallan->schoolLogo}}" alt="" width="60" height="60">
                                </div>
                                <div class="col-md-7 col-sm-8 custom-margin-top" style="font-size: 12px;margin-top: 12px;">
                                    @if($feeChallan->ShowAccNoOnVoucher === 1)
                                        <div class="row bank-detail-div" style="border: solid;border-width: 1px;width: inherit;padding-top: 2px;">
                                            <div class="col-md-12 bank-detail-font" style="padding-bottom: 0px;">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;">{{$feeChallan->bankName}}</label>
                                            </div>
                                            <div class="col-md-12 bank-detail-font">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;">{{$feeChallan->bankBranch}}</label>
                                            </div>
                                            <div class="col-md-12 bank-detail-font">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;"><b>Account Number</b><br> {{$feeChallan->bankAccNo}}</label>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-2 col-sm-3" style="margin-left:-40px;">
                                    @if(isset($feeChallan->voucherUUID) && $feeChallan->voucherUUID)
                                        {!! QrCode::size(100)
                                        ->errorCorrection('H')
                                        ->generate($feeChallan->voucherUUID); !!}
                                    @endif
                                </div>
                                <div class="col-12 div-school-name">
                                    <h2>{{$feeChallan->schoolName}}</h2>
                                    <p>
                                        {{$feeChallan->address1}}

                                        {{$feeChallan->address2}}

                                        {{$feeChallan->address3}}
                                        <br/>
                                        <b>Phone #: </b>{{$feeChallan->contact1}}, {{$feeChallan->contact2}}
                                    </p>
                                </div>
                                <div class="row m-auto">
                                    <div class="col-12 text-center div-fee-voucher">
                                        <h2 class="btn y m-auto" disabled><b><u>FEE VOUCHER</u></b></h1>
                                    </div>
                                </div>
                                <div class="col-12 mt-2 btn y text-center div-billing">BILLING PERIOD:<strong class="pl-3">
                                        @if($feeChallan->billingPeriod)
                                        {{$feeChallan->billingPeriod}}
                                        {{-- {{\Carbon\Carbon::parse($feeChallan->billingPeriod)->formatLocalized('%B-%Y')}} --}}
                                        @endif
                                    </strong>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 order-md-1">
                                <div class="row">
                                    <div class="col-6 bb-1">Invoice No:<strong class="pl-3">{{$feeChallan->invoiceNumber}}</strong></div>
                                    <div class="col-6 bb-1">Invoice Date:<strong class="pl-3">{{\Carbon\Carbon::parse($feeChallan->invoiceDate)->formatLocalized('%d-%b-%Y')}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 bb-1">Class:<strong class="pl-3">{{$feeChallan->classAlias}}</strong></div>
                                    <div class="col-6 bb-1">Section:<strong class="pl-3">{{$feeChallan->classSection}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 bb-1">Roll No:<strong class="pl-3">{{$feeChallan->rollNo}}</strong></div>
                                    <div class="col-6 bb-1">Due Date:<strong class="pl-3">{{\Carbon\Carbon::parse($feeChallan->dueDate)->formatLocalized('%d-%b-%Y')}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-12  mt-2 bb-1">Student Name:<strong class="pl-3">{{$feeChallan->studentName}}</strong></div>
                                    <div class="col-12 mt-2 bb-1">Parent Name:<strong class="pl-3">{{$feeChallan->parentName}}</strong></div>
                                    <div class="col-12 mt-2 bb-1">CNIC:<strong class="pl-3">
                                        @if($feeChallan->parentCNIC)
                                            {{$feeChallan->parentCNIC}}
                                        @endif
                                    </strong></div>
                                </div>
                                <div class="row pt-3">
                                    <table class="table border">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="th-fee-type" width="200" scope="col">Fee Type</th>
                                                <th class="th-tax" width="150" scope="col">Tax</th>
                                                <th scope="col"><b>Amount</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($feeChallan->feeDetailsData) &&  count($feeChallan->feeDetailsData) > 0)
                                            @foreach($feeChallan->feeDetailsData as $detailKey => $detail)
                                                <tr>
                                                    <th>{{$detail->feeType}}</th>
                                                    <td><b>{{isset($detail->taxRate) && $detail->taxRate > 0 ? $detail->taxRate." %" : '-'}}</b></td>
                                                    <td><b>{{$feeChallan->currencyCode}} {{number_format($detail->feeTypeAmount)}}</b></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table border">
                                        <thead class="thead-light">
                                            <tr>
                                                <th  scope="col">Total Amount Before Tax</th>
                                                <th  scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->totalAmountBeforeDue,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Tax Amount</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmount,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount After Tax (Before Due Date)</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmountDue,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Penalty Amount (
                                                    @if($feeChallan->penaltyType === 'F')
                                                        {{'Fixed'}}
                                                    @else
                                                        {{'%'}}
                                                    @endif
                                                )</th>
                                                <th scope="col">
                                                @if($feeChallan->penaltyType === 'F')
                                                    {{$feeChallan->currencyCode}} {{number_format($feeChallan->penaltyValue,0)}}
                                                    @else
                                                        {{number_format($feeChallan->penaltyValue,0)." %"}}
                                                    @endif
                                                </th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount After Tax (After Due Date)</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmountAfterDue,0)}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-12 px-2"><b><u>Amount in Words:</u></b><span class="pl-3"> {{\Illuminate\Support\Str::title(App\Http\controllers\StudentFeeChallanController::numberInWords($feeChallan->taxAmountDue))." Only"}}</span></div>
                                </div>

                                <div class="row mt-2">
                                    <h5 class="px-2"><u>Notes:</u></h5>
                                    <div class="px-2" style="height:260px; overflow:hidden;">{!! $feeChallan->challanNote !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bb-dot-1"></div>
                </div>
                <div class="grid-item">
                    @if ($feeChallan->paymentStatus && $feeChallan->paymentStatus == 'P')
                        <img class="watermark" src="{{asset('assets/img/paid-stamp.png')}}" alt="">
                    @endif
                    <div class="container">
                        <div class="py-3 text-center">
                        <label class="m-auto copy-tab" disabled><b>Bank Copy</b></label>
                            <div class="row div-top">
                                <div class="col-md-3 col-sm-1">
                                    <img style="margin-top:20px;" class="mr-auto mb-4" src="data:image/*;base64,{{$feeChallan->schoolLogo}}" alt="" width="60" height="60">
                                </div>
                                <div class="col-md-7 col-sm-8 custom-margin-top" style="font-size: 12px;margin-top: 12px;">
                                    @if($feeChallan->ShowAccNoOnVoucher === 1)
                                        <div class="row bank-detail-div" style="border: solid;border-width: 1px;width: inherit;padding-top: 2px;">
                                            <div class="col-md-12 bank-detail-font" style="padding-bottom: 0px;">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;">{{$feeChallan->bankName}}</label>
                                            </div>
                                            <div class="col-md-12 bank-detail-font">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;">{{$feeChallan->bankBranch}}</label>
                                            </div>
                                            <div class="col-md-12 bank-detail-font">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;"><b>Account Number</b><br> {{$feeChallan->bankAccNo}}</label>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-2 col-sm-3" style="margin-left:-41px;">
                                    @if(isset($feeChallan->voucherUUID) && $feeChallan->voucherUUID)
                                        {!! QrCode::size(100)
                                        ->errorCorrection('H')
                                        ->generate($feeChallan->voucherUUID); !!}
                                    @endif
                                </div>
                                <div class="col-12 div-school-name">
                                    <h2>{{$feeChallan->schoolName}}</h2>
                                    <p>
                                        {{$feeChallan->address1}}

                                        {{$feeChallan->address2}}

                                        {{$feeChallan->address3}}
                                        <br/>
                                        <b>Phone #: </b>{{$feeChallan->contact1}}, {{$feeChallan->contact2}}
                                    </p>
                                </div>
                                <div class="row m-auto">
                                    <div class="col-12 text-center div-fee-voucher">
                                        <h2 class="btn y m-auto" disabled><b><u>FEE VOUCHER</u></b></h1>
                                    </div>
                                </div>
                                <div class="col-12 mt-2 btn y text-center div-billing">BILLING PERIOD:<strong class="pl-3">
                                        @if($feeChallan->billingPeriod)
                                        {{$feeChallan->billingPeriod}}
                                        {{-- {{\Carbon\Carbon::parse($feeChallan->billingPeriod)->formatLocalized('%B-%Y')}} --}}
                                        @endif
                                    </strong>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 order-md-1">
                                <div class="row">
                                    <div class="col-6 bb-1">Invoice No:<strong class="pl-3">{{$feeChallan->invoiceNumber}}</strong></div>
                                    <div class="col-6 bb-1">Invoice Date:<strong class="pl-3">{{\Carbon\Carbon::parse($feeChallan->invoiceDate)->formatLocalized('%d-%b-%Y')}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 bb-1">Class:<strong class="pl-3">{{$feeChallan->classAlias}}</strong></div>
                                    <div class="col-6 bb-1">Section:<strong class="pl-3">{{$feeChallan->classSection}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 bb-1">Roll No:<strong class="pl-3">{{$feeChallan->rollNo}}</strong></div>
                                    <div class="col-6 bb-1">Due Date:<strong class="pl-3">{{\Carbon\Carbon::parse($feeChallan->dueDate)->formatLocalized('%d-%b-%Y')}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-12  mt-2 bb-1">Student Name:<strong class="pl-3">{{$feeChallan->studentName}}</strong></div>
                                    <div class="col-12 mt-2 bb-1">Parent Name:<strong class="pl-3">{{$feeChallan->parentName}}</strong></div>
                                    <div class="col-12 mt-2 bb-1">CNIC:<strong class="pl-3">
                                        @if($feeChallan->parentCNIC)
                                            {{$feeChallan->parentCNIC}}
                                        @endif
                                    </strong></div>
                                </div>
                                <div class="row pt-3">
                                    <table class="table border">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="th-fee-type" width="200" scope="col">Fee Type</th>
                                                <th class="th-tax" width="150" scope="col">Tax</th>
                                                <th scope="col"><b>Amount</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($feeChallan->feeDetailsData) &&  count($feeChallan->feeDetailsData) > 0)
                                            @foreach($feeChallan->feeDetailsData as $detailKey => $detail)
                                                <tr>
                                                    <th>{{$detail->feeType}}</th>
                                                    <td><b>{{isset($detail->taxRate) && $detail->taxRate > 0 ? $detail->taxRate." %" : '-'}}</b></td>
                                                    <td><b>{{$feeChallan->currencyCode}} {{number_format($detail->feeTypeAmount)}}</b></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table border">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount Before Tax</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->totalAmountBeforeDue,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Tax Amount</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmount,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount After Tax (Before Due Date)</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmountDue,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Penalty Amount (
                                                    @if($feeChallan->penaltyType === 'F')
                                                        {{'Fixed'}}
                                                    @else
                                                        {{'%'}}
                                                    @endif
                                                )</th>
                                                <th scope="col">
                                                @if($feeChallan->penaltyType === 'F')
                                                    {{$feeChallan->currencyCode}} {{number_format($feeChallan->penaltyValue,0)}}
                                                    @else
                                                        {{number_format($feeChallan->penaltyValue,0)." %"}}
                                                    @endif
                                                </th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount After Tax (After Due Date)</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmountAfterDue,0)}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-12 px-2"><b><u>Amount in Words:</u></b><span class="pl-3"> {{\Illuminate\Support\Str::title(App\Http\controllers\StudentFeeChallanController::numberInWords($feeChallan->taxAmountDue))." Only"}}</span></div>
                                </div>

                                <div class="row mt-2">
                                    <h5 class="px-2"><u>Notes:</u></h5>
                                    <div class="px-2" style="height:260px; overflow:hidden;">{!! $feeChallan->challanNote !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bb-dot-1"></div>
                </div>
                <div class="grid-item">
                    @if ($feeChallan->paymentStatus && $feeChallan->paymentStatus == 'P')
                        <img class="watermark" src="{{asset('assets/img/paid-stamp.png')}}" alt="">
                    @endif
                    <div class="container">
                        <div class="py-3 text-center">
                        <label class="m-auto copy-tab" disabled><b>School Copy</b></label>
                            <div class="row div-top">
                                <div class="col-md-3 col-sm-1">
                                    <img style="margin-top:20px;" class="mr-auto mb-4" src="data:image/*;base64,{{$feeChallan->schoolLogo}}" alt="" width="60" height="60">
                                </div>
                                <div class="col-md-7 col-sm-8 custom-margin-top" style="font-size: 12px;margin-top: 12px;">
                                    @if($feeChallan->ShowAccNoOnVoucher === 1)
                                        <div class="row bank-detail-div" style="border: solid;border-width: 1px;width: inherit;padding-top: 2px;">
                                            <div class="col-md-12 bank-detail-font" style="padding-bottom: 0px;">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;">{{$feeChallan->bankName}}</label>
                                            </div>
                                            <div class="col-md-12 bank-detail-font">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;">{{$feeChallan->bankBranch}}</label>
                                            </div>
                                            <div class="col-md-12 bank-detail-font">
                                                <label class="bank-detail-label" style="margin-bottom: .0rem !important;"><b>Account Number</b><br> {{$feeChallan->bankAccNo}}</label>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-2 col-sm-3" style="margin-left:-40px;">
                                    @if(isset($feeChallan->voucherUUID) && $feeChallan->voucherUUID)
                                        {!! QrCode::size(100)
                                        ->errorCorrection('H')
                                        ->generate($feeChallan->voucherUUID); !!}
                                    @endif
                                </div>
                                <div class="col-12 div-school-name">
                                    <h2>{{$feeChallan->schoolName}}</h2>
                                    <p>
                                        {{$feeChallan->address1}}

                                        {{$feeChallan->address2}}

                                        {{$feeChallan->address3}}
                                        <br/>
                                        <b>Phone #: </b>{{$feeChallan->contact1}}, {{$feeChallan->contact2}}
                                    </p>
                                </div>
                                <div class="row m-auto">
                                    <div class="col-12 text-center div-fee-voucher">
                                        <h2 class="btn y m-auto" disabled><b><u>FEE VOUCHER</u></b></h1>
                                    </div>
                                </div>
                                <div class="col-12 mt-2 btn y text-center div-billing">BILLING PERIOD:<strong class="pl-3">
                                        @if($feeChallan->billingPeriod)
                                        {{$feeChallan->billingPeriod}}
                                        {{-- {{\Carbon\Carbon::parse($feeChallan->billingPeriod)->formatLocalized('%B-%Y')}} --}}
                                        @endif
                                    </strong>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 order-md-1">
                                <div class="row">
                                    <div class="col-6 bb-1">Invoice No:<strong class="pl-3">{{$feeChallan->invoiceNumber}}</strong></div>
                                    <div class="col-6 bb-1">Invoice Date:<strong class="pl-3">{{\Carbon\Carbon::parse($feeChallan->invoiceDate)->formatLocalized('%d-%b-%Y')}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 bb-1">Class:<strong class="pl-3">{{$feeChallan->classAlias}}</strong></div>
                                    <div class="col-6 bb-1">Section:<strong class="pl-3">{{$feeChallan->classSection}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 bb-1">Roll No:<strong class="pl-3">{{$feeChallan->rollNo}}</strong></div>
                                    <div class="col-6 bb-1">Due Date:<strong class="pl-3">{{\Carbon\Carbon::parse($feeChallan->dueDate)->formatLocalized('%d-%b-%Y')}}</strong></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-12  mt-2 bb-1">Student Name:<strong class="pl-3">{{$feeChallan->studentName}}</strong></div>
                                    <div class="col-12 mt-2 bb-1">Parent Name:<strong class="pl-3">{{$feeChallan->parentName}}</strong></div>
                                    <div class="col-12 mt-2 bb-1">CNIC:<strong class="pl-3">
                                        @if($feeChallan->parentCNIC)
                                            {{$feeChallan->parentCNIC}}
                                        @endif
                                    </strong></div>
                                </div>
                                <div class="row pt-3">
                                    <table class="table border">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="th-fee-type" width="200" scope="col">Fee Type</th>
                                                <th class="th-tax" width="150" scope="col">Tax</th>
                                                <th scope="col"><b>Amount</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($feeChallan->feeDetailsData) &&  count($feeChallan->feeDetailsData) > 0)
                                            @foreach($feeChallan->feeDetailsData as $detailKey => $detail)
                                                <tr>
                                                    <th>{{$detail->feeType}}</th>
                                                    <td><b>{{isset($detail->taxRate) && $detail->taxRate > 0 ? $detail->taxRate." %" : '-'}}</b></td>
                                                    <td><b>{{$feeChallan->currencyCode}} {{number_format($detail->feeTypeAmount)}}</b></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table border">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount Before Tax</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->totalAmountBeforeDue,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Tax Amount</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmount,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount After Tax (Before Due Date)</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmountDue,0)}}</th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Penalty Amount (
                                                    @if($feeChallan->penaltyType === 'F')
                                                        {{'Fixed'}}
                                                    @else
                                                        {{'%'}}
                                                    @endif
                                                )</th>
                                                <th scope="col">
                                                @if($feeChallan->penaltyType === 'F')
                                                    {{$feeChallan->currencyCode}} {{number_format($feeChallan->penaltyValue,0)}}
                                                    @else
                                                        {{number_format($feeChallan->penaltyValue,0)." %"}}
                                                    @endif
                                                </th>
                                            </tr>
                                        </thead>
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Total Amount After Tax (After Due Date)</th>
                                                <th scope="col">{{$feeChallan->currencyCode}} {{number_format($feeChallan->taxAmountAfterDue,0)}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="row mt-0">
                                    <div class="col-12 px-2"><b><u>Amount in Words:</u></b><span class="pl-3"> {{\Illuminate\Support\Str::title(App\Http\controllers\StudentFeeChallanController::numberInWords($feeChallan->taxAmountDue))." Only"}}</span></div>
                                </div>

                                <div class="row mt-2">
                                    <h5 class="px-2"><u>Notes:</u></h5>
                                    <div class="px-2" style="height:260px; overflow:hidden;">{!! $feeChallan->challanNote !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bb-dot-1"></div>
                </div>
            </div>

            <div class="pagebreak" style="page-break-after:always;"></div>

            @endforeach
        @endif
    @endforeach
@else
    <div class="container">
        <div class="row text-center mt-5">
                <h3 class="w-100 mt-5" style="color:#4680ff">No Data Available</h3>
        </div>
    </div>
@endif
  </body>
  <script>
      var css = '@page { size: 10.5in 8.5in; margin:0;}',
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
style.type = 'text/css';
style.media = 'print';

if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

head.appendChild(style);
  window.print();
  </script>
</html>
