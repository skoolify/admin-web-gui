@extends('master')

@section('title', Lang::get('messages.fee-voucher-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/select2/css/select2.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"> -->
<style>
    .switchToggle .slider.round {
    border-radius: 26px!important;
    }
    .switchToggle .slider {
        background-color: #dc3545;
    }
    .switchToggle .slider:before {
        left: 8px!important;
    }
    .round {
        width: 55px!important;
    }
</style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.fee-voucher-management')))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.billing-period-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.students-filter')
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="paymentStatus">@lang('messages.payment-status')</label>
                                    <select name="paymentStatus" id="payment-status-filter" class="form-control filter-select">
                                    <option value="">@lang('messages.selectStatus')</option>
                                    <option value="p">@lang('messages.paid')</option>
                                    <option value="u">@lang('messages.un-paid')</option>
                                    </select>
                                    <div id="payment-status-filter" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                    {!! $errors->first('paymentStatus','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('student-fee-challan.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.student-fee-challans')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    @endif

                    @if(Authorization::check(Route::currentRouteName(), "edit", Authorization::getParent(Route::currentRouteName()), true))
                        <a id="publish-all-vouchers" class="btn btn-round btn-danger pull-right hidden" data-toggle="tooltip" title="Print"><i class="fa fa-upload"></i> @lang('messages.publish')</a>

                        <a id="unpublish-all-vouchers" class="btn btn-round btn-danger pull-right hidden" data-toggle="tooltip" title="Print"><i class="fa fa-upload"></i> @lang('messages.un-publish')</a>
                    @endif
                    @if(Authorization::check(Route::currentRouteName(), "read", Authorization::getParent(Route::currentRouteName()), true))
                        <button class="btn btn-success pull-right pr-2 pl-2 hidden" id="download-csv" style="margin-left: 10px;"><i class="fa fa-download"></i> @lang('messages.export-excel')</button> <i class="exportLoader hidden fa fa-spinner fa-pulse pull-right mt-3" style="font-size:24px;color: #188ae2;"></i>
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th style="width: 12%">@lang('messages.roll-number')</th>
                                    <th>@lang('messages.student-name')</th>
                                    <th>@lang('messages.invoice-date')</th>
                                    <th>@lang('messages.total-fee')</th>
                                    <th>@lang('messages.after-due-date')</th>
                                    <th>@lang('messages.paid')/@lang('messages.un-paid')</th>
                                    <th>@lang('messages.detail')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="due-date"></td>
                                    <td class="payment-date"></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
       <div style="text-align:center">
           <h5 class="" id="billing-period-modal" style="color:#337ab7;font-weight:bold;position: absolute; right: 0;left: 0;padding-top: 2px;font-size: 18px !important;" > @lang('messages.billing-period')</h5>
        </div>

        <a class="closeModalDiv" data-dismiss="modal" aria-label="Close" style="z-index:1000">
          <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
        </a>
      </div>
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <h4 class="modal-title" style="margin-top: 12px;margin-left:0px !important; position: absolute;color:#337ab7 !important" id="exampleModalLabel" > @lang('messages.detail') (<span style="color:#337ab7; font-weight:bold;" id="student-name"></span> )</h4>
                </div>

                <div class="col-md-4" style="text-align: center;">
                    <label id="successLabel" style="font-weight: 500;padding-top: 10px; color:red;display:none;" for="">@lang('messages.successfully-updated')</label>
                </div>

                @if(Authorization::check(Route::currentRouteName(), "edit",  Authorization::getParent(Route::currentRouteName()), true))
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-9">
                            <label style="float: right;padding-top: 10px;color: #337ab7;font-size: 18px !important;font-weight: bold;" id="publishedToggleLabel" for="">@lang('messages.publish') @lang('messages.status-column') </label>
                        </div>
                        <div class="col-md-3">
                            <label style="margin-top: 12px;float: right;" class="switchToggle">
                                <input type="checkbox" id="publishedToggle" value="">
                                <span id="publishedToggleSpan" class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
                @else
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-9">
                            <label style="float: right;padding-top: 10px;color: #337ab7;font-size: 18px !important;font-weight: bold;" id="publishedToggleLabel" for=""></label>
                        </div>
                        <div class="col-md-3">
                            <label style="margin-top: 12px;float: right;" class="switchToggle">
                            </label>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="container-fluid">
            <div class="row" style="border-bottom: 1px solid #EFEFEF;">
                <div class="col-md-12">
                    <table class="table" id="fee-detail">
                        <tbody>
                            <tr>
                                <td class="p-1">@lang('messages.invoice-number') </td>
                                <td class="p-1 br-1" id="invoiceNumbers"> </td>
                                <td class="p-1">@lang('messages.invoice-date') </td>
                                <td class="p-1" id="invoiceDates"> </td>
                            </tr>
                            <tr>
                                <td class="p-1">@lang('messages.invoice-due-date') </td>
                                <td class="p-1 br-1" id="invoiceDueDate"> </td>
                                <td class="p-1">@lang('messages.payment-date') </td>
                                <td class="p-1" id="invoicePaymentDate"> </td>
                            </tr>
                            <tr>
                                <td class="p-1">@lang('messages.amount-before-tax') </td>
                                <td class="p-1 br-1" id="amountBeforeTax"> </td>
                                <td class="p-1">@lang('messages.late-fee-charges') </td>
                                <td class="p-1" id="invoicePenaltyValue"> </td>

                            </tr>
                            <tr>
                                <td class="p-1">@lang('messages.tax-amount') </td>
                                <td class="p-1 br-1" id="taxAmount"> </td>
                                <td class="p-1">@lang('messages.payment-status') </td>
                                <td class="p-1" id="paymentStatus"> </td>

                            </tr>
                            <tr>
                                <td class="p-1">@lang('messages.total-amount-before-due-date') </td>
                                <td class="p-1 br-1" id="invoiceTaxAmountDue"> </td>
                                <td class="p-1">@lang('messages.total-amount-after-due-date') </td>
                                <td class="p-1" id="invoiceTaxAmountAfterDue"> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <h4 class="modal-title" style="color:#337ab7" > @lang('messages.fee-note')</h4>
                </div>
                <div class="col-md-2" style="padding-top: 10px;">
                    <a style="float: right;" id="invoicechallanNote" style="text-align: center;"></a>
                </div>
            </div>

            <div id="noteTableRow" style="border-bottom: 1px solid #EFEFEF;display:none;" class="row">
                <div id="hiddenFeeChallanNote" class="col-md-12">

                </div>
            </div>

        <div class="row">
            <div class="col-md-12"  style="height:203px; overflow-y:scroll; margin-bottom:20px;">
                    <h4 class="modal-title" id="exampleModalLabel" style="color:#337ab7" > @lang('messages.fee-types')</h4>
                <table class="table " id="fee-detail-table">
                    <thead>
                        <tr>
                            <th class="p-1">@lang('messages.fee-type')</th>
                            <th class="p-1">@lang('messages.fee-amount')</th>
                            <th class="p-1">@lang('messages.action-column')</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  class="p-2 text-center center" colspan="3">@lang('messages.record-not-found')</td>
                        </tr>
                    </tbody>
                </table>
                <span id="success-span" style="color:green"></span>
                <span id="error-span" style="color:red" ></span>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger cancelBtnModal" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.js')}}" ></script>
<script src="{{ asset('assets/js/pages/select2/select2-init.js')}}" ></script>


@endpush
