<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="student-fee-challan-form" action="{{ url('exam-term-results/add') }}" novalidate>
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group">
                    <div class="checkbox checkbox-aqua auto-generate-checkbox">
                        <input id="invoiceNumberCheckBox" checked class="studentCheckBox invoiceCheckBox" type="checkbox">
                        <label for="invoiceNumberCheckBox">
                        @lang('messages.auto-generate')
                        </label>
                    </div>
                        <label for="overallNotes" class="invoice-number-label">@lang('messages.invoice-number')</label>
                        <input class ="form-control invoiceNumber" name="invoiceNumber" disabled data-validation="required"  data-validation-error-msg="{{Lang::get('invoice-number-is-required')}}" type = "text" id = "invoiceNumber" placeholder="{{Lang::get('messages.invoice-number')}}" value="{{ old('invoiceNumber') }}"  autocomplete="off"/>
                        <span id="errmsg" style="color:red;"></span>
                        {!! $errors->first('invoiceNumber','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                 <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('quizDate') ? 'has-error' :'' }}">
                        <label for="quizDate">@lang('messages.invoice-date')
                        </label>
                        <input class = "form-control" name="invoiceDate" type = "date" id = "invoiceDate" placeholder="Invoice Date" value="{{ old('quizDate') }}" data-validation="required"  data-validation-error-msg="Invoice Date is required."  />
                        {!! $errors->first('invoiceDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                 <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('quizDate') ? 'has-error' :'' }}">
                        <label for="quizDate">@lang('messages.due-date')
                        </label>
                        <input class = "form-control" name="dueDate" type = "date" id = "dueDate" placeholder="Due Date" value="{{ old('dueDate') }}" data-validation="required"  data-validation-error-msg="Due Date is required."  />
                        {!! $errors->first('dueDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div style="margin-top: 30px;" class="form-group {{ $errors->has('voucherNote') ? 'has-error' :'' }}">
                    <textarea class="hidden" name="voucherNote" id="voucherNote" cols="30" rows="10"></textarea>
                        <a id="openNotesModal" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('messages.add-notes')</a>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group">
                        <label for="">@lang('messages.recurring-fee')</label>
                        <ul id="fee-type-filters-list" type="square">
                        </ul>
                        <div id="p2-roles" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col-lg-3 p-t-20">
                    <label for="activeField">
                        @lang('messages.penalty-type')
                    </label>
                    <div class="radio p-0">
                        <input type="radio" class="radio-button" name="penaltyType" id="activeFieldF" value="F" checked />
                        <label for="activeFieldF">
                            @lang('messages.fixed')
                        </label>
                        <input style="margin-left:10px;" class="radio-button" type="radio" name="penaltyType" id="activeFieldP"  value="P" />
                        <label for="activeFieldP">
                            @lang('messages.percentage')
                        </label>
                    </div>
                </div>
                <div class="col-lg-3 p-t-20">
                    <label for="activeField">
                        @lang('messages.penalty-value')
                    </label>
                    <input class = "form-control  penaltyValue" data-validation="number" data-validation-allowing="float" name="penaltyValue" type = "text" id = "penaltyValue" placeholder="{{Lang::get('fixed-amount-figure')}}" value="{{ old('penaltyValue') }}"  />
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-10 p-t-20 col-xs-8">
                            <table class="table table-striped table-hover table-responsive-lg  order-column valign-middle fee-type-table" id="fee-type-table" style="width: 100% !important;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th width="40%">@lang('messages.fee-type')</th>
                                        <th>@lang('messages.tax-rate-in-percentage')</th>
                                        <th>@lang('messages.amount')</th>
                                        <th>@lang('messages.action-column')</th>
                                    </tr>
                                </thead>
                                <tbody id="fee-type-table-body">
                                </tbody>
                            </table> 
                            <div id="p2-feeTypesTable" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div> 
                        </div>
                        <div class="col-lg-2 col-xs-4">
                            <button class="btn btn-primary btn-round btn-md fee-type-add-button" type="button" data-toggle="tooltip" title="Add Fee type"><i class="fa fa-plus"></i> @lang('messages.add-fee-type')</button>
                        </div>   
                    </div>
                </div>
            </div>
                <!-- Recurring error will show here  -->
                <div class="col-lg-12 p-t-20 text-center">
                    <ol id="assigned-recurring-error" class="text-left" style="color:red !important;font-weight:bold" type="1">
                    </ol>
                </div>
                
                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
        <div class="modal fade" id="notesModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.add-notes')</h4>
                    <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color:red">&times;</span>
                    </button>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <label for="roleName">@lang('messages.notes')</label>
                            <textarea name="voucherNoteTextArea" id="voucherNoteTextArea" style="width:100%;height:400px;" data-id="contract" class="contract-summernote" data-validation="required" data-validation-error-msg="Description is required.">
                            </textarea>  
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="saveVoucherNote" class="btn btn-primary" data-dismiss="modal">@lang('messages.save')</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
