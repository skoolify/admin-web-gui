@extends('master')

@section('title', Lang::get('messages.registration-reports'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<style>
    .card-box:hover {
     -webkit-transform: none !important;
     transform: none !important;
     box-shadow: 0 20px 20px rgba(0,0,0,.1);
     -moz-box-shadow: 0 20px 20px rgba(0,0,0,.1);
     -webkit-box-shadow: 0 20px 20px rgba(0,0,0,.1);
 }
 </style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.registration-reports')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="#" id="search-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-multiple-filter')
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12" id="registration-mobile-no">
                                <div class="form-group">
                                    <label for="mobileNo">@lang('messages.mobile-number')</label>
                                    <input maxlength="15" class="form-control" name="mobileNo"  type="text" id = "mobileNo" placeholder="+923121234567" data-validation="length" data-validation-length="max15" dir="ltr"/>
                                    {{-- <input maxlength="15" class="form-control" name="mobileNo"  type="text" id = "mobileNo" placeholder="{{App::getLocale() === 'en' ? '+923121234567' : '923121234567+'}}" data-validation="length" data-validation-length="max15" dir="ltr"/> --}}
                                    {{-- <input maxlength="15" class="form-control" name="mobileNo"  type="text" id = "mobileNo" placeholder="+923121234567" data-validation="length" data-validation-length="max15" /> --}}
                                    {!! $errors->first('mobileNo','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-3 justify-content-center align-self-center mt-3">
                                <button class="btn btn-primary align-middle" type="submit" id="search"><i class="fa fa-search"></i> @lang('messages.search')</button>
                                <button class="btn btn-warning" id="clear-btn" type="button" title="Clear"><i class="fa fa-refresh"></i> @lang('messages.clear')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('fee-type.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.registration-reports')</header>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <p>@lang('messages.total-parents') - <strong id="overall-total">0</strong></p>
                        </div>
                        <div class="col-md-4 text-center">
                            <p>@lang('messages.registered-parents') - <strong id="overall-registered">0</strong></p>
                        </div>
                        <div class="col-md-4 text-right">
                            <p>@lang('messages.un-registered-parents') - <strong id="overall-unregistered">0</strong></p>
                        </div>
                        <div class="col-md-4">
                            <p>@lang('messages.active-students') - <strong id="overall-active-students">0</strong></p>
                        </div>
                        <div class="col-md-4 text-center">
                            <p>@lang('messages.registered-students') - <strong id="unique-registered-students">0</strong></p>
                        </div>
                        <div class="col-md-4 text-right">
                            <p>@lang('messages.inactiveStudents') - <strong id="overall-non-active-students">0</strong></p>
                        </div>
                    </div>
                    <div class="table-scrollable" id="reports-data" >
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle fixed_header-reports" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th class="jconfirm-title-c w-73">#</th>
                                    <th class="jconfirm-title-c w-200">@lang('messages.parent-name')</th>
                                    <th class="jconfirm-title-c w-200">@lang('messages.mobile-number')</th>
                                    <th class="jconfirm-title-c w-200">@lang('messages.students')</th>
                                    <th class="jconfirm-title-c w-200">@lang('messages.status-column')</th>
                                    <th class="jconfirm-title-c w-300">@lang('messages.registration-date')</th>
                                </tr>
                            </thead>
                            <tbody class="reports-data">

                            </tbody>
                        </table>
                        <div id="loadmoreajaxloader" style="display:none;"><center><img class="row center text-center" src="{{asset('loading.gif')}}" height="50" width="50" /></center></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="studentDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.linked-student-list')</h4>
        <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
      </div>
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12" style = "max-height:400px;overflow:auto;">
                <table class="table table-striped table-bordered table-responsive-lg table-responsive-xs table-hover table-checkable" id="linked-student-list">
                    <thead >
                        <tr>
                            <th>#</th>
                            <th>@lang('messages.roll-number')</th>
                            <th>@lang('messages.name')</th>
                            <th>@lang('messages.dob')</th>
                            <th>@lang('messages.branch')</th>
                            <th>@lang('messages.shift')</th>
                            <th>@lang('messages.class')</th>
                            <th>@lang('messages.year')</th>
                            <th>@lang('messages.status-active')</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://unpkg.com/@webcreate/infinite-ajax-scroll/dist/infinite-ajax-scroll.min.js"></script>
@endpush
