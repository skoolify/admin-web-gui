<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="parents-form" action="{{ url('parents/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">
                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.basic-information')</h3><hr>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('mobileNo') ? 'has-error' :'' }}">
                        <label for="mobileNo" id="phoneNumberLabel">@lang('messages.mobile-no')
                        </label>
                            <input class = "form-control mobile-no-validation" type = "input" id = "phoneNumber" placeholder="{{Lang::get('messages.mobile-no')}}" maxlength="15" value="{{ old('mobileNo') }}"   onkeypress="return validatePhoneNumber(event)"  name="mobileNumber" dir="ltr"/>
                        <p id="phone-error" class="text-danger phone-error" style="display: none;"></p>
                        {!! $errors->first('mobileNo','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('parentName') ? 'has-error' :'' }}">
                        <label for="parentName">@lang('messages.first-name')
                        </label>
                        <input maxlength="100" class = "form-control" name="parentName" type = "text" id = "parentName" placeholder="{{Lang::get('messages.first-name')}}" value="{{ old('parentName') }}" data-validation="required length" data-validation-length="max100" />
                        {!! $errors->first('parentName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('parentLastName') ? 'has-error' :'' }}">
                        <label for="parentLastName">@lang('messages.last-name')
                        </label>
                        <input maxlength="100" class = "form-control" name="parentLastName" type = "text" id = "parentLastName" placeholder="{{Lang::get('messages.last-name')}}" value="{{ old('parentLastName') }}" data-validation="required length" data-validation-length="max100" />
                        {!! $errors->first('parentLastName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('parentCNIC') ? 'has-error' :'' }}">
                        <label for="parentCNIC">@lang('messages.national-id')
                        </label>
                        <input class = "form-control" name="parentCNIC" maxlength="20" type = "text" id = "parentCNIC"  type="text"  placeholder="{{Lang::get('messages.national-id')}}" value="{{ old('parentCNIC') }}" dir="ltr"/>
                        {!! $errors->first('parentCNIC','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' :'' }}">
                        <label for="email">@lang('messages.email')</label>
                        <input maxlength="100" class = "form-control" name="email" type = "email" id = "email" placeholder="{{Lang::get('messages.email')}}" value="{{ old('email') }}" data-validation="length" data-validation-length="max100"  />
                        {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('parentType') ? 'has-error' :'' }}">
                        <label for="parentType">@lang('messages.parent-type')</label>
                        <select name="parentType" id="parentType" class="form-control" data-validation="required" data-validation-error-msg="{{Lang::get('messages.parent-type-is-required')}}">
                            <option value="">@lang('messages.select-type')</option>
                            <option value="F">@lang('messages.father')</option>
                            <option value="M">@lang('messages.mother')</option>
                            <option value="G">@lang('messages.guardian')</option>
                        </select>
                        {!! $errors->first('parentType','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <!-- @include('partials.active-field') -->

                <div class="col-lg-3 p-t-20" style="margin-top: 36px;">
                    <div class="checkbox checkbox-aqua">
                        <input type="hidden" name="isActive" id="isActive" value="0" />
                        <input class="notification-only" id="checkIsActive" type="checkbox" checked>
                        <label for="checkIsActive">
                        <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                        </label>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20" style="margin-top: 36px;">
                    <div class="checkbox checkbox-aqua" style="padding-left:0px !important;">
                        <input class="tax-beneficiary" id="tax-beneficiary" name="isTaxRegistered" type="checkbox" value="0">
                        <label for="tax-beneficiary">
                        <p style="margin-top: -3px;">@lang('messages.tax-beneficiary')</p>
                        </label>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div id="taxId" class="hidden form-group {{ $errors->has('TaxID') ? 'has-error' :'' }}">
                        <label for="TaxID">@lang('messages.tax-id') <span class="required">*</span>
                        </label>
                        <input maxlength="50" class = "form-control" name="taxID" type = "text" id="TaxID" placeholder="{{Lang::get('messages.tax-id')}}" value="{{ old('TaxID') }}" data-validation="required alphanumeric length" data-validation-length="max50" />
                        {!! $errors->first('TaxID','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.optional-information')</h3><hr>
                </div>

                @include('partials.components.countries-filter', array('all'=>'All'))
                @include('partials.components.cities-filter', array('all'=>'All'))

                <div class="col-lg-3">

                </div>
                <div class="col-lg-3">

                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('addressLine1') ? 'has-error' :'' }}">
                        <label for = "mdl-addressLine1" >@lang('messages.address-1')
                        </label>
                        <input type="text" placeholder="{{Lang::get('messages.address-1')}}" maxlength="40" class = "form-control" name="addressLine1" id = "addressLine1" data-validation="length" data-validation-length="max100" >{{ old('addressLine1') }}</input>
                        {!! $errors->first('addressLine1','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('addressLine2') ? 'has-error' :'' }}">
                        <label for = "mdl-addressLine2" >@lang('messages.address-2')
                        </label>
                        <input type="text" placeholder="{{Lang::get('messages.address-2')}}" maxlength="40" class = "form-control" name="addressLine2" id = "addressLine2" data-validation="length" data-validation-length="max100" >{{ old('addressLine2') }}</input>
                        {!! $errors->first('addressLine2','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class=" form-group col-lg-3 p-t-20 {{ $errors->has('addressState') ? 'has-error' :'' }}">
                    <label for="addressState" >@lang('messages.state')</label>
                    <input maxlength="45" class="form-control" name="addressState" type="text" value="{{ old('addressState') }}" data-validation="length" data-validation-length="max45" id="addressState" placeholder="{{Lang::get('messages.state')}}" />
                    {!! $errors->first('addressState','<span class="help-block">:message</span>') !!}
                </div>

                <div class=" form-group col-lg-3 p-t-20 {{ $errors->has('addressPostalCode') ? 'has-error' :'' }}">
                    <label for="addressPostalCode" >@lang('messages.postal-code')</label>
                    <input maxlength="45" class="form-control" name="addressPostalCode" type="text" value="{{ old('addressPostalCode') }}" data-validation="length" data-validation-length="max45" id="addressPostalCode" placeholder="{{Lang::get('messages.postal-code')}}" />
                    {!! $errors->first('addressPostalCode','<span class="help-block">:message</span>') !!}
                </div>



                <div class="col-lg-12 p-t-20 text-center">
                    <!-- <button type="button" id="link-student-button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-success hidden"><strong>Link Student</strong></button> -->
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-default">@lang('messages.cancel-btn')</button>
                    <button type="button" id="reset-add-form" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-danger">@lang('messages.reset')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
