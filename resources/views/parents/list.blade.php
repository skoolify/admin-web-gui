@extends('master')
@section('title', Lang::get('messages.parents-management'))
@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.parents-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')

                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('searchMobileNumber') ? 'has-error' :'' }}">
                                    <label for="searchMobileNumber">@lang('messages.mobile-number')
                                            {{-- <span class="required"> * </span> --}}
                                    </label>
                                    <input class = "form-control" name="searchMobileNumber" type = "text" id = "searchMobileNumber" placeholder="+923121234567" value="{{ old('searchMobileNumber') }}" dir="ltr" />
                                    {!! $errors->first('searchMobileNumber','<span class="help-block">:message</span>') !!}
                                    <p id="phone-error" class="text-danger phone-error"></p>
                        {!! $errors->first('mobileNo','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('searchParentName') ? 'has-error' :'' }}">
                                    <label for="searchParentName">@lang('messages.parent-name')
                                    </label>
                                    <input class = "form-control" name="searchParentName" type = "text" id = "searchParentName" placeholder="@lang('messages.parent-name')" value="{{ old('searchParentName') }}"  />
                                    <div class="invalid-feedback">
                                        @lang('messages.required-field-error')
                                    </div>
                                    {!! $errors->first('searchParentName','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('searchParentCNIC') ? 'has-error' :'' }}">
                                    <label for="searchParentCNIC">@lang('messages.nationalID')
                                    </label>
                                    <input class = "form-control" name="searchParentCNIC" type = "text" id = "searchParentCNIC" placeholder="@lang('messages.parentNationalID')" value="{{ old('searchParentCNIC') }}"  />
                                    <div class="invalid-feedback">
                                        This field is required
                                    </div>
                                    {!! $errors->first('searchParentCNIC','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-lg-1 justify-content-center align-self-center">
                                {{-- <div class="form-group"> --}}
                                    <button class="btn btn-primary search-parent align-middle mt-3" type="submit"><i class="fa fa-search"></i> @lang('messages.search')</button>
                                {{-- </div> --}}
                            </div>

                            <div class="col-lg-1 justify-content-center align-self-center ml-4">
                               <button class="btn btn-warning clear-parent align-middle mt-3" type="button"><i class="fa fa-refresh"></i> @lang('messages.clear') </button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('parents.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.parents-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.name')</th>
                                    <th>@lang('messages.nationalID')</th>
                                    <th>@lang('messages.mobile')</th>
                                    <th>@lang('messages.email')</th>
                                    <th>@lang('messages.parent')</th>
                                    <th>@lang('messages.registered')</th>
                                    <th>@lang('messages.linked-students')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="linkedStudentDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.linked-student-list')</h4>
        <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
      </div>
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12" style = "max-height:400px;overflow:auto;">
                <table class="table table-striped table-bordered table-responsive-lg table-responsive-xs table-hover table-checkable" id="linked-student-list">
                    <thead >
                        <tr>
                            <th>#</th>
                            <th>@lang('messages.roll-number')</th>
                            <th>@lang('messages.name')</th>
                            <th>@lang('messages.dob')</th>
                            <th>@lang('messages.branch')</th>
                            <th>@lang('messages.shift')</th>
                            <th>@lang('messages.class')</th>
                            <th>@lang('messages.year')</th>
                            <th>@lang('messages.status-active')</th>
                            <th>@lang('messages.action-column')</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="linkedStudentAssignModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title link-unlink-title" id="exampleModalLabel" style="color:green;font-weight:bold;font-size:20px;" >@lang('messages.link-student')</h4>
        <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close" style="color:red !important">
          <span aria-hidden="true">&times;</span>
        </button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
      </div>
      <div class="container-fluid"  style = "min-height:300px;overflow:auto;">
        <div class="row">
            <div class="col-md-12 col-xs-12" id="class-div">
                <div class="form-group {{ $errors->has('classLevelName') ? 'has-error' :'' }}">
                    <label for="classLevelName">@lang('messages.class')
                        <span class="required"> * </span>
                    </label>
                    <select name="classLevelID" id="filter-classLevels" class="form-control filter-select">
                        <option value="">@lang('messages.select-class-name')</option>
                    </select>
                    <div id="p2-classLevels" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    <p class="text-danger class-level-error"></p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12" id="section-div">
            <div class="form-group">
                <label for="">@lang('messages.section')</label>
                <select name="classID" id="filter-classes" class="form-control filter-select">
                    <option value="">{{ isset($all) && $all ? $all : 'Select' }} @lang('messages.section')</option>
                </select>
                <div id="p2-classes" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                <p class="text-danger class-error"></p>
            </div>
            </div>
            <div class="col-md-12 col-xs-12" >
                <div class="form-group">
                    <label for="">@lang('messages.student')</label>
                    <select name="studentID" id="filter-students" class="form-control filter-select">
                        <option value="">{{ isset($all) && $all ? $all : 'Select' }} @lang('messages.student')</option>
                    </select>
                    <div id="p2-students" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    <p class="text-danger student-error"></p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="form-group">
                <label for=""></label>
                    <button type="button" class="btn btn-round btn-primary btn-block link-unlink-submit">
                        @lang('messages.link-student')
                    </button>
                </div>
            </div>
            <div class="col-lg-12 p-t-20" id="link-form-errors">
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
