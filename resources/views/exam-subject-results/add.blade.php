<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="exam-subject-results-form" action="{{ url('exam-subject-results/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('totalMarks') ? 'has-error' :'' }}">
                        <label for="totalMarks">Total Marks
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="totalMarks" type = "text" id = "totalMarks" placeholder="Total Marks" value="{{ old('totalMarks') }}"  />
                        {!! $errors->first('totalMarks','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('obtainedMarks') ? 'has-error' :'' }}">
                        <label for="obtainedMarks">Obtained Marks
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="obtainedMarks" type = "text" id = "obtainedMarks" placeholder="Obtained Marks" value="{{ old('obtainedMarks') }}"  />
                        {!! $errors->first('obtainedMarks','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('grade') ? 'has-error' :'' }}">
                        <label for="grade">Grade
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="grade" type = "text" id = "grade" placeholder="Grade" value="{{ old('grade') }}"  />
                        {!! $errors->first('grade','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('notes') ? 'has-error' :'' }}">
                        <label for="notes">Notes
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="notes" type = "text" id = "notes" placeholder="Notes" value="{{ old('notes') }}"  />
                        {!! $errors->first('notes','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
