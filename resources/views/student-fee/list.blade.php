@extends('master')

@section('title', Lang::get('messages.student-fee-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.student-fee-management')))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.fee-type-filter')
                            @include('partials.components.students-filter')
                           
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('student-fee.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.student-fee-list')</header>
                   
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')  / <i class="fa fa-pencil"></i> @lang('messages.edit-btn')</button>
                    @endif
                </div>
                
                <div class="card-body">
                <div class="row">
                    <div class="offset-md-4 col-md-4">
                        <label class="center text-center justify-content-center label label-lg academic-flag" style="display:none;"> </label>    
                    </div>
                </div>
                
                    <div class="table-scrollable">
                        <div class="table-scrollable" id="student-fee-list">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="recurring-result-table">
                                <thead>
                                    <tr>
                                        <th>#</th>      
                                        <th>@lang('messages.name')</th>                              
                                        <th>@lang('messages.type')</th>
                                        <th>@lang('messages.amount')</th>
                                    </tr>
                                </thead>
                                <tbody id="student-fee-table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
@endpush
