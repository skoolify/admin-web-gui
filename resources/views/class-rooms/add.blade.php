<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="class-rooms-form" action="{{ url('class-rooms/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                    <div class="col-lg-3 p-t-20">
                        <div class="form-group {{ $errors->has('roomName') ? 'has-error' :'' }}">
                            <label for="classAlias">@lang('messages.room-name')
                            </label>
                            <input maxlength="45" class = "form-control" name="roomName" type = "text" id = "roomName" placeholder="{{Lang::get('messages.room-name')}}" value="{{ old('roomName') }}" data-validation="required length" data-validation-length="max45"  />
                            {!! $errors->first('roomName','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="col-lg-3 p-t-20">
                        <div class="form-group {{ $errors->has('roomDesc') ? 'has-error' :'' }}">
                            <label for="classAlias">@lang('messages.room-description')
                            </label>
                            <textarea maxlength="100" class = "form-control" rows="5" name="roomDesc" id = "roomDesc" data-validation="length" data-validation-length="max100" placeholder="{{Lang::get('messages.room-description')}}">{{ old('roomDesc') }}</textarea>
                        </div>
                    </div>

                    <div class="col-lg-3 p-t-20">
                        <div class="form-group {{ $errors->has('deviceIdentifier') ? 'has-error' :'' }}">
                            <label for="classAlias">@lang('messages.device-identifier')
                            </label>
                            <input maxlength="100" class = "form-control" name="deviceIdentifier" type = "text" id = "deviceIdentifier" placeholder="{{Lang::get('messages.device-identifier')}}" value="{{ old('deviceIdentifier') }}" data-validation="length" data-validation-length="max100"/>
                            {!! $errors->first('deviceIdentifier','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="col-lg-3 p-t-20">
                        <div class="row checkbox-div">
                            <div class="form-group {{ $errors->has('isClassable') ? 'has-error' :'' }}">
                                <div class="checkbox checkbox-aqua d-flex">
                                    <input class="isClassable" id="isClassable" name="isClassable" type="checkbox">
                                    <label for="isClassable" id="check-box-right">
                                    <p style="margin-top: -3px;">@lang('messages.class-room')</p>
                                    </label>
                                </div>
                                <div class="checkbox checkbox-aqua d-flex">
                                    <input class="isActive" id="isActive" name="isActive" type="checkbox">
                                    <label for="isActive" id="check-box-right">
                                    <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
