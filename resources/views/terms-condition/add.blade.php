@push('extra-css')
    <style>
        .jodit-container{
            text-align: right !important;
        }
    </style>
@endpush
<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="term-condition-form">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('roleName') ? 'has-error' :'' }}">
                        <label for="roleName">Version
                        </label>
                        <input class = "form-control" name="version" type = "text" id = "version" value="{{ old('version') }}" data-validation="required" data-validation-error-msg="Version is required.">
                        {!! $errors->first('roleName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @include('partials.published-field')
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('roleName') ? 'has-error' :'' }}">
                        <label for="roleName">@lang('messages.terms-condition')
                        </label>
                        <textarea name="tncText" id="summernote" style="width:100%;height:400px;" data-id="termsCondition" class="terms-conditions" placeholder="Terms & Condition">
                        </textarea>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('roleName') ? 'has-error' :'' }} arabicSummernote">
                        <label for="roleName">الشروط والأحكام (عربي) </label>
                        <textarea name="tncText_ar" id="arabicSummernote" style="width:100%;height:400px;text-align: right !important;" data-id="termsCondition" class="arabic-terms-conditions" placeholder="Terms & Condition">
                        </textarea>
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    {{-- <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button> --}}

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-3">
                            <i class="submitLoader hidden fa fa-spinner fa-pulse pull-left mt-3 mr-2" style="font-size:24px;color: #188ae2;"></i>
                            <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                            <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
