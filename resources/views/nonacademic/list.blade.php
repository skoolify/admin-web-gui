@extends('master')

@section('title', Lang::get('messages.non-academic-result-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.non-academic-result-management')))

    <div class="row">
    <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.exam-term-filter')
                            @include('partials.components.non-subjects-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('nonacademic.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.non-academic-result-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')  / <i class="fa fa-pencil"></i> @lang('messages.edit-btn')</button>
                    @endif
                </div>
                
                <div class="card-body">
                    <div class="row">
                        <div class="offset-md-4 col-md-4">
                            <label class="label label-lg non-academic-flag" style="display:none"></label>  
                        </div>
                    </div> 
                    <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="example44">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('messages.student-name')</th>                                    
                                <th>@lang('messages.non-subject-name')</th>
                                <th>@lang('messages.notes')</th>
                                <th>@lang('messages.grade')</th>
                                <th>@lang('messages.action-column')</th>
                            </tr>
                        </thead>
                        <tbody id="non-academic-result-list-table">
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
@endpush
