<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        <form method="POST" id="non-academic-form" action="{{ url('nonacademic/add') }}">
            <div class="container-fluid">
                <div class="col-lg-12">
                <div id="p2-students-result-list" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100% !important">
                    </div>
                    <table class="table table-striped  table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="student-non-academic-result-table">
                        <thead>
                            <tr>
                                <th>@lang('messages.s-number')</th>
                                <th>@lang('messages.roll-number')</th>
                                <th>@lang('messages.student')</th>
                                <th>@lang('messages.legend')</th>
                                <th width="40%" class="text-center">@lang('messages.notes')</th>
                                <th>@lang('messages.grades')</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="5"><strong style="color:#188ae2;font-weight:bold">@lang('messages.select-exam-term-first')</<strong></td>
                        </tr>
                        </tbody>
                    </table>
                   
                    <div class="col-lg-12 p-t-20" id="add-form-errors"></div>
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                        <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                    </div>
                </div>
            </div>
        </form>   
    </div>
</div>

