<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="class-levels-form" action="{{ url('class-levels/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                    <div class="col-lg-3 p-t-20">
                        <label for="classLevelName">@lang('messages.class-name')
                        <span class="required" style="color:red !important"> * </span>
                        </label>
                        <select name="classLevelName" id="classLevelName" class="form-control" data-validation="required" data-validation-error-msg="class Level is required.">
                            <option value="">@lang('messages.select') @lang('messages.class-name')</option>
                            @foreach (Config::get('constants.classLevels') as $key => $level)
                            <option value="{{ $key }}" {{ isset($recordLevel) ? ($recordLevel == $key ? 'selected' : '') : '' }}>{{ $level }}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('classLevelName','<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="col-lg-3 p-t-20"> 
                        <div class="form-group {{ $errors->has('classAlias') ? 'has-error' :'' }}">
                            <label for="classAlias">@lang('messages.alias')
                            </label>
                            <input maxlength="45" class = "form-control" name="classAlias" type = "text" id = "classAlias" placeholder="{{Lang::get('messages.alias')}}" value="{{ old('classAlias') }}" data-validation="required length" data-validation-length="max45"  />
                            {!! $errors->first('classAlias','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="col-lg-3 p-t-20">
                        <div class="form-group {{ $errors->has('assessmentOnly') ? 'has-error' :'' }}">
                            <label for="assessmentOnly">@lang('messages.assessment-only')
                                <span class="required"> * </span>
                            </label>
                            <select name="assessmentOnly" id="assessmentOnly" class="form-control" data-validation="required" data-validation-error-msg="Assessment is required.">
                                <option value="">@lang('messages.select-type')</option>
                                <option value="1">@lang('messages.yes')</option>
                                <option value="0">@lang('messages.no')</option>
                            </select>
                            {!! $errors->first('assessmentOnly','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <!-- @include('partials.active-field') -->

                    <div class="col-lg-3 p-t-20" style="margin-top:30px;">
                        <div class="checkbox checkbox-aqua">
                        <input type="hidden" id="isActiveHidden" name="isActive" value="0">
                            <input class="isActive1" id="isActive1" type="checkbox" >
                            <label for="isActive1">
                            <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-4 p-t-20">
                        <div class="checkbox checkbox-aqua">
                            <input type="hidden" id="isSubjectLevelAttendanceHidden" name="isSubjectLevelAttendance" value="0">
                            <input class="isSubjectLevelAttendance1" id="isSubjectLevelAttendance1" type="checkbox">
                            <label for="isSubjectLevelAttendance1">
                            <p style="margin-top: -3px;">@lang('messages.this-class-has-subject-level-attendance')</p>
                            </label>
                        </div>
                    </div>

                    

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
