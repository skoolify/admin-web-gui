@extends('master')

@section('title', Lang::get('messages.exam-term-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.exam-term-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('exam-term.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.exam-term-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    @endif
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>                                    
                                    <th>@lang('messages.term-name')</th>
                                    <th>@lang('messages.term-note')</th>
                                    <th>@lang('messages.status-active')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($records as $index => $record)
                                <tr class="{{ $index % 2 === 0 ? 'even' : 'odd' }} gradeX" role="row">
                                    <td >{{ $index + 1 }}</td>
                                    <td>{{ $record->subjectName }}</td>
                                    <td>{{ $record->marksTotal }}</td>
                                    <td>{{ $record->marksObtained }}</td>
                                    <td>{{ $record->comments }}</td>
                                    <td>{{ \Carbon\Carbon::parse($record->quizDate)->format('d M, Y') }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ url('/quiz-results/edit/') }}" class="btn-primary btn btn-xs" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-pencil"></i>
                                            </a> 
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
