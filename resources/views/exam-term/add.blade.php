<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="exam-term-form" action="{{ url('exam-term/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('termName') ? 'has-error' :'' }}">
                        <label for="termName">@lang('messages.term-name')
                        </label>
                        <input maxlength="45" class = "form-control" name="termName" type = "text" id = "termName" placeholder="{{Lang::get('messages.term-name')}}" value="{{ old('termName') }}" data-validation="required length" data-validation-length="max45" />
                        {!! $errors->first('termName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('termNote') ? 'has-error' :'' }}">
                        <label for="termNote">@lang('messages.term-note')
                        </label>
                        <input maxlength="45" class = "form-control" name="termNote" type = "text" id = "termNote" placeholder="{{Lang::get('messages.term-note')}}" value="{{ old('termNote') }}" data-validation="required length" data-validation-length="max45" />
                        {!! $errors->first('termNote','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group">
                        <label for="">@lang('messages.from')</label>
                        <input type="date" name="termStartDate" value="" id="filter-date-from" class="form-control" data-validation="required" data-validation-error-msg="Start date is required." />
                        <div id="p2-date-from" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group">
                        <label for="">@lang('messages.to')</label>
                        <input type="date" name="termEndDate" value="" id="filter-date-to" class="form-control" data-validation="required" data-validation-error-msg="To date is required."/>
                        <div id="p2-date-to" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20 ml-3 checkbox-div-alignment">
                    <div class="row">
                        <label for="">@lang('messages.published')</label>
                    </div>
                    <div class="radio p-0 d-flex">
                        <input type="radio" name="isPublished" id="isPublished1" data-status-1="1" value="1"/>
                        <label for="isPublished1" id="radio-right">
                            @lang('messages.yes')
                        </label>
                    </div>
                    <div class="radio p-0 d-flex">
                        <input type="radio" name="isPublished" id="isPublished0" data-status-0="0" value="0"checked />
                        <label for="isPublished0" id="radio-right">
                            @lang('messages.no')
                        </label>
                    </div>
                </div>
                <div class="col-lg-3 p-t-20">
                    <div class="row">
                        <label for="">@lang('messages.status-column')</label>
                    </div>
                    <div class="radio p-0 d-flex">
                        <input type="radio" name="isActive" id="activeField1" data-status-1="1" value="1" checked />
                        <label for="activeField1" id="radio-right">
                            @lang('messages.status-active')
                        </label>
                    </div>
                    <div class="radio p-0 d-flex">
                        <input type="radio" name="isActive" id="activeField0" data-status-0="0" value="0" />
                        <label for="activeField0" id="radio-right">
                            @lang('messages.status-inactive')
                        </label>
                    </div>
                </div>
                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
