@extends('auth.master')

@push('extra-css')
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }

        #loader-logo {
            -webkit-animation: action 1s infinite  alternate;
            animation: action 1s infinite  alternate;
        }
        @-webkit-keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }
        @keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }

        .tick {
            padding-top: 7px;
            font-size: 32px;
            color: #2fa8ff;
        }

        .close {
            padding-top: 7px;
            font-size: 32px;
            color: #ec1000;
        }

        .button-radious{
            border-radius: 50%;
            background: #2fa8ff;
            width: 25px;
            height: 25px;
            margin-left: 15px;
            margin-right: 15px;
        }
        .animate {
            -webkit-animation-duration: 3s;
            animation-duration: 3s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
         }

         @-webkit-keyframes fadeOutUpper {
            0% {
               opacity: 1;
               -webkit-transform: translateY(0);
            }
            100% {
               opacity: 0;
               -webkit-transform: translateY(-20px);
            }
         }

         @keyframes fadeOutUpper {
            0% {
               opacity: 1;
               transform: translateY(0);
            }
            100% {
               opacity: 0;
               transform: translateY(-20px);
            }
         }

         .fadeOutUpper {
            -webkit-animation-name: fadeOutUpper;
            animation-name: fadeOutUpper;
         }

        #loader-text{
            color: #525252;
        }

        .center{
            margin:auto!important;
        }

    </style>
@endpush

@section('title', 'Secure Payment Success')

@section('content')

<div class="container" style="overflow:hidden;">
    <div class="row">
        <div class="col-md-12 text-center pt-2 pb-2">
            <img src="{{ asset('assets/img/logo-single-small.png') }}" class="img-responsive" alt="Skoolify" style="height: 50px;" />
        </div>
        @if($postBackData['decision'] == 'SUCCESSFUL')
            <div class="col-md-12">
                <div class="row text-center" style="background-color: #edf7fe;">
                    <div class="col-md-12">
                        <i class="fa fa-check tick"></i>
                    </div>
                    <div class="col-md-12 pb-3">
                        <p style="font-size: 14px; padding-left:10px">Your Transaction is successful.<br><b>{{isset($postBackData['req_transaction_uuid']) ? $postBackData['req_transaction_uuid'] : '---'}}</b></p>
                    </div>
                </div>
            </div>
        @elseif($postBackData['decision'] == 'ACCEPT' && $postBackData['decision_reason_code'] == '100')
            <div class="col-md-12">
                <div class="row text-center" style="background-color: #edf7fe;">
                    <div class="col-md-12">
                        <i class="fa fa-check tick"></i>
                    </div>
                    <div class="col-md-12 pb-3">
                        <p style="font-size: 14px; padding-left:10px">Your Transaction is successful.<br><b>{{isset($postBackData['req_transaction_uuid']) ? $postBackData['req_transaction_uuid'] : '---'}}</b></p>
                    </div>
                </div>
            </div>
        @elseif($postBackData['decision'] == 'REJECT' && $postBackData['decision_reason_code'] == '481')
            <div class="col-md-12">
                <div class="row text-center" style="background-color: #edf7fe;">
                    <div class="col-md-12">
                        <i class="fa fa-check tick"></i>
                    </div>
                    <div class="col-md-12 pb-3">
                        <p style="font-size: 14px; padding-left:10px">Awaiting Approval.<br><b>{{isset($postBackData['req_transaction_uuid']) ? $postBackData['req_transaction_uuid'] : '---'}}</b></p>
                    </div>
                </div>
            </div>
        @elseif($postBackData['decision'] == 'DECLINE')
            <div class="col-md-12">
                <div class="row text-center" style="background-color: #edf7fe;">
                    <div class="col-md-12 close">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="col-md-12 pb-3">
                        <p style="font-size: 14px; padding-left:10px">Unsuccessful.<br><b>{{isset($postBackData['req_transaction_uuid']) ? $postBackData['req_transaction_uuid'] : '---'}}</b></p>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <div class="row text-center" style="background-color: #edf7fe;">
                    <div class="col-md-12 close">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="col-md-12 pb-3">
                        <p style="font-size: 14px; padding-left:10px">Your Transaction is unsuccessful.<br><b>{{isset($postBackData['req_transaction_uuid']) ? $postBackData['req_transaction_uuid'] : '---'}}</b></p>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-md-12 text-center pt-2" id="payment-modules-loader">
            @if($postBackData['decision'] == 'SUCCESSFUL')
                <h6 style="text-align:justify;">Thank you for your payment, Your transaction has been received successfully and your account will be updated accordingly.</h6>
            @elseif($postBackData['decision'] == 'ACCEPT' && $postBackData['decision_reason_code'] == '100')
                <h6 style="text-align:justify;">Thank you for your payment, Your transaction has been received successfully and your account will be updated accordingly.</h6>
            @elseif($postBackData['decision'] == 'REJECT' && $postBackData['decision_reason_code'] == '481')
                <h6 style="text-align:justify;">Your payment has been accepted but waiting approval. Please contact your bank to complete the authorization process. Please note that your fee will be marked as paid once the approval is received.</h6>
            @elseif($postBackData['decision'] == 'DECLINE')
                <h6 style="text-align:justify;">We are unable to process your transaction because the issuing bank has declined the transaction.</h6>
            @else
                <h6 style="text-align:justify;">We are unable to process your transaction due to unknown reason. Incase you experience this again, please send your screenshot to our facebook page.</h6>
            @endif
        </div>
    </div>

    <div class="row ml-3 mt-2" style="font-size: 12px;">
        <div class="w-50">
            <div class="row">
                <div class="col-md-12">
                    <label style="font-weight: 600;">Amount</label>
                </div>
                <div class="col-md-12">{{isset($postBackData['currency']) ? number_format($postBackData['currency']) : 'PKR'}} {{isset($postBackData['req_amount']) ? number_format($postBackData['req_amount']) : '---'}}</div>
                {{-- <div class="col-md-12">PKR {{isset($postBackData['req_amount']) ? number_format($postBackData['req_amount']) : '---'}}</div> --}}
            </div>
        </div>
        <div class="w-50">
            <div class="row">
                <div class="col-md-12">
                    <label style="font-weight: 600;">Card Number</label>
                </div>
                <div class="col-md-12">{{isset($postBackData['req_card_number']) ? $postBackData['req_card_number'] : '---'}}</div>
            </div>
        </div>
    </div>
    <div class="row mt-3 ml-3" style="font-size: 12px;">
        <div class="w-50">
            <div class="row">
                <div class="col-md-12">
                    <label style="font-weight: 600;">Customer IP</label>
                </div>
                <div class="col-md-12">{{isset($postBackData['customer_ip_address']) ? $postBackData['customer_ip_address'] : '---'}}</div>
                {{-- <div class="col-md-12">{{isset($postBackData['bill_to_forename']) ? $postBackData['bill_to_forename'] : '---'.' '.isset($postBackData['req_bill_to_surname']) ? $postBackData['req_bill_to_surname'] : '---'}}</div> --}}
            </div>
        </div>
        <div class="w-50">
            <div class="row">
                <div class="col-md-12">
                    <label style="font-weight: 600;">Date/Time</label>
                </div>
                <div class="col-md-12">{{\Carbon\Carbon::now()->setTimezone('Asia/Karachi')->toDayDateTimeString()}}</div>
            </div>
        </div>
    </div>
    {{-- params print --}}
    {{-- <div class="row mt-3 ml-3" style="font-size: 12px;">
        @if(isset($postBackData))
            @foreach ($postBackData as $key => $param)
                <div class="col-md-12">
                    <p><b>{{$key}} :</b> {{$param}}</p>
                </div>
            @endforeach
        @endif
    </div> --}}
    <div class="row mt-4">
        <div class="d-flex w-100 text-center" style="justify-content:center;">
            <a class="btn btn-primary" href="{{route('school-payment')}}" ><i class="fa
                fa-arrow-left" style="color: white;"></i> Back</a>
            {{-- <a class="button button-radious" href="https://www.facebook.com/skoolify.apps" ><i class="fa fa-facebook px-3" style="padding-left: 8px !important;color: white;"></i></a>
            <a class="button button-radious" href="https://www.linkedin.com/company/skoolify"><i class="fa fa-linkedin px-3" style="padding-left: 8px !important;color: white;"></i></a>
            <a class="button button-radious" href="https://www.youtube.com/channel/UCQ8lKLQ3--HRITZk7D6Rw7Q"><i class="fa fa-youtube px-3" style="padding-left: 7px !important;color: white;"></i></a>
            <a class="button button-radious" href="https://www.instagram.com/skoolify.app"><i class="fa fa-instagram px-3" style="width: 42px; padding-left:0px !important;color: white;"></i></a> --}}
        </div>
    </div>
</div>

@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/hbl-payment.js') }}"></script>
@endpush
