@extends('auth.master')

@push('extra-css')
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }

        #loader-logo {
            -webkit-animation: action 1s infinite  alternate;
            animation: action 1s infinite  alternate;
        }
        @-webkit-keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }
        @keyframes action {
            0% { transform: translateY(0); }
            100% { transform: translateY(-10px); }
        }


        .animate {
            -webkit-animation-duration: 3s;
            animation-duration: 3s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
         }
         
         @-webkit-keyframes fadeOutUpper {
            0% {
               opacity: 1;
               -webkit-transform: translateY(0);
            }
            100% {
               opacity: 0;
               -webkit-transform: translateY(-20px);
            }
         }
         
         @keyframes fadeOutUpper {
            0% {
               opacity: 1;
               transform: translateY(0);
            }
            100% {
               opacity: 0;
               transform: translateY(-20px);
            }
         }
         
         .fadeOutUpper {
            -webkit-animation-name: fadeOutUpper;
            animation-name: fadeOutUpper;
         }

        #loader-text{
            color: #525252;
        }

        .center{
            margin:auto!important;
        }

    </style>
@endpush

@section('title', 'Secure Payment')

@section('content')

<div class="limiter">
    <div class="container-login100 page-background">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" id="payment-modules-loader">
                    <img src="{{ asset('assets/img/payment-loader.gif') }}" id="loader-logo" class="animate img-responsive" alt="Skoolify" />
                    <h5 id="payment-loader-text" class="animate">Connecting...</h5>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <form id="payment_form" action="{{$providerURL}}" method="POST">
        @foreach ($params as $key => $param)
            @if($key !== 'providerURL')
                <input type="hidden" name="{{ $key }}" value="{{ $param }}" />
            @endif
        @endforeach
        <input type="hidden" name="signature" value="{{ $hash }}" />
    </form>
</div>

@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/hbl-payment.js') }}"></script>
@endpush
