@extends('master')

@section('title', 'Subject Attendance')

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<style>
.custom-validation-error{
    outline: none !important;
    border:1px solid red;
    box-shadow:red !important;
}
.custom-validation-error-msg{
    color: red !important;
}
}
</style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => 'Subject Attendance Management'))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> Filters</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.subjects-filter')
                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('subjectAttendanceDate') ? 'has-error' :'' }}">
                                    <label for="subjectAttendanceDate">Attendance Date
                                    </label>
                                    <input class = "form-control datepicker" name="subjectAttendanceDate" type = "date" max="{{ \Carbon\Carbon::now()->toDateString() }}" value="{{ \Carbon\Carbon::now()->toDateString() }}" id = "subjectAttendanceDate" placeholder="" value="{{ old('subjectAttendanceDate') }}"  />
                                    {!! $errors->first('subjectAttendanceDate','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            @include('partials.components.subjects-in-timetable-filter')
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                <label for="batchNo">Batch No
                                            <span class="required"> * </span>
                                    </label>
                                    <select name="batchNo" id="filter-batch-no" class="form-control filter-select">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                    {!! $errors->first('marksTotal','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('subject-attendance.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="subject-attendance-list">
            <div class="card-box">
                

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>Subject Attendance</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <!-- <div class="btn-group pull-right"> -->
                        <button class="btn btn-primary pull-right btn-round m-w-50 mr-1" id="add-subject-attendance">ADD / EDIT</button>
                    <!-- </div> -->
                    @endif
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle " id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>                                    
                                    <th>Student</th>
                                    <th>Roll No</th>
                                    <th class="text-center">Notes</th>
                                    <th class="text-center">Attendance</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="subject-attendance">
            <div class="card-box">

                <div class="card-head">
                    <header>Add Attendance</header>
                    <!-- <div class="btn-group pull-right"> -->
                        {{-- <button class="btn btn-primary cancel-subject-attendance-form" id="">SUBMIT</button> --}}
                        <button class="btn btn-danger pull-right btn-round" id="cancel-attendance-today">CANCEL</button>
                    <!-- </div> -->
                </div>
                <form method="POST" id="bulk-subject-attendance-form" action="{{ url('attendance/add') }}">
                <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    <div class="card-body">
                        <div class="table-scrollable">
                            <div class="attendance-add-table">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle " id="example45" style="width: 100% !important;">
                                <thead>
                                    <tr>
                                        <th style=" padding-right:0px!important;" class="th-w">
                                        <div class="flex-container" style="justify-content: center">
                                            <div class="round" data-toggle="tooltip" title="Present">
                                                <input type="checkbox" data-index="attendance-check-p" data-target="P" class="mark-attendances check-attendance attendance-check-p" data-json='' id="checkbox-p" />
                                                <label for="checkbox-p"><b class="check-text">P</b></label>
                                            </div>

                                            <div class="round round-d" data-toggle="tooltip" title="Absent">
                                                <input type="checkbox" data-index="attendance-check-a" data-target="A" class="mark-attendances check-attendance attendance-check-a" data-json='' id="checkbox-a" />
                                                <label for="checkbox-a"><b class="check-text">A</b></label>
                                            </div>

                                            <div class="round round-l" data-toggle="tooltip" title="Leave">
                                                <input type="checkbox" data-index="attendance-check-l" data-target="L" class="mark-attendances check-attendance attendance-check-l" data-json='' id="checkbox-l" />
                                                <label for="checkbox-l"><b class="check-text">L</b></label>
                                            </div>

                                            <div class="round round-t" data-toggle="tooltip" title="Late">
                                                <input type="checkbox" data-index="attendance-check-t" data-target="T" class="mark-attendances check-attendance attendance-check-t" data-json='' id="checkbox-t" />
                                                <label for="checkbox-t"><b class="late-text check-text">Late</b></label>
                                            </div>
                                        </div>
                                        </th>
                                        <th width="20%">Student</th>
                                        <th width="20%">Roll No</th>
                                        <th width="10%">Notes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                            <div class="col-lg-12 p-t-20" id="add-attendance-form-errors">
                            </div>
                            <div class="col-lg-12 p-t-20 text-center">
                                <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                                <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default cancel-subject-attendance-form">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="offDayMarkingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Off Day Attendance</h5>
        <button type="button" style="color:red !important" class="close" data-dismiss="modal" aria-label="Close">
            &times;
        </button>
      </div>
      <div class="modal-body">
            <div class = "row">
            <div class="col-lg-12 p-t-20" id="offDayNoteDiv">
                <label for="subjectAttendanceDate">Off Day Note</label>
            <textarea name="offDayNote" class="form-control" id="offDayNote"></textarea>
                <span class="custom-validation-error-msg hidden">Off Day Attendance is required.</span> 
            </div>

            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit-off-day-attendance">Submit</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
