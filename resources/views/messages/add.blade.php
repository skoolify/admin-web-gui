<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="messages-form" action="{{ url('messages/add') }}" enctype="multipart/form-data">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">
                <input type="hidden" id="editHiddenField" value="" />
                <div class="col-lg-12 custom-p-t-20">
                    @if(session()->get('user')->isAdmin)
                        <div class="checkbox checkbox-aqua">
                            <input class="notification-only" id="checkboxbg4" type="checkbox">
                            <label for="checkboxbg4">
                            <p style="margin-top: -3px;">@lang('messages.notification-only')</p>
                            </label>
                        </div>
                    @endif
                </div>
                
                <div style="min-height: 71px !important;" class="col-lg-12 custom-p-t-20">
                    <div style="margin-left: 16px; min-height: 71px !important;" class="form-group">
                        <div id="subject-text" class="form-group hidden">
                            <label for="subjectText">@lang('messages.subject-text')</label><br>
                            <input class="form-control" placeholder="{{Lang::get('messages.subject-text')}}" id="subjectText" type="text">
                        </div>
                    </div>
                    
                    <div id="message-text" style="margin-left: 16px;" class="form-group {{ $errors->has('messageText') ? 'has-error' :'' }}">
                        <label for="messageText">@lang('messages.messages-text')
                        </label>
                        <textarea maxlength="1000" class = "form-control" rows="5" name="messageText" id = "messageText" placeholder="{{Lang::get('messages.messages-text')}}" data-validation="required length" data-validation-length="max1000">{{ old('messageText') }}</textarea>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-2 ">
                                    <p class="text-success"><em><strong><span>@lang('messages.characters')  </span><span id="char-count"> </span></strong></em></p>  
                                </div>
                                <div class="col-md-10">
                                    <p class="text-danger"><em><small>@lang('messages.note-mobile-notification')</small></em></p>
                                </div>
                            </div>
                        </div>
                        {!! $errors->first('messageText','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="row col-md-12">
                        <!-- <div id="attachement-div" style="display:none;">
                            <a  class="delete-attachment" data-toggle="tooltip" title="Delete Attachment">x</a>
                            <img width="350" height="150" id="diary-attachment"  src="http://www.rangerwoodperiyar.com/images/joomlart/demo/default.jpg"   id="merchant-logo-tag" alt="Logo" class="img-thumbnail img-fluid pull-right justify-content-end">
                        </div> -->
                        <div style="min-width: 395px;min-height: 114px;" class="col-md-4">
                            <div id="attachmentDiv" style="margin-left:16px;min-width:342px;" >
                                <label class="control-label">@lang('messages.attachment')</label>
                                    <input type="file" class="form-control" style="height:auto !important" id="attachmentUpload" name="attachmentUpload" accept=".jpeg,.jpg,.gif,.png,.pdf,.doc,.docx,.xlsx,.xls" />
                                    <input type="hidden" id="hiddenAttachment" name="attachment" value="" />
                                    <p><small><em>@lang('messages.file-format')</em></small></p>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-right: 32px;"></div>
                        <div class="col-md-3" style="margin-top: 32px;">
                            <div style="margin-left: 28px;">
                                <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                                <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
