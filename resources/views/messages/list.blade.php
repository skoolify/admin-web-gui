@extends('master')

@section('title', Lang::get('messages.messages-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<style>
   .card-box:hover {
 /*    box-shadow: 0 1px 5px 1px rgba(0,0,0,0.14), 0 3px 14px 2px rgba(0,0,0,0.12), 0 5px 5px -3px rgba(0,0,0,0.2);
    transition: all 150ms linear; */
    -webkit-transform: none !important;
    transform: none !important;
    box-shadow: 0 20px 20px rgba(0,0,0,.1);
    -moz-box-shadow: 0 20px 20px rgba(0,0,0,.1);
    -webkit-box-shadow: 0 20px 20px rgba(0,0,0,.1);
}
</style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.messages-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @if(session()->has('user') && session()->get('user')->isAdmin === 1)
                                @include('partials.components.countries-filter', array('all'=> Lang::get('messages.all')))
                                @include('partials.components.cities-filter', array('all'=> Lang::get('messages.all')))
                                @include('partials.components.areas-filter', array('all'=>Lang::get('messages.all')))
                            @endif
                            @include('partials.components.schools-filter', array('all'=>Lang::get('messages.all')))
                            @include('partials.components.branches-filter', array('all'=>Lang::get('messages.all')))
                            @include('partials.components.class-levels-filter-multiple', array('all'=>Lang::get('messages.all')))
                            @include('partials.components.classes-filter-multiple', array('all'=>Lang::get('messages.all')))
                            @include('partials.components.students-filter-multiple', array('all'=>Lang::get('messages.all')))
                            @include('partials.components.message-type-filter')
                            @include('partials.components.date-from-filter')
                            @include('partials.components.date-to-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('messages.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.messages-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    @endif

                    {{-- <button type="button" class="view-messages-list btn btn-round btn-danger pull-right" data-student-id="28142" data-guiid="42452b33-a468-405d-9987-0c504a1c1f10" data-check-isassessment="0" data-examterm-id="1"><i class="fa fa-plus"></i> view</button> --}}
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.messages-text')</th>
                                    <th>@lang('messages.messages-date')</th>
                                    <th>@lang('messages.messages-type')</th>
                                    <th>@lang('messages.recipient')</th>
                                    <th>@lang('messages.list')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="messageDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      {{-- <div class="modal-header"> --}}
          <div class="row">
                <div class="col-md-4">
                    <h4 class="modal-title message-list-title pl-3" id="exampleModalLabel" style="color:green;font-weight:bold;font-size:20px;" >@lang('messages.message-detail-list')</h4>
                </div>
                <div class="col-md-8 pr-4">
                    <button class="btn btn-sm btn-primary mt-2 pull-right pr-2 pl-2" id="exportPDFBtn" style="margin-left: 10px;"><i class="fa fa-download"></i> @lang('messages.export-excel')</button> <i class="exportLoader hidden fa fa-spinner fa-pulse pull-right mt-3" style="font-size:24px;color: #188ae2;"></i>
                </div>
                {{-- <div class="col-md-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color:red">&times;</span>
                    </button>
                </div> --}}
          </div>


        <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      {{-- </div> --}}
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12" style = "max-height:400px;overflow:auto;">
                <table class="table table-striped table-bordered table-responsive-lg table-responsive-xs table-hover table-checkable" id="message-detail-table">
                    <thead >
                        <tr>
                            <th>#</th>
                            <th>@lang('messages.student')</th>
                            <th>@lang('messages.class')</th>
                            <th>@lang('messages.consent-response')</th>
                        </tr>
                    </thead>
                    <tbody>
                         <td></td>
                        <td></td>
                        <td></td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
      </div>
    </div>
  </div>
</div>

<table id="consent-report-table-heading" class="table fixed_header w-100 hidden">
    <thead>
        <tr>
            <th style="text-align: center;font-size:14;">@lang('messages.message-consent-report')</th>
        </tr>
    </thead>
</table>

<table class="table fixed_header w-100 hidden" id="consent-report-header">
    <thead>
        <tr>
            <th>@lang('messages.s-number')</th>
            <th>@lang('messages.student')</th>
            <th>@lang('messages.class')</th>
            <th>@lang('messages.responded-by')</th>
            <th>@lang('messages.consent-response')</th>
            <th>@lang('messages.consent-text')</th>
            <th>@lang('messages.date')</th>
        </tr>
    </thead>
    <tbody id="consent-report-table-body">

    </tbody>
</table>

<table id="responseDataTable">
    <tbody id="responseData">
    </tbody>
</table>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="{{asset('assets/js/jspdf.min.js')}}"></script>
<script src="{{asset('assets/js/auto-table.min.js')}}"></script>
<script src="{{asset('assets/js/jspdf.plugin.autotable.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
@endpush
