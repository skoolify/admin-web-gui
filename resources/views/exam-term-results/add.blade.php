<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="exam-term-results-form" action="{{ url('exam-term-results/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-12">
                    <h3>Exam Term Result</h3>
                    <hr>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('overallNotes') ? 'has-error' :'' }}">
                        <label for="overallNotes">Overall Notes
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="overallNotes" type = "text" id = "overallNotes" placeholder="Overall Notes" value="{{ old('overallNotes') }}"  />
                        {!! $errors->first('overallNotes','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('overallPercentage') ? 'has-error' :'' }}">
                        <label for="overallPercentage">Overall Percentage
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="overallPercentage" type = "text" id = "overallPercentage" placeholder="Overall Percentage" value="{{ old('overallPercentage') }}"  />
                        {!! $errors->first('overallPercentage','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('rank') ? 'has-error' :'' }}">
                        <label for="rank">Rank
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="rank" type = "text" id = "rank" placeholder="Rank" value="{{ old('rank') }}"  />
                        {!! $errors->first('rank','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <label for="">Published</label>
                    <div class="radio p-0">
                        <input type="radio" name="isPublished" id="isPublished1" value="1" {{ old('isPublished') == 1 ? 'checked' : '' }}>
                        <label for="isPublished1">
                            Yes
                        </label>
                    </div>
                    <div class="radio p-0">
                        <input type="radio" name="isPublished" id="isPublished2" value="0" {{ old('isPublished') == 0 ? 'checked' : '' }}>
                        <label for="isPublished2">
                            No
                        </label>
                    </div>
                </div>

                <div class="col-lg-12">
                    <h3>Subject Result</h3>
                    <hr>
                </div>

                <div class="col-lg-2 p-t-20">
                    <div class="form-group {{ $errors->has('subjectID') ? 'has-error' :'' }}">
                        <label for="subjectID">Subject
                                <span class="required"> * </span>
                        </label>
                        <select name="subjectId" class="form-control" id="subjects-list">                            
                        </select>
                    </div>
                </div>

                <div class="col-lg-2 p-t-20">
                    <div class="form-group {{ $errors->has('totalMarks') ? 'has-error' :'' }}">
                        <label for="totalMarks">Total Marks
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="totalMarks"  type = "text" id = "totalMarks" placeholder="Total Marks" value="{{ old('totalMarks') }}"  />
                        {!! $errors->first('totalMarks','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-2 p-t-20">
                    <div class="form-group {{ $errors->has('obtainedMarks') ? 'has-error' :'' }}">
                        <label for="obtainedMarks">Obtained Marks
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="obtainedMarks" type = "text" id = "obtainedMarks" placeholder="Obtained Marks" value="{{ old('obtainedMarks') }}"  />
                        {!! $errors->first('obtainedMarks','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-2 p-t-20">
                    <div class="form-group {{ $errors->has('grade') ? 'has-error' :'' }}">
                        <label for="grade">Grade
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="grade" type = "text" id = "grade" placeholder="Grade" value="{{ old('grade') }}"  />
                        {!! $errors->first('grade','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('notes') ? 'has-error' :'' }}">
                        <label for="notes">Notes
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="notes" type = "text" id = "notes" placeholder="Notes" value="{{ old('notes') }}"  />
                        {!! $errors->first('notes','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-1 p-t-20">
                    <div class="form-group">
                        <br class="hidden-xs hidden-sm" />
                        <button type="button" id="add-subject" class="btn btn-facebook waves-effect waves-light">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>

                
                <div class="col-lg-12 p-t-10">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Subject Name</th>
                                <th>Total Marks</th>
                                <th>Obtained Marks</th>
                                <th>Grade</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody id="subjects-list-table">
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12">
                    <h3>Non-Subject Result</h3>
                    <hr>
                </div>

                <div class="col-lg-2 p-t-20">
                    <div class="form-group {{ $errors->has('nonSubjectID') ? 'has-error' :'' }}">
                        <label for="nonSubjectID">Non-Subject
                                <span class="required"> * </span>
                        </label>
                        <select name="nonSubjectID" class="form-control" id="non-subjects-list">                            
                        </select>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('gradeNS') ? 'has-error' :'' }}">
                        <label for="gradeNS">Grade NS
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="gradeNS" type = "text" id = "gradeNS" placeholder="Grade" value="{{ old('gradeNS') }}"  />
                        {!! $errors->first('gradeNS','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('notesNS') ? 'has-error' :'' }}">
                        <label for="notesNS">Notes NS
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="notesNS" type = "text" id = "notesNS" placeholder="Notes" value="{{ old('notesNS') }}"  />
                        {!! $errors->first('notesNS','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-1 p-t-20">
                    <div class="form-group">
                        <br class="hidden-xs hidden-sm" />
                        <button type="button" id="add-non-subject" class="btn btn-facebook waves-effect waves-light">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                </div>

                <div class="col-lg-12 p-t-10">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Non-Subject Name</th>
                                <th>Grade</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody id="non-subjects-list-table">
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
