
<div class="table-scrollable" id="subject-results-list">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="subject-table">
        <thead>
            <tr>
                <th>#</th>      
                <th>Student Name</th>                              
                <th>Subject Name</th>
                <th>Total Marks</th>
                <th>Obtained Marks</th>
                <th>Grade</th>
                <th>Notes</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="subject-result-list-table">
        </tbody>
    </table>
</div>