<span class="view-list-error" style="color:red; font-style:italic"></span>
<div class="table-scrollable" id="overall-result-list">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="overall-result-table">
        <thead>
            <tr>
                <th>#</th>   
                <th>Student Name</th>                                 
                <th>Overall Notes</th>
                <th>Overall Percentage</th>
                <th>Rank</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="overall-result-list-table">
        </tbody>
    </table>
</div>
<div class="student-subject-and-nonsubject-div borderBox light bordered card-box" style="display:none;border:#188ae2 !important;"> 
<div class="md-offset-6">
    <button class="btn btn-danger btn-md pull-right student-result-list"> Close</button>
</div>
<h3 id="student-subject-result">Subject Results</h3>
<div class="table-scrollable" id="subject-results-list-for-student">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="student-subject-table">
        <thead>
            <tr>
                <th>#</th>                                    
                <th>Subject Name</th>
                <th>Total Marks</th>
                <th>Obtained Marks</th>
                <th>Grade</th>
                <th>Notes</th>
            </tr>
        </thead>
        <tbody id="subject-result-list-table">
        </tbody>
    </table>
</div>
    <h3 style= "margin-top:10px;" id="student-non-subject-result">Non-Subject Results</h3>
    <div class="table-scrollable" id="subject-non-results-list-for-student">
        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="student-non-subject-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Non-Subject Name</th>
                    <th>Notes</th>
                    <th>Grade</th>
                </tr>
            </thead>
            <tbody id="non-subject-result-list-table">
            </tbody>
        </table>
    </div>
</div>

