
<div class="table-scrollable" id="non-subject-results-list">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="non-subject-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Student Name</th>                                    
                <th>Non-Subject Name</th>
                <th>Notes</th>
                <th>Grade</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="non-subject-result-list-table">
        </tbody>
    </table>
</div>