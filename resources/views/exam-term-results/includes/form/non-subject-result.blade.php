<form action="#" class="non-subject-edit-form" id="non-subject-result-edit-form" style="">
<div class="row" id="non-subject-result-edit-div" style="display:none;">

    <input type="hidden" id="editHiddenFieldNonSubject" class="hidden-field" />
    <input type="hidden" id="non-subject-result-hidden-field" />
    <div class="col-lg-3 p-t-20">
        <div class="form-group {{ $errors->has('obtainedMarks') ? 'has-error' :'' }}">
            <label for="obtainedMarks">Student
            </label>
            <input class = "form-control" name="studentName"  type = "text" id = "studentName" placeholder="Student Name " value="{{ old('studentName') }}"  readonly/>
            <!-- name="studentName" -->
            {!! $errors->first('studentName','<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-lg-3 p-t-20">
        <div class="form-group {{ $errors->has('obtainedMarks') ? 'has-error' :'' }}">
            <label for="obtainedMarks">Grade Ns
                    <span class="required"> * </span>
            </label>
            
            <input class = "form-control" name="gradeNS"  type = "text" id = "gradeNS" placeholder="Grade Ns" value="{{ old('gradeNS') }}"  />
            <!-- name="gradeNS" -->
            {!! $errors->first('gradeNS','<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-3 p-t-20">
        <div class="form-group {{ $errors->has('grade') ? 'has-error' :'' }}">
            <label for="grade">Note Ns
                    <span class="required"> * </span>
            </label>
            <input class = "form-control"  type = "text" name="notesNS" id = "notesNS" placeholder="notes NS " value="{{ old('notesNS') }}"  />
            <!-- name="notesNS" -->
            {!! $errors->first('notesNS','<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-12 p-t-20 error-div" id="add-form-errors">
    </div>
        <div class="col-lg-12 p-t-20 text-right">
                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default reset-form" data-form="non-subject-result-edit-form">Reset</button>
                <button type="button" id="hide-non-subject-edit-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
        </div>
    </div>
</form>
<form action="#" id="non-subject-result-form" style="display:none;">
            <div class="row">

                <input type="hidden" id="editHiddenFieldNonSubject" class="hidden-field" />
                <input type="hidden" id="non-subject-result-hidden-field" />
                <div class="container-fluid " id="subject-result-list-container">
                <div class="col-lg-12">
                    <table class="table table-striped  table-hover table-checkable order-column valign-middle" style="width: 80% !important" id="student-non-subject-result-table">
                        <thead>
                            <tr>
                                <th>Student Name</th>
                                <th>Grade Ns</th>
                                <th>Notes Ns</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td colspan="3"><strong>Select Class First</strong></td></tr>
                        </tbody>
                    </table>
                    <div id="p2-non-subject-students-result-list" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 80%"></div>
                </div>
            </div>
            <div class="col-lg-12 p-t-20 text-right">
                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default reset-form" data-form="non-subject-result-form">Reset</button>
                <button type="button" id="hide-non-subject-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
            </div>
        </div>
</form>
    