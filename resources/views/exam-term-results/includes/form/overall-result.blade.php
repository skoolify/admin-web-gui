<form action="#" id="overall-result-form" style="display:none;">
    <div class="row">

        <input type="hidden" id="editHiddenFieldOverall" class="hidden-field" />


        <div class="col-lg-3 p-t-20">
            <div class="form-group {{ $errors->has('overallNotes') ? 'has-error' :'' }}">
                <label for="overallNotes">Overall Notes
                        <span class="required"> * </span>
                </label>
                <input class = "form-control" name="overallNotes" type = "text" id = "overallNotes" placeholder="Overall Notes" value="{{ old('overallNotes') }}"  />
                {!! $errors->first('overallNotes','<span class="help-block">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-3 p-t-20">
            <div class="form-group {{ $errors->has('overallPercentage') ? 'has-error' :'' }}">
                <label for="overallPercentage">Overall Percentage
                        <span class="required"> * </span>
                </label>
                <input class = "form-control" name="overallPercentage" type = "text" id = "overallPercentage" placeholder="Overall Percentage" value="{{ old('overallPercentage') }}"  />
                {!! $errors->first('overallPercentage','<span class="help-block">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-3 p-t-20">
            <div class="form-group {{ $errors->has('rank') ? 'has-error' :'' }}">
                <label for="rank">Rank
                        <span class="required"> * </span>
                </label>
                <input class = "form-control" name="rank" type = "text" id = "rank" placeholder="Rank" value="{{ old('rank') }}"  />
                {!! $errors->first('rank','<span class="help-block">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-12 p-t-20 text-right">
            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
            <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default reset-form" data-form="overall-result-form">Reset</button>
            <button type="button" id="hide-overall-subject-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
        </div>


    </div>
</form>
