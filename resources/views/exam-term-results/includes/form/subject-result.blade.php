<form action="#" id="subject-result-edit-form" style="">
<div class="row" id="subject-result-add-fields" style="display:none;">

    <input type="hidden" id="editHiddenFieldSubject" class="hidden-field" />
    <input type="hidden" id="subject-result-hidden-field" />
    <div class="col-lg-3 p-t-20">
        <div class="form-group {{ $errors->has('obtainedMarks') ? 'has-error' :'' }}">
            <label for="obtainedMarks">Student
            </label>
            <input class = "form-control" name="studentName"  type = "text" id = "studentName" placeholder="Student Name " value="{{ old('studentName') }}"  readonly/>
            <!-- name="studentName" -->
            {!! $errors->first('studentName','<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-lg-3 p-t-20">
        <div class="form-group {{ $errors->has('obtainedMarks') ? 'has-error' :'' }}">
            <label for="obtainedMarks">Obtained Marks
                    <span class="required"> * </span>s
            </label>
            
            <input class = "form-control" name="obtainedMarks"  type = "text" id = "obtainedMarks" placeholder="Obtained Marks" value="{{ old('obtainedMarks') }}"  />
            <!-- name="obtainedMarks" -->
            {!! $errors->first('obtainedMarks','<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-3 p-t-20">
        <div class="form-group {{ $errors->has('grade') ? 'has-error' :'' }}">
            <label for="grade">Grade
                    <span class="required"> * </span>
            </label>
            <input class = "form-control"  type = "text" name="grade" id = "grade" placeholder="Grade" value="{{ old('grade') }}"  />
            <!-- name="grade" -->
            {!! $errors->first('grade','<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-3 p-t-20">
        <div class="form-group {{ $errors->has('notes') ? 'has-error' :'' }}">
            <label for="notes">Notes
                    <span class="required"> * </span>
            </label>
            <input class = "form-control" type = "text" name="notes" id = "notes" placeholder="Notes" value="{{ old('notes') }}"  />
            <!-- name="notes"  -->
            {!! $errors->first('notes','<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-lg-12 p-t-20" id="add-form-errors">
    </div>
    <div class="col-lg-12 p-t-20 text-right">
            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
            <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default reset-form" data-form="subject-result-edit-form">Reset</button>
            <button type="button" id="hide-subject-edit-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
    </div>

</div>
</form>



<!-- Bulk Student data div  -->
<form action="#" id="subject-result-form" style="display:none;">
<div class="row" id="subject-result-list-container">


    <div class="container-fluid " >
        <div class="col-lg-12">
        <table class="table table-striped  table-hover table-checkable order-column valign-middle" style="width: 80% !important" id="student-result-table">
                 <thead>
                     <tr>
                         <th>Student Name</th>
                         <th>Marks Obtain</th>
                         <th>Grade</th>
                         <th>Note</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                        <td colspan="4"><strong>Select Class First</<strong></td>
                    </tr>

                 </tbody>
             </table>
             <div id="p2-students-result-list" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 80%"></div>
        </div>
    </div>
    <div class="col-lg-12 p-t-20 text-right">
        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default reset-form" data-form="subject-result-form">Reset</button>
        <button type="button" id="hide-subject-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
    </div>

</div>
</form>

