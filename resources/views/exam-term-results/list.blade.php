@extends('master')

@section('title', 'Exam Term Results')

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => 'Exam Term Results Management'))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> Filters</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.students-filter')
                            @include('partials.components.exam-term-filter')
                            @include('partials.components.subjects-filter')
                            @include('partials.components.non-subjects-filter')
                            <div class="col-md-3 col-sm-12 col-xs-12" style="display:none" id="total-marks-div">  
                                <div class="form-group">
                                    <label for="totalMarks">Total Marks
                                        <span class="required"> * </span>
                                    </label>
                                    <input class = "form-control" name="totalMarks"  type = "text" id = "totalMarks" placeholder="Total Marks" value="{{ old('totalMarks') }}"  />
                                    {!! $errors->first('totalMarks','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
       


        <div class="col-sm-12 col-md-12 col-xl-12">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="borderBox light bordered card-box">
                <div class="borderBox-title tabbable-line">
                    <div class="caption">
                        <span class="caption-subject font-dark bold uppercase result-upper-heading">Overall Result List</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a href="#overall-result-tab" data-toggle="tab" class="active show" id="overall-result-link"> Overall Result </a>
                        </li>
                        <li class="nav-item" id="subject-result-tab1">
                            <a href="#subject-result-tab" data-toggle="tab" id="subject-result-link"> Subject Result </a>
                        </li>
                        <li class="nav-item" id="non-subject-result-tab1">
                            <a href="#non-subject-result-tab" data-toggle="tab" id="non-subject-result-link"> Non-Subject Result </a>
                        </li>
                    </ul>
                </div>
                <div class="borderBox-body">
                    <div class="tab-content">
                    <div class="tab-pane active show" id="overall-result-tab">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <button class="btn btn-primary btn-sm pull-right mr-0" id="show-overall-result-add-form-container"><i class="fa fa-plus" ></i> Add Result</button>
                                </div>
                            </div>
                            @include('exam-term-results.includes.form.overall-result')
                            <hr>
                            @include('exam-term-results.includes.list.overall-result')
                          
                        </div>
                        <div class="tab-pane" id="subject-result-tab">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <button class="btn btn-primary btn-sm pull-right mr-0" id="show-subject-result-add-form-container"><i class="fa fa-plus" ></i> Add Result</button>
                                </div>
                            </div>
                            
                            @include('exam-term-results.includes.form.subject-result')
                            @include('exam-term-results.includes.list.subject-result')
                        </div>
                        <div class="tab-pane" id="non-subject-result-tab">
                        <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <button class="btn btn-primary btn-sm pull-right mr-0" id="show-non-subject-result-add-form-container"><i class="fa fa-plus" ></i> Add Result</button>
                                </div>
                        </div>
                            @include('exam-term-results.includes.form.non-subject-result')
                             @include('exam-term-results.includes.list.non-subject-result')
                            
                        </div>
                       
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
