@extends('master')

@section('title', Lang::get('messages.academic-calendar'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.academic-calendar')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter-multiple', array('all'=>'All'))
                            @include('partials.components.classes-filter-multiple', array('all'=>'All'))
                            @include('partials.components.date-from-filter')
                            @include('partials.components.date-to-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('academic-calendar.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <header>@lang('messages.academic-event-list-heading')</header>
                            </div>
                            @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                                <div class="col-md-2 pl-0">
                                    {{-- <div class="custom-control custom-checkbox custom-control-inline mr-0">
                                        <input type="checkbox" class="custom-control-input" id="academicEvent">
                                        <label style="margin-top: 6px;" class="custom-control-label" for="academicEvent"></label>
                                    </div>
                                    <label class="mb-0">Academic Event</label> --}}
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container" academic-event="0"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.academic-event-name')</th>
                                    <th>@lang('messages.academic-event-date')</th>
                                    <th>@lang('messages.class')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($records as $index => $record)
                                <tr class="{{ $index % 2 === 0 ? 'even' : 'odd' }} gradeX" role="row">
                                    <td >{{ $index + 1 }}</td>
                                    <td>{{ $record->subjectName }}</td>
                                    <td>{{ $record->marksTotal }}</td>
                                    <td>{{ $record->marksObtained }}</td>
                                    <td>{{ $record->comments }}</td>
                                    <td>{{ \Carbon\Carbon::parse($record->quizDate)->format('d M, Y') }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ url('/quiz-results/edit/') }}" class="btn-primary btn btn-xs" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
