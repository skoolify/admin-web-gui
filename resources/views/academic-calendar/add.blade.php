<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="academic-calendar-form" action="{{ url('academic-calendar/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />
                <div class="col-lg-3 p-t-20 "> 
                    <div class="form-group {{ $errors->has('eventName') ? 'has-error' :'' }}">
                        <label for="eventName">@lang('messages.academic-event-name')
                        </label>
                        <input maxlength="45" class = "form-control" name="eventName" type = "text" id = "eventName" placeholder="{{Lang::get('messages.academic-event-name')}}" value="{{ old('eventName') }}"  data-validation="required length" data-validation-length="max45">
                        {!! $errors->first('eventName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-3 p-t-20 calender-add">  
                    <div class="form-group">
                        <label for="">@lang('messages.academic-event-date')</label>
                        <input type="date" name="eventDate" value="{{ \Carbon\Carbon::now()->toDateString() }}" id="event-date" class="form-control" />
                        <div id="p2-date-from" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
