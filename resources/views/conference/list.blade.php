@extends('master')

@section('title', Lang::get('messages.meeting-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
<link href="{{ asset('assets/plugins/select2/css/select2.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.vl {
  border-left: 2px solid black;
  height: 70px;
}
.switchToggle .slider.round {
    border-radius: 26px!important;
    }
.switchToggle .slider {
    background-color: #dc3545;
}
.switchToggle .slider:before {
    left: 8px!important;
}
.round {
    width: 55px!important;
}
.hidden{
    display: none;
}
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: #188ae2 !important;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  bottom: 30px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
.offscreen {
    position: absolute;
    left: -999em;
}
</style>
@endsection
@section('content')
<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.meeting-management')))
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            <!-- @include('partials.components.subjects-filter') -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('conference.add',array('zones'=> $timeZones))

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
            <div id="snackbar">@lang('messages.url-copied')</div>

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif
                    <div class="card-head">
                        <header>@lang('messages.meeting-list')</header>
                        @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                        <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.create-meeting') </button>
                        @endif
                    </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <div class="table-scrollable" >
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="example44">
                                <thead>
                                    <tr>
                                        <th>#</th>      
                                        <th>@lang('messages.meeting-host')</th>   
                                        <th>@lang('messages.class')</th>
                                        <th>@lang('messages.subject')</th> 
                                        <th>@lang('messages.time')</th>                       
                                        <th>@lang('messages.meeting-details')</th>
                                        <th>@lang('messages.action-column')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="conferenceDetailModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="conferenceDetailModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
        <div style="text-align:center">  
            <h5 class="" id="billing-period-modal" style="color:#337ab7;font-weight:bold;position: absolute; right: 0;left: 0;padding-top: 2px;font-size: 18px !important;" > @lang('messages.meeting-details')</h5>
            </div>
            <a class="closeModalDiv" data-dismiss="modal" aria-label="Close" style="z-index:1000">
            <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
            </a>
        </div>
        <div class="container-fluid mt-3">
            <div class="row" style="border-bottom: 1px solid #EFEFEF;">
                <div class="col-md-12">
                    <table class="table" id="fee-detail">
                        <tbody>
                            <tr>
                                <td class="p-1 w-25">@lang('messages.topic') </td>
                                <td class="p-1  w-25 br-1" id="meetingTopic"> </td>
                                <td class="p-1  w-25">@lang('messages.meeting-host') </td>
                                <td class="p-1  w-25 " id="hostName"> </td>
                            </tr>
                            <tr>
                                <td class="p-1">@lang('messages.meeting-link')</td>
                                <td class="p-1" id="meetingLink" colspan="3"></td>
                            </tr>
                            {{-- <tr>
                                <td class="p-1  w-25">Meeting ID: </td>
                                <td class="p-1  w-25 br-1" id="meetingID2"> </td>
                                <td class="p-1  w-25">Meeting Password: </td>
                                <td class="p-1  w-25" id="meetingPassword"> </td>
                            </tr> --}}
                            <tr>
                                <td class="p-1 w-25">@lang('messages.start-date') </td>
                                <td class="p-1  w-25 br-1" id="startDateDetail"> </td>
                                <td class="p-1 w-25">@lang('messages.end-date') </td>
                                <td class="p-1  w-25" id="endDateDetail"> </td>
                            </tr>
                            <tr>
                                <td class="p-1  w-25">@lang('messages.reminder') </td>
                                <td class="p-1  w-25 br-1" id="reminderOn2"> </td>
                                <td class="p-1 w-25">@lang('messages.meeting-recurring') </td>
                                <td class="p-1  w-25 " id="isRecurring2"> </td>
                            </tr>
                            {{-- <tr>
                                <td class="p-1">Meeting Link:</td>
                                <td class="p-1" id="meetingLink" colspan="3"></td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <h4 class="modal-title" style="color:#337ab7" > @lang('messages.agenda-description')</h4>
                </div>
                <div class="col-md-2" style="padding-top: 10px;">
                    <a style="float: right;" id="invoicechallanNote" style="text-align: center;"><button id="viewChallanNoteButton" class="btn btn-sm btn-primary circle"><i class="fa fa-arrow-down"></i> </button></a>
                </div>
            </div>
            <div id="noteTableRow" style="border-bottom: 1px solid #EFEFEF;display:none;" class="row">
                <div id="agendaDesc" class="col-md-12">
                </div>
            </div>

            <div class="row recurrence-list mt-5"  style="display:none">
            <div class="col-md-12"  style="height:203px; overflow-y:scroll; margin-bottom:20px;">
                    <h4 class="modal-title" id="exampleModalLabel" style="color:#337ab7;font-weight:bold" > @lang('messages.recurrence')</h4>
                <table class="table " id="recurrence-detail-table">
                    <thead>
                        <tr>
                            <th class="p-1">@lang('messages.day')</th>
                            <th class="p-1">@lang('messages.start-date')</th>
                            <th class="p-1">@lang('messages.start-time')</th>
                            <th class="p-1">@lang('messages.end-time')</th>
                            <th class="p-1">@lang('messages.action-column')</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  class="p-2 text-center center" colspan="3">@lang('messages.record-not-found')</td>
                        </tr>
                    </tbody>
                </table>
                <span id="success-span" style="color:green"></span>
                <span id="error-span" style="color:red" ></span>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger cancelBtnModal" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="conferenceInvitesModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="conferenceDetailModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div style="text-align:center">  
                <h5 class="" id="billing-period-modal" style="color:#337ab7;font-weight:bold;position: absolute; right: 0;left: 0;padding-top: 2px;font-size: 18px !important;" >@lang('messages.invites-list')</h5>
            </div>
                <a class="closeModalDiv" data-dismiss="modal" aria-label="Close" style="z-index:1000">
                    <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
                </a>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" id="invite-list-table">
                        <thead>
                            <tr>
                                <th>@lang('messages.s-number')</th>
                                <th>@lang('messages.name') </th>
                                <th>@lang('messages.roll-number') </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
        </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"  charset="UTF-8"></script>
<script src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js')}}"  charset="UTF-8"></script>
<script src="{{asset('assets/plugins/select2/js/select2.js')}}" ></script>
<script src="{{asset('assets/js/pages/select2/select2-init.js')}}" ></script>
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
