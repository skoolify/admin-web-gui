<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        <div class="card-body">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <form method="POST" id="conference-form" action="" role="form">
                @if(session()->get('user')->isAdmin)
                  <div class="form-group row">
                      <label class="col-md-2 col-xs-12" for="">@lang('messages.teacher')</label>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                              <select name="userID" id="filter-class-teachers" class="form-control filter-select">
                                  <option value="">@lang('messages.select-teacher')</option>
                              </select>
                              <div id="p2-class-teachers" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                        </div>
                  </div>
                @endif
                <div class="form-group row">
                    <label class="col-md-2" for="">@lang('messages.students')</label>
                    <div class="col-md-8 col-sm-12 col-xs-12" id="student-filter">
                        <select name="studentID" id="filter-students" data-live-search="true"  class="form-control filter-select" multiple>
                            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.all') }} @lang('messages.student')</option>
                            @foreach($students as $student)
                                <option {{ session()->get('tempStudentID') == $student->studentID ? 'selected' : '' }} value="{{ $student->studentID }}">{{ $student->studentName }}</option>
                            @endforeach
                        </select>
                        <div id="p2-students" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                <div class="form-group row" id="subject">
                    <label class="col-md-2" for="">@lang('messages.subject')</label>
                    <div class="col-md-8 col-sm-12 col-xs-12" >
                        <select name="subjectID" id="filter-subjects" class="form-control filter-select">
                            <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.subject')</option>
                            @foreach($subjects as $subject)
                                <option {{ session()->get('tempSubjectID') == $subject->subjectID ? 'selected' : '' }} value="{{ $subject->subjectID }}">{{ $subject->subjectName }}</option>
                            @endforeach
                        </select>
                        <div id="p2-subjects" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.topic')</label>
                    <div class="input-group col-md-8">
                        <input class="form-control meetingTopic" maxlength="100" name="meetingTopic" id="meetingTopic" type="text" value="" autocomplete="off" data-validation="required" placeholder="{{Lang::get('messages.topic')}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.meeting-agenda')</label>
                    <div class="input-group col-md-8">
                        <textarea class="form-control agendaDesc"  maxlength="500" name="agendaDesc" value="" placeholder="{{Lang::get('messages.meeting-agenda')}}"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.meeting-link')</label>
                    <div class="input-group col-md-8 customWebLinkDiv">
                        <input class="form-control webLink" maxlength="100" name="webLink" id="webLink" type="url" value="" autocomplete="off" data-validation="required" placeholder="{{Lang::get('messages.meeting-link')}}">
                    </div>
                </div>



                <div class="form-group row " style="display: none;">
                    <div class="col-md-1 pt-2">
                    </div>
                    <div class="col-md-1 pt-2">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.start-time')</label>
                    <div class="input-group date col-md-4" >
                        <input class="form-control" type="date" name="startDate" id="startDate" value="" autocomplete="off" min="">
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="startHour" id="startHourInput">
                                <select class="form-control select2" id="startHour" data-placeholder="Select Hour">
                                    @foreach(config()->get('constants.hours') as $hour)
                                        <option value="{{$hour}}">{{$hour}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" name="startMinute" id="startMinuteInput">
                                <select class="form-control select2" id="startMinute" data-placeholder="Select Minutes">
                                    @foreach(config()->get('constants.minutes') as $startMinute)
                                        <option value="{{$startMinute}}">{{$startMinute}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.end-time')</label>
                    <div class="input-group date col-md-4" >
                    <input type="hidden" name="meetingRefNo" id="meetingRefNo">
                    {{-- <input type="hidden" name="webLink" id="webLink"> --}}
                    <input type="hidden" name="meetingID" id="meetingID">
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="endHour" id="endHourInput">
                                <select class="form-control select2" id="endHour" data-placeholder="Select Hour">
                                    @foreach(config()->get('constants.hours') as $hour)
                                        <option  value="{{$hour}}">{{$hour}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" name="endMinute" id="endMinuteInput">
                                <select class="form-control select2" id="endMinute" data-placeholder="Select Minutes">
                                    @foreach(config()->get('constants.minutes') as $minute)
                                        <option value="{{$minute}}">{{$minute}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 control-label">@lang('messages.timezone')</label>
                    <div class="input-group col-md-8">
                        <select name="timezone" id="filter-timezone" data-placeholder="Select Timezone" class="form-control select2" style="width: 100%" data-validation="required">
                        @foreach($zones as $key => $zone)
                            <option value="{{$zone}}" {{ $zone === 'Asia/Karachi' ? 'selected' : ''}}>{{$zone}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                        <label  class="col-md-2 control-label" style="padding-top: 10px;color: #337ab7;font-size: 18px !important;font-weight: bold;" id="publishedToggleLabel" for="">@lang('messages.recurrence') </label>
                    <div class="col-md-3">
                        <label style="margin-top: 12px;" class="switchToggle">
                            <input type="hidden" name="isRecurring" id="isRecurring" value="0">
                            <input type="checkbox" id="recurrenceToggle" value="1" >
                            <span id="publishedToggleSpan" class="slider round green"></span>
                        </label>
                    </div>
                </div>
                <div class="recuurence" id="recuurence-div"  style="display:none;">
                    <label class="row ml-4"><u>@lang('messages.recurrence-pattern')</u></label> <br/>
                    <div class="row">
                        <div class="col-md-2 col-sm-3">
                            <div class="radio radio-aqua">
                                <input id="radiobg1" class="pattern" data-name="daily" name="recurringType" type="radio" value="D" checked="checked">
                                <label for="radiobg1">
                                    @lang('messages.daily')
                                </label>
                            </div>
                            <div class="radio radio-aqua inline">
                                <input id="radiobg2" class="pattern" data-name="weekly" name="recurringType" value="W" type="radio">
                                <label for="radiobg2">
                                    @lang('messages.weekly')
                                </label>
                            </div>
                        </div>
                        <div class="vl"></div>
                        <div class="col-md-9 col-sm-9 weekly-div" style="display:none">
                            <div class="row">
                                <div class="form-group col-lg-3">
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg1" class="weekly" value="1" data-no="1"  type="checkbox" >
                                        <label for="checkboxbg1">
                                            @lang('messages.sunday')
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3">
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg2" class="weekly" value="2" data-no="2"  type="checkbox" >
                                        <label for="checkboxbg2">
                                            @lang('messages.monday')
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3">
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg3" class="weekly" value="3" data-no="3"  type="checkbox" >
                                        <label for="checkboxbg3">
                                            @lang('messages.tuesday')
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3">
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg4" class="weekly" value="4" data-no="4" type="checkbox" >
                                        <label for="checkboxbg4">
                                            @lang('messages.wednesday')
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3">
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg5" class="weekly" value="5" data-no="5" type="checkbox" >
                                        <label for="checkboxbg5">
                                            @lang('messages.thursday')
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3">
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg6" class="weekly" value="6" data-no="6"  type="checkbox" >
                                        <label for="checkboxbg6">
                                            @lang('messages.friday')
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3">
                                    <div class="checkbox checkbox-aqua">
                                        <input id="checkboxbg7" class="weekly" value="7" data-no="7"  type="checkbox">
                                        <label for="checkboxbg7">
                                            @lang('messages.saturday')
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <label class="row ml-4 mt-3"><u>@lang('messages.terminate-meeting-at')</u></label><br/>
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <div class="row">
                                <div class="col-md-3 col-sm-3">
                                    <div class="radio radio-aqua">
                                        <input id="radiobg4" class="range-end"  name="recurrenceRangeType" data-fieldIdsToDiable="endDate,addonCalender" value="O" data-fieldIdToEnable="iteration" type="radio">
                                        <label for="radiobg4">
                                        @lang('messages.end-after')
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-9">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 p-0">
                                            <input class="form-control numberOfRecurrence"  type="number" id="iteration" name="noOfRecurrence" value="" min="1" max="50">
                                        </div>
                                        <div class="col-md-6 col-sm-6 pl-2">
                                            <label for="">
                                            @lang('messages.occurence')
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-3 col-sm-3">
                                    <div class="radio radio-aqua">
                                        <input id="radiobg5"  class="range-end" data-fieldIdsToDiable="iteration" data-fieldIdToEnable="endDate,addonCalender" name="recurrenceRangeType" type="radio" data-value="D" value="D">
                                        <label for="radiobg5">
                                        @lang('messages.end-by')
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8 p-0">
                                    <div class="form-group row">
                                    <input class="form-control" type="hidden" name="" id="endDate" value="" autocomplete="off" min="" checked>
                                    </div>
                                </div>
                            </div>
                            <br/>
                        </div>
                    </div>

                </div>
                <div class="form-group row">
                        <label  class="col-md-2 control-label" style="padding-top: 10px;" id="publishedToggleLabel" for="">@lang('messages.reminder') </label>
                    <div class="col-md-3">
                        <label style="margin-top: 12px;" class="switchToggle">
                            <input type="hidden" name="reminderOn" id="reminderOn" value="0">
                            <input type="checkbox" id="reminderToggle" value="1" >
                            <span id="publishedToggleSpan" class="slider round green"></span>
                        </label>
                    </div>
                </div>
                <div class="form-group row" id="reminderTime-div" style="display:none">
                    <label class="col-md-2 control-label">@lang('messages.reminder-time')</label>
                    <div class="input-group col-md-8">
                        <select name="reminderTime" id="filter-reminderTime" data-placeholder="Select Timezone" class="form-control select2" style="width: 100%">
                            <option value="00:15"> @lang('messages.15-minutes')</option>
                            <option value="00:30"> @lang('messages.30-minutes')</option>
                            <option value="00:45"> @lang('messages.45-minutes')</option>
                            <option value="00:59"> @lang('messages.1-hour')</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                        <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-default">@lang('messages.cancel-btn')</button>
                        <button type="button" id="reset-add-form" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-danger">@lang('messages.reset')</button>
                </div>
                <div class="col-lg-12 p-t-20" id="add-form-errors"></div>
            </form>
            </div>
        </div>
    </div>
</div>
