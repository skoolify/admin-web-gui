@extends('auth.master')

@push('extra-css')
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }
    </style>
@endpush

@section('title', 'Error')

@section('content')

<div class="limiter">
    <div class="container-login100 page-background">
        <div class="wrap-login100">
            <form class="form-404">
                <span class="login100-form-logo">
                    <img alt="" src="{{ asset('assets/img/403.jpg') }}" />
                </span>
                <span class="form404-title p-b-34 p-t-27">
                    @lang('messages.error')
                </span>
                <p class="content-404">@lang('messages.cannotProcessTheRequestDuetoMissingSecretKey')</p>
                {{-- <div class="container-login100-form-btn">
                    <a class="login100-form-btn" href="{{ session()->get('home-page') ? route(session()->get('home-page')) : url('auth/login') }}">Go Home</a>
                </div>

                <br />
                <div class="container-login100-form-btn">
                    <a class="login100-form-btn" href="{{ url('auth/login') }}">Logout</a>
                </div> --}}
            </form>
        </div>
    </div>
</div>

@endsection
