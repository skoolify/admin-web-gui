@extends('auth.master')

@push('extra-css')
    <style>
        .page-background{
            background-image: url('{{ asset("assets/img/login-banner.jpg") }}') !important;
        }
    </style>
@endpush

@section('title', 'Skoolify System Maintenance')

@section('content')

<div class="limiter">
    <div class="container-login100 page-background">
        <div class="wrap-login100">
            <form class="form-404">
                <span class="login100-form-logo">
                    <h1><i class="fa fa-cogs fa-spin"></i></h1>
                </span>
                <h1 class="text-white">
                    @lang('messages.serviceUnavailable')
                </h1>
                <hr />
                <h5 class="text-white">@lang('messages.systemMaintenanceProgressRegretTheInconvenience')</h5>
                <br />

            </form>
        </div>
    </div>
</div>

@endsection
