<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="quiz-results-form" action="{{ url('quiz-results/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('marksTotal') ? 'has-error' :'' }}">
                        <label for="marksTotal">@lang('messages.total-marks')
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="marksTotal" type = "text" id = "marksTotal" placeholder="Total Marks" value="{{ old('marksTotal') }}"  />
                        {!! $errors->first('marksTotal','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('marksObtained') ? 'has-error' :'' }}">
                        <label for="marksObtained">@lang('messages.obtained-marks')
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="marksObtained" type = "text" id = "marksObtained" placeholder="{{Lang::get('messages.obtained-marks')}}" value="{{ old('marksObtained') }}"  />
                        {!! $errors->first('marksObtained','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('comments') ? 'has-error' :'' }}">
                        <label for="comments" >@lang('messages.comments')
                            <span class="required"> * </span>
                        </label>
                        <textarea class = "form-control" name="comments" id = "comments" required>{{ old('comments') }}</textarea>
                        {!! $errors->first('comments','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('quizDate') ? 'has-error' :'' }}">
                        <label for="quizDate">@lang('messages.quiz-date')
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="quizDate" type = "date" id = "quizDate" placeholder="{{Lang::get('messages.quiz-date')}}" value="{{ old('quizDate') }}" required  />
                        {!! $errors->first('quizDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
