<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Skoolify" />
    <meta name="author" content="Skoolify" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Skoolify | @yield('title')</title>

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="{{ asset('assets/fonts/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('assets/fonts/material-design-icons/material-icon.css') }}" rel="stylesheet" type="text/css" />
	<!--bootstrap -->
	<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- data tables -->
    <link href="{{ asset('assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/material/material.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/material_style.css') }}">
	<!-- Theme Styles -->
    <link href="{{ asset('assets/css/theme/full/theme_style.css') }}" rel="stylesheet" id="rt_style_components" type="text/css" />
    <link href="{{ asset('assets/css/theme/full/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/sweetalert.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/theme/full/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">
    <link href="{{ asset('assets/plugins/summernote/summernote.css')}}" rel="stylesheet">
    
	<!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}" />
    <link href="{{ asset('assets/css/jquery-confirm.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-multiselect.css') }}">
    <script>
    var invokedFunctionArray = [];
    var __CURRENTDATE__ = '{{ \Carbon\Carbon::now()->toDateString() }}'
    var __DATEFORMAT__ = '{{ config("app.default_timeformat") }}' 
    var __BASEURL__ = '{{ env("API_URL") }}/{{env("API_VERSION")}}';
    var __SUBPARENT__ = '{{Config::get("constants.subParentModule.subParent")}}';
    var __SUBCHILDREN__ = '{{implode(" ",Config::get("constants.subChildren"))}}';
    __SUBCHILDREN__ = __SUBCHILDREN__.split(' ');
    var __BASE_AJAX__ = '{{ url("/ajax/") }}';
    var __BASE__ = '{{ url("/") }}';
    var __ROUTE__ = '{{ Route::currentRouteName() }}';
    var __ACCESS__ = '{{ Authorization::check(Route::currentRouteName(), "edit", Authorization::getParent(Route::currentRouteName()), true) }}';
    var __COMM_ACCESS__ = '{{ Authorization::check("comm-requests", "edit", Authorization::getParent("comm-requests"), true) }}';
    var __DEL__ = '{{ Authorization::check(Route::currentRouteName(), "delete", Authorization::getParent(Route::currentRouteName()), true) }}';
    var __BRANCHES__ = '{{ Authorization::check("school-branches", "read", Authorization::getParent("school-branches"), true) }}';
    var __SCHOOLS__ = '{{ Authorization::check("schools", "read", Authorization::getParent("schools"), true) }}';
    </script>
    @yield('extra-css')

    <style>
        span.arrow::before{
            color:blue !important;
            font-weight:bold;
            font-size : 20px !important;
            width: 24px !important;
            height: 24px !important;
        }
    /* .page-wrapper {
        display:none;
    } */
    .preload { 
        width: 100%;
        height: 100%;
        position: fixed;
        background: #ffffffd9;
        z-index:99999999999999;
    }
    #preload-image{
        right: 0;
        left: 0;
        position: absolute;
        top: 0;
        bottom: 0;
        margin: auto
    }
    </style>
@stack('styles')
</head>

<!-- add class 'page-full-width' for full width layout -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-indigo">
   
    <div class="preload" style="display:none">
        <img id="preload-image" class="img-responsive" src="{{ asset('assets/loader.gif') }}">
    </div> 

    <div class="page-wrapper">
        
        @include('partials.header-left-sidebar')<!-- Uncomment to enable Left Sidebar menu layout --> 
        
        {{-- @include('partials.header') --}}

        <div class="page-container">
            @include('partials.sidebar')

            <!-- Uncomment the below div to enable left sidebar layout -->
            <div class="page-content-wrapper">
                @include('partials.msg-box')
                @yield('content')
            </div>
        </div>

        <!-- Comment the below div to disable left sidebar layout -->
        {{-- <div class="page-content-wrapper">
            @yield('content')
        </div> --}}
    
        @include('partials.footer')
    </div>

    <!-- start js include path -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.min.js')}}"></script>
	<script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <!-- data tables -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
 	<script src="{{ asset('assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/table/table_data.js') }}" ></script>
    <!-- Common js-->
	<script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/js/layout.js') }}"></script>
	<script src="{{ asset('assets/js/theme-color.js') }}"></script>
	<!-- Material -->
	<script src="{{ asset('assets/plugins/material/material.min.js') }}"></script>
    <!-- end js include path -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script> -->
    <script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/js/moment.js') }}"></script>
    <!-- <script src="https://momentjs.com/downloads/moment-timezone-with-data.js"></script> -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{asset('assets/js/jquery.form-validator.min.js')}}"></script>
    <script src="{{ asset('scripts/global.js?version='.time()) }}"></script>
    <script src="{{ asset('scripts/list-functions.js?version='.time()) }}"></script>
    <script src="{{ asset('scripts/dependent-dropdown-list.js?version='.time()) }}"></script>
    <script src="{{ asset('scripts/js-md5.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone.min.js"></script> -->
    <script src="{{asset('assets/plugins/summernote/summernote.js')}} "></script>
    <script src="{{asset('assets/js/pages/summernote/summernote-data.js')}}" ></script>
    <script src="{{asset('assets/js/jquery-confirm.min.js')}}" ></script>
    <script src="{{asset('assets/js/bootstrap-multiselect.js')}}" ></script>
    <script src="{{asset('assets/js/loadingoverlay.min.js')}}"></script>

    @yield('extra-scripts')
    @stack('extra-scripts')
    @stack('scripts')
</body>
</html>