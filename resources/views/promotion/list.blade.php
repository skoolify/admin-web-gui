@extends('master')

@section('title', Lang::get('messages.promotions-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.promotions-management')))

    <div class="row">
    <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            <div class="col-md-3 col-sm-12 col-xs-12" style="display:none" id="total-marks-div">
                                <div class="form-group">
                                    <label for="totalMarks">@lang('messages.total-marks')
                                        <span class="required"> * </span>
                                    </label>
                                    <input class = "form-control" name="totalMarks"  type = "text" id = "totalMarks" placeholder="Total Marks" value="{{ old('totalMarks') }}"  />
                                    {!! $errors->first('totalMarks','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('promotion.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.promotions-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container" data-toggle="tooltip" title="{{Lang::get('messages.move-student-to-next-class')}}"><i class="fa fa-arrows" aria-hidden="true" ></i> @lang('messages.move') </button>
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="promotion-result-table" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>@lang('messages.roll-number')</th>
                                    <th>@lang('messages.student')</th>
                                    <th>@lang('messages.class')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>

@endpush
