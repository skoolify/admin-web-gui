<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        <div class="col-lg-12">
                <div id="p2-students-result-list" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100% !important">
                </div>
                <table class="table table-striped  table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="move-student-table">
                    <thead>
                        <tr>
                            <th>@lang('messages.s-number')</th>
                            <th>@lang('messages.roll-number')</th>
                            <th>@lang('messages.student')</th>
                            <th>@lang('messages.class')</th>
                            <th>@lang('messages.select')</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="9">
                            <strong style="color:#188ae2;font-weight:bold">@lang('messages.loading')</strong>
                            {{-- <strong style="color:#188ae2;font-weight:bold">Please Select Exam Term</strong> --}}
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="col-lg-12 p-t-20" id="add-form-errors"></div>
                        <div class="col-lg-12 p-t-20 text-center">
                        <button type="button" id="submit-add-form-container1" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink move">@lang('messages.move')</button>
                        <button type="button" id="hide-add-form-container-promotion" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade move-modal" id="moveStudentModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style="min-width: 300px">
    <form method="POST" id="move-student-form" >
      <div class="modal-header">
        <h4 class="modal-title message-list-title" id="exampleModalLabel" style="color:rgb(47, 167, 255);font-weight:bold;font-size:20px;" >@lang('messages.move-students')</h4>
        <button type="button" style="color:red !important; " class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
      </div>
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="">@lang('messages.classes')</label>
                    <select name="classLevelID" id="filter-move-classLevels" class="form-control">
                        <option>@lang('messages.select') @lang('messages.class')</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" style="display: {{ Route::currentRouteName() === 'subjects' ? 'none' : '' }}">
                <div class="form-group">
                    <label for="">@lang('messages.section')</label>
                    <select name="classID" id="filter-move-classes" class="form-control">
                        <option>@lang('messages.select') @lang('messages.section')</option>
                    </select>
                    <div id="p2-classes-move" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                </div>
            </div>
        </div>
    </div>
      <div class="modal-footer">
      <div class="row">
            <div class="col-lg-12 p-t-20" id="add-form-errors-tab">
            </div>
            <div class="d-flex justify-content-between p-t-20">
                <button type="submit" id="submit-move-student-form" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" disabled="disabled">@lang('messages.submit-btn')</button>
                <button type="button" id="close-hide-add-form-container" data-dismiss="modal" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
            </div>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>
