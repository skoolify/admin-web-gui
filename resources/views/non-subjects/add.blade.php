<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="non-subjects-form" action="{{ url('non-subjects/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('nonSubjectName') ? 'has-error' :'' }}">
                        <label for="nonSubjectName">@lang('messages.non-subject-name')
                        </label>
                        <input maxlength="45" class = "form-control" name="nonSubjectName" type = "text" id = "nonSubjectName" value="{{ old('nonSubjectName') }}" data-validation="required length" data-validation-length="max45" placeholder="{{Lang::get('messages.non-subject-name')}}">
                        {!! $errors->first('nonSubjectName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @include('partials.active-field')

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
<div class="non-subjects-legend-form-container col-md-12 col-lg-12" style="display:none;" >
            <div class="card-box">
                <div class="card-head">
                    <header  style="color: rgb(47, 167, 255);"> @lang('messages.legends')</header>
                    {{-- <button type="button" id="hide-non-subject-legend-container" class="non-subject-cancel-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default pull-right">@lang('messages.cancel-btn')</button> --}}
                </div>
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">
                <div class="col-lg-12 p-t-20 text-right">
               <table class="table table-striped table-hover table-checkable" id="legends-table" style="width: 60% !important;">
                    <thead style="text-align: center;">
                        <tr>
                            <th width="60%">@lang('messages.legend')</th>
                            <th>@lang('messages.status-column')</th>
                            <th>@lang('messages.action-column')</th>
                        </tr>
                    </thead>
                    <tbody style="text-align: center!important;">
                        <tr>
                            <td>
                                <input class="form-control legend" type="text" name="legendValue">
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>
                                <button type="button" class="btn btn-primary btn-sm"> @lang('messages.submit-btn')</button>
                            </td>
                        </tr>

                    </tbody>
               </table>
               <div id="p2-legend" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 60%"></div>
            </div>
            </div>
    </div>
</div>
