<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="countries-form" action="{{ url('countries/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-4 p-t-20"> 
                    <div class="form-group {{ $errors->has('countryName') ? 'has-error' :'' }}">
                        <label for="countryName">@lang('messages.name')
                        </label>
                        <input maxlength="50" class = "form-control" name="countryName" type = "text" id = "countryName" placeholder="{{Lang::get('messages.name')}}" value="{{ old('countryName') }}" data-validation="required length" data-validation-length="max50"  />
                        {!! $errors->first('countryName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20"> 
                    <div class="form-group {{ $errors->has('dialingCode') ? 'has-error' :'' }}">
                        <label for="dialingCode">@lang('messages.dialing-code')
                        </label>
                        <input class = "form-control" name="dialingCode" type = "text" id = "dialingCode" placeholder="{{Lang::get('messages.dialing-code')}}" value="{{ old('dialingCode') }}" data-validation="required number length" data-validation-length="max5" />
                        {!! $errors->first('dialingCode','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20"> 
                    <div class="form-group {{ $errors->has('currencyISOCode') ? 'has-error' :'' }}">
                        <label for="currencyISOCode">@lang('messages.country-ISO-code')
                        </label>
                        <input maxlength="3" onKeyUp="numericFilter(this);" class="form-control" name="currencyISOCode" type = "text" id = "currencyISOCode" placeholder="{{Lang::get('messages.country-ISO-code')}}" value="{{ old('currencyISOCode') }}" data-validation="required alphanumeric length" data-validation-length="max3" />
                        {!! $errors->first('currencyISOCode','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20"> 
                    <div class="form-group {{ $errors->has('currencyAlphaCode') ? 'has-error' :'' }}">
                        <label for="currencyAlphaCode">@lang('messages.currency-alpha-code')
                        </label>
                        <input onkeypress="return onlyAlphabets(event,this);" maxlength="3" class="form-control" name="currencyAlphaCode" type = "text" id = "currencyAlphaCode" placeholder="{{Lang::get('messages.currency-alpha-code')}}" value="{{ old('currencyAlphaCode') }}" data-validation="required alphanumeric length" data-validation-length="max3" />
                        {!! $errors->first('currencyAlphaCode','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20"> 
                    <div class="form-group {{ $errors->has('currencyName') ? 'has-error' :'' }}">
                        <label for="currencyName">@lang('messages.currency-name')
                        </label>
                        <input class="form-control" name="currencyName" type = "text" id = "currencyName" placeholder="{{Lang::get('messages.currency-name')}}" value="{{ old('currencyName') }}" data-validation="required alphanumeric length" data-validation-length="max100" />
                        {!! $errors->first('currencyName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-4 p-t-20"> 
                    <div class="form-group {{ $errors->has('currencyDecimalValue') ? 'has-error' :'' }}">
                        <label for="currencyDecimalValue">@lang('messages.currency-decimal-value')
                        </label>
                        <select class = "form-control" placeholder="Currency Decimal Value" name="currencyDecimalValue" id="currencyDecimalValue" data-validation="required number length" data-validation-length="max3">
                            <option value="0">@lang('messages.0')</option>
                            <option value="1">@lang('messages.1')</option>
                            <option value="2">@lang('messages.2')</option>
                            <option value="3">@lang('messages.3')</option>
                        </select>
                        {!! $errors->first('currencyDecimalValue','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @include('partials.active-field')

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
<script language="Javascript" type="text/javascript"> 

    function onlyAlphabets(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                return true;
            else
                return false;
        }
        catch (err) {
            alert(err.Description);
        }
    } 

    function numericFilter(txb) {
   txb.value = txb.value.replace(/[^\0-9]/ig, "");
}

    </script>