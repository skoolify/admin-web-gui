@extends('master')

@section('title', Lang::get('messages.role-access-rights-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />

<style>
    .blue-row{
        background: #188ae2 !important;
        color: white !important;
    }
    .green-row{
        background: #2ECC71 !important;
        color: white !important;
    }
</style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.role-access-rights-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.roles-filter')
                            @include('partials.components.modules-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('role-access-rights.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.role-access-rights-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <!-- <div class="btn-group pull-right"> -->
                        <button class="btn btn-primary pull-right" id="add-access-rights"> <i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    <!-- </div> -->
                    @endif
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.module-name')</th>
                                    <th>@lang('messages.read')</th>
                                    <th>@lang('messages.add-new-btn')</th>
                                    <th>@lang('messages.edit-btn')</th>
                                    <th>@lang('messages.delete-btn')</th>
                                    <th>@lang('messages.status-active')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="modules-list">
            <div class="card-box">

                <div class="card-head">
                    <header>@lang('messages.add-role-access-rights')</header>
                    <!-- <div class="btn-group "> -->
                        <button class="btn btn-primary pull-right mr-1" id="submit-access-rights">@lang('messages.submit-btn')</button>
                        <button class="btn btn-danger pull-right mr-2" id="cancel-access-rights">@lang('messages.cancel-btn')</button>
                    <!-- </div> -->
                </div>
                <form action="#" id="bulk-access-rights-form">
                    <div class="card-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('messages.module-name')</th>
                                        {{-- <th>
                                            <div class="checkbox checkbox-aqua">
                                                <input class="readAccess bulk-access" id="checkboxbg1" type="checkbox" data-key="readAccess">
                                                <label for="checkboxbg1">
                                                <p style="margin-top: -3px;">Read</p>
                                                </label>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="checkbox checkbox-aqua">
                                                <input class="addAccess bulk-access" id="checkboxbg2" type="checkbox" data-key="addAccess">
                                                <label for="checkboxbg2">
                                                <p style="margin-top: -3px;">Add</p>
                                                </label>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="checkbox checkbox-aqua">
                                                <input class="editAccess bulk-access" id="checkboxbg3" type="checkbox" data-key="editAccess">
                                                <label for="checkboxbg3">
                                                <p style="margin-top: -3px;">Edit</p>
                                                </label>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="checkbox checkbox-aqua">
                                                <input class="deleteAccess bulk-access" id="checkboxbg4" type="checkbox" data-key="deleteAccess">
                                                <label for="checkboxbg4">
                                                <p style="margin-top: -3px;">Delete</p>
                                                </label>
                                            </div>
                                        </th> --}}
                                        <th class="text-center bulk-access" data-key="readAccess">@lang('messages.read') <i class="fa fa-check-square-o"></i></th>
                                        <th class="text-center bulk-access" data-key="addAccess">@lang('messages.add-new-btn') <i class="fa fa-check-square-o"></i></th>
                                        <th class="text-center bulk-access" data-key="editAccess">@lang('messages.edit-btn') <i class="fa fa-check-square-o"></i></th>
                                        <th class="text-center bulk-access" data-key="deleteAccess">@lang('messages.delete-btn') <i class="fa fa-check-square-o"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6" class="text-center text-success" style="font-weight:bold"> @lang('messages.loading')</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
