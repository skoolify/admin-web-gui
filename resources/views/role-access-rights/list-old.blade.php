@extends('master')

@section('title', 'Role Access Rights')

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />

<style>
    .blue-row{
        background: #188ae2 !important;
        color: white !important;
    }
    .green-row{
        background: #2ECC71 !important;
        color: white !important;
    }
</style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => 'Role Access Rights Management'))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> Filters</header>
                </div>
                
                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.roles-filter')
                            @include('partials.components.modules-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('role-access-rights.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>Role Access Rights List</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <!-- <div class="btn-group pull-right"> -->
                        <button class="btn btn-primary pull-right" id="add-access-rights"> <i class="fa fa-plus"></i> ADD</button>
                    <!-- </div> -->
                    @endif
                </div>
                
                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Module Name</th>
                                    <th>Read</th>
                                    <th>Add</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    <th>Active</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="modules-list">
            <div class="card-box">

                <div class="card-head">
                    <header>Add Role Access Rights</header>
                    <!-- <div class="btn-group "> -->
                        <button class="btn btn-primary pull-right mr-1" id="submit-access-rights">SUBMIT</button>
                        <button class="btn btn-danger pull-right mr-2" id="cancel-access-rights">CANCEL</button>
                    <!-- </div> -->
                </div>
                <form action="#" id="bulk-access-rights-form">
                    <div class="card-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Module Name</th>
                                        <th class="text-center bulk-access" data-key="readAccess">Read <i class="fa fa-check-square-o"></i></th>
                                        <th class="text-center bulk-access" data-key="addAccess">Add <i class="fa fa-check-square-o"></i></th>
                                        <th class="text-center bulk-access" data-key="editAccess">Edit <i class="fa fa-check-square-o"></i></th>
                                        <th class="text-center bulk-access" data-key="deleteAccess">Delete <i class="fa fa-check-square-o"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6" class="text-center text-success" style="font-weight:bold"> Loading ...</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
