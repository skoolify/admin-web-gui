<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="role-access-rights-form" action="{{ url('role-access-rights/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="checkbox checkbox-icon-black p-0">
                        <input id="readAccess" type="checkbox" name="readAccess" value="1" />
                        <label for="readAccess">
                            @lang('messages.read-access')
                        </label>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="checkbox checkbox-icon-black p-0">
                        <input id="addAccess" type="checkbox" name="addAccess" value="1" />
                        <label for="addAccess">
                            @lang('messages.add-access')
                        </label>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="checkbox checkbox-icon-black p-0">
                        <input id="editAccess" type="checkbox" name="editAccess" value="1" />
                        <label for="editAccess">
                            @lang('messages.edit-access')
                        </label>
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="checkbox checkbox-icon-black p-0">
                        <input id="deleteAccess" type="checkbox" name="deleteAccess" value="1" />
                        <label for="deleteAccess">
                            @lang('messages.delete-access')
                        </label>
                    </div>
                </div>             

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
