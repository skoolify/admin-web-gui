<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        <form method="POST" id="schools-form" action="{{ url('schools/add') }}" enctype="multipart/form-data">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">
                <input type="hidden" id="editHiddenField" value="" />
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('schoolName') ? 'has-error' :'' }}">
                        <label for="schoolName">@lang('messages.name')
                        </label>
                        <input maxlength="150" class="form-control" name="schoolName" type="text" id="schoolName" placeholder="{{Lang::get('messages.name')}}" value="{{ old('schoolName') }}" data-validation="required length" data-validation-length="max150"  />
                        {!! $errors->first('schoolName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('schoolDomain') ? 'has-error' :'' }}">
                        <label for="schoolDomain">@lang('messages.domain')

                        </label>
                        <input maxlength="100" class="form-control" name="domain" type="text" id="schoolDomain" placeholder="{{Lang::get('messages.domain')}}" value="{{ old('schoolDomain') }}" data-validation="required length" data-validation-length="max100"   />
                        <label class="hidden" id="schoolLabelId">@lang('messages.domain-already-exist')</label>
                        {!! $errors->first('schoolDomain','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('schoolOwnerName') ? 'has-error' :'' }}">
                        <label for="schoolOwnerName">@lang('messages.owner-name')
                        </label>
                        <input class="form-control" name="owner" type="text" id="schoolOwnerName" placeholder="{{Lang::get('messages.owner-name')}}" value="{{ old('schoolOwnerName') }}" data-validation="required length" data-validation-length="max50" maxlength="50"  />
                        {!! $errors->first('schoolOwnerName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('schoolWebsiteURL') ? 'has-error' :'' }}">
                        <label for="schoolWebsiteURL">@lang('messages.website-url')
                        </label>
                        <input maxlength="100" class="form-control" name="websiteURL" type="url" id="schoolWebsiteURL" placeholder="{{Lang::get('messages.website-url')}}" value="" data-validation="required url length" data-validation-length="max100"  />
                        {!! $errors->first('schoolWebsiteURL','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-4 p-t-20" id="upload-school-logo">
                    <label class="control-label col-md-12">@lang('messages.upload-logo') <button type="button" class="btn btn-xs btn-danger pull-right hidden" id="reset-logo-image"  title="Remove Image"><i class="fa fa-trash"></i></button> <button type="button" class="btn btn-xs btn-danger pull-right hidden" id="reset-logo-image-edit"  title="Remove Image"><i class="fa fa-trash"></i></button></label>
                    <input type="hidden" name="schoolLogo" id="hiddenSchoolLogo" value="" />
                    <input type="file" class="form-control" name="logoImage" id="logoImg" data-validation="required"
                        data-validation-allowing="jpg, png, gif"
                        data-validation-max-size="100kb"
                        data-validation-error-msg-size="You can not upload images larger than 100kb"
                        data-validation-error-msg-mime="You can only upload images" accept="image/x-png,image/gif,image/jpeg" />
                    <p><em><small>*@lang('messages.max-2-mb-allowed')</small></em></p>
                </div>
                <div class="col-lg-4 p-t-20 hidden" id="existing-school-logo">
                    <label class="control-label col-md-12">@lang('messages.school-logo') <button type="button" class="btn btn-xs btn-primary pull-right" id="camera-icon"  title="Edit Image"><i class="fa fa-camera"></i></button></label>
                    <input type="file" id="imgupload" style="display:none" accept="image/x-png,image/gif,image/jpeg" />
                    <img src="" style="margin: auto" class="img-responsive" id="existing-logo" height="75" width="75" alt="Logo" />
                </div>
                <div class="col-lg-4 p-t-20">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="isActive" id="isActiveInput" value="1">
                        <input class="isActive" id="isActive" type="checkbox" checked>
                        <label for="isActive">
                            <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                        </label>
                    </div>
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="selfRegistered" id="selfRegisteredInput" value="0">
                        <input class="selfRegistered" id="selfRegistered" type="checkbox">
                        <label for="selfRegistered">
                            <p style="margin-top: -3px;">@lang('messages.self-registered')</p>
                        </label>
                    </div>
                </div>

                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.legal-details')</h3><hr>
                </div>
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('legalEntityName') ? 'has-error' :'' }}">
                        <label for="legalEntityName">@lang('messages.business-name')<span class="required">*</span>
                        </label>
                        <input maxlength="200" class="form-control" data-validation="required length" data-validation-length="max200" name="legalEntityName" type="text" id="legalEntityName" placeholder="{{Lang::get('messages.business-name')}}" value="" required />
                        {!! $errors->first('legalEntityName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('legalEntityAddress') ? 'has-error' :'' }}">
                        <label for="legalEntityAddress">@lang('messages.business-address')<span class="required">*</span>
                        </label>
                        <input maxlength="500" class="form-control" data-validation="required length" data-validation-length="max500" name="legalEntityAddress" type="text" id="legalEntityAddress" placeholder="{{Lang::get('messages.business-address')}}" value="" required/>
                        {!! $errors->first('legalEntityAddress','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('legalEntityPerson') ? 'has-error' :'' }}">
                        <label for="legalEntityPerson">@lang('messages.point-of-contact')<span class="required">*</span>
                        </label>
                        <input maxlength="100" class="form-control" data-validation="required length" data-validation-length="max100" name="legalEntityPerson" type="text" id="legalEntityPerson" placeholder="{{Lang::get('messages.point-of-contact')}}" value="" required/>
                        {!! $errors->first('legalEntityPerson','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-4 p-t-20">
                    <div class="form-group {{ $errors->has('legalEntityContactNo') ? 'has-error' :'' }}">
                        <label for="legalEntityContactNo">@lang('messages.contact-no')<span class="required">*</span>
                        </label>
                        <input maxlength="45" class="form-control" data-validation="required length" data-validation-length="max45" name="legalEntityContactNo" type="tel" id="legalEntityContactNo" placeholder="{{Lang::get('messages.contact-no')}}" value="" required />
                        {!! $errors->first('legalEntityContactNo','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <h3 class="text-primary">@lang('messages.school-parameters')</h3><hr>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="feePaymentAllowed" id="feePaymentAllowedInput" value="0">
                        <input class="feePaymentAllowed" id="feePaymentAllowed" type="checkbox">
                        <label for="feePaymentAllowed">
                            <p style="margin-top: -3px;">@lang('messages.enable-fee-payments')
                                <span class="required" style="color: red;"> * </span>
                            </p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="showAccNoOnVoucher" id="showAccNoOnVoucherInput" value="0">
                        <input class="showAccNoOnVoucher" id="showAccNoOnVoucher" type="checkbox">
                        <label for="showAccNoOnVoucher">
                            <p style="margin-top: -3px;">@lang('messages.show-acc-no-on-voucher')
                                <span class="required" style="color: red;"> * </span>
                            </p>
                        </label>
                    </div>
                </div>
                {{-- <div class="col-lg-4"></div> --}}
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('weekStartDay') ? 'has-error' :'' }}">
                        <label for="weekStartDay">@lang('messages.week-first-day')<span class="required">*</span>
                        </label>
                        <select name="weekStartDay" id="filter-week-day" class="form-control filter-select">
                            <option value="">@lang('messages.please-select')</option>
                            <option value="1">@lang('messages.sunday')</option>
                            <option value="2">@lang('messages.monday')</option>
                            <option value="3">@lang('messages.tuesday')</option>
                            <option value="4">@lang('messages.wednesday')</option>
                            <option value="5">@lang('messages.thursday')</option>
                            <option value="6">@lang('messages.friday')</option>
                            <option value="7">@lang('messages.saturday')</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('studentArrivalEndTime') ? 'has-error' :'' }}">
                        <label for="marksTotal">@lang('messages.studentArrivalTime')
                        </label>
                        <div class="input-group clockpicker">
                            <input class = "form-control" name="studentArrivalEndTime"  type = "text" id = "studentArrivalEndTime" placeholder="00:00" value="" autocomplete="off" />
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('studentAttendanceGracePeriod') ? 'has-error' :'' }}">
                        <label for="marksTotal">@lang('messages.student-grace-period')
                        </label>
                        <div class="input-group clockpicker">
                            <input class = "form-control" name="studentAttendanceGracePeriod"  type = "text" id = "studentAttendanceGracePeriod" placeholder="00:00" value="" autocomplete="off" />
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('studentCutOffTime') ? 'has-error' :'' }}">
                        <label for="marksTotal">@lang('messages.studentCutOffTime')
                        </label>
                        <div class="input-group clockpicker">
                            <input class = "form-control" name="studentCutOffTime"  type = "text" id = "studentCutOffTime" placeholder="00:00" value="" autocomplete="off" />
                        </div>
                    </div>
                </div>







                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('staffArrivalEndTime') ? 'has-error' :'' }}">
                        <label for="marksTotal">@lang('messages.staffArrivalTime')
                        </label>
                        <div class="input-group clockpicker">
                            <input class = "form-control" name="staffArrivalEndTime"  type = "text" id = "staffArrivalEndTime" placeholder="00:00" value="" autocomplete="off" />
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('staffAttendanceGracePeriod') ? 'has-error' :'' }}">
                        <label for="marksTotal">@lang('messages.staff-grace-period')
                        </label>
                        <div class="input-group clockpicker">
                            <div class="input-group clockpicker">
                                <input class = "form-control" name="staffAttendanceGracePeriod"  type = "text" id = "staffAttendanceGracePeriod" placeholder="00:00" value=""  autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('staffCutOffTime') ? 'has-error' :'' }}">
                        <label for="marksTotal">@lang('messages.staffCutOffTime')
                        </label>
                        <div class="input-group clockpicker">
                            <input class = "form-control" name="staffCutOffTime"  type = "text" id = "staffCutOffTime" placeholder="00:00" value="" autocomplete="off" />
                        </div>
                    </div>
                </div>






                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appHomework" id="appHomeworkInput" value="0">
                        <input class="appHomework" id="appHomework" type="checkbox">
                        <label for="appHomework">
                            <p style="margin-top: -3px;">@lang('messages.enable-homework')</p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appTimetable" id="appTimetableInput" value="0">
                        <input class="appTimetable" id="appTimetable" type="checkbox">
                        <label for="appTimetable">
                            <p style="margin-top: -3px;">@lang('messages.enable-time-table')</p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appExamQuiz" id="appExamQuizInput" value="0">
                        <input class="appExamQuiz" id="appExamQuiz" type="checkbox">
                        <label for="appExamQuiz">
                            <p style="margin-top: -3px;">@lang('messages.enable-exam-quiz')</p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appfeeVoucher" id="appfeeVoucherInput" value="0">
                        <input class="appfeeVoucher" id="appfeeVoucher" type="checkbox">
                        <label for="appfeeVoucher">
                            <p style="margin-top: -3px;">@lang('messages.enable-fee-voucher')</p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appAttendance" id="appAttendanceInput" value="0">
                        <input class="appAttendance" id="appAttendance" type="checkbox">
                        <label for="appAttendance">
                            <p style="margin-top: -3px;">@lang('messages.enable-attendance')</p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appMessage" id="appMessageInput" value="0">
                        <input class="appMessage" id="appMessage" type="checkbox">
                        <label for="appMessage">
                            <p style="margin-top: -3px;">@lang('messages.enable-messaging')</p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appCommunicate" id="appCommunicateInput" value="0">
                        <input class="appCommunicate" id="appCommunicate" type="checkbox">
                        <label for="appCommunicate">
                            <p style="margin-top: -3px;">@lang('messages.enable-communicate')</p>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appEvents" id="appEventsInput" value="0">
                        <input class="appEvents" id="appEvents" type="checkbox">
                        <label for="appEvents">
                            <p style="margin-top: -3px;">@lang('messages.enable-events')</p>
                        </label>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appForms" id="appFormsInput" value="0">
                        <input class="appForms" id="appForms" type="checkbox">
                        <label for="appForms">
                            <p style="margin-top: -3px;">@lang('messages.enableForm')</p>
                        </label>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                        <input type="hidden"  name="appVanTracking" id="appVanTrackingInput" value="0">
                        <input class="appVanTracking" id="appVanTracking" type="checkbox">
                        <label for="appVanTracking">
                            <p style="margin-top: -3px;">@lang('messages.enableVanTracking')</p>
                        </label>
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>
                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">@lang('messages.school-logo')</h5>
          <button type="button" class="btn btn-danger btn-sm closeModal" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">x</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="upload-demo" style="width:470px;"></div>
        </div>
        <div class="modal-footer">
          <button id="crop_image" type="button" class="btn btn-primary">@lang('messages.Crop-save')</button>
        </div>
      </div>
    </div>
</div>
