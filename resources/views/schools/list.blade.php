@extends('master')

@section('title', Lang::get('messages.schools-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/bootstrap-clockpicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/jquery-clockpicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/jquery-clockpicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.schools-management')))

    <div class="row">

        @include('schools.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.schools-list')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn')</button>
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>@lang('messages.logo')</th>
                                    <th>@lang('messages.name')</th>
                                    <th>@lang('messages.domain')</th>
                                    <th>@lang('messages.website')</th>
                                    <th>@lang('messages.onboarding-date')</th>
                                    <th>@lang('messages.status-active')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addContractModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.contract-details')</h4>

          <button type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:red">&times;</span>
          </button>
        </div>
        <form id="contract-form">
        <div class="container-fluid">
            <div class="row">
                <input type="hidden" name="chargesID" id="chargesID">
            </div>
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.groupName')</label>
                        </div>
                        <div class="col-md-8">
                            <select name="branchGroupID" id="filter-branch-group" class="form-control">
                                <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.branchGroup')</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.currency'):</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control" name="currencyCode" id="currencyCode">
                                <option value="PKR">@lang('messages.pkr')</option>
                                <option value="USD">@lang('messages.usd')</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.price-student'):</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="number" step="0.01" name="unitPrice" id="unitPrice" data-validation-allowing="float">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.billing-period')</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control" name="billingFrequency" id="billFrequency">
                                <option value="Monthly">@lang('messages.monthly')</option>
                                <option value="Bi-Monthly">@lang('messages.bi-monthly')</option>
                                <option value="Quarterly">@lang('messages.quarterly')</option>
                                <option value="Half-Yearly">@lang('messages.half-yearly')</option>
                                <option value="Annually">@lang('messages.annually')</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.start-date'):</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="date" name="startDate" id="startDate">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.end-date'):</label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="date" name="endDate" id="endDate">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.notes'): </label>
                        </div>
                        <div class="col-md-8">
                            <textarea class="form-control" data-validation="length" data-validation-length="max100" name="chargesNotes" id="chargesNotes" cols="20" rows="1"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.tax-rate'): </label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="taxRate" id="taxRate" data-validation="length" data-validation-length="max100">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">@lang('messages.status-active'): </label>
                        </div>
                        <div class="col-md-8">
                            <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                                <input type="hidden"  name="isActive" id="isActiveChargesInput" value="0">
                                <input class="isActiveCharges" id="isActiveCharges" type="checkbox">
                                <label for="isActiveCharges"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <p style="padding-top:15px;" class="text-danger hidden" id="hiddenError"></p>
            <i class="exportLoader fa fa-spinner fa-pulse pull-right mr-2 hidden" style="font-size:24px;color: #188ae2;"></i>
            <button class="btn btn-primary" type="submit">@lang('messages.save')</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.cancel')</button>
        </div>
    </form>
      </div>
    </div>
</div>

<div class="modal fade" id="addCollectionModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title linked-parent-list-title" id="exampleModalLabel" style="color:#337ab7 !important;" >@lang('messages.collectionDetails')</h4>

          <button type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:red">&times;</span>
          </button>
        </div>
        <form id="collection-form">
        <div class="container-fluid">
            <div class="row">
                <input type="hidden" name="collectionID" id="collectionID">
            </div>
            <hr style="margin: 2px;">
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">@lang('messages.sidebar-branch-tab'):</label>
                        </div>
                        <div class="col-md-9">
                            <select name="branchID" id="filter-branches" class="form-control" multiple="multiple">
                                <option value="">{{ isset($all) && $all ? $all : Lang::get('messages.select') }} @lang('messages.sidebar-branch-tab')</option>
                                @foreach($branches as $branch)
                                    <option {{ session()->has('temp') ? session()->get('temp')->branchID == $branch->branchID ? 'selected' : '' : '' }} value="{{ $branch->branchID }}">{{ $branch->classAlias }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">@lang('messages.start-date'):</label>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" type="date" name="startDate" id="startDateCollection">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">@lang('messages.type'):</label>
                        </div>
                        <div class="col-md-9">
                            <select class="form-control" name="collectionType" id="collectionType">
                                <option value="F">@lang('messages.fixed')</option>
                                <option value="P">@lang('messages.percentage')</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">@lang('messages.rate'):</label>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" type="number" step="0.01" name="collectionRate" id="collectionRate" data-validation-allowing="float">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">@lang('messages.notes'):</label>
                        </div>
                        <div class="col-md-9">
                            <textarea class="form-control" data-validation="length" data-validation-length="max100" name="collectionNotes" id="collectionNotes" cols="20" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">@lang('messages.status-active'): </label>
                        </div>
                        <div class="col-md-9">
                            <div style="padding-left: 0px;" class="checkbox checkbox-aqua">
                                <input type="hidden"  name="isActive" id="isActiveCollectionInput" value="0">
                                <input class="isActiveCollection" id="isActiveCollection" type="checkbox">
                                <label for="isActiveCollection"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <p style="padding-top:15px;" class="text-danger hidden" id="hiddenErrorCollection"></p>
            <i class="exportLoader fa fa-spinner fa-pulse pull-right mr-2 hidden" style="font-size:24px;color: #188ae2;"></i>
            <button class="btn btn-primary" type="submit">@lang('messages.save')</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.cancel')</button>
        </div>
    </form>
      </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{asset('assets/js/bootstrap-clockpicker.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-clockpicker.js')}}"></script>
<script src="{{asset('assets/js/jquery-clockpicker.min.js')}}"></script>
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
