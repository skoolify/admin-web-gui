@extends('master')

@section('title', Lang::get('messages.assign-subjects-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.assign-subjects-management')))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">
                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.subjects-filter')
                            @include('partials.components.subjects-in-timetable-filter')
                            @include('partials.components.students-filter')
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('assign-subjects.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif
                    <div id="non-assessment-case" class="card-head">
                        <header>@lang('messages.assign-subject-list')</header>
                        <button type="button" class="btn btn-round btn-danger pull-right ml-2" id="delete-assigned-subjects" data-toggle="tooltip" title="Unlink Subjects"><i class="fa fa-trash"></i> @lang('messages.delete-btn')</button>
                        @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                        <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn') / <i class="fa fa-pencil"></i> @lang('messages.edit-btn')</button>
                        @endif
                    </div>

                <div class="card-body non-assessment-body" >
                    <div class="table-scrollable">
                        <div class="table-scrollable" >
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="example44">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('messages.student')</th>
                                        <th>@lang('messages.teacher')</th>
                                        <th>@lang('messages.subject')</th>
                                        <th>@lang('messages.weekday')</th>
                                        <th>@lang('messages.start-time')</th>
                                        <th>@lang('messages.end-time')</th>
                                        <th>@lang('messages.room')</th>
                                        <th>@lang('messages.delete-btn')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>


@endpush
