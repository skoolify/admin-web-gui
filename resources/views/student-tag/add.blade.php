<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="tag-form" action="{{ url('subjects/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20"> 
                    <div class="form-group {{ $errors->has('tagText') ? 'has-error' :'' }}">
                        <label for="tagText">@lang('messages.tag-name')
                        </label>
                        <input maxlength="45" class = "form-control" data-validation="required length" data-validation-length="max45" name="tagText" type = "text" id = "tagText" value="{{ old('tagText') }}" placeholder="{{Lang::get('messages.tag-name')}}">
                        {!! $errors->first('tagText','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20" style="margin-top: 27px;"> 
                    <div class="checkbox checkbox-aqua">
                        <input type="hidden" name="isActive" id="isActiveInput" value="0">
                        <input class="isActive" id="isActive" type="checkbox">
                        <label for="isActive">
                        <p style="margin-top: -3px;">@lang('messages.status-active')</p>
                        </label>
                    </div>
                </div>

                {{-- @include('partials.active-field') --}}

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
