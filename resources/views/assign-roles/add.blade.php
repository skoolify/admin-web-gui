<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="roles-form" action="{{ url('roles/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20"> 
                    <div class="form-group {{ $errors->has('roleName') ? 'has-error' :'' }}">
                        <label for="roleName">Role Name
                            <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="roleName" type = "text" id = "roleName" value="{{ old('roleName') }}" required>
                        {!! $errors->first('roleName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('roleDescription') ? 'has-error' :'' }}">
                        <label for="roleDescription" >Role Description
                            <span class="required"> * </span>
                        </label>
                        <textarea class = "form-control" name="roleDesc" id = "roleDescription" required>{{ old('roleDescription') }}</textarea>
                        {!! $errors->first('roleDescription','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @include('partials.active-field')

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
