<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">

        <form method="POST" id="attendance-form" action="{{ url('attendance/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('attendanceDate') ? 'has-error' :'' }}">
                        <label for="attendanceDate">@lang('messages.attendance-date')
                                <span class="required"> * </span>
                        </label>
                        <input class = "form-control" name="attendanceDate" type = "date" value="{{ \Carbon\Carbon::now()->toDateString() }}" id = "attendanceDate" placeholder="Total Marks" value="{{ old('attendanceDate') }}"  />
                        {!! $errors->first('attendanceDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-3 p-t-20">
                    <div class="form-group {{ $errors->has('isPresent') ? 'has-error' :'' }}">
                        <div class="radio p-0">
                            <input type="radio" name="isPresent" id="present" value="1" checked="checked">
                            <label for="present">@lang('messages.present')</label>
                        </div>
                        <div class="radio p-0">
                            <input type="radio" name="isPresent" id="absent" value="0" checked="">
                            <label for="absent">@lang('messages.absent')</label>
                        </div>
                        {!! $errors->first('isPresent','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>

    </div>
</div>
