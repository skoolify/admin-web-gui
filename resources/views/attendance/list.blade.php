@extends('master')

@section('title', Lang::get('messages.attendance-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
<style>
.custom-validation-error{
    outline: none !important;
    border:1px solid red;
    box-shadow:red !important;
}
.custom-validation-error-msg{
    color: red !important;
}
}
</style>
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.attendance-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('attendanceDate') ? 'has-error' :'' }}">
                                    <label for="attendanceDate">@lang('messages.attendance-date')
                                    </label>
                                    <input class = "form-control datepicker" readonly name="attendanceDate" type = "text" max="{{ \Carbon\Carbon::now()->toDateString() }}" value="{{ \Carbon\Carbon::now()->toDateString() }}" id = "attendanceDate" placeholder="" value="{{ old('attendanceDate') }}"  />
                                    {!! $errors->first('attendanceDate','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12 hidden" id="subject-dropdown-div">
                                <div class="form-group">
                                    <label for="">@lang('messages.subject')</label>
                                    <select name="subjectID" id="filter-subjects" class="form-control filter-select">
                                        <option value="">{{ isset($all) && $all ? $all : 'Select' }} @lang('messages.subject')</option>
                                        @foreach($subjects as $subject)
                                            <option {{ session()->get('tempSubjectID') == $subject->subjectID ? 'selected' : '' }} value="{{ $subject->subjectID }}">{{ $subject->subjectName }}</option>
                                        @endforeach
                                    </select>
                                    <div id="p2-subjects" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12 hidden" id="subject-in-time-table">
                                <div class="form-group">
                                    <label for="">@lang('messages.time-table-subjects')</label>
                                    <select name="timetableSubjectID" id="filter-subjects-in-time-table" class="form-control filter-select">
                                        <option value="">@lang('messages.select-time-table-subject')</option>
                                    </select>
                                    <div id="p2-subjectsInTimeTable" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12 hidden" id="batch-no">
                                <div class="form-group">
                                <label for="batchNo">@lang('messages.batch-no')
                                            <span class="required"> * </span>
                                    </label>
                                    <select name="batchNo" id="filter-batch-no" class="form-control filter-select">
                                        <option value="1">@lang('messages.1')</option>
                                        <option value="2">@lang('messages.2')</option>
                                        <option value="3">@lang('messages.3')</option>
                                        <option value="4">@lang('messages.4')</option>
                                    </select>
                                    {!! $errors->first('marksTotal','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('attendance.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">


                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.attendance')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <!-- <div class="btn-group pull-right"> -->
                        <button class="btn btn-primary pull-right btn-round m-w-50 mr-1" id="add-attendance-today">@lang('messages.add-new-btn') / @lang('messages.edit-btn')</button>
                    <!-- </div> -->
                    @endif
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <button class="btn btn-primary pull-right btn-round mr-1 m-mt-1 m-w-50" id="mark-off-day-attendance" data-toggle="tooltip" title="Mark Off Day Attendance">@lang('messages.off-day-marking')</button>
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable" id="attendance-table-div">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle " id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.student-name')</th>
                                    <th>@lang('messages.teacher-name')</th>
                                    <th class="text-center">@lang('messages.off-day-note')</th>
                                    <th class="text-center">@lang('messages.attendance')</th>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-scrollable hidden" id="subject-attendance-table-div">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="subject-attendance-table" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.student')</th>
                                    <th>@lang('messages.roll-number')</th>
                                    <th class="text-center">@lang('messages.notes')</th>
                                    <th class="text-center">@lang('messages.attendance')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="attendance-today">
            <div class="card-box">
                <div class="card-head">
                    <header>@lang('messages.add-attendance')</header>
                    <!-- <div class="btn-group pull-right"> -->
                        {{-- <button class="btn btn-primary" id="submit-attendance-today">SUBMIT</button> --}}
                        <button class="btn btn-danger pull-right btn-round" id="cancel-attendance-today">@lang('messages.cancel-btn')</button>
                    <!-- </div> -->
                </div>
                <form method="POST" id="bulk-attendance-form" action="{{ url('attendance/add') }}">
                <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    <div class="card-body">
                        <div class="table-scrollable" id="attendance-add-table-div">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle " id="attendance-add-table" style="width: 100% !important;">
                                <thead>
                                    <tr>
                                        <th width ="200" style=" padding-right:0px!important;max-width: 200px;" class="th-w" >
                                        <div class="flex-container" style="justify-content: center">
                                            <div class="round" data-toggle="tooltip" title="Present">
                                                <input type="checkbox" data-index="attendance-check-p" data-target="P" class="mark-attendances check-attendance attendance-check-p" data-json='' id="checkbox-p" />
                                                <label for="checkbox-p"><b class="check-text">@lang('messages.p')</b></label>
                                            </div>

                                            <div class="round round-d" data-toggle="tooltip" title="Absent">
                                                <input type="checkbox" data-index="attendance-check-a" data-target="A" class="mark-attendances check-attendance attendance-check-a" data-json='' id="checkbox-a" />
                                                <label for="checkbox-a"><b class="check-text">@lang('messages.a')</b></label>
                                            </div>

                                            <div class="round round-l" data-toggle="tooltip" title="Leave">
                                                <input type="checkbox" data-index="attendance-check-l" data-target="L" class="mark-attendances check-attendance attendance-check-l" data-json='' id="checkbox-l" />
                                                <label for="checkbox-l"><b class="check-text">@lang('messages.l')</b></label>
                                            </div>

                                            <div class="round round-t" data-toggle="tooltip" title="Late">
                                                <input type="checkbox" data-index="attendance-check-t" data-target="T" class="mark-attendances check-attendance attendance-check-t" data-json='' id="checkbox-t" />
                                                <label for="checkbox-t"><b class="late-text check-text">@lang('messages.late')</b></label>
                                            </div>
                                        </div>
                                        </th>
                                        <!-- <th>Status</th> -->
                                        <th style="max-width:671px;min-width:671px;">@lang('messages.student-name')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div class="col-lg-12 p-t-20" id="add-attendance-form-errors"></div>
                            <div class="col-lg-12 p-t-20 text-center">
                                <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                                <button type="button" id="cancel-attendance-form" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="subject-attendance">
            <div class="card-box">

                <div class="card-head">
                    <header>@lang('messages.add-attendance')</header>
                    <!-- <div class="btn-group pull-right"> -->
                        {{-- <button class="btn btn-primary cancel-subject-attendance-form" id="">SUBMIT</button> --}}
                        <button class="btn btn-danger pull-right btn-round" id="cancel-subject-attendance-today">@lang('messages.cancel')</button>
                    <!-- </div> -->
                </div>
                <form method="POST" id="bulk-subject-attendance-form" action="{{ url('attendance/add') }}">
                <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    <div class="card-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle " id="subject-attendance-add-table" style="width: 100% !important;">
                                <thead>
                                    <tr>
                                        <th style=" padding-right:0px!important;" class="th-w-300">
                                        <div class="flex-container" style="justify-content: center">
                                            <div class="round" data-toggle="tooltip" title="Present">
                                                <input type="checkbox" data-index="subject-attendance-check-p" data-target="P" class="mark-subject-attendances check-subject-attendance subject-attendance-check-p" data-json='' id="subject-checkbox-p" />
                                                <label for="subject-checkbox-p"><b class="check-text">@lang('messages.p')</b></label>
                                            </div>

                                            <div class="round round-d" data-toggle="tooltip" title="Absent">
                                                <input type="checkbox" data-index="subject-attendance-check-a" data-target="A" class="mark-subject-attendances check-subject-attendance subject-attendance-check-a" data-json='' id="subject-checkbox-a" />
                                                <label for="subject-checkbox-a"><b class="check-text">@lang('messages.a')</b></label>
                                            </div>

                                            <div class="round round-l" data-toggle="tooltip" title="Leave">
                                                <input type="checkbox" data-index="subject-attendance-check-l" data-target="L" class="mark-subject-attendances check-subject-attendance subject-attendance-check-l" data-json='' id="subject-checkbox-l" />
                                                <label for="subject-checkbox-l"><b class="check-text">@lang('messages.l')</b></label>
                                            </div>

                                            <div class="round round-t" data-toggle="tooltip" title="Late">
                                                <input type="checkbox" data-index="subject-attendance-check-t" data-target="T" class="mark-subject-attendances check-subject-attendance subject-attendance-check-t" data-json='' id="subject-checkbox-t" />
                                                <label for="subject-checkbox-t"><b class="late-text check-text">@lang('messages.late')</b></label>
                                            </div>
                                        </div>
                                        </th>
                                        <th width="20%">@lang('messages.student')</th>
                                        <th width="20%">@lang('messages.roll-number')</th>
                                        <th width="10%">@lang('messages.notes')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div class="col-lg-12 p-t-20" id="add-subject-attendance-form-errors"></div>
                            <div class="col-lg-12 p-t-20 text-center">
                                <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                                <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default cancel-subject-attendance-form">@lang('messages.cancel-btn')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="offDayMarkingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">@lang('messages.off-day-attendance')</h5>
        <button type="button" style="color:red !important" class="close" data-dismiss="modal" aria-label="Close">
            &times;
        </button>
      </div>
      <div class="modal-body">
            <div class = "row">
            <div class="col-lg-12 p-t-20" id="offDayNoteDiv">
                <label for="attendanceDate">@lang('messages.off-day-note')</label>
            <textarea maxlength="100" name="offDayNote" class="form-control" id="offDayNote"></textarea>
                <span class="custom-validation-error-msg hidden">@lang('messages.off-day-attendance-is-required')</span>
            </div>

            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('messages.close')</button>
        <button type="button" class="btn btn-primary submit-off-day-attendance">@lang('messages.submit-btn')</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
