<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        
        <form method="POST" id="calendar-days-form" action="{{ url('calendar-days/add') }}">
            <div id="submit-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-body row">

                <input type="hidden" id="editHiddenField" value="" />
                <div class="col-lg-3 p-t-20 "> 
                    <div class="form-group {{ $errors->has('holidayName') ? 'has-error' :'' }}">
                        <label for="holidayName">@lang('messages.event-name')
                        </label>
                        <input maxlength="45" class = "form-control" name="holidayName" type = "text" id = "holidayName" value="{{ old('holidayName') }}"  data-validation="required length" data-validation-length="max45" placeholder="{{Lang::get('messages.event-name')}}">
                        {!! $errors->first('holidayName','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-3 p-t-20 calender-add">  
                    <div class="form-group">
                        <label for="">@lang('messages.start-from')</label>
                        <input type="date" name="calenderdateFrom" value="{{ \Carbon\Carbon::now()->toDateString() }}" id="calender-filter-date-from" class="form-control" />
                        <div id="p2-date-from" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col-lg-3 p-t-20 endDateDiv hidden">  
                    <div class="form-group">
                        <label for="">@lang('messages.end-to')</label>
                        <input type="date" name="calenderdateTo" value="{{ \Carbon\Carbon::now()->toDateString() }}" id="calender-filter-date-to" class="form-control" />
                        <div id="p2-date-to" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col-lg-3 p-t-20 calender-edit hidden">
                    <div class="form-group {{ $errors->has('calendarDate') ? 'has-error' :'' }}">
                        <label for="calendarDate" >@lang('messages.calendar-date')
                        </label>
                        <input class = "form-control" name="calendarDate" type = "date" id = "calendarDate" value="{{ old('calendarDate') }}" data-validation="required" data-validation-error-msg="Date is required">
                        {!! $errors->first('calendarDate','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                
                <div class="col-lg-3 p-t-20 multipleDaysDiv">
                    <div class="checkbox checkbox-aqua mt-5">
                        <input id="multipleDays" class="multipleDays" type="checkbox">
                        <label for="multipleDays">
                            @lang('messages.multiple-days')
                        </label>
                    </div>
                </div>

                <div class="col-lg-12 p-t-20 text-center">
                    <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                    <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>

                <div class="col-lg-12 p-t-20" id="add-form-errors">
                </div>
            </div>
        </form>
        
    </div>
</div>
