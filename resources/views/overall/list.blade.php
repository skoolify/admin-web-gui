@extends('master')

@section('title', Lang::get('messages.overall-result-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.overall-result-management')))

    <div class="row">
        <div class="col-sm-12 col-md-12 col-xl-12">
                <div class="card-box">
                    <div class="card-head">
                        <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                    </div>
                    
                    <form action="" id="filter-form">
                        <div class="card-body">
                            <div class="row">
                                @include('partials.components.schools-filter')
                                @include('partials.components.branches-filter')
                                @include('partials.components.class-levels-filter')
                                @include('partials.components.classes-filter')
                                @include('partials.components.exam-term-filter')
                                @include('partials.components.students-filter')
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @include('overall.add')

            <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
                <div class="card-box">
                    @if (session('flash-message'))
                        <div class="alert alert-{{ head(session('flash-message')) }}">
                            {{ last(session('flash-message')) }}
                        </div>
                    @endif
                    <div class="card-head">
                        <header>@lang('messages.overall-result-list')</header>
                        @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))

                        {{-- <button type="button" class="printExamsResult btn btn-round btn-danger pull-right" data-student-id="28142" data-check-isassessment="0" data-examterm-id="1"><i class="fa fa-plus"></i> Print</button> --}}

                        <button type="button" class="btn btn-round btn-primary pull-right" id="show-add-form-container"><i class="fa fa-plus"></i> @lang('messages.add-new-btn') / @lang('messages.edit-btn')</button>
                        @endif
                    </div>
                   
                    <div class="card-body">
                    @if(Session::has('user'))
                        @if(Session::get('user')->isAdmin)
                            <button id="publish-button" class="btn  btn-round btn-md  m-t-20 " style="width: 20% !important;display:none;">@lang('messages.publish')</button>
                        @endif
                    @endif
                        <div class="table-scrollable" id="overall-result-list">
                            <div id="overall-div" >
                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="overall-result-table">
                                    <thead>
                                        <tr>
                                            <th width="8%">#</th>   
                                            <th width="8%">@lang('messages.student')</th>         
                                            <th width="8%">@lang('messages.percentage')</th>
                                            <th width="8%">@lang('messages.rank')</th>
                                            <th width="8%">@lang('messages.grade')</th>
                                            <th width="58%">@lang('messages.notes')</th>
                                            <th width="8%">@lang('messages.detail')</th>
                                            <th width="8%">@lang('messages.status-column')</th>
                                            <th>@lang('messages.completed')</th>
                                        </tr>
                                    </thead>
                                    <tbody id="overall-result-list-table">
                                    </tbody>
                                </table>
                            </div>
                            <div id="assesment-div" style="display:none">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="overall-result-table-assessment">
                                    <thead>
                                        <tr>
                                            <th width="10%">#</th>   
                                            <th width="10%">@lang('messages.student')</th>
                                            <th width="50%">@lang('messages.notes')</th>
                                            <th width="10%">@lang('messages.grade')</th>
                                            <th width="10%">@lang('messages.status-column')</th>
                                            <th width="10%">@lang('messages.completed')</th>
                                            <th width="10%">@lang('messages.action-column')</th>
                                        </tr>
                                    </thead>
                                    <tbody id="overall-result-list-table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
       <div style="text-align:center">  
           <h5 class="" id="billing-period-modal" style="color:#337ab7;font-weight:bold;    position: absolute;
    right: 0;
    left: 0;z-index:0" > @lang('messages.academics-non-academics') </h5>
        </div>
        <h4 class="modal-title" style="margin-left:0px !important; margin-top:20px; position: absolute;color:#337ab7 !important" id="exampleModalLabel" > @lang('messages.detail') (<span style="color:#337ab7; font-weight:bold;" id="student-name"></span> )</h4>
        <a class="" data-dismiss="modal" aria-label="Close"style="z-index:2">
          <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
        </a>
      </div>
      <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="fee-detail">
                            <tbody>
                                <tr>
                                    <div class="row" style="padding-top: 7px;">
                                        <div class="col-md-2">
                                            <p style="padding-top: 8px;">@lang('messages.percentage')</p>
                                        </div>
                                        <div class="col-md-2">
                                            <p style="padding-top: 8px;" id="percentage"> </p>
                                        </div>
                                        <div class="col-md-1">
                                            <p style="padding-top: 8px;">@lang('messages.rank')<span style="color:red;">*</span></p>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="form-control"  id="rankField" name="rankField"/> 
                                        </div>
                                        <div class="col-md-1">
                                            <p style="padding-top: 8px;">@lang('messages.grade')<span style="color:red;">*</span></p>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="form-control"  id="gradeField" name="gradeField"/>
                                        </div>
                                    </div>
                                </tr>
                                <tr>
                                    <td class="p-2">@lang('messages.pass') / @lang('messages.fail') </td>
                                    <td class="p-2 br-2" id="passStatus"> </td>
                                    <td class="p-2">@lang('messages.hold') / @lang('messages.release') </td>
                                    <td class="p-2" id="holdStatus"> </td>
                                </tr>
                                <tr>
                                    <td class="p-2">@lang('messages.notes') </td>
                                    <td class="p-2 " colspan="3"> <textarea class="form-control"  id="overallNotesField" name="overallNotesField"> </textarea> </td>
                                </tr>
                                <tr>
                                    <td class="p-2" colspan="4"> <button class="update-overall-exam-result btn btn-primary  pull-right btn-sm" data-json=""> @lang('messages.update')</button> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        <div class="row">
            <div class="col-md-12"  style=" margin-bottom:20px;">
                    <h4 class="modal-title" id="exampleModalLabel" style="color:#337ab7" > @lang('messages.academic')</h4>
                <table class="table  fixed_header" id="subject-detail-table">
                    <thead>
                        <tr>
                            <th class="p-1">@lang('messages.subject')</th>
                            <th class="p-1">@lang('messages.total-marks')</th>
                            <th class="p-1">@lang('messages.obtain-marks')</th>
                            <th class="p-1">@lang('messages.grade')</th>
                            <th class="p-1">@lang('messages.notes')</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  class="p-2 text-center center" colspan="5">@lang('messages.record-not-found')</td>
                        </tr>
                    </tbody>
                </table>
                <span id="success-span" style="color:green"></span>
                <span id="error-span" style="color:red" ></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"  style=" margin-bottom:20px;">
                    <h4 class="modal-title" id="exampleModalLabel" style="color:#337ab7" > @lang('messages.non-academic')</h4>
                <table class="table fixed_header w-100" id="non-subject-detail-table">
                    <thead>
                        <tr>
                            <th class="p-1">@lang('messages.non-subject')</th>
                            <th class="p-1">@lang('messages.grade')</th>
                            <th class="p-1">@lang('messages.notes')</th>
                            <th class="p-1">@lang('messages.legend')</th>
                            <th class="p-1"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  class="p-2 text-center center" colspan="4">@lang('messages.record-not-found')</td>
                        </tr>
                    </tbody>
                </table>
                <span id="success-span" style="color:green"></span>
                <span id="error-span" style="color:red" ></span>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>

<table id="subject-result-detail-table-heading" class="table fixed_header w-100 hidden">
    <thead>
        <tr>
            <th>@lang('messages.results')</th>
        </tr>
    </thead>
</table>
    
<table id="subject-result-detail-table" class="table fixed_header w-100 hidden" >        
    <thead>
        <tr>
            <th>@lang('messages.s-number')</th>
            <th>@lang('messages.subject')</th>
            <th style="text-align: center;">@lang('messages.total-marks')</th>
            <th>@lang('messages.obtain-marks')</th>
            <th>@lang('messages.grade')</th>
            <th>@lang('messages.comment')</th>
        </tr>
    </thead>
    <tbody id="subject-result-detail-table-body">
        
    </tbody>
</table>

<table id="subject-result-detail-table-without-grade" class="table fixed_header w-100 hidden" >        
    <thead>
        <tr>
            <th>@lang('messages.s-number')</th>
            <th>@lang('messages.subject')</th>
            <th style="text-align: center;">@lang('messages.total-marks')</th>
            <th>@lang('messages.obtain-marks')</th>
            <th>@lang('messages.comment')</th>
        </tr>
    </thead>
    <tbody id="subject-result-detail-table-body-without-grade">
        
    </tbody>
</table>

<table id="subject-result-detail-table-without-marks" class="table fixed_header w-100 hidden" >        
    <thead>
        <tr>
            <th>@lang('messages.s-number')</th>
            <th>@lang('messages.subject')</th>
            <th>@lang('messages.grade')</th>
            <th>@lang('messages.comment')</th>
        </tr>
    </thead>
    <tbody id="subject-result-detail-table-body-without-marks">
        
    </tbody>
</table>

<table id="subject-result-detail-table-without-marks-and-grade" class="table fixed_header w-100 hidden" >        
    <thead>
        <tr>
            <th>@lang('messages.s-number')</th>
            <th>@lang('messages.subject')</th>
            <th>@lang('messages.comment')</th>
        </tr>
    </thead>
    <tbody id="subject-result-detail-table-body-without-marks-and-grade">
        
    </tbody>
</table>

    <table id="non-subject-result-detail-table-heading" class="table fixed_header w-100 hidden">
        <thead>
            <tr>
                <th style="text-align: center;font-size:14;">Assessments</th>
            </tr>
        </thead>
    </table>
    <table id="non-subject-result-detail-table" class="table fixed_header w-100 hidden" >
        <thead>
            <tr>
                <th>@lang('messages.s-number')</th>
                <th>@lang('messages.subject')</th>
                <th>@lang('messages.response')</th>
                <th>@lang('messages.grade')</th>
                <th>@lang('messages.comment')</th>
            </tr>
        </thead>
        <tbody id="non-subject-result-detail-table-body">
            
        </tbody>
    </table>

    <table id="non-subject-result-detail-table-without-grade" class="table fixed_header w-100 hidden" >
        <thead>
            <tr>
                <th>@lang('messages.s-number')</th>
                <th>@lang('messages.subject')</th>
                <th>@lang('messages.response')</th>
                <th>@lang('messages.comment')</th>
            </tr>
        </thead>
        <tbody id="non-subject-result-detail-table-body-without-grade">
            
        </tbody>
    </table>

    <table id="attendance-details-table-heading" class="table fixed_header w-100 hidden">
        <thead>
            <tr>
                <th style="text-align: center;font-size:14;">@lang('messages.attendance-detail')</th>
            </tr>
        </thead>
    </table>

    <table class="table fixed_header w-100 hidden" id="attendance-details">
        <thead>
            <tr>
                <th>@lang('messages.total-days')</th>
                <th>@lang('messages.present')</th>
                <th>@lang('messages.absent')</th>
                <th>@lang('messages.late')</th>
                <th>@lang('messages.leave')</th>
            </tr>
        </thead>
        <tbody id="attendance-details-table-body">
            
        </tbody>
    </table>
    
    <table class="table fixed_header w-100 hidden" id="overall-exam-notes">

    </table>

@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="{{asset('assets/js/jspdf.min.js')}}"></script>
<script src="{{asset('assets/js/auto-table.min.js')}}"></script>
<script src="{{asset('assets/js/jspdf.plugin.autotable.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
@endpush
