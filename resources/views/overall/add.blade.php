<div class="col-sm-12 col-md-12 col-xl-12" style="display:none;" id="add-form-container">
    <div class="card-box">
        <div class="col-lg-12">
            <form action="#" id="overall-result-form">
                <div id="p2-students-result-list" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100% !important">
                </div>
                <table class="table table-striped  table-hover table-checkable order-column valign-middle" style="width: 100% !important" id="student-overall-result-table">
                    <thead>
                        <tr>
                            <th>@lang('messages.s-number')</th>
                            <th>@lang('messages.roll-number')</th>
                            <th>@lang('messages.student')</th>
                            <th class="text-center" width="20%">@lang('messages.notes')</th>
                            <th class="" width="10%">@lang('messages.percentage')</th>
                            <th width="10%">@lang('messages.rank')</th>
                            <th class="" width="10%" >@lang('messages.results-status')</th>
                            <th width="10%">@lang('messages.pass') | @lang('messages.fail')</th>
                            <th width="15%" >@lang('messages.release') | @lang('messages.hold')</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="6">
                            <strong style="color:#188ae2;font-weight:bold">@lang('messages.select-exam-term-first')</<strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="col-lg-12 p-t-20" id="add-form-errors"></div>
                        <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" id="submit-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">@lang('messages.submit-btn')</button>
                        <button type="button" id="hide-add-form-container" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">@lang('messages.cancel-btn')</button>
                </div>
            </form>
        </div>
    </div>
</div>
