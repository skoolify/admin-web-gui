@extends('master')

@section('title', Lang::get('messages.staffAttendance'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.staffAttendance')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="settlement-report-form">
                    <div class="card-body">
                        <div class="row">
                            @if (session()->get('user')->isAdmin == 1)
                                @include('partials.components.schools-filter')
                                @include('partials.components.branches-filter')
                            @endif
                            @include('partials.components.date-from-filter')
                            @include('partials.components.settlement-date-to-filter')

                            <div class="col-lg-3 justify-content-center align-self-center mt-3">
                                <button class="btn btn-primary align-middle" type="submit" id="searchSettlementReport"><i class="fa fa-search"></i> @lang('messages.search')</button>
                                {{-- <button class="btn btn-warning" id="exportSettlementReport" type="button" title="Clear"><i class="fa fa-refresh"></i> @lang('messages.export-excel')</button> --}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">
                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <header>@lang('messages.staffAttendanceList')</header>
                            </div>
                            @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                                <div class="col-md-2 pl-0">

                                </div>
                                <div class="col-md-1">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table settlement-report-table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('messages.name')</th>
                                    <th>@lang('messages.month')</th>
                                    <th>@lang('messages.present')</th>
                                    <th>@lang('messages.absent')</th>
                                    <th>@lang('messages.late')</th>
                                    <th>@lang('messages.leave')</th>
                                    <th>@lang('messages.holiday')</th>
                                    <th>@lang('messages.total')</th>
                                    <th>@lang('messages.detail')</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" data-backdrop="static" keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
         <div style="text-align:center">
             <h5 class="" id="staff-report-modal" style="color:#337ab7;font-weight:bold;position: absolute; right: 0;left: 0;padding-top: 2px;font-size: 18px !important;" > @lang('messages.report')</h5>
          </div>

          <a class="closeModalDiv" data-dismiss="modal" aria-label="Close" style="z-index:1000">
            <span aria-hidden="true" style="color: #ff5f5f;font-size: 23px;">&times;</span>
          </a>
        </div>
          {{-- <div class="container">
              <div class="row">

                  <div class="col-md-4">
                      <h4 class="modal-title" style="margin-top: 12px;margin-left:0px !important; position: absolute;color:#337ab7 !important" id="exampleModalLabel" > @lang('messages.detail') (<span style="color:#337ab7; font-weight:bold;" id="student-name"></span> )</h4>
                  </div>

                  <div class="col-md-4" style="text-align: center;">
                      <label id="successLabel" style="font-weight: 500;padding-top: 10px; color:red;display:none;" for="">@lang('messages.successfully-updated')</label>
                  </div>

                  <div class="col-md-4">
                      <div class="row">
                          <div class="col-md-9">
                              <label style="float: right;padding-top: 10px;color: #337ab7;font-size: 18px !important;font-weight: bold;" id="publishedToggleLabel" for=""></label>
                          </div>
                          <div class="col-md-3">
                              <label style="margin-top: 12px;float: right;" class="switchToggle">
                              </label>
                          </div>
                      </div>
                  </div>
              </div>
          </div> --}}

          <div class="container-fluid">
              {{-- <div class="row" style="border-bottom: 1px solid #EFEFEF;">
                  <div class="col-md-12">
                      <table class="table" id="fee-detail">
                          <tbody>
                              <tr>
                                  <td class="p-1">@lang('messages.invoice-number') </td>
                                  <td class="p-1 br-1" id="invoiceNumbers"> </td>
                                  <td class="p-1">@lang('messages.invoice-date') </td>
                                  <td class="p-1" id="invoiceDates"> </td>
                              </tr>
                              <tr>
                                  <td class="p-1">@lang('messages.invoice-due-date') </td>
                                  <td class="p-1 br-1" id="invoiceDueDate"> </td>
                                  <td class="p-1">@lang('messages.payment-date') </td>
                                  <td class="p-1" id="invoicePaymentDate"> </td>
                              </tr>
                              <tr>
                                  <td class="p-1">@lang('messages.amount-before-tax') </td>
                                  <td class="p-1 br-1" id="amountBeforeTax"> </td>
                                  <td class="p-1">@lang('messages.late-fee-charges') </td>
                                  <td class="p-1" id="invoicePenaltyValue"> </td>

                              </tr>
                              <tr>
                                  <td class="p-1">@lang('messages.tax-amount') </td>
                                  <td class="p-1 br-1" id="taxAmount"> </td>
                                  <td class="p-1">@lang('messages.payment-status') </td>
                                  <td class="p-1" id="paymentStatus"> </td>

                              </tr>
                              <tr>
                                  <td class="p-1">@lang('messages.total-amount-before-due-date') </td>
                                  <td class="p-1 br-1" id="invoiceTaxAmountDue"> </td>
                                  <td class="p-1">@lang('messages.total-amount-after-due-date') </td>
                                  <td class="p-1" id="invoiceTaxAmountAfterDue"> </td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div> --}}

          <div class="row">
              <div class="col-md-12"  style="height:203px; overflow-y:scroll; margin-bottom:20px;">
                      {{-- <h4 class="modal-title" id="exampleModalLabel" style="color:#337ab7" > @lang('messages.fee-types')</h4> --}}
                  <table class="table " id="staff-attendance-detail-table">
                      <thead>
                          <tr>
                              <th class="p-1">@lang('messages.name')</th>
                              <th class="p-1">@lang('messages.date')</th>
                              <th class="p-1">@lang('messages.arrival')</th>
                              <th class="p-1">@lang('messages.departure')</th>
                              <th class="p-1">@lang('messages.status-column')</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td  class="p-2 text-center center" colspan="3">@lang('messages.record-not-found')</td>
                          </tr>
                      </tbody>
                  </table>
                  <span id="success-span" style="color:green"></span>
                  <span id="error-span" style="color:red" ></span>
              </div>
          </div>
      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger cancelBtnModal" data-dismiss="modal">@lang('messages.close')</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
@endpush
