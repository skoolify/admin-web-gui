@extends('master')

@section('title', Lang::get('messages.quiz-results-management'))

@section('extra-css')
<!-- form layout -->
<link href="{{ asset('assets/css/pages/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-content">
    @include('partials.page-bar', array('pageTitle' => Lang::get('messages.quiz-results-management')))

    <div class="row">

        <div class="col-sm-12 col-md-12 col-xl-12">
            <div class="card-box">

                <div class="card-head">
                    <header id="filter-form-title"> @lang('messages.filters-heading')</header>
                </div>

                <form action="" id="filter-form">
                    <div class="card-body">
                        <div class="row">
                            @include('partials.components.schools-filter')
                            @include('partials.components.branches-filter')
                            @include('partials.components.class-levels-filter')
                            @include('partials.components.classes-filter')
                            @include('partials.components.subjects-filter')
                            @include('partials.components.students-filter')

                            <div class="col-lg-3">
                                <div class="form-group {{ $errors->has('quizDate') ? 'has-error' :'' }}">
                                    <label for="quizDate">@lang('messages.quiz-date')
                                            <span class="required"> * </span>
                                    </label>
                                    <input class = "form-control" name="quizDate" type = "date" id = "quizDate" placeholder="quizDate" value="" required  />
                                    {!! $errors->first('quizDate','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-lg-3 hidden hidden-div">
                                <div class="form-group {{ $errors->has('marksTotal') ? 'has-error' :'' }}">
                                    <label for="marksTotal">@lang('messages.total-marks')
                                            <span class="required"> * </span>
                                    </label>
                                    <input class="form-control" name="marksTotal" type = "text" id="marksTotal" placeholder="Total Marks" value="{{ old('marksTotal') }}"  />
                                    <span class="marksTotal-error hidden">@lang('messages.total-marks-required') </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 p-t-20">
                                <div class="checkbox checkbox-aqua">
                                    <input type="hidden" name="showGraph" id="showGraph">
                                    <input id="showGraphInput" class="studentCheckBox" type="checkbox" />
                                    <label for="showGraphInput">@lang('messages.show-graph')</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @include('quiz-results.add')

        <div class="col-sm-12 col-md-12 col-xl-12" id="results-list">
            <div class="card-box">

                @if (session('flash-message'))
                    <div class="alert alert-{{ head(session('flash-message')) }}">
                        {{ last(session('flash-message')) }}
                    </div>
                @endif

                <div class="card-head">
                    <header>@lang('messages.quizResultsList')</header>
                    @if (Authorization::check(Route::currentRouteName(), "add", Authorization::getParent(Route::currentRouteName()), true))
                    <!-- <div class="btn-group pull-right"> -->
                        <button type="button" class="btn btn-round btn-primary pull-right"  id="add-add-quiz-result"> @lang('messages.add-new-btn') / @lang('messages.edit-btn') </button>
                    <!-- </div> -->
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example44" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>@lang('messages.roll-number')</th>
                                    <th>@lang('messages.student-name')</th>
                                    <th>@lang('messages.subject-name')</th>
                                    <th>@lang('messages.total-marks')</th>
                                    <th>@lang('messages.obtained-marks')</th>
                                    <th>@lang('messages.comments')</th>
                                    <th>@lang('messages.quiz-date')</th>
                                    <th>@lang('messages.action-column')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-xl-12 hidden" id="add-quiz-results-table">
            <div id="bulk-quiz-result-loader" class="mdl-progress mdl-js-progress mdl-progress__indeterminate hidden" style="width: 100%"></div>
            <div class="card-box">

                <div class="card-head">
                    <header>@lang('messages.add-quiz-results')</header>
                    <!-- <div class="btn-group pull-right"> -->
                        <button type="button" class="btn btn-primary pull-right mr-2" id="submit-add-quiz-result">@lang('messages.submit-btn')</button>
                        <button type="button" class="btn btn-danger pull-right mr-2" id="cancel-add-quiz-result">@lang('messages.cancel-btn')</button>
                    <!-- </div> -->
                </div>

                <div class="card-body">
                    <div class="table-scrollable">
                        <form action="#" id="bulk-quiz-results-form">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="quiz-results-add-table" style="width: 100% !important;" >
                                <thead>
                                    <tr>
                                    <!-- <th style="display:none;">testing</th> -->
                                        <th>@lang('messages.roll-number')</th>
                                        <th>@lang('messages.student-name')</th>
                                        <th>@lang('messages.obtained-marks')</th>
                                        <th>@lang('messages.comments')</th>
                                    </tr>
                                </thead>
                                <tbody id="quiz-results-add-tbody">

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('extra-scripts')
<script src="{{ asset('scripts/ajax-scripts/'.Route::currentRouteName().'.js?version='.time()) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
@endpush
