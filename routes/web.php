<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Auth Routes */

Route::get('/test', 'AcademicController@test')->name('test');
Route::get('/switch-language', 'AuthController@switchLanguage')->name('switch.language');

Route::group(['middleware' => 'setLanguage'], function(){
    Route::prefix('/auth')->group(function(){
        Route::get('/login', 'AuthController@index')->name('auth-login');
        Route::get('/authorization', 'AuthController@authorization')->name('auth-authorization');
        Route::get('/user/detail', 'AuthController@userDetail')->name('auth-user-detail');
        Route::get('/password', 'AuthController@password')->name('auth-change-password');
        Route::post('/login', 'AuthController@login');
        Route::post('/validate', 'AuthController@validateUser');
        Route::get('/userModules', 'AuthController@userModules');
        Route::get('/switch-role/{id}', 'AuthController@switchRole')->name('switch.role');
        // Route::get('/switch-language', 'AuthController@switchLanguage')->name('switch.language');
    });

    Route::prefix('/secure/payment')->group(function(){
        Route::post('/', 'PaymentsController@viewForm')->name('view.payment.form');
        Route::post('/hash', 'PaymentsController@generateHash')->name('payment.generate.hash');
        Route::post('/status', 'PaymentsController@paymentStatus')->name('payment.success');
        Route::post('/school', 'SchoolPaymentController@submitPayment')->name('submit.school.payment.form');

        // testing route
        // Route::get('/', 'PaymentsController@viewForm')->name('view.payment.form');
    });

    Route::post('/messages/attachment', 'MessagesController@pictureUpload');
    Route::post('/daily-diary/attachment', 'DailyDiaryController@pictureUpload');
    Route::post('/daily-diary/document', 'DailyDiaryController@documentUpload');
    //Route::get('/test', 'DailyDiaryController@testPictureUpload');
    Route::post('/students/picture', 'StudentsController@pictureUpload');
    Route::post('/schools/logo', 'SchoolsController@compressImageAndConvertToBase64');
    Route::post('/school/logo', 'SchoolsController@compressBase64Image');

    /* School Logo Routes */
    Route::prefix('/school-logo')->group(function(){
        Route::post('/', 'SchoolLogoController@index')->name('school-logo');
    });

    Route::fallback('ErrorsController@error404');

    /* Error Routes */
    Route::get('/404', 'ErrorsController@error404')->name('404');

    Route::get('/500', 'ErrorsController@error500')->name('500');

    Route::get('/403', 'ErrorsController@error403')->name('403');
    /* Error Routes */

    Route::get('/', 'AuthController@default')->name('default');

    Route::get('/student-fee-challan/{id}/voucher', 'StudentFeeChallanController@printFeeVoucher')->name('student-fee-challan.printVoucher');

    Route::group(['middleware' => ['authentication', 'permissions', 'lastVisitedRoute']], function(){

        /* Schools Routes */
        Route::prefix('/schools')->group(function(){
            Route::get('/', 'SchoolsController@index')->name('schools')->middleware('resetTempParams');
            Route::get('/add', 'SchoolsController@create')->name('schools.add');
            Route::get('/edit/{id}', 'SchoolsController@edit')->name('schools.edit');
            Route::post('/add', 'SchoolsController@store');
            Route::post('/edit/{id}', 'SchoolsController@update');
        });

        /* School Branches Routes */
        Route::prefix('/school-branches')->group(function(){
            Route::get('/', 'SchoolBranchesController@index')->name('school-branches')->middleware('resetTempParams');
            Route::get('/add', 'SchoolBranchesController@create')->name('school-branches.add');
            Route::get('/edit/{id}', 'SchoolBranchesController@edit')->name('school-branches.edit');
            Route::post('/add', 'SchoolBranchesController@store');
            Route::post('/edit/{id}', 'SchoolBranchesController@update');
        });

        /* Countries Routes */
        Route::prefix('/countries')->group(function(){
            Route::get('/', 'CountriesController@index')->name('countries')->middleware('resetTempParams');
            Route::get('/add', 'CountriesController@create')->name('countries.add');
            Route::get('/edit/{id}', 'CountriesController@edit')->name('countries.edit');
            Route::post('/add', 'CountriesController@store');
            Route::post('/edit/{id}', 'CountriesController@update');
        });

        /* Cities Routes */
        Route::prefix('/cities')->group(function(){
            Route::get('/', 'CitiesController@index')->name('cities')->middleware('resetTempParams');
            Route::get('/add', 'CitiesController@create')->name('cities.add');
            Route::get('/edit/{id}', 'CitiesController@edit')->name('cities.edit');
            Route::post('/add', 'CitiesController@store');
            Route::post('/edit/{id}', 'CitiesController@update');
        });

        /* Areas Routes */
        Route::prefix('/areas')->group(function(){
            Route::get('/', 'AreasController@index')->name('areas')->middleware('resetTempParams');
            Route::get('/add', 'AreasController@create')->name('areas.add');
            Route::get('/edit/{id}', 'AreasController@edit')->name('areas.edit');
            Route::post('/add', 'AreasController@store');
            Route::post('/edit/{id}', 'AreasController@update');
        });

        /* Fee Type Routes */
        Route::prefix('/fee-type')->group(function(){
            Route::get('/', 'FeeTypeController@index')->name('fee-type')->middleware('resetTempParams');
            Route::get('/add', 'FeeTypeController@create')->name('fee-type.add');
            Route::get('/edit/{id}', 'FeeTypeController@edit')->name('fee-type.edit');
            Route::post('/add', 'FeeTypeController@store');
            Route::post('/edit/{id}', 'FeeTypeController@update');
        });

        /* Roles Routes */
        Route::prefix('/roles')->group(function(){
            Route::get('/', 'RolesController@index')->name('roles')->middleware('resetTempParams');
            Route::get('/add', 'RolesController@create')->name('roles.add');
            Route::get('/edit/{id}', 'RolesController@edit')->name('roles.edit');
            Route::post('/add', 'RolesController@store');
            Route::post('/edit/{id}', 'RolesController@update');
        });

        /* Assign Roles Routes */
        Route::prefix('/assign-roles')->group(function(){
            Route::get('/', 'AssignRolesController@index')->name('assign-roles')->middleware('resetTempParams');
            Route::get('/add', 'AssignRolesController@create')->name('assign-roles.add');
            Route::get('/edit/{id}', 'AssignRolesController@edit')->name('assign-roles.edit');
            Route::post('/add', 'AssignRolesController@store');
            Route::post('/edit/{id}', 'AssignRolesController@update');
        });

        /* Users Routes */
        Route::prefix('/users')->group(function(){
            Route::get('/', 'UsersController@index')->name('users')->middleware('resetTempParams');
            Route::get('/add', 'UsersController@create')->name('users.add');
            Route::get('/edit/{id}', 'UsersController@edit')->name('users.edit');
            Route::post('/add', 'UsersController@store');
            Route::post('/edit/{id}', 'UsersController@update');
        });


        /* Classes Routes */
        Route::prefix('/classes')->group(function(){
            Route::get('/', 'ClassesController@index')->name('classes')->middleware('resetTempParams');
            Route::get('/add', 'ClassesController@create')->name('classes.add');
            Route::get('/edit/{id}', 'ClassesController@edit')->name('classes.edit');
            Route::post('/add', 'ClassesController@store');
            Route::post('/edit/{id}', 'ClassesController@update');
        });

        /* Attendance Routes */
        Route::prefix('/attendance')->group(function(){
            Route::get('/', 'AttendanceController@index')->name('attendance')->middleware('resetTempParams');
            Route::get('/add', 'AttendanceController@create')->name('attendance.add');
            Route::get('/edit/{id}', 'AttendanceController@edit')->name('attendance.edit');
            Route::post('/add', 'AttendanceController@store');
            Route::post('/edit/{id}', 'AttendanceController@update');
        });

        /* Timetable  Routes */
        Route::prefix('/time-table')->group(function(){
            Route::get('/', 'TimeTableController@index')->name('time-table')->middleware('resetTempParams');
            Route::get('/add', 'TimeTableController@create')->name('time-table.add');
            Route::get('/edit/{id}', 'TimeTableController@edit')->name('time-table.edit');
            Route::post('/add', 'TimeTableController@store');
            Route::post('/edit/{id}', 'TimeTableController@update');
        });

        /* new Timetable  Routes */
        Route::prefix('/new-time-table')->group(function(){
            Route::get('/', 'NewTimeTableController@index')->name('new-time-table')->middleware('resetTempParams');
            Route::get('/add', 'NewTimeTableController@create')->name('new-time-table.add');
            Route::get('/edit/{id}', 'NewTimeTableController@edit')->name('new-time-table.edit');
            Route::post('/add', 'NewTimeTableController@store');
            Route::post('/edit/{id}', 'NewTimeTableController@update');
        });

        /* Attendance Today */
        Route::prefix('/attendance-today')->group(function(){
            Route::get('/', 'AttendanceTodayController@index')->name('attendance-today')->middleware('resetTempParams');
            Route::get('/add', 'AttendanceTodayController@create')->name('attendance-today.add');
            Route::get('/edit/{id}', 'AttendanceTodayController@edit')->name('attendance-today.edit');
            Route::post('/add', 'AttendanceTodayController@store');
            Route::post('/edit/{id}', 'AttendanceTodayController@update');
        });

        /* Class Levels Routes */
        Route::prefix('/class-levels')->group(function(){
            Route::get('/', 'ClassLevelsController@index')->name('class-levels')->middleware('resetTempParams');
            Route::get('/add', 'ClassLevelsController@create')->name('class-levels.add');
            Route::get('/edit/{id}', 'ClassLevelsController@edit')->name('class-levels.edit');
            Route::post('/add', 'ClassLevelsController@store');
            Route::post('/edit/{id}', 'ClassLevelsController@update');
        });

        /* Calendar Days Routes */
        Route::prefix('/calendar-days')->group(function(){
            Route::get('/', 'CalendarDaysController@index')->name('calendar-days')->middleware('resetTempParams');
            Route::get('/add', 'CalendarDaysController@create')->name('calendar-days.add');
            Route::get('/edit/{id}', 'CalendarDaysController@edit')->name('calendar-days.edit');
            Route::post('/add', 'CalendarDaysController@store');
            Route::post('/edit/{id}', 'CalendarDaysController@update');
        });

        /* Academic Calendar Routes */
        Route::prefix('/academic-calendar')->group(function(){
            Route::get('/', 'AcademicCalendarController@index')->name('academic-calendar')->middleware('resetTempParams');
            Route::get('/add', 'AcademicCalendarController@create')->name('academic-calendar.add');
            Route::get('/edit/{id}', 'AcademicCalendarController@edit')->name('academic-calendar.edit');
            Route::post('/add', 'AcademicCalendarController@store');
            Route::post('/edit/{id}', 'AcademicCalendarController@update');
        });

        /* Subjects Routes */
        Route::prefix('/subjects')->group(function(){
            Route::get('/', 'SubjectsController@index')->name('subjects')->middleware('resetTempParams');
            Route::get('/add', 'SubjectsController@create')->name('subjects.add');
            Route::get('/edit/{id}', 'SubjectsController@edit')->name('subjects.edit');
            Route::post('/add', 'SubjectsController@store');
            Route::post('/edit/{id}', 'SubjectsController@update');
        });

        /* Non-Subjects Routes */
        Route::prefix('/non-subjects')->group(function(){
            Route::get('/', 'NonSubjectsController@index')->name('non-subjects')->middleware('resetTempParams');
            Route::get('/add', 'NonSubjectsController@create')->name('non-subjects.add');
            Route::get('/edit/{id}', 'NonSubjectsController@edit')->name('non-subjects.edit');
            Route::post('/add', 'NonSubjectsController@store');
            Route::post('/edit/{id}', 'NonSubjectsController@update');
        });


        /* Modules Routes */
        Route::prefix('/modules')->group(function(){
            Route::get('/', 'ModulesController@index')->name('modules')->middleware('resetTempParams');
            Route::get('/add', 'ModulesController@create')->name('modules.add');
            Route::get('/edit/{id}', 'ModulesController@edit')->name('modules.edit');
            Route::post('/add', 'ModulesController@store');
            Route::post('/edit/{id}', 'ModulesController@update');
        });


        /* Students Routes */
        Route::prefix('/students')->group(function(){
            Route::get('/', 'StudentsController@index')->name('students')->middleware('resetTempParams');
            Route::get('/add', 'StudentsController@create')->name('students.add');
            Route::get('/edit/{id}', 'StudentsController@edit')->name('students.edit');
            Route::post('/add', 'StudentsController@store');
            Route::post('/edit/{id}', 'StudentsController@update');
        });

        /* Quiz Results Routes */
        Route::prefix('/quiz-results')->group(function(){
            Route::get('/', 'QuizResultsController@index')->name('quiz-results')->middleware('resetTempParams');
            Route::get('/add', 'QuizResultsController@create')->name('quiz-results.add');
            Route::get('/edit/{id}', 'QuizResultsController@edit')->name('quiz-results.edit');
            Route::post('/add', 'QuizResultsController@store')->name('quiz-results.add-post');
            Route::post('/edit/{id}', 'QuizResultsController@update')->name('quiz-results.edit-post');
        });

        /* Student fee challan Routes */
        Route::prefix('/student-fee-challan')->group(function(){
            Route::get('/', 'StudentFeeChallanController@index')->name('student-fee-challan')->middleware('resetTempParams');
            Route::get('/add', 'StudentFeeChallanController@create')->name('student-fee-challan.add');
            Route::get('/edit/{id}', 'StudentFeeChallanController@edit')->name('student-fee-challan.edit');
            Route::post('/add', 'StudentFeeChallanController@store')->name('student-fee-challan.add-post');
            Route::post('/edit/{id}', 'StudentFeeChallanController@update')->name('student-fee-challan.edit-post');
        });

        /* Assessment Routes */
        Route::prefix('/assessments')->group(function(){
            Route::get('/', 'AssessmentController@index')->name('assessments')->middleware('resetTempParams');
            Route::get('/add', 'AssessmentController@create')->name('assessments.add');
            Route::get('/edit/{id}', 'AssessmentController@edit')->name('assessments.edit');
            Route::post('/add', 'AssessmentController@store')->name('assessments.add-post');
            Route::post('/edit/{id}', 'AssessmentController@update')->name('assessments.edit-post');
        });


        /* Exam Term Routes */
        Route::prefix('/exam-term')->group(function(){
            Route::get('/', 'ExamTermController@index')->name('exam-term')->middleware('resetTempParams');
            Route::get('/add', 'ExamTermController@create')->name('exam-term.add');
            Route::get('/edit/{id}', 'ExamTermController@edit')->name('exam-term.edit');
            Route::post('/add', 'ExamTermController@store')->name('exam-term.add-post');
            Route::post('/edit/{id}', 'ExamTermController@update')->name('exam-term.edit-post');
        });


        /* Messages Routes */
        Route::prefix('/messages')->group(function(){
            Route::get('/', 'MessagesController@index')->name('messages')->middleware('resetTempParams');
            Route::get('/add', 'MessagesController@create')->name('messages.add');
            Route::get('/edit/{id}', 'MessagesController@edit')->name('messages.edit');
            Route::post('/add', 'MessagesController@store')->name('messages.add-post');
            Route::post('/edit/{id}', 'MessagesController@update')->name('messages.edit-post');
        });

        /* Exam Results Routes */
        Route::prefix('/exam-term-results')->group(function(){
            Route::get('/', 'ExamTermResultsController@index')->name('exam-term-results')->middleware('resetTempParams');
            Route::get('/add', 'ExamTermResultsController@create')->name('exam-term-results.add');
            Route::get('/edit/{id}', 'ExamTermResultsController@edit')->name('exam-term-results.edit');
            Route::post('/add', 'ExamTermResultsController@store')->name('exam-term-results.add-post');
            Route::post('/edit/{id}', 'ExamTermResultsController@update')->name('exam-term-results.edit-post');
        });

        /* Exam Results Routes */
        Route::prefix('/exam-subject-results')->group(function(){
            Route::get('/', 'ExamTermSubjectsController@index')->name('exam-subject-results')->middleware('resetTempParams');
            Route::get('/add', 'ExamTermSubjectsController@create')->name('exam-subject-results.add');
            Route::get('/edit/{id}', 'ExamTermSubjectsController@edit')->name('exam-subject-results.edit');
            Route::post('/add', 'ExamTermSubjectsController@store')->name('exam-subject-results.add-post');
            Route::post('/edit/{id}', 'ExamTermSubjectsController@update')->name('exam-subject-results.edit-post');
        });

        /* Daily Diary Routes */
        Route::prefix('/daily-diary')->group(function(){
            Route::get('/', 'DailyDiaryController@index')->name('daily-diary')->middleware('resetTempParams');
            Route::get('/add', 'DailyDiaryController@create')->name('daily-diary.add');
            Route::get('/edit/{id}', 'DailyDiaryController@edit')->name('daily-diary.edit');
            Route::post('/add', 'DailyDiaryController@store')->name('daily-diary.add-post');
            Route::post('/edit/{id}', 'DailyDiaryController@update')->name('daily-diary.edit-post');
        });


        /* Parents Routes */
        Route::prefix('/parents')->group(function(){
            Route::get('/', 'ParentsController@index')->name('parents')->middleware('resetTempParams');
            Route::get('/add', 'ParentsController@create')->name('parents.add');
            Route::get('/edit/{id}', 'ParentsController@edit')->name('parents.edit');
            Route::post('/add', 'ParentsController@store')->name('parents.add-post');
            Route::post('/edit/{id}', 'ParentsController@update')->name('parents.edit-post');
        });

        /* Role Access Rights Routes */
        Route::prefix('/role-access-rights')->group(function(){
            Route::get('/', 'RoleAccessRightsController@index')->name('role-access-rights')->middleware('resetTempParams');
            Route::get('/add', 'RoleAccessRightsController@create')->name('role-access-rights.add');
            Route::get('/edit/{id}', 'RoleAccessRightsController@edit')->name('quiz-role-access-rights.edit');
            Route::post('/add', 'RoleAccessRightsController@store')->name('role-access-rights.add-post');
            Route::post('/edit/{id}', 'RoleAccessRightsController@update')->name('role-access-rights.edit-post');
        });

        /* Communication Requests Routes */
        Route::prefix('/comm-requests')->group(function(){
            Route::get('/', 'CommRequestsController@index')->name('comm-requests')->middleware('resetTempParams');
            Route::get('/add', 'CommRequestsController@create')->name('comm-requests.add');
            Route::get('/edit/{id}', 'CommRequestsController@edit')->name('comm-requests.edit');
            Route::post('/add', 'CommRequestsController@store')->name('comm-requests.add-post');
            Route::post('/edit/{id}', 'CommRequestsController@update')->name('comm-requests.edit-post');
        });

        /* Terms & Condition Routes */
        Route::prefix('/terms-conditions')->group(function(){
            Route::get('/', 'TermsConditionsController@index')->name('terms-conditions')->middleware('resetTempParams');

        });

        /* Contract Routes */
        Route::prefix('/contract')->group(function(){
            Route::get('/', 'ContractController@index')->name('contract')->middleware('resetTempParams');

        });
        /* Contract Routes */
        Route::prefix('/overall-exam')->group(function(){
            Route::get('/', 'OverallController@index')->name('overall-exam')->middleware('resetTempParams');

        });
        Route::prefix('/academic')->group(function(){
            Route::get('/', 'AcademicController@index')->name('academic')->middleware('resetTempParams');

        });
        Route::prefix('/nonacademic')->group(function(){
            Route::get('/', 'NonAcademicController@index')->name('nonacademic')->middleware('resetTempParams');

        });
        Route::prefix('/promotions')->group(function(){
            Route::get('/', 'PromotionController@index')->name('promotions')->middleware('resetTempParams');

        });
        Route::prefix('/student-fee')->group(function(){
            Route::get('/', 'StudentFeeController@index')->name('student-fee')->middleware('resetTempParams');

        });
        Route::prefix('/billing-period')->group(function(){
            Route::get('/', 'BillingPeriodController@index')->name('billing-period')->middleware('resetTempParams');
        });

        Route::prefix('/registration-reports')->group(function(){
            Route::get('/', 'RegistrationReportsController@index')->name('registration-reports')->middleware('resetTempParams');
        });

        Route::prefix('/attendance-reports')->group(function(){
            Route::get('/', 'AttendanceReportsController@index')->name('attendance-reports')->middleware('resetTempParams');
        });

        Route::prefix('/app-installs')->group(function(){
            Route::get('/', 'AppInstallsController@index')->name('app-installs')->middleware('resetTempParams');
        });

        Route::prefix('/activities')->group(function(){
            Route::get('/', 'ActivitiesController@index')->name('activities')->middleware('resetTempParams');
            Route::get('/add', 'ActivitiesController@create')->name('activities.add');
        });

        Route::prefix('/assign-activities')->group(function(){
            Route::get('/', 'AssignActivitiesController@index')->name('assign-activities')->middleware('resetTempParams');
        });

        Route::prefix('/conference')->group(function(){
            Route::get('/', 'ConferenceController@index')
                    ->name('conference')
                    ->middleware('resetTempParams');
        });

        Route::prefix('settlement-report')->group(function(){
            Route::get('/', 'SettlementReportController@index')->name('settlement-report')->middleware('resetTempParams');
        });

        Route::prefix('form-management')->group(function(){
            Route::get('/', 'FormManagementController@index')->name('form-management')->middleware('resetTempParams');
        });

        Route::prefix('staff-attendance')->group(function(){
            Route::get('/', 'StaffAttendanceController@index')->name('staff-attendance')->middleware('resetTempParams');
        });

        Route::prefix('/import-excel')->group(function(){
            Route::get('/', 'ImportParentStudentsController@index')->name('import-excel');
            Route::post('/file','ImportParentStudentsController@importExcel')->name('excel.import');

        });
    });

    Route::prefix('/class-rooms')->group(function(){
        Route::get('/', 'ClassRoomsController@index')->name('class-rooms')->middleware('resetTempParams');
    });

    Route::prefix('/assign-subjects')->group(function(){
        Route::get('/', 'AssignSubjectsController@index')->name('assign-subjects')->middleware('resetTempParams');
    });

    Route::prefix('/subject-time-table')->group(function(){
        Route::get('/', 'SubjectTimeTablesController@index')->name('subject-time-table')->middleware('resetTempParams');
    });

    Route::prefix('/subject-attendance')->group(function(){
        Route::get('/', 'SubjectAttendanceController@index')->name('subject-attendance')->middleware('resetTempParams');
    });

    Route::prefix('/student-tag')->group(function(){
        Route::get('/', 'StudentTagController@index')->name('student-tag')->middleware('resetTempParams');
    });

    Route::prefix('/otp-management')->group(function(){
        Route::get('/', 'OtpController@index')->name('otp-management')->middleware('resetTempParams');
    });

    Route::prefix('/school-payment')->group(function(){
        Route::get('/', 'SchoolPaymentController@index')->name('school-payment')->middleware('resetTempParams');

    });

    /* AJAX Routes */
    Route::prefix('/ajax')->group(function(){
        Route::get('/updateApiToken', 'AjaxController@updateApiToken');
        Route::get('/countries', 'AjaxController@countries');
        Route::get('/cities', 'AjaxController@cities');
        Route::get('/areas', 'AjaxController@areas');
        Route::get('/schools', 'AjaxController@schools');
        Route::get('/branches', 'AjaxController@branches');
        Route::get('/comms', 'AjaxController@comms');
        Route::get('/users', 'AjaxController@users');
        Route::get('/subjectsInTimeTable', 'AjaxController@subjectsInTimeTable');
        Route::get('/roles', 'AjaxController@roles');
        Route::get('/classes', 'AjaxController@classes');
        Route::get('/classLevels', 'AjaxController@classLevels');
        Route::get('/groups', 'AjaxController@groups');
        Route::get('/subjects', 'AjaxController@subjects');
        Route::get('/userSubjects', 'AjaxController@examSubjects');
        Route::get('/students', 'AjaxController@students');
        Route::get('/examTerms', 'AjaxController@examTerms');
        Route::get('/feeTypes', 'AjaxController@feeTypes');
        Route::get('/modules', 'AjaxController@modules');
        Route::get('/non-subjects', 'AjaxController@nonSubjects');
    });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/auth/get/contract', 'AuthController@contractAcceptance');
    Route::get('/auth/contract-acceptance', 'AuthController@contractAcceptanceView')->name('contract-acceptance');
});
